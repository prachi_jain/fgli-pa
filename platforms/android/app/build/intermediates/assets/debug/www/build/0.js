webpackJsonp([0],{

/***/ 1016:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentpendingPageModule", function() { return PaymentpendingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paymentpending__ = __webpack_require__(1064);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentpendingPageModule = /** @class */ (function () {
    function PaymentpendingPageModule() {
    }
    PaymentpendingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__paymentpending__["a" /* PaymentpendingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__paymentpending__["a" /* PaymentpendingPage */]),
            ],
        })
    ], PaymentpendingPageModule);
    return PaymentpendingPageModule;
}());

//# sourceMappingURL=paymentpending.module.js.map

/***/ }),

/***/ 1064:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentpendingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PaymentpendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentpendingPage = /** @class */ (function () {
    function PaymentpendingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PaymentpendingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentpendingPage');
    };
    PaymentpendingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-paymentpending',template:/*ion-inline-start:"/Users/iosdeveloper/Documents/Ionic/PA/fgli-pa/src/pages/health/paymentpending/paymentpending.html"*/'<!--\n  Generated template for the PaymentSuccessPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <!--  <button ion-button class="backBt" ></button> -->\n   <h1><span>Health Insurance</span>Buy Health Policy</h1>\n  <!--  <div class="notification"><span>100</span><img src="assets/imgs/notification.png"> </div> -->\n   <button ion-button menuToggle end class="humburgerMenu">\n           <i></i>\n           <i></i>\n           <i></i>\n   </button>\n </ion-header> \n <ion-content padding class="body-bg ">\n \n <div class="whiteRow paymentSuccess">\n \n <div class="chequeSuccess" [class.hide]="chequeSuccess">\n <h5>\n   <img src="assets/imgs/cong_img.jpg">\n   <!-- <span>You have successfully generated your health insurance policy.</span> -->\n </h5>\n <div class="pinkBg"> \n   <h6>Quote Number {{ReferenceNo}}<span class="refNum"></span></h6>\n   <p>Cheque of amount <span></span> to be deposited at the branch within <span>2 days.</span> Failure to do that or bounced cheque will lead to immediate policy lapse.</p>\n </div>\n <ion-row class="successBt">\n   <p>We have emailed your policy details to <span>{{email}}</span> and an SMS on <span>{{mobile}}</span>.</p>\n   <p>If you don’t get an email or SMS from us within 1 hour then, write to us at <span>fgcare@futuregenerali.in</span></p>\n </ion-row>\n <!-- \n <ion-row class="printBT">\n   <button ion-button (click)= "showPDF();">DOWNLOAD PDF</button>\n </ion-row>  -->\n \n </div>\n \n <div class="chequeSuccess" [class.hide]="paymentLinkSuccess">\n   <h5>\n     <img src="assets/imgs/cong_img.jpg">\n     <span>You have successfully generated your health insurance policy payment link.</span>\n   </h5>\n   <div class="pinkBg"> \n     <h6>Quote Number {{ReferenceNo}}<span class="refNum"></span></h6>\n     <!-- <p>Cheque of amount <span></span> to be deposited at the branch within <span>2 days.</span> Failure to do that or bounced cheque will lead to immediate policy lapse.</p> -->\n   </div>\n   <ion-row class="successBt">\n     <p>We have emailed your policy details payment link to <span>{{email}}</span> and an SMS on <span>{{mobile}}</span>.</p>\n     <p>If you don’t get an email or SMS from us within 1 hour then, write to us at <span>fgcare@futuregenerali.in</span></p>\n   </ion-row>\n   <!-- \n   <ion-row class="printBT">\n     <button ion-button (click)= "showPDF();">DOWNLOAD PDF</button>\n   </ion-row>  -->\n   \n   </div>\n \n \n <div class="onlineSuccess" [class.hide]="olSuccess">\n   <h5>\n     <img src="assets/imgs/cong_img.jpg">\n     <span>You have successfully generated health insurance policy.</span>\n   </h5>\n   <div class="transactionGrid"> \n     <p>Your payment of <span>₹{{transactionAmount}}</span> was successful</p>   \n   <ion-grid class="transGrid">\n     <ion-row>\n       <ion-col col-6 class="gLeft">Transaction ID</ion-col>\n       <ion-col col-6 class="gRight">{{transactionID}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Policy Number</ion-col>\n       <ion-col col-6 class="gRight">{{policyNumber}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Bank Transaction ID</ion-col>\n       <ion-col col-6 class="gRight">{{bankTransactionID}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Transaction Date</ion-col>\n       <ion-col col-6 class="gRight">{{transactionDate}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Transaction Amount</ion-col>\n       <ion-col col-6 class="gRight">₹{{transactionAmount}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Quote Number</ion-col>\n       <ion-col col-6 class="gRight">{{ReferenceNo}}</ion-col>\n     </ion-row>\n   </ion-grid>\n   </div>\n   <ion-row class="successBt">\n     <p>We have sent your policy details to <span>{{email}}</span> and a SMS to <span>{{mobile}}</span> confirming.</p>\n     <p>If you do not get an email or SMS from us within 1 hour, write to us at <span>fgcare@futuregenerali.in</span></p>\n   </ion-row>\n   <!-- <ion-row class="printBT">\n     <button ion-button (click)= "showPDF();">DOWNLOAD PDF</button>\n   </ion-row>  -->\n </div>\n \n \n \n <div class="onlineFail" [class.hide]="olFail">\n   <h5>\n     <img src="assets/imgs/payment_fail.jpg">\n   </h5>\n   <div class="transactionGrid"> \n     <p>Your payment for <span>₹3613</span> has failed.</p>   \n   <ion-grid class="transGrid">\n     <ion-row>\n       <ion-col col-6 class="gLeft">Transaction ID</ion-col>\n       <ion-col col-6 class="gRight">{{transactionID}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Transaction Date</ion-col>\n       <ion-col col-6 class="gRight">{{transactionDate}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Transaction Amount</ion-col>\n       <ion-col col-6 class="gRight">{{transactionAmount}}</ion-col>\n     </ion-row>\n     <ion-row>\n       <ion-col col-6 class="gLeft">Quote Number</ion-col>\n       <ion-col col-6 class="gRight">{{ReferenceNo}}</ion-col>\n     </ion-row>\n   </ion-grid>\n   </div>\n   <ion-row class="successBt">\n     <p>Your transaction has been failed. Kindly click on the payment method and try again</p>\n     <p>If money has been dedcuted from your bank account then, please contact your banks customer service.</p>\n   </ion-row>\n   <ion-row class="printBT">\n     <button ion-button>Retry</button>\n   </ion-row> \n   <ion-row>\n   <div class="saveBt"><button ion-button><ion-icon name="ios-mail-outline" class="email"></ion-icon>Email payment link</button></div>\n </ion-row> \n </div>\n \n <div class="trans" [class.hide]="serviceResponsePopup"></div>\n <div class="commonPopup otpPopup agentdPopup" [class.hide]="serviceResponsePopup">      \n     <div class="popupInner otpPopupContent " text-center>       \n         \n             <p> {{message}} </p>          \n          \n           <div class="btwrap">                \n             <div class="col11">\n               <button ion-button class="sendmailBt" (click)="serviceResponseOkButton();" style="padding: 15px 30px; margin:0 auto">OK</button>\n             </div>\n     </div>        \n </div>\n </div>\n \n \n \n \n </div>\n </ion-content>\n <ion-footer [class.hide]="showFooter">\n     <button ion-button class="commonBt" (click)="createPDF()"> Fetch PDF </button>\n </ion-footer>'/*ion-inline-end:"/Users/iosdeveloper/Documents/Ionic/PA/fgli-pa/src/pages/health/paymentpending/paymentpending.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], PaymentpendingPage);
    return PaymentpendingPage;
}());

//# sourceMappingURL=paymentpending.js.map

/***/ })

});
//# sourceMappingURL=0.js.map