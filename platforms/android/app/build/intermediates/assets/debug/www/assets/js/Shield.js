(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 360,
	height: 300,
	fps: 24,
	color: "#FFFFFF",
	webfonts: {},
	manifest: [
		{src:"assets/imgs/Shield.png", id:"Shield_1"}
	]
};



lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.Shield_1 = function() {
	this.initialize(img.Shield_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,205,256);


(lib.mc_Star = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-2.2,-4.8,0,-2.2,-4.8,62.4).s().p("AgVCSIlAFUIEEl8Ij6BQIDXiCInCh+IHWBEIhzjSICiCoIgHnFIBOG9IBzjcIg3DSID6iHIj1DDIHggKIngBOIDIB9Ijhg5IANFsg");
	this.shape.setTransform(0.7,-2.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.1,-52.4,113.5,99.7);


(lib.mc_Shield = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Shield_1();
	this.instance.setTransform(-102.5,-128);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-102.5,-128,205,256);


(lib.mc_Gradient = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","#FFFFFF","rgba(255,255,255,0)"],[0,0.341,0.525,1],8.2,-3.6,-8.7,-0.6).s().p("AhjgeIC6ghIANBeIi6Ahg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10,-6.5,20.1,13);


(lib.mc_Glow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],0,0,0,0,0,16.7).s().p("Ah0B0QgvgvAAhFQAAhDAvgxQAxgvBDAAQBEAAAwAvQAwAxAABDQAABFgwAvQgwAwhEAAQhDAAgxgwg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.5,-16.5,33,33);


(lib.mc_Light = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mc_Gradient();
	this.instance.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.9,-6.5,20,13);


// stage content:
(lib.Shield = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AtfHDIgBgFIAIxAQA1gOA0gNIAJgCIABAAIACAAQLviuNVDGIAADVQxBFwoDMJIgCACIgBADIgJAMIgQAZQhWiHgKing");
	mask.setTransform(68,118.9);

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.502)","rgba(255,255,255,0)"],[0,0.51,1],-268.4,-2.2,-93.4,-2.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape.setTransform(68.5,103.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.482)","rgba(255,255,255,0)"],[0,0.51,1],-250.4,-2.6,-75.4,-2.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_1.setTransform(68.5,103.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.463)","rgba(255,255,255,0)"],[0,0.51,1],-232.9,-3,-57.9,-3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_2.setTransform(68.5,103.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.443)","rgba(255,255,255,0)"],[0,0.51,1],-215.8,-3.4,-40.8,-3.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_3.setTransform(68.5,103.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.427)","rgba(255,255,255,0)"],[0,0.51,1],-199.2,-3.8,-24.2,-3.8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_4.setTransform(68.5,103.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.408)","rgba(255,255,255,0)"],[0,0.51,1],-183,-4.2,-8,-4.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_5.setTransform(68.5,103.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.392)","rgba(255,255,255,0)"],[0,0.51,1],-167.3,-4.5,7.7,-4.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_6.setTransform(68.5,103.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.373)","rgba(255,255,255,0)"],[0,0.51,1],-152,-4.9,23,-4.9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_7.setTransform(68.5,103.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.357)","rgba(255,255,255,0)"],[0,0.51,1],-137.2,-5.2,37.8,-5.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_8.setTransform(68.5,103.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.341)","rgba(255,255,255,0)"],[0,0.51,1],-122.9,-5.5,52.1,-5.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_9.setTransform(68.5,103.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.325)","rgba(255,255,255,0)"],[0,0.51,1],-109,-5.8,66,-5.8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_10.setTransform(68.5,103.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.314)","rgba(255,255,255,0)"],[0,0.51,1],-95.5,-6.1,79.5,-6.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_11.setTransform(68.5,103.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.298)","rgba(255,255,255,0)"],[0,0.51,1],-82.6,-6.4,92.4,-6.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_12.setTransform(68.5,103.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.282)","rgba(255,255,255,0)"],[0,0.51,1],-70.1,-6.7,104.9,-6.7).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_13.setTransform(68.5,103.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.271)","rgba(255,255,255,0)"],[0,0.51,1],-58,-7,117,-7).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_14.setTransform(68.5,103.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.259)","rgba(255,255,255,0)"],[0,0.51,1],-46.4,-7.3,128.6,-7.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_15.setTransform(68.5,103.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.247)","rgba(255,255,255,0)"],[0,0.51,1],-35.2,-7.5,139.8,-7.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_16.setTransform(68.5,103.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.235)","rgba(255,255,255,0)"],[0,0.51,1],-24.5,-7.7,150.5,-7.7).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_17.setTransform(68.5,103.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.224)","rgba(255,255,255,0)"],[0,0.51,1],-14.3,-8,160.7,-8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_18.setTransform(68.5,103.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.212)","rgba(255,255,255,0)"],[0,0.51,1],-4.5,-8.2,170.5,-8.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_19.setTransform(68.5,103.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.204)","rgba(255,255,255,0)"],[0,0.51,1],4.9,-8.4,179.9,-8.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_20.setTransform(68.5,103.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.192)","rgba(255,255,255,0)"],[0,0.51,1],13.7,-8.6,188.7,-8.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_21.setTransform(68.5,103.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.184)","rgba(255,255,255,0)"],[0,0.51,1],22.2,-8.8,197.2,-8.8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_22.setTransform(68.5,103.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.173)","rgba(255,255,255,0)"],[0,0.51,1],30.1,-9,205.1,-9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_23.setTransform(68.5,103.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.165)","rgba(255,255,255,0)"],[0,0.51,1],37.7,-9.2,212.7,-9.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_24.setTransform(68.5,103.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.157)","rgba(255,255,255,0)"],[0,0.51,1],44.7,-9.3,219.7,-9.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_25.setTransform(68.5,103.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.149)","rgba(255,255,255,0)"],[0,0.51,1],51.3,-9.5,226.3,-9.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_26.setTransform(68.5,103.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.145)","rgba(255,255,255,0)"],[0,0.51,1],57.5,-9.6,232.5,-9.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_27.setTransform(68.5,103.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.137)","rgba(255,255,255,0)"],[0,0.51,1],63.2,-9.7,238.2,-9.7).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_28.setTransform(68.5,103.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.133)","rgba(255,255,255,0)"],[0,0.51,1],68.4,-9.9,243.4,-9.9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_29.setTransform(68.5,103.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.125)","rgba(255,255,255,0)"],[0,0.51,1],73.2,-10,248.2,-10).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_30.setTransform(68.5,103.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.122)","rgba(255,255,255,0)"],[0,0.51,1],77.5,-10.1,252.5,-10.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_31.setTransform(68.5,103.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.118)","rgba(255,255,255,0)"],[0,0.51,1],81.4,-10.1,256.4,-10.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_32.setTransform(68.5,103.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.114)","rgba(255,255,255,0)"],[0,0.51,1],84.8,-10.2,259.8,-10.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_33.setTransform(68.5,103.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.11)","rgba(255,255,255,0)"],[0,0.51,1],87.8,-10.3,262.8,-10.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_34.setTransform(68.5,103.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.11)","rgba(255,255,255,0)"],[0,0.51,1],90.3,-10.3,265.3,-10.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_35.setTransform(68.5,103.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.106)","rgba(255,255,255,0)"],[0,0.51,1],92.3,-10.4,267.3,-10.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_36.setTransform(68.5,103.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.106)","rgba(255,255,255,0)"],[0,0.51,1],93.9,-10.4,268.9,-10.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_37.setTransform(68.5,103.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.102)","rgba(255,255,255,0)"],[0,0.51,1],95.1,-10.5,270.1,-10.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_38.setTransform(68.5,103.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.102)","rgba(255,255,255,0)"],[0,0.51,1],95.7,-10.5,270.7,-10.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_39.setTransform(68.5,103.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.102)","rgba(255,255,255,0)"],[0,0.51,1],96,-10.5,271,-10.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_40.setTransform(68.5,103.8);

	this.shape.mask = this.shape_1.mask = this.shape_2.mask = this.shape_3.mask = this.shape_4.mask = this.shape_5.mask = this.shape_6.mask = this.shape_7.mask = this.shape_8.mask = this.shape_9.mask = this.shape_10.mask = this.shape_11.mask = this.shape_12.mask = this.shape_13.mask = this.shape_14.mask = this.shape_15.mask = this.shape_16.mask = this.shape_17.mask = this.shape_18.mask = this.shape_19.mask = this.shape_20.mask = this.shape_21.mask = this.shape_22.mask = this.shape_23.mask = this.shape_24.mask = this.shape_25.mask = this.shape_26.mask = this.shape_27.mask = this.shape_28.mask = this.shape_29.mask = this.shape_30.mask = this.shape_31.mask = this.shape_32.mask = this.shape_33.mask = this.shape_34.mask = this.shape_35.mask = this.shape_36.mask = this.shape_37.mask = this.shape_38.mask = this.shape_39.mask = this.shape_40.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).wait(40));

	// Layer 5
	this.instance = new lib.mc_Star();
	this.instance.setTransform(168.5,43.3,0.052,0.052,30.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({_off:false},0).to({scaleX:0.52,scaleY:0.52,rotation:390.5},4,cjs.Ease.get(1)).to({scaleX:0.05,scaleY:0.05},3).to({_off:true},1).wait(56));

	// Layer 6
	this.instance_1 = new lib.mc_Glow();
	this.instance_1.setTransform(169,43.3,0.5,0.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(16).to({_off:false},0).to({alpha:1},3).wait(3).to({_off:true},1).wait(57));

	// Layer 8
	this.instance_2 = new lib.mc_Light();
	this.instance_2.setTransform(79.2,268.7,1,1,-23.1);
	this.instance_2.alpha = 0.25;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:1,scaleY:1,rotation:-74.9,guide:{path:[79.3,268.7,154.8,222.1,162.4,169.5]},alpha:1},9).to({scaleX:1,scaleY:1,rotation:-79.4,guide:{path:[162.4,169.5,163.2,164.1,163.2,158.7,163.2,102.9,163.2,47.2]}},9,cjs.Ease.get(1)).to({_off:true},1).wait(61));

	// Layer 10
	this.instance_3 = new lib.mc_Light();
	this.instance_3.setTransform(-26.5,45.9,1,1,-9.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({rotation:29.2,guide:{path:[-26.3,45.8,-16.7,43.2,-7,41,-6.6,40.9,-6.2,40.8,-6.1,40.8,-6,40.8,-5.9,40.8,-5.9,40.8,78.4,22.6,163,45.7]}},18,cjs.Ease.get(1)).to({_off:true},1).wait(61));

	// Layer 14
	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#D7D7D7").s().p("AOOKAIAAxZQswjZs9CwIgCAAIgBABIgJACQhQARhRAVIAARZIACAaIhghLIAAxsQCAghB/gYIAJgCIABAAIACgBQNrikNgDgIAARrIhgBRIACgfg");
	this.shape_41.setTransform(67.6,96.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_41).wait(80));

	// Shield
	this.instance_4 = new lib.mc_Shield();
	this.instance_4.setTransform(67.5,155);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(80));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(142.7,150.1,397.4,480);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;