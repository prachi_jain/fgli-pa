import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AddCommasPipe } from '../../pipes/add-commas/add-commas';
//import { setupUrlSerializer } from 'ionic-angular/umd/navigation/url-serializer';

/**
 * Generated class for the NumericStepperComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * 
 * Components.
 */


@Component({
  selector: 'numeric-stepper',
  templateUrl: 'numeric-stepper.html'
})
export class NumericStepperComponent {
  @Input('min') minValue
  @Input('max') maxValue
  // @Input('step') stepValue
  @Input('init') initValue
  @Input('maxlength') maxlength
  @Output() getCurrValue = new EventEmitter();
  @Output() isValueChanged = new EventEmitter();

  @ViewChild("coverInput") coverInputFocus;

  text: string;
  userValue: any = "";
  // maxValue: any;
  changeValue = false

  constructor() {
    console.log('Hello NumericStepperComponent Component');
    // this.text = 'Hello World';
    // this.text = 'Numeric Stepper Component';
    console.log("this.initValue " + this.initValue);
  }

  ngOnChanges(changes) {
    console.log(changes);
    // this.initValue = changes.initValue.currentValue;
    this.userValue = this.addCommas(this.initValue);
    // this.maxValue = changes.maxValue.currentValue;

    // if (changes.initValue.previousValue == undefined || String(changes.initValue.previousValue) == "") {
      
    //   this.maxValue = changes.initValue.currentValue;
    // }
    // console.log(this.maxValue );
  }



  currValue(ev) {
    const initalValue = this.userValue;

    this.userValue = this.addCommas(String(this.userValue).replace(/[^0-9]*/g, ''));
    console.log(this.userValue);
    this.isValueChanged.emit(this.removeCommas(this.userValue));
    // console.log("this.minValue: "+this.minValue);
    if (this.removeCommas(initalValue) !== this.removeCommas(this.userValue)) {
      ev.stopPropagation();
    }
    Number(this.removeCommas(this.userValue)) > Number(this.removeCommas(this.maxValue)) ? this.userValue = this.addCommas(this.maxValue) : this.userValue = this.addCommas(this.userValue);
    console.log("Emitting: "+this.removeCommas(this.userValue));
    this.getCurrValue.emit(this.removeCommas(this.userValue));
  }

  checkVal() {
    this.userValue == "" || this.userValue == null || this.userValue == undefined ? this.userValue = this.addCommas(this.minValue) : this.userValue = this.addCommas(this.userValue);
    Number(this.removeCommas(this.userValue)) > Number(this.removeCommas(this.maxValue)) ? this.userValue = this.addCommas(this.maxValue) : this.userValue = this.addCommas(this.userValue);
    Number(this.removeCommas(this.userValue)) < Number(this.removeCommas(this.minValue)) ? this.userValue = this.addCommas(this.minValue) : this.userValue = this.addCommas(this.userValue);
    console.log("Emitting: "+this.removeCommas(this.userValue));
    this.getCurrValue.emit(this.removeCommas(this.userValue));
  }

  addCommas(x) {
    x = Number(this.removeCommas(x));
    console.log("From addCommasFnctn:  "+x);
    if (x != undefined) {
        var isNegative = false;
        x = x.toString();
        if (x.indexOf("-") != -1) {
        x = x.slice(1)
        isNegative = true
        }
        var afterPoint = '';
        if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
        x = Math.floor(x);
        x = x.toString();
        var lastThree = x.substring(x.length - 3);
        var otherNumbers = x.substring(0, x.length - 3);
        if (otherNumbers != '')
        lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
        if (isNegative) {
        res = "-" + res
        }
        return res;
        } else {
        return null;
        }
    // return value;
  }

  removeCommas(x){
    x = String(x).replace(/,/g, "");
    return Number(x);
  }
  
  ngAfterViewInit() {
    this.userValue = this.addCommas(this.initValue);
    // console.log(this.initValue);
    console.log("Emitting: "+this.removeCommas(this.userValue));
    this.getCurrValue.emit(this.removeCommas(this.userValue));
    // this.coverInputFocus.setFocus();
    // console.log(this.minValue,this.maxValue,this.stepValue);
  }
}
