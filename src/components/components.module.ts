import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from "@angular/common";
import { NumericStepperComponent } from './numeric-stepper/numeric-stepper';
import { IonicPageModule } from 'ionic-angular';
@NgModule({
	declarations: [
		NumericStepperComponent
	],
	imports: [CommonModule, IonicPageModule.forChild(NumericStepperComponent)],
	exports: [
		NumericStepperComponent], schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }
