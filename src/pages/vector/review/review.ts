import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { LandingScreenPage } from '../../health/landing-screen/landing-screen';
import { LoginPage } from '../../health/login/login';
import { VectordeclarationquestionPage } from '../vectordeclarationquestion/vectordeclarationquestion';
import { VectorHealthDeclarationPage } from '../vector-health-declaration/vector-health-declaration';

/**
 * Generated class for the ReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({  
  selector: 'page-review',
  templateUrl: 'review.html',
}) 
export class ReviewPage {
  pet = 'Policy';
  pet1 = 'Self';
  hideMe= true;
  
  policyDetails:boolean;
  contactDetails:boolean;
  insuredDetails:boolean;
  memberArray;
  selectedCardArray;
  policyType;
  policyTerm;
  premium;
  installment;
  totalSI = 0;
  email;
  mobile;
  address;
  nameOfProposer;
  smokingText;
  displayPolicyDetails:any;
  memberlist = [];
  sumOfSI;
  resultData;
  titleProposer;
  panShow = true;
  ReferenceNo;
  hidebutton = true;
  ClientDetails: any = [];
  loading;
  isDisabledContact = true;
  clickSave = false;
  clickEdit = false;
  editBrd;
  isDisabledInsured = true;
  saveBtnHideShowContact = true;
  saveBtnHideShowInsured = true;
  saveandEmailHidShow = true;
  pID;
  thankyouPopup=true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  disabledClick = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public toast: ToastController,  public appService: AppService, private loadingCtrl : LoadingController) {
    this.memberArray = navParams.get("ProposerDetails");
    console.log("memberReview" + JSON.stringify(this.memberArray));  
    this.selectedCardArray = JSON.parse(sessionStorage.selectedPremiumDetails);
    console.log("Card:" + JSON.stringify(this.selectedCardArray));
      

    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      sessionStorage.isViewPopped = "";
    }
    
    for(let i=0; i< this.memberArray.length; i++){
      this.memberArray[i].isDisabled = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewPage');
    document.getElementById("splash").style.display='none';
    this.loadMembersData();
    this.ReferenceNo = sessionStorage.quoteno;


    if(sessionStorage.isViewPopped == true){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      sessionStorage.isViewPopped = undefined;
      }
  }

  loadMembersData(){
    this.PolicyDetails();

  }

  PolicyDetails(){
    if(this.clickEdit == false && this.clickSave == false){
      this.memberlist = [];
      this.hidebutton = true;
      this.totalSI = 0;
      this.policyDetails = false;
      this.contactDetails = false;
      this.insuredDetails = false;
      this.pet = "Policy";
      if(this.memberArray[0].PolicyType == "HTI"){
        this.policyType = "Individual";
        this.policyTerm = this.selectedCardArray.ptTitle;
        this.premium = Math.round(Number(this.selectedCardArray.discountAmount));
        this.installment = this.selectedCardArray.Installment;
        this.displayPolicyDetails = this.selectedCardArray.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member;
        console.log("MemberData:" + JSON.stringify(this.displayPolicyDetails));
        if(this.displayPolicyDetails.length > 0){
          for(let f=0; f<this.displayPolicyDetails.length; f++){
            this.totalSI += parseInt(this.displayPolicyDetails[f].SumInsured);
            //this.totalSI +=  this.sumOfSI;
          }
        }
        for(let i=0; i< this.memberArray.length; i++){
        
          this.memberlist.push({name: this.memberArray[i].title});
          if(this.memberArray[i].SumInsured == ""){
            this.memberlist.splice(i, 1);
          }
        }
      }else{
        this.displayPolicyDetails = this.selectedCardArray.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member;
        this.policyType = this.selectedCardArray.Result_vector.Root.Policy.Parameters.PolicyType;
        this.policyTerm = this.selectedCardArray.Result_vector.Root.Policy.Parameters.Duration;
        this.premium = Math.round(Number(this.selectedCardArray.Result_vector.Root.Policy.Parameters.TotalPremium));
        this.installment = this.selectedCardArray.Result_vector.Root.Policy.Parameters.Installments;
        if(this.displayPolicyDetails.length > 0){
          for(let f=0; f<this.displayPolicyDetails.length; f++){
            this.totalSI += parseInt(this.displayPolicyDetails[f].SumInsured);
            //this.totalSI +=  this.sumOfSI;
          }
        }
        this.totalSI = this.addCommas(this.totalSI);
        for(let i=0; i< this.memberArray.length; i++){         
          if(this.memberArray[i].SumInsured == ""){
            this.memberlist.splice(i, 1);
          }else{
            this.memberlist.push({name: this.memberArray[i].title, sumInsured: this.memberArray[i].SumInsured});
          }
        }
        console.log('Latest:' + JSON.stringify(this.memberlist));
      }
    }else{
      this.pet = "Contact";
      this.showToast("Please save your changes first");
    }   
     
  }

  ContactDetails(){
    if(this.clickEdit == false && this.clickSave == false){
      this.pet = "Contact";
      this.hidebutton = true;
      this.policyDetails = true;
      this.contactDetails = true;
      this.insuredDetails = false;
      this.disabledClick = true;
    }else{
      this.showToast("Please save your changes first");
    }
  }

  InsuredDetails(){
    if(this.disabledClick == true){
      this.disabledClick == true;
      // this.clickEdit = false;
      // this.clickSave = false;
      this.hidebutton = false;
      this.policyDetails = true;
      this.contactDetails = false;
      this.insuredDetails = true;
    }else{
      if(this.clickEdit == false && this.clickSave == false){  
        this.pet = "Insured";    
        this.hidebutton = false;
        this.policyDetails = true;
        this.contactDetails = false;
        this.insuredDetails = true;
        // for(let i=0;i<this.memberArray.length;i++){
        //   this.nameOfProposer = this.memberArray[i].name;
        //   if(this.memberArray[i].smoking == true){
        //     this.memberArray[i].smokingText = "Smoker";
        //   }else{
        //     this.memberArray[i].smokingText = "Non-Smoker";
        //   }
  
        //   if(this.memberArray[i].title == "Self"){
        //     this.memberArray[0].titleProposer = "Proposer";
        //   }else{
        //     this.memberArray[i].titleProposer = this.memberArray[i].title;
        //   }
  
  
        //     if(this.memberArray[i].title == "Spouse" && this.memberArray[0].Gender == "M"){
        //       this.memberArray[i].genderText = "Female";
        //     }else if(this.memberArray[i].title == "Spouse" && this.memberArray[0].Gender == "F"){
        //       this.memberArray[i].genderText = "Male";
        //     }else if(this.memberArray[i].title == "Daughter" || this.memberArray[i].title == "Daughter1" || this.memberArray[i].title == "Daughter2" || this.memberArray[i].title == "Daughter3" || this.memberArray[i].title == "Daughter4" || this.memberArray[i].title == "Mother") {
        //       this.memberArray[i].genderText = "Female";
        //     }else if(this.memberArray[i].title == "Son" || this.memberArray[i].title == "Son1" || this.memberArray[i].title == "Son2" || this.memberArray[i].title == "Son3" || this.memberArray[i].title == "Son4" || this.memberArray[i].title == "Father") {
        //       this.memberArray[i].genderText = "Male";
        //     }
        // }
      }else{    
        this.showToast("Please save your changes first");
      }
    }
  
  }

  addCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    console.log("test: " + parts);
    return parts.join(".");   
  }

  showToast(Message) {
    let pageToast = this.toast.create({
      message: Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 2500,
    });
    pageToast.present();
  }

  onTabChange(event){
    switch(event.value){
      case 'Policy':
        break;
      case 'Contact':
        break;
      case 'Insured':
        break;
    }
  }
  onEditClick(event, index, member){
    // for(let i=0; i< this.memberDetails.length; i++){
    //     this.memberDetails[i].isDisabled = index == i ? !this.memberDetails[i].isDisabled : true;
        
    // }
    this.clickEdit = !this.clickEdit;
    member.saveBtnHideShowInsured = !member.saveBtnHideShowInsured;
    this.isDisabledInsured = !this.isDisabledInsured;
    member.editBrd = !member.editBrd;
  }
  
  onEditContactDetails(){
    if(this.disabledClick == true){
      this.disabledClick = false;
    }else{
      this.disabledClick = true;
    }
    
    this.clickEdit = !this.clickEdit;
    this.isDisabledContact = !this.isDisabledContact;
    this.editBrd = !this.editBrd;
    this.saveBtnHideShowContact = !this.saveBtnHideShowContact;
  }
  
  saveContactData(){
    if(this.memberArray[0].email.trim() == ""){
      this.showToast("Please enter proposer email");
    }else if (!this.matchEmail(this.memberArray[0].email)) {
      this.showToast("Please enter valid email id");      
    }else if (this.memberArray[0].mobile.trim() == undefined || this.memberArray[0].mobile.trim() == null || this.memberArray[0].mobile.trim() == "") {      
            this.showToast("Please enter mobile no");                             
    }else if(this.memberArray[0].mobile.length < 10){
            this.showToast("Please enter 10 digit mobile number");            
    }else if(!this.isValidMobile(this.memberArray[0].mobile)){
      this.showToast("Please enter valid mobile no");
      //this.mobileFocus.setFocus();
    }else if(this.memberArray[0].address == ""){
      this.showToast("Please enter your address");
    }else{
      this.disabledClick = true;
      sessionStorage.mobile = this.memberArray[0].mobile;
      sessionStorage.email = this.memberArray[0].email;
      this.saveBtnHideShowContact = true;
      this.clickSave = false;
      this.clickEdit = false;
      this.isDisabledContact = !this.isDisabledContact;
      this.editBrd = !this.editBrd;
    }  
  }
  
  matchEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  
  isValidMobile(mobile){
    var regex = /^[6-9][0-9]{9}$/;
    return regex.test(mobile);
  }
  
  saveInsuredDetails(member){
  //   for(let i=0; i< this.memberDetails.length; i++){
  //     this.memberDetails[i].isDisabled = index == i ? !this.memberDetails[i].isDisabled : true;
  // }
  if(member.title == "Self"){ 
    if (member.pan == undefined || member.pan == null || member.pan == "") { 
        this.showToast("Please enter your pan");   
    } else if (!this.matchPan(member.pan)) {
        this.showToast("Please enter valid pan");   
    } else if(member.nominee == undefined || member.nominee == null || member.nominee == ""){
      this.showToast("Please enter nominee name");
    } else if(member.NomineeRelation == undefined || member.NomineeRelation == null || member.NomineeRelation == ""){
      this.showToast("Please enter nominee relation");
    }else{
      member.saveBtnHideShowInsured = !member.saveBtnHideShowInsured;
      member.editBrd = !member.editBrd;
      this.isDisabledInsured = !this.isDisabledInsured;
      this.clickSave = false;
      this.clickEdit = false;
    }
  }else{
    if(member.nominee == undefined || member.nominee == null || member.nominee == ""){
      this.showToast("Please enter nominee name");
    } else if(member.NomineeRelation == undefined || member.NomineeRelation == null || member.NomineeRelation == ""){
      this.showToast("Please enter nominee relation");
    }else{
      this.clickSave = false;
      this.clickEdit = false;
      member.saveBtnHideShowInsured = !member.saveBtnHideShowInsured;
      member.editBrd = !member.editBrd;
      this.isDisabledInsured = true;
    }
   }
  }

  proceedNextTab(){
    console.log("edit: " + this.clickEdit);
    console.log("edit: " + this.clickSave);
    console.log("PET" + this.pet);
    
    
    if(this.clickEdit == false && this.clickSave == false){
      if(this.pet == "Policy"){
        this.pet = "Contact";
        this.ContactDetails();
      }else  if(this.pet == "Contact"){
        this.pet = "Insured";
        this.InsuredDetails();
        this.saveandEmailHidShow = false;
        this.hidebutton = false;
      }
    }else{
      this.showToast("Please save your changes first");
    }   
  }


  proceedVerifyPage(){
    if(this.clickEdit == false && this.clickSave == false){
      //this.navCtrl.push(VectorHealthDeclarationPage,{"memberDetails":this.memberArray, "planCardDetails": this.selectedCardArray});
      var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
      this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
      
    }else{
      this.showToast("Please save your changes first");
    }   
  }

  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          // this.pID = Resp.Proceed_PolicyResult.Data.pID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.navCtrl.push(VectorHealthDeclarationPage,{"memberDetails":this.memberArray, "planCardDetails": this.selectedCardArray});
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.loading.dismiss();
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
    });
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": sessionStorage.QuotationID,
      "UID": this.selectedCardArray.UID,
      "stage":"4",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.selectedCardArray.Result_vector.Root.Policy.Parameters.PolicyType ,//"HTF",
        "Duration": this.selectedCardArray.Result_vector.Root.Policy.Parameters.Duration,
        "Installments": this.selectedCardArray.Result_vector.Root.Policy.Parameters.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails":this.selectedCardArray.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member.length == 1 ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberArray,
        "CardDetails": this.selectedCardArray
      }
    }
    console.log("BuyPageProceedData: " + JSON.stringify(sendRequest));
    return sendRequest;
  }
  
  getClientNode(){
    let age = this.memberArray[0].age;
    let clientSalution = "MR";
    if(this.memberArray[0].Gender == "F"){
      clientSalution = "MRS";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberArray[0].proposerName,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberArray[0].Gender,
      "MaritalStatus": this.memberArray[0].maritalCode,
      "Occupation": this.memberArray[0].occupationCode,
      "PANNo": this.memberArray[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberArray[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode":this.memberArray[0].pincode,
        "City": this.memberArray[0].city,
        "State": this.memberArray[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberArray[0].mobile,
        "EmailAddr": this.memberArray[0].email
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": "",
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberArray[0].mobile,
        "EmailAddr": this.memberArray[0].email
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  
  }

  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberArray[0].Gender;
    let numericAge;
    // if(this.memberArray[0].nomineeAge == undefined || this.memberArray[0].nomineeAge == ""){

    // }else{
      if(this.memberArray.length == 1){
        numericAge = this.getAge(this.memberArray[0].nomineeAge).toString();
      }else{
        numericAge = this.getAge(this.memberArray[1].nomineeAge).toString(); 
      }
      
    //}
    
    
    BeneDetails.push({
      "MemberId": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].MemberId ,
      "InsuredName": this.memberArray[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberArray[0].occupationCode,
      "CoverType": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].CoverType,
      "SumInsured": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].SumInsured,
      "DeductibleDiscount": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].DeductibleDiscount,
      "Relation": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].Relation,
      "NomineeName": this.memberArray[0].nominee,
      "NomineeRelation": this.memberArray[0].NomineeRelation,
      "AnualIncome": "",
      "Height": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].Height,
      "Weight": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].Weight,
      "NomineeAge": numericAge,
      "AppointeeName": this.memberArray[0].appointeeName,
      "AptRelWithominee": this.memberArray[0].appointeeRelation,
      "MedicalLoading": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      }
    })
    return BeneDetails;
  }
  
  getBeneficiaryDetails(cardDetails){
    //var content = document.querySelector('div .nomineePage').children;

    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberArray.length;j++){
        //if(i==j){
          if( this.memberArray[j].code== (cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation)){           
            let numericAge = this.getAge(this.memberArray[j].nomineeAge).toString();
        
        
            let mDob = cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
            let gendr = this.memberArray[j].Gender;
            BeneDetails.push({
              "MemberId": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].MemberId ,
              "InsuredName": this.memberArray[j].proposerName,
              "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
              "InsuredGender": gendr,
              "InsuredOccpn": this.memberArray[j].occupationCode,
              "CoverType": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].CoverType,
              "SumInsured": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].SumInsured,
              "DeductibleDiscount": "0",
              "Relation": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation,
              "NomineeName": this.memberArray[j].nominee,
              "RelationName":this.memberArray[i].title,
              "NomineeRelation":this.memberArray[j].NomineeRelation,
              "AnualIncome": "",
              "Height": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Height,
              "Weight": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Weight,
              "NomineeAge": numericAge,
              "AppointeeName": this.memberArray[j].appointeeName,
              "AptRelWithominee": this.memberArray[j].appointeeRelation,
              "MedicalLoading": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].MedicalLoading,
              "PreExstDisease": "N",
              "DiseaseMedicalHistoryList": {
                "DiseaseMedicalHistory": {
                  "PreExistingDiseaseCode": "",
                  "MedicalHistoryDetail": ""
                }
              }
            })
          }
        //}
 
  
      }
  
    }
    return BeneDetails;
  
  } 

  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365);
  }

  matchPan(pan){
    var regex = /[A-Za-z]{5}\d{4}[A-Za-z]{1}/;
    return regex.test(pan);
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }


  onSaveAndEmail(){
    // if(this.clickEdit == false && this.clickSave == false){
    //   var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    //   this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
    // }else{
    //   this.showToast("Please save your changes first");
    // }   
  }

  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;    
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
    });
  }
  
  callEProposalService(){   
    var eProposalData = {
          "QuotationID": sessionStorage.QuotationID,
          "UID": this.selectedCardArray.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }
  
  sendEProposerData(serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        this.loading.dismiss();
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup = false;
          //this.closeModalPop();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp.ReturnCode == "807"){
          //this.showToast(resp.ReturnMsg);
          sessionStorage.TokenId =resp.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(resp.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = resp.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          sessionStorage.TokenId = resp.UserToken.TokenId;
          //this.showToast(resp.ReturnMsg); 
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }        
       
      }, (err) => {
      console.log(err);
      });
  }
  
  OkButton(){
    this.thankyouPopup = true;
    this.navCtrl.push(LandingScreenPage);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }

  onBackClick(){
    sessionStorage.isViewPopped = "true";
    sessionStorage.ProposerDetails = JSON.stringify(this.memberArray);
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
    this.navCtrl.pop();
  }

  popupShow(){
   
    this.hideMe=false;
   }
   closePop(){
    this.hideMe=true;    
   }
  

}
