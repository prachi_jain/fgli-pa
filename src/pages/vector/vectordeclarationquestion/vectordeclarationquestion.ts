import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the VectordeclarationquestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage() 
@Component({
  selector: 'page-vectordeclarationquestion',
  templateUrl: 'vectordeclarationquestion.html',
})
export class VectordeclarationquestionPage {

    otpPopup = true; 
	trans = true; 
	thankyouPopup = true; 

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VectordeclarationquestionPage');

    document.getElementById("splash").style.display='none';
    
  }  

  // onBackClick(){
  //   sessionStorage.isViewPopped = "true";
  //   sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
  //   sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
  //   this.navCtrl.pop();
  // }

  mobileVerify(){
  	this.otpPopup = false;
  	this.trans = false;
  }
  closeOtpModal(){
  	this.otpPopup = true;
  	this.trans = true;
  }
  proceedToReviewPage(){
  	this.thankyouPopup = false;

  	this.otpPopup = true;
  	this.trans = true; 
  }

  OkButton(){
  	this.thankyouPopup = true;
  }

}
