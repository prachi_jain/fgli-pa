import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, LoadingController, } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import * as $ from 'jquery';
import { DatePicker } from '@ionic-native/date-picker';
import { Keyboard } from '@ionic-native/keyboard';
import { VectornomineedeclarationPage } from '../vectornomineedeclaration/vectornomineedeclaration';
import { LoginPage } from '../../health/login/login';
import { ReviewPage } from '../review/review';
//import { LoginPage } from '../login/login';

/**
 * Generated class for the VecproposerdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage() 
@Component({
  selector: 'page-vecproposerdetails',
  templateUrl: 'vecproposerdetails.html',
})
export class VecproposerdetailsPage {
 partdisab = true;
 permdisab = true;
 tempdisab = true;
 //isActive = true; 
  totalDis;	
  loading;
  appData;
  proposerName;
  proposerOccupation = '';
  canShowToast = true;
  customText = "male";
  address;
  pinCode;
  state;
  city;
  email;
  mobile;
  apiState = [];
  apiStateArray;
  checkedAddress;
  corspndncAddressStatus = false;
  corpAddress;
  corpondncPinCode;
  corpondncState;
  corpondncCity;
  pan;
  stateCode;
  nameOfProposer= '';
  memberTitle;
  memberOccupation='';
  selectedCardArray;
  occupationList =[];
  occupation;
  occupationOther;
  hideShowDiv = true;
  selfDiv = false;
  forQuickQuote = true;
  activeMember = "SELF";
  activeMemberIndex = 0;
  pID;
  emailPattern;
  ReferenceNo;
  pet;
  disableProceedBt = true;
  segmentactive = false;
  inactive = false;
  SelfDob;
  memberDOB;
  selfMaritalStatus;
  otherGender = false;
  salutationList = [];
  ClientDetails = [];
  pinData;
  thumbnail;
  ImageData;
  imageCaptured = true;
  imagePOp = true;
  saveandEmailHideShowProposer = true;
  saveandEmailHideShowOther = true;
  fromPage;
  testVar = false;
  currentNumber = 0;
  thankyouPopup=true;
  insuredPlanTitle;
  isActive = true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  memberArray;
  memberAge:any;
  childAgeStatus;
  marriedStatusArray = [{value: "Single", status: false},{value: "Married", status: true},{value: "Divorced", status: false},{value: "Widow / Widower", status: false}];
  marriedStatus;
  proposerItem;
  isPinValid:boolean = true;
  OtherNomineeRelation;

  //  memberArray = [
  //   {"hideAptLabel": true,"hideAptInput": true,"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","maritalCode":"M","age":"10-04-1960","ageText":"30 years","insuredCode":"si002","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"93.98","HeightText":"3' 1\"","Weight":"54","ProposerDetailsDivShow":true,"medicalDeclaration":false,"showMedicalQuestions":true,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"occupation":"ENGR","proposerName":"Dgxydy","Gender":"M","genderText":"Male","address":"Dhydyfyf","pincode":"868686","state":"Dhfhfjfjffjfjfjfj","city":"Djcjck","proposerEmail":"dyduff","mobile":"8686856868","pan":"DYDYXGXGDY","occupationCode":"ENG"},
  //   {"hideAptInput": true,"hideAptLabel": true,"code":"SPOU","title":"Spouse","smoking":false,"smokingText":"","popupType":"1","showMarried":false,"maritalStatus":"","maritalCode":"M","age":"30-04-1971","ageText":"27 years","insuredCode":"si002","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"93.98","HeightText":"3' 1\"","Weight":"45","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true,"proposerName":"TestSpouse","occupation":"Maintenance Engineer","occupationCode":"ENG"},
  //   {"hideAptInput": true,"hideAptLabel": true,"code":"SON","title":"Son","smoking":false,"smokingText":"","popupType":"1","showMarried":false,"maritalStatus":"","maritalCode":"S","age":"09-07-1994","ageText":"27 years","insuredCode":"si002","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"93.98","HeightText":"3' 1\"","Weight":"45","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true,"proposerName":"TestSon","occupation":"Student","occupationCode":"ENG"}];


    regexEmail(email){
      let emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
      return emailPattern.match(this.email);
    }
    // = [{"showMedicalQuestions":true,"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","age":"1993-03-02","ageText":"25 years","insuredCode":"si004","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P002","insuredPlanTypeText":"SUPERIOR","insuredAmtInDigit":"","Height":"96.520","HeightText":"3' 2\"","Weight":"5","ProposerDetailsDivShow":true,"medicalDeclaration":false,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"nameOfProposer":"TEST","Gender":"M","genderText":"Male"},{"showMedicalQuestions":true,"nameOfProposer":"test3","code":"SPOU","title":"Spouse","smoking":true,"smokingText":"Smoking","popupType":"1","showMarried":false,"maritalStatus":"","age":"2001-03-02","ageText":"17 years","insuredCode":"si004","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P002","insuredPlanTypeText":"SUPERIOR","insuredAmtInDigit":"","Height":"96.520","HeightText":"3' 2\"","Weight":"3","ProposerDetailsDivShow":false,"medicalDeclaration":false,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true}];
  
    today = new Date();
    maxDate = (this.today.getFullYear()+"-"+this.numberLenght(this.today.getMonth())+"-"+this.numberLenght(this.today.getDate())).toString();
    
    minDate = '1900-01-01';
   
    numberLenght(Num){if(Num>=10){return Num}else{return"0"+Num}}
    numberLength(Num){if(Num>=10){return Num}else{return"0"+Num}}

  constructor(public navCtrl: NavController, private base64: Base64, public camera: Camera,
    public navParams: NavParams, private keyboard: Keyboard, private cdr: ChangeDetectorRef, private datePicker: DatePicker, public toast: ToastController, public appService: AppService, private loadingCtrl : LoadingController) {

      this.appData = JSON.parse(localStorage.AppData);      
      this.occupationList = this.appData.Masters.Occupation;
      this.fromPage = this.navParams.get("fromDashboardListing");
      if(this.fromPage == "true"){
        this.memberArray = navParams.get("buyPageVectorMemberDetailsResult");
        this.selectedCardArray = navParams.get("buyPageCardDetailsResult");
        console.log("Card:" + JSON.stringify(this.selectedCardArray));
        console.log("member: " + JSON.stringify(this.memberArray));
      }else{
        this.memberArray = navParams.get("buyPageMemberDetailsResult");
        this.selectedCardArray = JSON.parse(sessionStorage.selectedPremiumDetails);
        console.log("Card:" + JSON.stringify(this.selectedCardArray));
        console.log("member: " + JSON.stringify(this.memberArray));
      }
     
      
      //this.insuredPlanTitle = this.memberArray[0].insuredPlanTypeText;
       
      
      //console.log("fromPage: " + this.fromPage);
  }  

  ionViewDidLoad() {
    console.log('ionViewDidLoad VecproposerdetailsPage');
        setTimeout(function() {         
          document.getElementById("splash").style.display='none';                 
      }, 7000); 

      
    

     
      // for(let i=0; i<this.memberArray.length; i++){
      //   if(this.memberArray[i].title == "Spouse"){
      //     this.marriedStatusArray =  [{value: "Married", status: true}];
      //     break;
      //   }else{
      //     this.marriedStatusArray =  [{value: "Single", status: false},{value: "Divorced", status: false},{value: "Widow / Widower", status: false}];
      //   }
      // }

      
      $(".gSec").click(function(){
        //alert($(this).attr('rel'))
        this.customText = $(this).attr('rel'); 
        if(this.customText == "male"){
          //this.customText = $(".male").attr('rel');
          this.customText = "male";
          this.memberArray[0].gender = "M"
        }else{
          //this.customText = $(".female").attr('rel');           
          this.customText = "female";
          this.memberArray[0].gender = "M"
        }   
        this.isActive = !this.isActive;
        console.log("gender" + this.customText);
               
     });

      if(this.fromPage == "true"){
        this.proposerName = this.memberArray[0].proposerName;
        this.email = this.memberArray[0].email;
        this.mobile = this.memberArray[0].mobile;
        //this.selfMaritalStatus = this.memberArray[0].showMarried;
        this.marriedStatus = this.memberArray[0].showMarried;
        this.memberAge = this.memberArray[0].age;
        this.address = this.memberArray[0].address;
        this.pinCode = this.memberArray[0].pincode;
        this.state = this.memberArray[0].state;
        this.city = this.memberArray[0].city;
        this.pan =  this.memberArray[0].pan;
        this.proposerOccupation = this.memberArray[0].occupationCode;
        sessionStorage.isViewPopped == "";
      }else{
        this.proposerName = sessionStorage.propName;
        this.email = sessionStorage.email;
        this.mobile = sessionStorage.mobile;
        //this.SelfDob = this.memberArray[0].age;
      }        
      //this.ReferenceNo = this.selectedCardArray.ReferenceNo;
      //this.loadState();
      this.pet = "Proposer";
      this.thumbnail = "assets/imgs/proposer_profile_pic.svg";  
      this.forQuickQuote = this.memberArray.length == 1 ? false : true;
      this.saveandEmailHideShowProposer = this.memberArray.length == 1 ? false : true;  
  
      
      console.log("back State: " + sessionStorage.isViewPopped);

      
      
      if(sessionStorage.isViewPopped == "true"){
        this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
        console.log("MemberA:" + JSON.stringify(this.memberArray));     
        this.memberArray[0].proposerName =  sessionStorage.propName;
        this.memberArray[0].email = sessionStorage.email;
        this.memberArray[0].mobile = sessionStorage.mobile;
        if(sessionStorage.gender == "M"){
          this.isActive = true;          
        }
        else if(sessionStorage.gender == "F"){
          this.isActive = false;    
        }
        this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);          
        this.selfDiv = false;
        this.pet = "Proposer";
        this.hideShowDiv = true;
        this.setStoredData(0);
        sessionStorage.isViewPopped = ""; 
      }else{
       
        // for(let i=0; i< this.memberArray.length; i++){
        //   if(this.memberArray[i].title == "Self"){
        //     this.memberArray[i].title = "Proposer";
        //     this.forQuickQuote = true;
        //     //this.memberArray[i].code = "SELF";
        //     break;
        //   }else{
        //     //this.memberArray
        //     this.memberArray.splice(0, 0, this.proposerItem);
        //     this.memberArray[i].title = "Proposer";
        //     this.memberArray[i].code = "SELF";
        //     this.forQuickQuote = true;
        //     //this.selfDiv = false;
        //     break;
        //   }
        // }
        
      }

      
  }

  

  ionViewDidEnter(){
    console.log("back State: " + sessionStorage.isViewPopped);
    this.thumbnail = "assets/imgs/proposer_profile_pic.svg";
    this.ReferenceNo = sessionStorage.quoteno;
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
    
      console.log("BACK: " + JSON.stringify(this.memberArray));
      
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);     
     
      this.selfDiv = false; 
    
      
      if(this.memberArray.length == 1){
        this.forQuickQuote = false;
        this.pet = "Proposer";
      }else{
        this.forQuickQuote = true;
        this.pet = "Proposer";
      }
      
      this.hideShowDiv = true;
      this.setStoredData(0);

      sessionStorage.isViewPopped = ""; 

      this.activeMemberIndex = 0;

      // if(this.memberArray[0].Gender == "M"){
      //   this.isActive = !this.isActive;
      // }else{
      //   this.isActive = !this.isActive;
      // }

      // if(i == 0 || i == 1){

      // }
    }
    
    $(".gSec").click(function(){
      //alert($(this).attr('rel'))
      this.customText = $(this).attr('rel'); 
      if(this.customText == "male"){
        //this.customText = $(".male").attr('rel');
        this.customText = "male";
      }else{
        //this.customText = $(".female").attr('rel');           
        this.customText = "female";
      }   
      this.isActive = !this.isActive;
      console.log("gender" + this.customText);
             
   });


 
  }


  setStoredData(i){
    //for(let i=0; i< this.memberArray.length; i++){  
  
      if(i == 0){
        this.proposerName = this.memberArray[i].proposerName;
        this.email = this.memberArray[i].email;
        this.mobile = this.memberArray[i].mobile;
        this.memberAge = this.memberArray[i].age;
        this.marriedStatus = this.memberArray[i].maritalStatus;
       // this.isActive = !this.isActive;
        if(this.memberArray[i].maritalCode == "M"){
          this.selfMaritalStatus = true;
        }    
        if(sessionStorage.gender == "M"){

          this.isActive = true;
          
        }else if(sessionStorage.gender == "F"){
          this.isActive = false;
  
  
        }   
        this.address = this.memberArray[i].address;
        this.pinCode = this.memberArray[i].pincode;
        this.state = this.memberArray[i].state;
        this.city = this.memberArray[i].city;
        this.pan =  this.memberArray[i].pan;
        this.proposerOccupation = this.memberArray[i].occupationCode;
        this.disableProceedBt = false;

      
      }else{
        console.log("propName: " + JSON.stringify(this.memberArray[i].proposerName));
        console.log(this.memberArray[i].proposerName != undefined);
        
        
        this.nameOfProposer = this.memberArray[i].proposerName != undefined ? this.memberArray[i].proposerName : "";
        this.memberDOB = this.memberArray[i].age != undefined ? this.memberArray[i].age : "";
        this.memberOccupation = this.memberArray[i].occupationCode != undefined ? this.memberArray[i].occupationCode : "";
        this.disableProceedBt = false;
      }
    //}
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }


  changePinCode(){   
       
    this.pinData = {"pin": this.pinCode} 
    var sendData = this.appService.encryptData(JSON.stringify(this.pinData),sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.stateCityService("FGHealth.svc/GetPinDetails", {'request': sendData});
  }

    /* call state city service */
    stateCityService(URL,serviceData){
      this.presentLoadingDefault();
      //this.appService.presentServiceLoading();
      let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService(URL,serviceData,headerString)
        .subscribe(data =>{
         //this.loading.dismiss();       
          console.log();
          if(data && data.GetPinDetailsResult.ReturnCode == "0"){
            this.isPinValid = true;
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            //this.showToast(data.GetPinDetailsResult.ReturnMsg); 
            this.state = data.GetPinDetailsResult.Data.state;
            this.city = data.GetPinDetailsResult.Data.city;
            this.loading.dismiss();
          }else if(data.GetPinDetailsResult.ReturnCode == "807"){
            this.isPinValid = false;
            this.state = "";
            this.city = "";
            //this.showToast(data.GetPinDetailsResult.ReturnMsg);
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            //this.navCtrl.push(LoginPage);
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 807;

          }else if(data.GetPinDetailsResult.ReturnCode == "500"){
            this.isPinValid = false;
            this.state = "";
            this.city = "";
            //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 500;
            //this.navCtrl.push(LoginPage);
          } else{
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            this.isPinValid = false;
            this.state = "";
            this.city = "";
            //this.showToast(data.GetPinDetailsResult.ReturnMsg); 
            this.loading.dismiss();
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 400;
          }
        
        });
    }

    //continue Click function
    continueButtonClick(){

      console.log("active: " + this.activeMemberIndex);
      this.emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
      let panPattern = "[A-Za-z]{5}\d{4}[A-Za-z]{1}";
      console.log("1: " + this.activeMemberIndex + "2: " + (this.memberArray.length - 1));
      if(this.activeMemberIndex == (this.memberArray.length-1)){
        this.forQuickQuote = false;
        this.saveandEmailHideShowProposer = true;
        this.saveandEmailHideShowOther = false;
      }
       if(this.activeMemberIndex == 0){
          let proceed = true;
          if (this.proposerName.trim() == undefined || this.proposerName.trim() == null || this.proposerName.trim() == "") {     
                this.showToast("Please enter proposer name to proceed");
                proceed = false;
                this.segmentactive = true;
          } else if(this.memberAge.trim() == "" || this.memberAge.trim() == undefined){
            this.showToast("Please select member's age.");
          } else if(this.invalidDob(this.memberAge)){
            this.showToast("Please enter valid Date of Birth.");
          } else if(this.marriedStatus == "" && this.pet.code == "SELF"){
            this.showToast("Please select marital status.");
          } else if (this.address.trim() == undefined || this.address.trim() == null || this.address.trim() == "") {     
                this.showToast("Please enter your address to proceed");                   
                proceed = false;  
                this.segmentactive = true; 
          } else  if (this.pinCode.trim() == undefined || this.pinCode.trim() == null || this.pinCode.trim() == "") {     
                this.showToast("Please enter your pincode to proceed");                   
                proceed = false;   
                this.segmentactive = true;
          } else if(this.pinCode.length<6){
                this.showToast("Please enter 6 digit pincode");
                proceed = false;  
                this.segmentactive = true; 
          } else if (this.state.trim() == undefined || this.state.trim() == null || this.state.trim() == "") {     
                this.showToast("Please enter your state to proceed");                    
                proceed = false;   
                this.segmentactive = true;
          } else if (this.city.trim() == undefined || this.city.trim() == null || this.city.trim() == "") {     
                this.showToast("Please enter your city to proceed");                    
                proceed = false;  
                this.segmentactive = true; 
          } else if (this.email.trim() == undefined || this.email.trim() == null || this.email.trim() == "") { 
                this.showToast("Please enter your email Id to proceed");
                proceed = false;   
                this.segmentactive = true;
          } else if (!this.matchEmail(this.email)) {
            this.showToast("Please enter valid email id");
            proceed = false; 
            this.segmentactive = true;
            }
           else if (this.mobile.trim() == undefined || this.mobile.trim() == null || this.mobile.trim() == "") {      
                  this.showToast("Please enter mobile number to proceed");                   
                  proceed = false;   
                  this.segmentactive = true;
          } else if(this.mobile.length < 10){
                  this.showToast("Please enter 10 digit mobile number");
                  proceed = false;   
                  this.segmentactive = true;
          } else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
              this.showToast("Please select occupation to proceed");
              proceed = false;
              this.segmentactive = true;
          } 
          /*else if ( this.pan.trim() == undefined || this.pan.trim() == null || this.pan.trim() == "") { 
              this.showToast("Please enter your PAN to proceed");
              proceed = false;
              this.segmentactive = true;
          } else if (!this.matchPan(this.pan)) {
              this.showToast("Please enter valid pan");
              proceed = false;
              this.segmentactive = true;
          } else if(this.imageCaptured == true){
              this.showToast("Please click image of Proposer");
              proceed = false;
          }*/
         else if(proceed){ 
  
            //this.segmentactive = false;
  
            this.disableProceedBt = true;          
            this.getMemberContactDetails();
            this.activeMemberIndex++;
            this.pet = this.memberArray[this.activeMemberIndex].title;
            this.memberTitle = this.memberArray[this.activeMemberIndex].title;
            this.memberAge = this.memberArray[this.activeMemberIndex].age;
            this.setStoredData(this.activeMemberIndex);
            this.hideShowDiv = false;
            this.selfDiv = true;
          
            if(this.memberArray.length == 2){
              this.forQuickQuote = false;
              this.saveandEmailHideShowProposer = true;
              this.saveandEmailHideShowOther = false;
            }
          }else{
  
          }  
          
       } else{
        for(let i=1; i<this.memberArray.length;i++){
          //if(this.memberArray[i].title == "")
          
          if(this.activeMemberIndex == i){
           
            //this.nameOfProposer = "";
            //this.memberOccupation = "";
            if(this.nameOfProposer == ""){
             this.showToast("Please enter "+this.memberArray[i].title+" name");
             this.segmentactive = true;
            }else if(this.memberAge.trim() == ""){
              this.showToast("Please select member's age.");
            }else if(this.invalidDob(this.memberAge)){
              this.showToast("Please enter valid Date of Birth.");
            } else if(this.marriedStatus == "" && this.pet.code == "SELF"){
              this.showToast("Please select marital status.");
            }else if(this.memberOccupation == ''){
              this.showToast("Please select "+this.memberArray[i].title+" occupation");
            }else if((this.memberArray[this.activeMemberIndex].title == "Son" || this.memberArray[this.activeMemberIndex].title == "Son2" || this.memberArray[this.activeMemberIndex].title == "Son3" || this.memberArray[this.activeMemberIndex].title == "Daughter" || this.memberArray[this.activeMemberIndex].title == "Daughter2" || this.memberArray[this.activeMemberIndex].title == "Daughter3") && (this.memberOccupation == "BUSM" || this.memberOccupation == "HSWF" || this.memberOccupation == "RETR" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS")){
              this.showToast("Only dependent childs are covered");
            }else if((this.memberArray[this.activeMemberIndex].title == "Father" || this.memberArray[this.activeMemberIndex].title == "Mother") && (this.memberOccupation == "BUSM" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS" || this.memberOccupation == "STDN" || this.memberOccupation == "UNEM")){
              this.showToast("Only dependent parents are covered");
            }else{           
              this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
              this.memberArray[this.activeMemberIndex].age = this.memberAge;
              this.memberArray[this.activeMemberIndex].ageText = this.getAge(this.memberAge);
              this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
              this.memberArray[this.activeMemberIndex].maritalStatus = this.marriedStatus;
  
              if(this.customText == "female" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
                this.memberArray[this.activeMemberIndex].Gender = 'M';
                this.memberArray[this.activeMemberIndex].genderText = 'Male';
              }else if(this.customText == "male" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
                this.memberArray[this.activeMemberIndex].Gender = 'F';
                this.memberArray[this.activeMemberIndex].genderText = 'Female';
              }else if(this.memberArray[this.activeMemberIndex].title == "Son"){
                this.memberArray[this.activeMemberIndex].Gender = 'M';
                this.memberArray[this.activeMemberIndex].genderText = 'Male';
              }else if(this.memberArray[this.activeMemberIndex].title == "Son2"){
                this.memberArray[this.activeMemberIndex].Gender = 'M';
                this.memberArray[this.activeMemberIndex].genderText = 'Male';
              }else if(this.memberArray[this.activeMemberIndex].title == "Son3"){
                this.memberArray[this.activeMemberIndex].Gender = 'M';
                this.memberArray[this.activeMemberIndex].genderText = 'Male';
              }else if(this.memberArray[this.activeMemberIndex].title == "Daughter"){
                this.memberArray[this.activeMemberIndex].Gender = 'F';
                this.memberArray[this.activeMemberIndex].genderText = 'Female';
              }else if(this.memberArray[this.activeMemberIndex].title == "Daughter1"){
                this.memberArray[this.activeMemberIndex].Gender = 'F';
                this.memberArray[this.activeMemberIndex].genderText = 'Female';
              }else if(this.memberArray[this.activeMemberIndex].title == "Daughter2"){
                this.memberArray[this.activeMemberIndex].Gender = 'F';
                this.memberArray[this.activeMemberIndex].genderText = 'Female';
              }else if(this.memberArray[this.activeMemberIndex].title == "Father"){
                this.memberArray[this.activeMemberIndex].Gender = 'M';
                this.memberArray[this.activeMemberIndex].genderText = 'Male';
              }else if(this.memberArray[this.activeMemberIndex].title == "Mother"){
                this.memberArray[this.activeMemberIndex].Gender = 'F';
                this.memberArray[this.activeMemberIndex].genderText = 'Female';
              }else if(this.memberArray[this.activeMemberIndex].title == "Grand Mother"){
                this.memberArray[this.activeMemberIndex].Gender = 'F';
                this.memberArray[this.activeMemberIndex].genderText = 'Female';
              }else if(this.memberArray[this.activeMemberIndex].title == "Grand Father"){
                this.memberArray[this.activeMemberIndex].Gender = 'M';
                this.memberArray[this.activeMemberIndex].genderText = 'Male';
              }              
              // if(this.otherGender == false){
              //   this.memberArray[this.activeMemberIndex].Gender = 'M';
              //   this.memberArray[this.activeMemberIndex].genderText = 'Male';
              // }else{
              //   this.memberArray[this.activeMemberIndex].Gender = 'F';
              //   this.memberArray[this.activeMemberIndex].genderText = 'Female';
              // }
  
              for(let j=0; j<this.occupationList.length; j++){
                if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
                this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
                break;
                }
                }
     
              i++;
              if( i < this.memberArray.length){
                this.disableProceedBt = true;
                this.nameOfProposer = "";
                this.memberOccupation ="";
                this.activeMemberIndex++;  
                this.pet = this.memberArray[this.activeMemberIndex].title;
                console.log("activeMembers: " + this.pet);
                this.setStoredData(this.activeMemberIndex);
  
                this.memberTitle = this.memberArray[this.activeMemberIndex].title; 
                this.memberAge = this.memberArray[this.activeMemberIndex].age;
                //this.memberOccupation ="";
  
                if(this.activeMemberIndex == (this.memberArray.length - 1)){
                  this.forQuickQuote = false;
                  //this.disableProceedBt = false;
                  this.saveandEmailHideShowProposer = true;
                  this.saveandEmailHideShowOther = false;
                }
              
              }/* else{
                this.forQuickQuote = false;
              }   */      
                     
            }
            break;
          }      
        }
       }  
      
    }

    invalidDob(userDoB){
      let today = new Date();
      let uDob = userDoB.split("-");
      let uDte = ( this.numberLenght(Number(uDob[1]))+"-"+this.numberLenght( Number(uDob[0]))+"-"+(uDob[2]) ).toString();
      let usrDob = new Date(uDte);    
      if(usrDob.toString() == "Invalid Date"){
        return true;
      }else if(usrDob > today){
        return true;
      }
      return false;
    }

    getMemberContactDetails(){
      var contactDetails = {};
      let a = this.proposerName;
      let first = a.split(" ");  
      console.log(JSON.stringify(first));
            
      if(first.length == 3){
        var firstSplit = first[0].replace(/[^a-z]/gi,'');
        var secondSplit = first[2].replace(/[^a-z]/gi,'');
        console.log("Regex:" + firstSplit + secondSplit);
        
        this.memberArray[0].proposerName = firstSplit + " " +  secondSplit;
      }else if(first.length == 2){
        var firstSplit = first[0].replace(/[^a-z]/gi,'');
        var secondSplit = first[1].replace(/[^a-z]/gi,'');
      console.log("Regex:" + firstSplit + secondSplit);    
      this.memberArray[0].proposerName = firstSplit + " " +  secondSplit;
      }
      
  
  
      if(this.customText == "male"){
        this.memberArray[0].Gender = 'M';
        this.memberArray[0].genderText = 'Male';
      }else{
        this.memberArray[0].Gender = 'F';
        this.memberArray[0].genderText = 'Female';
      }
  
  
      // if(this.selfMaritalStatus){
      //   this.memberArray[0].maritalStatus = "Married";
      //   this.memberArray[0].maritalCode = "M"
      //   this.hidePopupSmokerDiv = true;
      // }
      // this.memberArray[0].showMarried = this.selfMaritalStatus;
      // this.memberArray[0].maritalCode = this.address;
      this.memberArray[0].proposerName = this.proposerName;
      this.memberArray[0].age = this.memberAge;
      this.memberArray[0].ageText = this.getAge(this.memberAge);
      console.log("status:" + this.marriedStatus);
      
      if(this.marriedStatus == "Married"){
        this.memberArray[0].maritalStatus = "Married";
        this.memberArray[0].maritalCode = "M"   
      }else{
        this.memberArray[0].maritalStatus = "Single"; 
        this.memberArray[0].maritalCode = "S"       
      }    
      this.memberArray[0].address = this.address;
      this.memberArray[0].pincode = this.pinCode;
      this.memberArray[0].state = this.state;
      this.memberArray[0].city = this.city;
      this.memberArray[0].email = this.email;
      this.memberArray[0].mobile = this.mobile;
      // this.memberArray[0].corpAddress = this.corpAddress;
      // this.memberArray[0].corpPinCode = this.corpondncPinCode;
      // this.memberArray[0].corpState = this.corpondncState;
      // this.memberArray[0].corpCity = this.corpondncCity;
      this.memberArray[0].pan = (this.pan).toUpperCase();
      this.memberArray[0].occupationCode = this.proposerOccupation;
        for(let j=0; j<this.occupationList.length; j++){
          if(this.memberArray[0].occupationCode == this.occupationList[j].OccCode){
            this.memberArray[0].occupation = this.occupationList[j].OccTitle;
            break;
          }
        }
  
      console.log("Updated :  "+ JSON.stringify(this.memberArray));
    }

//ProceedButtonClick function

proceedNomineeDetailsPage(){
  //alert(this. );
  // var a = this.UI_Validate();
  // console.log("State: " + a);
  // if(!this.UI_Validate){

  // }

  if(this.memberArray.length == 1){    
    // if(this.proposerName == ""){
    //   this.showToast("Enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
    //  }else if(this.memberOccupation == ''){
    //    this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
    //  }else{           
    //    this.memberArray[this.activeMemberIndex].proposerName = this.proposerName;
    //    this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;

    //    for(let j=0; j<this.occupationList.length; j++){
    //      if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
    //      this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
    //      break;
    //      }
    //      }
    //    this.getMemberContactDetails();   
    //    this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
    //  }

    if (this.proposerName.trim() == undefined || this.proposerName.trim() == null || this.proposerName.trim() == "") {     
      this.showToast("Please enter proposer name to proceed");              
    } else if(this.memberAge.trim() == ""){
      this.showToast("Please select member's age.");
    } else if(this.invalidDob(this.memberAge)){
      this.showToast("Please enter valid Date of Birth.");
    } else if(this.marriedStatus == "" && this.pet.code == "SELF"){
      this.showToast("Please select marital status.");
    } else if (this.address.trim() == undefined || this.address.trim() == null || this.address.trim() == "") {     
          this.showToast("Please enter your address to proceed");                             
    } else  if (this.pinCode.trim() == undefined || this.pinCode.trim() == null || this.pinCode.trim() == "") {     
          this.showToast("Please enter your pincode to proceed");                         
    } else if(this.pinCode.length<6){
          this.showToast("Please enter 6 digit pincode");          
    } else if (this.state.trim() == undefined || this.state.trim() == null || this.state.trim() == "") {     
          this.showToast("Please enter your state to proceed");                            
    } else if (this.city.trim() == undefined || this.city.trim() == null || this.city.trim() == "") {     
          this.showToast("Please enter your city to proceed");                             
    } else if (this.email.trim() == undefined || this.email.trim() == null || this.email.trim() == "") { 
          this.showToast("Please enter your email Id to proceed");           
    } else if (!this.matchEmail(this.email)) {
      this.showToast("Please enter valid email id");   
      }
    else if (this.mobile.trim() == undefined || this.mobile.trim() == null || this.mobile.trim() == "") {      
            this.showToast("Please enter mobile number to proceed");                              
    } else if(this.mobile.length < 10){
            this.showToast("Please enter 10 digit mobile number");            
    }else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
        this.showToast("Please select occupation to proceed");     
    } 
    /*else if ( this.pan.trim() == undefined || this.pan.trim() == null || this.pan.trim() == "") { 
        this.showToast("Please enter your PAN to proceed");      
    } else if (!this.matchPan(this.pan)) {
        this.showToast("Please enter valid pan");     
    } else if(this.imageCaptured == true){
      this.showToast("Please click image of Proposer");   
    }*/else{
      this.disableProceedBt = true;          
      this.getMemberContactDetails();      
     
      this.encryptIncompleteData();
      console.log("member:" + JSON.stringify(this.memberArray));
      
      //this.navCtrl.push(VectornomineedeclarationPage, {"ProposerDetails" : this.memberArray});

    }
  }else{
    if(this.nameOfProposer.trim() == ""){
      this.showToast("Please enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
    }else if(this.memberAge.trim() == ""){
      this.showToast("Please select member's age.");
    }else if(this.invalidDob(this.memberAge)){
      this.showToast("Please enter valid Date of Birth.");
    } else if(this.marriedStatus == "" && this.pet.code == "SELF"){
      this.showToast("Please select marital status.");
    }else if(this.memberOccupation == ''){
       this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
    }else if((this.memberArray[this.activeMemberIndex].title == "Son" || this.memberArray[this.activeMemberIndex].title == "Son2" || this.memberArray[this.activeMemberIndex].title == "Son3" || this.memberArray[this.activeMemberIndex].title == "Daughter" || this.memberArray[this.activeMemberIndex].title == "Daughter2" || this.memberArray[this.activeMemberIndex].title == "Daughter3") && (this.memberOccupation == "BUSM" || this.memberOccupation == "HSWF" || this.memberOccupation == "RETR" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS")){
      this.showToast("Only dependent childs are covered");
    }else if((this.memberArray[this.activeMemberIndex].title == "Father" || this.memberArray[this.activeMemberIndex].title == "Mother") && (this.memberOccupation == "BUSM" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS" || this.memberOccupation == "STDN" || this.memberOccupation == "UNEM")){
      this.showToast("Only dependent parents are covered");
    }else{           
       this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
       this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
       this.memberArray[this.activeMemberIndex].age = this.memberAge;      

     
      if(this.customText == "male" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
        this.memberArray[this.activeMemberIndex].Gender = 'F';
        this.memberArray[this.activeMemberIndex].genderText = 'Female';
      }else if(this.customText == "female" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
        this.memberArray[this.activeMemberIndex].Gender = 'M';
        this.memberArray[this.activeMemberIndex].genderText = 'Male';
      }else if(this.memberArray[this.activeMemberIndex].title == "Son"){
        this.memberArray[this.activeMemberIndex].Gender = 'M';
        this.memberArray[this.activeMemberIndex].genderText = 'Male';
      }else if(this.memberArray[this.activeMemberIndex].title == "Son2"){
        this.memberArray[this.activeMemberIndex].Gender = 'M';
        this.memberArray[this.activeMemberIndex].genderText = 'Male';
      }else if(this.memberArray[this.activeMemberIndex].title == "Son3"){
        this.memberArray[this.activeMemberIndex].Gender = 'M';
        this.memberArray[this.activeMemberIndex].genderText = 'Male';
      }else if(this.memberArray[this.activeMemberIndex].title == "Daughter"){
        this.memberArray[this.activeMemberIndex].Gender = 'F';
        this.memberArray[this.activeMemberIndex].genderText = 'Female';
      }else if(this.memberArray[this.activeMemberIndex].title == "Daughter2"){
        this.memberArray[this.activeMemberIndex].Gender = 'F';
        this.memberArray[this.activeMemberIndex].genderText = 'Female';
      }else if(this.memberArray[this.activeMemberIndex].title == "Daughter3"){
        this.memberArray[this.activeMemberIndex].Gender = 'F';
        this.memberArray[this.activeMemberIndex].genderText = 'Female';
      }else if(this.memberArray[this.activeMemberIndex].title == "Father"){
        this.memberArray[this.activeMemberIndex].Gender = 'M';
        this.memberArray[this.activeMemberIndex].genderText = 'Male';
      }else if(this.memberArray[this.activeMemberIndex].title == "Mother"){
        this.memberArray[this.activeMemberIndex].Gender = 'F';
        this.memberArray[this.activeMemberIndex].genderText = 'Female';
      }else if(this.memberArray[this.activeMemberIndex].title == "Grand Mother"){
        this.memberArray[this.activeMemberIndex].Gender = 'F';
        this.memberArray[this.activeMemberIndex].genderText = 'Female';
      }else if(this.memberArray[this.activeMemberIndex].title == "Grand Father"){
        this.memberArray[this.activeMemberIndex].Gender = 'M';
        this.memberArray[this.activeMemberIndex].genderText = 'Male';
      }      
      
      console.log("member: " + JSON.stringify(this.memberArray));
      

       for(let j=0; j<this.occupationList.length; j++){
         if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
         this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
         break;
         }
         }

         for(let i=0; i<this.memberArray.length; i++){
          if(this.memberArray[i].title == "Proposer"){
            this.memberArray[i].ProposerDetailsDivShow = true;
          }else{
            this.memberArray[i].ProposerDetailsDivShow = false;
          }
         }
       //this.getMemberContactDetails();   
       
      this.encryptIncompleteData();
      //this.navCtrl.push(VectornomineedeclarationPage, {"ProposerDetails" : this.memberArray});
      //  this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
     }
  }  

}


encryptIncompleteData(){      
  var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  console.log("testt: " + JSON.stringify(sendData));
  this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
}



sendStage1Data(URL,serviceData){
  let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
  this.presentLoadingDefault();
  this.appService.callService(URL,serviceData,headerString)
  .subscribe(Resp =>{
    this.loading.dismiss();
    console.log("Stage 1: "+ JSON.stringify(Resp)); 
      if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
        this.pID = Resp.Proceed_PolicyResult.Data.pID;
        sessionStorage.policyID = this.pID;
        //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        //sessionStorage.isViewPopped = undefined;
        if(this.memberArray[0].SumInsured == ""){
          this.getNomineeForOtherMember();
        }else{
          this.navCtrl.push(VectornomineedeclarationPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
          console.log("cardProp: " + JSON.stringify(this.selectedCardArray));
          console.log("memberFromProposer: " + JSON.stringify(this.memberArray));
        }
        
        
      }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
        //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
        this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        this.navCtrl.push(LoginPage);
      }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
        //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
        this.message = Resp.Proceed_PolicyResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 500;
        //this.navCtrl.push(LoginPage);
      }else{
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
        this.loading.dismiss();
        //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        this.message = Resp.Proceed_PolicyResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 400;
      }
  });
}


getProceedrequest(){
  let sendRequest = {
    "QuotationID": sessionStorage.QuotationID,
    "UID": this.selectedCardArray.UID,
    "stage":"2",
    "Client": this.getClientNode(),
    "Risk": {
      "PolicyType": this.selectedCardArray.Result_vector.Root.Policy.Parameters.PolicyType ,//"HTF",
      "Duration": this.selectedCardArray.Result_vector.Root.Policy.Parameters.Duration,
      "Installments": this.selectedCardArray.Result_vector.Root.Policy.Parameters.Installments,
      "IsFgEmployee": sessionStorage.IsFGEmployee,
      "IsPos" : sessionStorage.Ispos,
      "BeneficiaryDetails": this.selectedCardArray.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member.length == 1 ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
      "MemberDetails": this.memberArray,
      "CardDetails": this.selectedCardArray
    }
  }
  console.log("Request data: "+JSON.stringify(sendRequest));
  return sendRequest;
}

getClientNode(){
  let age = this.memberArray[0].age;
  let clientSalution = "MR";
  if(this.memberArray[0].Gender == "F"){
    clientSalution = "MRS";
  }
  if(this.memberArray[0].maritalStatus == "Married"){
    this.memberArray[0].maritalCode = "M";
  }else if(this.memberArray[0].maritalStatus == "Single"){
    this.memberArray[0].maritalCode = "S";
  }else {
    this.memberArray[0].maritalCode = "S";
  }
  this.ClientDetails.push({
    "ClientType":"I",
    "CreationType": "C",
    "Salutation": clientSalution,
    "FirstName": this.proposerName ,
    "LastName": "",
    "DOB": age,
    "Gender": this.memberArray[0].Gender,
    "MaritalStatus": this.memberArray[0].maritalCode,
    "Occupation": this.memberArray[0].occupationCode,
    "PANNo": this.memberArray[0].pan,
    "GSTIN": "",
    "AadharNo": "",
    "CKYCNo": "",
    "EIANo": "",
    "Address1": {
      "AddrLine1": this.address,
      "AddrLine2": "",
      "AddrLine3": "",
      "Landmark": "",
      "Pincode": this.pinCode,
      "City": this.city,
      "State": this.state,
      "Country": "IND",
      "AddressType": "R",
      "HomeTelNo": "",
      "OfficeTelNo": "",
      "FAXNO": "",
      "MobileNo": this.mobile,
      "EmailAddr": this.email
    },
    "Address2": {
      "AddrLine1": "",
      "AddrLine2": "",
      "AddrLine3": "",
      "Landmark": "",
      "Pincode": this.pinCode,
      "City": "",
      "State": "",
      "Country": "IND",
      "AddressType": "P",
      "HomeTelNo": "",
      "OfficeTelNo": "",
      "FAXNO": "",
      "MobileNo": this.mobile,
      "EmailAddr": this.email
    }
  });
  return this.ClientDetails[this.ClientDetails.length -1];
  //console.log("clientData: " + JSON.stringify(this.ClientDetails));
  
}


getSelfBeneficiaryDetails(cardDetails){
  let BeneDetails = [];
  let mDob = cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].InsuredDob.split(" ")[0].split("/");
  let gendr = this.memberArray[0].Gender;

  BeneDetails.push({
    "MemberId": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].MemberId ,
    "InsuredName": this.proposerName,
    "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
    "InsuredGender": gendr,
    "InsuredOccpn": this.memberArray[0].occupationCode,
    "CoverType": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].CoverType,
    "SumInsured": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].SumInsured,
    "DeductibleDiscount": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].DeductibleDiscount,
    "Relation": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].Relation,
    "NomineeName": "",
    "NomineeRelation": "",
    "AnualIncome": "",
    "Height": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].Height,
    "Weight": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].Weight,
    "NomineeAge": "",
    "AppointeeName": "",
    "AptRelWithominee": "",
    "MedicalLoading": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].MedicalLoading,
    "PreExstDisease": "N",
    "DiseaseMedicalHistoryList": {
      "DiseaseMedicalHistory": {
        "PreExistingDiseaseCode": "",
        "MedicalHistoryDetail": ""
      }
    }
  })
  return BeneDetails;
}


getBeneficiaryDetails(cardDetails){
  let BeneDetails = [];
  
  for(let i = 0 ;i <= cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member.length-1 ; i++){
    for(let j = 0; j <=this.memberArray.length-1;j++){
      //Need to commment below If statement when vector dashboard task is started
      if(i==j){
        if( this.memberArray[j].code == (cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation)){
        
          let mDob = cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
          let gendr = this.memberArray[j].Gender;
          let name;
          if(this.memberArray[j].code == "SELF"){
            name = this.proposerName; 
          }else{
            name = this.nameOfProposer;
          }
          console.log("Gender: " + this.memberArray[j].Gender);
          
          BeneDetails.push({
            "MemberId": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].MemberId ,
            "InsuredName": name,
            "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
            "InsuredGender": gendr,
            "InsuredOccpn": this.memberArray[j].occupationCode,
            "CoverType": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].CoverType,
            "SumInsured": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].SumInsured,
            "DeductibleDiscount": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].DeductibleDiscount,
            "Relation": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation,
            "NomineeName": "",
            "NomineeRelation": "",
            "AnualIncome": "",
            "Height": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Height,
            "Weight": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Weight,
            "NomineeAge": "",
            "AppointeeName": "",
            "AptRelWithominee": "",
            "MedicalLoading": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].MedicalLoading,
            "PreExstDisease": "N",
            "DiseaseMedicalHistoryList": {
              "DiseaseMedicalHistory": {
                "PreExistingDiseaseCode": "",
                "MedicalHistoryDetail": ""
              }
            }
          })
        }
      }


    }

  }
  console.log("ben Details:" + JSON.stringify(BeneDetails));
  
  return BeneDetails;

} 

// DatePick(event){
//     let element = (<HTMLInputElement>event.target.previousElementSibling.querySelector('input[name="age"]'));
//     let dateFieldValue = element.value;
//     let year : any = 1985, month : any = 1, date : any = 1;
//     if(dateFieldValue!="" || dateFieldValue == undefined){
//     date = dateFieldValue.split("-")[0]
//     month = dateFieldValue.split("-")[1];
//     year = dateFieldValue.split("-")[2]
//     }
    
//     let minDateLimit = ( new Date(1985,1,0).getTime());
//     this.datePicker.show({
//     date: new Date(year,month-1,date),
//     minDate: minDateLimit,
//     maxDate: Date.now(),
//     mode: 'date',
//     androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
//     }).then(
//     date => {
//     let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
//     element.focus();
//     element.value = dt;
//     element.textContent = dt;
//     },
    
//     err => console.log('Error occurred while getting date: '+err)
//     );
    
//     }

DatePick(){
  console.log("Click" + this.pet);
  //this.memberAge = "26-04-1985";
  //this.memberAge = "";
  let element;
  this.keyboard.close();   
  if(this.memberAge == undefined){
    element = "";
  }else{
    element = this.memberAge;
  }   
  
  console.log("age" + this.memberAge);
  console.log("age" + element);
  
  let dateFieldValue = element;
  console.log("age" + element);
  let year : any = 1985, month : any = 1, date : any = 1;
  this.childAgeStatus = true;
  if(dateFieldValue!=""){
    date = dateFieldValue ? dateFieldValue.split("-")[0]  : "";
    month = dateFieldValue ? dateFieldValue.split("-")[1] : "";
    year = dateFieldValue ? dateFieldValue.split("-")[2] : "";
    console.log("Year" + 1);
    // date = dateFieldValue.split("-")[0]
    //   month = dateFieldValue.split("-")[1];
    //   year = dateFieldValue.split("-")[2]
    console.log("Year" + year);
    
  }else if(dateFieldValue == "" && (this.pet == "Proposer" || this.pet == "Spouse" || this.pet == "Father" || this.pet == "Mother")){
    let year : any = 1985, month : any = 1, date : any = 1;
    console.log("Year" +2);
  }else if(this.pet == "Son" || this.pet == "Son2" || this.pet == "Son3"  || this.pet == "Daughter" || this.pet == "Daughter2" || this.pet == "Daughter3"){
    date = 1;
    month = 1;
    year = this.today.getFullYear()-25;
    console.log("Year" + 3);
  }
  console.log("year" + year);
  console.log("year" + month);
  console.log("year" + date);
  

  //let minDateLimit = new Date(1985,1,1).getTime();;
  let minDateLimit  = this.pet == "Son" || this.pet == "Son2" || this.pet == "Son3" || this.pet == "Daughter" || this.pet == "Daughter2" || this.pet == "Daughter3" ? ( new Date(year,1,1).getTime()) : (new Date(1900,1,1).getTime()); 

  

  //let minDateLimit  = (this.pet == "Son" || this.pet == "Daughter") ? ( new Date(year,1,1).getTime()) : (new Date(year,1,1).getTime()); 
  console.log("popMember: " + this.pet);
  console.log("minDate: " + minDateLimit);
  
  
  this.datePicker.show({
    date: new Date(year, month-1, date),
    minDate: minDateLimit,
    maxDate: Date.now(),
    mode: 'date',
    androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
  }).then(
    date => {
      let dt = ( this.numberLength(date.getDate())+"-"+this.numberLength(date.getMonth() + 1)+"-"+(date.getFullYear())).toString();
      let age = this.getAge(dt);
     if(parseInt(age.split(" ")[0]) > 65){
      this.showToast("Insured person's age is above 65 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
      this.memberAge = "";
     }else if((this.pet == "Proposer" || this.pet == "Spouse" || this.pet == "Father" || this.pet == "Mother") &&  parseInt(age.split(" ")[0]) < 18){
      this.showToast("Insured person's age is below 18 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
      this.memberAge = "";
     }else if((this.pet == "Son" || this.pet == "Son2" || this.pet == "Son3" || this.pet == "Daughter" || this.pet == "Daughter2" || this.pet == "Daughter3") && parseInt(age.split(" ")[0]) > 25){
      this.showToast("Insured person's age is above 25 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
      this.memberAge = "";
     }
    //  else if(parseInt(age.split(" ")[0]) > 24 && this.pet == "Child" && this.insuredPlanTitle == "vital"){
    //     this.showToast("Dependent child's age is more than 25 years. We apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
    //     this.memberAge = "";
    //     this.childAgeStatus = false;
    //  }
     else if(parseInt(age.split(" ")[0]) > 65 && (this.pet == "Father" || this.pet == "Mother")){
      console.log("Pare");
      
     this.showToast("Parent's age must not be more than 60 years to enter policy");
     this.memberAge = "";
    }
      this.memberAge = dt;
      //this.memberArray[index].nomineeAge  = this.SelfDob[index];
      //console.log("DOB: " + members.nomineeDate);
      console.log("DOB: " + this.memberAge);
    },

    err => console.log('Error occurred while getting date: '+err)
  );
  }


  onDateChange(ev){
    console.log("Pop:" + this.pet);
    
    
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.memberAge+"-";
   
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(ev.target.value);
      if(parseInt(age.split(" ")[0]) > 65){
        this.showToast("Insured person's age is above 65 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
        this.memberAge = "";
      }else if((this.pet == "Proposer" || this.pet == "Spouse" || this.pet == "Father" || this.pet == "Mother") &&  parseInt(age.split(" ")[0]) < 18){
        this.showToast("Insured person's age is below 18 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
        this.memberAge = "";
       }else if((this.pet == "Son" || this.pet == "Son2" || this.pet == "Son3" || this.pet == "Daughter" || this.pet == "Daughter2" || this.pet == "Daughter3") && parseInt(age.split(" ")[0]) > 25){
        this.showToast("Insured person's age is above 18 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
        this.memberAge = "";
       }
      // else if(parseInt(age.split(" ")[0]) > 24 && (this.pet == "Son" || this.pet == "Daughter") && this.insuredPlanTitle == "vital"){
      //   console.log("child");
      //   this.showToast("Dependent child's age is more than 25 years. We apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
      //   this.memberAge = "";
      //   ev.target.value = "";
      //   this.cdr.detectChanges();
      // }
      else if(parseInt(age.split(" ")[0]) > 65 && (this.pet == "Father" || this.pet == "Mother")){
        console.log("Pare");
        
       this.showToast("Parent's age must not be more than 65 years to enter policy");
       this.memberAge = "";
       ev.target.value = "";
      }
      //this.memberArray[idx].age = ev.target.value;
      this.keyboard.close();
    }
    // else if(ev.keyCode == 8){
    //   ev.target.value.pop();
    // }
  }

  onSelectChange() {
    console.log('Selected self', this.occupation);
   
      this.memberArray[0].occupation = this.occupation;    
  
  }



onBackClick(){
  sessionStorage.isViewPopped = "true";
  sessionStorage.ProposerDetails = JSON.stringify(this.memberArray);
  console.log("backStorage" + JSON.stringify(this.memberArray));   
  sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
  this.navCtrl.pop();
}

captureImage(){

  let options: CameraOptions = {   
    // destinationType: this.camera.DestinationType.DATA_URL,
    // encodingType: this.camera.EncodingType.JPEG,
    // mediaType: this.camera.MediaType.PICTURE,
    // quality: 100,
    // saveToPhotoAlbum: false,
    // correctOrientation: true
    quality: 95,
    destinationType: this.camera.DestinationType.FILE_URI,      
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    saveToPhotoAlbum: true,
    correctOrientation: true
  };

  this.presentLoadingDefault();
  this.camera.getPicture(options).then(imageData => {    
  this.thumbnail= imageData;       
  console.log("image: "  +this.thumbnail);
  this.getFileAsBase64(this.thumbnail);
  this.ImageData = imageData;
  this.imageCaptured = false;
  this.loading.dismiss();
  this.showToast("Proposer image captured.");
  }, (err) => {
  /*  Handle error */
    this.loading.dismiss();
  });

}

showImage(){
  this.imagePOp = false;
}


getFileAsBase64(thumbnail){
  this.base64.encodeFile(thumbnail).then((base64File: string) => {
    console.log("base: " + base64File);
  }, (err) => {
    console.log(err);
  });
}

closePop(){
  this.imagePOp = true;
}




    getAge(date){
      let dateVar = date.split("-");
      let ag = (Date.now()-(new Date(dateVar[2],dateVar[1]-1,dateVar[0])).getTime());
      let years = Math.floor(ag / 31536000000);
      let days = Math.floor((ag % 31536000000) / 86400000);
      let months = Math.floor(days/30);
      console.log("years" + years);
      console.log("months" + months);
      console.log("days" + days);                  
                         
      console.log("calculate" + (Date.now()-(new Date(dateVar[2],dateVar[1]-1,dateVar[0])).getTime()));
      
      return Math.floor((ag / (1000 * 3600 * 24))/365)+" years";
    }

    onSelectChangeSelf() {
      console.log('Selected self', this.occupation);
     
        this.memberArray[0].occupation = this.occupation;    
    
    }
    onSelectChangeOther(){
  
      for(let i=0; i<this.memberArray.length;i++){
        //this.memberArray[i].occupationOther = this.occupationOther;
      }
  
      console.log('Selected other', this.occupationOther);
    }

    checkValidPan(){
      if(!this.matchPan(this.pan)){
        this.disableProceedBt = true;
      }else{
        this.disableProceedBt = false;
      }
    }


    matchEmail(email){
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
  
    matchPan(pan){
      var regex = /[A-Za-z]{5}\d{4}[A-Za-z]{1}/;
      return regex.test(pan);
    }
  
    checkFocus(){  
      if((this.activeMemberIndex == 0 && (this.pan != undefined || this.pan != null || this.pan != "")) || this.activeMemberIndex == this.memberArray.length - 1){
        this.disableProceedBt = false;
      }
    }
  
    otherFocusOut(){
      this.disableProceedBt = false;
    }

    serviceResponseOkButton(){
      if(this.serviceCodeStatus == 807){
        this.serviceResponsePopup = true;
        this.navCtrl.push(LoginPage);   
      }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
        this.serviceResponsePopup = true;     
      }
    
    }



  genderClick(gender){

    if(sessionStorage.isViewPopped == "true"){
      if(gender == "male"){
        //this.customText = $(".male").attr('rel');
        this.customText = "male";
        this.memberArray[0].gender = "M"
        sessionStorage.gender = "M"

      }else{
        //this.customText = $(".female").attr('rel');           
        this.customText = "female";
        this.memberArray[0].gender = "F"
        sessionStorage.gender = "F"
      }
    }else{
      if(gender == "male"){
        //this.customText = $(".male").attr('rel');
        this.customText = "male";
        this.memberArray[0].gender = "M"
        sessionStorage.gender = "M"
      }else{
        //this.customText = $(".female").attr('rel');           
        this.customText = "female";
        this.memberArray[0].gender = "F"
        sessionStorage.gender = "F"
      }
    }

  
    this.isActive = !this.isActive;
  } 

    perClick(num){
    console.log(this.totalDis)
    if(num ==  1){
    this.partdisab = !this.partdisab;
  }if(num ==  2){
    this.permdisab = !this.permdisab;
  }else if(num ==3){
    this.tempdisab = !this.tempdisab;
  }

  }


  getNomineeForOtherMember(){
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequestSkipNominee()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendStage3Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  getProceedrequestSkipNominee(){
    let sendRequest = {
      "QuotationID": sessionStorage.QuotationID,
      "UID": this.selectedCardArray.UID,
      "stage":"3",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.selectedCardArray.Result_vector.Root.Policy.Parameters.PolicyType ,//"HTF",
        "Duration": this.selectedCardArray.Result_vector.Root.Policy.Parameters.Duration,
        "Installments": this.selectedCardArray.Result_vector.Root.Policy.Parameters.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        //"c": this.getBeneficiaryDetails(this.selectedCardArray),
        "BeneficiaryDetails" : this.getBeneficiaryDetailsSkipNominee(this.selectedCardArray),
        "MemberDetails": this.memberArray,
        "CardDetails": this.selectedCardArray
      }
    }
    console.log("VerifyPage: "+JSON.stringify(sendRequest));
    return sendRequest;
  }

  getBeneficiaryDetailsSkipNominee(cardDetails){
    console.log("CardNominee: " + JSON.stringify(cardDetails));
    console.log("MemberNominee: " + JSON.stringify(this.memberArray));
    
    //var content = document.querySelector('div .nomineePage').children;

    // this.memberArray[0].nominee = this.nomineeName;
    // this.memberArrayCopy[0].NomineeRelation = this.nomineeRelation;
    // this.memberArrayCopy[0].appointeeMember = this.appointeeName;
    // this.memberArrayCopy[0].AptRelWithominee = this.appointeeRelation;
    // this.memberArrayCopy[0].nomineeAge = this.nomineeDate;
   

    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberArray.length;j++){
        //if(i==j){
          if(this.memberArray[j].code == (cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation) ){

            /*For Nominee and appointee*/
            //let title = content[j].querySelector('h4').textContent;
            // let name = this.nomineeName;
            // let relation = this.nomineeRelation;
            // let dob = this.nomineeDate;
            // let appointeeName =this.appointeeName;
            // let appointeeRelation = this.appointeeRelation;
          /*For Nominee and appointee*/
  
          
      
              // this.memberArrayCopy[j].appointeeName = appointeeName;
              // this.memberArrayCopy[j].appointeeRelation = appointeeRelation;
         
          
          
            let mDob = cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
            let gendr = this.memberArray[j].Gender;
            if(this.memberArray[j].code == "SELF"){
             
            }else{
              this.memberArray[j].nominee = this.memberArray[0].proposerName;
              this.memberArray[j].NomineeRelation = this.getRelationForOtherMembers(this.memberArray[j].title, this.memberArray[0].Gender);
              this.memberArray[j].appointeeMember = "";
              this.memberArray[j].AptRelWithominee = "";
              this.memberArray[j].nomineeAge = this.memberArray[0].age;
      
              this.memberArray[j].appointeeName = "";
              this.memberArray[j].appointeeRelation = "";
              BeneDetails.push({
                "MemberId": Number(cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].MemberId),
                "InsuredName": this.memberArray[j].proposerName,
                "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
                "InsuredGender": gendr,
                "InsuredOccpn": this.memberArray[j].occupationCode,
                "CoverType": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].CoverType,
                "SumInsured": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].SumInsured,
                "DeductibleDiscount": "0",
                "Relation": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation,
                "NomineeName": this.memberArray[0].proposerName,
                "RelationName":this.memberArray[j].title,
                "NomineeRelation": this.getRelationForOtherMembers(this.memberArray[j].title, this.memberArray[0].Gender),
                "AnualIncome": "",
                "Height": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Height,
                "Weight": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Weight,
                "NomineeAge": this.getAge(this.memberArray[0].age).toString(),
                "AppointeeName": "",
                "AptRelWithominee": "",
                "MedicalLoading": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].MedicalLoading,
                "PreExstDisease": "N",
                "DiseaseMedicalHistoryList": {
                  "DiseaseMedicalHistory": {
                    "PreExistingDiseaseCode": "",
                    "MedicalHistoryDetail": ""
                  }
                }
              })
            }
   
          }
        //}
      
  
      }
  
    }
    return BeneDetails;
  
  }
  
  getRelationForOtherMembers(memberDetailsrelation, selfGender){
    
    if(memberDetailsrelation == "Spouse"){
      this.OtherNomineeRelation = "Spouse";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son1" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son1" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son2" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son2" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son3" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son3" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son4" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Son4" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter1" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter1" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter2" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter2" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter3" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter3" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter4" && selfGender == "M"){
      this.OtherNomineeRelation = "Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Daughter4" && selfGender == "F"){
      this.OtherNomineeRelation = "Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Father" && selfGender == "M"){
      this.OtherNomineeRelation = "Son";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Father" && selfGender == "F"){
      this.OtherNomineeRelation = "Daughter";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Mother" && selfGender == "M"){
      this.OtherNomineeRelation = "Son";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Mother" && selfGender == "F"){
      this.OtherNomineeRelation = "Daughter";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Mother" && selfGender == "M"){
      this.OtherNomineeRelation = "Grand Son";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Mother" && selfGender == "F"){
      this.OtherNomineeRelation = "Grand Daughter";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Father" && selfGender =="M"){
      this.OtherNomineeRelation = "Grand Son";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Father" && selfGender =="F"){
      this.OtherNomineeRelation = "Grand Daughter";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Daughter" && selfGender == "M"){
      this.OtherNomineeRelation = "Grand Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Daughter" && selfGender == "F"){
      this.OtherNomineeRelation = "Grand Mother";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Son" && selfGender == "M"){
      this.OtherNomineeRelation = "Grand Father";
      return this.OtherNomineeRelation;
    }else if(memberDetailsrelation == "Grand Son" && selfGender == "F"){
      this.OtherNomineeRelation = "Grand Mother";
      return this.OtherNomineeRelation;
    }
      
    }

  sendStage3Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("nomineeProceedData: " + JSON.stringify(Resp));
      
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          // this.pID = Resp.Proceed_PolicyResult.Data.pID;         
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          console.log("sendToReview:" + JSON.stringify(this.memberArray));
          
         this.navCtrl.push(ReviewPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.loading.dismiss();
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

}
