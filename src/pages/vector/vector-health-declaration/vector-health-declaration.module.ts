import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VectorHealthDeclarationPage } from './vector-health-declaration';

@NgModule({
  declarations: [
    //VectorHealthDeclarationPage,
  ],
  imports: [
    IonicPageModule.forChild(VectorHealthDeclarationPage),
  ],
})
export class VectorHealthDeclarationPageModule {}
