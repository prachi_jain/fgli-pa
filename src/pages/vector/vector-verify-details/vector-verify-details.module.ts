import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VectorVerifyDetailsPage } from './vector-verify-details';

@NgModule({
  declarations: [
    //VectorVerifyDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(VectorVerifyDetailsPage),
  ],
})
export class VectorVerifyDetailsPageModule {}
