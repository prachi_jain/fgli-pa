import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { LoginPage } from '../../health/login/login';

/**
 * Generated class for the QuickquoteOfflinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quickquote-offline',
  templateUrl: 'quickquote-offline.html',
})
export class QuickquoteOfflinePage {

		showTransBaground = true;
    isActive = true;

    //added by vinay
    isSelfSelected:boolean = false;

    premium1Active:boolean = false;
    premium2Active:boolean = false;
    premium3Active:boolean = true; //default 3 year policy should be selected
    
    sInsured:any = "";
    sInsured1:any = "";
    sInsured2:any = "";
    sInsured3:any = "";
    sInsured4:any = "";
    sInsured5:any = "";
    sInsured6:any = "";
    sInsured7:any = "";
    sInsured8:any = "";
    sInsured9:any = "";

    childList:any = [];
    hideDaughterSection:boolean = true;
    hideDaughter2Section:boolean = true;
    hideSonSection:boolean = true;
    hideSon2Section:boolean = true;
    selfsuminsuredList:any = [];
    suminsuredList:any = [];

    premium:number = 0;
    premium2:number = 0;
    premium3:number = 0;
    
    totalsuminsured:number = 0;
    totalpremium:number = 0;
    totalpremium2:number = 0;
    totalpremium3:number = 0;
    selectedPremium:number = 0; 

    selfinsurance = new Insurance(0,0,"self");
    spouseinsurance = new Insurance(0,0,"spouse");
    soninsurance = new Insurance(0,0,"son");
    son2insurance = new Insurance(0,0,"son2");
    son3insurance = new Insurance(0,0,"son3");
    daughterinsurance = new Insurance(0,0,"daughter");
    daughter2insurance = new Insurance(0,0,"daughter2");
    daughter3insurance = new Insurance(0,0,"daughter3");
    fatherinsurance = new Insurance(0,0,"father");
    motherinsurance = new Insurance(0,0,"mother");
    showButton = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public commonProvider:AppService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickquoteOfflinePage');
    setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
    }, 7000);
    this.prepareData();
    if(sessionStorage.pagename == "Login"){
      this.showButton = false;
    }
  }

  
  genderClick(){
    this.isActive = !this.isActive;
  }

  navigateToNextPage(){
    console.log(this.selectedPremium);
    this.navCtrl.push(LoginPage)
  }

  showSonSection(){
    if(this.childList.length < 3){
      if(this.hideSonSection){
        this.hideSonSection = !this.hideSonSection;
      }else{
        this.hideSon2Section = this.hideSonSection;
      }
    }else{
      this.commonProvider.showToast("Up to 3 dependent children allowed");
    }
  }

  showDaughterSection(){
    if(this.childList.length < 3){
      if(this.hideDaughterSection)
        this.hideDaughterSection = !this.hideDaughterSection;
      else
      this.hideDaughter2Section = this.hideDaughterSection;
    }else{
      this.commonProvider.showToast("Up to 3 dependent children allowed");
    }
  }

  //pending 3 child addition logic
  addChild(type:any, insurance:Insurance){
    // console.log("Before");
    // console.log(this.childList);
    if(this.childList.length == 0){
      this.childList.push(insurance);
    }else if(this.childList.length > 0 && this.childList.length <= 3){
      //Check if element is present then replace else add new element.
      var isPresent = false;

      this.childList.forEach((data)=>{
        if(data.insureType === insurance.insureType){
          isPresent = true;
          data.suminsured = insurance.suminsured;
          data.premium = insurance.premium;
        }
      });  
      
      if(!isPresent && this.childList.length < 3)
        this.childList.push(insurance);
      else if(!isPresent && this.childList.length == 3)
        this.commonProvider.showToast("Up to 3 dependent children allowed");

    }else{
      this.commonProvider.showToast("Up to 3 dependent children allowed");
    }
    console.log("After");
    console.log(this.childList);

  }

  deleteSection(insureType:any){

    if(insureType == "self"){
      this.isSelfSelected = false;

      this.sInsured = "";
      this.selfinsurance = new Insurance(0,0,"self");
      
      // this.sInsured1 = "";
      // this.spouseinsurance = new Insurance(0,0,"spouse");

      // this.sInsured2 = "";
      // this.soninsurance = new Insurance(0,0,"son");

      // this.sInsured3 = "";
      // this.daughterinsurance = new Insurance(0,0,"daughter");

      // this.sInsured4 = "";
      // this.fatherinsurance = new Insurance(0,0,"father");

      // this.sInsured5 = "";
      // this.motherinsurance = new Insurance(0,0,"mother");

      // this.sInsured6 = "";
      // this.son2insurance = new Insurance(0,0,"son2");
      // this.hideSonSection = true;

      // this.sInsured7 = "";
      // this.daughter2insurance = new Insurance(0,0,"daughter2");
      // this.hideDaughterSection = true;

      // this.sInsured8 = "";
      // this.hideSon2Section = true;
      // this.son3insurance = new Insurance(0,0,"son3");

      // this.sInsured9 = "";
      // this.hideDaughter2Section = true;
      // this.daughter3insurance = new Insurance(0,0,"daughter3");

      // this.suminsuredList = [];
      // this.childList = [];
      
      // this.premium1Active = false;
      // this.premium2Active = false;
      // this.premium3Active = false;
      
    }else if(insureType == "spouse"){
      this.sInsured1 = "";
      this.spouseinsurance = new Insurance(0,0,"spouse");
    }else if(insureType == "son"){
      this.sInsured2 = "";
      this.soninsurance = new Insurance(0,0,"son");
      if(this.childList.length > 0)
        this.childList.splice(this.childList.length-1,1);
    }else if(insureType == "daughter"){
      this.sInsured3 = "";
      this.daughterinsurance = new Insurance(0,0,"daughter");
      if(this.childList.length > 0)
        this.childList.splice(this.childList.length-1,1);
    }else if(insureType == "father"){
      this.sInsured4 = "";
      this.fatherinsurance = new Insurance(0,0,"father");
    }else if(insureType == "mother"){
      this.sInsured5 = "";
      this.motherinsurance = new Insurance(0,0,"mother");
    }else if(insureType == "son2"){
      this.hideSonSection = true;
      this.sInsured6 = "";
      this.son2insurance = new Insurance(0,0,"son2");
      if(this.childList.length > 0)
        this.childList.splice(this.childList.length-1,1);
    }else if(insureType == "daughter2"){
      this.hideDaughterSection = true;
      this.sInsured7 = "";
      this.daughter2insurance = new Insurance(0,0,"daughter2");
      if(this.childList.length > 0)
        this.childList.splice(this.childList.length-1,1);
    }else if(insureType == "son3"){
      this.hideSon2Section = true;
      this.sInsured8 = "";
      this.son3insurance = new Insurance(0,0,"son3");
      if(this.childList.length > 0)
        this.childList.splice(this.childList.length-1,1);
    }else if(insureType == "daughter3"){
      this.hideDaughter2Section = true;
      this.sInsured9 = "";
      this.daughter3insurance = new Insurance(0,0,"daughter3");
      if(this.childList.length > 0)
        this.childList.splice(this.childList.length-1,1);
    }
    this.calculatePremium("", new Insurance(0,0,""), 0);

  }
  
  //added by vinay
  prepareData(){
    let insuredamount1 = new Insurance(10000,254,"");
    let insuredamount2 = new Insurance(25000,461,"");
    let insuredamount3 = new Insurance(50000,923,"");
    let insuredamount4 = new Insurance(75000,1384,"");

    this.selfsuminsuredList[0] = insuredamount1;
    this.selfsuminsuredList[1] = insuredamount2;
    this.selfsuminsuredList[2] = insuredamount3;
    this.selfsuminsuredList[3] = insuredamount4;

    // this.suminsuredList[0] = insuredamount1;
    // this.suminsuredList[1] = insuredamount2;
    // this.suminsuredList[2] = insuredamount3;
    // this.suminsuredList[3] = insuredamount4;

    //this.selectedPremium = this.totalpremium3; //default 3 year policy should be selected
    this.updateSumInsuredList(3);
  }

  updateSumInsuredForOtherMembers(selfMember:Insurance){
    if(selfMember.suminsured < this.spouseinsurance.suminsured){
      this.spouseinsurance.suminsured = selfMember.suminsured;
      this.sInsured1 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.soninsurance.suminsured){
      this.soninsurance.suminsured = selfMember.suminsured;
      this.sInsured2 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.daughterinsurance.suminsured){
      this.daughterinsurance.suminsured = selfMember.suminsured;
      this.sInsured3 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.fatherinsurance.suminsured){
      this.fatherinsurance.suminsured = selfMember.suminsured;
      this.sInsured4 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.motherinsurance.suminsured){
      this.motherinsurance.suminsured = selfMember.suminsured;
      this.sInsured5 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.son2insurance.suminsured){
      this.son2insurance.suminsured = selfMember.suminsured;
      this.sInsured6 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.son3insurance.suminsured){
      this.son3insurance.suminsured = selfMember.suminsured;
      this.sInsured8 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.daughter2insurance.suminsured){
      this.daughter2insurance.suminsured = selfMember.suminsured;
      this.sInsured7 = selfMember.suminsured;
    } 
    if(selfMember.suminsured < this.daughter3insurance.suminsured){
      this.daughter3insurance.suminsured = selfMember.suminsured;
      this.sInsured9 = selfMember.suminsured;
    }
  }
  

  //function to validate Self sum insured must be greater than other members
  updateSumInsuredList(index){
    this.suminsuredList = [];

    let insuredamount1 = new Insurance(10000,254,"");
    let insuredamount2 = new Insurance(25000,461,"");
    let insuredamount3 = new Insurance(50000,923,"");
    let insuredamount4 = new Insurance(75000,1384,"");

    if(index>=0)
      this.suminsuredList[0] = insuredamount1;
    if(index>=1)
      this.suminsuredList[1] = insuredamount2;
    if(index>=2)
      this.suminsuredList[2] = insuredamount3;
    if(index>=3)
      this.suminsuredList[3] = insuredamount4;

  }

  //added by vinay
  //called on ionSelect of select-option
  calculatePremium(insuranceHolder:any, insurancemember:Insurance, index){
    var insurance = new Insurance(insurancemember.suminsured, insurancemember.premium, insuranceHolder);
    insurance.insureType = insuranceHolder;
    if(insuranceHolder == "self"){
      this.isSelfSelected = true;
      this.selfinsurance = insurance;
      this.updateSumInsuredForOtherMembers(insurance);
      this.updateSumInsuredList(index);
    }else if(insuranceHolder == "spouse"){
      this.spouseinsurance = insurance;
    }else if(insuranceHolder == "son"){
      this.soninsurance = insurance;
      this.addChild(insuranceHolder, insurance);   
    }else if(insuranceHolder == "daughter"){
      this.daughterinsurance = insurance;
      this.addChild(insuranceHolder, insurance);  
    }else if(insuranceHolder == "father"){
      this.fatherinsurance = insurance;
    }else if(insuranceHolder == "mother"){
      this.motherinsurance = insurance;
    }else if(insuranceHolder == "son2"){
      this.son2insurance = insurance;
      this.addChild(insuranceHolder, insurance);  
    }else if(insuranceHolder == "daughter2"){
      this.daughter2insurance = insurance;
      this.addChild(insuranceHolder, insurance);  
    }else if(insuranceHolder == "son3"){
      this.son3insurance = insurance;
      this.addChild(insuranceHolder, insurance);  
    }else if(insuranceHolder == "daughter3"){
      this.daughter3insurance = insurance;
      this.addChild(insuranceHolder, insurance);  
    }

    //calculate total sum insured and total premium
    this.totalsuminsured = this.selfinsurance.suminsured + this.spouseinsurance.suminsured + this.soninsurance.suminsured + this.daughterinsurance.suminsured + this.motherinsurance.suminsured + this.fatherinsurance.suminsured + this.son2insurance.suminsured + this.daughter2insurance.suminsured + this.son3insurance.suminsured + this.daughter3insurance.suminsured;
    
    var premium = this.selfinsurance.premium + this.spouseinsurance.premium + this.soninsurance.premium + this.daughterinsurance.premium + this.motherinsurance.premium + this.fatherinsurance.premium + this.son2insurance.premium + this.daughter2insurance.premium + this.son3insurance.premium + this.daughter3insurance.premium;
    this.totalpremium = Math.round(premium + premium*0.18); //Including GST
    
    var premium2 = (premium*2) - (premium*2)/20; //5% discount
    this.premium2 = Math.round(premium*2 + (premium*2)*0.18); //value without discount
    this.totalpremium2 = Math.round(premium2 + premium2*0.18); //Including GST
    
    var premium3 = (premium*3) - (premium*3)*0.075; //7.5% discount
    this.premium3 = Math.round(premium*3 + (premium*3)*0.18); //value without discount
    this.totalpremium3 = Math.round(premium3 + premium3*0.18); //Including GST
    
    console.log(premium);
    console.log(premium2);
    console.log(premium3);

    console.log(this.totalpremium);
    console.log(this.totalpremium2);
    console.log(this.totalpremium3);
  }

  listClick(premium) {
    console.log(premium);
    if(premium > 0){
      this.selectedPremium = premium;  // don't forget to update the model here
      this.premium1Active = false;
      this.premium2Active = false;
      this.premium3Active = false;

      if(this.selectedPremium == this.totalpremium)
        this.premium1Active = true;
      if(this.selectedPremium == this.totalpremium2)
        this.premium2Active = true;
      if(this.selectedPremium == this.totalpremium3)
        this.premium3Active = true;
    }
  }

  validateSelfInsuredFirst(){
    if(this.selfinsurance.suminsured <= 0)
      this.commonProvider.showToast("Please insure 'Self' first before choosing other member/s.");
  }

}

export class Insurance {
  suminsured:number = 0;
  premium:number = 0;
  insureType:String = "";

  constructor(suminsured:number, premium:number, insureType:String){
    this.suminsured = suminsured;
    this.premium = premium;
    this.insureType = insureType;
  }
}
