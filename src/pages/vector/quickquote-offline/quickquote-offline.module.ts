import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuickquoteOfflinePage } from './quickquote-offline';

@NgModule({
  declarations: [
    //QuickquoteOfflinePage,
  ],
  imports: [
    IonicPageModule.forChild(QuickquoteOfflinePage),
  ],
})
export class QuickquoteOfflinePageModule {}
