import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { LoginPage } from '../../health/login/login';
import { VecproposerdetailsPage } from '../vecproposerdetails/vecproposerdetails';

/**
 * Generated class for the VecquickquotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vecquickquote',
  templateUrl: 'vecquickquote.html',
})
export class VecquickquotePage {
  @ViewChild('input') myInput;
  @ViewChild('emailfocus') emailFocus;
  @ViewChild('mobilefocus') mobileFocus;
	showTransBaground = true;
  isActive = true;
  premPet = 'summary';
  pet ='OneTime';  
  showPopup = true;
  bDetailsPop = true;
  successLabel = true;
  NameOfEmailsmsProposer;
  email;
  mobile;
  sendEmailList = [{"value":""}];
  emailTo=[];
  sendSMSList = [{"value":""}];
  smsTo;
  canShowToast = true;
  serviceResponsePopup = true;
  message;
  serviceCodeStatus;
  ReferenceNo;
  QuotationID;
  cardUID;
  thankyouPopup = true;
  ClientDetails= [];
  pID;
  spouseGender = "F";
  selectedPremium = new Premium(0,0,0);
  premiumDetails;
  selectedPremiumDetails;
  cardClick;
  ENQ_PolicyResponse;
  tempMember;
  memberDetailList:any = [];
  membersPremiumList:any = [];
  policyType;
  beneficiaryDetails = [];
  newBeneficiaryDetails = [];
  transparentDiv = true;
  isChildListFull = false;

  proposerItem;
  //added by vinay
  isSelfSelected:boolean = false;

  premium1Active:boolean = true;
  premium2Active:boolean = false;
  premium3Active:boolean = false; //default 3 year policy should be selected
  
  sInsured:any = "";
  sInsured1:any = "";
  sInsured2:any = "";
  sInsured3:any = "";
  sInsured4:any = "";
  sInsured5:any = "";
  sInsured6:any = "";
  sInsured7:any = "";
  sInsured8:any = "";
  sInsured9:any = "";

  membersList:any = [];
  childList:any = [];
  hideDaughterSection:boolean = true;
  hideDaughter2Section:boolean = true;
  hideSonSection:boolean = true;
  hideSon2Section:boolean = true;
  selfsuminsuredList:any = [];
  suminsuredList:any = [];

  premium:number = 0;
  premium2:number = 0;
  premium3:number = 0;
  
  totalsuminsured:number = 0;
  totalpremium:number = 0;
  totalpremium2:number = 0;
  totalpremium3:number = 0;  
  taxpremium:number = 0;
  taxpremium2:number = 0;
  taxpremium3:number = 0;
  premiumwithoutdiscount2 = 0;
  premiumwithoutdiscount3 = 0;
  discountedValue;
  directDiscount;
  intDD;
  intDiscountValue;
  nettotalpremium:number = 0;

  soninsurance = new Insurance(0,0,"son");
  son2insurance = new Insurance(0,0,"son2");
  son3insurance = new Insurance(0,0,"son3");
  daughterinsurance = new Insurance(0,0,"daughter");
  daughter2insurance = new Insurance(0,0,"daughter2");
  daughter3insurance = new Insurance(0,0,"daughter3");

  buyPopup:Boolean = false;
  childTypeList:any = ["son", "daughter", "son2", "daughter2", "son3", "daughter3"];
  userType;

  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  constructor(public navCtrl: NavController,public toast: ToastController, public navParams: NavParams, public commonProvider:AppService, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VecquickquotePage');
    	setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);
    this.prepareData();
    this.transparentDiv = true;   
    console.log("back State: " + sessionStorage.isViewPopped);
    if(sessionStorage.isViewPopped == "true"){
      this.memberDetailList = JSON.parse(sessionStorage.ProposerDetails);
      console.log("mem" + JSON.stringify(this.memberDetailList));
      
      this.selectedPremiumDetails = JSON.parse(sessionStorage.planCardDetails);    
      console.log("cardData: " + JSON.stringify(this.selectedPremiumDetails));
        
      sessionStorage.isViewPopped = "";
      this.setStoredData();
      this.successLabel = true;
    }
    
  }

  setStoredData(){
    this.NameOfEmailsmsProposer = this.memberDetailList[0].proposerName;
    this.email = this.memberDetailList[0].email;
    this.mobile = this.memberDetailList[0].mobile;
  }


  ionViewDidEnter(){
    if(sessionStorage.isViewPopped == "true"){
      this.memberDetailList = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedPremiumDetails = JSON.parse(sessionStorage.planCardDetails);    
      console.log("cardData: " + JSON.stringify(this.selectedPremiumDetails));
      this.buyPopup = true;
      this.serviceResponsePopup = true;
      this.NameOfEmailsmsProposer = sessionStorage.propName;
      this.email = sessionStorage.email;
      this.mobile = sessionStorage.mobile;    
      this.successLabel = true;     
      //sessionStorage.isViewPopped = "";
      
    }
  }
  

  //added by gaushal
  genderClick(){
    this.isActive = !this.isActive;
  }  
  premiumClick(){
    console.log("Wait");    
    console.log(this.intDD);
    console.log(this.intDiscountValue);
    console.log(this.selectedPremium.netPremium);
        
    this.showPopup = false;
    this.showTransBaground = false;
  }
  
  hidePopup(){
    this.showPopup = true;
    this.showTransBaground = true;
  }
  buyPolicyClick(){
    this.bDetailsPop = false;
    this.showTransBaground = false;
    sessionStorage.membersList = JSON.stringify(this.memberDetailList);
  }
  bDetailsHide(){
      this.bDetailsPop = true;
      this.showTransBaground = true; 
  }

  //added by vinay
  navigateToNextPage(){
    console.log(this.selectedPremium);
    this.buyPopup = true;
    this.showPopup = true;
    this.successLabel = true;

      // this.NameOfEmailsmsProposer = "";
      // this.smsTo = [];
      // this.emailTo = [];
      // this.email = "";
      // this.mobile = "";

      //for testing purpose hardcodded value added -----------------------
      // this.NameOfEmailsmsProposer = "Pooja";
      // this.email = "Pooja_bhole@idealake.com";
      // this.mobile = "7276433090";
    sessionStorage.membersList = JSON.stringify(this.memberDetailList);
    this.setStoredData();
  }

  // showSonSection(){
  //   if(this.childList.length < 3){
  //     if(this.hideSonSection){
  //       this.hideSonSection = !this.hideSonSection;
  //     }else{
  //       this.hideSon2Section = this.hideSonSection;
  //     }
  //   }else{
  //     this.commonProvider.showToast("Up to 3 dependent children allowed");
  //   }
  // }

  showSonSection(){
    if(this.childList.length < 3){
        this.hideSonSection = !this.hideSonSection;
    }else{
      this.commonProvider.showToast("Up to 3 dependent children allowed");
    }
  }

  showSon2Section(){
    if(this.childList.length < 3){
        this.hideSon2Section = !this.hideSon2Section;
    }else{
      this.commonProvider.showToast("Up to 3 dependent children allowed");
    }
  }

  showDaughterSection(){
    if(this.childList.length < 3){
        this.hideDaughterSection = !this.hideDaughterSection;
    }else{
      this.commonProvider.showToast("Up to 3 dependent children allowed");
    }
  }

  showDaughter2Section(){
    if(this.childList.length < 3){
        this.hideDaughter2Section = !this.hideDaughter2Section;
    }else{
      this.commonProvider.showToast("Up to 3 dependent children allowed");
    }
  }

  // showDaughterSection(){
  //   if(this.childList.length < 3){
  //     if(this.hideDaughterSection)
  //       this.hideDaughterSection = !this.hideDaughterSection;
  //     else
  //     this.hideDaughter2Section = this.hideDaughterSection;
  //   }else{
  //     this.commonProvider.showToast("Up to 3 dependent children allowed");
  //   }
  // }

  //pending 3 child addition logic
  addChild(type:any, insurance:Insurance){

    let updatedItem = this.childList.find( member=> member.insureType === insurance.insureType );
    let index = this.childList.indexOf(updatedItem);
    if(index >= 0 && this.childList.length < 3)
      this.childList[index] = insurance;
    else if(index < 0 && this.childList.length < 3)
      this.childList.push(insurance);
    else
      this.commonProvider.showToast("Up to 3 dependent children allowed");
  }
  
  //added by vinay
  prepareData(){
    let insuredamount1 = new Insurance(10000,254,"");
    let insuredamount2 = new Insurance(25000,461,"");
    let insuredamount3 = new Insurance(50000,923,"");
    let insuredamount4 = new Insurance(75000,1384,"");

    this.selfsuminsuredList[0] = insuredamount1;
    this.selfsuminsuredList[1] = insuredamount2;
    this.selfsuminsuredList[2] = insuredamount3;
    this.selfsuminsuredList[3] = insuredamount4;

    // this.suminsuredList[0] = insuredamount1;
    // this.suminsuredList[1] = insuredamount2;
    // this.suminsuredList[2] = insuredamount3;
    // this.suminsuredList[3] = insuredamount4;

    //this.selectedPremium = this.totalpremium3; //default 3 year policy should be selected
    this.updateSumInsuredList(3);
  }

  //function to validate Self sum insured must be greater than other members
  updateSumInsuredList(index){
    this.suminsuredList = [];

    let insuredamount1 = new Insurance(10000,254,"");
    let insuredamount2 = new Insurance(25000,461,"");
    let insuredamount3 = new Insurance(50000,923,"");
    let insuredamount4 = new Insurance(75000,1384,"");

    if(index>=0)
      this.suminsuredList[0] = insuredamount1;
    if(index>=1)
      this.suminsuredList[1] = insuredamount2;
    if(index>=2)
      this.suminsuredList[2] = insuredamount3;
    if(index>=3)
      this.suminsuredList[3] = insuredamount4;

  }

  getSumInsuredFor(relation:any){
    if(this.membersList.length > 0){
      let updatedItem:Members = this.membersList.find( member=> member.Relation === relation );
      return updatedItem != undefined ? updatedItem.SumInsured : '0';  
    }else{
      return '0';
    }
  }

    //on click of delete icon     
  deleteSection(relation:any){
    //check if member is present then replace else push
    let updatedItem = this.membersList.find( member=> member.Relation === relation );
    let index = this.membersList.indexOf(updatedItem);
    if(index >= 0)
      this.membersList.splice(index,1);
    
    //update ngmodal for each dropdown
    if(relation == "SELF"){
      this.sInsured = "";
      this.updateSumInsuredList(3);
    }else if(relation == "SPOUSE"){
      this.sInsured1 = "";
      //this.spouseinsurance = new Insurance(0,0,"spouse");
    }else if(relation == "SON"){
      this.sInsured2 = "";
    }else if(relation == "DAUGHTER"){
      this.sInsured3 = "";
    }else if(relation == "FATHER"){
      this.sInsured4 = "";
      //this.fatherinsurance = new Insurance(0,0,"father");
    }else if(relation == "MOTHER"){
      this.sInsured5 = "";
      //this.motherinsurance = new Insurance(0,0,"mother");
    }else if(relation == "SON2"){
      this.hideSonSection = true;
      this.sInsured6 = "";
    }else if(relation == "DAUGHTER2"){
      this.hideDaughterSection = true;
      this.sInsured7 = "";
    }else if(relation == "SON3"){
      this.hideSon2Section = true;
      this.sInsured8 = "";
    }else if(relation == "DAUGHTER3"){
      this.hideDaughter2Section = true;
      this.sInsured9 = "";
    }

    if(this.childTypeList.includes(relation.toLowerCase())){
      if(this.childList.length > 0){
        //check childList array
        let updatedChildItem = this.childList.find( member=> member.insureType === relation.toLowerCase() );
        let childIndex = this.childList.indexOf(updatedChildItem);
        if(childIndex > 0)
          this.childList.splice(childIndex,1);
      }
    }
    console.log(this.membersList);
    console.log(this.childList);
    
    if(this.childList.length < 3)
      this.isChildListFull = false;
    else
      this.isChildListFull = true;

    //calculate premium
    this.calculatePremium();
  }

  //called on ionSelect of select-option
  updateMembersArray(insuranceHolder:any, member:Insurance, selectedindex:any){
    let newMember = new Members(insuranceHolder,member.suminsured.toString());
    let childMember = new Insurance(member.suminsured, member.premium, insuranceHolder.toLowerCase());

    member.insureType = insuranceHolder.toLowerCase();
    //check childList array
    let updatedChildItem = this.childList.find( childmember => childmember.insureType === insuranceHolder.toLowerCase() );
    let childIndex = this.childList.indexOf(updatedChildItem);

    //check memberList array, if member is present then replace else push
    let updatedItem = this.membersList.find( member=> member.Relation === newMember.Relation );
    let index = this.membersList.indexOf(updatedItem);

    if(this.childTypeList.includes(insuranceHolder.toLowerCase())){
      if(childIndex >= 0 && this.childList.length <= 3){
        this.childList[childIndex] = childMember;
        if(index >= 0)
          this.membersList[index] = newMember;
        else
          this.membersList.push(newMember);
        this.calculatePremium();        
      }else if(childIndex < 0 && this.childList.length < 3){
        this.childList.push(childMember);
        if(index >= 0)
          this.membersList[index] = newMember;
        else
          this.membersList.push(newMember);
        this.calculatePremium();        
      }else{
        if(insuranceHolder == "SON2"){
          this.sInsured6 = "null";
        }else if(insuranceHolder == "DAUGHTER2"){
          this.sInsured7 = "null";

        }else if(insuranceHolder == "SON3"){
          this.sInsured8 = "null";
        }else if(insuranceHolder == "DAUGHTER3"){
          this.sInsured9 = "null";
        }
        this.commonProvider.showToast("Up to 3 dependent children allowed");
      }      
    }else{
      if(index >= 0)
        this.membersList[index] = newMember;
      else
        this.membersList.push(newMember);

      if(insuranceHolder.toLowerCase() == "self"){
        this.updateSumInsuredForOtherMembers(newMember);
        this.updateSumInsuredList(selectedindex);
      }
      this.calculatePremium();
    }
      
    if(this.childList.length < 3)
      this.isChildListFull = false;
    else
      this.isChildListFull = true;

    console.log(this.membersList);
    console.log(this.childList);
  
  }

  updateSumInsuredForOtherMembers(selfMember:Members){
    for(var i=0; i < this.membersList.length; i++){
      if(this.membersList[i].Relation != "SELF"){
        if(selfMember.SumInsured < this.membersList[i].SumInsured){
          this.membersList[i].SumInsured = selfMember.SumInsured;

          if(this.membersList[i].Relation == "SPOUSE"){
            this.sInsured1 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "SON"){
            this.sInsured2 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "DAUGHTER"){
            this.sInsured3 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "FATHER"){
            this.sInsured4 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "MOTHER"){
            this.sInsured5 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "SON2"){
            this.sInsured6 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "DAUGHTER2"){
            this.sInsured7 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "SON3"){
            this.sInsured8 = selfMember.SumInsured;
          }else if(this.membersList[i].Relation == "DAUGHTER3"){
            this.sInsured9 = selfMember.SumInsured;
          }      
        }
      }
    }
  }

  //calculates premium
  calculatePremium(){    
    //call API and calculate premium 
    let callRequest;
    
    
    if(this.membersList.length > 0){
      for(let i=0; i<this.membersList.length; i++){
        this.membersList[i].MemberId = i+1;
      }  

      console.log("memberNew" + JSON.stringify(this.membersList));
      
      callRequest = {
        'Duration': [1,2,3],
        'Installments': ['FULL','MONTHLY','QUARTERLY','HALFYEARLY'], 
        'ProductType': 'vector',
        'RefrenceNo':'', 
        'Client': {
          'ClientType': 'I',
          'CreationType': 'C',
          'Salutation': 'MRS',
          'FirstName': '',
          'LastName': '',
          'DOB': '2018-03-12',
          'Gender': 'F',
          'MaritalStatus': 'M',
          'Occupation': '',
          'PANNo': '',
          'GSTIN': '',
          'AadharNo': '',
          'CKYCNo': '',
          'EIANo': '',
          'Address1': {
              'AddrLine1': '',
              'AddrLine2': '',
              'AddrLine3': '',
              'Landmark': '',
              'Pincode': '401107',
              'City': '',
              'State': '',
              'Country': 'IND',
              'AddressType': 'R',
              'HomeTelNo': '',
              'OfficeTelNo': '',
              'FAXNO': '',
              'MobileNo': '',
              'EmailAddr': ''
          }
        },
        
        'Risk': {
          'PolicyType': "HLI",
          'Duration': '1',
          'Installments': 'FULL',
          'IsFgEmployee': sessionStorage.IsFGEmployee,
          'BeneficiaryDetails': this.membersList,
        }
      };

      console.log("callRequest" + JSON.stringify(callRequest));
      
     
      var sendData = this.commonProvider.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
      
      let headerString = this.commonProvider.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.presentLoadingDefault();
      this.commonProvider.callService("QuotePurchase.svc/ENQ_Policy",{'request': sendData}, headerString).subscribe(ENQ_Policy =>{
        
        if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "0"){
           console.log("ENQ" + JSON.stringify(ENQ_Policy));
          this.loading.dismiss();
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.ENQ_PolicyResponse  = ENQ_Policy.ENQ_PolicyResult.Data;
          this.ReferenceNo = this.ENQ_PolicyResponse.RefrenceNo;
          this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
          sessionStorage.QuotationID = this.QuotationID;
          let responsearray = ENQ_Policy.ENQ_PolicyResult.Data.PurchaseResponse;
          let updatedItem = responsearray.filter( member=> member.Installment === "FULL" );
           console.log("EnquiryData" + JSON.stringify(updatedItem));
          
          this.premiumDetails = updatedItem;
          // //calculate total sum insured and total premium
          let members = updatedItem[0].Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member;
          this.membersPremiumList = members;
          var totalsum = 0;
          members.forEach(member => {
            totalsum += Number(member.SumInsured);
          });
          this.totalsuminsured = totalsum;

          //extra loop used as we are gettinmg relation null from API
          this.memberDetailList = [];
          this.membersList.forEach(member => {
          var memberDetail = new MemberDetails(member.Relation,member.SumInsured);
          memberDetail.title = this.toTitleCase(member.Relation);
          this.memberDetailList.push(memberDetail);
          });
          
          this.premium = Number(updatedItem[0].Result_vector.Root.Policy.Parameters.NetPremium); //value without tax
          console.log("check" + this.premium);
          
          //this.premium = Math.round(updatedItem[0].Result_vector.Root.Policy.Parameters.NetPremium); //value without tax
          this.taxpremium = Math.round(updatedItem[0].Result_vector.Root.Policy.Parameters.TaxPremium); //GST value
          this.totalpremium = Math.round(updatedItem[0].Result_vector.Root.Policy.Parameters.TotalPremium); //Including GST          
          
          this.premium2 = Number(updatedItem[1].Result_vector.Root.Policy.Parameters.NetPremium); //value without tax
          this.taxpremium2 = Math.round(updatedItem[1].Result_vector.Root.Policy.Parameters.TaxPremium); //GST value
          this.totalpremium2 = Math.round(updatedItem[1].Result_vector.Root.Policy.Parameters.TotalPremium); //Including GST
          this.premiumwithoutdiscount2 = Math.round(this.totalpremium2 + this.totalpremium2/20);//totalpremium without discount
          
          this.premium3 = Number(updatedItem[2].Result_vector.Root.Policy.Parameters.NetPremium); //value without tax
          this.taxpremium3 = Math.round(updatedItem[2].Result_vector.Root.Policy.Parameters.TaxPremium); //GST value
          this.totalpremium3 = Math.round(updatedItem[2].Result_vector.Root.Policy.Parameters.TotalPremium); //Including GST
          this.premiumwithoutdiscount3 = Math.round(this.totalpremium3 + this.totalpremium3*0.075);//totalpremium without discount

          sessionStorage.quoteno = ENQ_Policy.ENQ_PolicyResult.Data.RefrenceNo;
          this.saveSelectedPremium();
          this.selectedPremium.totalSumInsured = this.totalsuminsured;

        }else if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "807"){
          this.loading.dismiss();
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          //this.commonProvider.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
          this.serviceCodeStatus = 807;
          this.message = ENQ_Policy.ENQ_PolicyResult.ReturnMsg;
          //this.navCtrl.push(LoginPage);
        }else if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          this.message = "Oops! There seems to be a technical issue at our end. Please try again later.";
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          //this.commonProvider.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          //this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          // console.log("Service resp: "+JSON.stringify(ENQ_Policy));
          this.loading.dismiss(); 
          //this.appService.showloading.dismiss(); 
          this.commonProvider.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
        
        }
      });
    }else{
      this.totalpremium = 0;
      this.totalpremium2 = 0;
      this.totalpremium3 = 0;
    }
  }

  saveSelectedPremium(){
    //selected premium
    console.log("A");
    let responsearray = this.ENQ_PolicyResponse.PurchaseResponse;
    let updatedItem = responsearray.filter( member=> member.Installment === "FULL" );

    if(this.premium1Active){
      this.selectedPremium = new Premium(Math.round(this.premium),this.taxpremium,this.totalpremium);
      this.selectedPremiumDetails = this.premiumDetails[0];
      this.discountedValue = 0;
      this.intDiscountValue = 0;
      if(this.ENQ_PolicyResponse.PurchaseResponse[0].Result_vector.Root.Policy.Parameters.IsFgEmployee == "True"){
        this.directDiscount = Math.round(this.premium*15/85);
        console.log("Premium" + this.premium);
        
        // this.directDiscount = ((Number(this.premium)*15)/85).toFixed(2);
        // this.intDD = parseInt(this.directDiscount);
      }else{
        this.directDiscount = 0;  
        //this.intDD = 0;
      }
      this.nettotalpremium = this.premium + (this.ENQ_PolicyResponse.PurchaseResponse[0].Result_vector.Root.Policy.Parameters.IsFgEmployee == "True" ? (this.premium*15/85) : 0) + 0;
      let members = updatedItem[0].Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member;
      this.membersPremiumList = members;
      this.cardUID = updatedItem[0].UID;
    }else if(this.premium2Active){
      this.selectedPremium = new Premium(Math.round(this.premium2),this.taxpremium2,this.totalpremium2);
      this.selectedPremiumDetails = this.premiumDetails[1];
      this.discountedValue = Math.round((this.premium*2)/20);
      //this.discountedValue = ((Number(this.premium)*2)/20).toFixed(2);
      //this.intDiscountValue = parseInt(this.discountedValue);
      if(this.ENQ_PolicyResponse.PurchaseResponse[1].Result_vector.Root.Policy.Parameters.IsFgEmployee == "True"){
        this.directDiscount = Math.round((this.premium*2)*15/85);      
        // this.directDiscount = ((Number(this.premium)*2)*15/85).toFixed(2);
        // this.intDD = parseInt(this.directDiscount);  
      }else{
        this.directDiscount = 0; 
        //his.intDD = 0;
      }
      this.nettotalpremium = Math.round(this.premium2 + (this.ENQ_PolicyResponse.PurchaseResponse[1].Result_vector.Root.Policy.Parameters.IsFgEmployee == "True" ? ((this.premium*2)*15/85) : 0) + (this.premium*2)/20);
      let members = updatedItem[1].Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member;
      this.membersPremiumList = members;
      this.cardUID = updatedItem[1].UID;
    }else if(this.premium3Active){
      this.selectedPremium = new Premium(Math.round(this.premium3),this.taxpremium3,this.totalpremium3);
      this.selectedPremiumDetails = this.premiumDetails[2];
      this.discountedValue = Math.round((this.premium*3)*0.075);
       //this.discountedValue = ((Number(this.premium)*3)*0.075).toFixed(2);
       //this.intDiscountValue = parseInt(this.discountedValue);
      if(this.ENQ_PolicyResponse.PurchaseResponse[2].Result_vector.Root.Policy.Parameters.IsFgEmployee == "True"){
         this.directDiscount = Math.round((this.premium*3)*15/85);
          // this.directDiscount = ((Number(this.premium)*3)*15/85).toFixed(2); 
          // this.intDD = parseInt(this.directDiscount);
       }else{
        this.directDiscount = 0;   
        //this.intDD = 0;
       }
      this.nettotalpremium = Math.round(this.premium3 + (this.ENQ_PolicyResponse.PurchaseResponse[2].Result_vector.Root.Policy.Parameters.IsFgEmployee == "True" ? ((this.premium*3)*15/85) : 0) + (this.premium*3)*0.075);
      let members = updatedItem[2].Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member;
      this.membersPremiumList = members;
      this.cardUID = updatedItem[2].UID;
    }
    sessionStorage.selectedPremiumDetails = JSON.stringify(this.selectedPremiumDetails);
  }

  listClick(premium:number) {
    if(premium > 0){
      this.premium1Active = false;
      this.premium2Active = false;
      this.premium3Active = false;

      if(premium == this.totalpremium)
        this.premium1Active = true;
      if(premium == this.totalpremium2)
        this.premium2Active = true;
      if(premium == this.totalpremium3)
        this.premium3Active = true;

      this.saveSelectedPremium();
    }
  }

  closeBreakUPPopup(){
    this.showPopup = true;
  }

  closeModalPop(){
    // this.showPopup = false;  
    sessionStorage.isViewPopped = undefined;  
    // sessionStorage.ProposerDetails = "";
    // sessionStorage.planCardDetails = "";
     this.buyPopup = false;
     this.NameOfEmailsmsProposer = "";
     this.smsTo = [];
     this.emailTo = [];
     this.sendSMSList = [];
     this.sendEmailList = [];
     this.hidePopup();
   }

     
  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

   showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'OK',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }  
  } 

  proceedMemberDetailsPage(){
    if(this.NameOfEmailsmsProposer == undefined || this.NameOfEmailsmsProposer == null || this.NameOfEmailsmsProposer.trim() == ""){
      this.showToast("Please enter proposer name");
      this.myInput.setFocus();
    }else if(this.email== undefined || this.email == null || this.email.trim() == ""){
      this.showToast("Please enter email id");
      this.emailFocus.setFocus();
    }else if (!this.matchEmail(this.email)) {
      this.showToast("Please enter valid email id");  
      this.emailFocus.setFocus();    
    }else if (this.mobile == undefined || this.mobile == null || this.mobile.trim() == "") {      
      this.showToast("Please enter mobile number"); 
      this.mobileFocus.setFocus();                            
    }else if(this.mobile.length < 10){
      this.showToast("Please enter 10 digit mobile number");   
      this.mobileFocus.setFocus();         
    }else if(!this.isValidMobile(this.mobile)){
      this.showToast("Please enter valid mobile no");
      this.mobileFocus.setFocus();
    }else{ 
      console.log("SessionData: " + sessionStorage.ProposerDetails);
          
      if(sessionStorage.isViewPopped == "true"){
        console.log("click: " +JSON.stringify(this.memberDetailList));
        
        sessionStorage.ProposerDetails = JSON.stringify(this.memberDetailList);
        console.log("click: " + sessionStorage.ProposerDetails);
        // this.membersInsured[0].proposerName = this.NameOfEmailsmsProposer;
        // this.membersInsured[0].proposerEmail = this.email;
        // this.membersInsured[0].mobile = this.mobile;
      }
      
      
      sessionStorage.propName = this.NameOfEmailsmsProposer;
      sessionStorage.email = this.email;
      sessionStorage.mobile = this.mobile;
      sessionStorage.selectedPremiumDetails = JSON.stringify(this.selectedPremiumDetails); 
      this.buyPopup = false; 
      // this.memberDetailList[0].proposerName = this.NameOfEmailsmsProposer;
      // this.memberDetailList[0].email = this.email;
      // this.memberDetailList[0].mobile = this.mobile;
      console.log("member: " + JSON.stringify(this.memberDetailList));
      
      //this.navCtrl.push(VecproposerdetailsPage,{"buyPageMemberDetailsResult" : this.memberDetailList});    
      this.encryptIncompleteData(this.selectedPremiumDetails);            
      //this.navCtrl.push(BuyProposerDetailsPage,{"buyPageMemberDetailsResult": this.membersInsured,"buyPageCardDetailsResult":this.insuranceDetailsToSend});
    }    
    
  }


  encryptIncompleteData(InsuredCardDetails){      
    var sendData = this.commonProvider.encryptData(JSON.stringify(this.getProceedrequest(InsuredCardDetails)),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1Data(URL,serviceData){
    console.log("proceed: " + sessionStorage.TokenId);
    let headerString = this.commonProvider.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.commonProvider.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          console.log("test: "+ JSON.stringify(this.membersList));
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          console.log("Prop" + JSON.stringify(this.memberDetailList));
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          var member = this.memberDetailList.find(member => member.title === "Self");  
          var index = this.memberDetailList.indexOf(member);
          this.proposerItem ={"code":"SELF","SumInsured":"","title":"Proposer","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"","maritalCode":"","age":"","ageText":"","insuredCode":"","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"","insuredPlanTypeText":"","insuredAmtInDigit":"","Height":"","HeightText":"","Weight":"","ProposerDetailsDivShow":true,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"","NomineeRelation":"","NomineeAge":"","appointeeMember":"","AptRelWithominee":"","memberSelectedOnAddmember":true,"imgUrl":"","detailsSaved":true,"bmiLoading":"No","loadingPerc":0,"proposerName":"","mobile":"","email":"","occupationCode":"","Gender":"","genderText":"","address":"","pincode":"","state":"","city":""};
          if(index < 0){
            for(let i=0; i<this.memberDetailList.length; i++){
              if(this.memberDetailList[i].title == "Proposer"){
                break;
              }else{
                this.memberDetailList.splice(0, 0, this.proposerItem);
                break;
              }
            }
            
            
            console.log("Sour" + JSON.stringify(this.memberDetailList));
           
            //this.forQuickQuote = this.memberArray.length == 1 ? false : true;
          }else{
              this.memberDetailList[index].title = "Proposer";
              //this.forQuickQuote = this.memberArray.length == 1 ? false : true;
              this.memberDetailList.splice(index, 1);
              this.memberDetailList.splice(0, 0, member);
              //this.memberArray.splice(index,1);
          }
          this.memberDetailList[0].proposerName = this.NameOfEmailsmsProposer;
            this.memberDetailList[0].email = this.email;
            this.memberDetailList[0].mobile = this.mobile;

          console.log("Final" + JSON.stringify(this.memberDetailList));
          
          
          this.sendEMailSMS(); 
          //this.navCtrl.push(VecproposerdetailsPage, {"buyPageMemberDetailsResult" : this.memberDetailList});
          // console.log("card: "+ JSON.stringify(this.individualDisplayData));
          // console.log("member: "+ JSON.stringify(this.membersInsured));
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.loading.dismiss();
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.serviceResponsePopup = false;
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.serviceResponsePopup = false;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
        
    });      
  }


   sendEMailSMS(){    
    if(this.NameOfEmailsmsProposer.trim() == undefined || this.NameOfEmailsmsProposer.trim() == null || this.NameOfEmailsmsProposer.trim() == ""){
      this.showToast("Please enter proposer name");
      this.myInput.setFocus();
    }else if(this.email.trim()== undefined || this.email.trim() == null || this.email.trim() == ""){
      this.showToast("Please enter email id");
      this.emailFocus.setFocus();
    }else if (!this.matchEmail(this.email)) {
      this.showToast("Please enter valid email id");  
      this.emailFocus.setFocus();    
    }else if (this.mobile.trim() == undefined || this.mobile.trim() == null || this.mobile.trim() == "") {      
      this.showToast("Please enter mobile number"); 
      this.mobileFocus.setFocus();                            
    }else if(this.mobile.length < 10){
      this.showToast("Please enter 10 digit mobile number");   
      this.mobileFocus.setFocus();         
    }else if(!this.isValidMobile(this.mobile)){
      this.showToast("Please enter valid mobile no");
      this.mobileFocus.setFocus();
    }else{     
    sessionStorage.propName = this.NameOfEmailsmsProposer;
    sessionStorage.email = this.email;
    sessionStorage.mobile = this.mobile;
    this.emailTo[0] = this.email;

    var emailData = {"emailto":this.emailTo[0],
    "pname":this.NameOfEmailsmsProposer,
    "qid":this.QuotationID,
    "uid":this.cardUID}; 
    this.encryptEmailData(emailData);
    console.log(JSON.stringify(emailData));

  }    
  }

  sendSms(){ 

    this.smsTo = this.mobile;
    var smsData = {"mobile":this.smsTo,
    "pname":this.NameOfEmailsmsProposer,
    "qid":this.QuotationID,
    "uid":this.cardUID}; 
    this.encryptSmsData(smsData);
    console.log(JSON.stringify(smsData));
    }

  encryptEmailData(sendEmailData){ 
    var sendData = this.commonProvider.encryptData(JSON.stringify(sendEmailData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.getEmailResponseData({'request': sendData});
    }
    
    encryptSmsData(sendSmsData){
    var sendData = this.commonProvider.encryptData(JSON.stringify(sendSmsData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData)); 
    this.getSmsResponseData({'request' : sendData});
    }
    getEmailResponseData(serviceData){
      this.presentLoadingDefault();
    let headerString = this.commonProvider.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.commonProvider.callService("FGHealth.svc/SendQuotationByEmail",serviceData, headerString)
    .subscribe(SendQuotationByEmail =>{
      this.loading.dismiss();
    console.log(SendQuotationByEmail.SendQuotationByEmailResult);
    if(SendQuotationByEmail && SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "0"){
    //this.NameOfEmailProposerIndividual = "";
    //this.emailTo = [];
    //this.sendEmailList = [];
    sessionStorage.TokenId = SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
    console.log("emailToken: " + sessionStorage.TokenId);
    console.log(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg);
    this.sendSms();    
    }else if(SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "807"){
      //this.showToast(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg);
      sessionStorage.TokenId =SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
      this.serviceResponsePopup = false;
      this.message = SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg;
      this.serviceCodeStatus = 807;
      //this.navCtrl.push(LoginPage);
    }else if(SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "500"){
      //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
      sessionStorage.TokenId = SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
      this.serviceResponsePopup = false;
      this.message = SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg;
      this.serviceCodeStatus = 500;
      //this.navCtrl.push(LoginPage);
    }else{
      this.loading.dismiss();
      sessionStorage.TokenId = SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
      //this.showToast(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg); 
      this.serviceResponsePopup = false;
      this.message = SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg;
      this.serviceCodeStatus = 400;
    }
    }, (err) => {
    console.log(err);
    });
    }
    
    getSmsResponseData(serviceData){
    let headerString = this.commonProvider.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.commonProvider.callService("FGHealth.svc/SendQuotationBySMS",serviceData, headerString)
    .subscribe(SendQuotationBySMS =>{
    
      console.log(SendQuotationBySMS.SendQuotationBySMSResult);
    if(SendQuotationBySMS && SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "0"){
    sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
    //this.NameOfEmailsmsProposer = "";
    // this.smsTo = [];
    // this.emailTo = [];
    // this.email = "";
    // this.mobile = ""; 
    //this.buyPopup = false;       
    this.successLabel = false;
    console.log("smsToken: " + sessionStorage.TokenId);
    console.log(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg); 
    this.navCtrl.push(VecproposerdetailsPage, {"buyPageMemberDetailsResult" : this.memberDetailList});  
    //this.navCtrl.push(BuyProposerDetailsPage,{"buyPageMemberDetailsResult": this.membersInsured,"buyPageCardDetailsResult":this.insuranceDetailsToSend});
    }else if(SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "807"){
      //this.showToast(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
      sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
      this.message = SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg;
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 807;
      //this.navCtrl.push(LoginPage);
    }else if(SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "500"){
      //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
      sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
      this.message = SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg;
      //this.navCtrl.push(LoginPage);
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 500;
    }else{
      sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
      //this.showToast(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
      this.message = SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg;
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 400;
    }
    }, (err) => {
    console.log(err);
    });
    }


    onSaveAndEmail(){
      if(this.NameOfEmailsmsProposer.trim() == undefined || this.NameOfEmailsmsProposer.trim() == null || this.NameOfEmailsmsProposer.trim() == ""){
        this.showToast("Please enter proposer name");
        this.myInput.setFocus();
      }else if(this.email.trim() == undefined || this.email.trim() == null || this.email.trim() == ""){
        this.showToast("Please enter email id");
        this.emailFocus.setFocus();
      }else if (!this.matchEmail(this.email)) {
        this.showToast("Please enter valid email id");  
        this.emailFocus.setFocus();    
      }else if (this.mobile.trim() == undefined || this.mobile.trim() == null || this.mobile.trim() == "") {      
        this.showToast("Please enter mobile number"); 
        this.mobileFocus.setFocus();                            
      }else if(this.mobile.length < 10){
        this.showToast("Please enter 10 digit mobile number");   
        this.mobileFocus.setFocus();         
      }else if(!this.isValidMobile(this.mobile)){
        this.showToast("Please enter valid mobile no");
        this.mobileFocus.setFocus();
      }else{
        sessionStorage.propName = this.NameOfEmailsmsProposer;
        sessionStorage.email = this.email;
        sessionStorage.mobile = this.mobile;        
        this.buyPopup = false;
    
        this.saveEmailData(this.selectedPremiumDetails);  
        //this.navCtrl.push(BuyProposerDetailsPage,{"buyPageMemberDetailsResult": this.membersInsured,"buyPageCardDetailsResult":this.insuranceDetailsToSend});
      }    
    
  }

  saveEmailData(InsuredCardDetails){      
    var sendData = this.commonProvider.encryptData(JSON.stringify(this.getProceedrequest(InsuredCardDetails)),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("Proceed_Policytestt: " + JSON.stringify(sendData));
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.commonProvider.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.commonProvider.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
     
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){ 
          this.loading.dismiss();                
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();
          //console.log("card: "+ JSON.stringify(this.individualDisplayData));
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
    });
  }

  callEProposalService(){   
    var eProposalData = {
          "QuotationID":this.QuotationID,
          "UID": this.cardUID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        console.log("Token: " + JSON.stringify(sessionStorage.TokenId));
        var sendData = this.commonProvider.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }

  sendEProposerData(serviceData){
    this.presentLoadingDefault();
    let headerString = this.commonProvider.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.commonProvider.callService("FGHealth.svc/Vector_Eproposal",serviceData, headerString)
      .subscribe(response =>{
      
        var resp = response.Vector_EproposalResult;
        console.log("Eproposal:" + JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup = false;
          //this.closeModalPop();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp.ReturnCode == "807"){
          this.loading.dismiss();
          //this.showToast(resp.ReturnMsg);
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(resp.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = resp.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          //this.showToast(resp.ReturnMsg); 
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }        
       
      }, (err) => {
        this.loading.dismiss();
        //sessionStorage.TokenId = response.EproposalResult.UserToken.TokenId;
      console.log(err);
      });
  }


  getProceedrequest(TracsactionDetails){
    //this.QuotationID = TracsactionDetails.QuotationID;
    //his.UID = TracsactionDetails.UID;  
    console.log("test" + JSON.stringify(TracsactionDetails));
    
    let sendRequest = {
      // "QuotationID": localStorage.QuotationID,
      // "UID":localStorage.UID,
      "QuotationID": this.QuotationID,
      "UID": TracsactionDetails.UID,
      "stage":"1",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": TracsactionDetails.Result_vector.Root.Policy.Parameters.PolicyType ,//"HTF",
        "Duration": TracsactionDetails.Result_vector.Root.Policy.Parameters.Duration,
        "Installments": TracsactionDetails.Result_vector.Root.Policy.Parameters.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails":  TracsactionDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member.length == 1 ? this.getSelfBeneficiaryDetails(TracsactionDetails)  : this.getBeneficiaryDetails(TracsactionDetails),
        "MemberDetails": this.memberDetailList,
        "CardDetails": this.selectedPremiumDetails
      }
    }
    console.log("CRTrequest: "+JSON.stringify(sendRequest));
    return sendRequest;
  }
  
  
  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    //let mDob = cardDetails.Request.BeneficiaryDetails.Member.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberDetailList[0].Gender;
  
    BeneDetails.push({
      "MemberId": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].MemberId ,
      "InsuredName": this.memberDetailList[0].proposerName,
      "InsuredDob": "",
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberDetailList[0].occupationCode,
      "CoverType": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].CoverType,
      "SumInsured": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].SumInsured,
      "DeductibleDiscount": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].DeductibleDiscount,
      "Relation": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[0].Relation,
      "NomineeName": "",
      "NomineeRelation": "",
      "AnualIncome": "",
      "Height": "",
      "Weight": "",
      "NomineeAge": "",
      "AppointeeName": "",
      "AptRelWithominee": "",
      "MedicalLoading": "",
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      }
    })
    return BeneDetails;
  }
  
  
  getClientNode(){
    //let age = this.membersList[0].age.split("-");
    let clientSalution = "MR";
    if(this.membersList[0].Gender == "F"){
      clientSalution = "MRS";
      this.spouseGender = "M";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.NameOfEmailsmsProposer ,
      "LastName": "",
      "DOB": "",
      "Gender":"M",
      "MaritalStatus": this.memberDetailList[0].maritalCode,
      "Occupation": this.memberDetailList[0].occupationCode,
      "PANNo": this.memberDetailList[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberDetailList[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetailList[0].pincode,
        "City": this.memberDetailList[0].city,
        "State": this.memberDetailList[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.mobile,
        "EmailAddr": this.email
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetailList[0].pincode,
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.mobile,
        "EmailAddr": this.email
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  
  }
  
  getBeneficiaryDetails(cardDetails){
    console.log("BuyPageResult: " + JSON.stringify(cardDetails));
    console.log("memberArray: " + JSON.stringify(this.memberDetailList));
    //let tempArray = this.membersInsured;
    //let tempCardArray = cardDetails.Request.BeneficiaryDetails.Member;
    //console.log("tempMember: " + JSON.stringify(tempArray));
    
    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberDetailList.length;j++){
        if(i==j){
          if( this.memberDetailList[j].code== (cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation) ){
            //let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ");
            let gendr = "M";
            if(this.memberDetailList[j].title.toLowerCase().indexOf("mother") >=0 || this.memberDetailList[j].title.toLowerCase().indexOf("daughter") >=0){
              gendr = "F";
            }else if(this.memberDetailList[j].title.toLowerCase() == "spouse"){
              gendr = this.spouseGender;
            }else if(this.memberDetailList[j].title.toLowerCase() == "self"){
              gendr = "M";
            }
            BeneDetails.push({
              "MemberId": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].MemberId ,
              "InsuredName": this.memberDetailList[j].proposerName,
              "InsuredDob": "",
              "InsuredGender": gendr,
              "InsuredOccpn": this.memberDetailList[j].occupationCode,
              "CoverType": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].CoverType,
              "SumInsured": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].SumInsured,
              "DeductibleDiscount": "",
              "Relation": cardDetails.Result_vector.Root.Policy.Parameters.BeneficiaryDetails.Member[i].Relation,
              "NomineeName": "",
              "NomineeRelation": "",
              "AnualIncome": "",
              "Height": "",
              "Weight": "",
              "NomineeAge": "",
              "AppointeeName": "",
              "AptRelWithominee": "",
              "MedicalLoading": "",
              "PreExstDisease": "N",
              "DiseaseMedicalHistoryList": {
                "DiseaseMedicalHistory": {
                  "PreExistingDiseaseCode": "",
                  "MedicalHistoryDetail": ""
                }
              }
            })
           // tempArray.splice(j, 1);
            //tempCardArray.splice(j, 1);
            
          }
        }
   
       
      }
  
    }
    return BeneDetails;
  
  } 

  toTitleCase(str:String) {
    return str.replace(/\w\S*/g,
    function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
    );
    }


  matchEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  isValidMobile(mobile){
    var regex = /^[6-9][0-9]{9}$/;
    return regex.test(mobile);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  }

  // validateSelfInsuredFirst(){
  //   if(this.selfinsurance.suminsured <= 0)
  //     this.commonProvider.showToast("Please insure 'Self' first before choosing other member/s.");
  // }


}

export class Insurance {
  suminsured:number = 0;
  premium:number = 0;
  insureType:String = "";

  constructor(suminsured:number, premium:number, insureType:String){
    this.suminsured = suminsured;
    this.premium = premium;
    this.insureType = insureType;
  }
}

export class Members {

  MemberId : String =  '';
  InsuredName : String =  '';
  InsuredDob : String =  '2015-03-12';
  InsuredGender : String =  'F';
  InsuredOccpn : String =  '';
  CoverType : String =  'VITAL';
  SumInsured : String =  '0';
  DeductibleDiscount : String =  '0';
  Relation : String =  '';
  NomineeName : String =  '';
  NomineeRelation : String =  '';
  AnualIncome : String =  '10000';
  Height : String =  '160';
  Weight : String =  '60';
  NomineeAge : String =  '';
  AppointeeName : String =  '';
  AptRelWithominee : String =  '';
  MedicalLoading : String =  '0';
  PreExstDisease : String =  'N';
  DiseaseMedicalHistoryList = new DiseaseMedicalHistory();

  constructor(Relation : String, SumInsured : String){
    this.Relation = Relation;
    this.SumInsured = SumInsured;
  }

}

export class DiseaseMedicalHistory {
    PreExistingDiseaseCode : '';
    MedicalHistoryDetail : ''
}

export class Premium{
  UID
  totalSumInsured:number = 0;
  netPremium:number = 0;
  gstPremium:number = 0;
  totalpremiumwithtax:number = 0;

  constructor(netPremium:number, gstPremium:number, totalpremiumwithtax:number){
    this.netPremium = netPremium;
    this.gstPremium = gstPremium;
    this.totalpremiumwithtax = totalpremiumwithtax;
  }
}


//////////pojo object
export class MemberDetails{
  code:String = "";
  SumInsured : String = '0';
  title:String = "";
  smoking:boolean = false;
  smokingText:String = "";
  popupType:String = "1";
  showMarried:boolean = true;
  maritalStatus:String = "";
  maritalCode:String = "";
  age:String = "";
  ageText:String = "";
  insuredCode:String = "";
  showDependent:boolean = false;
  showAddAnotherBtn:boolean = false;
  PolicyType:String = "";
  showFamilyMemberOnScreen:boolean = true;
  disableRemove:boolean = true;
  ShowInsurenceAmt:boolean = false;
  insuredPlanTypeCode:String = "";
  insuredPlanTypeText:String = "";
  insuredAmtInDigit:String = "";
  Height:String = "";
  HeightText:String = "";
  Weight:String = "";
  ProposerDetailsDivShow:boolean = true;
  medicalDeclaration:boolean = false;
  showMedicalQuestions:boolean = true;
  nominee:String = "";
  NomineeRelation:String = "";
  NomineeAge:String = "";
  appointeeMember:String = "";
  AptRelWithominee:String = "";
  memberSelectedOnAddmember:boolean = true;
  imgUrl:String = "";
  detailsSaved:boolean = true;
  bmiLoading:String = "No";
  loadingPerc:number = 0;
  proposerName:String = "";
  mobile:String = "";
  email:String = "";
  occupationCode:String = "";
  Gender:String = "";
  genderText: String="";
  address:String="";
  pincode:String="";
  state:String="";
  city:String="";

  constructor(code:String, SumInsured : String){
  this.code = code;
  this.SumInsured = SumInsured;
  }
  }



