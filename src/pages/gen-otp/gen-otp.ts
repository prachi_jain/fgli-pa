import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController ,AlertController} from 'ionic-angular';
import { AppService } from '../../providers/app-service/app-service';
import { ForgotpasswordPage } from '../forgotpassword/forgotpassword';
import { first } from 'rxjs/operator/first';
import { last } from 'rxjs/operator/last';

/**
 * Generated class for the GenOtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gen-otp',
  templateUrl: 'gen-otp.html',
})
export class GenOtpPage {
  otpOne;
  otpTwo;
  otpThree;
  otpFour;
  loading;
  canShowToast = true;
  userName;
  firstThree;
  lastThree;
  mobile;
  @ViewChild('passcode1') passcode1;
  @ViewChild('passcode2') passcode2;
  @ViewChild('passcode3') passcode3;
  @ViewChild('passcode4') passcode4;
  values:any=[];
  otpValues:any=[];
  constructor(public navCtrl: NavController,  public toast: ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, public appService: AppService,private alertCtrl:AlertController) {
    this.userName = navParams.get("userName");
    this.mobile = navParams.get("mobile");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GenOtpPage');
    setTimeout(function() {         
      document.getElementById("splash").style.display='none';                 
  }, 7000);
    this.getMobiledigits();
  }
  
  onKeyUp(event,index){  
    // console.log(event.key,index);
   
   
    if(event.target.value.length !=1){
      this.setFocus(index-2);  
    }else{
    
      this.values.push(event.target.value);  
      
      console.log(this.values)
      this.setFocus(index);   
    }
    event.stopPropagation();
  }


  
setFocus(index){
   
   switch(index){
     case 0:
     this.passcode1.setFocus();
     break;
     case 1:
     this.passcode2.setFocus();
     break;
     case 2:
     this.passcode3.setFocus();
     break;
     case 3:
     this.passcode4.setFocus();
     break;
    
     }
}
  

  
  getMobiledigits(){
    let proposerMobile = this.mobile;
    this.firstThree = proposerMobile.substring(0, 3);
    this.lastThree = proposerMobile.substring(7);
    console.log("mob3"+this.firstThree);
    
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

showToast(Message){
  if(this.canShowToast){
    let pageToast = this.toast.create({
      message:Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 5000,
    });
    this.canShowToast = false;
    pageToast.present();
    pageToast.onDidDismiss(() => {
      this.canShowToast = true;
    });
  }   
}

  verifyOtp(){
   
    
    if(this.otpOne == undefined || this.otpOne == "" || this.otpTwo == undefined || this.otpTwo == "" || this.otpThree == undefined || this.otpThree == "" || this.otpFour == undefined || this.otpFour == ""){
      this.showToast("Enter OTP.");
    }else{
      this.verifyOtpService();
      this.otpValues = [];
      this.values=[];
      this.passcode1.value="";
      this.passcode2.value="";
      this.passcode3.value="";
      this.passcode4.value="";
    
    } 
  }

  verifyOtpService(){
   this.otpOne = this.otpValues[0]
   this.otpTwo = this.otpValues[1]
   this.otpOne = this.otpValues[2]
   this.otpFour = this.otpValues[3]

   
    var sendData = {
      'username' : this.userName,
      'OTP' : this.passcode1.value + this.passcode2.value + this.passcode3.value+ this.passcode4.value     
    }

    console.log("sendData: " + JSON.stringify(sendData));
    var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),'');
    console.log("encrypt" + encryptSendData);
    this.callVerifyOTPService({"request" : encryptSendData}); 
  }

  callVerifyOTPService(sendData){
    this.presentLoadingDefault();         
    
    //let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    //console.log("header: " + JSON.stringify(headerString));
    this.appService.callService("Authenticate.svc/ValidateOTPForgotPassword",sendData,'')
    .subscribe(ValidateOTPForgotPassword =>{
     this.loading.dismiss();
     console.log("PreLoginData :" + JSON.stringify(ValidateOTPForgotPassword));

     if(ValidateOTPForgotPassword.ValidateOTPForgotPasswordResult.ReturnCode == "0"){
       sessionStorage.TokenId = ValidateOTPForgotPassword.ValidateOTPForgotPasswordResult.UserToken.TokenId;
       this.navCtrl.push(ForgotpasswordPage, {"userName" : this.userName});
     }else if(ValidateOTPForgotPassword.ValidateOTPForgotPasswordResult.ReturnCode == "2"){
       //this.showToast(ValidateOTPForgotPassword.ValidateOTPForgotPasswordResult.ReturnMsg);
       this.showToast("Invalid OTP.");
     }
        
    });
  }

  resendOTP(){
    var sendData = {
      'username' : this.userName,     
    }
  
    console.log("sendData: " + JSON.stringify(sendData));
    var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),'');
    console.log("encrypt" + encryptSendData);
    this.callServiceResendOTP({"request" : encryptSendData});
  }

  callServiceResendOTP(sendData){

    this.presentLoadingDefault();         
    
    //let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    //console.log("header: " + JSON.stringify(headerString));
    this.appService.callService("Authenticate.svc/GenerateOTPForgotPassword",sendData,'')
    .subscribe(GenerateOTPForgotPassword =>{
     this.loading.dismiss();
     this.otpOne = "";
     this.otpTwo = "";
     this.otpThree = "";
     this.otpFour = "";
     console.log("PreLoginData :" + JSON.stringify(GenerateOTPForgotPassword));

     if(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnCode == "0"){
       sessionStorage.TokenId = GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.UserToken.TokenId;
       this.showToast("OTP has been sent successfully.");
       //this.navCtrl.push(GenOtpPage, {"userName" : this.userName});
     }else if(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnCode == "807"){
       this.showToast(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnMsg);
     }else if(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnCode == "500"){
      this.showToast(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnMsg);
     }
        
    });
  }

  moveFocus(nextElement) {
    nextElement.setFocus();
  }

  moveForward(previousElement){
    previousElement.setFocus();
  }

}
