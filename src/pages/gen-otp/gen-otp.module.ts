import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GenOtpPage } from './gen-otp';

@NgModule({
  declarations: [
    //GenOtpPage,
  ],
  imports: [
    IonicPageModule.forChild(GenOtpPage),
  ],
})
export class GenOtpPageModule {}
