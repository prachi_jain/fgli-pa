import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BuyPage } from '../health/buy/buy';
import { VecquickquotePage } from '../vector/vecquickquote/vecquickquote';
import { QuickQuotePage } from '../health/quick-quote/quick-quote';
import { QuickquoteOfflinePage } from '../vector/quickquote-offline/quickquote-offline';
import { TbuyPage } from '../topup/tbuy/tbuy';
import { TplandingPage } from '../topup/tplanding/tplanding';
import { TplandingofflinePage } from '../topup/tplandingoffline/tplandingoffline';

/**
 * Generated class for the ProductlistingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productlisting',
  templateUrl: 'productlisting.html',
})
export class ProductlistingPage {

  page;
  //divHideShow;
  policyStatus = "firsttime";
  loginStatus;
  showButton = true;
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.page = navParams.get("page");
    console.log("pagName" + this.page);
    this.policyStatus = navParams.get("policyStatus");
    this.loginStatus = navParams.get("loginStatus");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductlistingPage');
    	setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);
      console.log("load" + sessionStorage.TokenId);
      if(sessionStorage.pagename == "Login"){
        this.showButton = false;
      }

      if(this.policyStatus == undefined){
        this.policyStatus = "firsttime";
      }

      // if(sessionStorage.pagename == "Login"){
      //   this.divHideShow = true;
      // }else{
      //   this.divHideShow = false;
      // }
  }

  clickHealthTotal(){
    console.log("nav:" + sessionStorage.pagename);

    if(sessionStorage.pagename == "Login" && this.policyStatus == "online"){
      this.navCtrl.push(BuyPage);
    }else if(sessionStorage.pagename == "Login"  && this.policyStatus == "firsttime"){
      this.navCtrl.push(BuyPage);
    }else{
      this.navCtrl.push(QuickQuotePage);
    }  

    // if(this.policyStatus == "online" && this.loginStatus == true){
    //   this.navCtrl.push(BuyPage);
    // }else{
    //   this.navCtrl.push(QuickQuotePage);
    // }
  }

  clickHealthVector(){
    console.log("nav:" + sessionStorage.pagename);
    if(sessionStorage.pagename == "Login"  && this.policyStatus == "online"){
      this.navCtrl.push(VecquickquotePage);
    }else if(sessionStorage.pagename == "Login"  && this.policyStatus == "firsttime"){
      this.navCtrl.push(VecquickquotePage);
    }else{
      this.navCtrl.push(QuickquoteOfflinePage)
    }    
  }

  clickSuperTop(){
    if(sessionStorage.pagename == "Login" && this.policyStatus == "online"){
      this.navCtrl.push(TplandingPage);
    }else if(sessionStorage.pagename == "Login"  && this.policyStatus == "firsttime"){
      this.navCtrl.push(TplandingPage);
    }else{
      this.navCtrl.push(TplandingofflinePage);
    }  
  }

}
