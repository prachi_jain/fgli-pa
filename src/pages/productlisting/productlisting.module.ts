import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductlistingPage } from './productlisting';

@NgModule({
  declarations: [
    //ProductlistingPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductlistingPage),
  ],
})
export class ProductlistingPageModule {}
