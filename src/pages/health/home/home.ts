import { Component } from '@angular/core';
import { Platform, NavController, IonicPage, NavParams } from 'ionic-angular';
import { LoginPage } from './../login/login';
import { DashboardPage } from '../dashboard/dashboard';
import { AppService } from '../../../providers/app-service/app-service';

import { Network } from '@ionic-native/network';
import { ConnectableObservable } from 'rxjs/observable/ConnectableObservable';
import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public online: boolean = false;


  constructor(public platform: Platform, private device: Device, public appService: AppService, public navCtrl: NavController, private network: Network) {
    if (!localStorage.Appid) {
      localStorage.Appid = '';
    }
  }

  ionViewDidLoad() {

    console.log('Device UUID is: ' + this.device.uuid);
    console.log('Device UUID is: ' + this.device.model);
    console.log('Device UUID is: ' + this.device.platform);
    console.log('Device UUID is: ' + this.device.version);
    console.log('Device UUID is: ' + this.device.manufacturer);
    console.log('Device UUID is: ' + this.device.isVirtual);
    console.log('Device UUID is: ' + this.device.serial);
    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 7000);


    if (this.network.type != "none") {
      //this.appService.presentServiceLoading();
      this.encryptAppIdData();
    } else if (localStorage.AppData) {
      this.navCtrl.push(LoginPage, {});
    }
  }
  encryptAppIdData() {
    var authenticate = { 'appid': localStorage.Appid, 'device': this.device.uuid, 'os': this.device.platform, 'fireware': '' };
    console.log("BootStrap" + JSON.stringify(authenticate));

    var sendData = this.appService.encryptData(JSON.stringify(authenticate), localStorage.TokenID);
    this.getAppID({ 'request': sendData });
    console.log("sendData: " + JSON.stringify(sendData));
  }

  getAppID(serviceData) {

    this.appService.callService("FGHealth.svc/Bootstrap", serviceData, '')
      .subscribe(Bootstrap => {
        console.log("test: " + JSON.stringify(Bootstrap));
        // this.appService.showloading.dismiss();
        if (Bootstrap && Bootstrap.BootstrapResult.ReturnCode == "0") {
          localStorage.AppID = Bootstrap.BootstrapResult.Data.appid;
          localStorage.DataUpdateDate = (new Date((Bootstrap.BootstrapResult.Data.lastUpdatedOn) * 1000)).toLocaleString()
          localStorage.AppData = JSON.stringify(Bootstrap.BootstrapResult.Data);
          localStorage.BankData = JSON.stringify(Bootstrap.BootstrapResult.Data.BankDetails);
          localStorage.ProductData = JSON.stringify(Bootstrap.BootstrapResult.Data.Masters.ProductMaster);
          localStorage.superTopUpDeductableData = JSON.stringify(Bootstrap.BootstrapResult.Data.stu_data.Deductibles);
          localStorage.STUDeductData = JSON.stringify(Bootstrap.BootstrapResult.Data.stu_premium.Deductibles);
          localStorage.STUChildData = JSON.stringify(Bootstrap.BootstrapResult.Data.stu_premium.FFactorChild);
          localStorage.STUSpouseData = JSON.stringify(Bootstrap.BootstrapResult.Data.stu_premium.FFactorChild);
           console.log("product: " + localStorage.ProductData);
          // console.log("SuperTop: " + localStorage.superTopUpDeductableData);

          sessionStorage.posVerification = Bootstrap.BootstrapResult.Data.posVerification;
        }
        this.navCtrl.push(LoginPage, { versionControl: Bootstrap.BootstrapResult.Data.versionControl });
      }, (err) => {
        if (localStorage.AppData) {
          //this.appService.showloading.dismiss();
          this.navCtrl.push(LoginPage, {});

        }
      });
  }


  // getCounter(){


  // }

  // runTimer(){
  //   setTimeout(() => {

  //   this.time = -- this.time;
  //   }, 1000);

  //   console.log("test" + this.time);
  // }


}
