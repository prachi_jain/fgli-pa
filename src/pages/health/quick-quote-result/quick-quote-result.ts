import { Component, ViewChild,Input,ElementRef,NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams,App,Nav } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import {Network} from '@ionic-native/network';

import { LoginPage } from '../login/login';
import { QuickQuotePage } from '../quick-quote/quick-quote';
import { LandingScreenPage } from '../landing-screen/landing-screen';

/**
 * Generated class for the QuickQuoteResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    QuickQuoteResultPage,
  ],
  imports: [
    //IonicPageModule.forChild(QuickQuoteResultPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-quick-quote-result',
  templateUrl: 'quick-quote-result.html',
})
export class QuickQuoteResultPage {
  @ViewChild('barCanvas') barCanvas;

  @ViewChild("employeeIdInput") userInput ;
  @ViewChild("passwordInput") passwordInput ;
  //barChart: any;
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    title: {
      display: true,
      text: 'Yearly Comparison of Premium'
    },
    legend: {position: 'bottom',labels: { boxWidth: 15, boxHeight: 2 }},
    scales: {
      xAxes: [{ stacked: true, barPercentage: 0.4 }],
      yAxes: [{ stacked: true, barPercentage: 1.3, scaleLabel:{display: true, labelString: 'Total Premium'}}]
    }
  };

  barChartLabels:string[];
  barChartLabelsFamily: string[];
  barChartAmountFamily: string[];
  barChartSavingFamily: string[];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  public barChartData:any;

  public barChartColors:Array<any> = [
    {
      backgroundColor: '#C70039',
      borderColor: '#C70039',
      pointBackgroundColor: '#C70039',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#C70039'
    },
    {
      backgroundColor: '#FF5733',
      borderColor: '#FF5733',
      pointBackgroundColor: '#FF5733',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#FF5733'
    },
  ];

  
  // events
  // public chartClicked(e:any):void {
  //   console.log(e);
  // }
  
  // public chartHovered(e:any):void {
  //   console.log(e);
  // }
  
  // public randomize():void {
  //   // Only Change 3 values
  //   let data = [
  //     Math.round(Math.random() * 100),
  //     59,
  //     80,
  //     (Math.random() * 100),
  //     56,
  //     (Math.random() * 100),
  //     40];
  //   let clone = JSON.parse(JSON.stringify(this.barChartData));
  //   clone[0].data = data;
  //   console.log(data);
  //   this.barChartData = clone;
  //   /**
  //    * (My guess), for Angular to recognize the change in the dataset
  //    * it has to change the dataset variable directly,
  //    * so one way around it, is to clone the data, change it and then
  //    * assign it;
  //    */
  // }
  NameOfEmailProposerIndividual:any;
  NameOfSMSProposerIndividual:any;
  sendEmailList = [{"value":""}];
  emailTo=[];
  sendSMSList = [{"value":""}];
  smsTo=[];
  selectionCard:boolean;
  delIcon:boolean = true;
  floaterDiv:boolean = true;
  individualDiv:boolean = true;
  graph:boolean = true;
  card:boolean = false;
  closeBtn:boolean = false;
  panelOpenState: boolean = false;
  memberArray:any;
  tempMemberArray : any;
  appData;
  premiums:any;
  basevalue:any;
  smokingState:boolean;
  indiviDiscount:any;
  policyType:any = "individual";
  planType: string = "";
  total:any;
  finalTotal:any = 0;
  termOfTwoAmount:any;
  termOfTwoDiscountAmount:any;
  termOfThreeAmount:any;
  termOfThreeDiscountAmount:any;
  twoYearSaveAmount:any;
  threeYearSaveAmount:any;
  oneTimecards:any;
  monthlyCards:any;
  quarterlyCards:any;
  halfyearlyCards:any;
  monthlyOneYearAmount:any;
  monthlyTwoYearAmount:any;
  monthlyThreeYearAmount:any;
  quarterlyOneYearAmount:any;
  quarterlyTwoYearAmount:any;
  quarterlyThreeYearAmount:any;
  halfyearlyOneYearAmount:any;
  halfyearlyTwoYearAmount:any;
  halfyearlyThreeYearAmount:any;
  frequencyPercArray:any;
  monthlyPerc:any;
  quarterlyPerc:any;
  halfyearlyPerc:any;
  siAmountArray:any;
  discountPercArray:any;
  oneyearDiscountPerc:any;
  twoyearDiscountPerc:any;
  threeyearDiscountPerc:any;
  policyterm:any;
  policyTermOneYear:any;
  policyTermTwoYear:any;
  policyTermThreeYear:any;
  oneMonthCount:any;
  twoMonthCount:any;
  threeMonthCount:any;
  shownGroup:any;
  show:boolean;
  slide="";
  hideOpDisc:boolean = false;
  activeMemberCode:any;
  activememberName:any;
  forQuickQuote = false;
  insurenceAmtToShow:any;
  maximumInsuAmtCode:any;
  insuredType = false;
  threeYearFltCards= [];
  individualGetQuoteArray = [];
  petFamily = "OneYears";
  graphShow:boolean = false;
  showYearValue:boolean = true;
  pet:string = "OneTime";
  openEdit = true;
  closeEdit = false;
  familyInsurence = '';
  familyGraph = false;
  installments : any;
  installTyp;
  NumYear = 0;
  hideOneYear = false;
  yrDiscount= "";
  familySumInsured = '';
  vitalMembers=["SELF","SPOU","SON","DAUG","FATH","MOTH","CHLD"];
  loginStatus;
  amountList;
  voluntaryPopup = true;
  amountDiscount;
  amount = "";
  voluntarSubmitClick;
  showButton = true;


  constructor(public navCtrl: NavController, public  app: App, private network: Network, public navParams: NavParams, public appService: AppService) {
    this.memberArray = JSON.parse(sessionStorage.quickQuoteMembersDetails);
    this.tempMemberArray = JSON.parse(sessionStorage.quickQuoteMembersDetails);
    this.forQuickQuote = this.memberArray[0].forQuickQuote;
    this.appData = JSON.parse(localStorage.AppData);
    console.log("offline: " + JSON.stringify(this.appData));
    
    this.premiums = this.appData.Premiums; 
    this.installments = this.appData.Masters.Installments;
    //this.loadDisplayMemberData();  
    console.log("quickMember:"+ JSON.stringify(this.memberArray));
    
    
  }

  ionViewDidLoad() {

    this.loginStatus = this.navParams.get("loginStatus");
    console.log("quoteResult: " + this.loginStatus);

    if(sessionStorage.pagename == "Login"){
      this.showButton = false;
    }
    
    this.maximumInsuAmtCode = this.memberArray[0].insuredCode;

    console.log(this.maximumInsuAmtCode+"this.maximumInsuAmtCode");
    console.log( this.memberArray[0].PolicyType+"PolicyType");
    this.loadDisplayMemberData(); 
    this.installTyp = this.installments[0].frequencyCode;
  }
  onSelectChange(selectedValue: any) {
    console.log('Selected', selectedValue);
    console.log(this.installTyp);
    
    if(selectedValue == 'OT'){
      this.petFamily = "OneYears";
      console.log("1");
      this.hideOneYear = false;
      this.showSumInByYear(1, selectedValue);
    }else{
      this.hideOpDisc = true;
      this.petFamily = "TwoYears";
      console.log("2");
      this.hideOneYear = true;
      this.showSumInByYear(2, selectedValue);
    }
  }

  loadDisplayMemberData(){
    this.oneTimecards = this.appData.Masters.PolicyTerm;
    this.monthlyCards = this.appData.Masters.PolicyTerm;
    this.quarterlyCards = this.appData.Masters.PolicyTerm;
    this.halfyearlyCards = this.appData.Masters.PolicyTerm;
    
    //this.finalTotal = 0;
    console.log(this.memberArray);
    if(this.memberArray[0].PolicyType == "HTI"){
      this.insuredType = false;
    }else{
      this.insuredType = true;
      this.openEdit = false;
    }
    this.siAmountArray = this.appService.getSumInsuredArray(this.appData.Masters.PlanType);

    if(this.memberArray[0].PolicyType == "HTI"){
      this.individualDiv = this.individualDiv;
      this.floaterDiv = !this.floaterDiv;
      let total = this.getTotalMemberArraySI();
      this.getPolicyTermOneTimeDisplayAmount(total);
  
      

    }else{
      // this.individualDiv = !this.individualDiv
      // this.floaterDiv = this.floaterDiv;
      // this.show = !this.show;
      this.individualDiv = false;
      this.setSIAmountValue();
      this.insuredType = true;
      this.familyInsurence = this.memberArray[0].insuredCode;
      this.changeInsurence(this.memberArray[0].insuredCode);
     /*  this.individualDiv = !this.individualDiv
      this.floaterDiv = this.floaterDiv;
      this.planType = "vital";
      
      this.showSumInByYear(); */
    }
  }

  getTotalMemberArraySI(){
    this.finalTotal = 0;
    console.log(JSON.stringify(this.memberArray));
    for(let i=0; i < this.memberArray.length; i++){     
      this.smokingState = this.memberArray[i].smoking;
      this.slide = this.memberArray[i].insuredCode;
      let smokingLoad = this.appData.Masters.Loading.smokingload[0].perc;
      let indiviDiscount = this.appData.Masters.Discount.indifloaterdisc[0].perc;

      for(let j = 0; j < this.premiums.length; j++){
        if(this.premiums[j].agecode == this.memberArray[i].age && this.premiums[j].sicode == this.slide){
          this.basevalue = this.premiums[j].rate;
          break;
        }
      }

      this.memberArray.length == 1 ? indiviDiscount = 0 : indiviDiscount;


      this.total = this.appService.getMemberCalculationAmount(this.basevalue, this.smokingState, smokingLoad, indiviDiscount);   
      console.log(this.total);
      this.finalTotal += this.total;
      console.log(JSON.stringify(this.finalTotal));
      }
      console.log("latest memberAmount " + this.total);
      return this.finalTotal;
  }


  addMember(){
    sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.memberArray);
    this.navCtrl.push(QuickQuotePage,{fromQuickQuoteResult : true});
  }
  closeQuickQuote(){
    this.navCtrl.push(LoginPage);
  }

  toggleGroup(group){
    this.slide = group.insuredCode;
    this.activeMemberCode = group.insuredCode;
    this.activememberName = group.title;
    if(this.activememberName == "Self"){      
      this.maximumInsuAmtCode = this.slide;
      this.setSIAmountValue();
    }else{
      this.maximumInsuAmtCode = ""; 
      this.setSIAmountValue();
    }    
  }

  selectedMemberValue(amount){ 
    this.slide =  amount.sicode ;     
    if(this.activememberName == "Self"){      
      this.maximumInsuAmtCode = this.slide;
    }else{
      this.maximumInsuAmtCode = ""; 
    }

   for(let m=0; m< this.tempMemberArray.length; m++){
     if(this.activememberName == this.memberArray[m].title ){
      this.tempMemberArray[m].insuredCode = amount.sicode;
        this.tempMemberArray[m].insuredAmtText = amount.amtTitle;
     }
   }
  }

  showCard(){  
     if(this.pet == "OneTime"){
      this.graph = true;
      this.card = false;
      this.graphShow = false;
      this.showYearValue = true;
      this.getPolicyTermOneTimeDisplayAmount(this.finalTotal);        
     }else if(this.pet == "Monthly"){
      this.graph = true;
      this.card = false;
      this.graphShow = false;
      this.showYearValue = true;
      this.getPolicyTermMonthlyDisplayAmount();
     }else if(this.pet == "Quarterly"){
      this.graph = true;
      this.card = false;
      this.graphShow = false;
      this.showYearValue = true;
      this.getPolicyTermQuarterlyDisplayAmount
     }else if(this.pet == "HalfYearly"){
      this.graph = true;
      this.card = false;
      this.graphShow = false;
      this.showYearValue = true;
      this.getPolicyTermHalfyearlyDisplayAmount();
     }
   }
   
  setSIAmountValue(){
    this.insurenceAmtToShow = [];
    if(this.maximumInsuAmtCode == '' && this.activememberName != 'Self'){
      this.maximumInsuAmtCode = this.tempMemberArray[0].insuredCode;
    }else if(this.activememberName == 'Self'){
      this.maximumInsuAmtCode = '';
    }
    for(let i = 0; i < this.siAmountArray.length;i++){
      if(this.maximumInsuAmtCode!='' && this.siAmountArray[i].sicode == this.maximumInsuAmtCode){
        this.insurenceAmtToShow.push(this.siAmountArray[i]);
        break;
      }else{
        this.insurenceAmtToShow.push(this.siAmountArray[i]);
      }
    }
  }


  calculateYearAmount(){
    this.policyterm = this.appData.Masters.PolicyTerm;
    for(let p=0; p<this.policyterm.length; p++){
        if(this.policyterm[p].ptTitle == "1 Year"){
          this.policyTermOneYear = this.policyterm[p].ptcode;
        }else if(this.policyterm[p].ptTitle == "2 Year"){
          this.policyTermTwoYear = this.policyterm[p].ptcode;
        }else if(this.policyterm[p].ptTitle == "3 Year"){
          this.policyTermThreeYear = this.policyterm[p].ptcode;
      }
    }
   
    
    console.log("GM : "+this.memberArray[0].PolicyType);
    if(this.memberArray[0].PolicyType == "HTI"){
      this.oneTimecards = this.appData.Masters.PolicyTerm;
      this.monthlyCards = this.appData.Masters.PolicyTerm;
      this.quarterlyCards = this.appData.Masters.PolicyTerm;
      this.halfyearlyCards = this.appData.Masters.PolicyTerm;
      

    }
  }

  changeInsurence(sicodesicode){
    for(let i = 0; i < this.siAmountArray.length;i++){
      if(this.siAmountArray[i].sicode == sicodesicode ){
       
        this.changePlan(this.siAmountArray[i].planIndex,this.siAmountArray[i].amtTitle);
        break;
      }
    }
  }
  changePlan(Type,amtText){
    if(Type == 0){
      for(let i =0; i < this.memberArray.length;i++){
        if(this.vitalMembers.indexOf(this.memberArray[i].code)<0 || this.memberArray.length>6){
          console.log("Vital Plan includes a maximum of 6 immediate family members.");
          console.log(this.vitalMembers.indexOf(this.memberArray[i].code), this.memberArray.length);
          break;
        }
        if(i == (this.memberArray.length-1)){
          this.planType = "vital";
          this.petFamily = "OneYears";
          this.showSumInByYear(1, "");
          this.toggleCustomize();
          this.familySumInsured = amtText;
        }
      }
    }else if(Type == 1){
      this.planType = "superior";
      this.petFamily = "OneYears";
      this.showSumInByYear(1,"");
      this.toggleCustomize();
      this.familySumInsured = amtText;
    }else if(Type == 2){
      this.planType = "premier";
      this.petFamily = "OneYears";
      this.showSumInByYear(1,"");
      this.toggleCustomize();
      this.familySumInsured = amtText;
    }
  }
  
  showNumberOfYear(years){
    if(years == 3){
      this.petFamily = "ThreeYears";
    }else if(years == 2){
      this.petFamily =  "TwoYears";
    }else if(years == 1){
      this.petFamily =  "OneYears" ;
    }
  }

  showSumInByYear(years, installmentType){
    console.log("year" + years);
    console.log("frequency" + this.installTyp);
    console.log("type:" + installmentType);
    
    if(years == 1 && installmentType == "OT"){
      this.hideOpDisc = true;
    }else if(years == 1 && installmentType == ""){
      this.hideOpDisc = true;
    }else if(years == 1 && installmentType == undefined && this.installTyp == "OT"){
      this.hideOpDisc = true;
    }else if(years == 2 && installmentType == undefined && this.installTyp == "OT"){
      this.hideOpDisc = false;
    }else if(years == 3 && installmentType == undefined && this.installTyp == "OT"){
      this.hideOpDisc = false;
    }else if(this.installTyp == "M" && installmentType == undefined){     
      this.hideOpDisc = true;        
    }else if((years == 3 || years == 2 ) && (installmentType == "M" || installmentType == undefined)){
      this.hideOpDisc = true;
    }else if(this.installTyp == "Q" && installmentType == undefined){
      if(installmentType == undefined){
        this.hideOpDisc = true;
      }  
    }else if((years == 3 || years == 2 ) && (installmentType == "Q" || installmentType == undefined)){
      this.hideOpDisc = true;
    }else if(this.installTyp == "HY" && installmentType == undefined){
      if(installmentType == undefined){
        this.hideOpDisc = true;
      }  
    }else if((years == 3 || years == 2 ) && (installmentType == "HY" || installmentType == undefined)){
      this.hideOpDisc = true;
    }
    
    this.threeYearFltCards = [];
    this.barChartLabelsFamily = [];
    this.barChartAmountFamily= [];
    this.barChartSavingFamily= [];
    for(let i = 0; i < this.siAmountArray.length ;i++){
      if( this.siAmountArray[i].title.toLowerCase() ==  this.planType){
        let calculatedArray = (this.calculatedInsurenceAmt(this.siAmountArray[i].plancode,this.siAmountArray[i].sicode,years)).split(":");
        console.log("this.siAmountArray[i].plancode  : "+this.siAmountArray[i].plancode);
        console.log("this.siAmountArray[i].sicode : "+this.siAmountArray[i].sicode);
        console.log("years : "+years);
        console.log(calculatedArray);
        // if(this.siAmountArray[i].plancode == "P001" && this.voluntarSubmitClick == true && this.amount == "10000"){
        //    calculatedArray[1] = parseInt(calculatedArray[1]) * 10%;
        // }
        this.threeYearFltCards.push({ 
          "siTitle": this.siAmountArray[i].amtTitle2,
          "siCalculated": this.addCommas(Number(calculatedArray[1]).toFixed()),
          "oldPrice": this.addCommas(calculatedArray[0]),
          "savedAmount": this.addCommas(calculatedArray[2]),
          'selected' : true,
          'showOld' : (calculatedArray[3] == 'true'),
          "hideInstalment" : (calculatedArray[4] == 'true'),
          "instalmentText":calculatedArray[5],
          "loadingText" : calculatedArray[6],
          "discountPerc": calculatedArray[7]
         });

          // if(this.threeYearFltCards[i].discountPerc == 0){
          //   this.hideOpDisc = true;
          // }else{
          //   this.hideOpDisc =  false;
          // }
         this.barChartAmountFamily.push(calculatedArray[1]);
         this.barChartSavingFamily.push(calculatedArray[2]);
         this.barChartLabelsFamily.push(this.siAmountArray[i].amtTitle);
      }
      if(i == this.siAmountArray.length-1){
        this.toggleGraph(1)
      }
    }
    if(sessionStorage.Ispos == "true"){
      this.threeYearFltCards = this.threeYearFltCards.slice(0, 2);
    }
    
  
  }

  calculatedInsurenceAmt(PlanCode, siCode,years){
    let membersTotal = 0;
      let longtermdisc = 0;
      let amountSave = "0";
      let oldCost =0;
      let newCost =0;
      this.basevalue = 0;
      let instfreqLoad = 0;
      let numberOfInstallmentsInYr = 1;
      let hideOld = false;
      let hideInstalment = true;
      let instalmentText = '';
      let loadingText ='';
      let discountPerc = '';
      this.yrDiscount = '';
      if( this.installTyp == "Q"){
        numberOfInstallmentsInYr = 4;
        hideInstalment = false;
        hideOld = true;
        instalmentText = "quarterly";
      }else if(this.installTyp == "HY"){
        numberOfInstallmentsInYr = 2;
        hideInstalment = false;
        hideOld = true;
        instalmentText = "halfyearly"
      }else if(this.installTyp == "M"){
        numberOfInstallmentsInYr = 12;
        hideInstalment = false;
        hideOld = true;
        instalmentText = "monthly"
      }

      if(years != 0){
        this.NumYear = parseInt(years);
      }
      for(let m = 0; m < this.appData.Masters.Loading.indifloaterload.length ; m++){
        if(this.appData.Masters.Loading.indifloaterload[m].frequencyCode == this.installTyp){
          instfreqLoad =(100+ parseFloat(this.appData.Masters.Loading.indifloaterload[m].perc))/100 ;
          loadingText = instalmentText+" loading "+ this.appData.Masters.Loading.indifloaterload[m].perc+ " %";
        }
      }
      for(let l = 0; this.appData.Masters.Discount.longtermdisc.length;l++){
        if(this.NumYear ==  parseInt(this.appData.Masters.Discount.longtermdisc[l].ptcode)){
          console.log(this.appData.Masters.Discount.longtermdisc);
          discountPerc = this.appData.Masters.Discount.longtermdisc[l].perc;
          longtermdisc = (100- parseFloat(this.appData.Masters.Discount.longtermdisc[l].perc))/100;
          console.log(parseFloat(this.appData.Masters.Discount.longtermdisc[l].perc));
          console.log("*******longtermdisc: "+longtermdisc);
          break;
        }
      }
      membersTotal = 0;
      for(let i = 0; i < this.memberArray.length;i++){
          let userSmokes = this.memberArray[i].smoking;
          let floatingDisPerc = 0;
          let smokingLoad = this.appData.Masters.Loading.smokingload[0].perc;
          for(let k = 0; this.appData.Masters.Discount.flyfloaterdisc.length;k++){
            if(this.memberArray[i].age ==  this.appData.Masters.Discount.flyfloaterdisc[k].agecode){
              floatingDisPerc = this.appData.Masters.Discount.flyfloaterdisc[k].perc;
              break;
            }
          }
          for(let j = 0; j < this.premiums.length; j++){
            if(this.premiums[j].agecode == this.memberArray[i].age && this.premiums[j].sicode == siCode){
              this.basevalue = this.premiums[j].rate;
              break;
            }
          }
          console.log(this.memberArray+"<<<<<<<<<<>>>>>>>>>");
          let memberTotal = parseFloat(this.appService.getMemberBaseFloaterValue( this.memberArray[i].code.toLowerCase(),this.basevalue,userSmokes,floatingDisPerc,smokingLoad));

          console.log("this.memberArray[i].code.toLowerCase()  :  "+this.memberArray[i].code.toLowerCase());
          console.log("this.basevalue : "+this.basevalue);
          console.log("userSmokes : "+userSmokes);
          console.log("floatingDisPerc : "+floatingDisPerc);
          console.log("smokingLoad : "+smokingLoad);

          console.log( this.memberArray[i].title+" : "+memberTotal);
          membersTotal = membersTotal + memberTotal;
          console.log("membersTotal : "+membersTotal);
      }
      oldCost =  (membersTotal*this.NumYear);
      if(numberOfInstallmentsInYr == 1){
        if(this.NumYear != 1){
          this.yrDiscount = "   ("+discountPerc+"% off)";
        }
        newCost = (membersTotal*this.NumYear*longtermdisc);
        amountSave = (oldCost - newCost).toFixed(2);
      }else{
        newCost = (membersTotal*this.NumYear * instfreqLoad)/(this.NumYear * numberOfInstallmentsInYr);
        amountSave = '0.0';
      }
      return oldCost.toFixed(2)+":"+newCost.toFixed(2)+":"+amountSave+":"+hideOld+":"+hideInstalment+":"+instalmentText+":"+loadingText+":"+discountPerc;
  }

  selectSumInsured(sumInsured, event : Event){
    console.log(sumInsured);
    console.log(this.pet);
    if(this.memberArray[0].PolicyType == "HTI"){
      console.log(JSON.stringify(this.oneTimecards));
      if(this.pet == "OneTime"){
          for(let i =0; i < this.oneTimecards.length; i++){
            if(this.oneTimecards[i].ptTitle == sumInsured.ptTitle){         
              this.oneTimecards[i].selected = false;
              //event.add('activeCard'); // To ADD
      
            // console.log("1")
            }else{
              this.oneTimecards[i].selected = true;
              //event.target.remove('activeCard'); // To Remove

            //  console.log("2")    
            }
          }
      } else  if(this.pet == "Monthly"){
        for(let i =0; i < this.monthlyCards.length; i++){
          if(this.monthlyCards[i].ptTitle == sumInsured.ptTitle){         
            this.monthlyCards[i].selected = false;
            //event.add('activeCard'); // To ADD
    
          // console.log("1")
          }else{
            this.monthlyCards[i].selected = true;
            //event.target.remove('activeCard'); // To Remove

          //  console.log("2")    
          }
        }
    }
    }else{
      for(let i =0; i < this.threeYearFltCards.length; i++){
          if(this.threeYearFltCards[i].siTitle == sumInsured.siTitle){
            this.threeYearFltCards[i].selected = false;
          // console.log("1")
          }else{
            this.threeYearFltCards[i].selected = true;
          //  console.log("2")
          }
        }
    }
  }
  
  
  
  getPolicyTermOneTimeDisplayAmount(total){
    console.log("boolean: " + this.graphShow);
    this.individualGetQuoteArray = [];
    this.getTotalMemberArraySI();
    this.showGraph(1);
    this.hideOpDisc = true;
    this.discountPercArray = this.appData.Masters.Discount.longtermdisc;
    console.log(this.discountPercArray);
    for(let d=0; d<this.discountPercArray.length; d++){
      if(this.discountPercArray[d].ptcode == 1){
        this.oneyearDiscountPerc = this.discountPercArray[d].perc;
        console.log(this.oneyearDiscountPerc);
      }else if(this.discountPercArray[d].ptcode == 2){
        this.twoyearDiscountPerc = this.discountPercArray[d].perc;
        console.log(this.twoyearDiscountPerc);
      }else if(this.discountPercArray[d].ptcode == 3){
        this.threeyearDiscountPerc = parseInt(this.discountPercArray[d].perc);
        console.log(this.threeyearDiscountPerc);
      }
    }

    this.policyterm = this.appData.Masters.PolicyTerm;
    for(let p=0; p<this.policyterm.length; p++){
      if(this.policyterm[p].ptTitle == "1 Year"){
        this.policyTermOneYear = this.policyterm[p].ptcode;
      }else if(this.policyterm[p].ptTitle == "2 Year"){
        this.policyTermTwoYear = this.policyterm[p].ptcode;
      }else if(this.policyterm[p].ptTitle == "3 Year"){
        this.policyTermThreeYear = this.policyterm[p].ptcode;
      }
    }


    this.termOfTwoAmount = (this.finalTotal * this.policyTermTwoYear).toFixed(2);  
    this.termOfTwoDiscountAmount = (this.termOfTwoAmount - (this.termOfTwoAmount * this.twoyearDiscountPerc)/100).toFixed();
    this.twoYearSaveAmount = ((this.termOfTwoAmount * this.twoyearDiscountPerc)/100).toFixed();   
    this.termOfThreeAmount = (this.finalTotal * this.policyTermThreeYear).toFixed(2);
    this.termOfThreeDiscountAmount = (this.termOfThreeAmount - (this.termOfThreeAmount * this.threeyearDiscountPerc)/100).toFixed();
    this.threeYearSaveAmount = ((this.termOfThreeAmount * this.threeyearDiscountPerc)/100).toFixed();

    this.oneTimecards[2].discountAmount = this.addCommas(this.termOfThreeDiscountAmount);
    this.oneTimecards[2].saveAmount = this.addCommas(this.threeYearSaveAmount);
    this.oneTimecards[2].yearAmount = this.addCommas(this.termOfThreeAmount);
    this.oneTimecards[2].totalDiscount = parseFloat(this.threeyearDiscountPerc) + "%";
    this.oneTimecards[2].selected = true;
    this.oneTimecards[1].discountAmount = this.addCommas(this.termOfTwoDiscountAmount);
    this.oneTimecards[1].saveAmount = this.addCommas(this.twoYearSaveAmount);
    this.oneTimecards[1].yearAmount = this.addCommas(this.termOfTwoAmount);
    this.oneTimecards[1].totalDiscount = parseFloat(this.twoyearDiscountPerc) + "%";
    this.oneTimecards[1].selected = true;
      this.oneTimecards[0].discountAmount = this.addCommas(this.finalTotal.toFixed());
      this.oneTimecards[0].saveAmount = "0";
      this.oneTimecards[0].yearAmount = this.addCommas(this.finalTotal.toFixed());
    this.oneTimecards[0].totalDiscount = "0 %";
    this.oneTimecards[0].selected = true;
    //this.individualGetQuoteArray = JSON.parse("quickQuote: " + JSON.stringify(this.oneTimecards));
  }


   getPolicyTermMonthlyDisplayAmount(){
     this.hideOpDisc = true;
     this.individualGetQuoteArray = [];
    this.getTotalMemberArraySI();
    this.showGraph(1);
    this.frequencyPercArray = this.appData.Masters.Loading.indifloaterload;
    for(let f=0; f<this.frequencyPercArray.length; f++){
      if(this.frequencyPercArray[f].frequencyCode == "M"){
        this.monthlyPerc = this.frequencyPercArray[f].perc;
        console.log(this.monthlyPerc);       
      }
    }

    this.policyterm = this.appData.Masters.PolicyTerm;
    for(let p=0; p<this.policyterm.length; p++){
      let a=12;
      if(this.policyterm[p].ptTitle == "1 Year"){
        this.policyTermOneYear = this.policyterm[p].ptcode;
        this.oneMonthCount = this.policyterm[p].ptcode * a;
      }else if(this.policyterm[p].ptTitle == "2 Year"){
        this.policyTermTwoYear = this.policyterm[p].ptcode;
        this.twoMonthCount = this.policyterm[p].ptcode * a;
      }else if(this.policyterm[p].ptTitle == "3 Year"){
        this.policyTermThreeYear = this.policyterm[p].ptcode;
        this.threeMonthCount = this.policyterm[p].ptcode * a;
      }
    }
    // this.monthlyOneYearAmount = (parseInt(this.finalTotal) + (this.finalTotal)).toFixed(2);  
    // this.monthlyTwoYearAmount = (((this.finalTotal * this.policyTermTwoYear))/this.twoMonthCount).toFixed(2);
    // this.monthlyThreeYearAmount = (((this.finalTotal * this.policyTermThreeYear))/this.threeMonthCount).toFixed(2);
    this.monthlyOneYearAmount = (parseInt(this.finalTotal) + (this.finalTotal * this.monthlyPerc)/100).toFixed(0);  
    this.monthlyTwoYearAmount = (((this.finalTotal * this.policyTermTwoYear) * (1+this.monthlyPerc/100))/this.twoMonthCount).toFixed(0);
    this.monthlyThreeYearAmount = (((this.finalTotal * this.policyTermThreeYear) * (1+this.monthlyPerc/100))/this.threeMonthCount).toFixed(0);
    console.log(this.monthlyThreeYearAmount);

    let getMonthlyTitle;
    let getMonthlyLoadingPerc;
    for(let i=0; i<this.installments.length; i++){
      if(this.installments[i].frequencyCode == "M"){
        getMonthlyTitle = this.installments[i].frequencyTitle.toLowerCase();
      }
    }
    let policyTermLoading = this.appData.Masters.Loading.indifloaterload;
    for(let f=0; f<policyTermLoading.length; f++){
      if(policyTermLoading[f].frequencyCode == "M"){
        getMonthlyLoadingPerc = policyTermLoading[f].perc;
      }
    }

    this.monthlyCards[0].view = true;
    this.monthlyCards[1].view = false;
    this.monthlyCards[2].view = false;
    this.monthlyCards[0].selected = true;
      this.monthlyCards[1].selected = true;
      this.monthlyCards[2].selected = true;
      this.monthlyCards[1].installmentTitle = getMonthlyTitle;
      this.monthlyCards[2].installmentTitle = getMonthlyTitle;
      this.monthlyCards[1].installmentLoading = getMonthlyLoadingPerc;
      this.monthlyCards[2].installmentLoading = getMonthlyLoadingPerc;
      this.monthlyCards[2].discountAmount = this.addCommas(Math.round(this.monthlyThreeYearAmount));
    this.monthlyCards[1].discountAmount = this.addCommas(Math.round(this.monthlyTwoYearAmount));
      this.individualGetQuoteArray = JSON.parse(JSON.stringify(this.monthlyCards)); 
   }

   getPolicyTermQuarterlyDisplayAmount(){
    this.hideOpDisc = true;
     this.individualGetQuoteArray = [];
    this.getTotalMemberArraySI();
    this.showGraph(1);
    this.frequencyPercArray = this.appData.Masters.Loading.indifloaterload;
    for(let f=0; f<this.frequencyPercArray.length; f++){
      if(this.frequencyPercArray[f].frequencyCode == "Q"){
        this.quarterlyPerc = this.frequencyPercArray[f].perc;
        console.log(this.quarterlyPerc);        
      }
    }

    this.policyterm = this.appData.Masters.PolicyTerm;
    for(let p=0; p<this.policyterm.length; p++){
      let a=4;
      if(this.policyterm[p].ptTitle == "1 Year"){
        this.policyTermOneYear = this.policyterm[p].ptcode;
        this.oneMonthCount = this.policyterm[p].ptcode * a;
      }else if(this.policyterm[p].ptTitle == "2 Year"){
        this.policyTermTwoYear = this.policyterm[p].ptcode;
        this.twoMonthCount = this.policyterm[p].ptcode * a;
        console.log(this.twoMonthCount);
      }else if(this.policyterm[p].ptTitle == "3 Year"){
        this.policyTermThreeYear = this.policyterm[p].ptcode;
        this.threeMonthCount = this.policyterm[p].ptcode * a;
      }
    }
    this.quarterlyOneYearAmount = (parseInt(this.finalTotal) + (this.finalTotal * this.quarterlyPerc)/100).toFixed(0);  
    this.quarterlyTwoYearAmount = (((this.finalTotal *   this.policyTermTwoYear ) * (1+this.quarterlyPerc/100))/this.twoMonthCount).toFixed(0);    
    this.quarterlyThreeYearAmount = (((this.finalTotal * this.policyTermThreeYear) * (1+this.quarterlyPerc/100))/this.threeMonthCount).toFixed(0);

    let getMonthlyTitle;
    let getMonthlyLoadingPerc;
    for(let i=0; i<this.installments.length; i++){
      if(this.installments[i].frequencyCode == "Q"){
        getMonthlyTitle = this.installments[i].frequencyTitle.toLowerCase();
      }
    }
    let policyTermLoading = this.appData.Masters.Loading.indifloaterload;
    for(let f=0; f<policyTermLoading.length; f++){
      if(policyTermLoading[f].frequencyCode == "Q"){
        getMonthlyLoadingPerc = policyTermLoading[f].perc;
      }
    }
    this.quarterlyCards[0].view = true;
    this.quarterlyCards[1].view = false;
    this.quarterlyCards[2].view = false;
    this.quarterlyCards[0].selected = true;
    this.quarterlyCards[1].selected = true;
    this.quarterlyCards[2].selected = true;
      this.quarterlyCards[1].installmentTitle = getMonthlyTitle;
      this.quarterlyCards[2].installmentTitle = getMonthlyTitle;
      this.quarterlyCards[1].installmentLoading = getMonthlyLoadingPerc;
      this.quarterlyCards[2].installmentLoading = getMonthlyLoadingPerc;
      this.quarterlyCards[2].discountAmount = this.addCommas(Math.round(this.quarterlyThreeYearAmount));
    this.quarterlyCards[1].discountAmount = this.addCommas(Math.round(this.quarterlyTwoYearAmount));
      this.individualGetQuoteArray = JSON.parse(JSON.stringify(this.quarterlyCards)); 
   }

   getPolicyTermHalfyearlyDisplayAmount(){
    this.hideOpDisc = true;
     this.individualGetQuoteArray = [];
    this.getTotalMemberArraySI();
    this.showGraph(1);
    this.frequencyPercArray = this.appData.Masters.Loading.indifloaterload;
    for(let f=0; f<this.frequencyPercArray.length; f++){
      if(this.frequencyPercArray[f].frequencyCode == "HY"){
        this.halfyearlyPerc = this.frequencyPercArray[f].perc;
        console.log(this.halfyearlyPerc);        
      }
    }

    this.policyterm = this.appData.Masters.PolicyTerm;
    for(let p=0; p<this.policyterm.length; p++){
      let a=2;
      if(this.policyterm[p].ptTitle == "1 Year"){
        this.policyTermOneYear = this.policyterm[p].ptcode;
        this.oneMonthCount = this.policyterm[p].ptcode * a;
      }else if(this.policyterm[p].ptTitle == "2 Year"){
        this.policyTermTwoYear = this.policyterm[p].ptcode;
        this.twoMonthCount = this.policyterm[p].ptcode * a;
        console.log(this.twoMonthCount);
      }else if(this.policyterm[p].ptTitle == "3 Year"){
        this.policyTermThreeYear = this.policyterm[p].ptcode;
        this.threeMonthCount = this.policyterm[p].ptcode * a;
      }
    }
    this.halfyearlyOneYearAmount = (parseInt(this.finalTotal) + (this.finalTotal * this.halfyearlyPerc)/100).toFixed(0);  
    this.halfyearlyTwoYearAmount = (((this.finalTotal * this.policyTermTwoYear ) * (1+this.halfyearlyPerc/100))/this.twoMonthCount).toFixed(0);    
    this.halfyearlyThreeYearAmount = (((this.finalTotal * this.policyTermThreeYear) * (1+this.halfyearlyPerc/100))/this.threeMonthCount).toFixed(0);

    let getMonthlyTitle;
    let getMonthlyLoadingPerc;
    for(let i=0; i<this.installments.length; i++){
      if(this.installments[i].frequencyCode == "HY"){
        getMonthlyTitle = this.installments[i].frequencyTitle.toLowerCase();
      }
    }
    let policyTermLoading = this.appData.Masters.Loading.indifloaterload;
    for(let f=0; f<policyTermLoading.length; f++){
      if(policyTermLoading[f].frequencyCode == "HY"){
        getMonthlyLoadingPerc = policyTermLoading[f].perc;
      }
    }
    this.halfyearlyCards[0].view = true;
    this.halfyearlyCards[1].view = false;
    this.halfyearlyCards[2].view = false;
    this.halfyearlyCards[0].selected = true;
      this.halfyearlyCards[1].selected = true;
      this.halfyearlyCards[2].selected = true;
      this.halfyearlyCards[1].installmentTitle = getMonthlyTitle;
      this.halfyearlyCards[2].installmentTitle = getMonthlyTitle;
      this.halfyearlyCards[1].installmentLoading = getMonthlyLoadingPerc;
      this.halfyearlyCards[2].installmentLoading = getMonthlyLoadingPerc;
      this.halfyearlyCards[2].discountAmount = this.addCommas(Math.round(this.halfyearlyThreeYearAmount));
    this.halfyearlyCards[1].discountAmount = this.addCommas(Math.round(this.halfyearlyTwoYearAmount));
      this.individualGetQuoteArray = JSON.parse(JSON.stringify(this.halfyearlyCards)); 
    
   }

  /*  latestQuote(){
    this.maximumInsuAmtCode ="";
    this.loadDisplayMemberData();
    this.individualDiv = this.individualDiv
    this.floaterDiv = !this.floaterDiv;
   }
   generateQuote(){
     this.memberArray = [];
     this.memberArray = JSON.parse(JSON.stringify(this.tempMemberArray));
     if(this.memberArray[0].PolicyType == "HTF"){
        this.changeInsurence(this.familyInsurence);
        this.petFamily = "ThreeYears";
        //this.showSumInByYear(3);
        this.toggleGraph(1);
        
     }else{

     }
      
   } */

   generateQuote(){
    this.maximumInsuAmtCode ="";
    this.memberArray = [];
    this.memberArray = JSON.parse(JSON.stringify(this.tempMemberArray));
    if(this.memberArray[0].PolicyType == "HTF"){
      this.changeInsurence(this.familyInsurence);
      this.petFamily = "OneYears";
      //this.showSumInByYear(3);
      this.showGraph(1);
      this.toggleGraph(1);
    }else{
      if(this.pet == "OneTime"){
        let total = this.getTotalMemberArraySI();
        this.getPolicyTermOneTimeDisplayAmount(total);       
      }else if(this.pet == "Monthly"){
        this.getTotalMemberArraySI();     
        this.getPolicyTermMonthlyDisplayAmount();
      } else if(this.pet == "Quarterly"){
        this.getTotalMemberArraySI();          
        this.getPolicyTermQuarterlyDisplayAmount();
      } else if(this.pet == "HalfYearly"){
        this.getTotalMemberArraySI();         
        this.getPolicyTermHalfyearlyDisplayAmount();
      }
    }
   }
    toggleGraph(Numbr){
      this.showFamilyGraph();
      if(Numbr == "1" || Numbr == 1){
        this.familyGraph = false;
      }else{
        this.familyGraph = true;
      }
    }

   customizeView(){
     this.show= !this.show;
     this.closeBtn = true;
   }

   closeModal(){
    this.show= false;
    this.closeBtn = !this.closeBtn;
   }

   removeMember(member, event : Event){
      event.stopPropagation();    
      let index = this.tempMemberArray.indexOf(member);    
      if(index > -1 ){
        this.tempMemberArray.splice(index, 1);
      }
   }

   showFamilyGraph(){
    let reversedYearAmount = [];
    let reverseSaveAmountData = [];
    this.barChartLabels = this.barChartLabelsFamily;
    reversedYearAmount = this.barChartAmountFamily;
    reverseSaveAmountData = this.barChartSavingFamily;
      this.barChartData= [
        {
          data: reversedYearAmount, 
          label: 'Premium you pay'
        },
        {
          data: reverseSaveAmountData, 
          label: 'Premium you save'
        }
      ]; 
   }
  

   showGraph(numbr){
    console.log(this.pet);
   //  let yearAmountData = [];
   //  let saveAmountData = [];
   if(numbr == "1" || numbr == 1){
    this.graph = true;
  }else{
    this.graph = false;
  }
    if(this.pet == "OneTime"){
     let yearAmountData = [];
     let saveAmountData = [];
     this.barChartLabels = ['3 Years', '2 Years', '1 Year'];
     for(let f=0; f<this.oneTimecards.length; f++){
       yearAmountData[f] = this.oneTimecards[f].discountAmount; 
       saveAmountData[f] = this.oneTimecards[f].saveAmount;      
      }     
     let reversedYearAmount = yearAmountData.reverse();
     let reverseSaveAmountData = saveAmountData.reverse();
     this.barChartData= [
       {
         data: reversedYearAmount, 
         label: 'Premium you pay'
       },
       {
         data: reverseSaveAmountData, 
         label: 'Premium you save'
       }
     ];
    }
    else if(this.pet == "Monthly")
    {
     let yearAmountData = [];
     let saveAmountData = [];      
     this.barChartLabels = ['3 Years', '2 Years'];
     for(let f=0; f<this.monthlyCards.length; f++){
       yearAmountData[f] = this.monthlyCards[f].discountAmount; 
       saveAmountData[f] = this.monthlyCards[f].discountAmount;
      }            
     let reversedYearAmount = yearAmountData.reverse();
     let reverseSaveAmountData = saveAmountData.reverse();
     this.barChartData= [
       {
         data: reversedYearAmount, 
         label: 'Premium you pay'
       }];
    }
    else if(this.pet == "Quarterly")
    {
     let yearAmountData = [];
     let saveAmountData = [];
     this.barChartLabels = ['3 Years', '2 Years'];
     for(let f=0; f<this.quarterlyCards.length; f++){
       yearAmountData[f] = this.quarterlyCards[f].discountAmount; 
       saveAmountData[f] = this.quarterlyCards[f].discountAmount;
      }     
      let reversedYearAmount = yearAmountData.reverse();
      let reverseSaveAmountData = saveAmountData.reverse();
     this.barChartData= [
       {
         data: reversedYearAmount, 
         label: 'Premium you pay'
       }];      
    }
    else if(this.pet == "HalfYearly")
    {
     let yearAmountData = [];
     let saveAmountData = [];
     this.barChartLabels = ['3 Years', '2 Years'];
     for(let f=0; f<this.halfyearlyCards.length; f++){
       yearAmountData[f] = this.halfyearlyCards[f].discountAmount; 
       saveAmountData[f] = this.halfyearlyCards[f].discountAmount;
      }     
      let reversedYearAmount = yearAmountData.reverse();
     let reverseSaveAmountData = saveAmountData.reverse();
     this.barChartData= [
       {
         data: reversedYearAmount, 
         label: 'Premium you pay'
       }];
    }
    if(this.showYearValue == true){
       this.graphShow = true;
       this.showYearValue = false;
    }else{
     this.graphShow = false;
     this.showYearValue = true;
    }
  }  
   proceedQuickQuote(){
    console.log(this.maximumInsuAmtCode+"this.maximumInsuAmtCode");

    if(this.memberArray[0].PolicyType== "HTF"){
      sessionStorage.familysumQQ = true;
    sessionStorage.familySumAmtQQ = this.maximumInsuAmtCode;
    }
    console.log("proceed: " + this.loginStatus);
    
    if(this.loginStatus == "true"){
      console.log("IF");
      
      this.navCtrl.push(LandingScreenPage,  {forQuickQuote: this.forQuickQuote, preLoginQuickQuoteData : [this.memberArray,this.threeYearFltCards,this.individualGetQuoteArray]});
    }else{
      console.log("Else");
      this.navCtrl.push(LoginPage,{preLoginQuickQuote : true, forQuickQuote: this.forQuickQuote,
        preLoginQuickQuoteData : [this.memberArray,this.threeYearFltCards,this.individualGetQuoteArray]
      });
    }
    
   }
   toggleCustomize(){
     this.openEdit = !this.openEdit;
   }

 
   addEmailMember(){
    this.sendEmailList.push({value:""});
    console.log(JSON.stringify(this.sendEmailList));     
  }

  sendEmail(){
    for(let i = 0 ; i < this.sendEmailList.length ; i++){
      this.emailTo.push(this.sendEmailList[i].value);     
    }
    var emailData = {"emailto":this.emailTo,
                        "pname":this.NameOfEmailProposerIndividual,
                        "qid":"QI30",
                        "uid":[
                        "90",
                        "45"]};  
    this.encryptEmailData(emailData);
    
  }

  addSmsMember(){
    this.sendSMSList.push({value:""});
    console.log(JSON.stringify(this.sendSMSList));     
  }

  sendSms(){
    for(let i = 0 ; i < this.sendSMSList.length ; i++){
      this.smsTo.push(this.sendSMSList[i].value);     
    }
    console.log(JSON.stringify(this.smsTo));
    var smsData = {"mobile":this.smsTo,
                        "pname":this.NameOfSMSProposerIndividual,
                        "qid":"QI30",
                        "uid":[
                        "90",
                        "45"]};  
    this.encryptSmsData(smsData);
    
  }


  encryptEmailData(sendEmailData){
    
    var sendData = this.appService.encryptData(JSON.stringify(sendEmailData),sessionStorage.TokenId);    
    console.log("testt: " + JSON.stringify(sendData));
    this.getEmailResponseData({'request': sendData});
  }

  encryptSmsData(sendSmsData){
    
    var sendData = this.appService.encryptData(JSON.stringify(sendSmsData),sessionStorage.TokenId);    
    console.log("testt: " + JSON.stringify(sendData));   
    this.getSmsResponseData({'request' : sendData});
  }

  getEmailResponseData(serviceData){
    this.appService.callService("FGHealth.svc/SendQuotationByEmail",serviceData,'')
    .subscribe(SendQuotationByEmail =>{

     // console.log(Bootstrap.BootstrapResult);
      if(SendQuotationByEmail && SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "0"){
        console.log(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg);
        alert(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg);
         
      }else{
        alert(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg); 
      }
    }, (err) => {
    console.log(err);
    });
}

getSmsResponseData(serviceData){
  this.appService.callService("FGHealth.svc/SendQuotationBySMS",serviceData,'')
  .subscribe(SendQuotationBySMS =>{

   // console.log(Bootstrap.BootstrapResult);
    if(SendQuotationBySMS && SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "0"){
      console.log(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
      alert(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
       
    }else{
      alert(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
    }
  }, (err) => {
  console.log(err);
  });
}

addCommas(x) {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  console.log("test: " + parts);
  return parts.join(".");
}


// voluntaryDeduction(){
//   console.log("Clicked");      
//   this.voluntaryPopup = false;
//   console.log("planType"+this.memberArray[0].insuredPlanTypeText);
  
//   if(this.memberArray[0].insuredPlanTypeText == "VITAL"){       
//     this.amountList = [{amount:"10000"},{amount:"25000"},{amount:"50000"}]; 
//     console.log(JSON.stringify(this.amountList));               
//   }else if(this.memberArray[0].insuredPlanTypeText == "SUPERIOR"){
//     this.amountList = [{amount:"50000"},{amount:"75000"},{amount: "100000"}];
//     console.log(JSON.stringify(this.amountList));                        
//   }      
// }

onSelectChangeDeductable(){

      
  if(this.amount == "10,000" && this.memberArray[0].insuredPlanTypeText == "VITAL"){
    this.amountDiscount = 10;
  }else if(this.amount == "25,000" && this.memberArray[0].insuredPlanTypeText == "VITAL"){
    this.amountDiscount = 15;
  }else if(this.amount == "50,000" && this.memberArray[0].insuredPlanTypeText == "VITAL"){
    this.amountDiscount = 20;
  }else if(this.amount == "50,000" && this.memberArray[0].insuredPlanTypeText == "SUPERIOR"){
    this.amountDiscount = 15;
  }else if(this.amount == "75,000" && this.memberArray[0].insuredPlanTypeText == "SUPERIOR"){
    this.amountDiscount = 20;
  }else if(this.amount == "1,00,000" && this.memberArray[0].insuredPlanTypeText == "SUPERIOR"){
    this.amountDiscount = 25;
  }
  console.log(this.amountDiscount);
  
}

closeVoluntaryModalPop(){
  this.voluntaryPopup = true;
}

proceedFromVoluntaryDeductable(){  
  this.voluntarSubmitClick = true;    
  this.loadDisplayMemberData();
} 

}



export class PlaceholderPage {}
 