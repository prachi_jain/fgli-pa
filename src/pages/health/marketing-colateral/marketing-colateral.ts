import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductBenefitsPage } from '../product-benefits/product-benefits';
import { PlansLimitsPage } from '../plans-limits/plans-limits';


/**
 * Generated class for the MarketingColateralPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    MarketingColateralPage,
  ],
  imports: [
    //IonicPageModule.forChild(NomineeDetailsPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-marketing-colateral',
  templateUrl: 'marketing-colateral.html',
})
export class MarketingColateralPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarketingColateralPage');
    document.getElementById("splash").style.display='none';
  }

  goToProductBenefits(){
  	this.navCtrl.push(ProductBenefitsPage);
  }
  goToPlanLimits(){
    this.navCtrl.push(PlansLimitsPage);
  }



}
