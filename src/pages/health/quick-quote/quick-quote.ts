

import { Component,NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, AlertController } from 'ionic-angular';
import { QuickQuoteResultPage } from '../quick-quote-result/quick-quote-result';
import { ActionSheetController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AppService } from '../../../providers/app-service/app-service';
//import { QuickQuotePage } from '../quick-quote/quick-quote';
/**
 * Generated class for the QuickQuotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    QuickQuotePage,
  ],
  imports: [
    //IonicPageModule.forChild(QuickQuotePage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-quick-quote',
  templateUrl: 'quick-quote.html',
})
export class QuickQuotePage {
  PolicyTypeSelected = "familyFloaterCode";forQuickQuote; familyFloaterCode='';individualCode='';showTransBaground:boolean = true;showTransBaground2 =false;
  appData; apiMembers; apiPlanType; apiAgeGroup; QuickQuotePlans=[]; insuredPlanTitle = '';policyTypes; showInsurenceDiv: boolean = false;
  public showMemberPopup:boolean = false; public showChildPopup:boolean = false; public hidePopupSmokerDiv:boolean = false; public hidePopupMarriedDiv:boolean = false; 
  popupTitle= ""; popupImg; insuredCode; slide=''; canShowToast = true; childAgeGroup =[]; showAddMember = false;familySlide='';
  popupMemberData = {"code":""}; membersInsured = []; ageGroupToshow=[]; memberSmokes =  false; membersAvailable; membersShow = [];
  /*memberMarried=  false;*/ memberAge =0 ; childsAge =0; maximumInsuAmtCode = ''; insurenceAmtToShow = []; membersShownOnPopup = []; 
  familyInsurenceAmt = []; familyInsuredPlanType = ''; familyInsuredPlanTypeText = '';fromQuickQuoteResult;  hideDisableCards =false;
  myMember=[{"id":"SELF","imgPath":"assets/imgs/self.png"},{"id":"SPOU","imgPath":"assets/imgs/spouse.png"},
  {"id":"SON","imgPath":"assets/imgs/son.png"},{"id":"DAUG","imgPath":"assets/imgs/daughterIcon.png"},
  {"id":"FATH","imgPath":"assets/imgs/self.png"},{"id":"MOTH","imgPath":"assets/imgs/self.png"},{"id":"GRCH","imgPath":"assets/imgs/self.png"},
  {"id":"SIB","imgPath":"assets/imgs/self.png"},{"id":"CHLD","imgPath":"assets/imgs/self.png"},{"id":"GRPA","imgPath":"assets/imgs/self.png"}]
  vitalMembers=["SELF","SPOU","SON","SON1","SON2","SON3","DAUG","DAUG1","DAUG2","DAUG3","FATH","MOTH","CHLD"];
  individualDiscount = false;
  isClassOneActive = false;
  disableProceedBt = true;
  editUserData = false;
  addMember= false;
  public hideMemberPopup:boolean = false;
  popupMember="SELF";
  hideProceedButton = true;
  marriedStatus="";
  loginStatus;
  hideForSelf = true;
  marriedStatusArray = [{value: "Single", status: false},{value: "Married", status: true},{value: "Divorced", status: false},{value: "Widow / Widower", status: false}];
  showButton = true;

  public static readonly pageName = 'QuickQuotePage';

  constructor(public navCtrl: NavController,public appService: AppService, public alertCtrl: AlertController, private actionSheetCtrl: ActionSheetController, public navParams: NavParams,public toast: ToastController) {
    this.appData = JSON.parse(localStorage.AppData);
    this.apiMembers = this.appData.Masters.Members;
    this.apiPlanType = this.appData.Masters.PlanType;
    this.apiAgeGroup = this.appData.Masters.Age;
    this.policyTypes = this.appData.Masters.PolicyType;
    this.forQuickQuote = navParams.get("forQuickQuote");
    this.fromQuickQuoteResult = navParams.get("fromQuickQuoteResult");        
  }

  ionViewDidLoad() {
    this.loginStatus = this.navParams.get("loginStatus");
    console.log("QuickQuote: " + this.loginStatus);

    if(sessionStorage.pagename == "Login"){
      this.showButton = false;
    }
    
    this.membersArrayFunction();
    this.getSumInsuredArray();
    this.childAgeRange();
    for(let i = 0; i < this.policyTypes.length;i++){
      if( (this.policyTypes[i].title).toLowerCase().indexOf("individual") >= 0){
        this.individualCode = this.policyTypes[i].code;
      }else if( (this.policyTypes[i].title).toLowerCase().indexOf("family") >=0 ){
        this.familyFloaterCode = this.policyTypes[i].code;
        this.PolicyTypeSelected = this.familyFloaterCode;
      }
    }

    this.populateInsurenceAmt(0, "SELF");
    this.showTransBaground = true;

    if(this.fromQuickQuoteResult){
      this.hideDisableCards = true;
      console.log(JSON.parse(sessionStorage.quickQuoteMembersDetails));
      this.membersShow = JSON.parse(sessionStorage.MembersShownOnforQuickQuote);
      this.membersInsured = JSON.parse(sessionStorage.quickQuoteMembersDetails);
      for(let i = 0; i < this.membersShow.length; i++){
        let term = this.membersInsured.filter(person => (person.title == this.membersShow[i].title && person.code == this.membersShow[i].code ));
         if(term.length <1){
            this.membersShow[i].detailsSaved = false;
            this.membersShow[i].age = '';
            this.membersShow[i].ageText = '';
            this.membersShow[i].insuredCode = '';
            this.membersShow[i].insuredAmtText = '';
            this.membersShow[i].PolicyType = '';
            this.membersShow[i].smoking = false;
            this.membersShow[i].smokingText = "";
          }
      }
      sessionStorage.removeItem("quickQuoteMembersDetails");
      sessionStorage.removeItem("MembersShownOnforQuickQuote");
      if(this.membersInsured[0].PolicyType == "HTF"){
        this.familySlide = this.membersInsured[0].insuredCode;
        this.PolicyTypeSelected = this.familyFloaterCode;
        this.individualDiscount = false;
        this.showInsurenceDiv = false;
      }else if(this.membersInsured[0].PolicyType == "HTI"){
        this.PolicyTypeSelected = this.individualCode;
        this.maximumInsuAmtCode = this.insuredCode;
        this.individualDiscount = true;
        this.showInsurenceDiv = true;
      }
    }
  }

  childAgeRange(){
    for(let i = 0; i <this.apiAgeGroup.length; i++){
        if(this.apiAgeGroup[i].min >  25){
          break;
        }
        this.childAgeGroup.push(this.apiAgeGroup[i]);
    }
  }
  fillData(memberData){
    console.log(memberData);
    this.slide = '';
    this.memberSmokes = false;
    //this.memberMarried = false;
    this.populateInsurenceAmt(1, memberData.code);
    if(memberData.detailsSaved){
      return;
    }
    if(memberData.code=="SELF" && this.membersInsured.length==0){  
      this.showTransBaground = false;    
      this.popupTitle = memberData.title;
      this.popupImg = memberData.imgUrl;
      this.showMemberPopup = true;
      this.popupMemberData = memberData;
      this.hideMemberPopup = true;  
      this.hidePopupSmokerDiv = true;
      this.hidePopupMarriedDiv =true;
      this.populateInsurenceAmt(1,memberData.code);
      if(this.PolicyTypeSelected == this.familyFloaterCode ){
        //this.memberMarried = true;
      }
    }else if(memberData.code!="SELF" && this.membersInsured.length==0){
      this.showToast("Please insure 'self' first before choosing other member/s.");
    }else if(memberData.code!="SELF" && this.membersInsured.length!=0 && !memberData.detailsSaved){
      if(this.membersInsured.length==1 && this.membersShow[0].maritalStatus == "Married" && memberData.code != "SPOU"){
        this.showToast("Please provide spouse details as your marital status is Married.");
      }else{
        //document.getElementById("addMember").style.marginBottom='60px';
        this.hideProceedButton = false;
        if(memberData.code=="SON" || memberData.code == "DAUG"){
          this.showTransBaground = false;  
          this.hideMemberPopup = false; 
          this.hidePopupSmokerDiv = false;   
          this.showChildPopup = true;
        }else if(memberData.code == "SPOU"){
          if(this.membersShow[0].maritalStatus == "Married"){
            this.hidePopupSmokerDiv = true;
            this.showTransBaground = false; 
            this.hideMemberPopup = true;     
            this.showMemberPopup = true;
          }else if(this.membersShow[0].maritalStatus == "Single"){
            this.showToast("As your marital status is single, you cannot fill spouse details.");
          }else if(this.membersShow[0].maritalStatus == "Divorced"){
            this.showToast("As your marital status is divorced, you cannot fill spouse details.");
          }else if(this.membersShow[0].maritalStatus == "Widow / Widower"){
            this.showToast("As your marital status is widow / widower, you cannot fill spouse details.");
          }
        }else if(memberData.code == "FATH"){       
          this.hideMemberPopup = true; 
          this.popupMember = "Father";
          this.hidePopupSmokerDiv = true;
        }else if(memberData.code == "MOTH"){
          this.hideMemberPopup = true; 
          this.popupMember = "Mother";
          this.hidePopupSmokerDiv =true;
        }else if(memberData.code != "SPOU"){
          this.showTransBaground = false;    
          this.showMemberPopup = true;
          this.hideMemberPopup = true;         
        }
        this.popupTitle = memberData.title;
        this.popupImg = memberData.imgUrl;
        this.popupMemberData = memberData;
      }

    }
    this.getSumInsuredArray();

  }
  hidePopup(){
    this.slide = '';
    this.showMemberPopup = false;
    this.showChildPopup = false;
    this.insuredCode = '';
    this.childsAge = 0;
    this.memberAge = 0;
    this.memberSmokes = false;
    //this.memberMarried = false;
    this.showTransBaground = true; 
    this.hideMemberPopup = false;  
  }
  membersArrayFunction(){
    for(let a = 0; a <this.apiMembers.length; a++){
      let term = this.myMember.filter(person => person.id == this.apiMembers[a].code );
      var memberDetails = {};
      memberDetails["code"] =this.apiMembers[a].code;
      memberDetails["forQuickQuote"] =this.forQuickQuote;
      memberDetails["title"] =this.apiMembers[a].title;
      memberDetails["smoking"] = false;
      memberDetails["smokingText"] = "";
      memberDetails["popupType"] = "1";
      memberDetails["showMarried"] = false;
      memberDetails["maritalStatus"] = "";
      memberDetails["age"] = "";
      memberDetails["ageText"] = "";
      memberDetails["insuredCode"] ="";
      memberDetails["insuredAmtText"] ="";
      memberDetails["showDependent"] = false;
      memberDetails["showAddAnotherBtn"] = false;
      memberDetails["PolicyType"]= '';
      memberDetails["showFamilyMemberOnScreen"] = false;
      memberDetails["disableRemove"] = false;
      memberDetails["ShowInsurenceAmt"] = false;
      memberDetails["insuredPlanTypeCode"] = '';
      memberDetails["insuredPlanTypeText"] = '';
      memberDetails["insuredAmtInDigit"] = '';
      memberDetails["memberSelectedOnAddmember"] = false;
      if( this.vitalMembers.indexOf(this.apiMembers[a].code)>=0){
        memberDetails["showFamilyMemberOnScreen"] = true;
        memberDetails["memberSelectedOnAddmember"] = true;
      }

     

      if(term.length!=0){
        memberDetails["imgUrl"] =term[0].imgPath;
        if(term[0].id == "SON" || term[0].id == "DAUG"){
          memberDetails["showDependent"] = true;
          memberDetails["popupType"] = "2";
          memberDetails["showAddAnotherBtn"] = true;
        }else if(term[0].id == "SELF"){
          memberDetails["showMarried"] = true;
          memberDetails["disableRemove"] = true;
        }else if(term[0].id == "SPOU"){
          memberDetails["disableRemove"] = true;
        }
      }
      if(this.apiMembers[a].title.toLowerCase().indexOf("son")>=0){
        memberDetails["imgUrl"] ="assets/imgs/son.png";
      }else if(this.apiMembers[a].title.toLowerCase().indexOf("daughter")>=0){
        memberDetails["imgUrl"] ="assets/imgs/daughterIcon.png";
      }else if(this.apiMembers[a].title.toLowerCase().indexOf("father")>=0){
        memberDetails["imgUrl"] ="assets/imgs/fatherIcon.png";
      }else if(this.apiMembers[a].title.toLowerCase().indexOf("mother")>=0){
        memberDetails["imgUrl"] ="assets/imgs/motherIcon.png";
      }else if(term.length==0){
        memberDetails["imgUrl"] ="assets/imgs/self.png";
      }
      memberDetails["detailsSaved"] =false;
      this.membersShow.push(memberDetails);
    }
  }
  
  displayAge(agecode){
    for(let i = 0; i < this.apiAgeGroup.length; i++){
      if(agecode == this.apiAgeGroup[i].ageCode){
        return this.apiAgeGroup[i].title+" years";
      }
    }
  }
  displayAmt(insuranceCode){
    for(let i = 0; i < this.QuickQuotePlans.length; i++){
      if(this.maximumInsuAmtCode!='' && insuranceCode == this.QuickQuotePlans[i].sicode){
        this.insuredPlanTitle = (this.QuickQuotePlans[i].title).toLowerCase();
      }else if( this.PolicyTypeSelected ==  this.familyFloaterCode && insuranceCode == this.QuickQuotePlans[i].sicode){
        this.insuredPlanTitle = (this.QuickQuotePlans[i].title).toLowerCase();
        this.familyInsuredPlanType = this.QuickQuotePlans[i].plancode;
      }
      if(insuranceCode == this.QuickQuotePlans[i].sicode){
        return this.QuickQuotePlans[i].amtSelectedTitle;
      }
    }
  }

  saveMemberDetails(popupNumber,addAnother){  
    //this.displayAge(this.memberAge);
    let ageValue = this.displayAge(this.memberAge);


if(this.popupMemberData.code == "SELF" || this.popupMemberData.code == "SPOU" || this.popupMemberData.code == "MOTH" || this.popupMemberData.code == "FATH"){

if(ageValue == "0-17 years"){
this.showToast("You cannot select age below 17 years.");
return
}
}


// if(this.editUserData == true){
//   for(let i = 0; i <this.membersInsured.length; i++){
//     if(this.membersInsured[i].code == this.popupMemberData.code){
//     var number_of_elements_to_remove = 1;
//     var removed_elements = this.membersInsured.splice(i, number_of_elements_to_remove);
//     this.membersInsured[i].push(this.membersInsured[i]);
//     console.log("latest Array:" + this.membersInsured)
//     }
//     }
// }


    console.log(this.editUserData+" <-this.editUserData");
    if(popupNumber == 1 || popupNumber==2){
      if(this.slide == '' && this.showInsurenceDiv){
        this.showToast("Please select insurance amount.");
      }else if(this.memberAge == 0 && popupNumber ==1){
        this.showToast("Please select member's age.");
      }else if(this.childsAge == 0 && popupNumber ==2){
        this.showToast("Please select member's age.");
      }else if(this.marriedStatus == ""){
        this.showToast("Please select marital status");
      }else{
        this.showTransBaground = false;  
        if(this.popupMemberData.code == "SELF"){
          this.maximumInsuAmtCode = this.slide;
          this.membersShow[0].detailsSaved = true;
          this.membersShow[0].age = this.memberAge;
          this.membersShow[0].ageText = this.displayAge(this.memberAge);
          this.membersShow[0].insuredCode = this.slide;
          this.membersShow[0].PolicyType = this.PolicyTypeSelected;
          this.membersShow[0].insuredAmtText = this.displayAmt(this.slide);
          this.membersShow[0].smoking = this.memberSmokes;
          this.membersShow[0].insuredAmtInDigit = "";
          if(this.memberSmokes){
            this.membersShow[0].smokingText = "Smoking";
          }else{
            this.membersShow[0].smokingText = "";
          }
          // if(this.memberMarried){
          //   this.membersShow[0].maritalStatus = "Married";
          //   this.membersShow[0].maritalCode = "M"
          //   this.hidePopupSmokerDiv = true;
          //   document.getElementById("addMember").style.marginBottom='5px';
          //   this.hideProceedButton = true;
          //   this.membersShow[0].showMarried = true;
          // }else{
          //   document.getElementById("addMember").style.marginBottom='60px';
          //   this.hideProceedButton = false;
          //   this.membersShow[0].maritalStatus = ""; 
          //   this.hidePopupSmokerDiv = false;
          //   this.membersShow[0].maritalStatus = "Single";
          //   this.membersShow[0].maritalCode = "S";
          //   this.membersShow[0].showMarried = false;
          // }

          if(this.marriedStatus == "Married"){
            this.membersShow[0].maritalStatus = "Married";
            this.membersShow[0].maritalCode = "M"
            this.hidePopupSmokerDiv = true;
            //document.getElementById("addMember").style.marginBottom='5px';
            //this.hideProceedButton = true;
            this.membersShow[0].showMarried = true;
            
            //this.popupImg = memberData.imgUrl;
            //this.popupMemberData = memberData;

          }else if(this.marriedStatus == "Single" || this.marriedStatus == "Divorced" || this.marriedStatus == "Widow / Widower"){
            //document.getElementById("addMember").style.marginBottom='60px';
            this.hideProceedButton = false;
            this.membersShow[0].maritalStatus = ""; 
            this.hidePopupSmokerDiv = false;
            if(this.marriedStatus == "Single"){
              this.membersShow[0].maritalStatus = "Single";
              if(this.membersInsured.length == 1){
                this.hideProceedButton = true;
              }
              //document.getElementById("addMember").style.marginBottom='5px';             
            }else if(this.marriedStatus == "Divorced"){
              //document.getElementById("addMember").style.marginBottom='5px';
              this.hideProceedButton = true;
              this.membersShow[0].maritalStatus = "Divorced";
            }else if(this.marriedStatus == "Widow / Widower"){
              //document.getElementById("addMember").style.marginBottom='5px';
              this.hideProceedButton = true;
              this.membersShow[0].maritalStatus = "Widow / Widower";
            }
            
            this.membersShow[0].maritalCode = "S";
            this.membersShow[0].showMarried = false;
          }
          this.slide ='';
          this.memberAge = 0;
          this.memberSmokes = false;
          //this.memberMarried = false;
          //this.showMemberPopup = false;
          this.showChildPopup = false;
          this.hidePopupMarriedDiv = false;
          this.hideMemberPopup = false;
          // if(this.editUserData)
          // {
          //   console.log("this.editUserData "+this.editUserData+"from saveMemberDetails");
          //   this.membersInsured.splice(0, 1, this.membersShow[0]);

          //   if(  (this.membersInsured.length >= 2 && this.PolicyTypeSelected == this.familyFloaterCode)|| ( this.membersInsured.length >= 1 &&  this.PolicyTypeSelected == this.individualCode)){
          //     this.disableProceedBt = false;
          //     this.hideProceedButton =false;
          //   }else{
          //     this.disableProceedBt = true;
          //   }

          // }else{
          //   this.membersInsured.push(this.membersShow[0]);
          // }
          if(this.editUserData){
            this.membersInsured.splice(0, 1, this.membersShow[0]);
            // for(let i = 0; i <this.membersInsured.length; i++){
            // if(this.membersInsured[i].code == this.popupMemberData.code){
            // var number_of_elements_to_remove = 1;
            // var removed_elements = this.membersInsured.splice(i, number_of_elements_to_remove);
            // this.membersInsured.push(this.membersInsured[i]);
            // console.log(this.membersInsured)
            //}
            //}
          }else{
          this.membersInsured.push(this.membersShow[0]);
          }

          this.showTransBaground = true;
          if( this.PolicyTypeSelected == this.individualCode){
            this.disableProceedBt = false;
          }
          if(this.membersInsured.length == 1){
            this.hideProceedButton == true;
          }
          this.editUserData = false;
        }else{
          
          // if(this.editUserData){
          //   for(let i = 0; i <this.membersInsured.length; i++){
          //     if(this.membersInsured[i].code == this.popupMemberData.code){
          //     var number_of_elements_to_remove = 1;
          //     var removed_elements = this.membersInsured.splice(i, number_of_elements_to_remove);
          //     this.membersInsured.push(this.membersInsured[i]);
          //     console.log("latest Array:" + this.membersInsured)
          //     }
          //     } 
          // }else{
          // this.membersInsured.push(this.membersInsured[i]);
          // }
        
          // if(this.editUserData == true){
          //   this.familyInsuredPlanTypeText == "";
          // }
          for(let i = 0;i < this.membersShow.length;i++){
            if(this.familyInsuredPlanTypeText == 'VITAL' && this.membersInsured.length >=6 && this.editUserData == false){
              this.showToast("You can insure maximum 6 members with Vital Plan.");
              break;
            }else if( this.membersInsured.length >=15 ){
              this.showToast("You can insure maximum 15 members.");
              break;
            }
            if(this.popupMemberData.code == this.membersShow[i].code){

              this.membersShow[i].smoking = this.memberSmokes;
              if(this.memberSmokes){
                this.membersShow[i].smokingText = "Smoking";
              }else{
                this.membersShow[i].smokingText = "";
              }
             
              if(this.familyInsuredPlanTypeText == 'VITAL' && this.vitalMembers.indexOf(this.membersShow[i].code)<0){
                this.showToast("Vital Plan includes only immediate family members.");
              }else if(this.familyInsuredPlanTypeText == 'VITAL' && this.vitalMembers.indexOf(this.membersShow[i].code)>=0){
                if(this.membersShow[i].title ==this.popupTitle ){
                  this.saveMemberData(i,popupNumber,addAnother);
                  break;
                }/* else if(this.popupMemberData.code != "SON" && this.popupMemberData.code != "DAUG"){
                  this.saveMemberData(i,popupNumber,addAnother);
                  break;
                } */
              }else if(this.familyInsuredPlanTypeText != 'VITAL'){
                if( /* (this.popupMemberData.code == "SON" || this.popupMemberData.code == "DAUG") &&  */this.membersShow[i].title ==this.popupTitle ){
                  this.saveMemberData(i,popupNumber,addAnother);
                  break;
                }/* else if(this.popupMemberData.code != "SON" && this.popupMemberData.code != "DAUG"){
                  this.saveMemberData(i,popupNumber,addAnother);
                  break;
                } */
              }
            }
          }
        }
       
      }
    }
  }

  editMemberDetails($event, member){
    // for(let i = 0; i <this.membersInsured.length; i++){
    //     if(this.membersInsured[i].code == member.code){
    //     var number_of_elements_to_remove = 1;
    //     var removed_elements = this.membersInsured.splice(i, number_of_elements_to_remove);
    //     console.log(this.membersInsured)
    //     }
    //     }
    event.stopPropagation();
    this.slide = member.insuredCode;
    this.populateInsurenceAmt(1,member.code);

      this.editUserData = true;
      this.addMember = false;
      this.memberSmokes = false;
      //this.memberMarried = false;
      this.showTransBaground = false;    
      this.popupTitle = member.title;
      this.popupImg = member.imgUrl;
      this.hideMemberPopup = true;
      this.popupMemberData = member;
      this.memberAge = member.age;
      // this.personHt = member.Height;
      // this.personWt = member.Weight;
      this.hidePopupSmokerDiv = false;
      this.hidePopupMarriedDiv =false;
      if(member.code=="SELF"){
        this.hidePopupSmokerDiv = true;
        this.hidePopupMarriedDiv =true;
        this.memberSmokes = member.smoking;
          if(member.maritalCode == "M"){
            //this.memberMarried = true;
          }
      }
      if(member.code=="SON" || member.code == "DAUG"){
      
this.showTransBaground = false; 
this.hideMemberPopup = false;
this.addMember = true;
this.popupMember = "Child"
this.showChildPopup = true;
this.childsAge = member.age;
        

      }else if(member.code == "SPOU"){
        this.hidePopupSmokerDiv = true;
        this.showTransBaground = false;    
        this.hideMemberPopup = true;
        this.memberSmokes = member.smoking;
      }else if(member.code == "FATH"){
        this.hidePopupSmokerDiv = true;
        this.showTransBaground = false;    
        this.hideMemberPopup = true;
        this.memberSmokes = member.smoking;
      }else if(member.code == "MOTH"){
        this.hidePopupSmokerDiv = true;
        this.showTransBaground = false;    
        this.hideMemberPopup = true;
        this.memberSmokes = member.smoking;
      }
  }

  

  saveMemberData(indexNumber,popupNumber,addAnother){
    this.membersShow[indexNumber].detailsSaved = true;
    if(popupNumber == 1){
      this.membersShow[indexNumber].age = this.memberAge;
      this.membersShow[indexNumber].ageText = this.displayAge(this.memberAge);
    }else{
      this.membersShow[indexNumber].age = this.childsAge;
      this.membersShow[indexNumber].ageText = this.displayAge(this.childsAge);
    }
    this.membersShow[indexNumber].insuredCode = this.slide;
    this.membersShow[indexNumber].insuredAmtText = this.displayAmt(this.slide);
    this.membersShow[indexNumber].PolicyType = this.PolicyTypeSelected;
    this.hideMemberPopup = false;
    this.showMemberPopup = false;
    this.showChildPopup = false;
    this.hidePopupSmokerDiv = false;
    this.hidePopupMarriedDiv = false;
    this.slide ='';
    this.memberAge = 0;
    this.childsAge= 0;
    if(this.membersShow[indexNumber].code == 'SPOU'){

      if(this.editUserData){
        this.membersInsured.splice(1, 1, this.membersShow[indexNumber]);
      }else{
        this.membersInsured.splice(1, 0, this.membersShow[indexNumber]);
      }

    }else{
      if(this.editUserData){
        for(let i = 1; i < this.membersInsured.length ; i++){
          if(this.membersShow[indexNumber].code == this.membersInsured[i].code && this.membersShow[indexNumber].title == this.membersInsured[i].title){
            this.membersInsured.splice(i, 1, this.membersShow[indexNumber]);
            break;
          }
        }
    }else{
      this.membersInsured.push(this.membersShow[indexNumber]);
    }
     
    }
    if(addAnother == 1){
      this.addMemberClicked2(this.popupMemberData.code);
    }
    this.showTransBaground = true;

    if(  (this.membersInsured.length >= 2 && this.PolicyTypeSelected == this.familyFloaterCode)|| ( this.membersInsured.length >= 1 &&  this.PolicyTypeSelected == this.individualCode)){
      this.disableProceedBt = false;
    }else{
      //this.disableProceedBt = true;
    }
    this.editUserData = false;
  }

  getSumInsuredArray(){
    this.QuickQuotePlans =[];
    this.QuickQuotePlans = this.appService.getSumInsuredArray(this.apiPlanType);
  }

  populateInsurenceAmt(Number, memberCode){
    this.insurenceAmtToShow = [];
    for(let i = 0; i < this.QuickQuotePlans.length;i++){
      if(this.maximumInsuAmtCode!='' && this.QuickQuotePlans[i].sicode == this.maximumInsuAmtCode){
        this.insurenceAmtToShow.push(this.QuickQuotePlans[i]);
        this.insurenceAmtToShow =  this.insurenceAmtToShow.slice(this.insurenceAmtToShow.length - 3, this.insurenceAmtToShow.length);              
        break;
      }else{
        this.insurenceAmtToShow.push(this.QuickQuotePlans[i]);
      }
      if(Number == "0" || Number == 0){
        this.familyInsurenceAmt.push(this.QuickQuotePlans[i]);
      }
    }
    console.log("InsuranceAmount: " + this.familyInsurenceAmt);
  }

  
  clearMemberDetails(event : Event, member){
    event.stopPropagation();
    //document.getElementById("addMember").style.marginBottom='5px';
    this.hideProceedButton = true;
    for(let i = 0; i < this.membersShow.length;i++){
      if(member.code == "SELF"){
        this.membersShow[i].detailsSaved = false;
        this.membersShow[i].smoking = false;
        member.insuredAmtText = "";
        member.ageText = "";
        member.smokingText = "";
        member.maritalStatus = "";
        this.maximumInsuAmtCode = '';
        this.insuredPlanTitle = '';
      }else if(member.code == this.membersShow[i].code && member.title == this.membersShow[i].title){
        this.membersShow[i].detailsSaved = false;
        this.membersShow[i].smoking = false;
        member.insuredAmtText = "";
        member.ageText = "";
        member.smokingText = "";
        member.maritalStatus = "";
        break;
      }
    }
    for(let i = 0 ; i < this.membersInsured.length ; i++){
      if(member.code == "SELF"){
        this.membersInsured = [];
      }else if(member.title == this.membersInsured[i].title){
        this.membersInsured.splice(i, 1);
        break;
      }
    }

    console.log("afterDelete" + JSON.stringify(this.membersInsured));
    

    if(  (this.membersInsured.length >= 2 && this.PolicyTypeSelected == this.familyFloaterCode)|| ( this.membersInsured.length >= 1 &&  this.PolicyTypeSelected == this.individualCode)){
      this.disableProceedBt = false;
      this.hideProceedButton = false;
    }else{
      this.disableProceedBt = true;
      //this.hideProceedButton = true;
    }
  }


  addMemberClicked2(memberCode){
    for(let i = 0; i < this.membersShow.length;i++){
      
      if(this.membersShow[i].code == memberCode &&(memberCode == "SON" || memberCode == "DAUG")  ){
        let memberName = '';
        let term = this.membersShow.filter(person => person.code == memberCode );
        if(memberCode == "SON"){
          memberName = 'Son';
          this.membersShow[i].title = 'Son1'
        }else if(memberCode == "DAUG"){
          memberName = 'Daughter';
          this.membersShow[i].title = 'Daughter1'
        }
        this.membersShow.splice(i+term.length, 0, {"code":this.membersShow[i].code,
        "title":memberName+ (term.length +1),
        "imgUrl":term[0].imgUrl,
        "smoking" : false,
        "showDependent":true,
        "showAddAnotherBtn":true,
        "smokingText":"",
        "showMarried":false,
        "maritalStatus":"",
        "age":"",
        "ageText":"",
        "insuredCode":"",
        "insuredAmtText":"",
        "PolicyType": '',
        "disableRemove":false,
        "ShowInsurenceAmt":false,
        "showFamilyMemberOnScreen":true,
        "popupType":"2",
        "detailsSaved":false
      }); 
        break;
      }
    }   
  }
  addMemberClicked(event : Event, k){
    for(let i = 0; i < this.membersShow.length;i++){
      if(this.membersShow[i].code == k.code &&(k.code == "SON" || k.code == "DAUG")){
        let term = this.membersShow.filter(person => person.code == k.code );
        let memberName = '';
        if(k.code == "SON"){
          memberName = 'Son';
          this.membersShow[i].title = 'Son1'
        }else if(k.code == "DAUG"){
          memberName = 'Daughter';
          this.membersShow[i].title = 'Daughter1'
        }
        this.membersShow.splice(i+term.length, 0, {"code":this.membersShow[i].code,
        "title":memberName+ (term.length +1),
        "imgUrl":k.imgUrl,
        "smoking" : false,
        "showDependent":true,
        "showAddAnotherBtn":true,
        "smokingText":"",
        "showMarried":false,
        "maritalStatus":"",
        "age":"",
        "ageText":"",
        "insuredCode":"",
        "insuredAmtText":"",
        "PolicyType": '',
        "disableRemove":false,
        "ShowInsurenceAmt":false,
        "showFamilyMemberOnScreen":true,
        "popupType":"2",
        "detailsSaved":false
      }); 
        break;
      }
    }   
  }

  getQuoteClicked(){
    this.displayAmt(this.familySlide);
    if(this.membersInsured.length<2 && this.PolicyTypeSelected ==  this.familyFloaterCode ){
      this.showToast("Minimum two beneficiaries are required for FLOATER policy.");
    }else if(this.membersInsured.length == 0){
      this.showToast("Please insure atleast 'self' to proceed.");
    }else if( (this.membersInsured.length == 1 && this.membersInsured[0].maritalStatus == "Married") || (this.membersInsured.length > 1 && this.membersInsured[1].code != "SPOU" && this.membersInsured[0].maritalStatus == "Married") ){
      this.showToast("Please enter spouse details.");
    }else if(this.PolicyTypeSelected == this.individualCode){
      sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.membersInsured);
      sessionStorage.MembersShownOnforQuickQuote = JSON.stringify(this.membersShow);
      this.navCtrl.push(QuickQuoteResultPage, {"loginStatus": this.loginStatus});
    }else if(this.PolicyTypeSelected ==  this.familyFloaterCode && this.familySlide == ''){
      this.showToast("Please select insurance amount.");
    }else if(this.PolicyTypeSelected ==  this.familyFloaterCode && this.familySlide != ''){
      this.membersInsured[0].insuredCode = this.familySlide;
      this.membersInsured[0].insuredPlanTypeCode = this.familyInsuredPlanType;
      this.membersInsured[0].insuredPlanTypeText = this.familyInsuredPlanTypeText;
      if(this.familyInsuredPlanTypeText == 'VITAL'){
        for(let i= 0 ; i < this.membersInsured.length ;i++ ){
          if(this.vitalMembers.indexOf(this.membersInsured[i].code)<0){
            this.showToast("Vital Plan includes only immediate family members.");
            break;
          }else if( i == (this.membersInsured.length -1)){
            sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.membersInsured);
            sessionStorage.MembersShownOnforQuickQuote = JSON.stringify(this.membersShow);
            this.navCtrl.push(QuickQuoteResultPage, {"loginStatus": this.loginStatus});
          }
        }
      }else{
        sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.membersInsured);
        this.navCtrl.push(QuickQuoteResultPage);
      }
    }
  }
  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  goToLoginPage(){
    this.navCtrl.push(LoginPage);
  }

  memberClicked(event : Event,memberToToggle){
    if(memberToToggle.code!= "SELF"){
      for(let i =0; i < this.membersShownOnPopup.length;i++){
        if(memberToToggle.code == this.membersShownOnPopup[i].code && memberToToggle.title == this.membersShownOnPopup[i].title){
          this.membersShownOnPopup[i].memberSelectedOnAddmember = !this.membersShownOnPopup[i].memberSelectedOnAddmember
        }
      }
    }
  }
  hideShowAddmember(Num){
    if(Num == 1){
      this.showTransBaground2 = !this.showTransBaground2;   
      console.log(this.membersShow);
      this.membersShownOnPopup = JSON.parse(JSON.stringify(this.membersShow));
      this.isClassOneActive = true;
      this.showAddMember = true;
    }else if(Num == 2){
      this.isClassOneActive = false;
      setTimeout( () => { 
            this.showAddMember = false;
            this.showTransBaground2 = !this.showTransBaground2;  
      }, 1000);
    }else if(Num == 3){
      for(let i = 0; i < this.membersShownOnPopup.length; i++){
        if(!this.membersShownOnPopup[i].showFamilyMemberOnScreen && this.membersShownOnPopup[i].memberSelectedOnAddmember){
          this.membersShow[i].showFamilyMemberOnScreen = this.membersShownOnPopup[i].memberSelectedOnAddmember;
          this.membersShow[i].memberSelectedOnAddmember = this.membersShownOnPopup[i].memberSelectedOnAddmember;
        }
      }   
      this.isClassOneActive = false;
      setTimeout( () => { 
            this.showAddMember = false;
            this.showTransBaground2 = !this.showTransBaground2;  
      }, 1000);
    }
  }

  selectFamilyInsuredSum(amt){
    console.log("amount: " + JSON.stringify(amt));
    this.familyInsuredPlanTypeText = amt.title;
    this.familySlide = amt.sicode;
    this.hideDisableCards = true;
  }

  changePolicyType(){
    //this.membersInsured = [];
    this.marriedStatus="";
    
    // for(let i = 0; i < this.membersShow.length;i++){
    //     this.membersShow[i].detailsSaved = false;
    //     this.membersShow[i].insuredAmtText = "";
    //     this.membersShow[i].ageText = "";
    //     this.membersShow[i].smokingText = "";
    //     this.membersShow[i].maritalStatus = "";
    //     this.maximumInsuAmtCode = '';
    //     this.familyInsuredPlanTypeText = '';
    //     if( this.PolicyTypeSelected == this.individualCode){
    //       this.membersShow[i].ShowInsurenceAmt = true;
    //       this.showInsurenceDiv = true;
    //       this.individualDiscount = true;
    //       this.familyInsuredPlanTypeText == 'VITAL';
    //       this.familySlide = '';
    //       this.hideDisableCards = true;
    //     }else if(this.PolicyTypeSelected == this.familyFloaterCode){
    //       this.membersShow[i].ShowInsurenceAmt = false;
    //       this.showInsurenceDiv = false;
    //       this.individualDiscount = false;
    //       this.familyInsuredPlanTypeText == '';
    //       this.hideDisableCards = false;
    //     }
    // }

    if(this.membersInsured.length != 0){
      const alert = this.alertCtrl.create({
        title: 'Alert',
        message: 'If you opt for this Policy, the members will be unselected from the Family Floater policy.',
        buttons: [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              if(this.PolicyTypeSelected == "HTI"){
                this.PolicyTypeSelected = "HTF";
                //this.hideProceedButton = true;
              }else{
                this.PolicyTypeSelected = "HTI";
                //this.hideProceedButton = true;
              }            
            }
        },{
            text: 'Continue',
            handler: () => {
              sessionStorage.isViewPopped = "";
              this.membersInsured = [];
              this.disableProceedBt = true;
              this.hideProceedButton = true;
              this.membersShow = [];
              this.membersArrayFunction();              
              //document.getElementById("addMember").style.marginBottom='5px';              
              for(let i = 0; i < this.membersShow.length;i++){
                  this.membersShow[i].detailsSaved = false;
                  this.membersShow[i].insuredAmtText = "";
                  this.membersShow[i].ageText = "";
                  this.membersShow[i].smokingText = "";
                  this.membersShow[i].maritalStatus = "";
                  this.maximumInsuAmtCode = '';
                  this.insuredPlanTitle = '';
                  if( this.PolicyTypeSelected == this.individualCode){
                    this.membersShow[i].ShowInsurenceAmt = true;
                    this.showInsurenceDiv = true;
                    this.individualDiscount = true;
                    this.insuredPlanTitle == 'vital';
                    this.familySlide = '';
                    this.hideDisableCards = true;
                  }else if(this.PolicyTypeSelected == this.familyFloaterCode){
                    this.hideDisableCards = false;
                    this.membersShow[i].ShowInsurenceAmt = false;
                    this.showInsurenceDiv = false;
                    this.individualDiscount = false;
                    this.insuredPlanTitle == '';
                  }
              }
            }
        }]
     });
     alert.present();
    }else{
      this.membersInsured = [];
      this.disableProceedBt = false;
      this.hideProceedButton = true;
      for(let i = 0; i < this.membersShow.length;i++){
          this.membersShow[i].detailsSaved = false;
          this.membersShow[i].insuredAmtText = "";
          this.membersShow[i].ageText = "";
          this.membersShow[i].smokingText = "";
          this.membersShow[i].maritalStatus = "";
          this.maximumInsuAmtCode = '';
          this.insuredPlanTitle = '';
          if( this.PolicyTypeSelected == this.individualCode){
            this.membersShow[i].ShowInsurenceAmt = true;
            this.showInsurenceDiv = true;
            this.individualDiscount = true;
            this.insuredPlanTitle == 'vital';
            this.familySlide = '';
            this.hideDisableCards = true;
          }else if(this.PolicyTypeSelected == this.familyFloaterCode){
            this.hideDisableCards = false;
            this.membersShow[i].ShowInsurenceAmt = false;
            this.showInsurenceDiv = false;
            this.individualDiscount = false;
            this.insuredPlanTitle == '';
          }
      }
    }
  }


                                                              
}
