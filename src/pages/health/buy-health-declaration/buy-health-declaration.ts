import { Component, NgModule } from '@angular/core';
import { IonicPage, Platform, NavController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { BuyPolicyReviewPage } from '../buy-policy-review/buy-policy-review';
import { VerifyDetailsPage } from '../verify-details/verify-details';
import { IonicPageModule } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { LandingScreenPage } from '../landing-screen/landing-screen';
import { LoginPage } from '../login/login';
/**
 * Generated class for the BuyHealthDeclarationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    BuyHealthDeclarationPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyHealthDeclarationPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-buy-health-declaration',
  templateUrl: 'buy-health-declaration.html',
})
export class BuyHealthDeclarationPage {

  selectedCardArray;
  canShowToast = true;
  //medicalStatus = false;
  medicalStatus = "No";
  memberSelection;
  questionDiv = true;
  membersDeclarationArray =[];
  memberDetails;
  ReferenceNo;
  checkedValue;
  ClientDetails: any = [];
  loading;
  pID;
  thankyouPopup=true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;

  constructor(private platform: Platform,  private loadingCtrl : LoadingController ,public appService: AppService, public navCtrl: NavController, public navParams: NavParams, public toast: ToastController) {
    this.memberDetails = navParams.get("memberDetails");
    //prachi---
    sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
    this.selectedCardArray = navParams.get("planCardDetails");
    console.log("Member: " + JSON.stringify(this.memberDetails));
    console.log("card: " + JSON.stringify(this.selectedCardArray));

    // platform.registerBackButtonAction(() => {
    //   console.log("backPressed 1");
    //   this.onBackClick();
    // },1);
  }

  ionViewDidLoad() {    
    console.log('state: ' + this.questionDiv);
    document.getElementById("splash").style.display='none';
    console.log(this.memberDetails);
    this.ReferenceNo = this.selectedCardArray.ReferenceNo;

    if(sessionStorage.isViewPopped == "true"){
      this.memberDetails = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      sessionStorage.isViewPopped = "";
    }  
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  // clickCheckBox(){
  //   console.log("checked: " + this.checkedValue);
  // }

  medicalStatusChange(){
    console.log("status :" + this.medicalStatus);
  }

//   selectedCheckboxMember(member){
//     console.log("check: " + JSON.stringify(member));
//     console.log("div: " + member.medicalDeclaration);
   
//     if(this.medicalStatus == true){
//       for(let i = 0; i < this.memberDetails.length ; i++){

//         if(this.memberDetails[i].nameOfProposer == member.nameOfProposer  && this.memberDetails[i].medicalDeclaration){
//           this.memberDetails[i].showMedicalQuestions = false;
//         }else{
//           this.memberDetails[i].showMedicalQuestions = true;
//         }
//         if(this.memberDetails[i].nameOfProposer != member.nameOfProposer){
//           this.memberDetails[i].showMedicalQuestions = true;
//          // this.memberDetails[i].medicalDeclaration = true;
//         }
//       }
//       console.log(this.memberDetails);

// /*         if(member.medicalDeclaration == true){
        

//         }else{
//           this.questionDiv = true;
//         } */
//     }else{

//       this.showToast("Please check atfirst if your are medicaly unfit");      
//     }
//     //console.log("selected :" + this.memberSelection);
//   }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  medicalIssue(value, member){
    console.log("medi" + value);
    this.medicalStatus = value;
    console.log("member: " + JSON.stringify(member));
    if(value == "Yes"){
      member.medicalDeclaration = true;
    }else{
      member.medicalDeclaration = false;
    }
    console.log("boolean: " +member.medicalDeclaration);
    
  }

  proceedReviewPage(){
  
    
    
    // if(this.medicalStatus == false){
    //   this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});
    // }else{
    //   if(this.membersDeclarationArray.length < 1){
    //     this.showToast("Please select minimum one member who have any medical condition or been hospitalized in the past");
    //   }else{

    //     this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});
    //   }
    // }
    
  //   if(this.medicalStatus == undefined){
  //     this.showToast("Please select your member medical status");
  //   }else{
  //     for(let i=0; i< this.memberDetails.length; i++){
  //       if(this.memberDetails[i].medicalDeclaration == false){
  //         proceed = true;
  //       }else{
  //         proceed = false;
  //         break;
  //       }
  //     }

  //     if(proceed == true){
  //       this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});      
  //     }else{
  //       this.showToast("due to medical conditions, you will have to contact your sales person in the nearest branch");
  //     }
  // } 
    // console.log("checked: " + this.checkedValue);
    // if(this.checkedValue){
    //   this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});     
    // }else{
    //   this.showToast("Please choose medical declaration");
    // }

    // if(this.medicalStatus == undefined){
    //        this.showToast("Please select your member medical status");
    // }else if(this.medicalStatus == "No"){
    //   this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});     
    // }else{

      let proceed = 0;
      for(let i=0; i< this.memberDetails.length; i++){
        console.log("int: for "+ proceed);
          if(this.memberDetails[i].medicalDeclaration == true){           
            proceed++;
            console.log("if"+ proceed);
          }
      }

    
      
    
    if(proceed == 0){
      var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
      this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
      console.log("Data: " + JSON.stringify(sendData));
            
    }else{
      this.showToast("In view of medical declaration, we apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
    }
  }

  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          // this.pID = Resp.Proceed_PolicyResult.Data.pID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});      
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
    });
  }
  

  onBackClick(){
    sessionStorage.isViewPopped = "true";
    sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
    this.navCtrl.pop();
  }

  onSaveAndEmail(){
    let proceed = 0;
    for(let i=0; i< this.memberDetails.length; i++){
      console.log("int: for "+ proceed);
        if(this.memberDetails[i].medicalDeclaration == true){           
          proceed++;
          console.log("if"+ proceed);
        }
    }
  
  if(proceed == 0){
    //this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("Eproposal: " + JSON.stringify(sendData));
    
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }else{
    this.showToast("due to medical conditions, you will have to contact your sales person in the nearest branch");
  }
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": this.selectedCardArray.QuotationID,
      "UID": this.selectedCardArray.UID,
      "stage":"4",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.selectedCardArray.Request.PolicyType ,//"HTF",
        "Duration": this.selectedCardArray.Request.Duration,
        "Installments": this.selectedCardArray.Request.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails": this.selectedCardArray.Request.BeneficiaryDetails.Member.length == undefined ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberDetails,
        "CardDetails": this.selectedCardArray
      }
    }
    console.log("HealthEproposal: " + JSON.stringify(sendRequest));
    return sendRequest;
  }

  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.Request.BeneficiaryDetails.Member.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberDetails[0].Gender;
    let numericAge = this.getAge(this.memberDetails[0].nomineeAge).toString();
    
    BeneDetails.push({
      "MemberId": cardDetails.Request.BeneficiaryDetails.Member.MemberId ,
      "InsuredName": this.memberDetails[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberDetails[0].occupationCode,
      "CoverType": cardDetails.Request.BeneficiaryDetails.Member.CoverType,
      "SumInsured": cardDetails.Request.BeneficiaryDetails.Member.SumInsured,
      "DeductibleDiscount": cardDetails.Request.BeneficiaryDetails.Member.DeductibleDiscount,
      "Relation": cardDetails.Request.BeneficiaryDetails.Member.Relation,
      "NomineeName": this.memberDetails[0].nominee,
      "NomineeRelation": this.memberDetails[0].NomineeRelation,
      "AnualIncome": "",
      "Height": cardDetails.Request.BeneficiaryDetails.Member.Height,
      "Weight": cardDetails.Request.BeneficiaryDetails.Member.Weight,
      "NomineeAge": numericAge,
      "AppointeeName": this.memberDetails[0].appointeeName,
      "AptRelWithominee": this.memberDetails[0].appointeeRelation,
      "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member.MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      }
    })
    return BeneDetails;
  }

  getClientNode(){
    let age = this.memberDetails[0].age;
    let clientSalution = "MR";
    if(this.memberDetails[0].Gender == "F"){
      clientSalution = "MRS";
    }
    console.log("email: " + this.memberDetails[0].email);
    
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberDetails[0].proposerName,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberDetails[0].Gender,
      "MaritalStatus": this.memberDetails[0].maritalCode,
      "Occupation": this.memberDetails[0].occupationCode,
      "PANNo": this.memberDetails[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberDetails[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetails[0].pincode,
        "City": this.memberDetails[0].city,
        "State": this.memberDetails[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": "",
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      }
    });
    console.log("client: " + JSON.stringify(this.ClientDetails));
   
    return this.ClientDetails[this.ClientDetails.length -1];

     
  
  }

  getBeneficiaryDetails(cardDetails){
    var content = document.querySelector('div .nomineePage').children;

    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.Request.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberDetails.length;j++){
        if( (this.memberDetails[j].code).toUpperCase()== (cardDetails.Request.BeneficiaryDetails.Member[i].Relation).toUpperCase() ){

        
        
          let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
          let gendr = this.memberDetails[j].Gender;
          let numericAge = this.getAge(this.memberDetails[j].nomineeAge).toString();
          BeneDetails.push({
            "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
            "InsuredName": this.memberDetails[j].proposerName,
            "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
            "InsuredGender": gendr,
            "InsuredOccpn": this.memberDetails[j].occupationCode,
            "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
            "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
            "DeductibleDiscount": "0",
            "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
            "NomineeName": this.memberDetails[j].nominee,
            "RelationName":this.memberDetails[i].title,
            "NomineeRelation":this.memberDetails[j].NomineeRelation,
            "AnualIncome": "",
            "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
            "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
            "NomineeAge": numericAge,
            "AppointeeName": this.memberDetails[j].appointeeName,
            "AptRelWithominee": this.memberDetails[j].AptRelWithominee,
            "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
            "PreExstDisease": "N",
            "DiseaseMedicalHistoryList": {
              "DiseaseMedicalHistory": {
                "PreExistingDiseaseCode": "",
                "MedicalHistoryDetail": ""
              }
            }
          })
        }
  
      }
  
    }
    return BeneDetails;
  
  } 

  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365);
  }


  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
    });
  }
  
  callEProposalService(){       
    var eProposalData = {
          "QuotationID": this.selectedCardArray.QuotationID,
          "UID": this.selectedCardArray.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }
  
  sendEProposerData(serviceData){
    this.presentLoadingDefault();
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup=false;
          //this.closeModalPop();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp && resp.ReturnCode == "807"){
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          //this.showToast(resp.ReturnMsg);
          //this.navCtrl.push(LoginPage);
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          
        }else if(resp.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          //this.navCtrl.push(LoginPage);
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          //this.showToast(resp.ReturnMsg);
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
       
      }, (err) => {
      console.log(err);
      });
  }

  OkButton(){
    this.thankyouPopup = true;
    this.navCtrl.push(LandingScreenPage);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }



}
