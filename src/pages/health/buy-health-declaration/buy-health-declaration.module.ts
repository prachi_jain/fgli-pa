import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyHealthDeclarationPage } from './buy-health-declaration';

@NgModule({
  declarations: [
    //BuyHealthDeclarationPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyHealthDeclarationPage),
  ],
})
export class BuyHealthDeclarationPageModule {}
