import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { IonicPage,Platform, NavController, NavParams, ToastController, LoadingController,Content, List } from 'ionic-angular'
import { BuyHealthDeclarationPage } from '../buy-health-declaration/buy-health-declaration';
import { NomineeDetailsPage } from '../nominee-details/nominee-details';
import { LandingScreenPage } from '../landing-screen/landing-screen';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material';
import { BuyPolicyReviewPage } from '../buy-policy-review/buy-policy-review';
import { DatePicker } from '@ionic-native/date-picker';
import { getLocaleFirstDayOfWeek } from '@angular/common';
import { AppService } from '../../../providers/app-service/app-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import * as $ from 'jquery';
import { LoginPage } from '../login/login';

/**
 * Generated class for the BuyProposerDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({ 
  selector: 'page-buy-proposer-details',
  templateUrl: 'buy-proposer-details.html',
})



export class BuyProposerDetailsPage {
 
  @ViewChild(List) list: List;
  loading;
  appData;
  proposerName;
  proposerOccupation = '';
  canShowToast = true;
  customText = "male";
  address;
  pinCode;
  state;
  city;
  email;
  mobile;
  apiState = [];
  apiStateArray;
  checkedAddress;
  corspndncAddressStatus = false;
  corpAddress;
  corpondncPinCode;
  corpondncState;
  corpondncCity;
  pan;
  stateCode;
  nameOfProposer= '';
  memberTitle;
  memberOccupation='';
  selectedCardArray;
  occupationList =[];
  occupation;
  occupationOther;
  hideShowDiv = true;
  selfDiv = false;
  forQuickQuote = true;
  activeMember = "SELF";
  activeMemberIndex = 0;
  pID;
  emailPattern;
  ReferenceNo;
  pet;
  disableProceedBt = true;
  segmentactive = false;
  inactive = false;
  SelfDob;
  memberDOB;
  selfMaritalStatus;
  otherGender = false;
  salutationList = [];
  ClientDetails = [];
  pinData;
  thumbnail;
  ImageData;
  imageCaptured = true;
  imagePOp = true;
  saveandEmailHideShowProposer = true;
  saveandEmailHideShowOther = true;
  fromPage;
  testVar = false;
  currentNumber = 0;
  thankyouPopup=true;
  insuredPlanTitle;
  isActive = true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  isPinValid:boolean = true;
  male = true;
  female = false;
  

  memberArray;
  // memberArray = [
  //   {"hideAptLabel": true,"hideAptInput": true,"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","maritalCode":"M","age":"10-04-1960","ageText":"30 years","insuredCode":"si002","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"93.98","HeightText":"3' 1\"","Weight":"54","ProposerDetailsDivShow":true,"medicalDeclaration":false,"showMedicalQuestions":true,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"occupation":"ENGR","proposerName":"Dgxydy","Gender":"M","genderText":"Male","address":"Dhydyfyf","pincode":"868686","state":"Dhfhfjfjffjfjfjfj","city":"Djcjck","proposerEmail":"dyduff","mobile":"8686856868","pan":"DYDYXGXGDY","occupationCode":"ENG"},
  //   {"hideAptInput": true,"hideAptLabel": true,"code":"SPOU","title":"Spouse","smoking":false,"smokingText":"","popupType":"1","showMarried":false,"maritalStatus":"","maritalCode":"M","age":"30-04-1971","ageText":"27 years","insuredCode":"si002","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"93.98","HeightText":"3' 1\"","Weight":"45","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true,"proposerName":"TestSpouse","occupation":"Maintenance Engineer","occupationCode":"ENG"},
  //   {"hideAptInput": true,"hideAptLabel": true,"code":"SON","title":"Son","smoking":false,"smokingText":"","popupType":"1","showMarried":false,"maritalStatus":"","maritalCode":"S","age":"09-07-1994","ageText":"27 years","insuredCode":"si002","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"93.98","HeightText":"3' 1\"","Weight":"45","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true,"proposerName":"TestSon","occupation":"Student","occupationCode":"ENG"}];

 
  regexEmail(email){
    let emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    return emailPattern.match(this.email);
  }
  // = [{"showMedicalQuestions":true,"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","age":"1993-03-02","ageText":"25 years","insuredCode":"si004","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P002","insuredPlanTypeText":"SUPERIOR","insuredAmtInDigit":"","Height":"96.520","HeightText":"3' 2\"","Weight":"5","ProposerDetailsDivShow":true,"medicalDeclaration":false,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"nameOfProposer":"TEST","Gender":"M","genderText":"Male"},{"showMedicalQuestions":true,"nameOfProposer":"test3","code":"SPOU","title":"Spouse","smoking":true,"smokingText":"Smoking","popupType":"1","showMarried":false,"maritalStatus":"","age":"2001-03-02","ageText":"17 years","insuredCode":"si004","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P002","insuredPlanTypeText":"SUPERIOR","insuredAmtInDigit":"","Height":"96.520","HeightText":"3' 2\"","Weight":"3","ProposerDetailsDivShow":false,"medicalDeclaration":false,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true}];

  today = new Date();
  maxDate = (this.today.getFullYear()+"-"+this.numberLenght(this.today.getMonth())+"-"+this.numberLenght(this.today.getDate())).toString();
  
  minDate = '1900-01-01';
 
  numberLenght(Num){if(Num>=10){return Num}else{return"0"+Num}}
  constructor(private platform: Platform, private base64: Base64, public camera: Camera, public navCtrl: NavController,  private cdr: ChangeDetectorRef, private datePicker: DatePicker,
    public navParams: NavParams, public toast: ToastController, public appService: AppService, private loadingCtrl : LoadingController) {
    this.appData = JSON.parse(localStorage.AppData);  
    //this.apiStateArray = this.appData.Masters.PinCode;
    this.occupationList = this.appData.Masters.Occupation;
    this.salutationList = this.appData.Masters.Salutations;
    this.memberArray = navParams.get("buyPageMemberDetailsResult");
    this.selectedCardArray = navParams.get("buyPageCardDetailsResult");
    console.log("card: " + JSON.stringify(this.selectedCardArray));
    console.log("member: " + JSON.stringify(this.memberArray));
    this.insuredPlanTitle = this.memberArray[0].insuredPlanTypeText;
     
    this.fromPage = this.navParams.get("fromDashboardListing");
    console.log("fromPage: " + this.fromPage);
    // platform.registerBackButtonAction(() => {
    //   console.log("backPressed 1");
    //   this.onBackClick();
    // },1);

    // platform.registerBackButtonAction(() => {
    //   this.onBackClick();
    // });
  
  }

  ionViewDidLoad() {   
   
    
    if(this.fromPage == "true"){
      this.proposerName = this.memberArray[0].proposerName;
      this.email = this.memberArray[0].proposerEmail;
      this.mobile = this.memberArray[0].mobile;
      this.selfMaritalStatus = this.memberArray[0].showMarried;
      this.SelfDob = this.memberArray[0].age;
      this.address = this.memberArray[0].address;
      this.pinCode = this.memberArray[0].pincode;
      this.state = this.memberArray[0].state;
      this.city = this.memberArray[0].city;
      this.pan =  this.memberArray[0].pan;
      this.proposerOccupation = this.memberArray[0].occupationCode;
    }else{
      this.proposerName = sessionStorage.propName;
      this.email = sessionStorage.email;
      this.mobile = sessionStorage.mobile;
      this.SelfDob = this.memberArray[0].age;
    }   
    console.log('ionViewDidLoad BuyProposerDetailsPage');  
    this.ReferenceNo = this.selectedCardArray.ReferenceNo;
    //this.loadState();
    this.pet = "Self";
    this.thumbnail = "assets/imgs/proposer_profile_pic.svg";  
    this.forQuickQuote = this.memberArray.length == 1 ? false : true;
    this.saveandEmailHideShowProposer = this.memberArray.length == 1 ? false : true;  

    
    console.log("back State: " + sessionStorage.isViewPopped);
    
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
      console.log("MemberA:" + JSON.stringify(this.memberArray));     
      this.memberArray[0].proposerName =  sessionStorage.propName;
      this.memberArray[0].proposerEmail = sessionStorage.email;
      this.memberArray[0].mobile = sessionStorage.mobile;
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);          
      this.selfDiv = false;
      this.pet = this.memberArray[0].title;
      this.hideShowDiv = true;
      this.setStoredData(0);
      sessionStorage.isViewPopped = ""; 
    }  
    
  }

  ionViewDidEnter(){
    console.log("back State: " + sessionStorage.isViewPopped);
    this.thumbnail = "assets/imgs/proposer_profile_pic.svg";
    this.ReferenceNo = this.selectedCardArray.ReferenceNo;
    this.pet = this.memberArray[0].title;
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
    
      console.log("BACK: " + JSON.stringify(this.memberArray));
      
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);     
     
      this.selfDiv = false;
      
      if(this.memberArray.length == 1){
        this.forQuickQuote = false;
        this.pet = this.memberArray[0].title;
      }else{
        this.forQuickQuote = true;
        this.pet = this.memberArray[0].title;
      }
      
      this.hideShowDiv = true;
      this.setStoredData(0);

      sessionStorage.isViewPopped = ""; 

      this.activeMemberIndex = 0;

      // if(i == 0 || i == 1){

      // }
    }
    
  //   $(".gSec").click(function(){
  //     //alert($(this).attr('rel'))
  //     // if(this.isActive){
  //     //   this.customText = $(this).attr('rel');    
  //     //   this.isActive = !this.isActive;
  //     // }else{
  //     //   this.customText = $(this).attr('rel');    
  //     //   this.isActive = !this.isActive;
  //     // }
     

  //     if($(this).attr('rel') == "male"){
  //       this.customText = $(".male").attr('rel');      
  //     }else{
  //       this.customText = $(".female").attr('rel');                    
  //     }

  //     this.customText = $(this).attr('rel');    
  //     this.isActive = !this.isActive;
      
      
      
  //     console.log("gender" + this.customText);
           
             
  //  });
  }


  setStoredData(i){
    //for(let i=0; i< this.memberArray.length; i++){  
  
      if(i == 0){
        this.proposerName = this.memberArray[i].proposerName;
        this.email = this.memberArray[i].proposerEmail;
        this.mobile = this.memberArray[i].mobile;
        this.SelfDob = this.memberArray[i].age;
        this.pet = this.memberArray[0].title;
        if(this.memberArray[i].maritalCode == "M"){
          this.selfMaritalStatus = true;
        }     
        this.address = this.memberArray[i].address;
        this.pinCode = this.memberArray[i].pincode;
        this.state = this.memberArray[i].state;
        this.city = this.memberArray[i].city;
        this.pan =  this.memberArray[i].pan;
        this.proposerOccupation = this.memberArray[i].occupationCode;
        this.disableProceedBt = false;
      }else{
        console.log("propName: " + JSON.stringify(this.memberArray[i].proposerName));
        console.log(this.memberArray[i].proposerName != undefined);
        
        
        this.nameOfProposer = this.memberArray[i].proposerName != undefined ? this.memberArray[i].proposerName : "";
        this.memberDOB = this.memberArray[i].age != undefined ? this.memberArray[i].age : "";
        this.memberOccupation = this.memberArray[i].occupationCode != undefined ? this.memberArray[i].occupationCode : "";
        this.disableProceedBt = false;
      }
    //}
  }
  // loadState(){
  //   for(let i=0; i<this.apiStateArray.length;i++){ 
  //     if(this.apiState.indexOf(this.apiStateArray[i].state) == -1) {
  //        this.apiState.push({title: this.apiStateArray[i].state});
  //       }        
  //   }
     
  //   console.log("State " + JSON.stringify(this.apiState));
  // }


  changePinCode(){   
    
    // for(let i=0; i<this.apiStateArray.length;i++){ 
    //     if(this.apiStateArray[i].pincode == this.pinCode){
    //       if(this.checkedAddress == true){
    //           this.corpondncState = this.apiStateArray[i].state;
    //           this.corpondncCity = this.apiStateArray[i].city;
    //           this.stateCode = this.apiStateArray[i].stateCode;
    //       }else{
    //           this.state = this.apiStateArray[i].state;
    //           this.city = this.apiStateArray[i].city;
    //           this.stateCode = this.apiStateArray[i].stateCode;
    //       }
    //     }    
    // }
    this.pinData = {"pin": this.pinCode} 
    var sendData = this.appService.encryptData(JSON.stringify(this.pinData),sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.stateCityService("FGHealth.svc/GetPinDetails", {'request': sendData});
  }

    /* call state city service */
    stateCityService(URL,serviceData){
      this.presentLoadingDefault();
      //this.appService.presentServiceLoading();
      let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService(URL,serviceData,headerString)
        .subscribe(data =>{
         //this.loading.dismiss();       
          console.log();
          if(data && data.GetPinDetailsResult.ReturnCode == "0"){
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            //this.showToast(data.GetPinDetailsResult.ReturnMsg); 
            this.isPinValid = true;
            this.state = data.GetPinDetailsResult.Data.state;
            this.city = data.GetPinDetailsResult.Data.city;
            this.loading.dismiss();
          }else if(data.GetPinDetailsResult.ReturnCode == "807"){
            //this.showToast(data.GetPinDetailsResult.ReturnMsg);
            this.isPinValid = false;
            this.state = "";
            this.city = "";
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            //this.navCtrl.push(LoginPage);
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 807;

          }else if(data.GetPinDetailsResult.ReturnCode == "500"){
            //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
            this.isPinValid = false;
            this.state = "";
            this.city = "";
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 500;
            //this.navCtrl.push(LoginPage);
          } else{
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            this.isPinValid = true;
            this.state = "";
            this.city = "";
            //this.showToast(data.GetPinDetailsResult.ReturnMsg); 
            this.loading.dismiss();
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 400;
          }
        
        });
    }

  addressChange(){
    console.log("Toggled: "+ this.checkedAddress);
    if(this.corspndncAddressStatus == false){
      this.corspndncAddressStatus = true;
    }else{
      this.corspndncAddressStatus = false;
    }
  }

  proceedNomineeDetailsPage(){
    //alert(this. );
    // var a = this.UI_Validate();
    // console.log("State: " + a);
    // if(!this.UI_Validate){

    // }

    if(this.memberArray.length == 1){    
      // if(this.proposerName == ""){
      //   this.showToast("Enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
      //  }else if(this.memberOccupation == ''){
      //    this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
      //  }else{           
      //    this.memberArray[this.activeMemberIndex].proposerName = this.proposerName;
      //    this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
  
      //    for(let j=0; j<this.occupationList.length; j++){
      //      if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
      //      this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
      //      break;
      //      }
      //      }
      //    this.getMemberContactDetails();   
      //    this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
      //  }

      if (this.proposerName == undefined || this.proposerName == null || this.proposerName == "") {     
        this.showToast("Please enter proposer name to proceed");              
      } else if (this.address == undefined || this.address == null || this.address == "") {     
            this.showToast("Please enter your address to proceed");                             
      } else  if (this.pinCode == undefined || this.pinCode == null || this.pinCode == "") {     
            this.showToast("Please enter your pincode to proceed");                         
      } else if(this.pinCode.length<6){
            this.showToast("Please enter 6 digit pincode");          
      } else if (this.state == undefined || this.state == null || this.state == "") {     
            this.showToast("Please enter your state to proceed");                            
      } else if (this.city == undefined || this.city == null || this.city == "") {     
            this.showToast("Please enter your city to proceed");                             
      } else if (this.email == undefined || this.email == null || this.email == "") { 
            this.showToast("Please enter your email Id to proceed");           
      } else if (!this.matchEmail(this.email)) {
        this.showToast("Please enter valid email id");   
        }
      else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
              this.showToast("Please enter mobile number to proceed");                              
      } else if(this.mobile.length < 10){
              this.showToast("Please enter 10 digit mobile number");            
      }else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
          this.showToast("Please select occupation to proceed");     
      } 
      /*else if ( this.pan == undefined || this.pan == null || this.pan == "") { 
          this.showToast("Please enter your PAN to proceed");      
      } else if (!this.matchPan(this.pan)) {
          this.showToast("Please enter valid pan");     
      } else if(this.imageCaptured == true){
        this.showToast("Please click image of Proposer");   
      }*/else{
        this.disableProceedBt = true;          
        this.getMemberContactDetails();      
       
        this.encryptIncompleteData();
      }
    }else{
      if(this.nameOfProposer == ""){
        this.showToast("Please enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
       }else if(this.memberOccupation == ''){
         this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
       }else if(this.selectedCardArray.Request.BeneficiaryDetails.Member[0].CoverType == "VITAL" && (this.memberArray[this.activeMemberIndex].title == "Son" || this.memberArray[this.activeMemberIndex].title == "Daughter") && (this.memberOccupation == "BUSM" || this.memberOccupation == "HSWF" || this.memberOccupation == "RETR" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS")){
        this.showToast("Only dependent childs are covered");
       }else if(this.selectedCardArray.Request.BeneficiaryDetails.Member[0].CoverType == "VITAL" && (this.memberArray[this.activeMemberIndex].title == "Father" || this.memberArray[this.activeMemberIndex].title == "Mother") && (this.memberOccupation == "BUSM" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS" || this.memberOccupation == "STDN" || this.memberOccupation == "UNEM")){
        this.showToast("Only dependent parents are covered");
      }else{           
         this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
         this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
         this.memberArray[this.activeMemberIndex].age = this.memberDOB;

        if(this.customText == "male" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.customText == "female" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }else if(this.memberArray[this.activeMemberIndex].title == "Son"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }else if(this.memberArray[this.activeMemberIndex].title == "Daughter"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.memberArray[this.activeMemberIndex].title == "Father"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }else if(this.memberArray[this.activeMemberIndex].title == "Mother"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.memberArray[this.activeMemberIndex].title == "Grand Mother"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.memberArray[this.activeMemberIndex].title == "Grand Father"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }      
        
        console.log("member: " + JSON.stringify(this.memberArray));
        
  
         for(let j=0; j<this.occupationList.length; j++){
           if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
           this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
           break;
           }
           }
         this.getMemberContactDetails();   
         
        this.encryptIncompleteData();
        //  this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
       }
    }  

  }


  getMemberContactDetails(){
    var contactDetails = {};
    let a = this.proposerName;
    let first = a.split(" ");  
    console.log(JSON.stringify(first));
          
    if(first.length == 3){
      var firstSplit = first[0].replace(/[^a-z]/gi,'');
      var secondSplit = first[2].replace(/[^a-z]/gi,'');
      console.log("Regex:" + firstSplit + secondSplit);
      
      this.memberArray[0].proposerName = firstSplit + " " +  secondSplit;
    }else if(first.length == 2){
      var firstSplit = first[0].replace(/[^a-z]/gi,'');
      var secondSplit = first[1].replace(/[^a-z]/gi,'');
    console.log("Regex:" + firstSplit + secondSplit);    
    this.memberArray[0].proposerName = firstSplit + " " +  secondSplit;
    }
    


    if(this.customText == "male"){
      this.memberArray[0].Gender = 'M';
      this.memberArray[0].genderText = 'Male';
    }else{
      this.memberArray[0].Gender = 'F';
      this.memberArray[0].genderText = 'Female';
    }


    // if(this.selfMaritalStatus){
    //   this.memberArray[0].maritalStatus = "Married";
    //   this.memberArray[0].maritalCode = "M"
    //   this.hidePopupSmokerDiv = true;
    // }
    // this.memberArray[0].showMarried = this.selfMaritalStatus;
    // this.memberArray[0].maritalCode = this.address;
    this.memberArray[0].age = this.SelfDob;
    this.memberArray[0].ageText = this.getAge(this.SelfDob);
    if(this.selfMaritalStatus){
      this.memberArray[0].maritalStatus = "Married";
      this.memberArray[0].maritalCode = "M"   
    }else{
      this.memberArray[0].maritalStatus = "Single"; 
      this.memberArray[0].maritalCode = "S"       
    }    
    this.memberArray[0].address = this.address;
    this.memberArray[0].pincode = this.pinCode;
    this.memberArray[0].state = this.state;
    this.memberArray[0].city = this.city;
    this.memberArray[0].proposerEmail = this.email;
    this.memberArray[0].mobile = this.mobile;
    this.memberArray[0].corpAddress = this.corpAddress;
    this.memberArray[0].corpPinCode = this.corpondncPinCode;
    this.memberArray[0].corpState = this.corpondncState;
    this.memberArray[0].corpCity = this.corpondncCity;
    this.memberArray[0].pan = (this.pan).toUpperCase();
    this.memberArray[0].occupationCode = this.proposerOccupation;
      for(let j=0; j<this.occupationList.length; j++){
        if(this.memberArray[0].occupationCode == this.occupationList[j].OccCode){
          this.memberArray[0].occupation = this.occupationList[j].OccTitle;
          break;
        }
      }

    console.log("Updated :  "+ JSON.stringify(this.memberArray));
  }


  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  // onChange(value) {
  //   this.pan = value;
  //  }

  onSelectChangeSelf() {
    console.log('Selected self', this.occupation);
   
      this.memberArray[0].occupation = this.occupation;    
  
  }
  onSelectChangeOther(){

    for(let i=0; i<this.memberArray.length;i++){
      this.memberArray[i].occupationOther = this.occupationOther;
    }

    console.log('Selected other', this.occupationOther);
  }

  continueButtonClick(){

    console.log("active: " + this.activeMemberIndex);
    this.emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    let panPattern = "[A-Za-z]{5}\d{4}[A-Za-z]{1}";
    console.log("1: " + this.activeMemberIndex + "2: " + (this.memberArray.length - 1));
    if(this.activeMemberIndex == (this.memberArray.length-1)){
      this.forQuickQuote = false;
      this.saveandEmailHideShowProposer = true;
      this.saveandEmailHideShowOther = false;
    }
     if(this.activeMemberIndex == 0){
        let proceed = true;
        if (this.proposerName == undefined || this.proposerName == null || this.proposerName == "") {     
              this.showToast("Please enter proposer name to proceed");
              proceed = false;
              this.segmentactive = true;
        } else if (this.address == undefined || this.address == null || this.address == "") {     
              this.showToast("Please enter your address to proceed");                   
              proceed = false;  
              this.segmentactive = true; 
        } else  if (this.pinCode == undefined || this.pinCode == null || this.pinCode == "") {     
              this.showToast("Please enter your pincode to proceed");                   
              proceed = false;   
              this.segmentactive = true;
        } else if(this.pinCode.length<6){
              this.showToast("Please enter 6 digit pincode");
              proceed = false;  
              this.segmentactive = true; 
        } else if (this.state == undefined || this.state == null || this.state == "") {     
              this.showToast("Please enter your state to proceed");                    
              proceed = false;   
              this.segmentactive = true;
        } else if (this.city == undefined || this.city == null || this.city == "") {     
              this.showToast("Please enter your city to proceed");                    
              proceed = false;  
              this.segmentactive = true; 
        } else if (this.email == undefined || this.email == null || this.email == "") { 
              this.showToast("Please enter your email Id to proceed");
              proceed = false;   
              this.segmentactive = true;
        } else if (!this.matchEmail(this.email)) {
          this.showToast("Please enter valid email id");
          proceed = false; 
          this.segmentactive = true;
          }
         else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
                this.showToast("Please enter mobile number to proceed");                   
                proceed = false;   
                this.segmentactive = true;
        } else if(this.mobile.length < 10){
                this.showToast("Please enter 10 digit mobile number");
                proceed = false;   
                this.segmentactive = true;
        } else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
            this.showToast("Please select occupation to proceed");
            proceed = false;
            this.segmentactive = true;
        } 
        /*else if ( this.pan == undefined || this.pan == null || this.pan == "") { 
            this.showToast("Please enter your PAN to proceed");
            proceed = false;
            this.segmentactive = true;
        } else if (!this.matchPan(this.pan)) {
            this.showToast("Please enter valid pan");
            proceed = false;
            this.segmentactive = true;
        } else if(this.imageCaptured == true){
            this.showToast("Please click image of Proposer");
            proceed = false;
        }*/
       else if(proceed){ 

          //this.segmentactive = false;

          this.disableProceedBt = true;          
          this.getMemberContactDetails();
          this.activeMemberIndex++;
          this.pet = this.memberArray[this.activeMemberIndex].title;
          this.memberTitle = this.memberArray[this.activeMemberIndex].title;
          this.memberDOB = this.memberArray[this.activeMemberIndex].age;
          this.setStoredData(this.activeMemberIndex);
          this.hideShowDiv = false;
          this.selfDiv = true;
        
          if(this.memberArray.length == 2){
            this.forQuickQuote = false;
            this.saveandEmailHideShowProposer = true;
            this.saveandEmailHideShowOther = false;
          }
        }else{

        }  
        
     } else{
      for(let i=1; i<this.memberArray.length;i++){
        //if(this.memberArray[i].title == "")
        
        if(this.activeMemberIndex == i){
         
          //this.nameOfProposer = "";
          //this.memberOccupation = "";
          if(this.nameOfProposer == ""){
           this.showToast("Please enter "+this.memberArray[i].title+" name");
           this.segmentactive = true;
          }else if(this.memberOccupation == ''){
            this.showToast("Please select "+this.memberArray[i].title+" occupation");
          }else if(this.selectedCardArray.Request.BeneficiaryDetails.Member[0].CoverType == "VITAL" && (this.memberArray[this.activeMemberIndex].title == "Son" || this.memberArray[this.activeMemberIndex].title == "Son1" || this.memberArray[this.activeMemberIndex].title == "Son2" || this.memberArray[this.activeMemberIndex].title == "Daughter" || this.memberArray[this.activeMemberIndex].title == "Daughter1" || this.memberArray[this.activeMemberIndex].title == "Daughter2") && (this.memberOccupation == "BUSM" || this.memberOccupation == "HSWF" || this.memberOccupation == "RETR" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS")){
            this.showToast("Only dependent childs are covered");
          }else if(this.selectedCardArray.Request.BeneficiaryDetails.Member[0].CoverType == "VITAL" && (this.memberArray[this.activeMemberIndex].title == "Father" || this.memberArray[this.activeMemberIndex].title == "Mother") && (this.memberOccupation == "BUSM" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS" || this.memberOccupation == "STDN" || this.memberOccupation == "UNEM")){
            this.showToast("Only dependent parents are covered");
          }else{           
            this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
            this.memberArray[this.activeMemberIndex].age = this.memberDOB;
            this.memberArray[this.activeMemberIndex].ageText = this.getAge(this.memberDOB);
            this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;

            if(this.customText == "female" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }else if(this.customText == "male" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Son"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }else if(this.memberArray[this.activeMemberIndex].title == "Daughter"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Father"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }else if(this.memberArray[this.activeMemberIndex].title == "Mother"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Grand Mother"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Grand Father"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }              
            // if(this.otherGender == false){
            //   this.memberArray[this.activeMemberIndex].Gender = 'M';
            //   this.memberArray[this.activeMemberIndex].genderText = 'Male';
            // }else{
            //   this.memberArray[this.activeMemberIndex].Gender = 'F';
            //   this.memberArray[this.activeMemberIndex].genderText = 'Female';
            // }

            for(let j=0; j<this.occupationList.length; j++){
              if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
              this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
              break;
              }
              }
   
            i++;
            if( i < this.memberArray.length){
              this.disableProceedBt = true;
              this.nameOfProposer = "";
              this.memberOccupation ="";
              this.activeMemberIndex++;  
              this.pet = this.memberArray[this.activeMemberIndex].title;
              console.log("activeMembers: " + this.pet);
              this.setStoredData(this.activeMemberIndex);

              this.memberTitle = this.memberArray[this.activeMemberIndex].title; 
              this.memberDOB = this.memberArray[this.activeMemberIndex].age;
              //this.memberOccupation ="";

              if(this.activeMemberIndex == (this.memberArray.length - 1)){
                this.forQuickQuote = false;
                //this.disableProceedBt = false;
                this.saveandEmailHideShowProposer = true;
                this.saveandEmailHideShowOther = false;
              }
            
            }/* else{
              this.forQuickQuote = false;
            }   */      
                   
          }
          break;
        }      
      }
     }  
    
  }

  genderClick(gender){  
    // if(gender == "male"){
    //   this.customText = $(".male").attr('rel');
    // }else{
    //   this.customText = $(".female").attr('rel');           
    // }
    
    
    //   this.isActive = !this.isActive;      

    if (gender == "male") {

      //this.isActive = !this.isActive;
      this.male = true;
      this.female = false;
      this.customText = "male"
      //this.selfGenderText = "M"      
    }
    else {


      //this.isActive = !this.isActive;
      this.female = true;
      this.male = false;
      this.customText = "female"
      //this.selfGenderText = "F"     
    }
    console.log("gender" + this.customText);
} 
  
  
// segmentClick(member,i,event){
//   console.log(i + member);  

//   //note you have to use "tap" or "click" - if you bind to "ionSelected" you don't get the "target" property
//   let segments = event.target.parentNode.children;
//   let len = segments.length;
//   // for (let i=0; i < len; i++) {
//   //   segments[i].classList.remove('segment-activated');
//   // }
//   // event.target.classList.add('segment-activated');
//   //$(".sumInsuredSlide ion-segment").removeClass('segment-activated');
//   //$(".sumInsuredSlide ion-segment").eq(this.currentNumber).addClass('segment-activated');
//       //   if(member=="Self"){
//       //     this.hideShowDiv = true;
//       //       this.selfDiv = false; 
//       //       this.activeMemberIndex = 0;
//       //       this.disableProceedBt = false;
//       //       this.forQuickQuote = true;
//       //   }else{
//       //     let proceed = true;
//       //     this.forQuickQuote = true;
//       //     this.disableProceedBt = true;
//       //     if (this.proposerName == undefined || this.proposerName == null || this.proposerName == "") {     
//       //       this.showToast("Please enter proposer name to proceed");
//       //       proceed = false;            
//       //       member[i].ShowInsurenceAmt = true;
//       //       segments[i].classList.remove('segment-activated');
//       //       //segments[0].classList.add('segment-activated');
//       //       this.pet = "Self"; 
//       //       //this.inactive = false;    
//       //   } else if (this.address == undefined || this.address == null || this.address == "") {     
//       //       this.showToast("Please enter your address to proceed");                   
//       //       proceed = false;   
//       //       //this.segmentactive = true;
//       //       //this.inactive = false;    
//       //   } else  if (this.pinCode == undefined || this.pinCode == null || this.pinCode == "") {     
//       //       this.showToast("Please enter your pincode to proceed");                   
//       //       proceed = false;   
//       //       //this.segmentactive = true;
//       //       //this.inactive = false;    
//       //   } else if(this.pinCode.length<6){
//       //       this.showToast("Please enter 6 digit pincode");
//       //       proceed = false;  
//       //       //this.segmentactive = true;
//       //       //this.inactive = false;     
//       //   } else if (this.state == undefined || this.state == null || this.state == "") {     
//       //       this.showToast("Please enter your state to proceed");                    
//       //       proceed = false;  
//       //       //this.segmentactive = true;
//       //       //this.inactive = false;     
//       //   } else if (this.city == undefined || this.city == null || this.city == "") {     
//       //       this.showToast("Please enter your city to proceed");                    
//       //       proceed = false; 
//       //       //this.segmentactive = true;
//       //       //this.inactive = false;      
//       //   } else if (this.email == undefined || this.email == null || this.email == "") { 
//       //       this.showToast("Please enter your email Id to proceed");
//       //       proceed = false; 
//       //       //this.segmentactive = true;
//       //       //this.inactive = false;      
//       //   } else if (!this.matchEmail(this.email)) {
//       //   this.showToast("Please enter valid email id");
//       //   proceed = false; 
//       //   //this.segmentactive = true;
//       //   //this.inactive = false;    
//       //   }
//       //   else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
//       //         this.showToast("Please enter mobile number to proceed");                   
//       //         proceed = false;  
//       //         //this.segmentactive = true;
//       //         //this.inactive = false;     
//       //   } else if(this.mobile.length < 10){
//       //         this.showToast("Please enter 10 digit mobile number");
//       //         proceed = false;   
//       //         //this.segmentactive = true;
//       //         //this.inactive = false;    
//       //   }
//       //   else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
//       //     this.showToast("Please select occupation to proceed");
//       //     proceed = false;
//       //     //this.segmentactive = true;
//       //     //this.inactive = false;    
//       // } else if ( this.pan == undefined || this.pan == null || this.pan == "") { 
//       //     this.showToast("Please enter your PAN to proceed");
//       //     proceed = false;
//       //     //this.segmentactive = true;
//       //     //this.inactive = false;    
//       // } else if (!this.matchPan(this.pan)) {
//       //     this.showToast("Please enter valid pan");
//       //     proceed = false;
//       //     //this.segmentactive = true;
//       //     //this.inactive = false; 
//       // } 

//       //   else if(proceed){

//       //     //this.inactive = true;
//       //     //$(".sumInsuredSlide ion-segment").addClass('activeTab');
//       //     this.segmentactive = false; 
//       //     //member[i].ShowInsurenceAmt = true;
//       //    this.currentNumber = i;
//       //     this.disableProceedBt = false;
//       //     if(i == 0 || i == 1){
//       //     this.activeMemberIndex = 0;
//       //     }else{
//       //       this.activeMemberIndex = 1;
//       //     }
//       //     this.continueButtonClick();
//       //     //this.forQuickQuote = true;
//       //   }
          
//        //}

//        if(member=="Self"){
//           this.hideShowDiv = true;
//           this.selfDiv = false; 
//           this.activeMemberIndex = 0;
//           //this.disableProceedBt = false;
//           this.forQuickQuote = true;
//       }else{
//         this.hideShowDiv = false;
//         this.selfDiv = true; 
//         this.forQuickQuote = true;
//         this.emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
//         let panPattern = "[A-Za-z]{5}\d{4}[A-Za-z]{1}";
//         console.log("1: " + this.activeMemberIndex + "2: " + (this.memberArray.length - 1));
//         if(i == (this.memberArray.length-1)){
//           this.forQuickQuote = false;
//           this.saveandEmailHideShowProposer = true;
//           this.saveandEmailHideShowOther = false;
//         }
//          if(i == 0){
       
    
//               //this.segmentactive = false;
    
//               this.disableProceedBt = true;          
//               this.getMemberContactDetails();
//               //this.activeMemberIndex++;
//               this.pet = this.memberArray[i].title;
//               this.memberTitle = this.memberArray[i].title;
//               this.memberDOB = this.memberArray[i].age;
//               this.setStoredData(i);
//               this.hideShowDiv = false;
//               this.selfDiv = true;
            
//               if(this.memberArray.length == 2){
//                 this.forQuickQuote = false;
//                 this.saveandEmailHideShowProposer = true;
//                 this.saveandEmailHideShowOther = false;
//               }
           
            
//          } else{
//           for(let j=1; i<this.memberArray.length;j++){
//             //if(this.memberArray[i].title == "")
            
//             if(i == j){
                             
//                 this.memberArray[i].proposerName = this.nameOfProposer;
//                 this.memberArray[i].age = this.memberDOB;
//                 this.memberArray[i].ageText = this.getAge(this.memberDOB);
//                 this.memberArray[i].occupationCode = this.memberOccupation;
    
//                 if(this.customText == false && this.memberArray[i].title == "Spouse"){
//                   this.memberArray[i].Gender = 'M';
//                   this.memberArray[i].genderText = 'Male';
//                 }else if(this.customText == true && this.memberArray[i].title == "Spouse"){
//                   this.memberArray[i].Gender = 'F';
//                   this.memberArray[i].genderText = 'Female';
//                 }else if(this.memberArray[i].title == "Son"){
//                   this.memberArray[i].Gender = 'M';
//                   this.memberArray[i].genderText = 'Male';
//                 }else if(this.memberArray[i].title == "Daughter"){
//                   this.memberArray[i].Gender = 'F';
//                   this.memberArray[i].genderText = 'Female';
//                 }else if(this.memberArray[i].title == "Father"){
//                   this.memberArray[i].Gender = 'M';
//                   this.memberArray[i].genderText = 'Male';
//                 }else if(this.memberArray[i].title == "Mother"){
//                   this.memberArray[i].Gender = 'F';
//                   this.memberArray[i].genderText = 'Female';
//                 }           
//                 // if(this.otherGender == false){
//                 //   this.memberArray[this.activeMemberIndex].Gender = 'M';
//                 //   this.memberArray[this.activeMemberIndex].genderText = 'Male';
//                 // }else{
//                 //   this.memberArray[this.activeMemberIndex].Gender = 'F';
//                 //   this.memberArray[this.activeMemberIndex].genderText = 'Female';
//                 // }
    
//                 for(let j=0; j<this.occupationList.length; j++){
//                   if(this.memberArray[i].occupationCode == this.occupationList[j].OccCode){
//                   this.memberArray[i].occupation = this.occupationList[j].OccTitle;
//                   break;
//                   }
//                   }
       
//                 i++;
//                 if( i < this.memberArray.length){
//                   this.disableProceedBt = true;
//                   this.nameOfProposer = "";
//                   this.memberOccupation ="";
//                   //this.activeMemberIndex++;  
//                   this.pet = this.memberArray[i].title;
//                   console.log("activeMembers: " + this.pet);
//                   this.setStoredData(i);
    
//                   this.memberTitle = this.memberArray[i].title; 
//                   this.memberDOB = this.memberArray[i].age;
//                   //this.memberOccupation ="";
    
//                   if(i == (this.memberArray.length - 1)){
//                     this.forQuickQuote = false;
//                     //this.disableProceedBt = false;
//                     this.saveandEmailHideShowProposer = true;
//                     this.saveandEmailHideShowOther = false;
//                   }
                
//                 }     
                                   
//               break;
//             }      
//           }
//          }  
       
//       }
           
//   }
  matchEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  matchPan(pan){
    var regex = /[A-Za-z]{5}\d{4}[A-Za-z]{1}/;
    return regex.test(pan);
  }

  checkFocus(){  
    if((this.activeMemberIndex == 0 && (this.pan != undefined || this.pan != null || this.pan != "")) || this.activeMemberIndex == this.memberArray.length - 1){
      this.disableProceedBt = false;
    }
  }

  otherFocusOut(){
    this.disableProceedBt = false;
  }

  DatePick(event, member){   
    //this.keyboard.close();
    // let element = (<HTMLInputElement>event.target.previousElementSibling.querySelector('input[name="nomineeDOB"]'));
    let element = this.SelfDob;
    let dateFieldValue = element;

    let year: any = 1985, month: any = 1, date: any = 1;
    if (dateFieldValue != "" || dateFieldValue == undefined) {
      date = dateFieldValue.split("-")[0]
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }


    let minDateLimit  = ( new Date(1900,1,1).getTime());  
      this.datePicker.show({
        date: new Date(year, month - 1, date),
        minDate: minDateLimit,
        maxDate: Date.now(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
      }).then(
        date => {
          let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
          this.SelfDob = dt.toString();
          console.log(this.SelfDob);
          //this.memberArray[0].age = this.SelfDob;
        },
        err => console.log('Error occurred while getting date: ', err)
      );
  }
  DatePickOtherMember(event, members, index){
    console.log("index: " + index);
    
    let element = this.memberDOB;
    let dateFieldValue = element;

    let year: any = 1985, month: any = 1, date: any = 1;
    if (dateFieldValue != "" || dateFieldValue == undefined) {
      date = dateFieldValue.split("-")[0];
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }

    let minDateLimit  = ( new Date(1900,1,1).getTime());  
      this.datePicker.show({
        date: new Date(year, month-1, date),
        minDate: minDateLimit,
        maxDate: Date.now(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
      }).then(
        date => {
          let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
          this.memberDOB = dt.toString();
          console.log(this.memberDOB);
        },
        err => console.log('Error occurred while getting date: ', err)
      );
  }


  onDateChange(ev,members){

    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.SelfDob+"-";
   
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(this.SelfDob);    
      if(this.invalidDob(this.SelfDob)){
        this.showToast('Please enter valid DOB.');
      }
      //this.keyboard.close();
    }
  }


  onDateOtherChange(ev,members){
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.memberDOB+"-";
   
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(this.memberDOB);
      this.memberArray[this.activeMemberIndex].age = this.memberDOB;
      if(this.invalidDob(this.memberDOB)){
        this.showToast('Please enter valid DOB.');
      }
      //this.keyboard.close();
    }
  }

  invalidDob(userDoB){
    let today = new Date();
    let uDob = userDoB.split("-");
    let uDte = ( this.numberLenght(Number(uDob[1]))+"-"+this.numberLenght( Number(uDob[0]))+"-"+(uDob[2]) ).toString();
    let usrDob = new Date(uDte);    
    if(usrDob.toString() == "Invalid Date"){
      return true;
    }else if(usrDob > today){
      return true;
    }
    return false;
  }

  getAge(date){
    let dateVar = date.split("-");
    let ag = (Date.now()-(new Date(dateVar[2],dateVar[1]-1,dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24))/365)+" years";
  }




  encryptIncompleteData(){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }



  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          sessionStorage.policyID = this.pID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //sessionStorage.isViewPopped = undefined;
          this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
          console.log("cardProp: " + JSON.stringify(this.selectedCardArray));
          console.log("memberFromProposer: " + JSON.stringify(this.memberArray));
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.loading.dismiss();
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": this.selectedCardArray.QuotationID,
      "UID": this.selectedCardArray.UID,
      "stage":"2",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.selectedCardArray.Request.PolicyType ,//"HTF",
        "Duration": this.selectedCardArray.Request.Duration,
        "Installments": this.selectedCardArray.Request.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails": this.selectedCardArray.Request.BeneficiaryDetails.Member.length == undefined ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberArray,
        "CardDetails": this.selectedCardArray
      }
    }
    console.log("Request data: "+JSON.stringify(sendRequest));
    return sendRequest;
  }
  
  getClientNode(){
    let age = this.SelfDob;
    let clientSalution = "MR";
    if(this.memberArray[0].Gender == "F"){
      clientSalution = "MRS";
    }
    if(this.memberArray[0].maritalStatus == "Married"){
      this.memberArray[0].maritalCode = "M";
    }else if(this.memberArray[0].maritalStatus == "Single"){
      this.memberArray[0].maritalCode = "S";
    }else {
      this.memberArray[0].maritalCode = "S";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.proposerName ,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberArray[0].Gender,
      "MaritalStatus": this.memberArray[0].maritalCode,
      "Occupation": this.memberArray[0].occupationCode,
      "PANNo": this.memberArray[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.pinCode,
        "City": this.city,
        "State": this.state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.mobile,
        "EmailAddr": this.email
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.pinCode,
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.mobile,
        "EmailAddr": this.email
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
    //console.log("clientData: " + JSON.stringify(this.ClientDetails));
    
  }
  
  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.Request.BeneficiaryDetails.Member.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberArray[0].Gender;

    BeneDetails.push({
      "MemberId": cardDetails.Request.BeneficiaryDetails.Member.MemberId ,
      "InsuredName": this.memberArray[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberArray[0].occupationCode,
      "CoverType": cardDetails.Request.BeneficiaryDetails.Member.CoverType,
      "SumInsured": cardDetails.Request.BeneficiaryDetails.Member.SumInsured,
      "DeductibleDiscount": cardDetails.Request.BeneficiaryDetails.Member.DeductibleDiscount,
      "Relation": cardDetails.Request.BeneficiaryDetails.Member.Relation,
      "NomineeName": "",
      "NomineeRelation": "",
      "AnualIncome": "",
      "Height": cardDetails.Request.BeneficiaryDetails.Member.Height,
      "Weight": cardDetails.Request.BeneficiaryDetails.Member.Weight,
      "NomineeAge": "",
      "AppointeeName": "",
      "AptRelWithominee": "",
      "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member.MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      }
    })
    return BeneDetails;
  }

  getBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    
    for(let i = 0 ;i <= cardDetails.Request.BeneficiaryDetails.Member.length-1 ; i++){
      for(let j = 0; j <=this.memberArray.length-1;j++){
        if(i==j){
          if( (this.memberArray[j].code).toUpperCase() == (cardDetails.Request.BeneficiaryDetails.Member[i].Relation).toUpperCase() ){
          
            let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
            let gendr = this.memberArray[j].Gender;
            console.log("Gender: " + this.memberArray[j].Gender);
            
            BeneDetails.push({
              "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
              "InsuredName": this.memberArray[j].proposerName,
              "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
              "InsuredGender": gendr,
              "InsuredOccpn": this.memberArray[j].occupationCode,
              "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
              "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
              "DeductibleDiscount": cardDetails.Request.BeneficiaryDetails.Member[i].DeductibleDiscount,
              "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
              "NomineeName": "",
              "NomineeRelation": "",
              "AnualIncome": "",
              "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
              "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
              "NomineeAge": "",
              "AppointeeName": "",
              "AptRelWithominee": "",
              "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
              "PreExstDisease": "N",
              "DiseaseMedicalHistoryList": {
                "DiseaseMedicalHistory": {
                  "PreExistingDiseaseCode": "",
                  "MedicalHistoryDetail": ""
                }
              }
            })
          }
        }
 
  
      }
  
    }
    console.log("ben Details:" + JSON.stringify(BeneDetails));
    
    return BeneDetails;
  
  } 


  onBackClick(){
    sessionStorage.isViewPopped = "true";
    sessionStorage.ProposerDetails = JSON.stringify(this.memberArray);
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
    this.navCtrl.pop();
  }

  captureImage(){

    let options: CameraOptions = {   
      // destinationType: this.camera.DestinationType.DATA_URL,
      // encodingType: this.camera.EncodingType.JPEG,
      // mediaType: this.camera.MediaType.PICTURE,
      // quality: 100,
      // saveToPhotoAlbum: false,
      // correctOrientation: true
      quality: 95,
      destinationType: this.camera.DestinationType.FILE_URI,      
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    this.presentLoadingDefault();
    this.camera.getPicture(options).then(imageData => {    
    this.thumbnail= imageData;       
    console.log("image: "  +this.thumbnail);
    this.getFileAsBase64(this.thumbnail);
    this.ImageData = imageData;
    this.imageCaptured = false;
    this.loading.dismiss();
    this.showToast("Proposer image captured.");
    }, (err) => {
    /*  Handle error */
      this.loading.dismiss();
    });

  }

  showImage(){
    this.imagePOp = false;
  }
  

  getFileAsBase64(thumbnail){
    this.base64.encodeFile(thumbnail).then((base64File: string) => {
      console.log("base: " + base64File);
    }, (err) => {
      console.log(err);
    });
  }

  closePop(){
    this.imagePOp = true;
  }

  onSaveAndEmail(){
    if(this.memberArray.length == 1){    
      // if(this.proposerName == ""){
      //   this.showToast("Enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
      //  }else if(this.memberOccupation == ''){
      //    this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
      //  }else{           
      //    this.memberArray[this.activeMemberIndex].proposerName = this.proposerName;
      //    this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
  
      //    for(let j=0; j<this.occupationList.length; j++){
      //      if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
      //      this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
      //      break;
      //      }
      //      }
      //    this.getMemberContactDetails();   
      //    this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
      //  }

      if (this.proposerName == undefined || this.proposerName == null || this.proposerName == "") {     
        this.showToast("Please enter proposer name to proceed");              
      } else if (this.address == undefined || this.address == null || this.address == "") {     
            this.showToast("Please enter your address to proceed");                             
      } else  if (this.pinCode == undefined || this.pinCode == null || this.pinCode == "") {     
            this.showToast("Please enter your pincode to proceed");                         
      } else if(this.pinCode.length<6){
            this.showToast("Please enter 6 digit pincode");          
      } else if (this.state == undefined || this.state == null || this.state == "") {     
            this.showToast("Please enter your state to proceed");                            
      } else if (this.city == undefined || this.city == null || this.city == "") {     
            this.showToast("Please enter your city to proceed");                             
      } else if (this.email == undefined || this.email == null || this.email == "") { 
            this.showToast("Please enter your email Id to proceed");           
      } else if (!this.matchEmail(this.email)) {
        this.showToast("Please enter valid email id");   
      }else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
        this.showToast("Please enter mobile number to proceed");                              
      } else if(this.mobile.length < 10){
        this.showToast("Please enter 10 digit mobile number");            
      }else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
        this.showToast("Please select occupation to proceed");     
      } else if ( this.pan == undefined || this.pan == null || this.pan == "") { 
        this.showToast("Please enter your PAN to proceed");      
      } else if (!this.matchPan(this.pan)) {
        this.showToast("Please enter valid pan");     
      } /*else if(this.imageCaptured == true){
        this.showToast("Please click image of Proposer");   
      }*/else{
        this.disableProceedBt = true;          
        this.getMemberContactDetails();      
       
        this.saveEmailData();
      }
    }else{
      if(this.nameOfProposer == ""){
        this.showToast("Enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
       }else if(this.memberOccupation == ''){
         this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
       }else if(this.selectedCardArray.Request.BeneficiaryDetails.Member[0].CoverType == "VITAL" && (this.memberArray[this.activeMemberIndex].title == "Son" || this.memberArray[this.activeMemberIndex].title == "Daughter") && (this.memberOccupation == "BUSM" || this.memberOccupation == "HSWF" || this.memberOccupation == "RETR" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS")){
        this.showToast("Only dependent childs are covered");
       }else if(this.selectedCardArray.Request.BeneficiaryDetails.Member[0].CoverType == "VITAL" && (this.memberArray[this.activeMemberIndex].title == "Father" || this.memberArray[this.activeMemberIndex].title == "Mother") && (this.memberOccupation == "BUSM" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS" || this.memberOccupation == "STDN" || this.memberOccupation == "UNEM")){
        this.showToast("Only dependent parents are covered");
      }else{           
         this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
         this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
         this.memberArray[this.activeMemberIndex].age = this.memberDOB;
  
         for(let j=0; j<this.occupationList.length; j++){
           if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
           this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
           break;
           }
           }
         this.getMemberContactDetails();   
         
        this.saveEmailData();
        //  this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
       }
    } 
  }

  saveEmailData(){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  callEProposalService(){   
    var eProposalData = {
          "QuotationID": this.selectedCardArray.QuotationID,
          "UID": this.selectedCardArray.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }

  sendEProposerData(serviceData){
    this.presentLoadingDefault();
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup = false;
          //this.closeModalPop();
          this.loading.dismiss();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp.ReturnCode == "807"){
          this.loading.dismiss();
          //this.showToast(resp.ReturnMsg);
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(resp.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(resp.ReturnMsg); 
        }        
       
      }, (err) => {
      console.log(err);
      this.loading.dismiss();
      });
  }

  checkValidPan(){
    if(!this.matchPan(this.pan)){
      this.disableProceedBt = true;
    }else{
      this.disableProceedBt = false;
    }
  }


  OkButton(){
      this.thankyouPopup = true;
      this.navCtrl.push(LandingScreenPage);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }

  // EProposerEmail(){
  //   this.encryptIncompleteData();
  // }

  // getFileContentAsBase64(thumbnail).then(base64Image => {
  //   //window.open(base64Image);
  //   console.log(base64Image); 
  //   // Then you'll be able to handle the myimage.png file as base64
  // });
  // html part (keypress)="onKeyPress($event)"
  // onKeyPress(event) {
  //   if ((event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 97 && event.keyCode <= 122) || event.keyCode == 32 || event.keyCode == 46) {
  //   return true
  //   }
  //   else {
  //   return false
  //   }
  //   }
}






