import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as $ from 'jquery';

/**
 * Generated class for the WaitingperiodsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    WaitingperiodsPage,
  ],
  imports: [
    //IonicPageModule.forChild(WaitingperiodsPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-waitingperiods',
  templateUrl: 'waitingperiods.html',
})
export class WaitingperiodsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    document.getElementById("splash").style.display='none';
    console.log('ionViewDidLoad WaitingperiodsPage');
    $(".accordionWrapper h4").click(function(){
      $(this).toggleClass('active');
      $(".accordionWrapper h4").not($(this)).removeClass('active');
      $(this).next(".accordContents").slideToggle();
      $(".accordionWrapper .accordContents").not($(this).next()).slideUp();
    });  
  }

  

}
