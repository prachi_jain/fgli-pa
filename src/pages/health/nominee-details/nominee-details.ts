import { Component, Input, ChangeDetectorRef, ElementRef, ViewChild, Renderer, NgModule } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { BuyPolicyReviewPage } from '../buy-policy-review/buy-policy-review';
import { LandingScreenPage } from '../landing-screen/landing-screen';
import * as $ from 'jquery';
import { Keyboard } from '@ionic-native/keyboard';
import { first } from 'rxjs/operator/first';
import { AppService } from '../../../providers/app-service/app-service';
import { BuyProposerDetailsPage } from '../buy-proposer-details/buy-proposer-details';
import { LoginPage } from '../login/login';

/**
 * Generated class for the NomineeDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    NomineeDetailsPage,
  ],
  imports: [
    //IonicPageModule.forChild(NomineeDetailsPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-nominee-details',
  templateUrl: 'nominee-details.html',
})
export class NomineeDetailsPage {
  /*variable declarations*/
  currToggleNum;
  nomTabActive: any = []
  ReferenceNo; //nomineeName; NomineeAge; NomineeRelation;
  members;
  nomineeDate: any;
  nomineeName: any;
  nomineeAge: any;
  nomineeRelation: any;
  appointeeName: any;
  appointeeRelation: any;
  temp: any = [];
  ClientDetails: any = [];
  loading;
  activeItem;
  //memberArray = [{"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","maritalCode":"M","age":"01-01-1985","ageText":"33 years","insuredCode":"si003","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"111.76","HeightText":"3' 8\"","Weight":"54","ProposerDetailsDivShow":true,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"","NomineeRelation":"","NomineeAge":"","appointeeMember":"","AptRelWithominee":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"occupation":"Businessman","proposerName":"Teat","Gender":"M","genderText":"Male","address":"Dhddj","pincode":"884994","state":"Zvsvsgge","city":"Sgsgsgs","proposerEmail":"a@gmail.com","mobile":"8097708434","pan":"HUHBF1234B","occupationCode":"BUSM"},{"code":"SPOU","title":"Spouse","smoking":false,"smokingText":"","popupType":"1","showMarried":false,"maritalStatus":"","maritalCode":"S","age":"01-01-1987","ageText":"31 years","insuredCode":"si003","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"111.76","HeightText":"3' 8\"","Weight":"54","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"","NomineeRelation":"","NomineeAge":"","appointeeMember":"","AptRelWithominee":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true,"proposerName":"Dhdhd","occupationCode":"HSWF","Gender":"M","genderText":"Male","occupation":"Housewife"},{"code":"SON","title":"Son","smoking":false,"smokingText":"","popupType":"2","showMarried":false,"maritalStatus":"","maritalCode":"S","age":"01-01-1990","ageText":"28 years","insuredCode":"si003","showDependent":true,"showAddAnotherBtn":true,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":false,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"114.30","HeightText":"3' 9\"","Weight":"85","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"","NomineeRelation":"","NomineeAge":"","appointeeMember":"","AptRelWithominee":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/son.png","detailsSaved":true,"proposerName":"Dhhdd","occupationCode":"STDN","occupation":"Student"}];

  memberArray = [];
  memberArrayCopy = [];
  nomineeMemberArray = [];
  appointeeRelationArray=[];
  selectedCardArray: any = [];
  isNomineeMinor = false;
  //immediateMemberArray = ['Self', 'Spouse', 'Mother', 'Father', 'Son', 'Daughter', 'Sibling', 'Grand Son', 'Grand Daughter', 'Grand Mother', 'Grand Father'];
  rellationArray = [];// = [{title:'Spouse'},{title : 'Son'},{title : 'Daughter'},{title : 'Mother'}, {title:'Father'}]; //= ['Spouse', 'Mother', 'Father', 'Son', 'Daughter'];
  //rellationArray = [{title: "Father", code: "FATH"},{title: "Mother", code: "MOTH"} ]
  tempRelationArray;
  hideDiv = true;
  shownGroup = null;
  isComplete = false;
  nomineeSelect : any = [];
  hideAptInput = true;
  hideApt = true;
  pID;
  proposerName;
  OtherNomineeRelation;
  thankyouPopup= true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  memberArrayWithCode = [];
  rellationArrayFinal = [];
  rellationArrayMembers = [];
  nomineeIndex;
  constructor(private platform: Platform, public navCtrl: NavController, public toast: ToastController, private datePicker: DatePicker,
    public navParams: NavParams, private cdr: ChangeDetectorRef, private keyboard: Keyboard, private render: Renderer,
    public appService: AppService, private loadingCtrl : LoadingController) {
      
    this.memberArray = navParams.get("ProposerDetails");
    console.log("nomineeMember:" + JSON.stringify(this.memberArray));
    
    this.selectedCardArray = navParams.get("planCardDetails");
    console.log("test:" + JSON.stringify(this.selectedCardArray));
    this.memberArrayCopy = this.memberArray;
    //this.nomineeMemberArray = this.immediateMemberArray;
    // platform.registerBackButtonAction(() => {
    //   this.onBackClick();
    // });
  }
  ionViewDidLoad() {
      this.hideApt = true;
    //this.currToggleNum = 0;
    // $(".acorContent:not(:first)").hide();
    //$('#nomDetails' + 0).slideDown();    
    this.ReferenceNo = this.selectedCardArray.ReferenceNo;
    //this.immediateMember(); 
    console.log("status: "+ this.memberArray[0].maritalStatus);
    // for(let i=0; i<this.memberArray.length; i++){
    //   if(this.memberArray[i].title == "Self"){
    //     this.rellationArray.splice(i,1)
    //   }else{
    //     this.rellationArray.push({
    //       "title":this.memberArray[i].title,
    //       "code": this.memberArray[i].code,
    //     });
    //   }     
    // }
    console.log("relationArray: " + JSON.stringify(this.rellationArray));



    
    // for(let i=0; i < this.memberArray.length; i++){
    //   if(this.memberArray[i].title == "Son1" || this.memberArray[i].title == "Son2" || this.memberArray[i].title == "Son3" || this.memberArray[i].title == "Son4"){
    //     for(let j=0; j < this.rellationArray.length; j++){
    //       if(this.rellationArray[j].title == "Son"){
    //         this.rellationArray.splice(j, 1);
    //         this.rellationArray.push(this.memberArray[i].title);
           
    //       }else if(this.rellationArray[j].title == "Spouse" || this.rellationArray[j].title == "Mother" || this.rellationArray[j].title == "Father" || this.rellationArray[j].title == "Daughter"){
           
    //       }else{
    //         this.rellationArray.push(this.memberArray[i].title);
    //       }          
    //     }
       
       
    //   }else if(this.memberArray[i].title == "Daughter1" || this.memberArray[i].title == "Daughter2" || this.memberArray[i].title == "Daughter3" || this.memberArray[i].title == "Daughter4"){
    //     for(let j=0; j < this.rellationArray.length; j++){
    //       if(this.rellationArray[j].title == "Daughter"){
    //         this.rellationArray.splice(j, 1);
    //         this.rellationArray.push(this.memberArray[i].title);
    //       }else if(this.rellationArray[j].title == "Spouse" || this.rellationArray[j].title == "Mother" || this.rellationArray[j].title == "Father" || this.rellationArray[j].title == "Son"){
            
    //       }else{
    //         this.rellationArray.push(this.memberArray[i].title);
    //       }          
    //     }
      
    //   }
    // }

    for(let i=0; i < this.memberArray.length; i++){
      this.memberArrayWithCode.push({Code:this.memberArray[i].code,title:this.memberArray[i].title})
      console.log( this.memberArrayWithCode) 
      }

    console.log("nomineeMemberRelation: " + JSON.stringify(this.rellationArray));
      
    if(this.memberArray[0].maritalStatus == "Divorced" || this.memberArray[0].maritalStatus == "Single" || this.memberArray[0].maritalStatus == "Widow / Widower"){
      // for(let k=0; k< this.rellationArray.length; k++){
      //   if(this.rellationArray[k].title == "Spouse"){
      //     this.rellationArray.splice(k,1)
      //   }
      // }
      this.rellationArray.push({Code:'SON',title:'Son'})
      this.rellationArray.push({Code:'MOTH',title:'Mother'})
      this.rellationArray.push({Code:'FATH',title:'Father'})
      this.rellationArray.push({Code:'DAUG',title:'Daughter'})
      console.log("Single" + JSON.stringify(this.rellationArray));
      console.log("IF");
      
    }
    else{
      this.rellationArray.push({Code:'SPOU',title:'Spouse'})
      this.rellationArray.push({Code:'SON',title:'Son'})
      this.rellationArray.push({Code:'MOTH',title:'Mother'})
      this.rellationArray.push({Code:'FATH',title:'Father'})
      this.rellationArray.push({Code:'DAUG',title:'Daughter'})
    }

    this.appointeeRelationArray.push({Code:'MOTH',title:'Mother'})
    this.appointeeRelationArray.push({Code:'FATH',title:'Father'})
    this.appointeeRelationArray.push({Code:'UNCLE', title: 'Uncle'});
    this.appointeeRelationArray.push({Code:'AUNT', title: 'Aunt'});
    this.appointeeRelationArray.push({Code:'SIST', title: 'Sister'});
    this.appointeeRelationArray.push({Code:'BROTH', title: 'Brother'});
    // var sonDeleted = 0;
    // var daugDeleted = 0;
    // for(let i = 0; i< this.memberArray.length; i++){
    //   if(this.memberArray[i].title == "Son"){

    //     if(this.memberArray[0].maritalStatus == "Married"){
    //       this.rellationArray.push(this.memberArray[i].title);
    //       if(sonDeleted = 0){
    //         //this.rellationArray.splice(1, 1);
    //         this.removeSON();
    //         sonDeleted = 1;
    //       }
    //     }else{
    //       this.rellationArray.push(this.memberArray[i].title);
    //       if(sonDeleted = 0){
    //       //this.rellationArray.splice(0, 1);
    //       this.removeSON();
    //       sonDeleted = 1;
    //       }
    //     }
       
    //   }else if(this.memberArray[i].title == "Daughter"){
    //     if(this.memberArray[0].maritalStatus == "Married"){
    //       this.rellationArray.push(this.memberArray[i].title);
    //       //this.rellationArray.splice(4, 1);
    //       this.removeDaughter();
    //       daugDeleted = 1;
    //     }else{
    //       this.rellationArray.push(this.memberArray[i].title);
    //       //this.rellationArray.splice(3, 1);
    //       this.removeDaughter();
    //       daugDeleted = 1;
    //     }
    //   }
     
    // }

   

    for(let i = 0; i<this.rellationArray.length; i++){
      this.rellationArrayFinal.push(this.rellationArray[i]);
      }
      for(let j=0; j<this.memberArrayWithCode.length; j++){
      this.rellationArrayFinal.push(this.memberArrayWithCode[j]);
      }


      this.RelationalMemberFunction()
     console.log("this.newtest" + JSON.stringify(this.rellationArrayMembers));
    //this.activeItem = "SELF"; 
    this.proposerName = this.memberArray[0].proposerName;
    console.log("back State: " + sessionStorage.isViewPopped);
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      console.log("memberStorage: " + JSON.stringify(this.memberArray));
      console.log("cardArray: " + JSON.stringify(this.selectedCardArray));
      
      sessionStorage.isViewPopped = "";
    }

    this.setStoredData();
  }

  ionViewDidEnter(){
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      console.log("memberStorage: " + JSON.stringify(this.memberArray));
      console.log("cardArray: " + JSON.stringify(this.selectedCardArray));      
      
      sessionStorage.isViewPopped = "";
    }
  }

  RelationalMemberFunction() {
    this.rellationArrayMembers = [{'Code': '', 'title': ''}];
    this.rellationArrayMembers[0].title=this.rellationArrayFinal[0].title;
    this.rellationArrayMembers[0].Code=this.rellationArrayFinal[0].Code;
    
    for (var i = 0; i <= this.rellationArrayFinal.length - 1; i++) {
    var duplicate = false; 
      for(var j = this.rellationArrayMembers.length - 1; j >= 0; j--){
      if(this.rellationArrayFinal[i].title == this.rellationArrayMembers[j].title) duplicate = true;
    }
    if(!duplicate) this.rellationArrayMembers.push(this.rellationArrayFinal[i]);
    
    } 
    //alert(JSON.stringify(this.rellationArrayMembers));
    
    
    for(let i = 0; i< this.rellationArrayMembers.length; i++){
    
    if(this.rellationArrayMembers[i].title == "Son1"){
    
    for(let j = 0; j< this.rellationArrayMembers.length;j++){ 
    if(this.rellationArrayMembers[j].title == "Son"){
    
    this.rellationArrayMembers.splice(j,1)
    }
    
    }
    }
  }

  for(let i = 0; i< this.rellationArrayMembers.length; i++){

    if(this.rellationArrayMembers[i].title == "Daughter1"){
    for(let j = 0; j< this.rellationArrayMembers.length;j++){ 
    if(this.rellationArrayMembers[j].title == "Daughter"){
    this.rellationArrayMembers.splice(j,1)
    }
    }
    }
  }
    for(let j = 0; j< this.rellationArrayMembers.length;j++){
    if(this.rellationArrayMembers[j].title == "Self"){
    
    this.rellationArrayMembers.splice(j,1)
    }
    
    }
    }
    
   
  removeSON(){
    for(let j=0; j<this.rellationArray.length; j++){
      if(this.rellationArray[j] == "Son"){
        this.rellationArray.splice(j, 1);
      }
    }
  }

  removeDaughter(){
    for(let j=0; j<this.rellationArray.length; j++){
      if(this.rellationArray[j] == "Daughter"){
        this.rellationArray.splice(j, 1);
      }
    }
  }

  // getNomineeArray(relation){
  //   let subString = relation;
  //   let string = "";
  //   let contains = false;

  //   for (let i = 0; i < this.memberArray.length; i++){
  //       string = this.memberArray[i].title;
  //       //string.contains
  //       if (string.contains(subString){ 
  //         contains = true;
  //        // break;
  //       } 
  //   }
  //   System.out.println(contains);
  // }

  // ionViewDidEnter(){
  //   this.setStoredData();
  //   //this.currToggleNum = 0;
  //   //$(".acorContent:not(:first)").hide();
  // }


  setStoredData(){
    this.nomineeRelation = this.memberArray[0].NomineeRelation;     
    this.nomineeName = this.memberArray[0].nominee;
    this.nomineeDate =  this.memberArray[0].nomineeAge;
    this.appointeeName = this.memberArray[0].appointeeName;
    this.appointeeRelation =  this.memberArray[0].appointeeRelation;
   
    this.hideApt = true;
    if((this.memberArray[0].nomineeAge != null  || this.memberArray[0].nomineeAge != undefined) && this.isDateValid(this.memberArray[0].nomineeAge) == "1"){
      this.hideApt = false;
    }else if((this.memberArray[0].nomineeAge != null  || this.memberArray[0].nomineeAge != undefined) && this.isDateValid(this.memberArray[0].nomineeAge) == "2"){
      this.hideApt = true;
    }    
  }

  // immediateMember() {
  //   this.memberArray.forEach((element, index) => {
  //     if (this.immediateMemberArray.indexOf(element.title) == -1) {
  //       this.memberArrayCopy.splice(index, 1);
  //     }
  //   });
  // }

  onDateChange(ev,members, idx){
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.nomineeDate+"-";

      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(ev.target.value);
      for(let j = 0; j<this.memberArrayCopy.length;j++){

        if(this.memberArrayCopy[j].title == this.nomineeRelation){
        this.memberArrayCopy[j].age = ev.target.value;
        }
        
        }
     // this.memberArrayCopy[0].age = ev.target.value;
      if(this.isDateValid(ev.target.value) == "2"){
        this.showToast('Please enter valid nominee DOB.');
      }else if(this.isDateValid(ev.target.value) == "1"){
        this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
        this.hideApt = false;
      }else{
        this.appointeeName = "";
        this.appointeeRelation = "";
        this.hideApt = true;
      }
      this.keyboard.close();
    }
  }
  showHideNomDetails(num) {
    console.log("index: " + num);
    
    if (this.currToggleNum != num) {
      this.currToggleNum = num
      $(".acorContent").slideUp();
      $('#nomDetails' + num).slideDown();
    }
  }

  isDateValid(date){

        var monthfield = date.split("-")[1]
        var dayfield = date.split("-")[0]
        var yearfield = date.split("-")[2]
        
        if((monthfield<=12)&&(dayfield<=31)&&(yearfield <= new Date().getFullYear())){
          return this.getAge(date) != null && this.getAge(date) >= 18 ? "0" : "1";
        }
        return "2";
      }

  // proceedNext(){
  //   let isComplete = true;
  //   var content = document.querySelector('div .nomineePage').children;
  //   for(let i=0; i<content.length;i++){
  //     let title = content[i].querySelector('h4').textContent;
  //     //let name = (<HTMLInputElement>content[i].querySelector('input[name="nomineeName'+i+'"]')).value;
  //     let name = this.nomineeName[i];
  //     let relation = content[i].querySelector('.nomineeSelect').textContent;
  //     let dob = this.nomineeDate[i];

  //     if(name == "" || name == undefined){
  //       this.showToast("Enter nominee name against "+title);
  //       //this.showHideNomDetails(i);
  //       isComplete = false;        
  //       break;
  //     }else if(relation == "" || relation == undefined){
  //       this.showToast("Enter relation with nominee against "+title);
  //       //this.showHideNomDetails(i);
  //       isComplete = false;     
  //       break;
  //     }else if((dob == "" || dob == undefined)){
  //       this.showToast("Enter nominee DOB against "+title);
  //       //this.showHideNomDetails(i);
  //       isComplete = false;        
  //       break;
  //     }else if(this.isDateValid(dob) == "2"){
  //       this.showToast("Enter valid nominee DOB against "+title);
  //       //this.showHideNomDetails(i);
  //       isComplete = false;        
  //       break;
  //     }else if(this.isDateValid(dob) == "1"){
  //       let appointeeName = this.appointeeName[i];//(<HTMLInputElement>content[i].querySelector('input[name="appointeeMember"]')).value;
  //       let appointeeRelation = content[i].querySelector('.appointeeSelect').textContent;
  //       if(appointeeName=="" || appointeeName == undefined){
  //         this.showToast("Enter appointee name against "+title);
  //         isComplete = false;  
  //         break;
  //       }else if(appointeeRelation=="" || appointeeRelation == undefined){
  //         this.showToast("Enter appointee relation against "+title);
  //         isComplete = false;  
  //         break;
  //       }
  //     }else{
  //       isComplete = true;
  //     }
  //   }
  //   isComplete ? this.encryptIncompleteData() : null ;
  // }

  proceedNext(){
    let isComplete = true;
    if(this.nomineeRelation.trim() == "" || this.nomineeRelation.trim() == undefined){
      this.showToast("Please enter relation with nominee against Self");
      isComplete = false;
    }else if(this.nomineeName.trim() == "" || this.nomineeName.trim() == undefined){
      this.showToast("Please enter nominee name against Self");
      isComplete = false;
    }else if(this.nomineeDate.trim() == "" || this.nomineeDate.trim() == undefined){
      this.showToast("Please enter nominee DOB against Self");
      isComplete = false;
    }else if(this.isDateValid(this.nomineeDate) == "2"){
      this.showToast("Please enter valid nominee DOB against Self");                       
      isComplete = false;
    }else if(this.isDateValid(this.nomineeDate) == "1"){
      if(this.appointeeName.trim() =="" || this.appointeeName.trim() == undefined){
        this.showToast("Please enter appointee name against Self");       
        isComplete = false;
      }else if(this.appointeeRelation.trim() =="" || this.appointeeRelation.trim() == undefined){
        this.showToast("Please enter appointee relation against Self");       
        isComplete = false;
      }
    }else{
      this.isComplete = true;

    }
    isComplete ? this.encryptIncompleteData() : null ;
  }

  showToast(Message) {
    let pageToast = this.toast.create({
      message: Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 2500,
    });
    pageToast.present();
  }


  numberLength(Num) { if (Num >= 10) { return Num } else { return "0" + Num } }

  DatePick(event, members, index) {
 
    this.keyboard.close();
    // let element = (<HTMLInputElement>event.target.previousElementSibling.querySelector('input[name="nomineeDOB"]'));
    let element = this.nomineeDate;
    let dateFieldValue = element;

    let year: any = 1985, month: any = 1, date: any = 1;
    if (dateFieldValue != "" || dateFieldValue == undefined) {
      date = dateFieldValue ? dateFieldValue.split("-")[0]  : "";
      month = dateFieldValue ? dateFieldValue.split("-")[1] : "";
      year = dateFieldValue ? dateFieldValue.split("-")[2] : "";
    }
  
    //let minDateLimit = (new Date(1985, 1, 0).getTime());
      this.datePicker.show({
      date: new Date(year, month - 1, date),
        maxDate: Date.now(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      }).then(
        date => {
        let dt = (this.numberLength(date.getDate()) + "-" + this.numberLength(date.getMonth() + 1) + "-" + (date.getFullYear())).toString();
        this.nomineeDate = dt
        //this.memberArrayCopy[0].age = this.nomineeDate;
        for(let j = 0; j<this.memberArrayCopy.length;j++){

          if(this.memberArrayCopy[j].title == this.nomineeRelation){
          this.memberArrayCopy[j].age = this.nomineeDate;
          }
          
          }
        let appointeeAge = this.getAge(dt);
        
        if (appointeeAge < 18) {
          this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
          this.hideApt = false;
        } else {
          this.appointeeName ="";
          this.appointeeRelation ="";
          this.hideApt = true;
        }
      },

      err => console.log('Error occurred while getting date: ' + err)
      );

  }
  
  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365);
  }

  encryptIncompleteData(){  
    //$(".acorContent:not(:first)").hide();    
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }



  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("nomineeProceedData: " + JSON.stringify(Resp));
      
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          // this.pID = Resp.Proceed_PolicyResult.Data.pID;         
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          console.log("sendToReview:" + JSON.stringify(this.memberArrayCopy));
          
          this.navCtrl.push(BuyPolicyReviewPage,{"ProposerDetails":this.memberArrayCopy, "planCardDetails":this.selectedCardArray});
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.loading.dismiss();
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": this.selectedCardArray.QuotationID,
      "UID": this.selectedCardArray.UID,
      "stage":"3",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.selectedCardArray.Request.PolicyType ,//"HTF",
        "Duration": this.selectedCardArray.Request.Duration,
        "Installments": this.selectedCardArray.Request.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        //"c": this.getBeneficiaryDetails(this.selectedCardArray),
        "BeneficiaryDetails" : this.selectedCardArray.Request.BeneficiaryDetails.Member.length == undefined ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberArray,
        "CardDetails": this.selectedCardArray
      }
    }
    console.log("VerifyPage: "+JSON.stringify(sendRequest));
    return sendRequest;
  }
  
  getClientNode(){
    let age = this.memberArray[0].age;
    let clientSalution = "MR";
    if(this.memberArray[0].Gender == "F"){
      clientSalution = "MRS";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberArray[0].proposerName,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberArray[0].Gender,
      "MaritalStatus": this.memberArray[0].maritalCode,
      "Occupation": this.memberArray[0].occupationCode,
      "PANNo": this.memberArray[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberArray[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberArray[0].pincode,
        "City": this.memberArray[0].city,
        "State": this.memberArray[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberArray[0].mobile,
        "EmailAddr": this.memberArray[0].proposerEmail
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": "",
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberArray[0].mobile,
        "EmailAddr": this.memberArray[0].proposerEmail
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  
  }

  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.Request.BeneficiaryDetails.Member.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberArray[0].Gender;
    let name = this.nomineeName;
    let relation = this.nomineeRelation;
    let dob = this.nomineeDate;
    let appointeeName =this.appointeeName;
    let appointeeRelation = this.appointeeRelation;

    this.memberArrayCopy[0].nominee = this.nomineeName;
    this.memberArrayCopy[0].NomineeRelation = this.nomineeRelation;
    this.memberArrayCopy[0].appointeeMember = this.appointeeName;
    this.memberArrayCopy[0].AptRelWithominee = this.appointeeRelation;
    this.memberArrayCopy[0].nomineeAge = this.nomineeDate;

    BeneDetails.push({
      "MemberId": cardDetails.Request.BeneficiaryDetails.Member.MemberId ,
      "InsuredName": this.memberArray[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberArray[0].occupationCode,
      "CoverType": cardDetails.Request.BeneficiaryDetails.Member.CoverType,
      "SumInsured": cardDetails.Request.BeneficiaryDetails.Member.SumInsured,
      "DeductibleDiscount": cardDetails.Request.BeneficiaryDetails.Member.DeductibleDiscount,
      "Relation": cardDetails.Request.BeneficiaryDetails.Member.Relation,
      "NomineeName": name,
      "NomineeRelation": relation.trim(),
      "AnualIncome": "",
      "Height": cardDetails.Request.BeneficiaryDetails.Member.Height,
      "Weight": cardDetails.Request.BeneficiaryDetails.Member.Weight,
      "NomineeAge": this.getAge(this.memberArrayCopy[0].age).toString(),
      "AppointeeName": appointeeName,
      "AptRelWithominee": appointeeRelation,
      "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member.MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      }
    })
    return BeneDetails;
  }
  
  getBeneficiaryDetails(cardDetails){
    console.log("CardNominee: " + JSON.stringify(cardDetails));
    console.log("MemberNominee: " + JSON.stringify(this.memberArray));
    
    var content = document.querySelector('div .nomineePage').children;

    this.memberArrayCopy[0].nominee = this.nomineeName;
    this.memberArrayCopy[0].NomineeRelation = this.nomineeRelation;
    this.memberArrayCopy[0].appointeeMember = this.appointeeName;
    this.memberArrayCopy[0].AptRelWithominee = this.appointeeRelation;
    this.memberArrayCopy[0].nomineeAge = this.nomineeDate;
   

    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.Request.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberArray.length;j++){
        if(i==j){
          if( (this.memberArray[j].code).toUpperCase()== (cardDetails.Request.BeneficiaryDetails.Member[i].Relation).toUpperCase() ){

            /*For Nominee and appointee*/
            //let title = content[j].querySelector('h4').textContent;
            let name = this.nomineeName;
            let relation = this.nomineeRelation;
            let dob = this.nomineeDate;
            let appointeeName =this.appointeeName;
            let appointeeRelation = this.appointeeRelation;
          /*For Nominee and appointee*/
  
          
      
              this.memberArrayCopy[j].appointeeName = appointeeName;
              this.memberArrayCopy[j].appointeeRelation = appointeeRelation;
         
          
          
            let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
            let gendr = this.memberArray[j].Gender;
            if(this.memberArray[j].code == "SELF"){
              this.memberArrayCopy[j].nominee = this.nomineeName;
              this.memberArrayCopy[j].NomineeRelation = this.nomineeRelation;
              this.memberArrayCopy[j].appointeeMember = appointeeName;
              this.memberArrayCopy[j].AptRelWithominee = appointeeRelation;
              this.memberArrayCopy[j].nomineeAge = dob;
             
      
              this.memberArrayCopy[j].appointeeName = appointeeName;
              this.memberArrayCopy[j].appointeeRelation = appointeeRelation;
              BeneDetails.push({
                "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
                "InsuredName": this.memberArray[j].proposerName,
                "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
                "InsuredGender": gendr,
                "InsuredOccpn": this.memberArray[j].occupationCode,
                "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
                "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
                "DeductibleDiscount": "0",
                "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
                "NomineeName": name,
                "RelationName":this.memberArray[i].title,
                "NomineeRelation":relation.trim(),
                "AnualIncome": "",
                "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
                "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
                "NomineeAge": this.getAge(this.memberArrayCopy[j].nomineeAge).toString(),
                "AppointeeName": appointeeName,
                "AptRelWithominee": appointeeRelation,
                "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
                "PreExstDisease": "N",
                "DiseaseMedicalHistoryList": {
                  "DiseaseMedicalHistory": {
                    "PreExistingDiseaseCode": "",
                    "MedicalHistoryDetail": ""
                  }
                }
              })
            }else{
              this.memberArrayCopy[j].nominee = this.memberArray[0].proposerName;
              this.memberArrayCopy[j].NomineeRelation = this.getRelationForOtherMembers(this.memberArray[j].title, this.memberArray[0].Gender);
              this.memberArrayCopy[j].appointeeMember = appointeeName;
              this.memberArrayCopy[j].AptRelWithominee = appointeeRelation;
              this.memberArrayCopy[j].nomineeAge = this.memberArray[0].age;
      
              this.memberArrayCopy[j].appointeeName = "";
              this.memberArrayCopy[j].appointeeRelation = "";
              BeneDetails.push({
                "MemberId": Number(cardDetails.Request.BeneficiaryDetails.Member[i].MemberId),
                "InsuredName": this.memberArray[j].proposerName,
                "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
                "InsuredGender": gendr,
                "InsuredOccpn": this.memberArray[j].occupationCode,
                "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
                "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
                "DeductibleDiscount": "0",
                "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
                "NomineeName": this.memberArray[0].proposerName,
                "RelationName":this.memberArray[j].title,
                "NomineeRelation": this.getRelationForOtherMembers(this.memberArray[j].title, this.memberArray[0].Gender),
                "AnualIncome": "",
                "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
                "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
                "NomineeAge": this.getAge(this.memberArrayCopy[0].age).toString(),
                "AppointeeName": "",
                "AptRelWithominee": "",
                "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
                "PreExstDisease": "N",
                "DiseaseMedicalHistoryList": {
                  "DiseaseMedicalHistory": {
                    "PreExistingDiseaseCode": "",
                    "MedicalHistoryDetail": ""
                  }
                }
              })
            }
   
          }
        }
      
  
      }
  
    }
    return BeneDetails;
  
  } 

  getRelationForOtherMembers(memberDetailsrelation, selfGender){
    
  if(memberDetailsrelation == "Spouse"){
    this.OtherNomineeRelation = "Spouse";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son1" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son1" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son2" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son2" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son3" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son3" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son4" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son4" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter1" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter1" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter2" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter2" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter3" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter3" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter4" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter4" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Father" && selfGender == "M"){
    this.OtherNomineeRelation = "Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Father" && selfGender == "F"){
    this.OtherNomineeRelation = "Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Mother" && selfGender == "M"){
    this.OtherNomineeRelation = "Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Mother" && selfGender == "F"){
    this.OtherNomineeRelation = "Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Mother" && selfGender == "M"){
    this.OtherNomineeRelation = "Grand Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Mother" && selfGender == "F"){
    this.OtherNomineeRelation = "Grand Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Father" && selfGender =="M"){
    this.OtherNomineeRelation = "Grand Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Father" && selfGender =="F"){
    this.OtherNomineeRelation = "Grand Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Daughter" && selfGender == "M"){
    this.OtherNomineeRelation = "Grand Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Daughter" && selfGender == "F"){
    this.OtherNomineeRelation = "Grand Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Son" && selfGender == "M"){
    this.OtherNomineeRelation = "Grand Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Son" && selfGender == "F"){
    this.OtherNomineeRelation = "Grand Mother";
    return this.OtherNomineeRelation;
  }
    
  }


  onBackClick(events: Event){
    sessionStorage.isViewPopped = "true";
    console.log("MemberBackNominee: " + JSON.stringify(this.memberArray));
    
    sessionStorage.ProposerDetails = JSON.stringify(this.memberArray);
    console.log("NomineeBackMemberDetails: " + sessionStorage.ProposerDetails);
    
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);   
    this.navCtrl.pop();
  }

  // filledRelation(item){
  //   for(let i =0; i < this.immediateMember.length;i++){
  //     if(this.nomineeRelation[i] == this.immediateMember[i].val){
  //       return this.immediateMember[i].filledText;
  //     }
  //   }
    
  // }

  ItemClicked(title){
    this.activeItem=title;
  }

  onSaveAndEmail(){
    let isComplete = true;
    if(this.nomineeRelation.trim() == "" || this.nomineeRelation.trim() == undefined){
      this.showToast("Please enter relation with nominee against Self");
      isComplete = false;
    }else if(this.nomineeName.trim() == "" || this.nomineeName.trim() == undefined){
      this.showToast("Please enter nominee name against Self");
      isComplete = false;
    }else if(this.nomineeDate.trim() == "" || this.nomineeDate.trim() == undefined){
      this.showToast("Please enter nominee DOB against Self");
      isComplete = false;
    }else if(this.isDateValid(this.nomineeDate) == "2"){
      this.showToast("Please enter valid nominee DOB against Self");                       
      isComplete = false;
    }else if(this.isDateValid(this.nomineeDate) == "1"){
      if(this.appointeeName.trim() =="" || this.appointeeName.trim() == undefined){
        this.showToast("Please enter appointee name against Self");       
        isComplete = false;
      }else if(this.appointeeRelation.trim() =="" || this.appointeeRelation.trim() == undefined){
        this.showToast("Please enter appointee relation against Self");       
        isComplete = false;
      }
    }else{
      this.isComplete = true;

    }   
    isComplete ? this.saveEmailData() : null ;
  }

  saveEmailData(){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.loading.dismiss();
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  callEProposalService(){   
    var eProposalData = {
          "QuotationID": this.selectedCardArray.QuotationID,
          "UID": this.selectedCardArray.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }

  sendEProposerData(serviceData){
    this.presentLoadingDefault();
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup = false;
          //this.closeModalPop();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp.ReturnCode == "807"){
          this.loading.dismiss();
          //this.showToast(resp.ReturnMsg);
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(resp.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(resp.ReturnMsg); 
        }        
       
      }, (err) => {
      console.log(err);
      });
  }

  setNomineeRelation(){
    console.log("self Nominee: " + this.nomineeRelation);

    for(let i=0; i< this.memberArray.length;i++){
      console.log("Array Nominee: " + this.memberArray[i].title);
      if(this.memberArray[i].title == this.nomineeRelation){   

        this.nomineeName = this.memberArray[i].proposerName;
        console.log("age: " + this.memberArray[i].age);
        
        this.nomineeDate = this.memberArray[i].age;   
        let appointeeAge = this.getAge(this.nomineeDate);    
        if (appointeeAge  < 18) {
          this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
          this.hideApt = false;
        } else {
          this.appointeeName = "";
          this.appointeeRelation = "";
          this.hideApt = true;
        }
        break;
      }else{
        if(this.memberArray[0].maritalStatus == "Divorced"){
          //this.showToast("Please ")
        }else{
          this.nomineeName = "";              
          this.nomineeDate = "";
        }
        
      }
    }
    
  }

  OkButton(){
    this.thankyouPopup = true;
    this.navCtrl.push(LandingScreenPage);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }


}
  
