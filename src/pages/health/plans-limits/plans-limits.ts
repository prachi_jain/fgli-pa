import { Component, NgModule } from '@angular/core';
import * as $ from 'jquery';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BuyPage } from '../buy/buy';

/**
 * Generated class for the PlansLimitsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    PlansLimitsPage,
  ],
  imports: [
    //IonicPageModule.forChild(BuyPageResultPage),
  ],
})

@IonicPage() 
@Component({ 
  selector: 'page-plans-limits',
  templateUrl: 'plans-limits.html'
})
export class PlansLimitsPage {

  pet = "All";
  petMaternity = "All";
  petOrgan  = "All";
  petPatient = "All";
  petDomiciliary = "All";
  petAlternative = "All";
  petWellness = "All";
  petCumulative = "All";
  petRestoration = "All";
  petNewborn = "All";

  popupShowHide = true;
  dependent = false; 
  proposer = true; 
  liability = true;
  etopic = true;
  prenatal = true; 

  vitalSpldiv = false;
  supSpldiv = true;
  preSpldiv = true;

  vtEmi=false;
  supEmi=true;
  premEmi=true;  


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlansLimitsPage');

    document.getElementById("splash").style.display='none';

     $(".accordionWrapper h4").click(function(){
      $(this).toggleClass('active');
      $(".accordionWrapper h4").not($(this)).removeClass('active');
      $(this).next(".accordContents").slideToggle();
      $(".accordionWrapper .accordContents").not($(this).next()).slideUp();
    });  

    $(".vitalPopup").click(function(){      
      var ex = $(this).attr('rel');
      $('#pbpopup'+ex).show();  
      $(".transPopup").show();      
    });
    
    $(".closeBtn").click(function(){      
      $(".popupContent").hide();
      $(".transPopup").hide();
    });  

  }

  dependentclick(){
    this.dependent = false;
    this.proposer = true;
    this.liability = true;
    this.etopic = true;
    this.prenatal = true;
  }

  proposerclick(){
    this.dependent = true;
    this.proposer = false;
    this.liability = true;
    this.etopic = true;
    this.prenatal = true;
  }
  liabilityclick(){
    this.dependent = true;
    this.proposer = true;
    this.liability = false;
    this.etopic = true;
    this.prenatal = true;
  }
  etopicclick(){
    this.dependent = true;
    this.proposer = true;
    this.liability = true;
    this.etopic = false;
    this.prenatal = true;
  }
  prenatalclick(){
    this.dependent = true;
    this.proposer = true;
    this.liability = true;
    this.etopic = true;
    this.prenatal = false;
  }




vitalclick(){
    this.vtEmi = false;
    this.supEmi = true;
    this.premEmi = true;

      this.vitalSpldiv = false;
      this.supSpldiv = true;
      this.preSpldiv = true;
      $(".accordionWrapper .accordContents").slideUp();
      $(".accordionWrapper h4").removeClass('active');
      // $(".accordionWrapper h4").click(function(){
      //   $(this).toggleClass('active');
      //   $(".accordionWrapper h4").not($(this)).removeClass('active');
      //   $(this).next(".accordContents").slideToggle();
      //   $(".accordionWrapper .accordContents").not($(this).next()).slideUp();
      // });  
  
      // $(".vitalPopup").click(function(){      
      //   var ex = $(this).attr('rel');
      //   $('#pbpopup'+ex).show();  
      //   $(".transPopup").show();      
      // });    
     }

  supriorclick(){
    this.vtEmi = true;
    this.supEmi = false;
    this.premEmi = true;

      this.vitalSpldiv = true;
      this.supSpldiv = false;
      this.preSpldiv = true;
      $(".accordionWrapper .accordContents").slideUp();
      $(".accordionWrapper h4").removeClass('active');


  }

  premereclick(){
    this.vtEmi = true;
    this.supEmi = true;
    this.premEmi = false; 

      this.vitalSpldiv = true;
      this.supSpldiv = true;
      this.preSpldiv = false;
      $(".accordionWrapper .accordContents").slideUp();
      $(".accordionWrapper h4").removeClass('active');

  }

  closePop(){
    // this.pet= "All";       
    this.popupShowHide = true;
    this.petMaternity = "All";
    this.petOrgan  = "All";
    this.petPatient = "All";
    this.petDomiciliary = "All";
    this.petAlternative = "All";
    this.petWellness = "All";
    this.petCumulative = "All";
    this.petRestoration = "All";
    this.petNewborn = "All";
  }

  showPopUp(){
    this.popupShowHide = false;
  }


  goToBuyPage(){
    this.navCtrl.push(BuyPage);
  }

}

 