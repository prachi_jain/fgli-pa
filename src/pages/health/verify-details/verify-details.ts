import { Component, NgModule } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { PaymentMethodPage } from '../payment-method/payment-method';
import { DisclaimerPage } from '../disclaimer/disclaimer';
import * as $ from 'jquery';
import { Observable } from 'rxjs/Observable'
import { Pipe, PipeTransform } from '@angular/core';
import { Injectable } from '@angular/core';
import { LandingScreenPage } from '../landing-screen/landing-screen';
import { LoginPage } from '../login/login';
import { Subscription } from "rxjs/Subscription";


/**
 * Generated class for the VerifyDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    VerifyDetailsPage,
  ],
  imports: [
    //IonicPageModule.forChild(VerifyDetailsPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-verify-details',
  templateUrl: 'verify-details.html',
})
export class VerifyDetailsPage {

  observableVar: Subscription;
  otpPopup:boolean;
  trans:boolean;
  memberDetails;
  selectedCardArray;
  termsConditionFirst = true;
  termsConditionSecond = false;
  termsConditionThird = true;
  termsConditionFour = true;
  mobile;
  canShowToast = true;
  otpValue;
  ReferenceNo;
  verifyDisable= true;
  lastDigits;
  emailString;
  disableSubmitButton = true;
  mobileField = true;
  time = 60;
  timer = false;
  loading;
  pID;
  ClientDetails = [];
  spouseGender = "F";
  adhaar;
  thankyouPopup=true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  //MyInterval;

  // counter = 60;
  // tick = 1000;
  // countDown;  


 
  
  constructor(private platform: Platform, public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams, public appService: AppService, public toast: ToastController) {
    this.memberDetails = navParams.get("memberDetails");
    this.selectedCardArray = navParams.get("planCardDetails");
    console.log("test: " + JSON.stringify(this.memberDetails));
    console.log("Card: " + JSON.stringify(this.selectedCardArray));

    // platform.registerBackButtonAction(() => {
    //   this.onBackClick();
    // });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyDetailsPage');
    console.log("1"+this.termsConditionSecond); 
    console.log("2"+this.termsConditionSecond);
    console.log("3"+this.termsConditionThird);
    console.log("4"+this.termsConditionFour); 
   // this.memberDetails[0].mobile = sessionStorage.mobile;
      this.mobile =  this.memberDetails[0].mobile;
    //document.getElementById("splash").style.display = "none";
    if(sessionStorage.isViewPopped == "true"){
     
      this.memberDetails = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      this.memberDetails[0].mobile = sessionStorage.mobile;
      this.mobile = sessionStorage.mobile;
      sessionStorage.isViewPopped = "";
    }  
    this.ReferenceNo = this.selectedCardArray.ReferenceNo;
    if(this.termsConditionFirst == true && this.termsConditionSecond == false && this.termsConditionThird == true && this.termsConditionFour == true){
      this.verifyDisable = false;
    }else{
      this.mobileField = true;
      this.mobile = this.memberDetails[0].mobile;
    }
  }

  termsToggleFirst(){
    console.log("First:" + this.termsConditionFirst);
    if(this.termsConditionFirst == true && this.termsConditionSecond == false && this.termsConditionThird == true && this.termsConditionFour == true){
      this.mobileField = false;
      this.verifyDisable = false;
    }else{
      this.mobileField = true;
      this.verifyDisable = true;
    }
  }

  termsToggleSecond(){
    console.log("Second:" + this.termsConditionSecond);
    if(this.termsConditionFirst == true && this.termsConditionSecond == false && this.termsConditionThird == true && this.termsConditionFour == true){
      this.mobileField = false;
      this.verifyDisable = false;
    }else{
      this.showToast("We apologise that you cannot purchase policy as you are already covered under this plan. Kindly contact nearest branch");
      this.mobileField = true;
      this.verifyDisable = true;
    }
  }

  termsToggleThird(){
    console.log("Third:" + this.termsConditionFirst);
    if(this.termsConditionFirst == true && this.termsConditionSecond == false && this.termsConditionThird == true && this.termsConditionFour == true){
      this.mobileField = false;
      this.verifyDisable = false;
    }else{
      this.mobileField = true;
      this.verifyDisable = true;
    }
  }

  termsToggleFourth(){
    console.log("Fourth:" + this.termsConditionFirst);
    if(this.termsConditionFirst == true && this.termsConditionSecond == false && this.termsConditionThird == true && this.termsConditionFour == true){
      this.mobileField = false;
      this.verifyDisable = false;
    }else{
      this.mobileField = true;
      this.verifyDisable = true;
    }
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  mobileVerify(){   
    this.otpValue = "";
    var re = new RegExp("^([0-9]{12})$");
    if(this.termsConditionFirst = true && this.termsConditionSecond == false && this.termsConditionThird == true && this.termsConditionFour == true){   
      // if(this.adhaar == "" || this.adhaar == undefined || this.adhaar == null){
      //   this.showToast("Please enter your Aadhaar details to proceed");
      // }else if(this.adhaar.length < 12){
      //   this.showToast("Please enter 12 digit Aadhaar number");
      // }else if(this.adhaar[0] == "0"){
      //   this.showToast("Can not start with 0");
      // }else if(this.mobile == undefined || this.mobile == "" || this.mobile == null){
      //   this.showToast("Please enter your mobile number to proceed");    
      // }else if(this.mobile.length >10){
      //   this.showToast("Please enter your 10 digit mobile number");
      // }else if(!re.test(this.adhaar))
      // {
      //   this.showToast("Please enter correct aadhar");
      // }
      // else{
        let proposerMobile = this.mobile;
        this.lastDigits = proposerMobile.substring(8);
        console.log("2 :" + this.lastDigits);
        //console.log("email: " + this.memberDetails[0].proposerEmail);
        let emailStringSplit = (this.memberDetails[0].proposerEmail).split('@');
        this.emailString = emailStringSplit[1];
        console.log("email: " + this.emailString);
        this.encryptIncompleteData(this.selectedCardArray); 
        //this.getGenerateOtpServiceCall();
      //}
      
    }else{
      this.showToast("Please accept Terms & Condition");
    }
  }

  encryptIncompleteData(InsuredCardDetails){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest(InsuredCardDetails)),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage6Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage6Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.getGenerateOtpServiceCall();
          //this.navCtrl.push(BuyProposerDetailsPage,{"buyPageMemberDetailsResult": this.membersInsured,"buyPageCardDetailsResult":this.insuranceDetailsToSend});
          console.log("card: "+ JSON.stringify(this.selectedCardArray));
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  getProceedrequest(TracsactionDetails){
    //this.QuotationID = TracsactionDetails.QuotationID;
    //his.UID = TracsactionDetails.UID;  
    let sendRequest = {
      // "QuotationID": localStorage.QuotationID,
      // "UID":localStorage.UID,
      "QuotationID": TracsactionDetails.QuotationID,
      "UID": TracsactionDetails.UID,
      "stage":"6",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": TracsactionDetails.Request.PolicyType ,//"HTF",
        "Duration": TracsactionDetails.Request.Duration,
        "Installments": TracsactionDetails.Request.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos": sessionStorage.Ispos,
        "BeneficiaryDetails": this.selectedCardArray.Request.BeneficiaryDetails.Member.length == undefined ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberDetails,
        "CardDetails": this.selectedCardArray
      }
    }
    console.log("CRTrequest: "+JSON.stringify(sendRequest));
    return sendRequest;
  }

  getClientNode(){
    let age = this.memberDetails[0].age.split("-");
    let clientSalution = "MR";
    if(this.memberDetails[0].Gender == "F" && this.memberDetails[0].maritalStatus == "Married"){
      clientSalution = "MRS";
      this.spouseGender = "M";
    }else if(this.memberDetails[0].Gender == "F" && this.memberDetails[0].maritalStatus == "Single"){
      clientSalution = "MISS";
      this.spouseGender = "M";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberDetails[0].proposerName,
      "LastName": "",
      "DOB": age[0]+"/"+age[1]+"/"+age[2],
      "Gender": this.memberDetails[0].Gender,
      "MaritalStatus": this.memberDetails[0].maritalCode,
      "Occupation": this.memberDetails[0].occupationCode,
      "PANNo": this.memberDetails[0].pan,
      "GSTIN": "",
      "AadharNo": this.adhaar,
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberDetails[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetails[0].pincode,
        "City": this.memberDetails[0].city,
        "State": this.memberDetails[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetails[0].pincode,
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  
  }

  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.Request.BeneficiaryDetails.Member.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberDetails[0].Gender;
    let numericAge = this.getAge(this.memberDetails[0].nomineeAge).toString();
    
    BeneDetails.push({
      "MemberId": cardDetails.Request.BeneficiaryDetails.Member.MemberId ,
      "InsuredName": this.memberDetails[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberDetails[0].occupationCode,
      "CoverType": cardDetails.Request.BeneficiaryDetails.Member.CoverType,
      "SumInsured": cardDetails.Request.BeneficiaryDetails.Member.SumInsured,
      "DeductibleDiscount": cardDetails.Request.BeneficiaryDetails.Member.DeductibleDiscount,
      "Relation": cardDetails.Request.BeneficiaryDetails.Member.Relation,
      "NomineeName": this.memberDetails[0].nominee,
      "NomineeRelation": this.memberDetails[0].NomineeRelation,
      "AnualIncome": "",
      "Height": cardDetails.Request.BeneficiaryDetails.Member.Height,
      "Weight": cardDetails.Request.BeneficiaryDetails.Member.Weight,
      "NomineeAge": numericAge,
      "AppointeeName": this.memberDetails[0].appointeeName,
      "AptRelWithominee": this.memberDetails[0].appointeeRelation,
      "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member.MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      }
    })
    return BeneDetails;
  }

  getBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.Request.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberDetails.length;j++){
        if(i==j){
          if( (this.memberDetails[j].code).toUpperCase()== (cardDetails.Request.BeneficiaryDetails.Member[i].Relation).toUpperCase() ){
            let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ");
            let gendr = "M";
            if(this.memberDetails[j].title.toLowerCase() == "mother" || this.memberDetails[j].title.toLowerCase() == "daughter"){
              gendr = "F";
            }else if(this.memberDetails[j].title.toLowerCase() == "spouse"){
              gendr = this.spouseGender;
            }else if(this.memberDetails[j].title.toLowerCase() == "self"){
              gendr = this.memberDetails[0].Gender;
            }
            let numericAge = this.getAge(this.memberDetails[j].nomineeAge).toString();
            BeneDetails.push({
              "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
              "InsuredName": this.memberDetails[j].proposerName,
              "InsuredDob": mDob[0],
              "InsuredGender": gendr,
              "InsuredOccpn": this.memberDetails[j].occupationCode,
              "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
              "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
              "DeductibleDiscount": "0",
              "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
              "NomineeName": this.memberDetails[j].nominee,
              "NomineeRelation": this.memberDetails[j].NomineeRelation,
              "AnualIncome": "",
              "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
              "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
              "NomineeAge": numericAge,
              "AppointeeName": this.memberDetails[j].appointeeName,
              "AptRelWithominee": this.memberDetails[j].appointeeRelation,
              "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
              "PreExstDisease": "N",
              "DiseaseMedicalHistoryList": {
                "DiseaseMedicalHistory": {
                  "PreExistingDiseaseCode": "",
                  "MedicalHistoryDetail": ""
                }
              }
            })
          }
        }
      
  
      }
  
    }
    return BeneDetails;
  
  } 

  getGenerateOtpServiceCall(){
    var mobileData = {"mobileNo":this.mobile};
    this.encryptMobileData(mobileData);
  }

  encryptMobileData(mobileData){
    
    var sendData = this.appService.encryptData(JSON.stringify(mobileData),sessionStorage.TokenId);    
    console.log("testt: " + JSON.stringify(sendData));   
    this.getResponseData({'request' : sendData});
  }

  getResponseData(serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService("FGHealth.svc/GenerateOTP",serviceData, headerString)
    .subscribe(GenerateOTP =>{
      this.loading.dismiss();
      console.log(GenerateOTP);
      if(GenerateOTP && GenerateOTP.GenerateOTPResult.ReturnCode == "0"){
        sessionStorage.TokenId = GenerateOTP.GenerateOTPResult.UserToken.TokenId;
        console.log(GenerateOTP.GenerateOTPResult.ReturnMsg);
       // this.showToast(GenerateOTP.GenerateOTPResult.ReturnMsg); 
        /* remove below line */
      //  this.navCtrl.push(PaymentMethodPage, {"memberDetails":this.memberDetails,  "planCardDetails": this.selectedCardArray});
        this.otpPopup = true;
        this.time = 60;
        this.getHoldFOrClickResend();
        this.trans=true;        
      }else if(GenerateOTP.GenerateOTPResult.ReturnCode == "807"){
        //this.showToast(GenerateOTP.GenerateOTPResult.ReturnMsg);
        sessionStorage.TokenId = GenerateOTP.GenerateOTPResult.UserToken.TokenId;
        this.message = GenerateOTP.GenerateOTPResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 807;
        //this.navCtrl.push(LoginPage);
      }else if(GenerateOTP.GenerateOTPResult.ReturnCode == "500"){
        //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
        sessionStorage.TokenId = GenerateOTP.GenerateOTPResult.UserToken.TokenId;
        this.message = GenerateOTP.GenerateOTPResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 500;
        //this.navCtrl.push(LoginPage);
      }else{
        sessionStorage.TokenId = GenerateOTP.GenerateOTPResult.UserToken.TokenId;
        this.message = GenerateOTP.GenerateOTPResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 400;
        //this.showToast(GenerateOTP.GenerateOTPResult.ReturnMsg); 
      }
    }, (err) => {
    console.log(err);
    });
}

// getHoldFOrClickResend(){
//   console.log("time:" + this.time)
//   setInterval(()=>{  
//       this.time = this.time - 1;
//       console.log("time:" + this.time)
//       if(this.time == 0){
//         $(".resentOtp").removeClass('not-active');
//         this.timer = true
//       }              
//   }, 1000);
// }

getHoldFOrClickResend(){
  console.log("time:" + this.time)
  // setInterval(()=>{ 
  // this.time = this.time - 1;
  // console.log("time:" + this.time)
  // if(this.time == 0){
  // $(".resentOtp").removeClass('not-active');
  // this.timer = true
  // } 
  
  // }, 1000);
  this.observableVar = Observable.interval(1000).subscribe(()=>{
    this.resendOTPCall();
    });
  }


  resendOTPCall(){
    this.time = this.time - 1;
    console.log("time:" + this.time)
    if(this.time == 0){
    
    console.log("time:" + this.time)
    $(".resentOtp").removeClass('not-active');
    this.timer = true
    this.observableVar.unsubscribe();
    }         
    }


    getAge(date) {
      let dateVar = date.split("-");
      let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
      return Math.floor((ag / (1000 * 3600 * 24)) / 365);
    }



  // UI_Validate(){
  //   let valid = true;
  //   let mobile = this.mobile;  
  //   if (mobile == undefined || mobile == null || mobile == "") {     
  //         this.showToast("Enter your Mobile");      
  //         valid = false;
  //       } else {
  //         //this.proposerName.setError(null);
  //       }
  //       return valid;
  // }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  closeOtpModal(){
    this.observableVar.unsubscribe();
    this.otpPopup = false;
    this.timer = false;
    this.trans=false;
    this.time = 60;
    }
    proceedToReviewPage(){

      this.getValidateOtpServiceCall();
      //if()
      //this.navCtrl.push(PaymentMethodPage, {"memberDetails":this.memberDetails,  "planCardDetails": this.selectedCardArray});
    }

    getValidateOtpServiceCall(){
      //var mobileData = {"mobileNo":this.mobile};
      var mobileData = {"mobileNo" : this.mobile, "OTP" : this.otpValue}
      console.log("Mobile : " + JSON.stringify(mobileData));
      this.encryptValidateOtpData(mobileData);
    }

    encryptValidateOtpData(mobileData){
    
      var sendData = this.appService.encryptData(JSON.stringify(mobileData),sessionStorage.TokenId);    
      console.log("testt: " + JSON.stringify(sendData));   
      this.getValidateOtpResponseData({'request' : sendData});
    }
  
    getValidateOtpResponseData(serviceData){
      let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.presentLoadingDefault();
      this.appService.callService("FGHealth.svc/ValidateOTP",serviceData, headerString)
      .subscribe(ValidateOTP =>{

        this.loading.dismiss();
        console.log("checkOTP: " + JSON.stringify(ValidateOTP));
        if(ValidateOTP && ValidateOTP.ValidateOTPResult.ReturnCode == "0"){
          sessionStorage.TokenId = ValidateOTP.ValidateOTPResult.UserToken.TokenId;
          console.log(ValidateOTP.ValidateOTPResult.ReturnMsg);
          //this.showToast(ValidateOTP.ValidateOTPResult.ReturnMsg); 
          console.log("verify : "+ JSON.stringify(this.memberDetails));
          this.timer = false;
          this.memberDetails[0].verifiedNumber = this.mobile;
          this.otpPopup = false;
          this.trans= false;
         // this.mobile = "";
          console.log("term: "+ this.selectedCardArray.Installment);
          
          if(this.selectedCardArray.Installment == "full" || this.selectedCardArray.Installment == "Full"){
            this.navCtrl.push(PaymentMethodPage, {"memberDetails":this.memberDetails,  "planCardDetails": this.selectedCardArray});
          }else{
            this.navCtrl.push(DisclaimerPage, {"memberDetails":this.memberDetails,  "planCardDetails": this.selectedCardArray});
          }
          //this.navCtrl.push(DisclaimerPage, {"memberDetails":this.memberDetails,  "planCardDetails": this.selectedCardArray});
         /*  this.otpPopup = true;
          this.trans=true;         */
        }else if(ValidateOTP.ValidateOTPResult.ReturnCode == "870"){
          //this.showToast(ValidateOTP.ValidateOTPResult.ReturnMsg);
          sessionStorage.TokenId = ValidateOTP.ValidateOTPResult.UserToken.TokenId;
          this.message = ValidateOTP.ValidateOTPResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(ValidateOTP.ValidateOTPResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = ValidateOTP.ValidateOTPResult.UserToken.TokenId;
          this.message = ValidateOTP.ValidateOTPResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = ValidateOTP.ValidateOTPResult.UserToken.TokenId;
          this.message = ValidateOTP.ValidateOTPResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(ValidateOTP.ValidateOTPResult.ReturnMsg); 
        }
      }, (err) => {
      console.log(err);
      });
  }

  resendOTP(){  
    this.timer = false;
    this.time = 60;
    this.getGenerateOtpServiceCall();
  }

  mobileChange(event){
    this.verifyDisable = event.length == 10 ? false : true;
  }

  otpChange(event){
    this.disableSubmitButton = event.length == 4 || event.length == 3 ? false : true; 
  }

  onBackClick(){
    sessionStorage.isViewPopped = "true";
    sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
    this.navCtrl.pop();
  }

  onSaveAndEmail(){
    this.otpValue = "";
    if(this.termsConditionFirst = true && this.termsConditionSecond == false && this.termsConditionThird == true && this.termsConditionFour == true){   
      if(this.adhaar.trim() == "" || this.adhaar.trim() == undefined || this.adhaar.trim() == null){
        this.showToast("Enter your adhaar number");
      }else if(this.adhaar.length > 12){
        this.showToast("Enter your 12 digit adhaar number");
      }else if(this.mobile.trim() == undefined || this.mobile.trim() == "" || this.mobile.trim() == null){
        this.showToast("Enter your mobile number");    
      }else if(this.mobile.length >10){
        this.showToast("Enter your 10 digit mobile number");
      }else{
        let proposerMobile = this.mobile;
        this.lastDigits = proposerMobile.substring(8);
        console.log("2 :" + this.lastDigits);
        //console.log("email: " + this.memberDetails[0].proposerEmail);
        let emailStringSplit = (this.memberDetails[0].proposerEmail).split('@');
        this.emailString = emailStringSplit[1];
        console.log("email: " + this.emailString);
        this.saveEmailData(this.selectedCardArray); 
        //this.getGenerateOtpServiceCall();
      }
      
    }else{
      this.showToast("Please accept Terms & Condition");
    }
  }

  saveEmailData(InsuredCardDetails){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest(InsuredCardDetails)),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  callEProposalService(){   
    var eProposalData = {
          "QuotationID": this.selectedCardArray.QuotationID,
          "UID": this.selectedCardArray.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }

  sendEProposerData(serviceData){
    this.presentLoadingDefault();
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup=false;
          //this.closeModalPop();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp.ReturnCode == "807"){
          this.loading.dismiss();
          //this.showToast(resp.ReturnMsg);
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(resp.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.message = resp.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(resp.ReturnMsg); 
        }        
       
      }, (err) => {
      console.log(err);
      });
  }

  OkButton(){
    this.thankyouPopup = true;
    this.navCtrl.push(LandingScreenPage);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }


}
