import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardListingPage } from './dashboard-listing';

@NgModule({
  declarations: [
    // DashboardListingPage,
  ],
  imports: [
    IonicPageModule.forChild(DashboardListingPage),
  ],
})
export class DashboardListingPageModule {}
