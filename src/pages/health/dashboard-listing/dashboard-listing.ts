import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController,Platform  } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { BuyProposerDetailsPage } from '../buy-proposer-details/buy-proposer-details';
import { CallNumber } from '@ionic-native/call-number';
import { DatePicker } from '@ionic-native/date-picker';
import { Jsonp } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LoginPage } from '../login/login';
import { VecproposerdetailsPage } from '../../vector/vecproposerdetails/vecproposerdetails';
/**
 * Generated class for the DashboardListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard-listing',
  templateUrl: 'dashboard-listing.html',
})
export class DashboardListingPage {

  policy_type;
  loading;
  pdfLink;
  policyInCompleteArray = [];
  quotesGenerationArray = [];
  policiesIssuedArray = [];
  rejectedPoliciesArray = [];
  eProposalStatsArray = [];

  policyInCompleteUnFliteredArray = [];
  quotesGenerationUnFliteredArray = [];
  policiesIssuedUnFliteredArray = [];
  rejectedPoliciesUnFliteredArray = [];
  eProposalUnFliteredArray = [];

  memberArray = [];
  selectedCardArray = [];
  beneficiaryArray = [];
  dateRange = true;
  dateRangeDisplay = true;
  fromDate;
  toDate;
  searchBlock;
  searchText = "";
  filterCount = 0;

  //------Variables for policy count obtained  from previous page-----------//
  totalPoliciesIssuedCount : number = 0;
  incompletePoliciesCount: number = 0;
  rejectedPoliciesCount: number = 0;
  totalQuoteGeneratedCount: number = 0;
  totalEProposalCount: Number = 0;
  activeStatus = false;
  policyTempData=[];
  policyData;
  date: Date;
  hideshowDiv = true;
  showTransBaground2 = false;
  policyModePopup;
  isClassOneActive = false;
  showPolicyMode = false;
  message;
  serviceResponsePopup = true;
  serviceCodeStatus;
  showRow = true;
  
  myMember=[{"id":"SELF","imgPath":"assets/imgs/self.png"},{"id":"SPOU","imgPath":"assets/imgs/spouse.png"},
  {"id":"SON","imgPath":"assets/imgs/son.png"},{"id":"DAUG","imgPath":"assets/imgs/daughterIcon.png"},
  {"id":"PARE","imgPath":"assets/imgs/self.png"},{"id":"GRCH","imgPath":"assets/imgs/self.png"},
  {"id":"SIB","imgPath":"assets/imgs/self.png"},{"id":"CHLD","imgPath":"assets/imgs/self.png"},{"id":"GRPA","imgPath":"assets/imgs/self.png"}]


  constructor(private platform: Platform, public navCtrl: NavController, public navParams: NavParams, private appService: AppService, private loadingCtrl: LoadingController,
  private toast: ToastController, private callNumber: CallNumber,private datePicker: DatePicker, private iab: InAppBrowser) {
    
  }

  ionViewDidLoad() {
    this.setCounts(this.navParams.get("TotalPolicyCount"),this.navParams.get("IncompletePolicyCount"),this.navParams.get("RejectedPoliciesCount"),this.navParams.get("TotalQuoteGeneratedCount"));
    // this.incompletePoliciesCount = this.navParams.get("IncompletePolicyCount");
    // this.rejectedPoliciesCount = this.navParams.get("RejectedPoliciesCount");
    // this.totalQuoteGeneratedCount = this.navParams.get("TotalQuoteGeneratedCount");
    
    this.showSelectedTabData(this.navParams.get("Type"), "all", "all");
    console.log("active: " + this.activeStatus);
     
  //     setTimeout(function() {
  //     document.getElementById("splash").style.display='none';
  // }, 3000);
  }

  setCounts(total,incmplete,rejected,totalQuote){
    this.totalPoliciesIssuedCount = total;
    this.incompletePoliciesCount = incmplete;
    this.rejectedPoliciesCount = rejected;
    this.totalQuoteGeneratedCount = totalQuote;
    //this.totalEProposalCount = 
    console.log(this.totalPoliciesIssuedCount,this.incompletePoliciesCount,this.rejectedPoliciesCount,this.totalQuoteGeneratedCount+"<<<<>>>>>>");
  }

  /*call service based on selected segement*/ 
  showSelectedTabData(type, fromDate, toDate){
    this.filterCount = 0;
    switch(type){
      case 1:
        this.policy_type ="policy_issued";
        this.policiesIssuedArray = [];
        let temp = this.incompletePoliciesCount;
        console.log(this.totalPoliciesIssuedCount,this.incompletePoliciesCount,this.rejectedPoliciesCount,this.totalQuoteGeneratedCount+"<<<<>>>>>>");
        //console.log(temp);
        this.callPoliciesIssued(fromDate, toDate);
        break;
      case 2:
        this.policy_type ="policy_incomplete";
        this.policyInCompleteArray = [];
        this.callIncompletePolicy(fromDate, toDate);
        break;
      case 3:
        this.policy_type ="policy_rejected";
        this.rejectedPoliciesArray = [];
        this.callRejectedPolicies(fromDate, toDate);
        break;
      case 4:
        this.policy_type ="quotes_generated";
        this.quotesGenerationArray = [];
        this.callQuotesGenerated(fromDate, toDate);
        break;
      case 5:
        this.policy_type ="e_proposal";
        this.eProposalStatsArray = [];
        this.callEProposal(fromDate, toDate);
        break;  
      default:
        break;
    }
  }

  onTypeChange(e){
    this.filterCount = 0;
    switch(e.value){
      case "policy_issued":
        this.policiesIssuedArray = [];
        this.callPoliciesIssued("all", "all");
        break;
      case "policy_incomplete":
        this.policyInCompleteArray = [];
        this.callIncompletePolicy("all", "all");
        break;
      case "policy_rejected":
        this.rejectedPoliciesArray = [];
        this.callRejectedPolicies("all", "all");
        break;
      case "quotes_generated":
        this.quotesGenerationArray = [];
        this.callQuotesGenerated("all", "all");
        break;
      case "e_proposal":
        this.eProposalStatsArray = [];
        this.callEProposal("all", "all");
        break;  
      default:
        break;
    }
    
  }
  /*call service based on selected segement*/ 


  /* For Incomplete Policies*/
  callIncompletePolicy(fromDate, toDate){
    let data = {
      "fromdate":fromDate,
      "todate":toDate
      };
    var sendData = this.appService.encryptData(JSON.stringify(data),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    // var sendData = this.appService.encryptData(JSON.stringify(data),"840884AF-0241-42C6-B88C-705F4937E273~f276bce6-ea39-4bdd-8698-25ea8c1e9645"); 
    this.sendData("Dashboard.svc/GetIncompletePolicy",{'request': sendData},this.incompletePolicyCallback);   
  }

  incompletePolicyCallback(Resp, context,totalPoliciesIssuedCount,incompletePoliciesCount,rejectedPoliciesCount,totalQuoteGeneratedCount){
    let respData = Resp.GetIncompletePolicyResult;
    sessionStorage.TokenId = respData.UserToken.TokenId;
    let tempDate = "";
    console.log(">>>>>>>>>>>>> Quote Generated"+JSON.stringify(respData));
    if(respData.ReturnCode == "0"){
      for(let i=0; i < incompletePoliciesCount; i++){
        let data = respData.Data.IncompletePolicies[i].PolicyDetails;
        data.createdOn = data.createdOn.split(' ')[0];
        tempDate == data.createdOn ? data.createdOn = "" :tempDate = data.createdOn;
        data.isDisabled = true;
        context.policyInCompleteArray.push(data);  
        context.policyInCompleteUnFliteredArray.push(data);  
      }
    }else{
      context.showToast(respData.ReturnMsg);  
    }
  }
  
  onViewClick(index,event){
    console.log(index+" is clicked "+this.policyInCompleteArray[index].UID);
    this.callHalfFilledDetails(this.policyInCompleteArray[index].QuotationID, this.policyInCompleteArray[index].UID, this.policyInCompleteArray[index].PID);
  }

  callHalfFilledDetails(qID,uID,pID){
    let data = {
      "QuotationID": qID,
      "UID": uID,
      "PID": pID
    };
    var sendData = this.appService.encryptData(JSON.stringify(data),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendData("Dashboard.svc/GetHalfFilledDetails",{'request': sendData},this.halfFilledDetailsCallback);   
  }

  halfFilledDetailsCallback(Resp, context){
    let respData = Resp.GetHalfFilledDetailsResult;
    sessionStorage.TokenId = respData.UserToken.TokenId;
    
    if(respData.ReturnCode == "0"){
      let userData = JSON.parse(Resp.GetHalfFilledDetailsResult.Data.data);
      console.log("new" + JSON.stringify(userData));
      
      context.memberArray = userData.Risk.MemberDetails;
      context.selectedCardArray = userData.Risk.CardDetails;
       if(userData.Risk.PolicyType == "HLI"){
        context.navCtrl.push(VecproposerdetailsPage,{"buyPageVectorMemberDetailsResult":context.memberArray, "buyPageCardDetailsResult":context.selectedCardArray, "fromDashboardListing": "true"});
       }else if(userData.Risk.PolicyType == "HTI" || userData.Risk.PolicyType == "HTF"){
        context.navCtrl.push(BuyProposerDetailsPage,{"buyPageMemberDetailsResult":context.memberArray, "buyPageCardDetailsResult":context.selectedCardArray, "fromDashboardListing": "true"});
       }
      
    }
    
  }
  
  /* For Incomplete Policies*/

  /* For Quotes generated*/
  callQuotesGenerated(fromDate, toDate){
    let data = {
      "fromdate":fromDate,
      "todate":toDate
      };
    var sendData = this.appService.encryptData(JSON.stringify(data),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendData("Dashboard.svc/GetQuotesGenerated",{'request': sendData},this.quotesGeneratedCallback);   
  }

  quotesGeneratedCallback(Resp, context,totalPoliciesIssuedCount,incompletePoliciesCount,rejectedPoliciesCount,totalQuoteGeneratedCount){
    let respData = Resp.GetQuotesGeneratedResult;
    sessionStorage.TokenId = respData.UserToken.TokenId;
    let tempDate = "";
    if(respData.ReturnCode == "0"){
      for(let i=0; i < totalQuoteGeneratedCount; i++){
        let data = respData.Data.Quotes[i];
        console.log("dateIssue: " + JSON.stringify(data));        
        data.createdOn = data.createdOn.split(' ')[0];
        console.log("date:" + data.createdOn.split(' ')[0]);
        tempDate == data.createdOn ? data.createdOn = "" :tempDate = data.createdOn;
        data.isDisabled = true;
        context.quotesGenerationArray.push(data);
        context.quotesGenerationUnFliteredArray.push(data);
      }
    }else{
      context.showToast(respData.ReturnMsg);    
    }
  }
  /* For Quotes generated*/

  /* For rejected policies*/
  callRejectedPolicies(fromDate, toDate){
    let data = {
      "fromdate":fromDate,
      "todate":toDate
      };
    var sendData = this.appService.encryptData(JSON.stringify(data),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendData("Dashboard.svc/GetRejectedPolicy",{'request': sendData},this.quotesRejectedPolicies);   
  }

  quotesRejectedPolicies(Resp, context,totalPoliciesIssuedCount,incompletePoliciesCount,rejectedPoliciesCount,totalQuoteGeneratedCount){
    let respData = Resp.GetRejectedPolicyResult;
    sessionStorage.TokenId = respData.UserToken.TokenId;
    let tempDate = "";
    if(respData.ReturnCode == "0"){
      for(let i=0; i < rejectedPoliciesCount; i++){
        let data = respData.Data.IncompletePolicies[i].PolicyDetails;
        data.createdOn = data.createdOn.split(' ')[0];
        tempDate == data.createdOn ? data.createdOn = "" :tempDate = data.createdOn;
        data.isDisabled = true;
        context.rejectedPoliciesArray.push(data);  
        context.rejectedPoliciesUnFliteredArray.push(data);  
      }
    }else{
      context.showToast(respData.ReturnMsg);    
    }
  }
 /* For rejected policies*/

  /* For Policies Issued*/
  callPoliciesIssued(fromDate, toDate){
    let data = {
      "fromdate":fromDate,
      "todate":toDate
      };
      console.log(this.incompletePoliciesCount);
    var sendData = this.appService.encryptData(JSON.stringify(data),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendData("Dashboard.svc/GetPoliciesIssued",{'request': sendData},this.policiesIssuedCallback);   
  }

  policiesIssuedCallback(Resp, context,totalPoliciesIssuedCount,incompletePoliciesCount,rejectedPoliciesCount,totalQuoteGeneratedCount){
    let respData = Resp.GetPoliciesIssuedResult;
    let tempDate = "";
    sessionStorage.TokenId = respData.UserToken.TokenId;
    console.log("<<<<<<<<>>>>>>");
    console.log("test:" + JSON.stringify(totalPoliciesIssuedCount));
    console.log("test:" + JSON.stringify(respData));

    if(respData.ReturnCode == "0"){
      for(let i=0; i < totalPoliciesIssuedCount; i++){
        let data = respData.Data.Quotes[i];
        console.log("issued" + JSON.stringify(data));
        
        data.createdOn = data.createdOn.split(' ')[0];
        tempDate == data.createdOn ? data.createdOn = "" :tempDate = data.createdOn;
        console.log("" + data.createdOn);
        data.isDisabled = true;
        context.policiesIssuedArray.push(data);
        context.policiesIssuedUnFliteredArray.push(data);
      }
    }else{
      context.showToast(respData.ReturnMsg);    
    }
  }
  /* For Policies Issued*/

  /* For E-Proposal Stats*/
  callEProposal(fromDate, toDate){
    let data = {
      "fromdate":fromDate,
      "todate":toDate
      };
    var sendData = this.appService.encryptData(JSON.stringify(data),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendData("Dashboard.svc/EproposalStats",{'request': sendData},this.eProposalCallback);   
  }

  eProposalCallback(Resp, context,totalPoliciesIssuedCount,incompletePoliciesCount,rejectedPoliciesCount,totalQuoteGeneratedCount){
    console.log("E-Proposal" + JSON.stringify(Resp));
    
    let respData = Resp.EproposalStatsResult;
    let eProposalCount = Resp.EproposalStatsResult.Data.EproposalResponseSet.length;
    console.log("CountEProposal" + eProposalCount);
    
    sessionStorage.TokenId = respData.UserToken.TokenId;
    let tempDate = "";
    if(respData.ReturnCode == "0"){
      for(let i=0; i < eProposalCount; i++){
        let data = respData.Data.EproposalResponseSet[i];
        console.log("dateIssue: " + JSON.stringify(data));        
        data.createdOn = data.createdOn.split(' ')[0];
        console.log("date:" + data.createdOn.split(' ')[0]);
        tempDate == data.createdOn ? data.createdOn = "" :tempDate = data.createdOn;
        data.isDisabled = true;
        context.eProposalStatsArray.push(data);
        context.eProposalUnFliteredArray.push(data);
        // if(data.PolicyNo == ""){
        //   this.showRow = true;
        // }else{
        //   this.showRow = false;
        // }
      }
    }else{
      context.showToast(respData.ReturnMsg);    
    }
  }
  /* For Quotes generated*/
   
  sendData(URL,serviceData, callbackFunc){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      console.log("Quote Generated"+ JSON.stringify(Resp));
      
      this.loading.dismiss();   
      this.policyData = Resp;  
      callbackFunc(Resp, this,this.totalPoliciesIssuedCount,this.incompletePoliciesCount,this.rejectedPoliciesCount,this.totalQuoteGeneratedCount);
    });
  }

  showToast(Message) {
    let pageToast = this.toast.create({
      message: Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 2500,
    });
    pageToast.present();
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365) + " years";
  }
  
  contactUser(mobile){
    this.callNumber.callNumber(mobile, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  onViewButtonClick(event, block, item){
   // console.log("policyDetails" + JSON.stringify(item));
    
    block.isDisabled = false;
    item.close();
    item.setElementClass("active-sliding", false);
    item.setElementClass("active-slide", false);
    item.setElementClass("active-options-right", false); 
  }

  showDatePicker(){
    this.dateRange = false;
  }
  
  dateAlteration(Num){if(Num>=10){return Num}else{return"0"+Num}}

  fromDatePick(event){
    let element = this.fromDate;
    let dateFieldValue = element;
    let year : any = 1985, month : any = 1, date : any = 1;
  
    if(dateFieldValue != undefined){
      date = dateFieldValue.split("-")[0]
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }  
        //minDateLimit  = ( new Date(this.today.getFullYear()-25,this.today.getMonth(),this.today.getDate()).getTime());
        this.datePicker.show({
          date: new Date(year, month-1, date),
          //minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
        }).then(
          date => {
            let dt = ( this.dateAlteration(date.getDate())+"-"+this.dateAlteration(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
            this.fromDate= dt.toString();          
          },
          err => console.log('Error occurred while getting date: ', err)
        );
  }
  
  toDatePick(event){
    let element = this.toDate;
    let dateFieldValue = element;
    let year : any = 1985, month : any = 1, date : any = 1;
  
    if(dateFieldValue != undefined){
      date = dateFieldValue.split("-")[0];
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2];
    }    
        //minDateLimit  = ( new Date(this.today.getFullYear()-25,this.today.getMonth(),this.today.getDate()).getTime());
        this.datePicker.show({
          date: new Date(year, month-1, date),
          //minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
        }).then(
          date => {
            let dt = ( this.dateAlteration(date.getDate())+"-"+this.dateAlteration(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
            this.toDate = dt.toString();          
          },
          err => console.log('Error occurred while getting date: ', err)
        );
  }
  
  onSubmitClick(){
    let type = this.policy_type == "policy_issued" ? 1 : 
               this.policy_type == "policy_incomplete" ? 2 : 
               this.policy_type == "policy_rejected" ? 3 : 4 ; 
    this.showSelectedTabData(type, this.fromDate, this.toDate);
    this.dateRange = true;
    this.dateRangeDisplay = false;
    this.toDate = "";
    this.fromDate = "";
  }
  
  onCancelClick(){
    this.dateRange = true; 
    this.toDate = "";
    this.fromDate = "";
  }

  onSearchClick(ev){
    
  
    if(this.searchText && this.searchText.trim() != ''){
      if(this.policy_type == "policy_issued"){
        this.policiesIssuedArray = [];
        this.policiesIssuedArray = this.filterData(this.policiesIssuedUnFliteredArray);        
        if(this.policiesIssuedArray.length == 0){
          this.hideshowDiv = false;
          this.searchBlock = true;
          this.showToast("No results found");
        }else{
          this.hideshowDiv = true;
          this.searchBlock = false;
        }
      }else if(this.policy_type == "policy_incomplete"){
        this.policyInCompleteArray = [];
        this.policyInCompleteArray = this.filterData(this.policyInCompleteUnFliteredArray);
    
        if(this.policyInCompleteArray.length == 0){
          this.hideshowDiv = false;
          this.searchBlock = true;
          this.showToast("No results found");
        }else{
          this.hideshowDiv = true;
          this.searchBlock = false;
        }
      }else if(this.policy_type == "policy_rejected"){
        this.rejectedPoliciesArray = [];
        this.rejectedPoliciesArray = this.filterData(this.rejectedPoliciesUnFliteredArray);
        ;
        if(this.rejectedPoliciesArray.length == 0){
          this.hideshowDiv = false;
          this.searchBlock = true;
          this.showToast("No results found");
        }else{
          this.hideshowDiv = true;
          this.searchBlock = false;
        }
      }else if(this.policy_type == "quotes_generated"){
        this.quotesGenerationArray = [];
        this.quotesGenerationArray = this.filterData(this.quotesGenerationUnFliteredArray);
        if(this.quotesGenerationArray.length == 0){
          this.hideshowDiv = false;
          this.searchBlock = true;
          this.showToast("No results found");
        }else{
          this.hideshowDiv = true;
          this.searchBlock = false;
        }
      }
    }else{
      this.showToast("You need to enter your query before pressing search.")
    }
    this.searchText = "";
  }

  filterData(unFliteredArray){
    var fliteredArray : any = [];
    if(this.searchText && this.searchText.trim() != ''){
      for(let i = 0; i< unFliteredArray.length ; i++){
        if(unFliteredArray[i].PolicyNo.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1){
          fliteredArray.push(unFliteredArray[i]);
        }
        if(unFliteredArray[i].FirstName.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1){
          fliteredArray.push(unFliteredArray[i]);
        }
        if(unFliteredArray[i].MobileNo.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1){
          fliteredArray.push(unFliteredArray[i]);
        }
      }
    }
    this.filterCount = fliteredArray.length;
    return fliteredArray;
  }

  lookActivePolicies(){
    this.policiesIssuedArray= [];
    console.log("active: " + this.activeStatus);
    let policyIssuedData = this.policyData.GetPoliciesIssuedResult.Data.Quotes;
    if(this.activeStatus == true){
      console.log("temp: " + JSON.stringify(this.policyTempData));
      for(let i=0; i<policyIssuedData.length; i++){
        this.date = new Date(policyIssuedData[i].PolicyEndDate);       
        let datemilsec = this.date.getTime();
        console.log("Date:" + this.date);
        console.log("Date:" + datemilsec);
        console.log("Date:" + Date.now());
        
        if(datemilsec > Date.now()){   
          console.log("IF");      
          this.policiesIssuedArray.push(policyIssuedData[i]); 
        }else{
          console.log("ELSE");
                   
        } 
      }
    }else{
      this.callPoliciesIssued("all", "all");
    }
  }

  getPDF(item){
    console.log("DashboardListing" + JSON.stringify(item));  
    
    let serviceData = {
      "username" : sessionStorage.username,
      "policyno" : item.PolicyNo
    }
    console.log("pdfData: " + JSON.stringify(serviceData));
    
    var sendData = this.appService.encryptData(JSON.stringify(serviceData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.getPDFServiceCall("Dashboard.svc/GetPDFDashboard",{'request': sendData})
  }

  getPDFServiceCall(url, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(url,serviceData,headerString)
    .subscribe(Resp =>{
     
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.GetPDFDashboardResult.ReturnCode == "0"){ 
          this.loading.dismiss();                
          sessionStorage.TokenId = Resp.GetPDFDashboardResult.UserToken.TokenId;
          //this.pID = Resp.Proceed_PolicyResult.Data.pID;
          //let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          //let UID = Resp.Proceed_PolicyResult.Data.UID;
          this.showToast(Resp.GetPDFDashboardResult.ReturnMsg);

          this.pdfLink = Resp.GetPDFDashboardResult.Data.PdfLink;

          this.showPDF(this.pdfLink);

          


          //window.open(this.pdfLink,'_system','location=no');    
         
          // const browser = this.iab.create(this.pdfLink);
          // browser.show();

          //Prachi=======
          // let path = null;
 
          // if (this.platform.is('ios')) {
          //   path = this.file.documentsDirectory;
          // } else if (this.platform.is('android')) {
          //   path = this.file.dataDirectory;
          // }
       
          // const transfer = this.transfer.create();
          // transfer.download(this.pdfLink, path + 'myfile.pdf').then(entry => {
          //   let url = entry.toURL();
          //   this.document.viewDocument(url, 'application/pdf', {});
          // });
          // const options: DocumentViewerOptions = {
          //   title: 'My PDF'
          // }
          // this.document.viewDocument(this.pdfLink, 'application/pdf', options);
          //this.callEProposalService();
          //console.log("card: "+ JSON.stringify(this.individualDisplayData));
          
        }else if(Resp.GetPDFDashboardResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.GetPDFDashboardResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(Resp.GetPDFDashboardResult.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.GetPDFDashboardResult.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else if(Resp.GetPDFDashboardResult.ReturnCode == "501"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.GetPDFDashboardResult.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = Resp.GetPDFDashboardResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 501;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.GetPDFDashboardResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          //this.serviceResponsePopup = false;
          //this.serviceCodeStatus = 400;
        }
    });
    
  }

  showPDF(pdfURL){
    console.log("URL: " + pdfURL);    
    // let browser = this.iab.create(pdfURL  ,'_blank', 'location=no');
    // browser.show();
    window.open(pdfURL,'_system','location=no'); 
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400 || this.serviceCodeStatus == 501){
      this.serviceResponsePopup = true;     
    }else if(this.serviceCodeStatus == 0){
      this.serviceResponsePopup = true;
    }
  
  }


  // hideShowAddmember(Num){
  //   if(Num == 1){
  //     this.showTransBaground2 = !this.showTransBaground2;   
  //     //this.policyModePopup = JSON.parse(JSON.stringify(this.membersShow));
  //     this.isClassOneActive = true;
  //     this.showPolicyMode = true;
  //   }else if(Num == 2){
  //     this.isClassOneActive = false;
  //     setTimeout( () => { 
  //           this.showPolicyMode = false;
  //           this.showTransBaground2 = !this.showTransBaground2;  
  //     }, 1000);
  //   }else if(Num == 3){
  //     for(let i = 0; i < this.policyModePopup.length; i++){
  //       if(!this.policyModePopup[i].showFamilyMemberOnScreen && this.policyModePopup[i].memberSelectedOnAddmember){
  //         this.policyModePopup[i].showFamilyMemberOnScreen = this.policyModePopup[i].memberSelectedOnAddmember;
  //         this.policyModePopup[i].memberSelectedOnAddmember = this.policyModePopup[i].memberSelectedOnAddmember;
  //       }
        
  //     }   
  //     this.isClassOneActive = false;
  //     setTimeout( () => { 
  //           this.showPolicyMode = false;
  //           this.showTransBaground2 = !this.showTransBaground2;  
  //     }, 1000);
  //   }
  // }

  sendEmail(item){
    let serviceData = {
      "QuotationID" : item.QuotationID,
      "UID" : item.UID
    }
    console.log("pdfData: " + JSON.stringify(serviceData));
    
    var sendData = this.appService.encryptData(JSON.stringify(serviceData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendEmailServiceCall("FGHealth.svc/Retrigger_Eproposal",{'request': sendData})
  }

  sendEmailServiceCall(url, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(url,serviceData,headerString)
    .subscribe(Resp =>{
     
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Retrigger_EproposalResult.ReturnCode == "0"){ 
          this.loading.dismiss();                
          sessionStorage.TokenId = Resp.Retrigger_EproposalResult.UserToken.TokenId;
          //this.pID = Resp.Proceed_PolicyResult.Data.pID;
          //let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          //let UID = Resp.Proceed_PolicyResult.Data.UID;
          this.message = Resp.Retrigger_EproposalResult.ReturnMsg;
          this.serviceCodeStatus = 0;
          this.serviceResponsePopup = false;
         // this.showToast(Resp.Retrigger_EproposalResult.ReturnMsg);
    
          
        }else if(Resp.Retrigger_EproposalResult.ReturnCode == "807"){
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Retrigger_EproposalResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
          this.message = Resp.Retrigger_EproposalResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(Resp.Retrigger_EproposalResult.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Retrigger_EproposalResult.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Retrigger_EproposalResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else if(Resp.Retrigger_EproposalResult.ReturnCode == "501"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Retrigger_EproposalResult.UserToken.TokenId;
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Retrigger_EproposalResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 501;
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Retrigger_EproposalResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          //this.serviceResponsePopup = false;
          //this.serviceCodeStatus = 400;
        }
    });
  }



}
