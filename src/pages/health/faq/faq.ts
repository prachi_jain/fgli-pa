import { Component } from '@angular/core';
import * as $ from 'jquery';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {

  vectorDiv = true;
  healthDiv = false;
  productList = [];
  product = "HTO";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.productList = JSON.parse(localStorage.ProductData);
    console.log("list" + localStorage.ProductData);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
    document.getElementById("splash").style.display='none';
    $(".accordionWrapper h4").click(function(){
      $(this).toggleClass('active');
      $(".accordionWrapper h4").not($(this)).removeClass('active');
      $(this).next(".accordContents").slideToggle();
      $(".accordionWrapper .accordContents").not($(this).next()).slideUp();
    });  
  }

  changeProduct(){
    //this.getKnowledgeBaseData(this.product);
    if(this.product == "HTO"){
      this.healthDiv = false;
      this.vectorDiv = true;
    }else if(this.product == "vector"){
      this.healthDiv = true;
      this.vectorDiv = false;
    }else if(this.product == "topup"){
      this.healthDiv = true;
      this.vectorDiv = true;
    }else if(this.product == "pa"){
      this.healthDiv = true;
      this.vectorDiv = true;
    }
  }

}
