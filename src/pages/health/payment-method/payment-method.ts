import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, Alert ,ToastController,LoadingController} from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppService } from '../../../providers/app-service/app-service';
import { PaymentSuccessPage } from '../payment-success/payment-success';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { DatePicker } from '@ionic-native/date-picker';
import { Base64 } from '@ionic-native/base64';
import { LoginPage } from '../login/login';
//import { parseDate } from 'ionic-angular/util/datetime-util';
/**
 * Generated class for the PaymentMethodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-method',
  templateUrl: 'payment-method.html',
})
export class PaymentMethodPage {
  
  pgUrl;
  fUrl;
  sUrl;
  TracsactionDetails;
  PaymentRequest;
  canShowToast = true;
  loading;
  pID;
  ImageData;
  memberDetails;
  issuerName = "";
  bankName = "";
  chequeNumber = "";
  chequeDate = "";
  chequeAmount;
  numberLenght(Num){if(Num>=10){return Num}else{return"0"+Num}}
  imageView = "";
  imageCaptured = true;
  payUTracsactionID;
  ClientDetails = [];
  spouseGender = "F";
  QuotationID;
  UID;
  crtData;
  ReferenceNo;
  email;
  mobile;
  thumbnail;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  countryFocus =false;
  bank_list = [];
  bank_listcopy = [];
  bankId;
  bankSelect;
  //Live
  //baseUrl = "https://i-insure.futuregenerali.in/";
  //UAT
  baseUrl = "https://i-insureuat.futuregenerali.in/";
  

  /* camera options */
  options: CameraOptions = {
    quality: 50,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  getClientNode(){
    let age = this.memberDetails[0].age.split("-");
    let clientSalution = "MR";
    if(this.memberDetails[0].Gender == "F"){
      clientSalution = "MRS";
      this.spouseGender = "M";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberDetails[0].proposerName ,
      "LastName": this.memberDetails[0].proposerName,
      "DOB": age[0]+"/"+age[1]+"/"+age[2],
      "Gender": this.memberDetails[0].Gender,
      "MaritalStatus": this.memberDetails[0].maritalCode,
      "Occupation": this.memberDetails[0].occupationCode,
      "PANNo": this.memberDetails[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberDetails[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetails[0].pincode,
        "City": this.memberDetails[0].city,
        "State": this.memberDetails[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].verifiedNumber,
        "EmailAddr": this.memberDetails[0].proposerEmail
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetails[0].pincode,
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo":this.memberDetails[0].verifiedNumber,
        "EmailAddr": this.memberDetails[0].proposerEmail
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  
  }

  getBeneficiaryDetails(){
    console.log("Payment: " + JSON.stringify(this.TracsactionDetails));
    
    let BeneDetails = [];
    for(let i = 0 ;i< this.TracsactionDetails.Request.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberDetails.length;j++){
        if( (this.memberDetails[j].code).toUpperCase()== (this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].Relation).toUpperCase() ){
          let mDob = this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ");
          let gendr = "M";
          if(this.memberDetails[j].title.toLowerCase().indexOf("mother") >=0 || this.memberDetails[j].title.toLowerCase().indexOf("daughter") >=0){
            gendr = "F";
          }else if(this.memberDetails[j].title.toLowerCase() == "spouse"){
            gendr = this.spouseGender;
          }else if(this.memberDetails[j].title.toLowerCase() == "self"){
            gendr = this.memberDetails[0].Gender;
          }
          BeneDetails.push({
            "MemberId": this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
            "InsuredName": this.memberDetails[j].proposerName,
            "InsuredDob": mDob[0],
            "InsuredGender": gendr,
            "InsuredOccpn": this.memberDetails[j].occupationCode,
            "CoverType": this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].CoverType,
            "SumInsured": this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
            "DeductibleDiscount": "0",
            "Relation": this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].Relation,
            "NomineeName": this.memberDetails[j].NomineeName,
            "NomineeRelation": this.memberDetails[j].NomineeRelation,
            "AnualIncome": "",
            "Height": this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].Height,
            "Weight": this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].Weight,
            "NomineeAge": "",
            "AppointeeName": "",
            "AptRelWithominee": "",
            "MedicalLoading": this.TracsactionDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
            "PreExstDisease": "N",
            "DiseaseMedicalHistoryList": {
              "DiseaseMedicalHistory": {
                "PreExistingDiseaseCode": "",
                "MedicalHistoryDetail": ""
              }
            }
          })
        }

      }

    }
    return BeneDetails;

  }  




  constructor(private platform: Platform, private base64: Base64, private datePicker: DatePicker,public navCtrl: NavController, public navParams: NavParams,public camera: Camera,public loadingCtrl: LoadingController,public toast: ToastController,private iab: InAppBrowser, public appService: AppService) {
    this.TracsactionDetails = navParams.get("planCardDetails");
    this.memberDetails = navParams.get("memberDetails");
    this.bank_listcopy = JSON.parse(localStorage.BankData);
    // this.bank_list =  [{"BankKey": "ZSK",
    // "BankName": "ZILA SAHAKARI KENDRIYA BK LTD",
    // "Currency": "INR"},
    // {
    //   "BankKey": "ZSK",
    //   "BankName": "HELLOOO",
    //   "Currency": "INR"
    // }
      
    // ];

    console.log("Bank" + JSON.stringify(this.bank_list));
    

    console.log("TracsactionDetails: "+ JSON.stringify(this.TracsactionDetails));
    console.log(this.memberDetails);
    console.log(this.getClientNode());
    console.log(this.getBeneficiaryDetails());
    console.log(this.getClientNode());

    // platform.registerBackButtonAction(() => {
    //   this.navCtrl.pop();
    // });
    
    

    this.presentLoadingDefault();
    this.loading.dismiss();
   // this.encryptData();

  }

  ionViewDidLoad() {      
    //this.encryptCrtData();
    this.thumbnail = "assets/imgs/proposer_profile_pic.svg"
    this.QuotationID = this.TracsactionDetails.QuotationID;
    this.UID = this.TracsactionDetails.UID;
    this.ReferenceNo = this.TracsactionDetails.ReferenceNo;
    sessionStorage.RefNO = this.ReferenceNo;
    this.email = sessionStorage.email;
    this.mobile = sessionStorage.mobile;
    this.chequeAmount = this.TracsactionDetails.CalculatedServiceResp.PremWithServTax;
    console.log("amount: " + this.chequeAmount);
    
  }

/*   "CURL":"http://fghealthapi.idealake.com/PayU/Cancel.aspx",
"FURL":"http://fghealthapi.idealake.com/PayU/Failure.aspx",
"FirstName":"Sachin Redekar",
"PGURL":"https://test.payu.in/_payment",
"Productinfo":"HTO APP for Health Total",
"SALT":"4jXde5Vt",
"marchantkey":"QWS0ZF"   
"SURL":"http://fghealthapi.idealake.com/PayU/Success.aspx",
 */
  
 /* show loading */
  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  
/*   getServiceResponse(serviceData){
    this.loading.present();
    this.appService.callService("Payment.svc/PaymentRequest",serviceData)
      .subscribe(payReq =>{
        this.loading.dismiss(); 
        console.log(payReq);

      });
  } */

  /* select Date */
  DatePick(){
    let maxLimit = new Date(new Date().setDate(new Date().getDate() + 30))
    //let dd = today.getMilliseconds();
  
   // console.log("date" + dd + (1000*60*60*24*30));    
    
      this.datePicker.show({
        date: new Date(),
        minDate: Date.now(),
        maxDate: maxLimit.getTime(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      }).then(
        date => {
          let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear())).toString();
          //this.numberLength(date.getDate())+"-"+this.numberLength(date.getMonth() + 1)+"-"+(date.getFullYear())
          this.chequeDate = dt.toString();
        console.log("dateformat"+ this.chequeDate)},
        err => console.log('Error occurred while getting date: ', err)
      );
  }

  /* online payment inappbrowser */
  openBrowser(){

    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    let request = this.appService.encryptData(JSON.stringify({"QuotationID":this.QuotationID,"UID":this.UID,"PID":sessionStorage.policyID}),sessionStorage.TokenId);

    let browser = this.iab.create(this.baseUrl+'PayU/PGRequest.aspx?request='+encodeURIComponent(request)+'&tokenID='+encodeURI(headerString), '_blank', 'location=no');
    let watch = browser.on('loadstart').subscribe(type => {
      console.log(JSON.stringify(type));
      if(type.url.indexOf("?")>=0){
        let url = type.url.split("?");
        console.log("Active Page URL : "+ JSON.stringify(url));
        if(url[0]== this.baseUrl+"PayU/Cancel.aspx"){
                let TIME_IN_MS = 10000;
                let hideFooterTimeout = setTimeout( () => {
                browser.close();
            }, TIME_IN_MS);         
          let err = url[1].split("=");
          sessionStorage.TokenId = err[1];
        }else if(url[0] == this.baseUrl+"PayU/Failure.aspx" ){
          let TIME_IN_MS = 10000;
          let hideFooterTimeout = setTimeout( () => {
          browser.close();
      }, TIME_IN_MS);  
      var paymentFailureData = {       
        "UID" : this.UID,  
     };
      console.log("sendData" + JSON.stringify(paymentFailureData));
      
      let dataToSend = this.appService.encryptData(JSON.stringify(paymentFailureData),sessionStorage.TokenId); 
      let request = {"request":dataToSend};
      console.log("encryptData: " + JSON.stringify(dataToSend));
      
      this.callPaymentFailureService("/Payment.svc/TransactionFailure",request);           
        }else if(url[0]== this.baseUrl+"PayU/Success.aspx"){
          let TIME_IN_MS = 10000;
          let hideFooterTimeout = setTimeout( () => {
          browser.close();
      }, TIME_IN_MS);  
         let err = url[1].split("=");
         sessionStorage.TokenId = err[1];
         var paymentData = {
          "QuotationID": this.QuotationID,
          "UID" : this.UID,
          "PID": sessionStorage.policyID, 
       };
        console.log(sessionStorage.TokenId);          
        sessionStorage.TokenId = err[1];
        console.log("Online sending Data: " + JSON.stringify(paymentData)); 
        let dataToSend = this.appService.encryptData(JSON.stringify(paymentData),sessionStorage.TokenId); 
        let request = {"request":dataToSend};
        console.log("encryptData: " + JSON.stringify(dataToSend));
        
        this.callPaymentDetailsService("/Payment.svc/GetPaymentDetails",request); 
        }else if(url[0]=this.baseUrl+"PayU/Error.aspx"){
/*           browser.close();
          console.log(type.url);
          let err = url[1].split("=");
          this.showToast(err[1]); */
          // setTimeout(function () {
          //   browser.close();
          // }, 9000);
        }else{
          // setTimeout(function () {
          //   browser.close();
          // }, 9000);
          this.showToast("Oops ! Something went wrong.");
        }
      }
    });
    browser.show();
  }

  
  /* capture image  */
  // captureImage(){
  //   this.presentLoadingDefault();
  //   this.camera.getPicture(this.options).then((imageData) => {
  //     console.log("image" + imageData);
  //   this.imageView = 'data:image/jpeg;base64,' + imageData;
  //   console.log("image" + this.imageView);
  //   this.ImageData = imageData;
  //   this.imageCaptured = false;
  //   this.loading.dismiss();
  //   this.showToast("Cheque image captured.");
  //   }, (err) => {
  //   /*  Handle error */
  //     this.loading.dismiss();
  //   });

  // }

  callPaymentFailureService(URL,serviceData){                                                                                  
    this.presentLoadingDefault();    

    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.appService.callService(URL,serviceData,headerString)
      .subscribe(Resp =>{
        this.loading.dismiss(); 
        console.log("test: " + JSON.stringify(Resp));
        if(Resp.TransactionFailureResult.ReturnCode == "0"){
                sessionStorage.TokenId = Resp.TransactionFailureResult.UserToken.TokenId;
                // this.transactionID = Resp.GetPaymentDetailsResult.Data.TransactionID;
                // this.policyNumber = Resp.GetPaymentDetailsResult.Data.PolicyNo;
                // this.bankTransactionID = Resp.GetPaymentDetailsResult.Data.BankTransactionID;
                // this.transactionAmount = this.addCommas(Resp.GetPaymentDetailsResult.Data.PremiumAmount);
                // this.transactionDate = Resp.GetPaymentDetailsResult.Data.TransactionDate;
                // this.email = Resp.GetPaymentDetailsResult.Data.Email;
                // this.mobile = Resp.GetPaymentDetailsResult.Data.Mobile;
                // this.pdfLink = Resp.GetPaymentDetailsResult.Data.PDF;
                // this.showFooter = false;
                //this.serviceCodeStatus = 0;
                this.navCtrl.push(PaymentSuccessPage,{"Result":0,"Data":Resp.TransactionFailureResult.Data, "ReferenceNo": this.TracsactionDetails.ReferenceNo});
               //this.navCtrl.push(PaymentSuccessPage,{"Result":1,"Data":Resp.TransactionFailureResult.Data});
        }else if(Resp.TransactionFailureResult.ReturnCode == "807"){
          //this.showToast(Resp.GetPaymentDetailsResult.ReturnMsg);
          sessionStorage.TokenId = Resp.TransactionFailureResult.UserToken.TokenId;
          this.message = Resp.TransactionFailureResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          this.navCtrl.push(LoginPage);
        }else if(Resp.TransactionFailureResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.TransactionFailureResult.UserToken.TokenId;
          this.message = Resp.TransactionFailureResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          this.navCtrl.push(LoginPage);
          //this.navCtrl.push(PaymentSuccessPage,{"Result":4,"Data":Resp.GetPaymentDetailsResult.Data});
        }else if(Resp.TransactionFailureResult.ReturnCode == "806"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.TransactionFailureResult.UserToken.TokenId;
          this.message = Resp.TransactionFailureResult.ReturnMsg;
          //this.serviceResponsePopup = false;
          this.serviceCodeStatus = 806;
          this.navCtrl.push(PaymentSuccessPage,{"Result":4,"Data":Resp.TransactionFailureResult.Data});
        }else{
          sessionStorage.TokenId = Resp.TransactionFailureResult.UserToken.TokenId;
          this.message = Resp.TransactionFailureResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
        }
      });

    }

  callPaymentDetailsService(URL,serviceData){                                                                                  
    this.presentLoadingDefault();    

    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.appService.callService(URL,serviceData,headerString)
      .subscribe(Resp =>{
        this.loading.dismiss(); 
        console.log("test: " + JSON.stringify(Resp));
        if(Resp.GetPaymentDetailsResult.ReturnCode == "0"){
                sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
                // this.transactionID = Resp.GetPaymentDetailsResult.Data.TransactionID;
                // this.policyNumber = Resp.GetPaymentDetailsResult.Data.PolicyNo;
                // this.bankTransactionID = Resp.GetPaymentDetailsResult.Data.BankTransactionID;
                // this.transactionAmount = this.addCommas(Resp.GetPaymentDetailsResult.Data.PremiumAmount);
                // this.transactionDate = Resp.GetPaymentDetailsResult.Data.TransactionDate;
                // this.email = Resp.GetPaymentDetailsResult.Data.Email;
                // this.mobile = Resp.GetPaymentDetailsResult.Data.Mobile;
                // this.pdfLink = Resp.GetPaymentDetailsResult.Data.PDF;
                // this.showFooter = false;
                //this.serviceCodeStatus = 0;
               this.navCtrl.push(PaymentSuccessPage,{"Result":1,"Data":Resp.GetPaymentDetailsResult.Data});
        }else if(Resp.GetPaymentDetailsResult.ReturnCode == "807"){
          //this.showToast(Resp.GetPaymentDetailsResult.ReturnMsg);
          sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
          this.navCtrl.push(LoginPage);
        }else if(Resp.GetPaymentDetailsResult.ReturnCode == "500"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
          this.navCtrl.push(LoginPage);
          //this.navCtrl.push(PaymentSuccessPage,{"Result":4,"Data":Resp.GetPaymentDetailsResult.Data});
        }else if(Resp.GetPaymentDetailsResult.ReturnCode == "806"){
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          //this.serviceResponsePopup = false;
          this.serviceCodeStatus = 806;
          this.navCtrl.push(PaymentSuccessPage,{"Result":4,"Data":Resp.GetPaymentDetailsResult.Data});
        }else{
          sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
        }
      });

    }

  captureImage(){

    let options: CameraOptions = {   
      // destinationType: this.camera.DestinationType.DATA_URL,
      // encodingType: this.camera.EncodingType.JPEG,
      // mediaType: this.camera.MediaType.PICTURE,
      // quality: 100,
      // saveToPhotoAlbum: false,
      // correctOrientation: true
      quality: 95,
      destinationType: this.camera.DestinationType.FILE_URI,      
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    this.presentLoadingDefault();
    this.camera.getPicture(options).then(imageData => {    
    this.thumbnail= imageData;       
    console.log("image: "  +this.thumbnail);
    this.getFileAsBase64(this.thumbnail);
    this.ImageData = imageData;
    this.imageCaptured = false;
    this.loading.dismiss();
    this.showToast("Cheque image is captured.");
    }, (err) => {
    /*  Handle error */
      this.loading.dismiss();
    });

  }

  getFileAsBase64(thumbnail){
    this.base64.encodeFile(thumbnail).then((base64File: string) => {
      console.log("base: " + base64File);
      this.imageView = base64File;
    }, (err) => {
      console.log(err);
    });
  }
  
  /* submit cheque  */
  submitCheque(){
    console.log("length: " + this.chequeNumber.length);
    
    if(this.chequeNumber.trim() =="" || this.chequeNumber.trim() == undefined || this.chequeNumber.trim() == null){
      this.showToast("Please enter Cheque number.");
    }else if(this.chequeNumber.length < 6){
      this.showToast("Please enter valid Cheque number.")
    }else if(this.chequeDate.trim() == "" || this.chequeDate.trim() == undefined || this.chequeDate.trim() == null){
      this.showToast("Please take cheque date.");
    }else if(this.bankName.trim() == "" || this.bankName.trim() == undefined || this.bankName.trim() == null){
      this.showToast("Please enter Bank name.");
    }else if(this.bankSelect == false){
      this.showToast("Please select bank name.");
    }else if(this.issuerName.trim() =="" || this.issuerName.trim() == undefined || this.issuerName.trim() == null){
      this.showToast("Please enter Issuer name.");
    }
    /*else if(this.thumbnail == "assets/imgs/proposer_profile_pic.svg"){
      this.showToast("Please take cheque photo.");
    }*/
    else{

      //this.presentLoadingDefault();
      let callRequest = this.getChequeRequestString();
      console.log("cheque details: "+ JSON.stringify(callRequest));
      let dataToSend = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
      //this.loading.dismiss();
      let request = {"request":dataToSend}
      console.log(JSON.stringify(request));
      this.callPaymentService("Payment.svc/PaymentRequestByCheque",request);
    }
  }
/* Payment.svc/PaymentRequestByCheque */

  /* get cheque */
  getChequeRequestString(){

    let enqReq = {
      "QuotationID": this.QuotationID,
      "UID" : this.UID,
      "PID": sessionStorage.policyID, 
      "Amount":this.TracsactionDetails.CalculatedServiceResp.PremWithServTax,
      "IssuerName":this.issuerName,
      "Bank":this.bankName,
      "Chequeno":this.chequeNumber,
      "Chequedate": this.chequeDate,/* this.chequeDate, */
      "Chequephoto":this.imageView,
      "CheckType":"L",
      "BSBCode" : this.bankId,
    }
    return enqReq;
  }
  
  /* call any service */
  callPaymentService(URL,serviceData){
    //this.presentLoadingDefault();    
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
      .subscribe(Resp =>{        
        this.loading.dismiss();
        console.log(" payment Data: "+ JSON.stringify(Resp));    
          if(Resp.PaymentRequestByChequeResult.ReturnCode == "0"){
            this.loading.dismiss();
            sessionStorage.TokenId = Resp.PaymentRequestByChequeResult.UserToken.TokenId;
           let TracsactionID = Resp.PaymentRequestByChequeResult.Data.TransactionID;
           this.navCtrl.push(PaymentSuccessPage,{"Result":2,"Data":Resp.PaymentRequestByChequeResult.Data, "QuotationID": this.QuotationID,
           "UID" : this.UID, "ReferenceNo": this.TracsactionDetails.ReferenceNo});
          }else if(Resp.PaymentRequestByChequeResult.ReturnCode == "807"){
            this.loading.dismiss();
            //this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
            sessionStorage.TokenId = Resp.PaymentRequestByChequeResult.UserToken.TokenId;
            this.message = Resp.PaymentRequestByChequeResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 807;
            //this.navCtrl.push(LoginPage);
          }else if(Resp.PaymentRequestByChequeResult.ReturnCode == "500"){
            this.loading.dismiss();
            //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
            sessionStorage.TokenId = Resp.PaymentRequestByChequeResult.UserToken.TokenId;
            this.message = Resp.PaymentRequestByChequeResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 500;
            //this.navCtrl.push(LoginPage);
          }else{
            sessionStorage.TokenId = Resp.PaymentRequestByChequeResult.UserToken.TokenId;
            this.message = Resp.PaymentRequestByChequeResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 400;
            //this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
          }
        /* else if(serviceFor == "PaymentRequest"){
          if(Resp.PaymentRequestResult.ReturnCode== "0"){
            this.openBrowser();
           }else{
             this.showToast(Resp.PaymentRequestResult.ReturnMsg);
           }
        } */


      });

  }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }  
  }

  
  // encryptCrtData(){
  //   var sendData = this.appService.encryptData(JSON.stringify(this.getCRTrequest()),sessionStorage.TokenId);
  //   this.callPaymentService("QuotePurchase.svc/CRT_Policy",{"request":sendData},"CRT_Policy");
  // }
  
  deleteImg(){
    this.imageView = '';
    this.imageCaptured = true;
  }

  /* Payment send link*/

  sendPaymentLink(){
    if(this.email.trim() == "" || this.email.trim() == undefined || this.email.trim() == null){
      this.showToast("Please enter email");
    }else if (!this.matchEmail(this.email)) {
      this.showToast("Enter valid email id");  
      //this.emailFocus.setFocus();    
    }else if (this.mobile.trim() == undefined || this.mobile.trim() == null || this.mobile.trim() == "") {      
      this.showToast("Enter mobile no"); 
      //this.mobileFocus.setFocus();                            
    }else if(this.mobile.length < 10){
      this.showToast("Enter 10 digit mobile number");   
      //this.mobileFocus.setFocus();         
    }else if(!this.isValidMobile(this.mobile)){
      this.showToast("Enter valid mobile no");
      //this.mobileFocus.setFocus();
    }else{      
      var requestData = {
        "QuotationID": this.TracsactionDetails.QuotationID,
        "UID": this.TracsactionDetails.UID,
        "PID": sessionStorage.policyID,
        // "email": this.email,
        // "mobile": this.mobile
      };
      this.encryptPaymentLinkData(requestData);
      console.log("PAymentLink: " + JSON.stringify(requestData));
      
    }
  }

  encryptPaymentLinkData(data){
    var sendData = this.appService.encryptData(JSON.stringify(data),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.getPaymentLinkResponseData({'request': sendData});
  }

  getPaymentLinkResponseData(serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    console.log("header" + JSON.stringify(headerString)); 
    //this.presentLoadingDefault();
    this.appService.callService("Payment.svc/PaymentRequestByLink",serviceData, headerString)
    .subscribe(PaymentRequestLink =>{
    //this.loading.dismiss();
    console.log("PaymentLinkResponse: " + JSON.stringify(PaymentRequestLink.PaymentRequestByLinkResult));
    if(PaymentRequestLink && PaymentRequestLink.PaymentRequestByLinkResult.ReturnCode == "0"){  
      sessionStorage.TokenId = PaymentRequestLink.PaymentRequestByLinkResult.UserToken.TokenId;
      this.email = "";
      this.mobile = "";
      this.showToast(PaymentRequestLink.PaymentRequestByLinkResult.ReturnMsg);
      this.navCtrl.push(PaymentSuccessPage,{"Result":3,"Data":PaymentRequestLink.PaymentRequestByLinkResult.Data, "QuotationID": this.QuotationID,
      "UID" : this.UID, "ReferenceNo": this.TracsactionDetails.ReferenceNo});
    }else if(PaymentRequestLink.PaymentRequestByLinkResult.ReturnCode == "807"){
      //this.showToast(PaymentRequestLink.PaymentRequestByLinkResult.ReturnMsg);
      sessionStorage.TokenId = PaymentRequestLink.PaymentRequestByLinkResult.UserToken.TokenId;
      this.message = PaymentRequestLink.PaymentRequestByLinkResult.ReturnMsg;
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 807;
      //this.navCtrl.push(LoginPage);
    }else if(PaymentRequestLink.PaymentRequestByLinkResult.ReturnCode == "500"){
      //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
      sessionStorage.TokenId = PaymentRequestLink.PaymentRequestByLinkResult.UserToken.TokenId;
      this.message = PaymentRequestLink.PaymentRequestByLinkResult.ReturnMsg;
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 500;
      //this.navCtrl.push(LoginPage);
    }else{
      sessionStorage.TokenId = PaymentRequestLink.PaymentRequestByLinkResult.UserToken.TokenId;
      this.message = PaymentRequestLink.PaymentRequestByLinkResult.ReturnMsg;
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 400;
      //this.showToast(PaymentRequestLink.PaymentRequestByLinkResult.ReturnMsg); 
    }
    }, (err) => {
    console.log(err);
    });
  }

  matchEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  isValidMobile(mobile){
    var regex = /^[6-9][0-9]{9}$/;
    return regex.test(mobile);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }

  onCountryTextChange(ev: any) {
    console.log("" + ev)
    
    this.bankSelect = false;
    let val = ev.target.value;
    if (val && val.length >= 3 ) {
      console.log("text" + val);
      
    this.countryFocus = true;
    this.bank_list = this.bank_listcopy.filter((data) => {
    return data.BankName.toLowerCase().indexOf(val.toLowerCase()) > -1;
    });
    } else {
    this.bank_list = this.bank_listcopy;
    this.countryFocus = false;
    }
    }
    onBankSelection(listitem: any) {
    this.bankSelect = true;
    this.bankName = listitem.BankName;
    this.bankId = listitem.BankKey;
    this.countryFocus = false;
    //this.getCity(this.countryid);
    }


}
