import { Component, NgModule} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocationmapPage } from '../locationmap/locationmap';
import { Http } from "@angular/http";
import { NativeGeocoder,NativeGeocoderReverseResult,NativeGeocoderForwardResult }  from '@ionic-native/native-geocoder';


/**
 * Generated class for the HospitalSearchResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    HospitalSearchResultPage,
  ],
  imports: [
    //IonicPageModule.forChild(LandingScreenPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-hospital-search-result',
  templateUrl: 'hospital-search-result.html',
})


export class HospitalSearchResultPage {

  hospitalData;
  hospitalDataList;
  geocoded : boolean;
  results : string;
  lat;
  lang;
  hideshowDiv = true;
  divShow;
  pageName;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _GEOCODE  : NativeGeocoder) {
    this.hospitalData = navParams.get("hospitalData");
    
    
    this.hospitalDataList = this.hospitalData.clists;
    console.log("hospitalData: " + JSON.stringify(this.hospitalDataList));
  }

  showLocationInMap(hospitalDetails){   
    this.getLangLatFromAddress(hospitalDetails.sAddress);   
  }

  getLangLatFromAddress(address){

   return this._GEOCODE.forwardGeocode(address)
  .then((coordinates: NativeGeocoderForwardResult[]) =>
     this.navCtrl.push(LocationmapPage, {"lat": coordinates[0].latitude, "lang": coordinates[0].longitude})
  .catch((error: any) => console.log(error)));
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HospitalSearchResultPage'); 
    this.pageName = this.navParams.get("page");
    console.log("page:" + this.pageName);
    if(this.hospitalDataList == ""){
      console.log("IF");
      this.hideshowDiv = false;
    } 

    if(this.pageName == "hospital"){
      this.divShow = false;
    }else{
      this.divShow =true;
    }
  }

}
