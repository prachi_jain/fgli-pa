import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaymentMethodPage } from '../payment-method/payment-method';


/**
 * Generated class for the HealthPolicyReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    HealthPolicyReviewPage,
  ],
  imports: [
    //IonicPageModule.forChild(HealthPolicyReviewPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-health-policy-review',
  templateUrl: 'health-policy-review.html',
})
export class HealthPolicyReviewPage {

  memberDetails;
  selectedCardArray;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.memberDetails = navParams.get("memberDetails");
    this.selectedCardArray = navParams.get("planCardDetails");
  }

  ionViewDidLoad() {
    document.getElementById("splash").style.display='none';
    console.log('ionViewDidLoad HealthPolicyReviewPage');
  }

  proceedMemberDetailsPage(){
    this.navCtrl.push(PaymentMethodPage, {"memberDetails":this.memberDetails,  "planCardDetails": this.selectedCardArray});
  }

}
