import { Component, NgModule } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { VerifyDetailsPage } from '../verify-details/verify-details';
import { BuyHealthDeclarationPage } from '../buy-health-declaration/buy-health-declaration';
import * as $ from 'jquery';
import { BuyPage } from '../buy/buy';
import { AppService } from '../../../providers/app-service/app-service';
import { BuyPageResultPage } from '../buy-page-result/buy-page-result';
import { LandingScreenPage } from '../landing-screen/landing-screen';
import { LoginPage } from '../login/login';

/**
 * Generated class for the BuyPolicyReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    BuyPolicyReviewPage,
  ],
  imports: [
    //IonicPageModule.forChild(BuyPolicyReviewPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-buy-policy-review',
  templateUrl: 'buy-policy-review.html',
})
export class BuyPolicyReviewPage {

  pet = "Policy";
  policyDetails:boolean;
  contactDetails:boolean;
  insuredDetails:boolean;
  memberDetails;
  selectedCardArray;
  policyType;
  policyTerm;
  premium;
  installment;
  totalSI = 0;
  email;
  mobile;
  address;
  nameOfProposer;
  smokingText;
  displayPolicyDetails:any;
  memberlist = [];
  sumOfSI;
  resultData;
  titleProposer;
  panShow = true;
  ReferenceNo;
  hidebutton = true;
  ClientDetails: any = [];
  loading;
  isDisabledContact = true;
  clickSave = false;
  clickEdit = false;
  editBrd;
  isDisabledInsured = true;
  saveBtnHideShowContact = true;
  saveBtnHideShowInsured = true;
  saveandEmailHidShow = true;
  pID;
  thankyouPopup=true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  disabledClick = false;


  constructor(private platform: Platform, public navCtrl: NavController, public navParams: NavParams, 
    public appService: AppService, private loadingCtrl : LoadingController , public toast: ToastController) {
    this.memberDetails = navParams.get("ProposerDetails");
    this.selectedCardArray = navParams.get("planCardDetails");
    console.log("memberDetailsReview: " + JSON.stringify(this.memberDetails));
    
    //this.memberDetails =  [{"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","maritalCode":"M","age":"01-01-1985","ageText":"33 years","insuredCode":"si001","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"111.76","HeightText":"3' 8\"","Weight":"5","ProposerDetailsDivShow":true,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"Vuf","NomineeRelation":"Son","NomineeAge":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"occupation":"Professional","proposerName":"Sknb","Gender":"M","genderText":"Male","address":"Guug","pincode":"400008","state":"Maharashtra","city":"Mumbai","proposerEmail":"skd@gmail.com","mobile":"8552688668","pan":"BNJVH5885H","occupationCode":"PRFS","nomineeAge":"09-07-1886"},{"code":"SPOU","title":"Spouse","smoking":false,"smokingText":"","popupType":"1","showMarried":false,"maritalStatus":"","maritalCode":"S","age":"01-01-1985","ageText":"33 years","insuredCode":"si001","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"114.30","HeightText":"3' 9\"","Weight":"9","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"Vjjg","NomineeRelation":"Daughter","NomineeAge":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true,"proposerName":"Ch","occupationCode":"BUSM","occupation":"Businessman","nomineeAge":"01-08-1589"}];
    //this.selectedCardArray = {"DurationYears":"3","Installment":"full","CalculatedServiceResp":{"BasePremium":"7322.2","DeductDisc":"0","DeductDiscRate":"0","EmpDisc":"0","EmpDiscRate":"0","ErrorMsg":"","FmlyDiscRate":"0","InstallLoad":"0","InstallLoadRate":0,"LngTrmDisc":"2196.66","LngTrmDiscRate":"10","OnlineDisc":"0","OnlineDiscRate":"0","PremWithLoad":"19769.94","PremWithServTax":"23329","PremWithoutServTax":"19769.94","PremiumAmt":"19769.94","ServiceTax":"3558.59","ServiceTaxRate":"18","TermPremium":"21966.6","TtlInstallment":"1"},"Request":{"BeneficiaryDetails":{"Member":[{"Age":"33","AnnualIncome":"0","AppointeeName":"","AptRelWithNominee":"","BeneBasePremium":"4724","CoverType":"VITAL","DeductibleDiscount":"0","DeductibleDiscountRate":"0","FloaterDiscRate":"0","Height":"111","InsuredDob":"01/01/1985 00:00:00","InsuredGender":"","InsuredName":"","InsuredOccpn":"","IsOnline":"False","MedicalLoading":"0","MemberId":"1","NomineeAge":"0","NomineeName":"","NomineeReln":"","PerPrsnPremium":"4724","PreExstDisease":"N","Relation":"SELF","SumInsured":"300000","Weight":"5"},{"Age":"33","AnnualIncome":"0","AppointeeName":"","AptRelWithNominee":"","BeneBasePremium":"4724","CoverType":"VITAL","DeductibleDiscount":"0","DeductibleDiscountRate":"0","FloaterDiscRate":"45","Height":"114","InsuredDob":"01/01/198500:00:00","InsuredGender":"","InsuredName":"","InsuredOccpn":"","IsOnline":"False","MedicalLoading":"0","MemberId":"2","NomineeAge":"0","NomineeName":"","NomineeReln":"","PerPrsnPremium":"2598.2","PreExstDisease":"N","Relation":"SPOU","SumInsured":"300000","Weight":"9"}]},"Duration":"3","Installments":"FULL","IsFgEmployee":"False","PolicyType":"HTF"},"UID":"43079","HideOneYear":false,"showPrice":"23,329","oldPrice":"25,525.66","selected":true,"disctAmt":"2,196.66","hideInstalment":false,"hideDiscountDiv":false,"disPersec":10,"sumInsured":"300000","sumInsuredTitle":"3L","IndexID":0,"clickColorChange":false,"savings":"2197","ReferenceNo":"HTOAP006080","QuotationID":"addebd93-2de3-43e2-9576-e32d28f389a6"};
    console.log("back State: " + sessionStorage.isViewPopped);
    if(sessionStorage.isViewPopped == "true"){
      this.memberDetails = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      sessionStorage.isViewPopped = "";
    }
    
    for(let i=0; i< this.memberDetails.length; i++){
      this.memberDetails[i].isDisabled = true;
    }
    // platform.registerBackButtonAction(() => {
    //   this.onBackClick();
    // },1);
  }

  ionViewDidLoad() {
    document.getElementById('splash').style.display = 'none';
    this.loadMembersData();
    this.ReferenceNo = this.selectedCardArray.ReferenceNo;


    if(sessionStorage.isViewPopped == true){
      this.memberDetails = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      sessionStorage.isViewPopped = undefined;
      }
  }

  loadMembersData(){
    this.PolicyDetails();

  }

  PolicyDetails(){
    if(this.clickEdit == false && this.clickSave == false){
      this.memberlist = [];
      this.hidebutton = true;
      this.totalSI = 0;
      this.policyDetails = false;
      this.contactDetails = false;
      this.insuredDetails = false;
      if(this.memberDetails[0].PolicyType == "HTI"){
        this.policyType = "Individual";
        this.policyTerm = this.selectedCardArray.ptTitle;
        this.premium = this.addCommas(this.selectedCardArray.discountAmount);
        this.installment = this.selectedCardArray.Installment;
        this.displayPolicyDetails = this.selectedCardArray.Request.BeneficiaryDetails.Member;
        console.log("MemberData:" + JSON.stringify(this.displayPolicyDetails));
        if(this.displayPolicyDetails.length > 1){
          for(let f=0; f<this.displayPolicyDetails.length; f++){
            this.sumOfSI = parseInt(this.displayPolicyDetails[f].SumInsured);
            this.totalSI +=  this.sumOfSI;
          }
        }else{
          this.totalSI = this.addCommas(this.displayPolicyDetails.SumInsured);
        }
        for(let i=0; i< this.memberDetails.length; i++){
          this.memberlist.push({name: this.memberDetails[i].title});
        }

      }else{
        this.displayPolicyDetails = this.selectedCardArray.Request.BeneficiaryDetails.Member;
        this.policyType = "Family Floater"
        this.policyTerm = this.selectedCardArray.DurationYears;
        this.premium = this.selectedCardArray.showPrice;
        this.installment = this.selectedCardArray.Installment;
        this.totalSI = this.addCommas(this.displayPolicyDetails[0].SumInsured);
        for(let i=0; i< this.memberDetails.length; i++){
          this.memberlist.push({name: this.memberDetails[i].title});
        }
        console.log('Latest:' + JSON.stringify(this.memberlist));
      }
    }else{
      this.pet = "Contact";
      this.showToast("Please save your changes first");
    }   
     
  }

  ContactDetails(){
    if(this.clickEdit == false && this.clickSave == false){
      this.hidebutton = true;
      this.policyDetails = true;
      this.contactDetails = true;
      this.insuredDetails = false;
    }else{
      this.showToast("Please save your changes first");
    }
  }

  InsuredDetails(){
    if(this.disabledClick == true){
      this.disabledClick == true;
    }else{
      if(this.clickEdit == false && this.clickSave == false){
        this.hidebutton = false;
        this.policyDetails = true;
        this.contactDetails = false;
        this.insuredDetails = true;
        for(let i=0;i<this.memberDetails.length;i++){
          this.nameOfProposer = this.memberDetails[i].nameOfProposer;
          if(this.memberDetails[i].smoking == true){
            this.memberDetails[i].smokingText = "Smoker";
          }else{
            this.memberDetails[i].smokingText = "Non-Smoker";
          }
  
          if(this.memberDetails[i].title == "Self"){
            this.memberDetails[0].titleProposer = "Proposer";
          }else{
            this.memberDetails[i].titleProposer = this.memberDetails[i].title;
          }
  
  
            if(this.memberDetails[i].title == "Spouse" && this.memberDetails[0].Gender == "M"){
              this.memberDetails[i].genderText = "Female";
            }else if(this.memberDetails[i].title == "Spouse" && this.memberDetails[0].Gender == "F"){
              this.memberDetails[i].genderText = "Male";
            }else if(this.memberDetails[i].title == "Daughter" || this.memberDetails[i].title == "Daughter1" || this.memberDetails[i].title == "Daughter2" || this.memberDetails[i].title == "Daughter3" || this.memberDetails[i].title == "Daughter4" || this.memberDetails[i].title == "Mother") {
              this.memberDetails[i].genderText = "Female";
            }else if(this.memberDetails[i].title == "Son" || this.memberDetails[i].title == "Son1" || this.memberDetails[i].title == "Son2" || this.memberDetails[i].title == "Son3" || this.memberDetails[i].title == "Son4" || this.memberDetails[i].title == "Father") {
              this.memberDetails[i].genderText = "Male";
            }
        }
      }else{    
        this.showToast("Please save your changes first");
      }
    }
  
  }

  proceedNextTab(){
    console.log("edit: " + this.clickEdit);
    console.log("edit: " + this.clickSave);
    
    if(this.clickEdit == false && this.clickSave == false){
      if(this.pet == "Policy"){
        this.pet = "Contact";
        this.ContactDetails();
      }else  if(this.pet == "Contact"){
        this.pet = "Insured";
        this.InsuredDetails();
        this.saveandEmailHidShow = false;
        this.hidebutton = false;
      }
    }else{
      this.showToast("Please save your changes first");
    }   
  }


  proceedVerifyPage(){
    if(this.clickEdit == false && this.clickSave == false){
      var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
      this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
    }else{
      this.showToast("Please save your changes first");
    }   
  }


  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          // this.pID = Resp.Proceed_PolicyResult.Data.pID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.navCtrl.push(BuyHealthDeclarationPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 807;
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          //this.navCtrl.push(LoginPage);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 500;
        }else{
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.loading.dismiss();
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": this.selectedCardArray.QuotationID,
      "UID": this.selectedCardArray.UID,
      "stage":"4",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.selectedCardArray.Request.PolicyType ,//"HTF",
        "Duration": this.selectedCardArray.Request.Duration,
        "Installments": this.selectedCardArray.Request.Installments,
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails":this.selectedCardArray.Request.BeneficiaryDetails.Member.length == undefined ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberDetails,
        "CardDetails": this.selectedCardArray
      }
    }
    console.log("BuyPageProceedData: " + JSON.stringify(sendRequest));
    return sendRequest;
  }
  
  getClientNode(){
    let age = this.memberDetails[0].age;
    let clientSalution = "MR";
    if(this.memberDetails[0].Gender == "F"){
      clientSalution = "MRS";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberDetails[0].proposerName,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberDetails[0].Gender,
      "MaritalStatus": this.memberDetails[0].maritalCode,
      "Occupation": this.memberDetails[0].occupationCode,
      "PANNo": this.memberDetails[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberDetails[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode":this.memberDetails[0].pincode,
        "City": this.memberDetails[0].city,
        "State": this.memberDetails[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": "",
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  
  }

  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.Request.BeneficiaryDetails.Member.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberDetails[0].Gender;
    let numericAge = this.getAge(this.memberDetails[0].nomineeAge).toString();
    
    BeneDetails.push({
      "MemberId": cardDetails.Request.BeneficiaryDetails.Member.MemberId ,
      "InsuredName": this.memberDetails[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberDetails[0].occupationCode,
      "CoverType": cardDetails.Request.BeneficiaryDetails.Member.CoverType,
      "SumInsured": cardDetails.Request.BeneficiaryDetails.Member.SumInsured,
      "DeductibleDiscount": cardDetails.Request.BeneficiaryDetails.Member.DeductibleDiscount,
      "Relation": cardDetails.Request.BeneficiaryDetails.Member.Relation,
      "NomineeName": this.memberDetails[0].nominee,
      "NomineeRelation": this.memberDetails[0].NomineeRelation,
      "AnualIncome": "",
      "Height": cardDetails.Request.BeneficiaryDetails.Member.Height,
      "Weight": cardDetails.Request.BeneficiaryDetails.Member.Weight,
      "NomineeAge": numericAge,
      "AppointeeName": this.memberDetails[0].appointeeName,
      "AptRelWithominee": this.memberDetails[0].appointeeRelation,
      "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member.MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      }
    })
    return BeneDetails;
  }
  
  getBeneficiaryDetails(cardDetails){
    var content = document.querySelector('div .nomineePage').children;

    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.Request.BeneficiaryDetails.Member.length ; i++){
      for(let j = 0; j<this.memberDetails.length;j++){
        if(i==j){
          if( (this.memberDetails[j].code).toUpperCase()== (cardDetails.Request.BeneficiaryDetails.Member[i].Relation).toUpperCase() ){

            // if(this.memberDetails[j].code == "SELF"){
            //   let numericAge = this.memberDetails[j].ageText.split("");
        
        
            //   let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
            //   let gendr = this.memberDetails[j].Gender;
            //   BeneDetails.push({
            //     "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
            //     "InsuredName": this.memberDetails[j].proposerName,
            //     "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
            //     "InsuredGender": gendr,
            //     "InsuredOccpn": this.memberDetails[j].occupationCode,
            //     "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
            //     "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
            //     "DeductibleDiscount": "0",
            //     "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
            //     "NomineeName": this.memberDetails[j].nominee,
            //     "RelationName":this.memberDetails[i].title,
            //     "NomineeRelation":this.memberDetails[j].NomineeRelation,
            //     "AnualIncome": "",
            //     "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
            //     "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
            //     "NomineeAge": numericAge[0],
            //     "AppointeeName": this.memberDetails[j].appointeeName,
            //     "AptRelWithominee": this.memberDetails[j].AptRelWithominee,
            //     "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
            //     "PreExstDisease": "N",
            //     "DiseaseMedicalHistoryList": {
            //       "DiseaseMedicalHistory": {
            //         "PreExistingDiseaseCode": "",
            //         "MedicalHistoryDetail": ""
            //       }
            //     }
            //   })
            // }else{
            //   let numericAge = this.memberDetails[j].ageText.split("");
        
        
            //   let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
            //   let gendr = this.memberDetails[j].Gender;
            //   BeneDetails.push({
            //     "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
            //     "InsuredName": this.memberDetails[j].proposerName,
            //     "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
            //     "InsuredGender": gendr,
            //     "InsuredOccpn": this.memberDetails[j].occupationCode,
            //     "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
            //     "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
            //     "DeductibleDiscount": "0",
            //     "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
            //     "NomineeName": this.memberDetails[j].nominee,
            //     "RelationName":this.memberDetails[i].title,
            //     "NomineeRelation":this.memberDetails[j].NomineeRelation,
            //     "AnualIncome": "",
            //     "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
            //     "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
            //     "NomineeAge": numericAge[0],
            //     "AppointeeName": this.memberDetails[j].appointeeName,
            //     "AptRelWithominee": this.memberDetails[j].AptRelWithominee,
            //     "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
            //     "PreExstDisease": "N",
            //     "DiseaseMedicalHistoryList": {
            //       "DiseaseMedicalHistory": {
            //         "PreExistingDiseaseCode": "",
            //         "MedicalHistoryDetail": ""
            //       }
            //     }
            //   })
            // }
            //let numericAge = this.memberDetails[j].ageText.split(" ");
            let numericAge = this.getAge(this.memberDetails[j].nomineeAge).toString();
        
        
            let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ")[0].split("/");
            let gendr = this.memberDetails[j].Gender;
            BeneDetails.push({
              "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
              "InsuredName": this.memberDetails[j].proposerName,
              "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
              "InsuredGender": gendr,
              "InsuredOccpn": this.memberDetails[j].occupationCode,
              "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
              "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
              "DeductibleDiscount": "0",
              "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
              "NomineeName": this.memberDetails[j].nominee,
              "RelationName":this.memberDetails[i].title,
              "NomineeRelation":this.memberDetails[j].NomineeRelation,
              "AnualIncome": "",
              "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
              "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
              "NomineeAge": numericAge,
              "AppointeeName": this.memberDetails[j].appointeeName,
              "AptRelWithominee": this.memberDetails[j].AptRelWithominee,
              "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
              "PreExstDisease": "N",
              "DiseaseMedicalHistoryList": {
                "DiseaseMedicalHistory": {
                  "PreExistingDiseaseCode": "",
                  "MedicalHistoryDetail": ""
                }
              }
            })
          }
        }
 
  
      }
  
    }
    return BeneDetails;
  
  } 

  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365);
  }

  showToast(Message) {
    let pageToast = this.toast.create({
      message: Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 2500,
    });
    pageToast.present();
  }

  onBackClick(){
    sessionStorage.isViewPopped = "true";
    sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
    this.navCtrl.pop();
  }

  /*Sharvari Edit Functionality */
onTabChange(event){
  switch(event.value){
    case 'Policy':
      break;
    case 'Contact':
      break;
    case 'Insured':
      break;
  }
}
onEditClick(event, index, member){
  // for(let i=0; i< this.memberDetails.length; i++){
  //     this.memberDetails[i].isDisabled = index == i ? !this.memberDetails[i].isDisabled : true;
      
  // }
  this.clickEdit = !this.clickEdit;
  member.saveBtnHideShowInsured = !member.saveBtnHideShowInsured;
  this.isDisabledInsured = !this.isDisabledInsured;
  member.editBrd = !member.editBrd;
}

onEditContactDetails(){
  this.disabledClick = true;
  this.clickEdit = !this.clickEdit;
  this.isDisabledContact = !this.isDisabledContact;
  this.editBrd = !this.editBrd;
  this.saveBtnHideShowContact = !this.saveBtnHideShowContact;
}

saveContactData(){
  if(this.memberDetails[0].proposerEmail == ""){
    this.showToast("Please enter proposer email");
  }else if (!this.matchEmail(this.memberDetails[0].proposerEmail)) {
    this.showToast("Please enter valid email id");      
  }else if (this.memberDetails[0].mobile == undefined || this.memberDetails[0].mobile == null || this.memberDetails[0].mobile == "") {      
          this.showToast("Please enter mobile no");                             
  }else if(this.memberDetails[0].mobile.length < 10){
          this.showToast("Please enter 10 digit mobile number");            
  }else if(!this.isValidMobile(this.memberDetails[0].mobile)){
    this.showToast("Please enter valid mobile no");
    //this.mobileFocus.setFocus();
  }else if(this.memberDetails[0].address == ""){
    this.showToast("Please enter your address");
  }else{
    this.disabledClick = false;
    sessionStorage.mobile = this.memberDetails[0].mobile;
    sessionStorage.email = this.memberDetails[0].proposerEmail;
    this.saveBtnHideShowContact = true;
    this.clickSave = false;
    this.clickEdit = false;
    this.isDisabledContact = !this.isDisabledContact;
    this.editBrd = !this.editBrd;
  }  
}

matchEmail(email){
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

isValidMobile(mobile){
  var regex = /^[6-9][0-9]{9}$/;
  return regex.test(mobile);
}

saveInsuredDetails(member){
//   for(let i=0; i< this.memberDetails.length; i++){
//     this.memberDetails[i].isDisabled = index == i ? !this.memberDetails[i].isDisabled : true;
// }
if(member.title == "Self"){ 
  if (member.pan == undefined || member.pan == null || member.pan == "") { 
      this.showToast("Please enter your pan");   
  } else if (!this.matchPan(member.pan)) {
      this.showToast("Please enter valid pan");   
  } else if(member.nominee == undefined || member.nominee == null || member.nominee == ""){
    this.showToast("Please enter nominee name");
  } else if(member.NomineeRelation == undefined || member.NomineeRelation == null || member.NomineeRelation == ""){
    this.showToast("Please enter nominee relation");
  }else{
    member.saveBtnHideShowInsured = !member.saveBtnHideShowInsured;
    member.editBrd = !member.editBrd;
    this.isDisabledInsured = !this.isDisabledInsured;
    this.clickSave = false;
    this.clickEdit = false;
  }
}else{
  if(member.nominee == undefined || member.nominee == null || member.nominee == ""){
    this.showToast("Please enter nominee name");
  } else if(member.NomineeRelation == undefined || member.NomineeRelation == null || member.NomineeRelation == ""){
    this.showToast("Please enter nominee relation");
  }else{
    this.clickSave = false;
    this.clickEdit = false;
    member.saveBtnHideShowInsured = !member.saveBtnHideShowInsured;
    member.editBrd = !member.editBrd;
    this.isDisabledInsured = true;
  }
 }
}

matchPan(pan){
  var regex = /[A-Za-z]{5}\d{4}[A-Za-z]{1}/;
  return regex.test(pan);
}

addCommas(x) {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  console.log("test: " + parts);
  return parts.join(".");   
}

onSaveAndEmail(){
  if(this.clickEdit == false && this.clickSave == false){
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }else{
    this.showToast("Please save your changes first");
  }   
}

// saveEmailData(){      
//   var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
//   console.log("testt: " + JSON.stringify(sendData));
//   this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
// }

sendStage1ForEProposalData(URL, serviceData){
  let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
  this.presentLoadingDefault();
  this.appService.callService(URL,serviceData,headerString)
  .subscribe(Resp =>{
    this.loading.dismiss();
    console.log("Stage 1: "+ JSON.stringify(Resp)); 
      if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
        this.pID = Resp.Proceed_PolicyResult.Data.pID;
        let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
        let UID = Resp.Proceed_PolicyResult.Data.UID;
        //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        this.callEProposalService();        
      }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
        //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        //this.navCtrl.push(LoginPage);
        this.message = Resp.Proceed_PolicyResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 807;
      }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
        //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;    
        //this.navCtrl.push(LoginPage);
        this.message = Resp.Proceed_PolicyResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 500;
      }else{
        this.loading.dismiss();
        sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
        //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        this.message = Resp.Proceed_PolicyResult.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 400;
      }
  });
}

callEProposalService(){   
  var eProposalData = {
        "QuotationID": this.selectedCardArray.QuotationID,
        "UID": this.selectedCardArray.UID
      };
      console.log("Raw: " + JSON.stringify(eProposalData));
      var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 

      this.sendEProposerData({'request': sendData});
}

sendEProposerData(serviceData){
  let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
  this.presentLoadingDefault();
    this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
    .subscribe(response =>{
      this.loading.dismiss();
      var resp = response.EproposalResult;
      console.log(JSON.stringify(response));
      if(resp && resp.ReturnCode == "0"){
        sessionStorage.TokenId = resp.UserToken.TokenId;
        this.thankyouPopup = false;
        //this.closeModalPop();
        //this.showToast("Successfully send E-Proposal data.")
      }else if(resp.ReturnCode == "807"){
        //this.showToast(resp.ReturnMsg);
        sessionStorage.TokenId =resp.UserToken.TokenId;
        //this.navCtrl.push(LoginPage);
        this.message = resp.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 807;
      }else if(resp.ReturnCode == "500"){
        //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
        sessionStorage.TokenId = resp.UserToken.TokenId;
        //this.navCtrl.push(LoginPage);
        this.message = resp.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 500;
      }else{
        sessionStorage.TokenId = resp.UserToken.TokenId;
        //this.showToast(resp.ReturnMsg); 
        this.message = resp.ReturnMsg;
        this.serviceResponsePopup = false;
        this.serviceCodeStatus = 400;
      }        
     
    }, (err) => {
    console.log(err);
    });
}

OkButton(){
  this.thankyouPopup = true;
  this.navCtrl.push(LandingScreenPage);
}

// onPolicyEditClick(){
//   sessionStorage.isViewPopped = true;
//   sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
//   sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
//   this.navCtrl.push(BuyPageResultPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});
// }

serviceResponseOkButton(){
  if(this.serviceCodeStatus == 807){
    this.serviceResponsePopup = true;
    this.navCtrl.push(LoginPage);   
  }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
    this.serviceResponsePopup = true;     
  }

}

}
