import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentpendingPage } from './paymentpending';

@NgModule({
  declarations: [
    PaymentpendingPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentpendingPage),
  ],
})
export class PaymentpendingPageModule {}
