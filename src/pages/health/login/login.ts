import { Component,NgModule,ElementRef, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController,MenuController   } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { DashboardPage } from '../dashboard/dashboard';
import {Network} from '@ionic-native/network';
import { Keyboard } from '@ionic-native/keyboard';
import { QuickQuotePage } from '../quick-quote/quick-quote';
import { BuyPage } from '../buy/buy';
import { LandingScreenPage } from '../landing-screen/landing-screen';
import { Events } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { GenOtpPage } from '../../gen-otp/gen-otp';
import { Market } from '@ionic-native/market';
import { VecproposerdetailsPage } from '../../vector/vecproposerdetails/vecproposerdetails';
import { VecquickquotePage } from '../../vector/vecquickquote/vecquickquote';
import { ProductLandingPage } from '../../product-landing/product-landing';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cryptojs: any;
@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    //IonicPageModule.forChild(LoginPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  userType = "Partners";
  employeeID;
  password;
  keepSignedIn = false;
  passwordType = 'password';
  showPass = false;
  loginData;
  canShowToast = true;
  picToView:string="assets/imgs/password_icon-01.svg";
  @ViewChild("employeeIdInput") userInput ;
  @ViewChild("passwordInput") passwordInput ;
  @ViewChild("toggleInput") toggleInput ;
  preLogin : false;
  preLoginQuickQuoteData;
  loading;
  savedUserData;
  showLabel = true;
  floatingSpace = true;
  AgentCodePopup = true;
  agentCode;
  loginWithAgentData;
  agentID;
  versionData;
  //live
  appVersion = "1.2.3";
  //appVersion = "1.2.2";
  forceUpdatePopup = true;
  pageName;

  public static readonly pageName = 'LoginPage';
  public data;


  constructor(public events: Events, private market: Market, private menuCtrl: MenuController, private iab: InAppBrowser, private keyboard: Keyboard,public navCtrl: NavController,public loadingCtrl: LoadingController,public appService: AppService, public navParams: NavParams,public toast: ToastController,private network: Network) {
    /* disable swipe menu for this page */
    menuCtrl.swipeEnable(false);
    this.preLogin = navParams.get("preLoginQuickQuote");
    this.preLoginQuickQuoteData = navParams.get("preLoginQuickQuoteData");    
    this.versionData = navParams.get("versionControl");
    console.log("version:" + JSON.stringify(this.versionData));
    
   /*  console.log(this.preLogin);
    console.log(this.preLoginQuickQuoteData);
    console.log(appService.getBase64string("876E6494-4C4E-4B1B-96E1-2F4F072D0877~ca014ca4-6363-44c0-95a9-fc6c4a71caad")); */
  }

  createUser(user, status) {
    this.events.publish('user:created', user, true, Date.now(), status);
  }
  

  ionViewDidLoad() {
    setTimeout(function() {         
          document.getElementById("splash").style.display='none';                 
      }, 7000);
      //this.appService.logButton('loginPage',{ pram: "paramValue" })
    /* redirect to DashboardPage if successful login in with remember me */
    if(localStorage.userData){
     // this.navCtrl.push(LandingScreenPage,{});
      this.savedUserData = JSON.parse(localStorage.userData);
      this.userType = this.savedUserData.userTypeName;
      this.employeeID = this.savedUserData.username;
      this.password = this.savedUserData.password;
      this.keepSignedIn = true;
      this.checkVersion();
    }

    this.checkVersion();
  }

  checkVersion(){
    if(this.versionData.version == this.appVersion){
      console.log("IF");
      
      this.showToast("your app is up to date");
    }else{
      console.log("ELSE");
      //this.appVersion = this.versionData.version;
      this.forceUpdatePopup = false;
      //this.showToast("need to force update");
    }
  }

  okButton(){
    if(this.versionData.playStoreLink != "" || this.versionData.playStoreLink == null){
      //let browser = this.iab.create(this.versionData.playStoreLink,'_blank', 'location=no');     
      this.market.open('com.fg.health');
      //this.forceUpdatePopup = true;
    }
    //location.href
    
  }

/* loading function */
  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss : false,
      dismissOnPageChange : true
    });
    this.loading.present();
  }

/* login button click function */
  loginBtn(){
    /* check internet connection */
    if(this.network.type != "none"){
      if(this.employeeID == undefined || this.employeeID == null || this.employeeID == ''){
        if(this.userType == "Partners"){
           this.showToast("Please enter User ID.");
        }else{
          this.showToast("Please enter User ID.");
        }
      }else if (this.password == undefined || this.password == null || this.password == ''){
        this.showToast("Please enter password.");
      }else{
        if(this.userType == "Employees"){
          this.loginData = {'username':this.employeeID,'password':this.password,'appID':localStorage.AppID,'usertype':'1','userTypeName':this.userType}
        }else if(this.userType == "Partners"){
          //this.AgentCodePopup = false;
          //this.loginData = {'username':this.employeeID,'password':this.password,'appID':localStorage.AppID,'usertype':'2','userTypeName':this.userType, 'agentCode': this.agentCode}
          this.loginData = {'username':this.employeeID,'password':this.password,'appID':localStorage.AppID,'usertype':'2','userTypeName':this.userType}
        }

        console.log("login: " + JSON.stringify(this.loginData));
        
        var sendData = this.appService.encryptData(JSON.stringify(this.loginData),'');
        this.loginService({'request': sendData});
      }
    }else{
      /* NO internet access */
      this.showToast("You need internet access to proceed.");
    }
  }



  /* Toast to show messages */
  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'OK',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
   

  }

 
  /* call login service */
  loginService(serviceData){
    console.log("sendData: " + JSON.stringify(serviceData));
    
    //this.presentLoadingDefault();
    this.appService.presentServiceLoading();
    this.appService.callService("Authenticate.svc/Login",serviceData,'')
      .subscribe(Login =>{
       // this.loading.dismiss();
       console.log("PreLoginData :" + JSON.stringify(Login));
       
       this.appService.showloading.dismiss();
        // if(Login.LoginResult.ReturnCode == "0"){
        //   /* login success */
        //   // this.AgentCodePopup = false;
        //   this.loginData.keepLogin = this.keepSignedIn;
        //   this.loginData.LoginStatus = true;
        //   this.agentID = Login.LoginResult.Data.User.UserId;
        //   sessionStorage.TokenId = Login.LoginResult.UserToken.TokenId;           
        //   sessionStorage.UserId = Login.LoginResult.Data.User.UserId;
          
        // }else{
        //   /* login failure */
        //   this.showToast(Login.LoginResult.ReturnMsg);
        // }

        if(Login.LoginResult.ReturnCode == "0"){          

          this.pageName = "Login"
          this.loginData.keepLogin = this.keepSignedIn;
          this.loginData.LoginStatus = true;
          sessionStorage.loginStatus = true;
          
          if(this.keepSignedIn==true){
            localStorage.userData = JSON.stringify(this.loginData);
            sessionStorage.TokenId = Login.LoginResult.UserToken.TokenId;           
            sessionStorage.UserId = Login.LoginResult.Data.User.UserId;
            sessionStorage.username = Login.LoginResult.Data.User.UserName;
            console.log("userID" + sessionStorage.UserId);
            sessionStorage.Ispos = Login.LoginResult.Data.User.IsPos;
            sessionStorage.IsFGEmployee = Login.LoginResult.Data.User.UserType;
            let loginUserPOSData = Login.LoginResult.Data.User;
            sessionStorage.SumInsured = Login.LoginResult.Data.User.SumInsured;
            sessionStorage.loginUserData =  JSON.stringify(Login.LoginResult.Data.User);
            sessionStorage.loginPosUserData = JSON.stringify(Login.LoginResult.Data.User);
            this.data = sessionStorage.loginUserData;
            this.createUser(Login.LoginResult.Data.User.UserName, sessionStorage.loginStatus);
            sessionStorage.pagename = this.pageName;
            this.navCtrl.push(LandingScreenPage,{"page" : this.pageName});
           
            // if(this.preLogin){
            //   //this.AgentCodePopup = true;
            //   this.navCtrl.push(BuyPage,{preLoginData : this.preLoginQuickQuoteData});
            // }else{
            //   //this.AgentCodePopup = true;
            //   this.navCtrl.push(LandingScreenPage,{});
            // }
          }else{
            sessionStorage.userData = JSON.stringify(this.loginData);
            sessionStorage.TokenId = Login.LoginResult.UserToken.TokenId;          
            sessionStorage.UserId = Login.LoginResult.Data.User.UserId;
            sessionStorage.username = Login.LoginResult.Data.User.UserName;
            console.log("userID" + sessionStorage.UserId);
            
            sessionStorage.loginUserData = Login.LoginResult.Data.User;
            sessionStorage.Ispos = Login.LoginResult.Data.User.IsPos;
            sessionStorage.IsFGEmployee = Login.LoginResult.Data.User.UserType;
            let loginUserPOSData = Login.LoginResult.Data.User;
            sessionStorage.SumInsured = Login.LoginResult.Data.User.SumInsured;
            console.log("LoginPage: " + sessionStorage.loginUserData);
            console.log("LoginDta: " + JSON.stringify(loginUserPOSData));
            this.createUser(Login.LoginResult.Data.User.UserName, sessionStorage.loginStatus);

            if(localStorage.userData){
              localStorage.removeItem("userData");
            }

            if(this.preLogin){
              //this.AgentCodePopup = true;
              sessionStorage.pagename = this.pageName;
              this.navCtrl.push(BuyPage,{preLoginData : this.preLoginQuickQuoteData, userData: loginUserPOSData});
              }else{
              //this.AgentCodePopup = true;
              sessionStorage.pagename = this.pageName;
              this.navCtrl.push(LandingScreenPage,{userData: loginUserPOSData,preloginNormal:true, "page" : this.pageName});
              }
          }
        }else{
          this.toast.create({
            message:Login.LoginResult.ReturnMsg,
            duration: 4000
          }).present();

        }
      });
  }

  ProceedLogin(){
      if(this.agentCode == "" || this.agentCode == undefined || this.agentCode == null){
        this.showToast("Please enter the agent code.")
      }else{
        this.loginWithAgentData = {
          'subAgent': this.agentID,
          'agentCode': this.agentCode
        }
        console.log("test:" + JSON.stringify(this.loginWithAgentData));
        
        var sendData = this.appService.encryptData(JSON.stringify(this.loginWithAgentData), sessionStorage.TokenId+"~"+sessionStorage.UserId);
        this.loginWithAgentService({'request': sendData});
      }
    
  }

  loginWithAgentService(serviceData){
    this.appService.presentServiceLoading();
    console.log("token: " + sessionStorage.TokenId + "," + "userID: " + sessionStorage.UserId );
    
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.appService.callService("Authenticate.svc/AuthenticateAgent",serviceData,headerString)
      .subscribe(AuthenticateAgentResult =>{
       // this.loading.dismiss();
       console.log(JSON.stringify(AuthenticateAgentResult));
       
       this.appService.showloading.dismiss();
        if(AuthenticateAgentResult.AuthenticateAgentResult.ReturnCode == "0"){      
          this.AgentCodePopup = true;
         // this.navCtrl.push(LandingScreenPage,{});
          if(this.keepSignedIn==true){           
              localStorage.userData = JSON.stringify(this.loginData);
              sessionStorage.TokenId = AuthenticateAgentResult.AuthenticateAgentResult.UserToken.TokenId;           
              sessionStorage.UserId = AuthenticateAgentResult.AuthenticateAgentResult.Data.User.UserId;
              sessionStorage.loginUserData =  JSON.stringify(AuthenticateAgentResult.AuthenticateAgentResult.Data.User);
              this.data = sessionStorage.loginUserData;
            if(this.preLogin){
              this.AgentCodePopup = true;
              this.navCtrl.push(BuyPage,{preLoginData : this.preLoginQuickQuoteData});
            }else{
              this.AgentCodePopup = true;
              this.navCtrl.push(LandingScreenPage,{});
            }
          }else{          
              sessionStorage.userData = JSON.stringify(this.loginData);
              sessionStorage.TokenId = AuthenticateAgentResult.AuthenticateAgentResult.UserToken.TokenId;          
              sessionStorage.UserId = AuthenticateAgentResult.AuthenticateAgentResult.Data.User.UserId;
              sessionStorage.loginUserData = JSON.stringify(AuthenticateAgentResult.AuthenticateAgentResult.Data.User);
              console.log("LoginPage: " + sessionStorage.loginUserData);
            
            if(localStorage.userData){
              localStorage.removeItem("userData");
            }
            

            if(this.preLogin){
              this.AgentCodePopup = true;
              this.navCtrl.push(BuyPage,{preLoginData : this.preLoginQuickQuoteData});
            }else{
              this.AgentCodePopup = true;
              this.navCtrl.push(LandingScreenPage,{});
            }
          }
        }else{
          /* login failure */
          this.showToast(AuthenticateAgentResult.AuthenticateAgentResult.ReturnMsg);
        }
      });
  }

  /* toggle Password input  */
  showpassword(){
    this.showPass = !this.showPass;
    if(this.showPass){
      this.picToView="assets/imgs/password_icon-02.svg";
      this.passwordType = 'text';
    } else {
      this.picToView="assets/imgs/password_icon-01.svg";
      this.passwordType = 'password';
    }
  }

  /* redirect to quick quote */
  quickquote(){
    sessionStorage.pagename = "";
    this.navCtrl.push(ProductLandingPage,{forQuickQuote : true});
  }

/* when OK is clciked on input keyboard */
  handleLogin(inputField){
    if( inputField== "employeeIDField" && (this.password == null || this.password == '')){
      this.passwordInput.setFocus();
    }else{
      this.keyboard.close();
    }
    
  }

  ontbChange(event){
    var text ;

    switch(event.value){
      case "Partners":
      text = "Partners";
      break;
      case "Employees":
      text = "Employees";
      break;
    }
    document.getElementById("loginType").innerHTML = text;    
  }

  closeAgentCodePop(){
    this.AgentCodePopup = true;
    this.agentCode = "";
  }

  goToGenOtpPage(){
    if(this.employeeID == undefined || this.employeeID == null || this.employeeID == ""){
      this.showToast("Please enter agent code before clicking forgot password");
    }else{
      this.forgotPasswordService();
    }
    
  }

  forgotPasswordService(){
    var sendData = {
      'username' : this.employeeID,     
    }
  
    console.log("sendData: " + JSON.stringify(sendData));
    var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),'');
    console.log("encrypt" + encryptSendData);
    this.callServiceForgotPassword({"request" : encryptSendData});
  }

  callServiceForgotPassword(sendData){
    this.presentLoadingDefault();         
    
    //let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    //console.log("header: " + JSON.stringify(headerString));
    this.appService.callService("Authenticate.svc/GenerateOTPForgotPassword",sendData,'')
    .subscribe(GenerateOTPForgotPassword =>{
     this.loading.dismiss();
     console.log("PreLoginData :" + JSON.stringify(GenerateOTPForgotPassword));

     if(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnCode == "0"){
       sessionStorage.TokenId = GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.UserToken.TokenId;
       this.navCtrl.push(GenOtpPage, {"userName" : this.employeeID, "mobile" :  GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.Data.Mobile});
     }else if(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnCode == "807"){

     }else if(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnCode == "500"){

     }else if(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnCode == "501"){
       this.showToast(GenerateOTPForgotPassword.GenerateOTPForgotPasswordResult.ReturnMsg);
    }
        
    });
  }
}
