import { Component, NgModule, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { DashboardListingPage } from '../dashboard-listing/dashboard-listing';
import { Chart } from 'chart.js';
import { DatePicker } from '@ionic-native/date-picker';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    DashboardPage,
  ],
  imports: [
    //IonicPageModule.forChild(DashboardPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  @ViewChild('totalPolicies') totalPolicies;
  @ViewChild('quoteGenerated') quoteGenerated;
  @ViewChild('incompletePolicies') incompletePolicies;
  @ViewChild('rejectedPolicies') rejectedPolicies;
  @ViewChild('paymentStatus') paymentStatus;
  @ViewChild('totalRevenueGenerated') totalRevenueGenerated;

  barChart: any;
  pieChart: any;
  loading;
  allPoliciesArray: any = [];
  inCompletePoliciesArray: any = [];
  quotesGeneratedArray: any = [];
  rejectedPoliciesArray: any = [];
  totalPoliciesIssuedCount = 0;
  totalQuoteGeneratedCount = 0;
  incompletePoliciesCount = 0;
  rejectedPoliciesCount = 0;
  statDuration = 'all';
  bannnerImagesArray: any = [];
  today = new Date();
  fromDate;
  toDate;
  dateRange = true;
  dateRangeDisplay = true;
  to;
  from;
  numberLenght(Num) { if (Num >= 10) { return Num } else { return "0" + Num } }

  constructor(private datePicker: DatePicker, public navCtrl: NavController, private menuCtrl: MenuController, public navParams: NavParams,
    public appService: AppService, private loadingCtrl: LoadingController) {
    console.log(menuCtrl.getMenus());
    menuCtrl.swipeEnable(true);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');

    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 3000);
    this.setPaymentStatus();
    this.setRevenueGenerated();
    this.callGetDashBoardStat("all", "all");
  }
  menuButton(Code) {
    console.log(Code);
    switch (Code) {
      case "logout":
      // this.navCtrl.push(LoginPage,{});
    }
  }

  /*Graph show and data bind */
  setTotalPolicies(stats: any, max) {
    this.barChart = new Chart(this.totalPolicies.nativeElement, {

      type: 'bar',
      data: {
        labels: ["Jan", "Feb", "Mar", 'Apr', 'May', "Jun", "Jul", "Aug", 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
          data: [stats[1], stats[2], stats[3], stats[4], stats[5], stats[6], stats[7], stats[8], stats[9], stats[10], stats[11], stats[12]],
          backgroundColor: '#967bdc',
          hoverBackgroundColor: '#967bdc',
          border: 0
        }]
      },
      options: {
        maintainAspectRatio: false,
        "hover": {
          "animationDuration": 0
        },
        "animation": {
          "duration": 1,
          "onComplete": function () {
            var chartInstance = this.chart,
              ctx = chartInstance.ctx;

            ctx.font = Chart.helpers.fontString(10, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = 'grey';

            this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                var data = dataset.data[index];
                ctx.fillText(data, bar._model.x, bar._model.y - 5);
              });
            });
          }
        },
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            barThickness: 15,
            ticks: {
              fontSize: 10,
              autoSkip: false
            },
            gridLines: {
              color: '#FFFFFF',
              border: false
            }
          }],
          yAxes: [{
            ticks: {
              display: false,
              min: 0,
              max: max + 10
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        }
      }

    });
  }

  setQuoteGenerated(stats: any, max) {
    this.barChart = new Chart(this.quoteGenerated.nativeElement, {

      type: 'bar',
      data: {
        labels: ["Jan", "Feb", "Mar", 'Apr', 'May', "Jun", "Jul", "Aug", 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
          data: [stats[1], stats[2], stats[3], stats[4], stats[5], stats[6], stats[7], stats[8], stats[9], stats[10], stats[11], stats[12]],
          backgroundColor: '#ff9f40',
          hoverBackgroundColor: '#ff9f40',
          border: 0
        }]
      },
      options: {
        maintainAspectRatio: false,
        "hover": {
          "animationDuration": 0
        },
        "animation": {
          "duration": 1,
          "onComplete": function () {
            var chartInstance = this.chart,
              ctx = chartInstance.ctx;

            ctx.font = Chart.helpers.fontString(12, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.fillStyle = '#000000';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = 'grey';

            this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                var data = dataset.data[index];
                ctx.fillText(data, bar._model.x, bar._model.y - 5);
              });
            });
          }
        },
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            barThickness: 15,
            ticks: {
              fontSize: 10,
              autoSkip: false
            },
            gridLines: {
              color: '#FFFFFF',
              border: false
            }
          }],
          yAxes: [{
            ticks: {
              display: false,
              min: 0,
              max: max + 10
            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }]
        }
      }

    });
  }

  setIncompletePolicies(count) {
    this.pieChart = new Chart(this.incompletePolicies.nativeElement, {

      type: 'doughnut',
      data: {
        labels: [count + "%"],
        datasets: [{
          data: [count, (100 - count)],
          backgroundColor: [
            "#36bc99",
            "#e3f6f2"
          ],
          hoverBackgroundColor: [
            "#36bc99",
            "#e3f6f2"
          ],
          hoverBorderColor: [
            "#36bc99",
            "#e3f6f2"
          ],
          borderWidth: [0, 0]
        }]
      },
      options: {
        segmentShowStroke: false,
        maintainAspectRatio: false,
        "animation": {
          "duration": 1,
          "onComplete": function (chart) {
            var width = chart.chart.width,
              height = chart.chart.height,
              ctx = chart.chart.ctx;

            ctx.restore();
            var fontSize = (height / 114).toFixed(2);
            // ctx.font = fontSize + "em sans-serif";
            ctx.textBaseline = "middle";
            ctx.font = Chart.helpers.fontString(16, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.fillStyle = '#000000';

            var text = count + "%",
              textX = Math.round((width - ctx.measureText(text).width) / 2),
              textY = height / 2;
            ctx.fillText(text, textX, textY);
            ctx.save();
          }
        },
        legend: {
          display: false
        },
        cutoutPercentage: 75,
        tooltips: {
          filter: function (item, data) {
            var label = data.labels[item.index];
            if (label) return item;
          }
        }
      }
    });
  }

  setRejectedPolicies(count) {
    this.pieChart = new Chart(this.rejectedPolicies.nativeElement, {

      type: 'doughnut',
      data: {
        labels: [count + "%"],
        datasets: [{
          data: [count, (100 - count)],
          backgroundColor: [
            "#d76fac",
            "#fcf6fa"
          ],
          hoverBackgroundColor: [
            "#d76fac",
            "#fcf6fa"
          ],
          hoverBorderColor: [
            "#d76fac",
            "#fcf6fa"
          ],
          borderWidth: [0, 0]
        }]
      },
      options: {
        segmentShowStroke: false,
        maintainAspectRatio: false,
        "animation": {
          "duration": 1,
          "onComplete": function (chart) {
            var width = chart.chart.width,
              height = chart.chart.height,
              ctx = chart.chart.ctx;

            ctx.restore();
            var fontSize = (height / 114).toFixed(2);
            // ctx.font = fontSize + "em sans-serif";
            ctx.textBaseline = "middle";

            ctx.font = Chart.helpers.fontString(16, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.fillStyle = '#000000';
            var text = count + "%",
              textX = Math.round((width - ctx.measureText(text).width) / 2),
              textY = height / 2;

            ctx.fillText(text, textX, textY);
            ctx.save();
          }
        },
        legend: {
          display: false
        },
        cutoutPercentage: 75,
        tooltips: {
          filter: function (item, data) {
            var label = data.labels[item.index];
            if (label) return item;
          }
        }
      }
    });
  }

  setPaymentStatus() {
    this.pieChart = new Chart(this.paymentStatus.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["15%", "85%"],
        datasets: [{
          data: [15, 85],
          backgroundColor: [
            "#fa6d51",
            "#f5ebe9"
          ],
          hoverBackgroundColor: [
            "#fa6d51",
            "#f5ebe9"
          ],
          borderWidth: [0, 0]
        }]
      },
      options: {
        maintainAspectRatio: false,
        "animation": {
          "duration": 1,
          "onComplete": function (chart) {
            var width = chart.chart.width,
              height = chart.chart.height,
              ctx = chart.chart.ctx;

            ctx.restore();
            var fontSize = (height / 114).toFixed(2);
            ctx.font = fontSize;
            ctx.textBaseline = "middle";

            ctx.font = Chart.helpers.fontString(16, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.fillStyle = '#000000';
            var text = "₹" + "15L",
              textX = Math.round((width - ctx.measureText(text).width) / 2),
              textY = height / 2;


            ctx.fillText(15, textX, textY);
            ctx.save();
          }
        },
        legend: {
          display: false
        },
        cutoutPercentage: 75,
        tooltips: {
          enabled: false
        }
      }
    });
  }

  setRevenueGenerated() {
    this.pieChart = new Chart(this.totalRevenueGenerated.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["15%"],
        datasets: [{
          data: [33, 67],
          backgroundColor: [
            "#ed5564",
            "#f7ebed"
          ],
          hoverBackgroundColor: [
            "#ed5564",
            "#f7ebed"
          ],
          hoverBorderColor: [
            "#ed5564",
            "#f7ebed"
          ],
          borderWidth: [0, 0]
        }]
      },
      options: {
        segmentShowStroke: false,
        maintainAspectRatio: false,
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        "animation": {
          "duration": 1,
          "onComplete": function (chart) {
            var width = chart.chart.width,
              height = chart.chart.height,
              ctx = chart.chart.ctx;

            ctx.restore();
            var fontSize = (height / 114).toFixed(2);
            // ctx.font = fontSize + "em sans-serif";
            ctx.textBaseline = "middle";

            ctx.font = Chart.helpers.fontString(16, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.fillStyle = '#000000';
            var text = "15%",
              textX = Math.round((width - ctx.measureText(text).width) / 2),
              textY = height / 2;

            ctx.fillText(text, textX, textY);
            ctx.save();
          }
        },
        legend: {
          display: false
        },
        cutoutPercentage: 85,
        tooltips: {
          filter: function (item, data) {
            var label = data.labels[item.index];
            if (label) return item;
          }
        }
      }
    });
  }

  /*Graph show and data bind */

  onBlockClick(type) {
    if (type < 5) {
      this.navCtrl.push(DashboardListingPage, { "Type": type, "TotalPolicyCount": this.totalPoliciesIssuedCount, "IncompletePolicyCount":this.incompletePoliciesCount, "RejectedPoliciesCount":this.rejectedPoliciesCount, "TotalQuoteGeneratedCount":this.totalQuoteGeneratedCount});
    }
  }

  /*Service call to get data */
  callGetDashBoardStat(fromDate, toDate) {
    let data = {
      "fromdate": fromDate,
      "todate": toDate
    };
    var sendData = this.appService.encryptData(JSON.stringify(data), sessionStorage.TokenId + "~" + sessionStorage.UserId);
    this.sendData("Dashboard.svc/GetDashBoardStats", { 'request': sendData }, this.getDashBoardStatCallback);
  }

  getDashBoardStatCallback(Resp, Context) {
    var respData = Resp.GetDashBoardStatsResult;
    sessionStorage.TokenId = respData.UserToken.TokenId;
    console.log("Dashbord Search Data: " + JSON.stringify(respData));
    

    if (respData.ReturnCode == "0" && respData.Data != null) {
      if(respData.Data.AllPoliciesIssued != null){
        for (let i = 0; i < respData.Data.AllPoliciesIssued.length; i++) {
          Context.allPoliciesArray.push(respData.Data.AllPoliciesIssued[i].PolicyDetails);
        }   
      }
     
      Context.quotesGeneratedArray = respData.Data.AllQuotes;
      if(respData.Data.IncompletePolicies != null){
        for (let i = 0; i < respData.Data.IncompletePolicies.length; i++) {
          Context.inCompletePoliciesArray.push(respData.Data.IncompletePolicies[i].PolicyDetails);
        }
      }
      if(respData.Data.IncompletePolicies != null){
        for (let i = 0; i < respData.Data.AllRejectedPolicies.length; i++) {
          Context.rejectedPoliciesArray.push(respData.Data.AllRejectedPolicies[i].PolicyDetails);
        }
      }
      Context.sortDataForBarGraph("All", "all");
      Context.sortDataForBarGraph("Quotes", "all");
      Context.sortDataForDonutGraph("Incomplete", "all");
      Context.sortDataForDonutGraph("Rejected", "all");
    }
    Context.callGetDashboardBanners();
  }

  callGetDashboardBanners() {
    let data = {
      "agentId": sessionStorage.UserId
    };
    var sendData = this.appService.encryptData(JSON.stringify(data), sessionStorage.TokenId + "~" + sessionStorage.UserId);
    this.sendData("Dashboard.svc/GetDashboardBanners", { 'request': sendData }, this.getDashboardBannersCallback);
  }

  getDashboardBannersCallback(Resp, Context) {
    let respData = Resp.GetDashboardBannersResult;
    sessionStorage.TokenId = respData.UserToken.TokenId;

    if (respData.ReturnCode == "0") {
      if (respData.Data.Banner.length != 0) {
        Context.bannnerImagesArray = respData.Data.Banner;
        console.log("dashboard search data:" + respData.Data.Banner);
      }
    }
  }

  sendData(URL, serviceData, callbackFunc) {
    let headerString = this.appService.getBase64string(sessionStorage.TokenId + "~" + sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL, serviceData, headerString)
      .subscribe(Resp => {
        this.loading.dismiss();
        console.log(JSON.stringify(Resp));
        callbackFunc(Resp, this);
      });
  }
  /*Service call to get data */

  /*Data sort */
  sortDataForBarGraph(Type, duration) {
    var stats = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0 };

    if (Type == "All") {
      stats = this.sortValue(this.allPoliciesArray, duration);
      let value = this.getMaxValue(stats);

      this.totalPoliciesIssuedCount = parseInt(value.split("-")[1]);
      this.setTotalPolicies(stats, parseInt(value.split("-")[0]));
    } else {
      stats = this.sortValue(this.quotesGeneratedArray, duration);

      let value = this.getMaxValue(stats);
      this.totalQuoteGeneratedCount = parseInt(value.split("-")[1]);
      this.setQuoteGenerated(stats, parseInt(value.split("-")[0]));
    }
  }

  sortValue(array: any, duration) {
    var stats = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0 };
    array.forEach((element, index) => {
      var date = new Date(element.createdOn);
      var month = date.getMonth() + 1;

      let weekAgo = new Date(new Date().setDate(new Date().getDate() - 7));
      let monthAgo = new Date(new Date().setDate(new Date().getDate() - 31));

      let isAcceptable = duration == "all" ? true :
        duration == "today" ? this.getFormattedDate(date.toString()) == this.getFormattedDate(new Date().toString()) :
          duration == "week" ? this.isDateValid(weekAgo, new Date(), date) : this.isDateValid(monthAgo, new Date(), date);


      if (isAcceptable) {
        stats[month] += parseInt(element.PolicyCount);
      }
    });
    return stats;
  }

  getFormattedDate(date) {
    var splitedDate = date.toString().split(' ');
    return splitedDate[0] + " " + splitedDate[1] + " " + splitedDate[2] + " " + splitedDate[3] + " "
  }

  getMaxValue(stats) {
    var max = 0;
    var count = 0;
    for (var i = 1; i <= 12; i++) {
      if (stats[i] > 0 && stats[i] > max) { max = stats[i]; }
      count += parseInt(stats[i]);
    }
    return max + "-" + count;
  }

  sortDataForDonutGraph(Type, duration) {
    var count = 0;
    if (Type == "Incomplete") {
      count = this.getCount(this.inCompletePoliciesArray, duration);
      this.incompletePoliciesCount = count;
      this.setIncompletePolicies(count != 0 ? Math.round((count / 500) * 100) : 0);
    } else {
      count = this.getCount(this.rejectedPoliciesArray, duration);
      this.rejectedPoliciesCount = count;
      this.setRejectedPolicies(count != 0 ? Math.round((count / 500) * 100) : 0);
    }

  }

  getCount(array: any, duration) {
    var count = 0;
    array.forEach((element, index) => {
      var date = new Date(element.createdOn);

      let weekAgo = new Date(new Date().setDate(new Date().getDate() - 7));
      let monthAgo = new Date(new Date().setDate(new Date().getDate() - 31));

      let isAcceptable = duration == "all" ? true :
        duration == "today" ? this.getFormattedDate(date.toString()) == this.getFormattedDate(new Date().toString()) :
          duration == "week" ? this.isDateValid(weekAgo, new Date(), date) : this.isDateValid(monthAgo, new Date(), date);

      if (isAcceptable) {
        var month = date.getMonth() + 1;
        count = count + parseInt(element.PolicyCount);
      }
    });
    return count;
  }

  isDateValid(startDate, endDate, checkDate) {

    startDate = new Date(startDate).getTime();
    endDate = new Date(endDate).getTime();
    checkDate = new Date(checkDate).getTime();

    if (checkDate > startDate && checkDate < endDate) return true;
    else return false;
  }

  /*Data sort */


  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  onTabChange(event) {
    console.log(event.value);

    switch (event.value) {
      case "all":
        this.sortDataForBarGraph("All", "all");
        this.sortDataForBarGraph("Quotes", "all");
        this.sortDataForDonutGraph("Incomplete", "all");
        this.sortDataForDonutGraph("Rejected", "all");
        break;
      case "today":
        this.sortDataForBarGraph("All", "today");
        this.sortDataForBarGraph("Quotes", "today");
        this.sortDataForDonutGraph("Incomplete", "today");
        this.sortDataForDonutGraph("Rejected", "today");
        break;
      case "week":
        this.sortDataForBarGraph("All", "week");
        this.sortDataForBarGraph("Quotes", "week");
        this.sortDataForDonutGraph("Incomplete", "week");
        this.sortDataForDonutGraph("Rejected", "week");
        break;
      case "month":
        this.sortDataForBarGraph("All", "month");
        this.sortDataForBarGraph("Quotes", "month");
        this.sortDataForDonutGraph("Incomplete", "month");
        this.sortDataForDonutGraph("Rejected", "month");
        break;
      default:
        break;
    }
  }

  /* Sourov Custom Date functionality */

  showDatePicker() {
    this.dateRange = false;
  }

  fromDatePick() {
    let element = this.fromDate;
    let dateFieldValue = element;
    let year : any = 1985, month : any = 1, date : any = 1;
  
    if(dateFieldValue != undefined){
      date = dateFieldValue.split("-")[0]
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }  
        //minDateLimit  = ( new Date(this.today.getFullYear()-25,this.today.getMonth(),this.today.getDate()).getTime());
        this.datePicker.show({
          date: new Date(),
          //minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
        }).then(
          date => {
            let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
            this.fromDate = dt.toString();  
            let dtFrom = ( this.numberLenght(date.getMonth() + 1)+"-"+this.numberLenght(date.getDate())+"-"+(date.getFullYear()) ).toString();
            this.from = dtFrom.toString(); 
                  
          },
          err => console.log('Error occurred while getting date: ', err)
        );
  }

  toDatePick() {
    let element = this.toDate;
    let dateFieldValue = element;
    let year : any = 1985, month : any = 1, date : any = 1;
  
    if(dateFieldValue != undefined){
      date = dateFieldValue.split("-")[0];
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2];
    }    
        //minDateLimit  = ( new Date(this.today.getFullYear()-25,this.today.getMonth(),this.today.getDate()).getTime());
        this.datePicker.show({
          date: new Date(),
          //minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
        }).then(
          date => {
            let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
            this.toDate = dt.toString(); 
              
           let dtTo = ( this.numberLenght(date.getMonth() + 1)+"-"+this.numberLenght(date.getDate())+"-"+(date.getFullYear()) ).toString();
           this.to = dtTo.toString();          
          },
          err => console.log('Error occurred while getting date: ', err)
        );
  }

  submitBtn() {

   

    if ((this.fromDate == "" || this.fromDate == undefined) && (this.toDate == "" || this.toDate == undefined)) {
      this.appService.showToast("Please enter the dates to proceed.");
    }
    else if (this.fromDate == "" || this.fromDate == undefined) {
      this.appService.showToast("Please enter From date to proceed.");
    } else if (this.toDate == "" || this.toDate == undefined) {
      this.appService.showToast("Please enter To date to proceed.");
    } else if(Date.parse(this.to) < Date.parse(this.from)){
      this.appService.showToast("Please select correct date range.");
    } else {
      this.allPoliciesArray = [];
      this.inCompletePoliciesArray = [];
      this.quotesGeneratedArray = [];
      this.rejectedPoliciesArray = [];
      this.callGetDashBoardStat(this.fromDate, this.toDate);
      this.dateRange = true;
      this.dateRangeDisplay = false;
    }

  }

  cancel() {
    this.dateRange = true;
    // this.dateRangeDisplay = 
  }

}


