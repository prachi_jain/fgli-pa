import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ProductBenefitsPage } from '../product-benefits/product-benefits';
import { PlansLimitsPage } from '../plans-limits/plans-limits';
import * as $ from 'jquery';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { AppService } from '../../../providers/app-service/app-service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LoginPage } from '../login/login';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { WaitingperiodsPage } from '../waitingperiods/waitingperiods';

/**
 * Generated class for the KnowledgebasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-knowledgebase',
  templateUrl: 'knowledgebase.html',
})
export class KnowledgebasePage {
 showTransBaground = true;
 VideoshowTransBaground = true;
 partdisab = true;
 permdisab = true;
 tempdisab = true;
 isActive = true; 
 showPopup = true;
 videoShowPopup = true; 
 totalDis;
 videos;
 videoLink: SafeResourceUrl;    
 itemLink;
 canShowToast = true;
 loading;
 collateralsData;
 email;
 mobile;
 pdfLinkURL;
 pdfName;
 pdfProduct;
 pdfData;
 serviceResponsePopup = true;
 message;
 serviceCodeStatus; 
 blankDiv = true; 
 clickStatusClose = false;
 videoCLickStatus = false;
 productList = [];
 product = "HTO";
 showDiv = false;

  constructor(public navCtrl: NavController, public screenOrientation: ScreenOrientation, private iab: InAppBrowser, public appService: AppService, public toast: ToastController, public loadingCtrl: LoadingController,private domSanitizer: DomSanitizer, public navParams: NavParams) {
    this.productList = JSON.parse(localStorage.ProductData);
    console.log("list" + localStorage.ProductData);    
    console.log("product" + JSON.stringify(this.productList));
    
  }
   pet ='Features';
   

  ionViewDidLoad() {
   
    console.log('ionViewDidLoad KnowledgebasePage');
        setTimeout(function() {         
          document.getElementById("splash").style.display='none';                 
      }, 7000);
      console.log("orientation :" + this.screenOrientation.type);
      this.getKnowledgeBaseData("HTO");
  }

  changeProduct(){
    this.getKnowledgeBaseData(this.product);
  }

  getKnowledgeBaseData(product){
    this.presentLoadingDefault();         
    console.log("selected data"+ product);
    let sendProduct;
    if(product == "HTO"){
      this.showDiv = false;
    }else if(product == "vector"){
      this.showDiv = true;
    }else if(product == "topup"){
      this.showDiv = true;
    }else if(product == "pa"){
      this.showDiv = true;
    }
    // if(product == 1){
    //   sendProduct = "HTO";
    //   this.showDiv = false;
    // }else if(product == 2){
    //   sendProduct = "vector";
    //   this.showDiv = true;
    // }else{
    //   sendProduct = "HTO";
    // }
    
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    console.log("header: " + JSON.stringify(headerString));       
    let request = {"ProductCode" : product};    
    var sendData = this.appService.encryptData(JSON.stringify(request),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    let sendRequestData = {"request" : sendData};
    this.appService.callService("FGHealth.svc/GetKnowledgeBaseData",sendRequestData,headerString)
    .subscribe(KnowledgeBase =>{
     this.loading.dismiss();
     console.log("PreLoginData :" + JSON.stringify(KnowledgeBase));

     if(KnowledgeBase.GetKnowledgeBaseDataResult.ReturnCode == "0"){
      this.pet = 'Features';
      sessionStorage.TokenId = KnowledgeBase.GetKnowledgeBaseDataResult.UserToken.TokenId;
      this.videos = KnowledgeBase.GetKnowledgeBaseDataResult.Data.VideoData;
      this.collateralsData = KnowledgeBase.GetKnowledgeBaseDataResult.Data.CollateralData;
      console.log("VideoURL: " + JSON.stringify(this.videos));
      
     }else if(KnowledgeBase.GetKnowledgeBaseDataResult.ReturnCode == "807"){
      sessionStorage.TokenId = KnowledgeBase.GetKnowledgeBaseDataResult.UserToken.TokenId;
     }else if(KnowledgeBase.GetKnowledgeBaseDataResult.ReturnCode == "500"){
      sessionStorage.TokenId = KnowledgeBase.GetKnowledgeBaseDataResult.UserToken.TokenId;
     }else if(KnowledgeBase.GetKnowledgeBaseDataResult.ReturnCode == "501"){
      sessionStorage.TokenId = KnowledgeBase.GetKnowledgeBaseDataResult.UserToken.TokenId;
     }        
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

showToast(Message){
  if(this.canShowToast){
    let pageToast = this.toast.create({
      message:Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 5000,
    });
    this.canShowToast = false;
    pageToast.present();
    pageToast.onDidDismiss(() => {
      this.canShowToast = true;
    });
  }   
}
    perClick(num){
    console.log(this.totalDis)
    if(num ==  1){
    this.partdisab = !this.partdisab;
    }if(num ==  2){
      this.permdisab = !this.permdisab;
    }else if(num ==3){
      this.tempdisab = !this.tempdisab;
  }

  }
    shareClick(item){
      console.log("pdfdata:" + JSON.stringify(item));
      
    this.pdfLinkURL = item.CollateralUrl;
    this.pdfName = item.CollateralTitle;
    this.pdfProduct = "Health Insurance";
    this.showPopup = false;
    this.showTransBaground = false;
  }
    videoClick(link){
      this.videoCLickStatus = true;
      console.log("link" + link);
  //     this.blankDiv = false;
  //     let TIME_IN_MS = 2000;
  //     let hideFooterTimeout = setTimeout( () => {
  //       this.blankDiv = true;
  // }, TIME_IN_MS);
       
      this.videoLink = this.domSanitizer.bypassSecurityTrustResourceUrl(link);
      console.log("videoSafe:" + this.videoLink);  
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
    });

    this.loading.present();
    let TIME_IN_MS = 2000;
        let hideFooterTimeout = setTimeout( () => {
          this.videoShowPopup = false;
          this.VideoshowTransBaground = false;
    }, TIME_IN_MS);
    //  this.videoShowPopup = false;
    // this.VideoshowTransBaground = false;
    //this.videoLink = link;
   
    console.log("video:" + this.videoLink);
    
  }

  handleIFrameLoadEvent(): void {
    this.loading.dismiss();
    if(this.clickStatusClose == true){
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    if(this.videoCLickStatus == true){
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    }else{
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }    
}

  videoHidePopup(){
    this.clickStatusClose = true;
    this.videoCLickStatus = false;
    //this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.videoShowPopup = true;
    this.VideoshowTransBaground = true;
    $('.videoPopup').each(function(index) {     
      $('iframe').attr('src', $('iframe').attr('src')); 
      return false;
    });    
  }
    hidePopup(){
    this.showPopup = true;
    this.showTransBaground = true;
  }

  goToProductBenefits(){
  	this.navCtrl.push(ProductBenefitsPage);
  }
  goToPlanLimits(){
    this.navCtrl.push(PlansLimitsPage);
  }


  goToWaitingPeriods(){
    this.navCtrl.push(WaitingperiodsPage);
  }

  downloadPdf(pdfLink){
    console.log("pdf:" + pdfLink);
    
    let browser = this.iab.create(pdfLink  ,'_system', 'location=yes');
    //browser.show();
  }

  shareButton(){

    if((this.email == "" || this.email == undefined) && (this.mobile == "" || this.mobile == undefined)){
      this.showToast("Please enter email or mobile.");
    }else if(this.email == "" || this.email == undefined){
      if(this.mobile == "" || this.mobile == undefined){
        this.showToast("Please enter mobile.");
      }else if(this.mobile.length < 10){
        this.showToast("Please enter 10 digit mobile number.");  
      }else if(!this.isValidMobile(this.mobile)){
        this.showToast("Please enter valid mobile no");     
      }else{
        var sendData = {
          'cust_email' : this.email,
          'cust_mobile' : this.mobile,
          'pdflink' : this.pdfLinkURL,
          'pdfname' : this.pdfName,
          'product' : this.pdfProduct
        }
  
  
      
        console.log("sendData: " + JSON.stringify(sendData));
        var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),sessionStorage.TokenId);
        console.log("encrypt" + encryptSendData);
        this.shareNowServiceClick({"request" : encryptSendData}); 
      }    
    }else if(this.mobile == "" || this.mobile == undefined){
      if(this.email == "" || this.email == undefined){
        this.showToast("Please enter email.");
      }else if(!this.matchEmail(this.email)){
        this.showToast("Please enter valid email.");         
      }else{
        var sendData = {
          'cust_email' : this.email,
          'cust_mobile' : this.mobile,
          'pdflink' : this.pdfLinkURL,
          'pdfname' : this.pdfName,
          'product' : this.pdfProduct
        }  
        
        console.log("sendData: " + JSON.stringify(sendData));
        var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),sessionStorage.TokenId);
        console.log("encrypt" + encryptSendData);
        this.shareNowServiceClick({"request" : encryptSendData}); 
      }
        
    }
         
  }

  shareNowServiceClick(sendData){
    this.presentLoadingDefault();         
    
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    console.log("header: " + JSON.stringify(headerString));
    this.appService.callService("FGHealth.svc/SendCollateralMailSMS",sendData,headerString)
    .subscribe(CollateralSmsEmailData =>{
      this.loading.dismiss();
      console.log("PreLoginData :" + JSON.stringify(CollateralSmsEmailData));

     if(CollateralSmsEmailData.SendCollateralMailSMSResult.ReturnCode == "0"){
      sessionStorage.TokenId = CollateralSmsEmailData.SendCollateralMailSMSResult.UserToken.TokenId;
      //this.serviceResponsePopup = false;
      this.email = "";
      this.mobile = "";
      this.showPopup = true;
      this.showToast("Document has been sent successfully.");
      //this.message = ;
      this.serviceCodeStatus = 0;
      //this.showToast("Password changed successfully.");        
     }else if(CollateralSmsEmailData.SendCollateralMailSMSResult.ReturnCode == "807"){
      sessionStorage.TokenId = CollateralSmsEmailData.SendCollateralMailSMSResult.UserToken.TokenId;
      this.serviceCodeStatus = 807;
     }else if(CollateralSmsEmailData.SendCollateralMailSMSResult.ReturnCode == "500"){
      sessionStorage.TokenId = CollateralSmsEmailData.SendCollateralMailSMSResult.UserToken.TokenId;
      this.serviceCodeStatus = 500;
     }else if(CollateralSmsEmailData.SendCollateralMailSMSResult.ReturnCode == "501"){
      sessionStorage.TokenId = CollateralSmsEmailData.SendCollateralMailSMSResult.UserToken.TokenId;
      this.message = CollateralSmsEmailData.SendCollateralMailSMSResult.ReturnMsg;
      //this.message = "Password cannot be the same as previous password.";
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 501;    
     }
        
    });
  }

  matchEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  isValidMobile(mobile){
    var regex = /^[6-9][0-9]{9}$/;
    return regex.test(mobile);
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }else if(this.serviceCodeStatus == 501){
      this.serviceResponsePopup = true;
    }
  
  }

}
