import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KnowledgebasePage } from './knowledgebase';

@NgModule({
  declarations: [
    //KnowledgebasePage,
  ],
  imports: [
    IonicPageModule.forChild(KnowledgebasePage),
  ],
})
export class KnowledgebasePageModule {} 
