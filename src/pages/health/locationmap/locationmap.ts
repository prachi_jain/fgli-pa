import { Component, NgModule, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LocationmapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


//declare var google:any;

declare var google:any;
@NgModule({
  declarations: [
    LocationmapPage,
  ],
  imports: [
    //IonicPageModule.forChild(LandingScreenPage),
  ],
})


@IonicPage()
@Component({
  selector: 'page-locationmap',
  templateUrl: 'locationmap.html',
})
export class LocationmapPage {  
  lat;
  lang;
  @ViewChild('map') mapElement: ElementRef;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.lat = navParams.get("lat");
    this.lang = navParams.get("lang");
    console.log(this.lat + "++++" + this.lang);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationmapPage');
    this.initMap();
  }

  initMap() {
    const location = new google.maps.LatLng(this.lat, this.lang);
    const options ={
      center: location,
      zoom:15
    };

    const map = new google.maps.Map(this.mapElement.nativeElement, options);
    this.addMarker(map);
  }

  addMarker(map:any){

    let marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: map.getCenter()
    });
    
    let content = "<h4>Information!</h4>";
    
    this.addInfoWindow(marker, content, map);    
    }

    addInfoWindow(marker, content, map){
 
      let infoWindow = new google.maps.InfoWindow({
        content: content
      });
     
      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(map, marker);
      });
     
    }

}
