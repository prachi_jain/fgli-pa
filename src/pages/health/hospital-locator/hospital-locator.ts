import { Component, NgModule, state } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HospitalSearchResultPage } from '../hospital-search-result/hospital-search-result';
import { AppService } from '../../../providers/app-service/app-service';
import { Http } from "@angular/http";
import { map, catchError } from 'rxjs/operators';
import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';

/**
 * Generated class for the HospitalLocatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    HospitalLocatorPage,
  ],
  imports: [
    //IonicPageModule.forChild(LandingScreenPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-hospital-locator',
  templateUrl: 'hospital-locator.html',
})
export class HospitalLocatorPage {

  stateList;
  cityList;
  state;
  stateID;
  city;
  hospitalName;
  canShowToast;
  divShow;
  pageName;

  constructor(public navCtrl: NavController, public toast: ToastController, public navParams: NavParams,private _http: Http, public appService: AppService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HospitalLocatorPage');    
    this.pageName = this.navParams.get("gotoWhichPage");
    if(this.pageName == "hospital"){
      this.divShow = false;
    }else{
      this.divShow =true;
    }
    this.getStateData();
  }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  changeState(){
    // for(let i=0; i< this.stateList.length; i++){
    //   if(this.state == this.stateList[i].sName){
    //     this.stateID = this.stateList[i].iID;
    //     console.log("id: " + this.stateID);
        
    //   }
    //}
    console.log("id: "+ this.state);
    
    this.appService.getCityFGIAPiCall(this.state).subscribe(
      result => {
        console.log("City: " + JSON.stringify(result));
        this.cityList = result.clists;
             },
            error => { },
            () => { }
    );
  }

  getStateData(){
    this.appService.getStateFGIAPiCall().subscribe(
      result => {
  console.log("State: " + JSON.stringify(result));
  this.stateList = result.clists;
       },
      error => { },
      () => { }
    );
    
  }

  goToSearchResultPage(){

    console.log(this.state);
    console.log(this.city);
    console.log(this.hospitalName);
    
    
    if(this.state == undefined && this.city == undefined && this.hospitalName == undefined){
      this.showToast("Please search by any one from above options");
    }else if(this.state != undefined && this.city == undefined){
      this.showToast("Please search by State with city");
    }
    /*else if (this.state != undefined && this.city == undefined && this.hospitalName == undefined){
      console.log("State");
      
      this.appService.getHospitalByState(this.pageName).subscribe(
        result => {
        console.log("hospital State: " + JSON.stringify(result)); 
        this.navCtrl.push(HospitalSearchResultPage,{"hospitalData": result}); 
         },
        error => { 
          console.log(error);
        },
        () => { }
      );
    }
    */else if(this.state != undefined && this.city != undefined && (this.hospitalName == "" || this.hospitalName == undefined)){
      this.appService.getHospitalDataByStateCity(this.city, this.pageName).subscribe(
        result => {
        console.log("hospital State City: " + JSON.stringify(result)); 
        console.log("page: " + this.pageName); 
        this.navCtrl.push(HospitalSearchResultPage,{"hospitalData": result, "page": this.pageName}); 
         },
        error => { 
          console.log(error);
          this.showToast(error);
        },
        () => { }
      );
    }else if(this.hospitalName != undefined && this.state == undefined && this.city == undefined){
      this.appService.getHospitalNameApiCall(this.hospitalName, this.pageName).subscribe(
        result => {
        console.log("hospital: " + JSON.stringify(result)); 
        console.log("page: " + this.pageName); 
        this.navCtrl.push(HospitalSearchResultPage,{"hospitalData": result, "page": this.pageName}); 
         },
        error => { 
          console.log(error);
          this.showToast(error);
        },
        () => { }
      );
    }else if(this.state != undefined && this.city != undefined && this.hospitalName != undefined){
      this.appService.getHospitalByStateCityHospitalName(this.state, this.city, this.hospitalName, this.pageName).subscribe(
        result => {
        console.log("hospital with state and city " + JSON.stringify(result)); 
        console.log("page: " + this.pageName); 
        this.navCtrl.push(HospitalSearchResultPage,{"hospitalData": result,  "page": this.pageName}); 
         },
        error => { 
          console.log(error);
          this.showToast(error);
        },
        () => { }
      );
    }
  }

}
