import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { PaymentMethodPage } from '../payment-method/payment-method';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppService } from '../../../providers/app-service/app-service';
import { PaymentSuccessPage } from '../payment-success/payment-success';
import { LoginPage } from '../login/login';

/**
 * Generated class for the DisclaimerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-disclaimer',
  templateUrl: 'disclaimer.html',
})
export class DisclaimerPage {
  memberDetails;
  selectedCardArray;
  hideEmandateDiv = true;
  pet;
  QuotationID;
  UID;
  selectedValue;
  canShowToast = true;
  ReferenceNo;
  loading;
  message;
  serviceResponsePopup = true;
  serviceCodeStatus;
   //Live
   baseUrl = "https://i-insure.futuregenerali.in/";
   //UAT
   //baseUrl = "https://i-insureuat.futuregenerali.in/";

  constructor(private platform: Platform, public navCtrl: NavController,  public toast: ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, private iab: InAppBrowser, public appService: AppService) {
    this.memberDetails = navParams.get("memberDetails");
    this.selectedCardArray = navParams.get("planCardDetails");
    console.log("test: " + JSON.stringify(this.memberDetails));
    console.log("Card: " + JSON.stringify(this.selectedCardArray));
    platform.registerBackButtonAction(() => {
      this.onBackClick();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DisclaimerPage');
    this.QuotationID = this.selectedCardArray.QuotationID;
    this.UID = this.selectedCardArray.UID;
    this.ReferenceNo = this.selectedCardArray.ReferenceNo;
    sessionStorage.RefNO = this.ReferenceNo;
  //     setTimeout(function() {
  //     document.getElementById("splash").style.display='none';
  // }, 3000);       
  }

  petChange(value){
    console.log("selected Radio: " + value);
    //selectedRadioVal = value;
    if(value == "standingScreen"){
      this.selectedValue = "Y";
    }
  }

  // goToPaymentPage(){
  //   console.log("state: " + this.pet);
    
  //   this.navCtrl.push(PaymentMethodPage, {"memberDetails":this.memberDetails,  "planCardDetails": this.selectedCardArray});
  // }

    /* online payment inappbrowser */
    goToPaymentPage(){
  
if(this.selectedValue == "Y"){
  let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
  let request = this.appService.encryptData(JSON.stringify({"QuotationID":this.QuotationID,"UID":this.UID,"PID":sessionStorage.policyID}),sessionStorage.TokenId);

  let browser = this.iab.create(this.baseUrl+'PayU/PGRequest.aspx?request=' + encodeURIComponent(request) + "&tokenID=" + encodeURIComponent(headerString) + "&SI=" + encodeURIComponent(this.selectedValue), '_blank', 'location=no');
  //let browser = this.iab.create('http://fghealthapi.idealake.com/PayU/PGRequest.aspx?request='+encodeURIComponent(request)+'&tokenID='+encodeURI(headerString)+'&SI='+"Y", '_blank', 'location=no');
  let watch = browser.on('loadstart').subscribe(type => {
    console.log(JSON.stringify(type));
    if(type.url.indexOf("?")>=0){
      console.log(type.url.indexOf("?"));
      
      //type.url = "https://i-insureuat.futuregenerali.in/PayU/PaymentLinkResponse.aspx?sdfdff";
      let url = type.url.split("?");
      //let url = type.url.split("?");
      console.log("Active Page URL : "+ JSON.stringify(type));
      if(url[0]== this.baseUrl+"PayU/Cancel.aspx"){
        let TIME_IN_MS = 10000;
                let hideFooterTimeout = setTimeout( () => {
                browser.close();
            }, TIME_IN_MS); 
        let err = url[1].split("=");
        sessionStorage.TokenId = err[1];
      }else if(url[0] == this.baseUrl+"PayU/Failure.aspx" ){
        let TIME_IN_MS = 10000;
                let hideFooterTimeout = setTimeout( () => {
                browser.close();
            }, TIME_IN_MS); 
        let err = url[1].split("=");
        console.log(sessionStorage.TokenId);
        console.log(err);
        sessionStorage.TokenId = err[1];
        this.navCtrl.push(PaymentSuccessPage,{"Result":0,"Data":{"QuotationID":this.QuotationID,"UID":this.UID,"PID":sessionStorage.policyID}});
      }else if(url[0]== this.baseUrl+"PayU/Success.aspx"){
        let TIME_IN_MS = 10000;
                let hideFooterTimeout = setTimeout( () => {
                browser.close();
            }, TIME_IN_MS); 
       let err = url[1].split("=");
       sessionStorage.TokenId = err[1];
       var paymentData = {
        "QuotationID": this.QuotationID,
        "UID" : this.UID,
        "PID": sessionStorage.policyID,
     };
      console.log(sessionStorage.TokenId);          
      sessionStorage.TokenId = err[1];
      console.log("Online sending Data: " + JSON.stringify(paymentData)); 
      let dataToSend = this.appService.encryptData(JSON.stringify(paymentData),sessionStorage.TokenId); 
      let request = {"request":dataToSend};
      console.log("encryptData: " + JSON.stringify(dataToSend));
      
      this.callPaymentDetailsService("/Payment.svc/GetPaymentDetails",request); 
      }else if(url[0]=this.baseUrl+"PayU/Error.aspx"){ 
      
      }else{
      
        this.showToast("Oops ! Something went wrong.");
      }

    }
  });
  browser.show();
}else{
  this.showToast("Please select any option from above"); 
}
     
    }

    callPaymentDetailsService(URL,serviceData){                                                                                  
      this.presentLoadingDefault();    
  
      let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService(URL,serviceData,headerString)
        .subscribe(Resp =>{
          this.loading.dismiss(); 
          console.log("test: " + JSON.stringify(Resp));
          if(Resp.GetPaymentDetailsResult.ReturnCode == "0"){
                  sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
                  this.serviceCodeStatus = 0;
                  // this.transactionID = Resp.GetPaymentDetailsResult.Data.TransactionID;
                  // this.policyNumber = Resp.GetPaymentDetailsResult.Data.PolicyNo;
                  // this.bankTransactionID = Resp.GetPaymentDetailsResult.Data.BankTransactionID;
                  // this.transactionAmount = this.addCommas(Resp.GetPaymentDetailsResult.Data.PremiumAmount);
                  // this.transactionDate = Resp.GetPaymentDetailsResult.Data.TransactionDate;
                  // this.email = Resp.GetPaymentDetailsResult.Data.Email;
                  // this.mobile = Resp.GetPaymentDetailsResult.Data.Mobile;
                  // this.pdfLink = Resp.GetPaymentDetailsResult.Data.PDF;
                  // this.showFooter = false;
                 this.navCtrl.push(PaymentSuccessPage,{"Result":1,"Data":Resp.GetPaymentDetailsResult.Data});
          }else if(Resp.GetPaymentDetailsResult.ReturnCode == "807"){
            //this.showToast(Resp.GetPaymentDetailsResult.ReturnMsg);
            sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
            this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 807;
            this.navCtrl.push(LoginPage);
          }else if(Resp.GetPaymentDetailsResult.ReturnCode == "500"){
            //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
            sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
            this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 500;
            this.navCtrl.push(PaymentSuccessPage,{"Result":4,"Data":Resp.GetPaymentDetailsResult.Data});
          }else{
            sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
            this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 400;
            //this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
          }
        });
  
      }

      presentLoadingDefault() {
        this.loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        this.loading.present();
      }

    showToast(Message){
      if(this.canShowToast){
        let pageToast = this.toast.create({
          message:Message,
          showCloseButton: true,
          closeButtonText: 'Ok',
          dismissOnPageChange: true,
          position: "bottom",
          duration: 5000,
        });
        this.canShowToast = false;
        pageToast.present();
        pageToast.onDidDismiss(() => {
          this.canShowToast = true;
        });
      }   
    }

    onBackClick(){
      sessionStorage.isViewPopped = true;
      sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
      sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
      this.navCtrl.pop();
    }

}
