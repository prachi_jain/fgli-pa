import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import { DashboardPage } from '../dashboard/dashboard';
import { BuyPage } from '../buy/buy';
import { MarketingColateralPage } from '../marketing-colateral/marketing-colateral';
import { HospitalLocatorPage } from '../hospital-locator/hospital-locator';
import { FaqPage } from '../faq/faq';
import { KnowledgebasePage } from '../knowledgebase/knowledgebase';
import { ProductLandingPage } from '../../product-landing/product-landing';
/**
 * Generated class for the LandingScreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    LandingScreenPage,
  ],
  imports: [
    //IonicPageModule.forChild(LandingScreenPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-landing-screen',
  templateUrl: 'landing-screen.html',
})
export class LandingScreenPage {

  public static readonly pageName = 'LandingScreenPage';
  userPosData;
  preLogin;
  preLoginQuickQuoteData;
  preloginNormal;
  page;
  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform) {
    // platform.registerBackButtonAction(() => {
    //   this.backButtonFunction();
    // });

    this.page = navParams.get("page");
    console.log("page" + this.page);
    
    this.preLogin = navParams.get("preLoginQuickQuote");
    this.preLoginQuickQuoteData = navParams.get("preLoginQuickQuoteData");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingScreenPage');
    //let userData = JSON.parse(sessionStorage.loginUserData);
    this.userPosData = this.navParams.get("userData");
    this.preloginNormal = this.navParams.get("preloginNormal");
    console.log("UserPosData: " + JSON.stringify(this.userPosData));
    //console.log("userData: " + JSON.stringify(userData));
     
  }
  
  redirectTo(Num){
    if(Num == 1){
      this.navCtrl.push(ProductLandingPage,{userPosData: this.userPosData,preLogin:this.preLogin,preLoginQuickQuoteData:this.preLoginQuickQuoteData,preloginNormal:this.preloginNormal, "page" : this.page});    
    }else if(Num == 2){
      this.navCtrl.push(DashboardPage,{});
    }else if(Num == 3){
      this.navCtrl.push(KnowledgebasePage, {});
    }else if(Num == 4){
      this.navCtrl.push(FaqPage, {});
    }else if(Num == 5){
      this.navCtrl.push(HospitalLocatorPage, {gotoWhichPage: "hospital"});
    }else if(Num == 6){
      this.navCtrl.push(HospitalLocatorPage, {gotoWhichPage: "branch"});
    }
  }
}
