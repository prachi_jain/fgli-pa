import { Component, NgModule } from '@angular/core';
import * as $ from 'jquery';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BuyPage } from '../buy/buy';

/**
 * Generated class for the ProductBenefitsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    ProductBenefitsPage,
  ],
  imports: [
    //IonicPageModule.forChild(BuyPageResultPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-product-benefits', 
  templateUrl: 'product-benefits.html',
})
export class ProductBenefitsPage { 
  pet = "All";
  vitalCon = false;
  supCon = true;
  preCon = true; 
  popupShowHide = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductBenefitsPage');

    document.getElementById("splash").style.display='none';

    $(".accordionWrapper h4").click(function(){ 
    	$(this).toggleClass('active');
    	$(".accordionWrapper h4").not($(this)).removeClass('active');
    	$(this).next(".accordContents").slideToggle();
    	$(".accordionWrapper .accordContents").not($(this).next()).slideUp();
    });		
    $(".vitalPopup").click(function(){
      this.pet= "All";  
      this.popupShowHide = false;
      var ex = $(this).attr('rel');
      $('#pbpopup'+ex).show();  
      $(".transPopup").show();      
    });
    
    // $(".closeBtn").click(function(){ 
    //   this.pet= "All";    
    //   this.popupShowHide = true;
    //   $(".popupContent").hide();
    //   $(".transPopup").hide();
    // });  
  }
  vitalclick(){
    this.vitalCon = false;
    this.supCon = true;
    this.preCon = true; 
  }

  supclick(){
    this.vitalCon = true;
    this.supCon = false;
    this.preCon = true;
  }
  preclick(){
    this.vitalCon = true;
    this.supCon = true;
    this.preCon = false;
  }

  closePop(){
    this.pet= "All";       
    this.popupShowHide = true;
  }

  showPopUp(){
    this.popupShowHide = false;
  }

  goToBuyPage(){
    this.navCtrl.push(BuyPage);
  }
}
