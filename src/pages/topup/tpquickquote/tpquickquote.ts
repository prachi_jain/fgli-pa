import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { httpFactory } from '@angular/http/src/http_module';
import { TpquickquoteResultPage } from '../tpquickquote-result/tpquickquote-result';
import { INTERNAL_BROWSER_DYNAMIC_PLATFORM_PROVIDERS } from '@angular/platform-browser-dynamic/src/platform_providers';

/**
 * Generated class for the TpquickquotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    TpquickquotePage,
  ],
  imports: [
    //IonicPageModule.forChild(TpquickquotePage),
  ],
})


@IonicPage()
@Component({
  selector: 'page-tpquickquote',
  templateUrl: 'tpquickquote.html',
})
export class TpquickquotePage {
  myMember = [{ "id": "SELF", "imgPath": "assets/imgs/self.png" }, { "id": "SPOU", "imgPath": "assets/imgs/spouse.png" },
  { "id": "SON", "imgPath": "assets/imgs/son.png" }, { "id": "DAUG", "imgPath": "assets/imgs/daughterIcon.png" },
  { "id": "FATH", "imgPath": "assets/imgs/self.png" }, { "id": "MOTH", "imgPath": "assets/imgs/self.png" }, { "id": "GRCH", "imgPath": "assets/imgs/self.png" },
  { "id": "SIB", "imgPath": "assets/imgs/self.png" }, { "id": "CHLD", "imgPath": "assets/imgs/self.png" }, { "id": "GRPA", "imgPath": "assets/imgs/self.png" }]
  vitalMembers = ["SELF", "SPOU", "SON", "SON1", "SON2", "SON3", "DAUG", "DAUG1", "DAUG2", "DAUG3", "FATH", "MOTH", "CHLD"];
  showTransBaground = true;
  hideMemberPopup = false;
  showFamilyMemberOnScreen = false;
  isActive = true;
  membersShow = [];
  childAgeGroup = [];
  QuickQuotePlans = [];
  memberCodes = [];
  sonArray = [];
  daughterArray = [];
  sonShowArray = [];
  daughterShowArray = [];
  showChildPopup = false;
  forQuickQuote; fromQuickQuoteResult;
  appData; apiMembers; apiAgeGroup;
  slide = '';
  maximumInsuAmtCode = '';
  showMemberPopup = false;
  insuredCode = '';
  childsAge = "";
  memberAge = "";
  memberSmokes = false;
  popupImg;
  membersInsured = [];
  childArray = [];
  popupTitle = "";
  popupMember = "SELF";
  childCount = 0;
  childCount1 = 0;
  popupMemberData = { "code": "" };
  public hidePopupSmokerDiv: boolean = false;
  public hidePopupMarriedDiv: boolean = false;
  showInsurenceDiv: boolean = false;
  PolicyTypeSelected = "familyFloaterCode";
  canShowToast = true;
  familyFloaterCode = '';
  editUserData = false;
  individualCode = '';
  marriedStatus = "";
  hideDisableCards = true;
  disableProceedBt = true;
  hideProceedButton = true;
  familyInsuredPlanType = '';
  familyInsuredPlanTypeText = '';
  insuredPlanTitle = '';
  addMember = false;
  insurenceAmtToShow = [];
  familyInsurenceAmt = [];
  memberShowCodes = [];
  shouldAddmember = false;
  loginStatus; deductableData; siAmount; sumInsuredListToPass;
  marriedStatusArray = [{ value: "Single", status: false }, { value: "Married", status: true }, { value: "Divorced", status: false }, { value: "Widow / Widower", status: false }];

  constructor(public navCtrl: NavController, public navParams: NavParams, public appService: AppService, public toast: ToastController) {
    this.appData = JSON.parse(localStorage.AppData);
    this.apiMembers = this.appData.Masters.Members;
    this.apiAgeGroup = this.appData.Masters.Age;
    this.deductableData = navParams.get("deductableData");
    this.siAmount = navParams.get("siAmount");
    this.sumInsuredListToPass = navParams.get("sumInsuredListToPass");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TpquickquotePage');
    console.log("deductableData", this.deductableData);
    console.log("siAmount", this.siAmount);
    console.log("sumInsuredList", this.sumInsuredListToPass)
    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 7000);
    console.log('myMember', this.myMember);
    console.log('membersShow', this.membersShow);
    console.log('apiMembers', this.apiMembers);
    // console.log('apiPlanType', this.apiPlanType);
    console.log('apiAgeGroup', this.apiAgeGroup);
    // console.log('policyTypes', this.policyTypes);
    this.membersArrayFunction();
    this.childAgeRange();
    this.populateInsurenceAmt(0, "SELF");
    console.log("membersInsured.length", this.membersInsured.length);
    console.log("forQuickQuote", this.forQuickQuote);
  }
  genderClick() {
    this.isActive = !this.isActive;
  }
  displayAge(agecode) {
    for (let i = 0; i < this.apiAgeGroup.length; i++) {
      if (agecode == this.apiAgeGroup[i].ageCode) {
        return this.apiAgeGroup[i].title + " years";
      }
    }
  }
  displayAmt(insuranceCode) {
    for (let i = 0; i < this.QuickQuotePlans.length; i++) {
      if (this.maximumInsuAmtCode != '' && insuranceCode == this.QuickQuotePlans[i].sicode) {
        this.insuredPlanTitle = (this.QuickQuotePlans[i].title).toLowerCase();
      } else if (this.PolicyTypeSelected == this.familyFloaterCode && insuranceCode == this.QuickQuotePlans[i].sicode) {
        this.insuredPlanTitle = (this.QuickQuotePlans[i].title).toLowerCase();
        this.familyInsuredPlanType = this.QuickQuotePlans[i].plancode;
      }
      if (insuranceCode == this.QuickQuotePlans[i].sicode) {
        return this.QuickQuotePlans[i].amtSelectedTitle;
      }
    }
  }
  clearMemberDetails(event: Event, member) {
    event.stopPropagation();
    var indexSon = 0;
    var indexDaughter = 0;
    var indexShowSon = 0;
    var indexChild = 0;
    var indexShowDaughter = 0;
    var deleteDetails = true;
    this.sonArray = [];
    this.daughterArray = [];
    this.sonShowArray = [];
    this.daughterShowArray = [];
    console.log("member", member);
    console.log("membersShow", this.membersShow);
    console.log("membersInsured", this.membersInsured);
    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].code == "SON") {
        this.sonArray.push(this.membersInsured[i]);
        indexSon = this.sonArray.findIndex(x => x.title === member.title);
      }
      if (this.membersInsured[i].code == "DAUG") {
        this.daughterArray.push(this.membersInsured[i]);
        indexDaughter = this.daughterArray.findIndex(x => x.title === member.title);
      }
    }
    for (let i = 0; i < this.membersShow.length; i++) {
      if (this.membersShow[i].code == "SON") {
        this.sonShowArray.push(this.membersShow[i]);
        indexShowSon = this.sonShowArray.findIndex(x => x.title === member.title);
      }
      if (this.membersShow[i].code == "DAUG") {
        this.daughterShowArray.push(this.membersShow[i]);
        indexShowDaughter = this.daughterShowArray.findIndex(x => x.title === member.title);
      }
    }
    for (let i = 0; i < this.membersShow.length; i++) {
      if (member.code == "SELF") {
        this.membersShow[i].detailsSaved = false;
        this.membersShow[i].smoking = false;
        member.insuredAmtText = "";
        member.ageText = "";
        member.smokingText = "";
        this.maximumInsuAmtCode = '';
        this.marriedStatus = "";
        member.maritalCode = "";
        this.insuredPlanTitle = '';
        deleteDetails = true;
        break;
      } else if (member.code == this.membersShow[i].code && member.title == this.membersShow[i].title) {
        if (this.sonArray.length >= 1 && indexSon != -1) {
          if (indexSon == this.sonArray.length - 1) {
            deleteDetails = true;
          } else {
            deleteDetails = false;
            this.showToast("You cannot delete " + member.title)
          }
        }
        if (this.daughterArray.length >= 1 && indexDaughter != -1) {
          if (indexDaughter == this.daughterArray.length - 1) {
            deleteDetails = true;
          } else {
            deleteDetails = false;
            this.showToast("You cannot delete " + member.title)
          }
        }
        if (deleteDetails) {
          this.membersShow[i].detailsSaved = false;
          this.membersShow[i].smoking = false;
          member.insuredAmtText = "";
          member.ageText = "";
          member.smokingText = "";
          member.maritalStatus = "";
          break;

        }
      }
    }
    if (deleteDetails) {
      this.editUserData = false;
      for (let i = 0; i < this.membersInsured.length; i++) {
        if (member.title == this.membersInsured[i].title) {
          if (member.code == "SON") {
            if (member.title == "Son1" || member.title == "Son") {
              this.membersInsured.splice(i, 1);
              for (let j = 0; j < this.membersShow.length; j++) {
                if (member.title == this.membersShow[j].title) {
                  if (this.membersShow[j + 1].code == "SON") {
                    this.membersShow.splice(j + 1, 1);
                  }
                }
              }
              break;
            } else {
              for (let j = 0; j < this.membersShow.length; j++) {
                if (member.title == this.membersShow[j].title) {
                  if (this.membersShow[j + 1].code == "SON") {
                    this.membersShow.splice(j, 2);
                  } else {
                    this.membersShow.splice(j, 1);
                  }
                }
              }
              this.membersInsured.splice(i, 1);
              break;
            }
          } else if (member.code == "DAUG") {
            if (member.title == "Daughter" || member.title == "Daughter1") {
              this.membersInsured.splice(i, 1);
              for (let j = 0; j < this.membersShow.length; j++) {
                if (member.title == this.membersShow[j].title) {
                  if (this.membersShow[j + 1].code == "DAUG") {
                    this.membersShow.splice(j + 1, 1);
                  }
                }
              }
              break;
            } else {
              for (let j = 0; j < this.membersShow.length; j++) {
                if (member.title == this.membersShow[j].title) {
                  if (this.membersShow[j + 1].code == "DAUG") {
                    this.membersShow.splice(j, 2);
                  } else {
                    this.membersShow.splice(j, 1);
                  }
                }
              }
              this.membersInsured.splice(i, 1);
              break;
            }
          } else {
            this.membersInsured.splice(i, 1);
            break;
          }
        }
      }
    }
    if (this.membersInsured.length < 1) {
      this.hideProceedButton = true;
      this.disableProceedBt = true;
    }
    console.log(this.membersInsured)
  }
  populateInsurenceAmt(Number, memberCode) {
    this.insurenceAmtToShow = [];
    for (let i = 0; i < this.QuickQuotePlans.length; i++) {
      if (this.maximumInsuAmtCode != '' && this.QuickQuotePlans[i].sicode == this.maximumInsuAmtCode) {
        this.insurenceAmtToShow.push(this.QuickQuotePlans[i]);
        this.insurenceAmtToShow = this.insurenceAmtToShow.slice(this.insurenceAmtToShow.length - 3, this.insurenceAmtToShow.length);
        break;
      } else {
        this.insurenceAmtToShow.push(this.QuickQuotePlans[i]);
      }
      if (Number == "0" || Number == 0) {
        this.familyInsurenceAmt.push(this.QuickQuotePlans[i]);
      }
    }
    console.log("InsuranceAmount: " + this.familyInsurenceAmt);
  }
  editMemberDetails($event, member) {
    console.log("edit")
    console.log("membersInsured.length", this.membersInsured.length);
    // for(let i = 0; i <this.membersInsured.length; i++){
    //     if(this.membersInsured[i].code == member.code){
    //     var number_of_elements_to_remove = 1;
    //     var removed_elements = this.membersInsured.splice(i, number_of_elements_to_remove);
    //     console.log(this.membersInsured)
    //     }
    //     }
    event.stopPropagation();
    this.slide = member.insuredCode;
    this.populateInsurenceAmt(1, member.code);

    this.editUserData = true;
    this.addMember = false;
    this.memberSmokes = false;
    this.showTransBaground = false;
    this.popupTitle = member.title;
    this.popupImg = member.imgUrl;
    this.hideMemberPopup = true;
    this.popupMemberData = member;
    this.memberAge = member.age;
    this.hidePopupSmokerDiv = false;
    this.hidePopupMarriedDiv = false;
    if (member.code == "SELF") {
      this.hidePopupSmokerDiv = true;
      this.hidePopupMarriedDiv = true;
      this.memberSmokes = member.smoking;
      if (member.maritalCode == "M") {
        //this.memberMarried = true;
      }
    }
    if (member.code == "SON" || member.code == "DAUG") {
      this.showTransBaground = false;
      this.hideMemberPopup = false;
      this.addMember = true;
      this.popupMember = "Child"
      this.showChildPopup = true;
      this.childsAge = member.age;
    } else if (member.code == "SPOU") {
      this.hidePopupSmokerDiv = true;
      this.showTransBaground = false;
      this.hideMemberPopup = true;
      this.memberSmokes = member.smoking;
    } else if (member.code == "FATH") {
      this.hidePopupSmokerDiv = true;
      this.showTransBaground = false;
      this.hideMemberPopup = true;
      this.memberSmokes = member.smoking;
    } else if (member.code == "MOTH") {
      this.hidePopupSmokerDiv = true;
      this.showTransBaground = false;
      this.hideMemberPopup = true;
      this.memberSmokes = member.smoking;
    }
  }
  getQuoteClicked() {
    // sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.membersInsured);
    // sessionStorage.MembersShownOnforQuickQuote = JSON.stringify(this.membersShow);
    this.memberCodes = [];
    this.childCount = 0;
    var maritalStatusSelf = "";
    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].code == "SON" || this.membersInsured[i].code == "DAUG") {
        this.childCount++;
      }
      if (this.membersInsured[i].code == "SELF") {
        maritalStatusSelf = this.membersInsured[i].maritalStatus;
      }
      this.memberCodes.push(this.membersInsured[i].code)
    }
    console.log("childCount", this.childCount);
    console.log("membersInsured", this.membersInsured);
    let self = (this.memberCodes).some(x => x == "SELF");
    let spou = (this.memberCodes).some(x => x == "SPOU");
    let moth = (this.memberCodes).some(x => x == "MOTH");
    let fath = (this.memberCodes).some(x => x == "FATH");
    console.log(self, spou, moth, fath);
    if (this.childCount > 1) {
      if (self && spou) {
        if (maritalStatusSelf == "Single") {
          this.showToast("As your marital status is single, you cannot fill spouse details.");
        } else if (maritalStatusSelf == "Divorced") {
          this.showToast("As your marital status is divorced, you cannot fill spouse details.");
        } else if (maritalStatusSelf == "Widow / Widower") {
          this.showToast("As your marital status is widow / widower, you cannot fill spouse details.");
        } else if (this.childCount > 5) {
          this.showTransBaground = true;
          this.hideMemberPopup = false;
          this.showChildPopup = false;
          this.showToast("You can insure maximum 5 child with this Plan.");
        } else if (this.childCount == 4) {
          this.showToast("You cannot insure 4 children");
        } else {
          this.navCtrl.push(TpquickquoteResultPage, { "loginStatus": this.loginStatus, deductableData: this.deductableData, siAmount: this.siAmount, sumInsuredListToPass: this.sumInsuredListToPass, childCount: this.childCount, membersInsured: this.membersInsured });
        }
      } else if (self || spou) {
        if (this.childCount > 3) {
          this.showTransBaground = true;
          this.hideMemberPopup = false;
          this.showChildPopup = false;
          this.showToast("You can insure maximum 3 child with this Plan.");
        } else if (this.childCount == 4) {
          this.showToast("You cannot insure 4 children ");
        } else {
          this.navCtrl.push(TpquickquoteResultPage, { "loginStatus": this.loginStatus, deductableData: this.deductableData, siAmount: this.siAmount, sumInsuredListToPass: this.sumInsuredListToPass, childCount: this.childCount, membersInsured: this.membersInsured });
        }
      } else {
        this.showToast("Only one child is allowed");
      }
    } else if ((this.childCount <= 1) && (self && spou)) {
      // if (self && spou) {
      if (maritalStatusSelf == "Single") {
        this.showToast("As your marital status is single, you cannot fill spouse details.");
      } else if (maritalStatusSelf == "Divorced") {
        this.showToast("As your marital status is divorced, you cannot fill spouse details.");
      } else if (maritalStatusSelf == "Widow / Widower") {
        this.showToast("As your marital status is widow / widower, you cannot fill spouse details.");
      } else if (this.childCount > 5) {
        this.showTransBaground = true;
        this.hideMemberPopup = false;
        this.showChildPopup = false;
        this.showToast("You can insure maximum 5 child with this Plan.");
      } else if (this.childCount == 4) {
        this.showToast("You cannot insure 4 children");
      } else {
        this.navCtrl.push(TpquickquoteResultPage, { "loginStatus": this.loginStatus, deductableData: this.deductableData, siAmount: this.siAmount, sumInsuredListToPass: this.sumInsuredListToPass, membersInsured: this.membersInsured, childCount: this.childCount });
      }
      // }
    } else {
      this.navCtrl.push(TpquickquoteResultPage, { "loginStatus": this.loginStatus, deductableData: this.deductableData, siAmount: this.siAmount, sumInsuredListToPass: this.sumInsuredListToPass, membersInsured: this.membersInsured, childCount: this.childCount });
    }

  }
  saveMemberDetails(popupNumber, addAnother) {
    // this.hideDisableCards = false;
    console.log(popupNumber, addAnother);
    console.log("membersInsured", this.membersInsured);
    console.log("membershow", this.membersShow);
    console.log(this.memberAge);
    let ageValue = this.displayAge(this.memberAge);
    if (this.popupMemberData.code == "SELF" || this.popupMemberData.code == "SPOU" || this.popupMemberData.code == "MOTH" || this.popupMemberData.code == "FATH") {
      if (ageValue == "0-17 years") {
        this.hideProceedButton = true;
        this.disableProceedBt = true;
        this.showToast("You cannot select age below 17 years.");
      }
    }
    console.log(this.editUserData + " <-this.editUserData");
    console.log("this.familyInsuredPlanTypeText", this.familyInsuredPlanTypeText)
    console.log("popupNumber", popupNumber);
    console.log("popupmemberData", this.popupMemberData);

    if (popupNumber == 1 || popupNumber == 2) {
      if (this.slide == '' && this.showInsurenceDiv) {
        this.showToast("Please select insurance amount.");
      } else if ((this.memberAge == "") && popupNumber == 1) {
        this.showToast("Please select member's age.");
      } else if (this.childsAge == "" && popupNumber == 2) {
        this.showToast("Please select member's age.");
      } else if (this.popupMemberData.code == "SELF" && this.marriedStatus == "") {
        this.showToast("Please select marital status");
      } else {
        this.showTransBaground = false;
        if (this.popupMemberData.code == "SELF") {
          this.maximumInsuAmtCode = this.slide;
          this.membersShow[0].detailsSaved = true;
          this.membersShow[0].age = this.memberAge;
          this.membersShow[0].ageText = this.displayAge(this.memberAge);
          this.membersShow[0].insuredCode = this.slide;
          this.membersShow[0].PolicyType = this.PolicyTypeSelected;
          this.membersShow[0].insuredAmtText = this.displayAmt(this.slide);
          this.membersShow[0].smoking = this.memberSmokes;
          this.membersShow[0].insuredAmtInDigit = "";

          this.hideProceedButton = false;
          this.disableProceedBt = false;
          if (this.memberSmokes) {
            this.membersShow[0].smokingText = "Smoking";
          } else {
            this.membersShow[0].smokingText = "";
          }
          // if(this.memberMarried){
          //   this.membersShow[0].maritalStatus = "Married";
          //   this.membersShow[0].maritalCode = "M"
          //   this.hidePopupSmokerDiv = true;
          //   document.getElementById("addMember").style.marginBottom='5px';
          //   this.hideProceedButton = true;
          //   this.membersShow[0].showMarried = true;
          // }else{
          //   document.getElementById("addMember").style.marginBottom='60px';
          //   this.hideProceedButton = false;
          //   this.membersShow[0].maritalStatus = ""; 
          //   this.hidePopupSmokerDiv = false;
          //   this.membersShow[0].maritalStatus = "Single";
          //   this.membersShow[0].maritalCode = "S";
          //   this.membersShow[0].showMarried = false;
          // }
          if (this.marriedStatus == "Married") {
            this.membersShow[0].maritalStatus = "Married";
            this.membersShow[0].maritalCode = "M"
            this.hidePopupSmokerDiv = true;
            //document.getElementById("addMember").style.marginBottom='5px';
            //this.hideProceedButton = true;
            this.membersShow[0].showMarried = true;

            //this.popupImg = memberData.imgUrl;
            //this.popupMemberData = memberData;

          } else if (this.marriedStatus == "Single" || this.marriedStatus == "Divorced" || this.marriedStatus == "Widow / Widower") {
            //document.getElementById("addMember").style.marginBottom='60px';
            // this.hideProceedButton = false;
            this.membersShow[0].maritalStatus = "";
            this.hidePopupSmokerDiv = false;
            if (this.marriedStatus == "Single") {
              this.membersShow[0].maritalStatus = "Single";
              if (this.membersInsured.length == 1) {
                // this.hideProceedButton = true;
              }
              //document.getElementById("addMember").style.marginBottom='5px';             
            } else if (this.marriedStatus == "Divorced") {
              //document.getElementById("addMember").style.marginBottom='5px';
              // this.hideProceedButton = true;
              this.membersShow[0].maritalStatus = "Divorced";
            } else if (this.marriedStatus == "Widow / Widower") {
              //document.getElementById("addMember").style.marginBottom='5px';
              // this.hideProceedButton = true;
              this.membersShow[0].maritalStatus = "Widow / Widower";
            }
            for (let j = 0; j < this.membersInsured.length; j++) {
              if (this.membersInsured[j].code.includes("SPOU")) {
                this.membersInsured.splice(j, 1);
                break;
              }
            }
            for (let j = 0; j < this.membersShow.length; j++) {
              if (this.membersShow[j].code.includes("SPOU")) {
                this.membersShow[j].detailsSaved = false;
                this.membersShow[j].smoking = false;
                this.membersShow[j].insuredAmtText = "";
                this.membersShow[j].ageText = "";
                this.membersShow[j].smokingText = "";
                this.membersShow[j].maritalStatus = "";
                break;
              }
            }
            this.membersShow[0].maritalCode = "S";
            this.membersShow[0].showMarried = false;
          }
          this.slide = '';
          this.memberAge = "";
          this.memberSmokes = false;
          //this.memberMarried = false;
          //this.showMemberPopup = false;
          this.showChildPopup = false;
          this.hidePopupMarriedDiv = false;
          this.hideMemberPopup = false;
          console.log(this.membersInsured);
          // if(this.editUserData)
          // {
          //   console.log("this.editUserData "+this.editUserData+"from saveMemberDetails");
          //   this.membersInsured.splice(0, 1, this.membersShow[0]);

          //   if(  (this.membersInsured.length >= 2 && this.PolicyTypeSelected == this.familyFloaterCode)|| ( this.membersInsured.length >= 1 &&  this.PolicyTypeSelected == this.individualCode)){
          //     this.disableProceedBt = false;
          //     this.hideProceedButton =false;
          //   }else{
          //     this.disableProceedBt = true;
          //   }

          // }else{
          //   this.membersInsured.push(this.membersShow[0]);
          // }
          if (this.editUserData) {
            this.membersInsured.splice(0, 1, this.membersShow[0]);
            // for(let i = 0; i <this.membersInsured.length; i++){
            // if(this.membersInsured[i].code == this.popupMemberData.code){
            // var number_of_elements_to_remove = 1;
            // var removed_elements = this.membersInsured.splice(i, number_of_elements_to_remove);
            // this.membersInsured.push(this.membersInsured[i]);
            // console.log(this.membersInsured)
            //}
            //}
          } else {
            this.membersInsured.splice(0, 0, this.membersShow[0]);
          }

          this.showTransBaground = true;
          // if (this.PolicyTypeSelected == this.individualCode) {
          //   this.disableProceedBt = false;
          // }
          // if (this.membersInsured.length == 1) {
          //   this.hideProceedButton == true;
          // }
          this.editUserData = false;
        } else {
          for (let i = 0; i < this.membersShow.length; i++) {
            if (this.popupMemberData.code == this.membersShow[i].code) {
              this.membersShow[i].smoking = this.memberSmokes;
              if (this.memberSmokes) {
                this.membersShow[i].smokingText = "Smoking";
              } else {
                this.membersShow[i].smokingText = "";
              }
              // this.saveMemberData(i, popupNumber, addAnother);

              this.hideProceedButton = false;
              this.disableProceedBt = false;
              if (this.membersShow[i].title == this.popupTitle) {
                console.log(this.editUserData);
                this.saveMemberData(i, popupNumber, addAnother);
                break;
              }
            }
          }
        }

      }
    }
  }
  saveMemberData(indexNumber, popupNumber, addAnother) {
    console.log(this.membersInsured);
    console.log(this.editUserData)
    var allowAddMember = true;
    this.shouldAddmember = false;
    this.membersShow[indexNumber].detailsSaved = true;
    if (popupNumber == 1) {
      this.membersShow[indexNumber].age = this.memberAge;
      this.membersShow[indexNumber].ageText = this.displayAge(this.memberAge);
    } else {
      this.membersShow[indexNumber].age = this.childsAge;
      this.membersShow[indexNumber].ageText = this.displayAge(this.childsAge);
    }
    this.membersShow[indexNumber].insuredCode = this.slide;
    this.membersShow[indexNumber].insuredAmtText = this.displayAmt(this.slide);
    this.membersShow[indexNumber].PolicyType = this.PolicyTypeSelected;
    this.hideMemberPopup = false;
    this.showMemberPopup = false;
    this.showChildPopup = false;
    this.hidePopupSmokerDiv = false;
    this.hidePopupMarriedDiv = false;
    this.slide = '';
    this.memberAge = "";
    this.childsAge = "";
    console.log(indexNumber);
    if (this.membersInsured.length > 0) {
      for (let j = 0; j < this.membersInsured.length; j++) {
        var indexSpouse = this.membersInsured.findIndex(x => x.code === "SPOU");
      }
    }
    if (this.membersShow[indexNumber].code == 'SPOU') {
      if (this.editUserData) {
        console.log(indexSpouse);
        this.membersInsured.splice(indexSpouse, 1, this.membersShow[indexNumber]);
      } else {
        this.membersInsured.push(this.membersShow[indexNumber]);
      }

    } else {
      if (this.editUserData) {
        this.shouldAddmember = true;
        for (let i = 1; i < this.membersInsured.length; i++) {
          if (this.membersShow[indexNumber].code == this.membersInsured[i].code && this.membersShow[indexNumber].title == this.membersInsured[i].title) {
            this.membersInsured.splice(i, 1, this.membersShow[indexNumber]);
            break;
          }
        }
      } else {
        this.membersInsured.push(this.membersShow[indexNumber]);
      }

    }
    this.childCount = 0;
    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].code == "SON" || this.membersInsured[i].code == "DAUG") {
        this.childCount++;
      }
    }
    this.childCount1 = 0;
    for (let i = 0; i < this.membersShow.length; i++) {
      if (this.membersShow[i].code == "SON" || this.membersShow[i].code == "DAUG") {
        this.childCount1++;
      }
    }
    console.log(this.membersShow[indexNumber]);
    if (!this.shouldAddmember) {
      for (let l = 0; l < this.membersShow.length; l++) {
        if (this.membersShow[indexNumber].code == this.membersShow[l].code && this.membersShow[indexNumber].title == this.membersShow[l].title) {
          if (!this.membersShow[l].detailsSaved) {
            this.showToast("You cannot insure child " + this.membersShow[indexNumber].title + " ,First fill " + this.membersShow[l].title);
            this.membersShow[indexNumber].detailsSaved = false;
            this.membersShow[indexNumber].smoking = false;
            this.membersShow[indexNumber].insuredAmtText = "";
            this.membersShow[indexNumber].ageText = "";
            this.membersShow[indexNumber].smokingText = "";
            this.membersShow[indexNumber].maritalStatus = "";
            allowAddMember = false;
            break;
          }
        }
      }
    }
    if (addAnother == 1) {
      console.log(this.childCount)
      console.log("membersShow", this.membersShow);
      // if (this.childCount < 5) {

      if (this.childCount >= 5 || this.childCount1 >= 5) {
        allowAddMember = false;
        this.showToast("You can insure maximum 5 child with this Plan.")
      }
      if (allowAddMember) {
        this.addMemberClicked2(this.popupMemberData.code);
      }
    }
    this.showTransBaground = true;
    this.editUserData = false;
  }
  addMemberClicked(event: Event, k) {
    event.stopPropagation();
    console.log(event);
    this.childCount = 0;
    for (let i = 0; i < this.membersShow.length; i++) {
      if (this.membersShow[i].code == "SON" || this.membersShow[i].code == "DAUG") {
        this.childCount++;
      }
    }
    if (this.childCount >= 5) {
      this.showToast("You can insure maximum 5 child with this Plan.");
    } else {
      for (let i = 0; i < this.membersShow.length; i++) {
        if (this.membersShow[i].code == k.code && (k.code == "SON" || k.code == "DAUG")) {
          let term = this.membersShow.filter(person => person.code == k.code);
          let memberName = '';
          if (k.code == "SON") {
            memberName = 'Son';
            this.membersShow[i].title = 'Son1'
          } else if (k.code == "DAUG") {
            memberName = 'Daughter';
            this.membersShow[i].title = 'Daughter1'
          }
          var checkIsMemberEmpty = false;
          for (let j = 0; j < term.length; j++) {
            console.log("term[j].detailsSaved" + term[j].detailsSaved);

            if (!term[j].detailsSaved) {
              console.log("if" + j);

              this.showToast("First fill details of " + term[j].title);
              checkIsMemberEmpty = true;
              break;
            }
          }
          if (!checkIsMemberEmpty) {
            this.membersShow.splice(i + term.length, 0, {
              "code": this.membersShow[i].code,
              "title": memberName + (term.length + 1),
              "imgUrl": k.imgUrl,
              "smoking": false,
              "showDependent": true,
              "showAddAnotherBtn": true,
              "smokingText": "",
              "showMarried": false,
              "maritalStatus": "",
              "age": "",
              "ageText": "",
              "insuredCode": "",
              "insuredAmtText": "",
              "PolicyType": '',
              "disableRemove": false,
              "ShowInsurenceAmt": false,
              "showFamilyMemberOnScreen": true,
              "popupType": "2",
              "detailsSaved": false
            });
          }
          break;
        }
      }
    }
  }
  addMemberClicked2(memberCode) {
    for (let i = 0; i < this.membersShow.length; i++) {

      if (this.membersShow[i].code == memberCode && (memberCode == "SON" || memberCode == "DAUG")) {
        let memberName = '';
        let term = this.membersShow.filter(person => person.code == memberCode);
        if (memberCode == "SON") {
          memberName = 'Son';
          this.membersShow[i].title = 'Son1'
        } else if (memberCode == "DAUG") {
          memberName = 'Daughter';
          this.membersShow[i].title = 'Daughter1'
        }
        let checkIsMemberEmpty = false;
        for (let j = 0; j < term.length; j++) {
          console.log("term[j].detailsSaved" + term[j].detailsSaved);

          if (!term[j].detailsSaved) {
            console.log("if" + j);

            this.showToast("First fill details of " + term[j].title);
            checkIsMemberEmpty = true;
            break;
          }
        }
        if (!checkIsMemberEmpty) {
          this.membersShow.splice(i + term.length, 0, {
            "code": this.membersShow[i].code,
            "title": memberName + (term.length + 1),
            "imgUrl": term[0].imgUrl,
            "smoking": false,
            "showDependent": true,
            "showAddAnotherBtn": true,
            "smokingText": "",
            "showMarried": false,
            "maritalStatus": "",
            "age": "",
            "ageText": "",
            "insuredCode": "",
            "insuredAmtText": "",
            "PolicyType": '',
            "disableRemove": false,
            "ShowInsurenceAmt": false,
            "showFamilyMemberOnScreen": true,
            "popupType": "2",
            "detailsSaved": false
          });
        }
        break;
        // this.membersShow.splice(i + term.length, 0, {
        //   "code": this.membersShow[i].code,
        //   "title": memberName + (term.length + 1),
        //   "imgUrl": term[0].imgUrl,
        //   "smoking": false,
        //   "showDependent": true,
        //   "showAddAnotherBtn": true,
        //   "smokingText": "",
        //   "showMarried": false,
        //   "maritalStatus": "",
        //   "age": "",
        //   "ageText": "",
        //   "insuredCode": "",
        //   "insuredAmtText": "",
        //   "PolicyType": '',
        //   "disableRemove": false,
        //   "ShowInsurenceAmt": false,
        //   "showFamilyMemberOnScreen": true,
        //   "popupType": "2",
        //   "detailsSaved": false
        // });
        // break;
      }
    }
  }
  fillData(memberData) {
    console.log('memberData', memberData);
    console.log('membersInsured', this.membersInsured)
    console.log("membersInsured", this.membersInsured.length);
    console.log("Child" + this.childAgeGroup);
    console.log("edituserData", this.editUserData);
    this.slide = '';
    if (memberData.code == "SELF") {
      if (this.membersInsured.length == 0) {
        console.log("self membersInsured 0")
        this.showTransBaground = false;
        this.hideMemberPopup = true;
        this.hidePopupSmokerDiv = true;
        this.hidePopupMarriedDiv = true;
      } else {
        console.log("self membersInsured !0")
        if (this.membersInsured[0].code == "FATH" || this.membersInsured[0].code == "MOTH") {
          this.hideMemberPopup = false;
          this.showToast("You cannot fill self and parent details");
        } else {
          this.showTransBaground = false;
          this.hideMemberPopup = true;
          this.hidePopupSmokerDiv = true;
          this.hidePopupMarriedDiv = true;
        }
      }
    } else if (memberData.code != "SELF") {
      this.hidePopupMarriedDiv = false;
      if (this.membersInsured.length == 0) {
        console.log("membersInsured 0", memberData.code);
        this.showTransBaground = false;
        this.hideMemberPopup = true;
        this.hidePopupMarriedDiv = false;
        if (memberData.code == "SON" || memberData.code == "DAUG") {
          this.showTransBaground = false;
          this.showChildPopup = true;
          this.hidePopupSmokerDiv = false;
        } else if (memberData.code == "SPOU") {
          this.hidePopupSmokerDiv = true;
        } else if (memberData.code == "FATH" || memberData.code == "MOTH") {
          this.hidePopupSmokerDiv = true;
        }
      } else {
        console.log(memberData.code, this.membersInsured.length);
        if (this.membersInsured[0].code == "FATH" || this.membersInsured[0].code == "MOTH") {
          if (memberData.code == "SON" || memberData.code == "DAUG") {
            this.showChildPopup = false;
            this.hideMemberPopup = false;
            this.showToast("You cannot fill child and parent details");
          } else if (memberData.code == "SPOU") {
            this.hideMemberPopup = false;
            this.showToast("You cannot fill spouse and parent details");
          } else if (memberData.code == "MOTH" || memberData.code == "FATH") {
            this.hideMemberPopup = false;
            this.showToast("You cannot fill both parent details");
          }
        } else {
          if (memberData.code == "SPOU") {
            for (let i = 0; i < this.membersInsured.length; i++) {
              if (this.membersInsured[i].code == "SELF") {
                if (this.membersInsured[i].maritalStatus == "Married") {
                  this.hidePopupSmokerDiv = true;
                  this.showTransBaground = false;
                  this.hideMemberPopup = true;
                } else if (this.membersInsured[i].maritalStatus == "Single") {
                  this.showTransBaground = true;
                  this.hideMemberPopup = false;
                  this.showToast("As your marital status is single, you cannot fill spouse details.");
                } else if (this.membersInsured[i].maritalStatus == "Divorced") {
                  this.showTransBaground = true;
                  this.hideMemberPopup = false;
                  this.showToast("As your marital status is divorced, you cannot fill spouse details.");
                } else if (this.membersInsured[i].maritalStatus == "Widow / Widower") {
                  this.showTransBaground = true;
                  this.hideMemberPopup = false;
                  this.showToast("As your marital status is widow / widower, you cannot fill spouse details.");
                }
                break;
              } else {
                this.showTransBaground = false;
                this.hideMemberPopup = true;
                this.hidePopupMarriedDiv = false;
                this.hidePopupSmokerDiv = true;
              }

            }
          } else if (memberData.code == "SON" || memberData.code == "DAUG") {
            this.showTransBaground = false;
            this.showChildPopup = true;
            this.hideMemberPopup = true;
            this.hidePopupSmokerDiv = false;
            this.memberCodes = [];
            this.childCount = 0;
            for (let i = 0; i < this.membersInsured.length; i++) {
              this.memberCodes.push(this.membersInsured[i].code)
              if (this.membersInsured[i].code == "SON" || this.membersInsured[i].code == "DAUG") {
                this.childCount++;
              }
            }
            console.log(this.childCount)
            console.log("childCount", this.childCount);
            console.log(this.memberCodes)
            let doesSelfExits = this.memberCodes.find(x => x == "SELF");
            let doesSpouseExits = this.memberCodes.find(x => x == "SPOU")
            console.log(doesSelfExits);
            console.log(doesSpouseExits)
            if ((doesSelfExits == undefined || doesSpouseExits == undefined) && (doesSpouseExits == "SPOU" || doesSelfExits == "SELF")) {
              if (this.childCount > 2) {
                this.showTransBaground = true;
                this.hideMemberPopup = false;
                this.showChildPopup = false;
                this.showToast("You can insure maximum 3 child with this Plan.");
              }
            } else if (doesSpouseExits == "SPOU" && doesSelfExits == "SELF") {
              if (this.childCount > 4) {
                this.showTransBaground = true;
                this.hideMemberPopup = false;
                this.showChildPopup = false;
                this.showToast("You can insure maximum 5 child with this Plan.");
              }
            } else {
              if (this.childCount > 4) {
                this.showTransBaground = true;
                this.hideMemberPopup = false;
                this.showChildPopup = false;
                this.showToast("You can insure maximum 5 child with this Plan.");
              }
            }

          } else if (memberData.code == "MOTH" || memberData.code == "FATH") {
            this.hideMemberPopup = false;
            this.showToast("You cannot fill details");
          }
        }
      }
      // else {
      //   if (this.membersInsured[0].code == "FATH" || this.membersInsured[0].code == "MOTH") {
      //     this.hideMemberPopup = false;
      //     this.showToast("You cannot fill self and parent details");
      //   }
      // }
    }
    // else if () {

    // }
    this.popupTitle = memberData.title;
    this.popupImg = memberData.imgUrl;
    this.popupMemberData = memberData;
    // this.disableProceedBt = false;
  }
  showToast(Message) {
    if (this.canShowToast) {
      let pageToast = this.toast.create({
        message: Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }
  }
  hidePopup() {
    console.log(this.childCount);
    console.log(this.editUserData);
    this.slide = '';
    this.showMemberPopup = false;
    this.showChildPopup = false;
    this.insuredCode = '';
    this.childsAge = "";
    this.memberAge = "";
    this.memberSmokes = false;
    this.marriedStatus = "";
    //this.memberMarried = false;
    this.showTransBaground = true;
    this.hideMemberPopup = false;
  }
  membersArrayFunction() {
    for (let a = 0; a < this.apiMembers.length; a++) {
      let term = this.myMember.filter(person => person.id == this.apiMembers[a].code);
      var memberDetails = {};
      memberDetails["code"] = this.apiMembers[a].code;
      memberDetails["forQuickQuote"] = this.forQuickQuote;
      memberDetails["title"] = this.apiMembers[a].title;
      memberDetails["smoking"] = false;
      memberDetails["smokingText"] = "";
      memberDetails["popupType"] = "1";
      memberDetails["showMarried"] = false;
      memberDetails["maritalStatus"] = "";
      memberDetails["age"] = "";
      memberDetails["ageText"] = "";
      memberDetails["insuredCode"] = "";
      memberDetails["insuredAmtText"] = "";
      memberDetails["showDependent"] = false;
      memberDetails["showAddAnotherBtn"] = false;
      memberDetails["PolicyType"] = '';
      memberDetails["showFamilyMemberOnScreen"] = false;
      memberDetails["disableRemove"] = false;
      memberDetails["ShowInsurenceAmt"] = false;
      memberDetails["insuredPlanTypeCode"] = '';
      memberDetails["insuredPlanTypeText"] = '';
      memberDetails["insuredAmtInDigit"] = '';
      memberDetails["memberSelectedOnAddmember"] = false;
      if (this.vitalMembers.indexOf(this.apiMembers[a].code) >= 0) {
        memberDetails["showFamilyMemberOnScreen"] = true;
        memberDetails["memberSelectedOnAddmember"] = true;
      }
      if (term.length != 0) {
        memberDetails["imgUrl"] = term[0].imgPath;
        if (term[0].id == "SON" || term[0].id == "DAUG") {
          memberDetails["showDependent"] = true;
          memberDetails["popupType"] = "2";
          memberDetails["showAddAnotherBtn"] = true;
        } else if (term[0].id == "SELF") {
          memberDetails["showMarried"] = true;
          memberDetails["disableRemove"] = true;
        } else if (term[0].id == "SPOU") {
          memberDetails["disableRemove"] = true;
        }
      }
      if (this.apiMembers[a].title.toLowerCase().indexOf("son") >= 0) {
        memberDetails["imgUrl"] = "assets/imgs/son.png";
      } else if (this.apiMembers[a].title.toLowerCase().indexOf("daughter") >= 0) {
        memberDetails["imgUrl"] = "assets/imgs/daughterIcon.png";
      } else if (this.apiMembers[a].title.toLowerCase().indexOf("father") >= 0) {
        memberDetails["imgUrl"] = "assets/imgs/fatherIcon.png";
      } else if (this.apiMembers[a].title.toLowerCase().indexOf("mother") >= 0) {
        memberDetails["imgUrl"] = "assets/imgs/motherIcon.png";
      } else if (term.length == 0) {
        memberDetails["imgUrl"] = "assets/imgs/self.png";
      }
      memberDetails["detailsSaved"] = false;
      this.membersShow.push(memberDetails);

    }
    console.log("membersShow", (this.membersShow));
  }
  childAgeRange() {
    for (let i = 0; i < this.apiAgeGroup.length; i++) {
      if (this.apiAgeGroup[i].min > 25) {
        break;
      }
      this.childAgeGroup.push(this.apiAgeGroup[i]);
    }
    console.log(this.childAgeGroup)
  }

  // hidePopup(){
  //  this.showTransBaground = !this.showTransBaground;
  //  this.hideMemberPopup = !this.hideMemberPopup;
  // } 



}

