import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TplandingofflinePage } from './tplandingoffline';

@NgModule({
  declarations: [
    // TplandingofflinePage,
  ],
  imports: [
    IonicPageModule.forChild(TplandingofflinePage),
  ],
})
export class TplandingofflinePageModule {}
