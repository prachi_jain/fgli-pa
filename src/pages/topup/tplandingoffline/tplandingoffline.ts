import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TbuyPage } from '../tbuy/tbuy';
import { TpquickquotePage } from '../tpquickquote/tpquickquote';
/**
 * Generated class for the TplandingofflinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tplandingoffline',
  templateUrl: 'tplandingoffline.html',
})
export class TplandingofflinePage {

  deductableDiscountList = [];
  deductableDiscountListShow;
  sumInsuredList = [];
  sumInsuredListToPass = [];
  selectSumInsured;
  selectDeduction;
  hideSumInsured = true;
  hideProceedButton = true;
  canShowToast = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toast: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TplandingofflinePage');
    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 7000);

    console.log(localStorage.STUDeductData);
    this.showDeductibleAmount();
    // this.serviceCallTopUp();
  }
  // serviceCallTopUp() {

  // }
  showDeductibleAmount() {
    this.deductableDiscountList = JSON.parse(localStorage.STUDeductData);
    console.log(this.deductableDiscountList);
    for (let i = 0; i < this.deductableDiscountList.length; i++) {
      console.log(this.deductableDiscountList[i].Amount);
      if ((Number(this.deductableDiscountList[i].Amount)) < 100000) {
        this.deductableDiscountList[i].Amount = (Number(this.deductableDiscountList[i].Amount) / 1000) + "K";
      } else if ((Number(this.deductableDiscountList[i].Amount)) < 10000000) {
        this.deductableDiscountList[i].Amount = (Number(this.deductableDiscountList[i].Amount) / 100000) + "L";
      } else if ((Number(this.deductableDiscountList[i].Amount)) < 100000000) {
        this.deductableDiscountList[i].Amount = (Number(this.deductableDiscountList[i].Amount) / 1000000) + "Cr";
      }
    }
    this.deductableDiscountListShow = this.deductableDiscountList;
    console.log(JSON.stringify(this.deductableDiscountList));
    console.log(this.deductableDiscountList[0].Amount)
    console.log(this.deductableDiscountListShow[0].Amount)
  }
  showToast(Message) {
    if (this.canShowToast) {
      let pageToast = this.toast.create({
        message: Message,
        showCloseButton: true,
        closeButtonText: 'OK',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }
  }

  selectDeductionClick(amt) {
    this.hideProceedButton = true;
    this.selectDeduction = amt;
    this.selectSumInsured = '';
    if (this.selectDeduction == undefined) {
      this.showToast("Please select deductible discount");
    } else {
      this.hideSumInsured = false;
      console.log(this.selectDeduction);
      for (let i = 0; i < this.deductableDiscountList.length; i++) {
        if (this.deductableDiscountList[i].Amount == this.selectDeduction) {
          this.sumInsuredList = this.deductableDiscountList[i].SumInsured;
        }
      }
    }
    for (let i = 0; i < this.sumInsuredList.length; i++) {
      this.sumInsuredListToPass.push(this.sumInsuredList[i].Amt);
      if ((Number(this.sumInsuredList[i].Amt)) < 100000) {
        this.sumInsuredList[i].Amt = (Number(this.sumInsuredList[i].Amt) / 1000) + "K";
      } else if ((Number(this.sumInsuredList[i].Amt)) < 10000000) {
        this.sumInsuredList[i].Amt = (Number(this.sumInsuredList[i].Amt) / 100000) + "L";
      } else if ((Number(this.sumInsuredList[i].Amt)) < 100000000) {
        this.sumInsuredList[i].Amt = (Number(this.sumInsuredList[i].Amt) / 1000000) + "Cr";
      }
    }
    console.log(this.sumInsuredList);
  }

  selectSumInsuredClick(SI) {
    this.selectSumInsured = SI;
    if (this.selectSumInsured == undefined) {
      this.showToast("Please select sum insured ");
    } else {
      this.hideProceedButton = false;
    }
  }


  proceedButtonClick() {
    if (this.selectDeduction == undefined || this.selectSumInsured == undefined || this.selectSumInsured == '') {
      this.showToast("Please select both option");
    } else {
      this.navCtrl.push(TpquickquotePage, { deductableData: this.selectDeduction, siAmount: this.selectSumInsured, sumInsuredListToPass: this.sumInsuredList });
    }
  }
}
