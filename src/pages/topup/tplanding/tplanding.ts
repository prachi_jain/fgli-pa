import { Component,NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TbuyPage } from '../tbuy/tbuy';
import { AppService } from '../../../providers/app-service/app-service';

/**
 * Generated class for the TplandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    TplandingPage,
  ],
  imports: [
    //IonicPageModule.forChild(TplandingPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-tplanding',
  templateUrl: 'tplanding.html',
})
export class TplandingPage {

  deductableDiscountList = [];
  sumInsuredList= [];
  QuickQuotePlans = [];
  selectSumInsured;
  selectDeduction;
  hideProceedButton = true;
  canShowToast = true;
  appData;
  apiPlanType;
  loginData;
  planTypeText;
  planTypeCode;
  sIAmount;
  showSumInsuredDiv = true;
  SIArray = [];

  constructor(public navCtrl: NavController, public appService : AppService, public navParams: NavParams, public toast: ToastController) {
    this.appData = JSON.parse(localStorage.AppData);
    this.apiPlanType = this.appData.Masters.PlanType;
    this.deductableDiscountList = JSON.parse(localStorage.superTopUpDeductableData);
    console.log("Deductable amount" + JSON.stringify(this.deductableDiscountList));
    //  for(let i=0; i<this.deductableList.length; i++){
    //    this.deductableDiscountList.push(this.deductableList.[i].Amount)
    //  }
    console.log("Deductable discount" + this.deductableDiscountList);    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TplandingPage');
    setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);
      this.loginData = JSON.parse(sessionStorage.Ispos);
      this.getSumInsuredArray();
      
      for (let i = 0; i < this.deductableDiscountList.length; i++) {
        if ((Number(this.deductableDiscountList[i].Amount)) < 100000) {
        this.deductableDiscountList[i].Amt = (Number(this.deductableDiscountList[i].Amount) / 1000) + "K";
        } else if ((Number(this.deductableDiscountList[i].Amount)) < 10000000) {
        this.deductableDiscountList[i].Amt = (Number(this.deductableDiscountList[i].Amount) / 100000) + "L";
        } else if ((Number(this.deductableDiscountList[i].Amount)) < 100000000) {
        this.deductableDiscountList[i].Amt = (Number(this.deductableDiscountList[i].Amount) / 1000000) + "Cr";
        }
        }
  
        console.log("update" + JSON.stringify(this.deductableDiscountList));
   
        
      //this.populateInsurenceAmt();
  }
   getSumInsuredArray(){
    this.QuickQuotePlans =[];
    this.QuickQuotePlans = this.appService.getSumInsuredArray(this.apiPlanType);
  }


  populateInsurenceAmt(){
    //this.insurenceAmtToShow = [];

    
    // for(let i = 0; i < this.QuickQuotePlans.length;i++){
    //   // if(this.maximumInsuAmtCode!='' && this.QuickQuotePlans[i].sicode == this.maximumInsuAmtCode && memberCode != "SELF"){
    //   //   this.insurenceAmtToShow.push(this.QuickQuotePlans[i]); 
    //   //   if(i==3){
    //   //     this.insurenceAmtToShow = this.insurenceAmtToShow.slice(this.insurenceAmtToShow.length - 3, this.insurenceAmtToShow.length); 
    //   //     break; 
    //   //     }
    //   //     else if(i>3){
    //   //     this.insurenceAmtToShow = this.insurenceAmtToShow.slice(i-2, this.insurenceAmtToShow.length); 
    //   //     break; 
    //   //     }
    //   //     else if(i<3){
    //   //     this.insurenceAmtToShow = this.insurenceAmtToShow.slice(0,i+1); 
    //   //     break;
    //   //     }                
    //   //   //break;
    //   // }else{
    //   //   this.insurenceAmtToShow.push(this.QuickQuotePlans[i]);
    //   // }
    //   //if(Number == "0" || Number == 0){
    //     this.sumInsuredList.push(this.QuickQuotePlans[i]);
    //   //}
    // }

    //saved on home page
    var posVerification = sessionStorage.posVerification;
    
    if(this.loginData == true && posVerification == "Y"){
      this.sumInsuredList = this.sumInsuredList.slice(0, 2);     
    }
    
  }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'OK',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  selectSumInsuredClick(amount, j){
    console.log("sum" + amount);  
    console.log("index" + j);
       this.SIArray = [];
    for(let i=0; i<this.sumInsuredList.length; i++){
      if(amount == this.sumInsuredList[i].Amount){
        if(j == (this.sumInsuredList.length - 1)){
          console.log("Last");                       
          this.SIArray.push({SI : this.sumInsuredList[(this.sumInsuredList.length -3)].Amount});
          this.SIArray.push({SI : this.sumInsuredList[(this.sumInsuredList.length -2)].Amount});
          this.SIArray.push({SI : this.sumInsuredList[(this.sumInsuredList.length -1)].Amount});
        }else if(j == (this.sumInsuredList.length - 2)){
          console.log("2nd Last");
          this.SIArray.push({SI : this.sumInsuredList[(this.sumInsuredList.length -3)].Amount});
          this.SIArray.push({SI : this.sumInsuredList[(this.sumInsuredList.length -2)].Amount});
          this.SIArray.push({SI : this.sumInsuredList[(this.sumInsuredList.length -1)].Amount});
        }else{
          console.log("Default");
          this.SIArray.push({SI : this.sumInsuredList[(j)].Amount});
          this.SIArray.push({SI : this.sumInsuredList[(j + 1)].Amount});
          this.SIArray.push({SI : this.sumInsuredList[(j + 2)].Amount});
        }
      }   
    }
    console.log("RequestService Array" + JSON.stringify(this.SIArray));    
    console.log("1" + this.selectSumInsured);  
    this.selectSumInsured = amount;
    console.log("2" + this.selectSumInsured);  
    this.planTypeText = amount;
    this.planTypeCode = amount;
    this.sIAmount = amount;
    if(this.selectDeduction == undefined){
      this.showToast("Please select deductable discount");
    }else{
      this.hideProceedButton = false;
    }  
  }

  selectDeductionClick(){
    console.log("2" + this.selectDeduction);
    if(this.selectDeduction == undefined){
      //this.showSumInsuredDiv = false;
    }else{
      this.showSumInsuredDiv = false;
      for(let i=0; i<this.deductableDiscountList.length; i++){
        if(this.deductableDiscountList[i].Amount == this.selectDeduction){
          //this.sumInsuredList.push(this.deductableDiscountList[i].SumInsured);
          this.sumInsuredList = JSON.parse(JSON.stringify(this.deductableDiscountList[i].SumInsured)) ;
        }
      }
  
      console.log("Sum" + JSON.stringify(this.sumInsuredList));
      for (let i = 0; i < this.sumInsuredList.length; i++) {
        if ((Number(this.sumInsuredList[i].Amt)) < 100000) {
          this.sumInsuredList[i].Amount=this.sumInsuredList[i].Amt;
          this.sumInsuredList[i].Amt= (Number(this.sumInsuredList[i].Amt) / 1000) + "K";         
        } else if ((Number(this.sumInsuredList[i].Amt)) < 10000000) {
          this.sumInsuredList[i].Amount=this.sumInsuredList[i].Amt;
          this.sumInsuredList[i].Amt= (Number(this.sumInsuredList[i].Amt) / 100000) + "L";
        } else if ((Number(this.sumInsuredList[i].Amt)) < 100000000) {
          this.sumInsuredList[i].Amount=this.sumInsuredList[i].Amt;
          this.sumInsuredList[i].Amt= (Number(this.sumInsuredList[i].Amt) / 1000000) + "Cr";
        }
        }      
      console.log("updateSum" + JSON.stringify(this.sumInsuredList));
    }
    // if(this.selectSumInsured == undefined){
    //   this.showToast("Please select sum insured");
    // }else{
    //   this.hideProceedButton = false;
    // }    
  }
  proceedButtonClick(){
    if(this.selectDeduction == undefined && this.selectSumInsured == undefined){
      this.showToast("Please select both option");      
    }else{
      this.navCtrl.push(TbuyPage, {deductableData: this.selectDeduction, siAmounut: this.sIAmount, planTypeText: this.planTypeText, planTypeCode: this.planTypeCode, SuminsuredArray: this.SIArray});
    }
  }

}
