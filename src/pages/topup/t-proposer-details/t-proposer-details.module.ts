import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TProposerDetailsPage } from './t-proposer-details';

@NgModule({
  declarations: [
    //TProposerDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TProposerDetailsPage),
  ],
})
export class TProposerDetailsPageModule {}
