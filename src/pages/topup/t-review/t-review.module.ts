import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TReviewPage } from './t-review';

@NgModule({
  declarations: [
    //TReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(TReviewPage),
  ],
})
export class TReviewPageModule {}
