import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    TReviewPage,
  ],
  imports: [
    //IonicPageModule.forChild(TReviewPage),
  ],
})

@IonicPage() 
@Component({
  selector: 'page-t-review',
  templateUrl: 't-review.html',
})
export class TReviewPage { 

  pet ='Policy';

	 policyDetails = false;
   contactDetails = true;
   insuredDetails = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TReviewPage');
    setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);
  }

  PolicyDetails(){
   this.policyDetails = false;
   this.contactDetails = true;
   this.insuredDetails = true;
  }
  ContactDetails(){
this.policyDetails = true;
   this.contactDetails = false;
   this.insuredDetails = true;
  }
  InsuredDetails(){
   this.policyDetails = true;
   this.contactDetails = true;
   this.insuredDetails = false;
  }

}
