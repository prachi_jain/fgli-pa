import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TNomineeDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    TNomineeDetailsPage,
  ],
  imports: [
    //IonicPageModule.forChild(TNomineeDetailsPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-t-nominee-details',
  templateUrl: 't-nominee-details.html',
})
export class TNomineeDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TNomineeDetailsPage');
     setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                
      }, 7000);
  }
}
