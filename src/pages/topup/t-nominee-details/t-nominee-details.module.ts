import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TNomineeDetailsPage } from './t-nominee-details';

@NgModule({
  declarations: [
    //TNomineeDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TNomineeDetailsPage),
  ],
})
export class TNomineeDetailsPageModule {}
