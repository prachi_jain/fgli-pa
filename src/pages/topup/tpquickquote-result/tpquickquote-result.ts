import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Conditional } from '@angular/compiler';
import { NULL_EXPR, NUMBER_TYPE } from '@angular/compiler/src/output/output_ast';
import { LoginPage } from '../../health/login/login';
import { AppService } from "../../../providers/app-service/app-service";
/**
 * Generated class for the TpquickquoteResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@NgModule({
  declarations: [
    TpquickquoteResultPage,
  ],
  imports: [
    //IonicPageModule.forChild(TpquickquoteResultPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-tpquickquote-result',
  templateUrl: 'tpquickquote-result.html',
})
export class TpquickquoteResultPage {
  showPopup = true;
  successLabel = true;
  buyPopup = true;
  deductableData;
  siAmount: string;
  siAmt;
  SIAmt;
  STUChildData;
  STUSpouseData;
  sumInsuredList;
  sumInsuredListToPass;
  sumInsuredListToShow = [];
  membersInsured;
  premiumAmt = [];
  premiumAmt2 = [];
  premiumAmt3 = [];
  preAmt = [];
  percent;
  keepSignedIn;
  showDiscountAmt = true;
  sumInsuredListToShow1Yr = [];
  sumInsuredListToShow2Yr = [];
  sumInsuredListToShow3Yr = [];
  sumInsuredListToShow2YrM = [];
  sumInsuredListToShow3YrM = [];
  sumInsuredListToShowSupreme = [];
  sumInsuredListToShowElite = [];
  sumInsuredListToOpt = [];
  sumInsuredListToPlanShow = [];
  sumInsuredListToOperate = [];
  sumInsuredListToOperate1 = [];
  selectedYr = '1';
  ageArray = [];
  FFChild;
  FFSpouse;
  maxAge;
  childCount = 0;
  Installment;
  oneTime = true;
  yrs = 0;
  Installments = 0;
  percentage = 0;
  pet: string = "OneYears";
  constructor(public navCtrl: NavController, public appService: AppService, public navParams: NavParams) {
    this.deductableData = navParams.get("deductableData");
    this.siAmount = navParams.get("siAmount");
    this.sumInsuredListToPass = navParams.get("sumInsuredListToPass");
    this.membersInsured = navParams.get("membersInsured");
    this.childCount = navParams.get("childCount");

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TBuyPageResultPage');
    console.log("deductableData", this.deductableData);
    console.log("siAmount", this.siAmount);
    console.log("sumInsuredListToPass", this.sumInsuredListToPass);
    console.log("membersInsured", this.membersInsured);

    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 7000);

    this.showCardDetailsPop();
    this.sumInsuredListToOpt = this.sumInsuredListToShow;
    this.calAmtForYearPlans();
    // this.calAmtForPlans();
  }
  showCardDetailsPop() {
    this.STUChildData = JSON.parse(localStorage.STUChildData);
    this.STUSpouseData = JSON.parse(localStorage.STUSpouseData);
    // this.showPopup = !this.showPopup;
    console.log("STUChildData", this.STUChildData);
    console.log("STUSpouseData", this.STUSpouseData);
    console.log("members", this.membersInsured)
    for (let i = 0; i < this.membersInsured.length; i++) {
      this.ageArray.push(this.membersInsured[i].ageText.split("-")[0])
    }
    for (let i = 0; i < this.STUChildData.length; i++) {
      if ((this.STUChildData[i].Age.split("-")[0]).includes(Math.max(...this.ageArray))) {
        this.FFChild = this.STUChildData[i].FF;
        console.log(Math.max(...this.ageArray))
      }
    }
    if (Math.max(...this.ageArray) == 18) {
      this.maxAge = "18-25";
    } else if (Math.max(...this.ageArray) == 26) {
      this.maxAge = "26-30";
    } else if (Math.max(...this.ageArray) == 31) {
      this.maxAge = "31-35";
    } else if (Math.max(...this.ageArray) == 36) {
      this.maxAge = "36-40";
    } else if (Math.max(...this.ageArray) == 41) {
      this.maxAge = "41-45";
    } else if (Math.max(...this.ageArray) == 46) {
      this.maxAge = "46-50";
    } else if (Math.max(...this.ageArray) == 51) {
      this.maxAge = "51-55";
    } else if (Math.max(...this.ageArray) == 56) {
      this.maxAge = "56-60";
    } else if (Math.max(...this.ageArray) == 61) {
      this.maxAge = "61-65";
    } else if (Math.max(...this.ageArray) == 66) {
      this.maxAge = "66-70";
    } else if (Math.max(...this.ageArray) == 71) {
      this.maxAge = "71-75";
    } else if (Math.max(...this.ageArray) == 76) {
      this.maxAge = "76&Above";
    } else {
      this.maxAge = "0-17";
    }
    console.log("FF", this.FFChild);
    console.log("MaxAge", this.maxAge);
    var preAmtSpouse = 0;
    var preAmtChild = 0;
    for (let i = 0; i < this.sumInsuredListToPass.length; i++) {
      if ((this.siAmount) == (this.sumInsuredListToPass[i].Amt)) {
        if (i == (this.sumInsuredListToPass.length - 1)) {
          for (let j = 0; j < this.sumInsuredListToPass[i].Premium.length; j++) {
            if ((this.maxAge) == (this.sumInsuredListToPass[i].Premium[j].AgeLimit)) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                console.log(preAmt1)
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt)) * 0.18;
              }

              this.sumInsuredListToShow.push({ Amt: (this.sumInsuredListToPass[i].Amt), PremiumAmount: (Math.round(preAmt)) });
              // this.sumInsuredListToShow.push({ Amt: this.appService.addCommas(this.sumInsuredListToPass[i].Amt), PremiumAmount: this.appService.addCommas(Math.round(preAmt)) });
            }
          }
          for (let j = 0; j < this.sumInsuredListToPass[i - 1].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i - 1].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i - 1].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
          for (let j = 0; j < this.sumInsuredListToPass[i - 2].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i - 2].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i - 2].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i - 2].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i - 2].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i - 2].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i - 2].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i - 2].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i - 2].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
        } else if (i == (this.sumInsuredListToPass.length - 2)) {
          for (let j = 0; j < this.sumInsuredListToPass[i - 1].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i - 1].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i - 1].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i - 1].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
          for (let j = 0; j < this.sumInsuredListToPass[i].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
          for (let j = 0; j < this.sumInsuredListToPass[i + 1].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i + 1].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i + 1].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
        } else {
          for (let j = 0; j < this.sumInsuredListToPass[i].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
          for (let j = 0; j < this.sumInsuredListToPass[i + 1].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i + 1].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i + 1].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i + 1].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
          for (let j = 0; j < this.sumInsuredListToPass[i + 2].Premium.length; j++) {
            if (this.maxAge == this.sumInsuredListToPass[i + 2].Premium[j].AgeLimit) {
              if (this.membersInsured.length > 1) {
                for (let k = 0; k < this.membersInsured.length; k++) {
                  console.log(this.membersInsured)
                  if (this.membersInsured[k].code.includes("SPOU")) {
                    preAmtSpouse = Number(this.sumInsuredListToPass[i + 2].Premium[j].PremiumAmt) * 0.55;
                    break;
                  }
                }
                if (this.childCount >= 1) {
                  preAmtChild = Number(this.sumInsuredListToPass[i + 2].Premium[j].PremiumAmt) * Number(this.FFChild / 100)
                }
                console.log(Number(this.sumInsuredListToPass[i + 2].Premium[j].PremiumAmt));
                console.log(Number(preAmtSpouse));
                console.log(Number(this.childCount * preAmtChild));
                var preAmt1 = Number(this.sumInsuredListToPass[i + 2].Premium[j].PremiumAmt) + Number(preAmtSpouse) + Number(this.childCount * preAmtChild);
                if (Math.max(...this.ageArray) < 71) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.1;
                } else if (Math.max(... this.ageArray) < 75) {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.075;
                } else {
                  var preAmtFamily = preAmt1 - preAmt1 * 0.05;
                }
                var preAmt = (preAmtFamily) + (preAmtFamily) * 0.18;
              } else {
                var preAmt = (Number(this.sumInsuredListToPass[i + 2].Premium[j].PremiumAmt)) + (Number(this.sumInsuredListToPass[i + 2].Premium[j].PremiumAmt)) * 0.18;
              }
              this.sumInsuredListToShow.push({ Amt: this.sumInsuredListToPass[i + 2].Amt, PremiumAmount: Math.round(preAmt) });
            }
          }
        }
      }
    }
    console.log("sumInsuredListToShow", this.sumInsuredListToShow);
  }
  calAmtForYearPlans() {

    this.sumInsuredListToPlanShow = this.sumInsuredListToShow;
    for (let i = 0; i < this.sumInsuredListToPlanShow.length; i++) {
      this.sumInsuredListToShowSupreme.push({ Amt: this.sumInsuredListToPlanShow[i].Amt, PremiumAmount: Math.round(this.sumInsuredListToPlanShow[i].PremiumAmount) })
      this.sumInsuredListToShowElite.push({ Amt: this.sumInsuredListToPlanShow[i].Amt, PremiumAmount: Math.round(((this.sumInsuredListToPlanShow[i].PremiumAmount) - ((this.sumInsuredListToPlanShow[i].PremiumAmount) * 0.3))) });
    }
    this.sumInsuredListToOpt = this.sumInsuredListToShow;
    for (let i = 0; i < this.sumInsuredListToOpt.length; i++) {
      this.sumInsuredListToShow1Yr.push({ Amt: this.sumInsuredListToOpt[i].Amt, PremiumAmount: Math.round(this.sumInsuredListToOpt[i].PremiumAmount) })
      this.sumInsuredListToShow2Yr.push({ Amt: this.sumInsuredListToOpt[i].Amt, PremiumAmount: Math.round(((this.sumInsuredListToOpt[i].PremiumAmount) * 2) - ((this.sumInsuredListToOpt[i].PremiumAmount) * 2) * 0.05), discountAmt: Math.round((this.sumInsuredListToOpt[i].PremiumAmount) * 2) })
      this.sumInsuredListToShow3Yr.push({ Amt: this.sumInsuredListToOpt[i].Amt, PremiumAmount: Math.round(((this.sumInsuredListToOpt[i].PremiumAmount) * 3) - ((this.sumInsuredListToOpt[i].PremiumAmount) * 3) * 0.1), discountAmt: Math.round((this.sumInsuredListToOpt[i].PremiumAmount) * 3) })

    }
  }
  closePopInfo() {
    this.showPopup = !this.showPopup;
  }

  buyPopUpOpen() {
    this.buyPopup = !this.buyPopup;
  }
  closeModalPop() {
    this.buyPopup = !this.buyPopup;
  }
  showSumInByYear(yr) {
    console.log("year", yr);
    console.log("sumInsuredListToShow", this.sumInsuredListToShow);
    if (yr == '1') {
      this.pet = "OneYears";
      this.selectedYr = '1'
      this.showDiscountAmt = true;
      this.sumInsuredListToShow = (this.sumInsuredListToShow1Yr);
    } else if (yr == '2') {
      this.pet = "TwoYears";
      this.selectedYr = '2'
      this.percent = " (5% off)"
      this.showDiscountAmt = false;
      this.sumInsuredListToShow = (this.sumInsuredListToShow2Yr);
      if (this.Installment != undefined) {
        this.sumInsuredListToShow = (this.sumInsuredListToShow2YrM);
        if (this.Installment.trim() == "Monthly") {
          this.showDiscountAmt = true;
          this.percentage = 0.05;
          this.Installments = 24;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Quarterly") {
          this.showDiscountAmt = true;
          this.percentage = 0.04;
          this.Installments = 8;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Half-Yearly") {
          this.showDiscountAmt = true;
          this.percentage = 0.03;
          this.Installments = 4;
          this.oneTime = false;
        } else if (this.Installment.trim() == "One time") {
          this.pet = "TwoYears";
          this.selectedYr = '2'
          this.percent = " (5% off)"
          this.showDiscountAmt = false;
          this.sumInsuredListToShow = (this.sumInsuredListToShow2Yr);
        }
      }

    } else if (yr == '3') {
      this.pet = "ThreeYears";
      this.selectedYr = '3'
      this.percent = " (10% off)"
      this.showDiscountAmt = false;
      this.sumInsuredListToShow = (this.sumInsuredListToShow3Yr);
      if (this.Installment != undefined) {
        this.sumInsuredListToShow = (this.sumInsuredListToShow3YrM);
        if (this.Installment.trim() == "Monthly") {
          this.showDiscountAmt = true;
          this.percentage = 0.05;
          this.Installments = 36;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Quarterly") {
          this.showDiscountAmt = true;
          this.percentage = 0.04;
          this.Installments = 12;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Half-Yearly") {
          this.showDiscountAmt = true;
          this.percentage = 0.03;
          this.Installments = 6;
          this.oneTime = false;
        } else if (this.Installment.trim() == "One time") {
          this.pet = "ThreeYears";
          this.selectedYr = '3'
          this.percent = " (10% off)"
          this.showDiscountAmt = false;
          this.sumInsuredListToShow = (this.sumInsuredListToShow3Yr);
        }
      }
    }
    // this.calAmtForYearPlans();
    console.log((this.sumInsuredListToShow));

  }
  planChanged(e) {
    console.log(this.keepSignedIn);

    this.sumInsuredListToShow1Yr = [];
    this.sumInsuredListToShow2Yr = [];
    this.sumInsuredListToShow3Yr = [];
    console.log(this.sumInsuredListToShow2YrM);
    console.log(this.sumInsuredListToShow3YrM);

    if (this.keepSignedIn) {
      this.sumInsuredListToShow = this.sumInsuredListToShowElite;
      this.sumInsuredListToOpt = this.sumInsuredListToShowElite;
    } else {
      this.sumInsuredListToShow = this.sumInsuredListToShowSupreme;
      this.sumInsuredListToOpt = this.sumInsuredListToShowSupreme;
    }

    for (let i = 0; i < this.sumInsuredListToOpt.length; i++) {
      this.sumInsuredListToShow1Yr.push({ Amt: this.sumInsuredListToOpt[i].Amt, PremiumAmount: Math.round(this.sumInsuredListToOpt[i].PremiumAmount) })
      this.sumInsuredListToShow2Yr.push({ Amt: this.sumInsuredListToOpt[i].Amt, PremiumAmount: Math.round(((this.sumInsuredListToOpt[i].PremiumAmount) * 2) - ((this.sumInsuredListToOpt[i].PremiumAmount) * 2) * 0.05), discountAmt: Math.round((this.sumInsuredListToOpt[i].PremiumAmount) * 2) })
      this.sumInsuredListToShow3Yr.push({ Amt: this.sumInsuredListToOpt[i].Amt, PremiumAmount: Math.round(((this.sumInsuredListToOpt[i].PremiumAmount) * 3) - ((this.sumInsuredListToOpt[i].PremiumAmount) * 3) * 0.1), discountAmt: Math.round((this.sumInsuredListToOpt[i].PremiumAmount) * 3) })

    }
    console.log(this.selectedYr);
    if (this.selectedYr == '1') {
      this.selectedYr = '1'
      this.showDiscountAmt = true;
      this.sumInsuredListToShow = (this.sumInsuredListToShow1Yr);
    } else if (this.selectedYr == '2') {
      this.selectedYr = '2'
      this.percent = " (5% off)"
      this.showDiscountAmt = false;
      this.sumInsuredListToShow = (this.sumInsuredListToShow2Yr);
      if (this.Installment != undefined) {
        this.sumInsuredListToShow = (this.sumInsuredListToShow2YrM);
        if (this.Installment.trim() == "Monthly") {
          this.showDiscountAmt = true;
          this.percentage = 0.05;
          this.Installments = 24;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Quarterly") {
          this.showDiscountAmt = true;
          this.percentage = 0.04;
          this.Installments = 8;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Half-Yearly") {
          this.showDiscountAmt = true;
          this.percentage = 0.03;
          this.Installments = 4;
          this.oneTime = false;
        } else if (this.Installment.trim() == "One time") {
          this.pet = "TwoYears";
          this.selectedYr = '2'
          this.percent = " (5% off)"
          this.showDiscountAmt = false;
          this.sumInsuredListToShow = (this.sumInsuredListToShow2Yr);
        }
      }
    } else if (this.selectedYr == '3') {
      this.selectedYr = '3'
      this.percent = " (10% off)"
      this.showDiscountAmt = false;
      this.sumInsuredListToShow = (this.sumInsuredListToShow3Yr);
      if (this.Installment != undefined) {
        this.sumInsuredListToShow = (this.sumInsuredListToShow3YrM);
        if (this.Installment.trim() == "Monthly") {
          this.showDiscountAmt = true;
          this.percentage = 0.05;
          this.Installments = 36;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Quarterly") {
          this.showDiscountAmt = true;
          this.percentage = 0.04;
          this.Installments = 12;
          this.oneTime = false;
        } else if (this.Installment.trim() == "Half-Yearly") {
          this.showDiscountAmt = true;
          this.percentage = 0.03;
          this.Installments = 6;
          this.oneTime = false;
        } else if (this.Installment.trim() == "One time") {
          this.pet = "ThreeYears";
          this.selectedYr = '3'
          this.percent = " (10% off)"
          this.showDiscountAmt = false;
          this.sumInsuredListToShow = (this.sumInsuredListToShow3Yr);
        }
      }
    }

    // if (this.Installment.trim() != "One time") {
    //   this.sumInsuredListToOperate = this.sumInsuredListToShow2Yr;
    //   for (let i = 0; i < this.sumInsuredListToOperate.length; i++) {
    //     let loadingAmt = Number(this.sumInsuredListToOperate[i].discountAmt) * this.percentage;
    //     let loadingPreAmt = Number(this.sumInsuredListToOperate[i].discountAmt) + loadingAmt;
    //     let installmentAmt = Number(loadingPreAmt) / this.Installments;
    //     let amtWithGST = Number(installmentAmt) * 0.18;
    //     let amtWithoutGst = installmentAmt + amtWithGST;
    //     let preAmt = amtWithoutGst;
    //     console.log("loadingAmt", loadingAmt);
    //     console.log("loadingPreAmt", loadingPreAmt);
    //     console.log("installmentAmt", installmentAmt);
    //     console.log("amtWithGST", amtWithGST);
    //     console.log("amtWithoutGst", amtWithoutGst);
    //     console.log("preAmt", preAmt);
    //     // Object.assign(this.sumInsuredListToShow2YrM[i], { Amt: this.sumInsuredListToOperate[i].Amt, PremiumAmount: Math.round(preAmt) });
    //     this.sumInsuredListToShow2YrM.push({ Amt: this.sumInsuredListToOperate[i].Amt, PremiumAmount: Math.round(preAmt) })
    //   }
    //   this.sumInsuredListToOperate = this.sumInsuredListToShow3Yr;
    //   for (let i = 0; i < this.sumInsuredListToOperate.length; i++) {
    //     if (this.Installment.trim() == "Monthly") {
    //       this.Installments = 36;
    //     } else if (this.Installment.trim() == "Quarterly") {
    //       this.Installments = 12;
    //     } else if (this.Installment.trim() == "Half-Yearly") {
    //       this.Installments = 6;
    //     } else {
    //       this.Installments = 0;
    //     }
    //     let loadingAmt = Number(this.sumInsuredListToOperate[i].discountAmt) * this.percentage;
    //     let loadingPreAmt = Number(this.sumInsuredListToOperate[i].discountAmt) + loadingAmt;
    //     let installmentAmt = Number(loadingPreAmt) / this.Installments;
    //     let amtWithGST = Number(installmentAmt) * 0.18;
    //     let amtWithoutGst = installmentAmt + amtWithGST;
    //     let preAmt = amtWithoutGst;
    //     console.log("loadingAmt", loadingAmt);
    //     console.log("installmentAmt", installmentAmt);
    //     console.log("loadingPreAmt", loadingPreAmt);
    //     console.log("amtWithoutGst", amtWithoutGst);
    //     console.log("amtWithGST", amtWithGST);
    //     console.log("preAmt", preAmt);
    //     // Object.assign(this.sumInsuredListToShow3YrM[i], { Amt: this.sumInsuredListToOperate[i].Amt, PremiumAmount: Math.round(preAmt) });
    //     this.sumInsuredListToShow3YrM.push({ Amt: this.sumInsuredListToOperate[i].Amt, PremiumAmount: Math.round(preAmt) })
    //   }

    //   if (this.pet.trim() == "TwoYears") {
    //     this.sumInsuredListToShow = this.sumInsuredListToShow2YrM;
    //   } else if (this.pet.trim() == "ThreeYears") {
    //     this.sumInsuredListToShow = this.sumInsuredListToShow3YrM;
    //   }
    // } else {
    //   if (this.pet.trim() == "TwoYears") {
    //     this.pet = "TwoYears";
    //     this.selectedYr = '2'
    //     this.percent = " (5% off)"
    //     this.showDiscountAmt = false;
    //     this.sumInsuredListToShow = this.sumInsuredListToShow2Yr;
    //   } else if (this.pet.trim() == "ThreeYears") {
    //     this.pet = "ThreeYears";
    //     this.selectedYr = '3'
    //     this.percent = " (10% off)"
    //     this.showDiscountAmt = false;
    //     this.sumInsuredListToShow = this.sumInsuredListToShow3Yr;
    //   } else if (this.pet.trim() == "OneYears") {
    //     this.pet = "OneYears";
    //     this.selectedYr = '1'
    //     this.showDiscountAmt = true;
    //     this.sumInsuredListToShow = this.sumInsuredListToShow1Yr;
    //   }
    // }

    console.log(this.sumInsuredListToShow);
  }
  installmentClicked() {
    console.log("pet", this.pet);
    console.log("Installment", this.Installment);
    this.pet = "TwoYears";
    this.sumInsuredListToShow2YrM = [];
    this.sumInsuredListToShow3YrM = [];
    if (this.Installment.trim() == "Monthly") {
      this.showDiscountAmt = true;
      this.percentage = 0.05;
      this.oneTime = false;
    } else if (this.Installment.trim() == "Quarterly") {
      this.showDiscountAmt = true;
      this.percentage = 0.04;
      this.oneTime = false;
    } else if (this.Installment.trim() == "Half-Yearly") {
      this.showDiscountAmt = true;
      this.percentage = 0.03;
      this.oneTime = false;
    } else {
      this.oneTime = true;
      this.percentage = 0;
      this.Installments = 0;
      this.pet = "OneYears";
    }
    if (this.pet.trim() == "TwoYears") {
      this.pet = "TwoYears";
      this.selectedYr = '2'
      this.percent = " (5% off)"
      // this.showDiscountAmt = false;
      if (this.Installment.trim() == "Monthly") {
        this.Installments = 24;
      } else if (this.Installment.trim() == "Quarterly") {
        this.Installments = 8;
      } else if (this.Installment.trim() == "Half-Yearly") {
        this.Installments = 4;
      } else {
        this.Installments = 0;
      }
      this.sumInsuredListToShow = (this.sumInsuredListToShow2YrM);
    } else if (this.pet.trim() == "ThreeYears") {
      this.pet = "ThreeYears";
      this.selectedYr = '3'
      this.percent = " (10% off)"
      // this.showDiscountAmt = false;
      if (this.Installment.trim() == "Monthly") {
        this.Installments = 36;
      } else if (this.Installment.trim() == "Quarterly") {
        this.Installments = 12;
      } else if (this.Installment.trim() == "Half-Yearly") {
        this.Installments = 6;
      } else {
        this.Installments = 0;
      }
      this.sumInsuredListToShow = (this.sumInsuredListToShow3YrM);
    } else {
      this.pet = "TwoYears";
      this.selectedYr = '2'
      this.percent = " (5% off)"
      // this.showDiscountAmt = false;
      if (this.Installment.trim() == "Monthly") {
        this.Installments = 24;
      } else if (this.Installment.trim() == "Quarterly") {
        this.Installments = 8;
      } else if (this.Installment.trim() == "Half-Yearly") {
        this.Installments = 4;
      } else {
        this.Installments = 0;
      }
      this.sumInsuredListToShow = (this.sumInsuredListToShow2YrM);
    }

    if (this.Installment.trim() != "One time") {
      this.sumInsuredListToOperate = this.sumInsuredListToShow2Yr;
      for (let i = 0; i < this.sumInsuredListToOperate.length; i++) {
        if (this.Installment.trim() == "Monthly") {
          this.Installments = 24;
        } else if (this.Installment.trim() == "Quarterly") {
          this.Installments = 8;
        } else if (this.Installment.trim() == "Half-Yearly") {
          this.Installments = 4;
        } else {
          this.Installments = 0;
        }
        let loadingAmt = Number(this.sumInsuredListToOperate[i].discountAmt) * this.percentage;
        let loadingPreAmt = Number(this.sumInsuredListToOperate[i].discountAmt) + loadingAmt;
        let installmentAmt = Number(loadingPreAmt) / this.Installments;
        let amtWithGST = Number(installmentAmt) * 0.18;
        let amtWithoutGst = installmentAmt + amtWithGST;
        let preAmt = amtWithoutGst;
        // console.log("loadingAmt", loadingAmt);
        // console.log("loadingPreAmt", loadingPreAmt);
        // console.log("installmentAmt", installmentAmt);
        // console.log("amtWithGST", amtWithGST);
        // console.log("amtWithoutGst", amtWithoutGst);
        // console.log("preAmt", preAmt);
        // Object.assign(this.sumInsuredListToShow2YrM[i], { Amt: this.sumInsuredListToOperate[i].Amt, PremiumAmount: Math.round(preAmt) });
        this.sumInsuredListToShow2YrM.push({ Amt: this.sumInsuredListToOperate[i].Amt, PremiumAmount: Math.round(preAmt) })
      }
      this.sumInsuredListToOperate1 = this.sumInsuredListToShow3Yr;
      for (let i = 0; i < this.sumInsuredListToOperate1.length; i++) {
        if (this.Installment.trim() == "Monthly") {
          this.Installments = 36;
        } else if (this.Installment.trim() == "Quarterly") {
          this.Installments = 12;
        } else if (this.Installment.trim() == "Half-Yearly") {
          this.Installments = 6;
        } else {
          this.Installments = 0;
        }
        let loadingAmt = Number(this.sumInsuredListToOperate1[i].discountAmt) * this.percentage;
        let loadingPreAmt = Number(this.sumInsuredListToOperate1[i].discountAmt) + loadingAmt;
        let installmentAmt = Number(loadingPreAmt) / this.Installments;
        let amtWithGST = Number(installmentAmt) * 0.18;
        let amtWithoutGst = installmentAmt + amtWithGST;
        let preAmt = amtWithoutGst;
        // console.log("loadingAmt", loadingAmt);
        // console.log("installmentAmt", installmentAmt);
        // console.log("loadingPreAmt", loadingPreAmt);
        // console.log("amtWithoutGst", amtWithoutGst);
        // console.log("amtWithGST", amtWithGST);
        // console.log("preAmt", preAmt);
        // Object.assign(this.sumInsuredListToShow3YrM[i], { Amt: this.sumInsuredListToOperate1[i].Amt, PremiumAmount: Math.round(preAmt) });
        this.sumInsuredListToShow3YrM.push({ Amt: this.sumInsuredListToOperate1[i].Amt, PremiumAmount: Math.round(preAmt) })
      }

      if (this.pet.trim() == "TwoYears") {
        this.sumInsuredListToShow = this.sumInsuredListToShow2YrM;
      } else if (this.pet.trim() == "ThreeYears") {
        this.sumInsuredListToShow = this.sumInsuredListToShow3YrM;
      }
    } else {
      // if (this.pet.trim() == "TwoYears") {
      //   this.pet = "TwoYears";
      //   this.selectedYr = '2'
      //   this.percent = " (5% off)"
      //   this.showDiscountAmt = false;
      //   this.sumInsuredListToShow = this.sumInsuredListToShow2Yr;
      // } else if (this.pet.trim() == "ThreeYears") {
      //   this.pet = "ThreeYears";
      //   this.selectedYr = '3'
      //   this.percent = " (10% off)"
      //   this.showDiscountAmt = false;
      //   this.sumInsuredListToShow = this.sumInsuredListToShow3Yr;
      // } else if (this.pet.trim() == "OneYears") {
      this.pet = "OneYears";
      this.selectedYr = '1'
      this.showDiscountAmt = true;
      this.sumInsuredListToShow = this.sumInsuredListToShow1Yr;
      // }
    }
    console.log(this.sumInsuredListToShow2YrM);
    console.log(this.sumInsuredListToShow3YrM);


  }
  okayClicked() {
    this.navCtrl.push(LoginPage);
  }
}
