import { Component, NgModule } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, App, Nav, ToastController, LoadingController } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { LoginPage } from '../../health/login/login';
import { TProposerDetailsPage } from '../t-proposer-details/t-proposer-details';

/**
 * Generated class for the TBuyPageResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    TBuyPageResultPage,
  ],
  imports: [
    //IonicPageModule.forChild(TBuyPageResultPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-t-buy-page-result',
  templateUrl: 't-buy-page-result.html',
})
export class TBuyPageResultPage {
  //showPopup = true;
  //successLabel = true;
  //buyPopup = true;

  sendData;
  PolicyTypeSelected;
  canShowToast = true;
  barChartLabels:string[];
  barChartLabelsFamily: string[];
  barChartAmountFamily: string[];
  barChartSavingFamily: string[];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  public barChartData:any;
  disableProceedBt = true;
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    title: {
      display: true,
      text: 'Yearly Comparison of Premium'
    },
    legend: {position: 'bottom',labels: { boxWidth: 15, boxHeight: 2 }},
    scales: {
      xAxes: [{ stacked: true, barPercentage: 0.4 }],
      yAxes: [{ stacked: true, barPercentage: 1.3, scaleLabel:{display: true, labelString: 'Total Premium'}}]
    }
  };
  public barChartColors:Array<any> = [
    {
      backgroundColor: '#C70039',
      borderColor: '#C70039',
      pointBackgroundColor: '#C70039',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#C70039'
    },
    {
      backgroundColor: '#FF5733',
      borderColor: '#FF5733',
      pointBackgroundColor: '#FF5733',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#FF5733'
    },
  ];
  QuickQuotePlans = []; beneficiaryDetails = []; selectedPlanArray  =[];
  apiPlanType; appData;insuredPlanTitle;
  NameOfEmailProposerIndividual:any;
  NameOfSMSProposerIndividual:any;
  sendEmailList = [{"value":""}];
  emailTo=[];
  sendSMSList = [{"value":""}];
  smsTo;
  selectionCard:boolean;
  delIcon:boolean = true;
  floaterDiv:boolean = true;
  individualDiv:boolean = true;
  graph:boolean = true;
  card:boolean = false;
  closeBtn:boolean = false;
  panelOpenState: boolean = false;
  membersInsured:any;
  encryptMemberData;
  tempMemberArray : any;  
  premiums:any;
  basevalue:any;
  smokingState:boolean;
  indiviDiscount:any;
  policyType:any = "individual";
  planType: string = "";
  total:any;
  finalTotal:any = 0;
  termOfTwoAmount:any;
  termOfTwoDiscountAmount:any;
  termOfThreeAmount:any;
  termOfThreeDiscountAmount:any;
  twoYearSaveAmount:any;
  threeYearSaveAmount:any;
  oneTimecards = [];
  monthlyCards = [];
  quarterlyCards = [];
  halfyearlyCards = [];
  monthlyOneYearAmount:any;
  monthlyTwoYearAmount:any;
  monthlyThreeYearAmount:any;
  quarterlyOneYearAmount:any;
  quarterlyTwoYearAmount:any;
  quarterlyThreeYearAmount:any;
  halfyearlyOneYearAmount:any;
  halfyearlyTwoYearAmount:any;
  halfyearlyThreeYearAmount:any;
  frequencyPercArray:any;
  monthlyPerc:any;
  quarterlyPerc:any;
  halfyearlyPerc:any;
  siAmountArray:any;
  discountPercArray:any;
  oneyearDiscountPerc:any;
  twoyearDiscountPerc:any;
  threeyearDiscountPerc:any;
  policyterm:any;
  policyTermOneYear:any;
  policyTermTwoYear:any;
  policyTermThreeYear:any;
  oneMonthCount:any;
  twoMonthCount:any;
  threeMonthCount:any;
  shownGroup:any;
  show= false;
  slide="";
  activeMemberCode:any;
  activememberName:any;
  forQuickQuote = false;
  insurenceAmtToShow:any;
  maximumInsuAmtCode:any;
  insuredType = false;
  threeYearFltCards= [];
  individualGetQuoteArray = [];
  petFamily = "OneYears";
  graphShow:boolean = false;
  showYearValue:boolean = true;
  pet:string = "OneTime";
  openEdit = true;
  closeEdit = false;
  familyInsurence = '';
  familyGraph = false;
  installments; 
  installTyp;
  NumYear = 0;
  hideOneYear = false;
  yrDiscount= "";
  familySumInsured = '';
  vitalMembers=["SELF","SPOU","SON","DAUG","PARE","CHLD"];
  individualDisplayData:any;
  resultData:any;
  hideProccedBtn = false;
  loading;
  ENQ_PolicyResponse;
  ENQ_FamilyInsuredPlanType;
  ENQ_FamilyInsuredPlanTypeOld;
  ENQ_FamilyInsurancePlans = [];
  ENQ_FamilySelectedAmt;
  ReferenceNo = "";
  QuotationID ="";
  familyIndex = -1;
  clickChangeColor;
  insuranceDetailsToSend = [];
  oldNumberOfMembers;
  newNumberOfMembers;
  showPopup = false;
  cardUID;
  basePremium;
  hidePremiumPopup = true;
  familyServiceCallPlanIndex = 0;
  deductableDiscount;
  employeeDiscount;
  termPremium;
  longTermDiscount;
  premiumwithoutservTax;
  installmentLoading;
  premiumWithLoading;
  totalInstallment;
  premiumAmount;
  serviceTax;
  premiumWithServiceTax;
  policyGroup;
  transparentDiv = true;
  smokingLoad = 0;
  hideFamilyShare = true;
  hideIndividualShare = true;
  buyPopup=false;
  NameOfEmailsmsProposer;
  email;
  mobile;
  zerodiscount = true;
  ClientDetails = [];
  spouseGender = "F";
  pID;
  successLabel = true;
  totalLoading;
  bmiLoad;
  voluntaryPopup = true;
  QID;
  amountList;
  tempAmount = [];
  amount = 0;
  medicalloading;
   voluntarydeduction;
  amountDiscount = 0;
  thankyouPopup = true;
  smokingloadSelf;
  showDisc = false;
  serviceResponsePopup = true;
  message;
  serviceCodeStatus;
  selectedPlanType;
  selectedPlanTypeBoolean = false;
  deductableAmount; 
  sIAmount;
  SIArray = [];
  showDiv = true;
  constructor(private platform: Platform,public navCtrl: NavController, public  app: App, public loadingCtrl: LoadingController, public toast: ToastController, public navParams: NavParams, public appService: AppService) {

    //console.log("display: " + navParams.get("displayMembers"));
    this.membersInsured = navParams.get("displayMembers");    
    console.log("member: " + JSON.stringify(this.membersInsured));  
    this.deductableAmount = navParams.get("deductableAmount");
    this.sIAmount = navParams.get("siAmounut");
    this.SIArray = navParams.get("SuminsuredArray");

    // platform.registerBackButtonAction(() => {
    //   console.log("backPressed 1");
    // },1);
     
    //this.memberArray = JSON.parse(sessionStorage.quickQuoteMembersDetails);
    // this.ENQ_FamilyInsuredPlanType = this.membersInsured[0].insuredPlanTypeText;
    // this.ENQ_FamilyInsuredPlanTypeOld = this.membersInsured[0].insuredPlanTypeText;
    // this.ENQ_FamilyInsuredPlanType = this.membersInsured[0].insuredPlanTypeText;
    // this.ENQ_FamilyInsuredPlanTypeOld = this.membersInsured[0].insuredPlanTypeText;
    this.tempMemberArray =  navParams.get("displayMembers"); 
    this.appData = JSON.parse(localStorage.AppData);
    this.apiPlanType = this.appData.Masters.PlanType;
    this.premiums = this.appData.Premiums;
    this.QuickQuotePlans = this.appService.getSumInsuredArray(this.apiPlanType);
    this.installments = this.appData.Masters.Installments; 
    this.installTyp = this.installments[0].FGCode;
    this.smokingLoad = this.appData.Masters.Loading.smokingload[0].perc;

    // platform.registerBackButtonAction(() => {
    //   this.backButtonFunction();
    // });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TBuyPageResultPage');
    setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);

      console.log("Select" + this.selectedPlanType);
      //this.buyPopup = false;  

      this.loadDisplayMemberData(); 
      //this.getDeductableAmountList();
      this.transparentDiv = true;   
      console.log("back State: " + sessionStorage.isViewPopped);
      if(sessionStorage.isViewPopped == "true"){
        this.membersInsured = JSON.parse(sessionStorage.ProposerDetails);
        this.insuranceDetailsToSend = JSON.parse(sessionStorage.planCardDetails);    
        console.log("cardData: " + JSON.stringify(this.insuranceDetailsToSend));
          
        sessionStorage.isViewPopped = "";
        
      }
      this.successLabel = true;
      this.setStoredData();
  }

  ionViewDidEnter(){
    if(sessionStorage.isViewPopped == "true"){
      this.membersInsured = JSON.parse(sessionStorage.ProposerDetails);
      this.insuranceDetailsToSend = JSON.parse(sessionStorage.planCardDetails);    
      console.log("cardData: " + JSON.stringify(this.insuranceDetailsToSend));
      this.buyPopup = true;
      this.serviceResponsePopup = true;
      this.NameOfEmailsmsProposer = sessionStorage.propName;
      this.email = sessionStorage.email;
      this.mobile = sessionStorage.mobile;         
      //sessionStorage.isViewPopped = "";
      this.successLabel = true;
    }
  }

  setStoredData(){
    this.NameOfEmailsmsProposer = this.membersInsured[0].proposerName;
    this.email = this.membersInsured[0].email;
    this.mobile = this.membersInsured[0].mobile;
  }

  onBackClick(){
    sessionStorage.isViewPopped = "";
    this.navCtrl.pop();
  }


  backButtonFunction(){
    this.navCtrl.pop();
  }

  selectPlan(){
    console.log("ClickSelect" + this.selectedPlanType);  
    if(this.selectedPlanTypeBoolean == false){
      this.selectedPlanType = "Supreme";
      this.loadDisplayMemberData();
    }else{
      this.selectedPlanType = "Elite";
      this.loadDisplayMemberData();
    }
    
  }



  onSelectChange(selectedValue: any) {
    console.log(selectedValue+">>>>>>>>>>>>>>>>>>>");
    
    if(selectedValue == 'FULL'){
      this.petFamily = "OneYears";
      this.hideOneYear = false;
      this.changeMemberInsuranceForService(3);
      this.getFamilyFoaterRespose();
    }else{   
      this.petFamily = "TwoYears";    
      this.hideOneYear = true;
      
      this.getFamilyFoaterRespose();
      this.changeMemberInsuranceForService("2");
    }
  }

  changeMemberInsuranceForService(Duration){
    console.log("duration: " + JSON.stringify(Duration)); 
   
    this.insuranceDetailsToSend = [];
    this.threeYearFltCards = [];
    this.barChartLabelsFamily = [];
    this.barChartAmountFamily= [];
    this.barChartSavingFamily= [];
    this.insuranceDetailsToSend = [];
    this.disableProceedBt = true;
    for(let i = 0 ;  i < this.ENQ_FamilyInsurancePlans.length ; i++){
      if(this.installTyp == (this.ENQ_FamilyInsurancePlans[i].Installment).toUpperCase() && Duration == this.ENQ_FamilyInsurancePlans[i].DurationYears){
        //this.threeYearFltCards.push(this.ENQ_FamilyInsurancePlans[i]);
        console.log("Family : " +  JSON.stringify(this.ENQ_FamilyInsurancePlans[i]));
        this.ENQ_FamilyInsurancePlans[i].selected = true;
        this.ENQ_FamilyInsurancePlans[i].clickColorChange = false;
        if(this.ENQ_FamilyInsurancePlans[i].Request.BeneficiaryDetails.Member[0].SumInsured == "300000" && Duration == "3" && this.ENQ_FamilyInsurancePlans[i].Installment == "full"){
          console.log("IF");
          this.ENQ_FamilyInsurancePlans[i].hideInstalment = false;          
        }
        if(this.ENQ_FamilyInsurancePlans[i].disPersec == 0){
          this.zerodiscount = true;
        }else{
          this.zerodiscount = false;
        }

        this.threeYearFltCards.splice(Number(this.ENQ_FamilyInsurancePlans[i].IndexID), 0, this.ENQ_FamilyInsurancePlans[i]);
        this.barChartLabelsFamily.splice(Number(this.ENQ_FamilyInsurancePlans[i].IndexID), 0, this.ENQ_FamilyInsurancePlans[i].sumInsuredTitle);
        this.barChartAmountFamily.splice(Number(this.ENQ_FamilyInsurancePlans[i].IndexID), 0, this.ENQ_FamilyInsurancePlans[i].showPrice);
        this.barChartSavingFamily.splice(Number(this.ENQ_FamilyInsurancePlans[i].IndexID), 0, this.ENQ_FamilyInsurancePlans[i].savings);
      }
    }
    console.log("ThreeCardArray: " +  JSON.stringify(this.threeYearFltCards));      
    
    if(this.familyServiceCallPlanIndex == this.selectedPlanArray.length){
      this.loading.dismiss();
    }
    this.hideFamilyShare = true;
            
  }

  loadDisplayMemberData(){
    this.insuranceDetailsToSend = [];
    if(this.membersInsured[0].PolicyType == "HTI"){
      this.insuredType = false;
    }else{
      this.insuredType = true;
      this.openEdit = true;
    }
    this.siAmountArray = this.appService.getSumInsuredArray(this.appData.Masters.PlanType);

    if(this.membersInsured[0].PolicyType == "HTI"){

      this.presentLoadingDefault();  
      this.getBuyPageServiceData();
      this.maximumInsuAmtCode = this.membersInsured[0].insuredCode;
      this.individualDiv = this.individualDiv;
      this.floaterDiv = !this.floaterDiv;
    }else{
      this.showDiv = false;
      this.setSIAmountValue();
      this.familyInsurence = this.membersInsured[0].insuredCode;
      this.PolicyTypeSelected = this.membersInsured[0].PolicyType;

      this.getFamilyFoaterRespose();
    }
  }

  getBuyPageServiceData(){
    this.PolicyTypeSelected = this.membersInsured[0].PolicyType;
    this.beneficiaryIndvDetailsArray();
    let callRequest = this.appService.callServiceForTopup(this.PolicyTypeSelected,this.beneficiaryDetails, this.membersInsured);
    console.log("sendData: " + JSON.stringify(callRequest));
    
    var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
    this.getServiceResponse({'request': sendData},"");  
    
  }

  getFamilyFoaterRespose(){
    console.log("state: " + this.zerodiscount);
     this.pet == "OneTime";
     this.ENQ_FamilyInsurancePlans = [];
     this.familyIndex = -1;
     this.presentLoadingDefault();
     this.selectedPlanArray = [];
     this.familyServiceCallPlanIndex = 0;
     console.log("quickquoteArray" + JSON.stringify(this.QuickQuotePlans));
     
    //  for(let i = 0 ; i < this.QuickQuotePlans.length ; i++){
    //    if(this.ENQ_FamilyInsuredPlanType == (this.QuickQuotePlans[i].title).toUpperCase() ){
    //      if(this.familyInsurence == this.QuickQuotePlans[i].sicode){
    //        this.ENQ_FamilySelectedAmt = this.QuickQuotePlans[i].suminsured;
    //        this.familySumInsured = this.QuickQuotePlans[i].amtTitle;
    //        this.ENQ_FamilyInsuredPlanTypeOld = (this.QuickQuotePlans[i].title).toUpperCase();
    //      }
    //      this.selectedPlanArray.push(this.QuickQuotePlans[i]);
    //    }
    //  }
    // for(let j=0; j<this.SIArray.length; j++){
    //   this.callFamilyService(this.SIArray[j].SI);   
    // }

    this.callFamilyService();   
     
   }

   callFamilyService(){
     //console.log("SendingAmount" + SIArrayAmount);     
    console.log("state2: " + this.zerodiscount);
    console.log("selected Planned array"  + JSON.stringify(this.selectedPlanArray));
    
    if(this.familyServiceCallPlanIndex < this.SIArray.length){
      let amount;
      // var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest(InsuredCardDetails)),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
      // console.log("testt: " + JSON.stringify(sendData));
      // this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData}); 
      for(let i=0;i<this.SIArray.length; i++){
        amount = this.SIArray[this.familyServiceCallPlanIndex].SI;
      }
      let beneficiaryDetail =  this.beneficiaryDetailsArray(amount);
      let callRequest = this.appService.callServiceForTopup(this.PolicyTypeSelected,beneficiaryDetail, this.membersInsured);
      //let callRequest = this.getProceedrequest(this.insuranceDetailsToSend);
      callRequest.Installments = [this.installTyp];     
      console.log(callRequest);
      console.log(this.familyInsurence + " : "+this.ENQ_FamilySelectedAmt );
      console.log("Data b4 encryp: "+JSON.stringify(callRequest)+" : "+sessionStorage.TokenId);
      
      var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
      console.log("Data aftr encryp: "+sendData);
      this.familyIndex++;
      this.getServiceResponse({'request': sendData},this.familyIndex);
      this.familyServiceCallPlanIndex++;  
    }
    this.hideFamilyShare = true;

  }


  getServiceResponse(serviceData,IndexNumber){
    //this.presentLoadingDefault();
    //this.loading.present();
    console.log("state3: " + this.zerodiscount);
    console.log("sendData: " + JSON.stringify(serviceData));
    this.pet = "OneTime";
    this.oneTimecards = [];
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    console.log("header: " + JSON.stringify(headerString));
    
    this.appService.callService("QuotePurchase.svc/ENQ_Policy",serviceData, headerString)
      .subscribe(ENQ_Policy =>{
        console.log("EnquiryData: " + JSON.stringify(ENQ_Policy));
        if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "0"){
			sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.ENQ_PolicyResponse  = ENQ_Policy.ENQ_PolicyResult.Data;
          if(this.membersInsured[0].PolicyType == "HTI"){ 
            this.loading.dismiss();
            this.voluntaryPopup = true;
            //this.appService.showloading.dismiss();      
            this.ReferenceNo = this.ENQ_PolicyResponse.RefrenceNo;
            this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
            this.individualDisplayData = this.ENQ_PolicyResponse.PurchaseResponse;
            for(let i = 0; i < this.individualDisplayData.length; i++){
              if(this.individualDisplayData[i].Installment == "FULL"){
                // if(this.individualDisplayData[i].Result == null){
                //   this.resultData = this.individualDisplayData[i].Result_sig;
                // }else{
                //   this.resultData = this.individualDisplayData[i].Result;
                // }       
                this.resultData = this.individualDisplayData[i].Result_topup;       
                //let disAmount = parseFloat(this.resultData.Root.Policy.OutputRes.PremWithServTax);
                //let disAmount = parseFloat(this.resultData.Root.Policy.OutputRes.PremWithServTax) - parseFloat(this.resultData.Root.Policy.OutputRes.LngTrmDisc);
                let disAmount = parseFloat(this.resultData.Root.Policy.OutputRes.PremWithServTax);
                let yearAmt = parseFloat(this.resultData.Root.Policy.OutputRes.PremWithServTax) + parseFloat(this.resultData.Root.Policy.OutputRes.LngTrmDisc);
                //let discountPercenage = parseFloat(this.resultData.Root.Policy.OutputRes.FmlyDiscRate) + parseFloat(this.resultData.Root.Policy.OutputRes.LngTrmDiscRate);
                let discountPercenage;
                if(this.resultData.Root.Policy.OutputRes.LngTrmDiscRate == 0){
                 discountPercenage = "";
                 this.showDisc = true;
                }else{
                 discountPercenage = parseFloat(this.resultData.Root.Policy.OutputRes.LngTrmDiscRate);
                 this.showDisc = false;
                }
                this.oneTimecards.push({ ptTitle: this.individualDisplayData[i].Duration, 
                                         //yearAmount: this.addCommas(Number(yearAmt).toFixed()),
                                         yearAmount: this.addCommas(Number(yearAmt).toFixed()),
                                         discountAmount: this.addCommas(Number(disAmount).toFixed()),
                                         saveAmount: this.addCommas(Number(this.resultData.Root.Policy.OutputRes.LngTrmDisc)),
                                         totalDiscount:discountPercenage,
                                         selected: true,
                                         Installment: "Full",
                                         UID: this.individualDisplayData[i].UID,
                                         clickColorChange: false,
                                         Request:this.resultData.Root.Policy.InputParameters,
                                         CalculatedServiceResp:this.resultData.Root.Policy.OutputRes,
                                         ReferenceNo:this.ReferenceNo,
                                         QuotationID: this.QuotationID,
                                         hideInstalmentIndiv: true,
                                         showDiscountPerc: this.showDisc
                          });                         
                          if(sessionStorage.posVerification == "Y"){
                            if(this.individualDisplayData[i].Duration == "3" && this.individualDisplayData[i].Installment == "FULL"){
                              console.log("hide");
                              this.oneTimecards[i].hideInstalmentIndiv = false;          
                            }  
                          }                                                        
                    }
                }   
                console.log("testOneTime" + JSON.stringify(this.oneTimecards));     
                // if(this.oneTimecards[0].totalDiscount == 0){
                //   console.log("IF");
                  
                //   this.showDisc = true;
                // }      
          }else if(this.membersInsured[0].PolicyType == "HTF"){
            this.voluntaryPopup = true;
            this.loading.dismiss();
            this.ReferenceNo = this.ENQ_PolicyResponse.RefrenceNo;
            this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
            this.getFloatingDetails(ENQ_Policy.ENQ_PolicyResult.Data,IndexNumber);
          }
          
        }else if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "807"){
          this.loading.dismiss();
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          //this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
          //this.navCtrl.push(LoginPage);
          this.serviceResponsePopup = false;
          this.message = ENQ_Policy.ENQ_PolicyResult.ReturnMsg;
          this.serviceCodeStatus = 807;
        }else if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          //this.navCtrl.push(LoginPage);
          this.serviceResponsePopup = false;
          this.message = ENQ_Policy.ENQ_PolicyResult.ReturnMsg;
          this.serviceCodeStatus = 500;
        }else{
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          console.log("Service resp: "+JSON.stringify(ENQ_Policy));
          this.loading.dismiss(); 
          //this.appService.showloading.dismiss(); 
          //this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
          this.serviceResponsePopup = false;
          this.message = ENQ_Policy.ENQ_PolicyResult.ReturnMsg;
          this.serviceCodeStatus = 400;
        }


      });
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }

  getPolicyTermOneTimeDisplayAmount(){
    this.oneTimecards = [];
    this.disableProceedBt = true;
    //this.showGraph(1);
    this.hideIndividualShare = true;
    this.insuranceDetailsToSend = [];
    //this.getTotalMemberArraySI();
    // if(this.graphShow == true){
    //  this.showGraph(1);
    // }
    for(let i = 0; i < this.individualDisplayData.length; i++){
      if(this.individualDisplayData[i].Installment == "FULL"){
        // if(this.individualDisplayData[i].Result == null){
        //   this.resultData = this.individualDisplayData[i].Result_sig;
        // }else{
        //   this.resultData = this.individualDisplayData[i].Result;
        // }     
        this.resultData = this.individualDisplayData[i].Result_topup;                      
        //let disAmount = parseFloat(this.resultData.Root.Policy.OutputRes.PremWithServTax) - parseFloat(this.resultData.Root.Policy.OutputRes.LngTrmDisc);
        let disAmount = parseFloat(this.resultData.Root.Policy.OutputRes.PremWithServTax);
        let yearAmt = parseFloat(this.resultData.Root.Policy.OutputRes.PremWithServTax) + parseFloat(this.resultData.Root.Policy.OutputRes.LngTrmDisc);
        let discountPercenage;
        if(this.resultData.Root.Policy.OutputRes.LngTrmDiscRate == 0){
         discountPercenage = "";
         this.showDisc = true;
        }else{
         discountPercenage = parseFloat(this.resultData.Root.Policy.OutputRes.LngTrmDiscRate);
         this.showDisc = false;
        }
        this.oneTimecards.push({ ptTitle: this.individualDisplayData[i].Duration, 
                                 yearAmount: this.addCommas(Number(yearAmt).toFixed()),
                                 discountAmount: this.addCommas(Number(disAmount).toFixed()),         
                                 saveAmount: this.addCommas(Number(this.resultData.Root.Policy.OutputRes.LngTrmDisc)),
                                 totalDiscount:discountPercenage,
                                 selected: true,
                                 Installment: "Full",
                                 UID: this.individualDisplayData[i].UID,
                                 clickColorChange: false,
                                 Request:this.resultData.Root.Policy.InputParameters,
                                 CalculatedServiceResp:this.resultData.Root.Policy.OutputRes,
                                 ReferenceNo:this.ReferenceNo,
                                 QuotationID: this.QuotationID,
                                 hideInstalmentIndiv: true,
                                 showDiscountPerc: this.showDisc 
        })
         
        console.log("One: " + JSON.stringify(this.oneTimecards));                              
      }      
      if(this.individualDisplayData[i].Duration == "3" && this.individualDisplayData[i].Installment == "FULL"){
        console.log("IF");
        this.oneTimecards[i].hideInstalmentIndiv = false;          
      }   
    //console.log("Data : " + JSON.stringify());
    }
      
         


  }

  getPolicyTermMonthlyDisplayAmount(){
    this.monthlyCards = [];
    //this.showGraph(1);
    this.disableProceedBt = true;
    this.hideIndividualShare = true;
    this.insuranceDetailsToSend = [];
  // this.getTotalMemberArraySI();
/*    if(this.graphShow == true){
     this.showGraph(1);
    } */
    for(let i = 0; i < this.individualDisplayData.length; i++){
      if(this.individualDisplayData[i].Installment == "MONTHLY" && this.individualDisplayData[i].Duration !=1){            
        
        // if(this.individualDisplayData[i].Result == null){
        //   this.resultData = this.individualDisplayData[i].Result_sig;
        // }else{
        //   this.resultData = this.individualDisplayData[i].Result;
        // }       
        this.resultData = this.individualDisplayData[i].Result_topup;                           
            this.monthlyCards.push({ ptTitle: this.individualDisplayData[i].Duration, 
                                     installmentTitle: (this.individualDisplayData[i].Installment).toLowerCase(),
                                     discountAmount: this.addCommas(Number(this.resultData.Root.Policy.OutputRes.PremWithServTax)),
                                     //saveAmount: this.resultData.Root.Policy.OutputRes.LngTrmDisc,
                                     installmentLoading: this.resultData.Root.Policy.OutputRes.FmlyDiscRate,
                                     selected: true,
                                     Installment: "Monthly",
                                     UID: this.individualDisplayData[i].UID,
                                     clickColorChange: false,
                                     Request:this.resultData.Root.Policy.InputParameters,
                                     CalculatedServiceResp:this.resultData.Root.Policy.OutputRes,
                                     ReferenceNo:this.ReferenceNo,
                                     QuotationID: this.QuotationID
            })                 
        }
    }

  }

  getPolicyTermQuarterlyDisplayAmount(){
    this.quarterlyCards = [];
    //this.showGraph(1);
    this.disableProceedBt = true;
    this.hideIndividualShare = true;
    this.insuranceDetailsToSend = [];
   //this.getTotalMemberArraySI();
  /*  if(this.graphShow == true){
     this.showGraph(1);
    }
 */
    for(let i = 0; i < this.individualDisplayData.length; i++){
      if(this.individualDisplayData[i].Installment == "QUARTERLY" && this.individualDisplayData[i].Duration !=1){            
        
        // if(this.individualDisplayData[i].Result == null){
        //   this.resultData = this.individualDisplayData[i].Result_sig;
        // }else{
        //   this.resultData = this.individualDisplayData[i].Result;
        // }          
        this.resultData = this.individualDisplayData[i].Result_topup;                        
            this.quarterlyCards.push({ ptTitle: this.individualDisplayData[i].Duration, 
                                     installmentTitle: (this.individualDisplayData[i].Installment).toLowerCase(),
                                     discountAmount: this.addCommas(Number(this.resultData.Root.Policy.OutputRes.PremWithServTax)),
                                     //saveAmount: this.resultData.Root.Policy.OutputRes.LngTrmDisc,
                                     installmentLoading: this.resultData.Root.Policy.OutputRes.FmlyDiscRate,
                                     selected: true,
                                     Installment: "Quarterly",
                                     UID: this.individualDisplayData[i].UID,
                                     clickColorChange: false,
                                     Request:this.resultData.Root.Policy.InputParameters,
                                     CalculatedServiceResp:this.resultData.Root.Policy.OutputRes,
                                     ReferenceNo:this.ReferenceNo,
                                     QuotationID: this.QuotationID
            })                 
        }
    }
  
  }

  getPolicyTermHalfyearlyDisplayAmount(){
    this.halfyearlyCards = [];
    this.disableProceedBt = true;
    this.hideIndividualShare = true;
    //this.showGraph(1);
    this.insuranceDetailsToSend = [];
   //this.getTotalMemberArraySI();
/*    if(this.graphShow == true){
     this.showGraph(1);
    }   */

    for(let i = 0; i < this.individualDisplayData.length; i++){
      if(this.individualDisplayData[i].Installment == "HALFYEARLY" && this.individualDisplayData[i].Duration !=1){            
        
        // if(this.individualDisplayData[i].Result == null){
        //   this.resultData = this.individualDisplayData[i].Result_sig;
        // }else{
        //   this.resultData = this.individualDisplayData[i].Result;
        // }   
        this.resultData = this.individualDisplayData[i].Result_topup;                               
            this.halfyearlyCards.push({ ptTitle: this.individualDisplayData[i].Duration, 
                                     installmentTitle: (this.individualDisplayData[i].Installment).toLowerCase(),
                                     discountAmount: this.addCommas(Number(this.resultData.Root.Policy.OutputRes.PremWithServTax)),
                                     //saveAmount: this.resultData.Root.Policy.OutputRes.LngTrmDisc,
                                     installmentLoading: this.resultData.Root.Policy.OutputRes.FmlyDiscRate,
                                     selected: true,
                                     Installment: "Half-Yearly",
                                     UID: this.individualDisplayData[i].UID,
                                     clickColorChange: false,
                                     Request:this.resultData.Root.Policy.InputParameters,
                                     CalculatedServiceResp:this.resultData.Root.Policy.OutputRes,
                                     ReferenceNo:this.ReferenceNo,
                                     QuotationID: this.QuotationID
            })                 
        }
    } 
  }

  getFloatingDetails(Data,IndexNo){
    //this.appService.showloading.dismiss(); 
    console.log("state4: " + this.zerodiscount);
    console.log("Data:: " + JSON.stringify(Data)); 
      for(let i = 0 ; i < Data.PurchaseResponse.length ; i++){    
        let calculatedArray;
        let serviceResquest;
        let hideOneYear = false;
        let hideInstalment = false;
        let hideDiscountDiv = true;
        let yearlyInvestments = 1;
        let showPrice;
        let disAmount;
        if(Data.PurchaseResponse[i].Result_topup.Root != null && Data.PurchaseResponse[i].Result_topup.Root.Policy != null){ 
          calculatedArray = Data.PurchaseResponse[i].Result_topup.Root.Policy.OutputRes;
          serviceResquest = Data.PurchaseResponse[i].Result_topup.Root.Policy.InputParameters;
          if(Data.PurchaseResponse[i].Installment == "FULL"){
            hideInstalment = true;
            hideDiscountDiv = false;
            showPrice = this.addCommas(Number(calculatedArray.PremWithServTax));          
            disAmount = this.addCommas(Number(calculatedArray.LngTrmDisc));
          }else if(Data.PurchaseResponse[i].Installment == "MONTHLY" && calculatedArray != null){   
            hideInstalment = true;              
            showPrice = this.addCommas(Number(calculatedArray.PremWithServTax));
           /*  showPrice = (Number(calculatedArray.PremWithServTax/(12 * Data.PurchaseResponse[i].Duration))).toFixed(0); */
          }else if(Data.PurchaseResponse[i].Installment == "QUARTERLY" && calculatedArray != null){  
            hideInstalment = true;                
            showPrice = this.addCommas(Number(calculatedArray.PremWithServTax));
            /* showPrice =  (Number(calculatedArray.PremWithServTax/(4 * Data.PurchaseResponse[i].Duration))).toFixed(0); */
          }else if(Data.PurchaseResponse[i].Installment == "HALFYEARLY" && calculatedArray != null){   
            hideInstalment = true;                 
            showPrice = this.addCommas(Number(calculatedArray.PremWithServTax));
           /*  showPrice =  (Number(calculatedArray.PremWithServTax/(2 * Data.PurchaseResponse[i].Duration))).toFixed(0); */
          }
        }else{
          hideOneYear = true;
          calculatedArray = 0;           
        }
  
        if(calculatedArray != null){
          let discountPercenage = (parseFloat(calculatedArray.FmlyDiscRate) + parseFloat(calculatedArray.LngTrmDiscRate));
          console.log("Discount; "+ discountPercenage);
          if(discountPercenage == 0){
            console.log("IF");
            this.zerodiscount = true;        
          }else{  
            this.zerodiscount = false;
          }
  
          if(this.ENQ_FamilySelectedAmt == serviceResquest.BeneficiaryDetails.Member[0].SumInsured){
            this.ReferenceNo = Data.RefrenceNo;
            this.QuotationID = Data.QuotationID;
            /* console.log(this.ReferenceNo + " : "+this.QuotationID); */
          }  
          
          if(sessionStorage.posVerification == "Y"){
            if(sessionStorage.Ispos == "true" && serviceResquest.BeneficiaryDetails.Member[0].SumInsured == "1000000"){
              console.log("ISPOS: " + "True");
              break;
              
            }
          }
  
          this.ENQ_FamilyInsurancePlans.push({
            "DurationYears" : Data.PurchaseResponse[i].Duration,
            "Installment" : (Data.PurchaseResponse[i].Installment).toLowerCase(),
            "CalculatedServiceResp":calculatedArray,
            "Request": serviceResquest,
            "UID": Data.PurchaseResponse[i].UID,
            "HideOneYear" : hideOneYear,
            "showPrice": showPrice,
            "oldPrice":  this.addCommas(Number(parseFloat(calculatedArray.PremWithServTax) +  parseFloat(calculatedArray.LngTrmDisc)).toFixed()),
            "selected":true,
            "disctAmt":disAmount,
            "hideInstalment": hideInstalment,
            "hideDiscountDiv" : hideDiscountDiv,
            "disPersec": discountPercenage,
            "sumInsured":serviceResquest.BeneficiaryDetails.Member[0].SumInsured,
            "sumInsuredTitle": this.digitTomileStone(serviceResquest.BeneficiaryDetails.Member[0].SumInsured),
            "IndexID" : IndexNo,
            "clickColorChange" : false,
            "savings":  parseFloat(calculatedArray.LngTrmDisc).toFixed(0),
            "ReferenceNo": Data.RefrenceNo,
            "QuotationID":Data.QuotationID
          });
        }
      }
  
      
      if(this.petFamily == "TwoYears"){
        this.changeMemberInsuranceForService("2");
      }else{
        this.changeMemberInsuranceForService("1");
      }
      this.callFamilyService();
    }

    digitTomileStone(amount){
      if (amount < 100000) {
        return Number(amount) / 1000 + "K";
        } else if (amount < 10000000) {
        return Number(amount) / 100000 + "L";
        } else if (amount < 100000000) {
        return Number(amount / 1000000) + "Cr";
        }
    }

    addCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      console.log("test: " + parts);
      return parts.join(".");   
  }

  proceedMemberDetailsPage(){
    if(this.NameOfEmailsmsProposer == undefined || this.NameOfEmailsmsProposer == null || this.NameOfEmailsmsProposer == ""){
      this.showToast("Please enter proposer name");
      //this.myInput.setFocus();
    }else if(this.email== undefined || this.email == null || this.email == ""){
      this.showToast("Please enter email id");
      //this.emailFocus.setFocus();
    }else if (!this.matchEmail(this.email)) {
      this.showToast("Please enter valid email id");  
      //this.emailFocus.setFocus();    
    }else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
      this.showToast("Please enter mobile number"); 
      //this.mobileFocus.setFocus();                            
    }else if(this.mobile.length < 10){
      this.showToast("Please enter 10 digit mobile number");   
      //this.mobileFocus.setFocus();         
    }else if(!this.isValidMobile(this.mobile)){
      this.showToast("Please enter valid mobile no");
      //this.mobileFocus.setFocus();
    }else{ 
      console.log("SessionData: " + sessionStorage.ProposerDetails);
          
      if(sessionStorage.isViewPopped == "true"){
        console.log("click: " +JSON.stringify(this.membersInsured));
        
        sessionStorage.ProposerDetails = JSON.stringify(this.membersInsured);
        console.log("click: " + sessionStorage.ProposerDetails);
        // this.membersInsured[0].proposerName = this.NameOfEmailsmsProposer;
        // this.membersInsured[0].proposerEmail = this.email;
        // this.membersInsured[0].mobile = this.mobile;
      }
      
      
      sessionStorage.propName = this.NameOfEmailsmsProposer;
      sessionStorage.email = this.email;
      sessionStorage.mobile = this.mobile;
      //sessionStorage.planCardDetails = JSON.stringify(this.insuranceDetailsToSend); 
      this.buyPopup = false;
      //this.navCtrl.push(TProposerDetailsPage,{"TbuyPageMemberDetailsResult": this.membersInsured});     
      this.encryptIncompleteData(this.insuranceDetailsToSend);            
      //this.navCtrl.push(BuyProposerDetailsPage,{"buyPageMemberDetailsResult": this.membersInsured,"buyPageCardDetailsResult":this.insuranceDetailsToSend});
    }    
    
  }

  encryptIncompleteData(InsuredCardDetails){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest(InsuredCardDetails)),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1Data(URL,serviceData){
    console.log("proceed: " + sessionStorage.TokenId);
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          console.log("test: "+ JSON.stringify(this.membersInsured));
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.membersInsured[0].proposerName = this.NameOfEmailsmsProposer;
          this.membersInsured[0].proposerEmail = this.email;
          this.membersInsured[0].mobile = this.mobile;
          this.sendEMailSMS(); 
          // this.navCtrl.push(TBuyPageResultPage,{"buyPageMemberDetailsResult": this.membersInsured,"buyPageCardDetailsResult":this.insuranceDetailsToSend});
          console.log("card: "+ JSON.stringify(this.individualDisplayData));
          console.log("member: "+ JSON.stringify(this.membersInsured));
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.loading.dismiss();
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.serviceResponsePopup = false;
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.serviceResponsePopup = false;
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceCodeStatus = 500;
          //this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.message = Resp.Proceed_PolicyResult.ReturnMsg;
          this.serviceResponsePopup = false;
          this.serviceCodeStatus = 400;
        }
        
    });      
  }


getProceedrequest(TracsactionDetails){
//this.QuotationID = TracsactionDetails.QuotationID;
//his.UID = TracsactionDetails.UID;  
console.log("trans" + JSON.stringify(TracsactionDetails));

let sendRequest = {
  // "QuotationID": localStorage.QuotationID,
  // "UID":localStorage.UID,
  "QuotationID": TracsactionDetails.QuotationID,
  "UID": TracsactionDetails.UID,
  "stage":"1",
  "Client": this.getClientNode(),
  "Risk": {
    "PolicyType": TracsactionDetails.Request.PolicyType ,//"HTF",
    "Duration": TracsactionDetails.Request.Duration,
    "Installments": TracsactionDetails.Request.Installments,
    "IsFgEmployee": sessionStorage.IsFGEmployee,
    "IsPos" : sessionStorage.Ispos,
    "BeneficiaryDetails":  TracsactionDetails.Request.BeneficiaryDetails.Member.length == undefined ? this.getSelfBeneficiaryDetails(TracsactionDetails)  : this.getBeneficiaryDetails(TracsactionDetails),
    "MemberDetails": this.membersInsured,
    "CardDetails": this.insuranceDetailsToSend
  }
}
console.log("CRTrequest: "+JSON.stringify(sendRequest));
return sendRequest;
}


getSelfBeneficiaryDetails(cardDetails){
  let BeneDetails = [];
  let mDob = cardDetails.Request.BeneficiaryDetails.Member.InsuredDob.split(" ")[0].split("/");
  let gendr = this.membersInsured[0].Gender;

  BeneDetails.push({
    "MemberId": cardDetails.Request.BeneficiaryDetails.Member.MemberId ,
    "InsuredName": this.membersInsured[0].proposerName,
    "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
    "InsuredGender": gendr,
    "InsuredOccpn": this.membersInsured[0].occupationCode,
    "CoverType": cardDetails.Request.BeneficiaryDetails.Member.CoverType,
    "SumInsured": cardDetails.Request.BeneficiaryDetails.Member.SumInsured,
    "DeductibleDiscount": cardDetails.Request.BeneficiaryDetails.Member.DeductibleDiscount,
    "Relation": cardDetails.Request.BeneficiaryDetails.Member.Relation,
    "NomineeName": "",
    "NomineeRelation": "",
    "AnualIncome": "",
    "Height": cardDetails.Request.BeneficiaryDetails.Member.Height,
    "Weight": cardDetails.Request.BeneficiaryDetails.Member.Weight,
    "NomineeAge": "",
    "AppointeeName": "",
    "AptRelWithominee": "",
    "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member.MedicalLoading,
    "PreExstDisease": "N",
    "DiseaseMedicalHistoryList": {
      "DiseaseMedicalHistory": {
        "PreExistingDiseaseCode": "",
        "MedicalHistoryDetail": ""
      }
    }
  })
  return BeneDetails;
}


getClientNode(){
  let age = this.membersInsured[0].age.split("-");
  let clientSalution = "MR";
  if(this.membersInsured[0].Gender == "F"){
    clientSalution = "MRS";
    this.spouseGender = "M";
  }
  this.ClientDetails.push({
    "ClientType":"I",
    "CreationType": "C",
    "Salutation": clientSalution,
    "FirstName": this.NameOfEmailsmsProposer ,
    "LastName": "",
    "DOB": age[0]+"/"+age[1]+"/"+age[2],
    "Gender":"M",
    "MaritalStatus": this.membersInsured[0].maritalCode,
    "Occupation": this.membersInsured[0].occupationCode,
    "PANNo": this.membersInsured[0].pan,
    "GSTIN": "",
    "AadharNo": "",
    "CKYCNo": "",
    "EIANo": "",
    "Address1": {
      "AddrLine1": this.membersInsured[0].address,
      "AddrLine2": "",
      "AddrLine3": "",
      "Landmark": "",
      "Pincode": this.membersInsured[0].pincode,
      "City": this.membersInsured[0].city,
      "State": this.membersInsured[0].state,
      "Country": "IND",
      "AddressType": "R",
      "HomeTelNo": "",
      "OfficeTelNo": "",
      "FAXNO": "",
      "MobileNo": this.mobile,
      "EmailAddr": this.email
    },
    "Address2": {
      "AddrLine1": "",
      "AddrLine2": "",
      "AddrLine3": "",
      "Landmark": "",
      "Pincode": this.membersInsured[0].pincode,
      "City": "",
      "State": "",
      "Country": "IND",
      "AddressType": "P",
      "HomeTelNo": "",
      "OfficeTelNo": "",
      "FAXNO": "",
      "MobileNo": this.mobile,
      "EmailAddr": this.email
    }
  });
  return this.ClientDetails[this.ClientDetails.length -1];

}

getBeneficiaryDetails(cardDetails){
  console.log("BuyPageResult: " + JSON.stringify(cardDetails));
  //let tempArray = this.membersInsured;
  //let tempCardArray = cardDetails.Request.BeneficiaryDetails.Member;
  //console.log("tempMember: " + JSON.stringify(tempArray));
  
  let BeneDetails = [];
  for(let i = 0 ;i< cardDetails.Request.BeneficiaryDetails.Member.length ; i++){
    for(let j = 0; j<this.membersInsured.length;j++){
      if(i==j){
        if( (this.membersInsured[j].code).toUpperCase()== (cardDetails.Request.BeneficiaryDetails.Member[i].Relation).toUpperCase() ){
          let mDob = cardDetails.Request.BeneficiaryDetails.Member[i].InsuredDob.split(" ");
          let gendr = "M";
          if(this.membersInsured[j].title.toLowerCase().indexOf("mother") >=0 || this.membersInsured[j].title.toLowerCase().indexOf("daughter") >=0){
            gendr = "F";
          }else if(this.membersInsured[j].title.toLowerCase() == "spouse"){
            gendr = this.spouseGender;
          }else if(this.membersInsured[j].title.toLowerCase() == "self"){
            gendr = "M";
          }
          BeneDetails.push({
            "MemberId": cardDetails.Request.BeneficiaryDetails.Member[i].MemberId ,
            "InsuredName": this.membersInsured[j].proposerName,
            "InsuredDob": mDob[0],
            "InsuredGender": gendr,
            "InsuredOccpn": this.membersInsured[j].occupationCode,
            "CoverType": cardDetails.Request.BeneficiaryDetails.Member[i].CoverType,
            "SumInsured": cardDetails.Request.BeneficiaryDetails.Member[i].SumInsured,
            "DeductibleDiscount": this.amount,
            "Relation": cardDetails.Request.BeneficiaryDetails.Member[i].Relation,
            "NomineeName": "",
            "NomineeRelation": "",
            "AnualIncome": "",
            "Height": cardDetails.Request.BeneficiaryDetails.Member[i].Height,
            "Weight": cardDetails.Request.BeneficiaryDetails.Member[i].Weight,
            "NomineeAge": "",
            "AppointeeName": "",
            "AptRelWithominee": "",
            "MedicalLoading": cardDetails.Request.BeneficiaryDetails.Member[i].MedicalLoading,
            "PreExstDisease": "N",
            "DiseaseMedicalHistoryList": {
              "DiseaseMedicalHistory": {
                "PreExistingDiseaseCode": "",
                "MedicalHistoryDetail": ""
              }
            }
          })
         // tempArray.splice(j, 1);
          //tempCardArray.splice(j, 1);
          
        }
      }
 
     
    }

  }
  return BeneDetails;

} 

sendEMailSMS(){    
  if(this.NameOfEmailsmsProposer.trim() == undefined || this.NameOfEmailsmsProposer.trim() == null || this.NameOfEmailsmsProposer.trim() == ""){
    this.showToast("Please enter proposer name");
    //this.myInput.setFocus();
  }else if(this.email.trim() == undefined || this.email.trim() == null || this.email.trim() == ""){
    this.showToast("Please enter email id");
    //this.emailFocus.setFocus();
  }else if (!this.matchEmail(this.email)) {
    this.showToast("Please enter valid email id");  
    //this.emailFocus.setFocus();    
  }else if (this.mobile.trim() == undefined || this.mobile.trim() == null || this.mobile.trim() == "") {      
    this.showToast("Please enter mobile number"); 
    //this.mobileFocus.setFocus();                            
  }else if(this.mobile.length < 10){
    this.showToast("Please enter 10 digit mobile number");   
    //this.mobileFocus.setFocus();         
  }else if(!this.isValidMobile(this.mobile)){
    this.showToast("Please enter valid mobile no");
    //this.mobileFocus.setFocus();
  }else{     
  sessionStorage.propName = this.NameOfEmailsmsProposer;
  sessionStorage.email = this.email;
  sessionStorage.mobile = this.mobile;
  this.emailTo[0] = this.email;
  var emailData = {"emailto":this.emailTo[0],
  "pname":this.NameOfEmailsmsProposer,
  "qid":this.QuotationID,
  "uid":this.cardUID}; 
  this.encryptEmailData(emailData);
  console.log(JSON.stringify(emailData));
}    
}

// addSmsMember(){
// this.sendSMSList.push({value:""});
// console.log(JSON.stringify(this.sendSMSList)); 
// }

sendSms(){ 
// for(let i = 0 ; i < this.sendSMSList.length ; i++){
// this.smsTo.push(this.sendSMSList[i].value); 
// }
console.log(JSON.stringify(this.smsTo));
this.smsTo = this.mobile;
var smsData = {"mobile":this.smsTo,
"pname":this.NameOfEmailsmsProposer,
"qid":this.QuotationID,
"uid":this.cardUID}; 
this.encryptSmsData(smsData);
console.log(JSON.stringify(smsData));
}


encryptEmailData(sendEmailData){ 
var sendData = this.appService.encryptData(JSON.stringify(sendEmailData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
console.log("testt: " + JSON.stringify(sendData));
this.getEmailResponseData({'request': sendData});
}

encryptSmsData(sendSmsData){
var sendData = this.appService.encryptData(JSON.stringify(sendSmsData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
console.log("testt: " + JSON.stringify(sendData)); 
this.getSmsResponseData({'request' : sendData});
}
getEmailResponseData(serviceData){
  this.presentLoadingDefault();
let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
this.appService.callService("FGHealth.svc/SendQuotationByEmail",serviceData, headerString)
.subscribe(SendQuotationByEmail =>{
  this.loading.dismiss();
console.log(SendQuotationByEmail.SendQuotationByEmailResult);
if(SendQuotationByEmail && SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "0"){
//this.NameOfEmailProposerIndividual = "";
//this.emailTo = [];
//this.sendEmailList = [];
sessionStorage.TokenId = SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
console.log("emailToken: " + sessionStorage.TokenId);
console.log(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg);
this.sendSms();    
}else if(SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "807"){
  //this.showToast(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg);
  sessionStorage.TokenId =SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
  this.serviceResponsePopup = false;
  this.message = SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg;
  this.serviceCodeStatus = 807;
  //this.navCtrl.push(LoginPage);
}else if(SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "500"){
  //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
  sessionStorage.TokenId = SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
  this.serviceResponsePopup = false;
  this.message = SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg;
  this.serviceCodeStatus = 500;
  //this.navCtrl.push(LoginPage);
}else{
  this.loading.dismiss();
  sessionStorage.TokenId = SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
  //this.showToast(SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg); 
  this.serviceResponsePopup = false;
  this.message = SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg;
  this.serviceCodeStatus = 400;
}
}, (err) => {
console.log(err);
});
}

getSmsResponseData(serviceData){
let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
this.appService.callService("FGHealth.svc/SendQuotationBySMS",serviceData, headerString)
.subscribe(SendQuotationBySMS =>{

  console.log(SendQuotationBySMS.SendQuotationBySMSResult);
if(SendQuotationBySMS && SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "0"){
sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
//this.NameOfEmailsmsProposer = "";
// this.smsTo = [];
// this.emailTo = [];
// this.email = "";
// this.mobile = ""; 
//this.buyPopup = false;       
this.successLabel = false;
console.log("smsToken: " + sessionStorage.TokenId);
console.log(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);   
this.navCtrl.push(TProposerDetailsPage,{"TbuyPageMemberDetailsResult": this.membersInsured,"TbuyPageCardDetailsResult":this.insuranceDetailsToSend});
}else if(SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "807"){
  //this.showToast(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
  sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
  this.message = SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg;
  this.serviceResponsePopup = false;
  this.serviceCodeStatus = 807;
  //this.navCtrl.push(LoginPage);
}else if(SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "500"){
  //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
  sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
  this.message = SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg;
  //this.navCtrl.push(LoginPage);
  this.serviceResponsePopup = false;
  this.serviceCodeStatus = 500;
}else{
  sessionStorage.TokenId = SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
  //this.showToast(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
  this.message = SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg;
  this.serviceResponsePopup = false;
  this.serviceCodeStatus = 400;
}
}, (err) => {
console.log(err);
});
}


  matchEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  isValidMobile(mobile){
    var regex = /^[6-9][0-9]{9}$/;
    return regex.test(mobile);
  }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'OK',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }  
  } 

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  displayAmt(insuranceCode){
    for(let i = 0; i < this.QuickQuotePlans.length; i++){
      if(this.maximumInsuAmtCode!='' && insuranceCode == this.QuickQuotePlans[i].sicode){
           this.insuredPlanTitle = (this.QuickQuotePlans[i].title);
      }
    }
  }
  
  beneficiaryIndvDetailsArray(){
    this.beneficiaryDetails = [];
    this.totalLoading ="";
   
    for(let i = 0;  i<this.membersInsured.length ; i++){
      this.bmiLoad = 0;
      let smokinLoad = 0;
      if(this.membersInsured[i].smoking == true){
        smokinLoad = 10;
      }else if(this.membersInsured[i].smoking == false){
        smokinLoad = 0;
      }
      if(this.membersInsured[i].bmiLoading == "Yes"){
        this.bmiLoad = Number(this.membersInsured[i].loadingPerc);
        // console.log("loadBMI: " + this.bmiLoad);  
        // this.totalLoading = smokinLoad + this.bmiLoad; 
        // console.log("total" + this.totalLoading);
             
      }else{
        this.bmiLoad = 0;
      }
      if(this.selectedPlanTypeBoolean == undefined || this.selectedPlanTypeBoolean == false){
        this.selectedPlanType = "Supreme";
      }else{
        this.selectedPlanType = "Elite";
      }
      this.beneficiaryDetails.push({
            "MemberId": i+1,
            "InsuredName": "",
            "InsuredDob": this.membersInsured[i].age,
            "InsuredGender": "",
            "InsuredOccpn": "",
            "Plantype": this.selectedPlanType,
            //"CoverType": this.getSumInsuredAmt(this.membersInsured[i].insuredCode,2),
            //"SumInsured": this.getSumInsuredAmt(this.membersInsured[i].insuredCode,1),
            "CoverType": "VITAL",
            "SumInsured": this.sIAmount,
            "Deductible": this.deductableAmount,
            "DeductibleDiscount": this.amount,
            "Relation": this.membersInsured[i].code,
            //"Relation": this.membersInsured[i].title.toUpperCase(),
            "NomineeName": "",
            "NomineeRelation": "",
            "AnualIncome": "",
            "Height": Math.floor(this.membersInsured[i].Height),
            "Weight": this.membersInsured[i].Weight,
            "NomineeAge": "",
            "AppointeeName": "",
            "AptRelWithominee": "",
            "MedicalLoading": Number(smokinLoad) + this.bmiLoad,
            "PreExstDisease": "",
            "DiseaseMedicalHistoryList":this.appService.DiseaseMedicalHistory
      })
    }
  }

  getSumInsuredAmt(sicode,type){
    for(let i = 0 ; i < this.QuickQuotePlans.length ; i++){
      if(sicode == this.QuickQuotePlans[i].sicode && type == 1){
        return this.QuickQuotePlans[i].suminsured;
      }else if(sicode == this.QuickQuotePlans[i].sicode && type==2){
        return (this.QuickQuotePlans[i].title).toUpperCase();
      }
     }
    }

  beneficiaryDetailsArray(SumInsured){
    //console.log("SumInsured: " + SumInsured  +"+++++" + CoverType);
    
    let beneficiaryDetailsForMembers = [];
    this.totalLoading ="";   
    
    for(let i = 0;  i<this.membersInsured.length ; i++){      
      let bmiLoad = 0;
      let smokinLoad = 0;
      if(this.membersInsured[i].smoking == true){
        smokinLoad = 10;
      }else{
        smokinLoad = 0;
      }
      if(this.membersInsured[i].bmiLoading == "Yes"){
        bmiLoad = Number(this.membersInsured[i].loadingPerc);
        // console.log("loadBMI: " + this.bmiLoad);  
        // this.totalLoading = smokinLoad + this.bmiLoad; 
        // console.log("total" + this.totalLoading);
             
      }else{
        bmiLoad = 0;
      }    
      if(this.selectedPlanTypeBoolean == undefined || this.selectedPlanTypeBoolean == false){
        this.selectedPlanType = "Supreme";
      }else{
        this.selectedPlanType = "Elite";
      }
      beneficiaryDetailsForMembers.push({
            "MemberId": i+1,
            "InsuredName": "",
            "InsuredDob": this.membersInsured[i].age,
            "InsuredGender": "",
            "InsuredOccpn": "",
            "Plantype": this.selectedPlanType,
            "CoverType": "VITAL", //  this.getSumInsuredAmt(this.membersInsured[i].insuredCode,2),
            "SumInsured": SumInsured, //this.getSumInsuredAmt(this.membersInsured[i].insuredCode,1),
            "Deductible": this.deductableAmount,
            "DeductibleDiscount": this.amount,
            "Relation": this.membersInsured[i].code,
            //"Relation": this.membersInsured[i].title.toUpperCase(),
            "NomineeName": "",
            "NomineeRelation": "",
            "AnualIncome": "",
            "Height": Math.floor(this.membersInsured[i].Height),
            "Weight": this.membersInsured[i].Weight,
            "NomineeAge": "",
            "AppointeeName": "",
            "AptRelWithominee": "",
            "MedicalLoading": Number(smokinLoad) + bmiLoad,
            "PreExstDisease": "",
            "DiseaseMedicalHistoryList":this.appService.DiseaseMedicalHistory
      })
      console.log("bene: "+JSON.stringify(this.beneficiaryDetails));
    }
    return beneficiaryDetailsForMembers;
    
    
//    console.log("bene: "+JSON.stringify(this.beneficiaryDetails));
  }

  closeQuickQuote(){
    this.navCtrl.push(LoginPage);
  }

  setSIAmountValue(){
    this.insurenceAmtToShow = [];
    if(this.maximumInsuAmtCode == '' && this.activememberName != 'Self'){
      this.maximumInsuAmtCode = this.tempMemberArray[0].insuredCode;
    }else if(this.activememberName == 'Self'){
      this.maximumInsuAmtCode = '';
    }
    for(let i = 0; i < this.siAmountArray.length;i++){
      if(this.maximumInsuAmtCode!='' && this.siAmountArray[i].sicode == this.maximumInsuAmtCode){
        this.insurenceAmtToShow.push(this.siAmountArray[i]);
        break;
      }else{
        this.insurenceAmtToShow.push(this.siAmountArray[i]);
      }
    }
  }

  showCardDetailsPop(event: Event, cardData){
    event.stopPropagation();
    console.log("test: " + JSON.stringify(cardData));
    this.showPopup = true;
    this.transparentDiv = false;
    this.premiumWithServiceTax == '';
    if(this.pet == "OneTime"){          
      this.basePremium =  parseFloat(cardData.CalculatedServiceResp.BasePremium).toFixed(0);  
      this.basePremium = this.addCommas(this.basePremium);    
      if(cardData.Request.PolicyType == "HTI"){
        this.policyGroup = "Individual";
      }else{
        this.policyGroup = "Floater";
      }      
      this.deductableDiscount = cardData.CalculatedServiceResp.DeductDiscRate;
      this.employeeDiscount = cardData.CalculatedServiceResp.EmpDiscRate;
      this.termPremium = parseFloat(cardData.CalculatedServiceResp.TermPremium).toFixed(0);
      this.termPremium = this.addCommas(this.termPremium);
      this.medicalloading =  this.membersInsured[0].loadingPerc;
      if(this.membersInsured[0].smoking == true){
        this.smokingloadSelf = 10.0
        }else{
        
        this.smokingloadSelf = 0.0
        }
      if(this.amount == 0){
        this.voluntarydeduction = "NIL"
      }else{
        this.voluntarydeduction = "₹" + this.amount;
      }
      
      this.longTermDiscount = cardData.CalculatedServiceResp.LngTrmDisc;
      this.premiumwithoutservTax = parseFloat(cardData.CalculatedServiceResp.PremWithoutServTax).toFixed(0);
      this.premiumwithoutservTax = this.addCommas(this.premiumwithoutservTax);
      this.installmentLoading = cardData.CalculatedServiceResp.InstallLoadRate;
      this.premiumWithLoading = parseFloat(cardData.CalculatedServiceResp.PremWithLoad).toFixed(0);
      this.premiumWithLoading = this.addCommas(this.premiumWithLoading);
      this.totalInstallment = cardData.CalculatedServiceResp.TtlInstallment;
      this.premiumAmount = parseFloat(cardData.CalculatedServiceResp.PremiumAmt).toFixed(0);
      this.premiumAmount = this.addCommas(this.premiumAmount);
      this.serviceTax = parseFloat(cardData.CalculatedServiceResp.ServiceTax).toFixed(0);
      this.serviceTax = this.addCommas(this.serviceTax);
      if(cardData.Request.PolicyType == "HTI"){
        this.premiumWithServiceTax = this.addCommas(cardData.discountAmount);
      }else{
        this.premiumWithServiceTax = this.addCommas(cardData.CalculatedServiceResp.PremWithServTax);
      }  
     
      
      console.log("test: " + JSON.stringify(cardData));
    }else if(this.pet == "Monthly"){
      this.basePremium = parseFloat(cardData.CalculatedServiceResp.BasePremium).toFixed(0);
      this.basePremium = this.addCommas(this.basePremium);      
      if(cardData.Request.PolicyType == "HTI"){
        this.policyGroup = "Individual";
      }else{
        this.policyGroup = "Floater";
      }      
      this.deductableDiscount = cardData.CalculatedServiceResp.DeductDiscRate;
      this.employeeDiscount = cardData.CalculatedServiceResp.EmpDiscRate;
      this.termPremium = parseFloat(cardData.CalculatedServiceResp.TermPremium).toFixed(0);
      this.termPremium = this.addCommas(this.termPremium);
      this.medicalloading =  this.membersInsured[0].loadingPerc;
      if(this.membersInsured[0].smoking == true){
        this.smokingloadSelf = 10.0
        }else{
        
        this.smokingloadSelf = 0.0
        }
      if(this.amount == 0){
        this.voluntarydeduction = "NIL"
      }else{
        this.voluntarydeduction = "₹" + this.amount;
      }
      // this.voluntarydeduction = this.amount;
      this.longTermDiscount = cardData.CalculatedServiceResp.LngTrmDisc;
      this.premiumwithoutservTax = parseFloat(cardData.CalculatedServiceResp.PremWithoutServTax).toFixed(0);
      this.premiumwithoutservTax = this.addCommas(this.premiumwithoutservTax);
      this.installmentLoading = cardData.CalculatedServiceResp.InstallLoadRate;
      this.premiumWithLoading = parseFloat(cardData.CalculatedServiceResp.PremWithLoad).toFixed(0);
      this.premiumWithLoading = this.addCommas(this.premiumWithLoading);
      this.totalInstallment = cardData.CalculatedServiceResp.TtlInstallment;
      this.premiumAmount = parseFloat(cardData.CalculatedServiceResp.PremiumAmt).toFixed(0);
      this.premiumAmount = this.addCommas(this.premiumAmount);
      this.serviceTax = parseFloat(cardData.CalculatedServiceResp.ServiceTax).toFixed(0);
      this.serviceTax = this.addCommas(this.serviceTax);
      this.premiumWithServiceTax = this.addCommas(cardData.CalculatedServiceResp.PremWithServTax);
    }else if(this.pet == "Quarterly"){
      this.basePremium = parseFloat(cardData.CalculatedServiceResp.BasePremium).toFixed(0);
      this.basePremium = this.addCommas(this.basePremium);      
      if(cardData.Request.PolicyType == "HTI"){
        this.policyGroup = "Individual";
      }else{
        this.policyGroup = "Floater";
      }      
      this.deductableDiscount = cardData.CalculatedServiceResp.DeductDiscRate;
      this.employeeDiscount = cardData.CalculatedServiceResp.EmpDiscRate;
      this.termPremium = parseFloat(cardData.CalculatedServiceResp.TermPremium).toFixed(0);
      this.termPremium = this.addCommas(this.termPremium);
      this.medicalloading =  this.membersInsured[0].loadingPerc;
      if(this.membersInsured[0].smoking == true){
        this.smokingloadSelf = 10.0
        }else{
        
        this.smokingloadSelf = 0.0
        }
      if(this.amount == 0){
        this.voluntarydeduction = "NIL"
      }else{
        this.voluntarydeduction = "₹" + this.amount;
      }
      this.longTermDiscount = cardData.CalculatedServiceResp.LngTrmDisc;
      this.premiumwithoutservTax = parseFloat(cardData.CalculatedServiceResp.PremWithoutServTax).toFixed(0);
      this.premiumwithoutservTax = this.addCommas(this.premiumwithoutservTax);
      this.installmentLoading = cardData.CalculatedServiceResp.InstallLoadRate;
      this.premiumWithLoading = parseFloat(cardData.CalculatedServiceResp.PremWithLoad).toFixed(0);
      this.premiumWithLoading = this.addCommas(this.premiumWithLoading);
      this.totalInstallment = cardData.CalculatedServiceResp.TtlInstallment;
      this.premiumAmount = parseFloat(cardData.CalculatedServiceResp.PremiumAmt).toFixed(0);
      this.premiumAmount = this.addCommas(this.premiumAmount);
      this.serviceTax = parseFloat(cardData.CalculatedServiceResp.ServiceTax).toFixed(0);
      this.serviceTax = this.addCommas(this.serviceTax);
      this.premiumWithServiceTax = this.addCommas(cardData.CalculatedServiceResp.PremWithServTax);
    }else if(this.pet == "HalfYearly"){
      this.basePremium = parseFloat(cardData.CalculatedServiceResp.BasePremium).toFixed(0);
      this.basePremium = this.addCommas(this.basePremium);    
      if(cardData.Request.PolicyType == "HTI"){
        this.policyGroup = "Individual";
      }else{
        this.policyGroup = "Floater";
      }      
      this.deductableDiscount = cardData.CalculatedServiceResp.DeductDiscRate;
      this.employeeDiscount = cardData.CalculatedServiceResp.EmpDiscRate;
      this.termPremium = parseFloat(cardData.CalculatedServiceResp.TermPremium).toFixed(0);
      this.termPremium = this.addCommas(this.termPremium);
      this.medicalloading =  this.membersInsured[0].loadingPerc;
      if(this.membersInsured[0].smoking == true){
        this.smokingloadSelf = 10.0
        }else{
        
        this.smokingloadSelf = 0.0
        }
      if(this.amount == 0){
        this.voluntarydeduction = "NIL"
      }else{
        this.voluntarydeduction = "₹" +  this.amount;
      }
      this.longTermDiscount = cardData.CalculatedServiceResp.LngTrmDisc;
      this.premiumwithoutservTax = parseFloat(cardData.CalculatedServiceResp.PremWithoutServTax).toFixed(0);
      this.premiumwithoutservTax = this.addCommas(this.premiumwithoutservTax);
      this.installmentLoading = cardData.CalculatedServiceResp.InstallLoadRate;
      this.premiumWithLoading = parseFloat(cardData.CalculatedServiceResp.PremWithLoad).toFixed(0);
      this.premiumWithLoading = this.addCommas(this.premiumWithLoading);
      this.totalInstallment = cardData.CalculatedServiceResp.TtlInstallment;
      this.premiumAmount = parseFloat(cardData.CalculatedServiceResp.PremiumAmt).toFixed(0);
      this.premiumAmount = this.addCommas(this.premiumAmount);
      this.serviceTax = parseFloat(cardData.CalculatedServiceResp.ServiceTax).toFixed(0);
      this.serviceTax = this.addCommas(this.serviceTax);
      this.premiumWithServiceTax = this.addCommas(cardData.CalculatedServiceResp.PremWithServTax);
    }
  }

  // showCardDetailsPop(){
  //   this.showPopup = !this.showPopup;
  // }
  closePopInfo(){
    this.showPopup = !this.showPopup;
  }

 
  buyPopUpOpen(sumInsured){
    console.log("sum" + JSON.stringify(sumInsured));
    
    this.ReferenceNo = sumInsured.ReferenceNo;
    this.successLabel = true;
    this.NameOfEmailsmsProposer = "";
    this.smsTo = [];
    this.emailTo = [];
    this.email = "";
    this.mobile = "";
    console.log("check: " + JSON.stringify(sumInsured));
    this.insuranceDetailsToSend = [];
    this.insuranceDetailsToSend = sumInsured;
    this.cardUID = sumInsured.UID;
    this.QID = sumInsured.QuotationID;
    this.buyPopup = true;
  }
  closeModalPop(){
    // this.showPopup = false;  
    sessionStorage.isViewPopped = undefined;   
     this.buyPopup = false;
     this.NameOfEmailsmsProposer = "";
     this.smsTo = [];
     this.emailTo = [];
     this.sendSMSList = [];
     this.sendEmailList = [];
   }

  


}
