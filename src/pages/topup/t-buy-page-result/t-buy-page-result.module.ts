import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TBuyPageResultPage } from './t-buy-page-result';

@NgModule({
  declarations: [
    //TBuyPageResultPage,
  ],
  imports: [
    IonicPageModule.forChild(TBuyPageResultPage),
  ],
})
export class TBuyPageResultPageModule {}
