import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TMedicaldeclarationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    TMedicaldeclarationPage,
  ],
  imports: [
    //IonicPageModule.forChild(TMedicaldeclarationPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-t-medicaldeclaration',
  templateUrl: 't-medicaldeclaration.html',
})
export class TMedicaldeclarationPage {

	thankyouPopup = true; 

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TMedicaldeclarationPage');
     setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);
  }

}
