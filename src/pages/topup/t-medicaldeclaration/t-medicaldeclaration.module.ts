import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TMedicaldeclarationPage } from './t-medicaldeclaration';

@NgModule({
  declarations: [
    //TMedicaldeclarationPage,
  ],
  imports: [
    IonicPageModule.forChild(TMedicaldeclarationPage),
  ],
})
export class TMedicaldeclarationPageModule {}
