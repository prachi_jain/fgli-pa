import { Component, ChangeDetectorRef, NgModule } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams,ToastController, Platform } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { Keyboard } from '@ionic-native/keyboard';
import { DatePicker } from '@ionic-native/date-picker';
import { ActionSheetController } from 'ionic-angular';
//import { parseDate } from 'ionic-angular/util/datetime-util';
import { IonicPageModule } from 'ionic-angular';
import { TBuyPageResultPage } from '../t-buy-page-result/t-buy-page-result';

/**
 * Generated class for the TbuyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    TbuyPage,
  ],
  imports: [
    //IonicPageModule.forChild(TbuyPage),
  ],
})

@IonicPage()
@Component({
  selector: 'page-tbuy',
  templateUrl: 'tbuy.html',
})
export class TbuyPage {

  PolicyTypeSelected = "familyFloaterCode";forQuickQuote; familyFloaterCode='';individualCode='';showTransBaground:boolean = true;showTransBaground2 =false;
  showTransBaground3 = false; addMember= false;
  appData; apiMembers; apiPlanType; apiAgeGroup; QuickQuotePlans=[]; insuredPlanTitle = '';policyTypes; showInsurenceDiv: boolean = false;
  public hideMemberPopup:boolean = false; public hideChildPopup:boolean = false; public hidePopupSmokerDiv:boolean = false; public hidePopupMarriedDiv:boolean = false; 
  popupTitle= ""; popupImg; insuredCode; slide=''; canShowToast = true; childAgeGroup =[]; showAddMember = false;familySlide='';
  popupMemberData = {"code":""}; membersInsured = []; ageGroupToshow=[]; memberSmokes =  false; membersAvailable; membersShow = [];
  memberMarried=  false; memberAge:any = "26-04-1985" ; childsAge =0; maximumInsuAmtCode = ''; insurenceAmtToShow = []; membersShownOnPopup = []; 
  familyInsurenceAmt = [];familyInsuredPlanType = ''; familyInsuredPlanTypeText = '';fromBuyResult; popupMember="SELF"; hideDisableCards =true;
  myMember=[{"id":"SELF","imgPath":"assets/imgs/topup/selfIcon.png"},{"id":"SPOU","imgPath":"assets/imgs/topup/spouseIcon.png"},
  {"id":"SON","imgPath":"assets/imgs/topup/son.png"},{"id":"DAUG","imgPath":"assets/imgs/topup/daughter.png"},
  {"id":"FATH","imgPath":"assets/imgs/topup/father.png"},{"id":"MOTH","imgPath":"assets/imgs/topup/mother.png"},{"id":"GRCH","imgPath":"assets/imgs/self.png"},
  {"id":"SIB","imgPath":"assets/imgs/self.png"},{"id":"CHLD","imgPath":"assets/imgs/self.png"},{"id":"GRPA","imgPath":"assets/imgs/self.png"}]
  vitalMembers=["SELF","SPOU","SON","SON1","SON2","SON3","DAUG","DAUG1","DAUG2","DAUG3","FATH","MOTH","CHLD"];
  beneficiaryDetails = [];
  ENQ_PolicyArray = [];
  individualDiscount = false;
  isClassOneActive = false;
  smokingLoad;
  disableProceedBt = true;
  editUserData = false;
  hideForSelf = true;
  hideProceedButton = false;
  getBMIOrNot = false;
  memberBMI;
  bmiLoadingStatus;
  bmiLoadingPerc;
  bmiLoad;
  totalLoading;
  familyPlan;
  individualPlan;
  childAgeStatus;
  userPosData;
  loginData;
  bmi;
  quickquoteSelectedData;
  preloginArrayRecieved = [];
  preLoginDataRecieved;
  preloginNormal;
  today = new Date();
  maxDate = (this.today.getFullYear()+"-"+this.numberLenght(this.today.getMonth())+"-"+this.numberLenght(this.today.getDate())).toString();
  numberLength(Num){if(Num>=10){return Num}else{return"0"+Num}}
  minDate = '1900-01-01';
  //minDate = ( this.numberLenght(this.today.getMonth() - 1)+"/"+this.numberLenght(this.today.getDate())+"/"+(this.today.getFullYear()-1) ).toString();
  weightArray = [];
  heightArray = [];
  marriedStatusArray = [{value: "Single", status: false},{value: "Married", status: true},{value: "Divorced", status: false},{value: "Widow / Widower", status: false}];
  marriedStatus = "";
  personHt = '';
  personWt = '';
  annualIncomeArray = [];
AnnualIncome = '';
selectedDeductable = "";
selectSIAmount = "";
planTypeText;
planTypeCode;
sIamount;
SIArray = [];

  numberLenght(Num){if(Num>=10){return Num}else{return"0"+Num}}
  constructor(private platform: Platform, private datePicker: DatePicker,  public alertCtrl: AlertController, private keyboard: Keyboard,public navCtrl: NavController,public appService: AppService, private actionSheetCtrl: ActionSheetController, public navParams: NavParams,public toast: ToastController, private cdr: ChangeDetectorRef) {
    this.appData = JSON.parse(localStorage.AppData);
    this.apiMembers = this.appData.Masters.Members;
    this.planTypeText = navParams.get("planTypeText");
    this.planTypeCode = navParams.get("planTypeCode");
    this.sIamount = navParams.get("siAmounut");
    this.SIArray = navParams.get("SuminsuredArray");
    console.log("selectedAmpuntCode" + this.planTypeCode + ""  + this.planTypeText);
    
    if(this.apiMembers != undefined){
      console.log("TotalMembers: " + JSON.stringify(this.apiMembers));
      
    }
    this.selectedDeductable = navParams.get("deductableData");
    this.selectedAmount = navParams.get("siAmounut");
    this.apiPlanType = this.appData.Masters.PlanType;
    this.apiAgeGroup = this.appData.Masters.Age;
    this.policyTypes = this.appData.Masters.PolicyType;
    this.forQuickQuote = navParams.get("forQuickQuote");
    this.fromBuyResult = navParams.get("fromBuyResult");
    this.smokingLoad = this.appData.Masters.Loading.smokingload[0].perc;
    this.userPosData = navParams.get("userPosData");
    console.log("userPosData: " + JSON.stringify(this.userPosData));
    //this.quickquoteSelectedData = navParams.get("preLoginData");
    console.log("quickQuoteData" + JSON.stringify(this.quickquoteSelectedData));
    // if(this.forQuickQuote != null){
    //   this.preLoginDataRecieved = navParams.get("preLoginData");
    // }else{
    //   this.preLoginDataRecieved = navParams.get("preLoginQuickQuoteData");
    // }

    // this.preLoginDataRecieved = navParams.get("preLoginData");
    

    
    //  platform.registerBackButtonAction(() => {
    //   this.backButtonFunction();
    // });

    this.preloginNormal = navParams.get("preloginNormal");
    if(navParams.get("preLoginQuickQuoteData")){
    
    this.preLoginDataRecieved = navParams.get("preLoginQuickQuoteData");
    }
    else if(navParams.get("preLoginData")){
    
    this.preLoginDataRecieved = navParams.get("preLoginData");
    }
  }

  backButtonFunction(){
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TbuyPage');
    setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);

      this.membersArrayFunction();
      this.loginData = JSON.parse(sessionStorage.Ispos);
      //console.log("LoginData: " + loginData);  
      console.log("session: " + this.loginData);
      console.log("Array: " + JSON.stringify( sessionStorage.loginUserData));
      
        
      // if(loginData == true){
      //   this.getSumInsuredArray();
      //   this.familyInsurenceAmt.slice(0, 2);
      //   //this.familyInsurenceAmt = this.userPosData.sumInsured;
      // }else{
      //   this.getSumInsuredArray();
      // }
  
      this.getSumInsuredArray();
      
      this.childAgeRange();
  
      console.log(sessionStorage.familySumAmtQQ+"sessionStorage.familysumAMTQQ");
      console.log(sessionStorage.familysumQQ+"sessionStorage.familysumQQ");
      if(sessionStorage.familysumQQ == "true"){
        if(this.preloginNormal==false || this.preloginNormal == undefined){
  
  
          /// this is to make selected ////
          console.log(sessionStorage.familySumAmtQQ);
          this.familySlide = sessionStorage.familySumAmtQQ;
          this.hideDisableCards = true;
          }
      }
  
      for(let i = 0; i < this.policyTypes.length;i++){
        if( (this.policyTypes[i].title).toLowerCase().indexOf("health total individual") >= 0){
          this.individualCode = this.policyTypes[i].code;
        }else if( (this.policyTypes[i].title).toLowerCase().indexOf("health total family") >=0 ){
          this.familyFloaterCode = this.policyTypes[i].code;
          this.PolicyTypeSelected = this.familyFloaterCode;
        }
      }
  
      //this.populateInsurenceAmt(0,"SELF");
      this.showTransBaground = true;
  
      for(let i = 2; i <=120; i++ ){
        this.weightArray.push(i);
      }
  
      for(let i: number=3  ; i < 7; i++){
        for(let j = 0; j < 12;j++ ){
          let htcm =  ((Number(i)* 30.48) + (Number(j) * 2.54)).toFixed(2);
          this.heightArray.push({ "text" : i+"' "+j+'' , "val" :htcm,"filledText":i+"' "+j+'"'});
         /*  this.heightArray.push({ "text" : i+"' "+j+'"ft  or   '+htcm+"  cms" , "val" :htcm,"filledText":i+"' "+j+'"'}); */
        }
        
      }
  
      if(this.fromBuyResult){
        this.hideDisableCards = true;
        this.disableProceedBt = false;
        this.membersShow = JSON.parse(sessionStorage.MembersShownOnforBuy);
        this.membersInsured = JSON.parse(sessionStorage.BuyMembersDetails);
        for(let i = 0; i < this.membersShow.length; i++){
          let term = this.membersInsured.filter(person => (person.title == this.membersShow[i].title && person.code == this.membersShow[i].code ));
           if(term.length <1){
              this.membersShow[i].detailsSaved = false;
              this.membersShow[i].age = '';
              this.membersShow[i].ageText = '';
              this.membersShow[i].insuredCode = '';
              this.membersShow[i].insuredAmtText = '';
              this.membersShow[i].PolicyType = '';
              this.membersShow[i].smoking = false;
              this.membersShow[i].smokingText = "";
            }
        }
        sessionStorage.removeItem("quickQuoteMembersDetails");
        sessionStorage.removeItem("MembersShownOnforBuy");
        if(this.membersInsured[0].PolicyType == "HTF"){
          console.log(this.membersInsured[0].insuredCode);
          this.familySlide = this.planTypeCode;
          console.log(this.familySlide);
          this.PolicyTypeSelected = this.familyFloaterCode;
          this.individualDiscount = false;
          this.showInsurenceDiv = false;
        }else if(this.membersInsured[0].PolicyType == "HTI"){
          this.PolicyTypeSelected = this.individualCode;
          this.maximumInsuAmtCode = this.insuredCode;
          this.individualDiscount = true;
          this.showInsurenceDiv = true;
        }
      }
  }

  membersArrayFunction(){
    for(let a = 0; a <this.apiMembers.length; a++){
      let term = this.myMember.filter(person => person.id == this.apiMembers[a].code );
      var memberDetails = {};
      memberDetails["code"] =this.apiMembers[a].code;
      memberDetails["forQuickQuote"] =this.forQuickQuote;
      memberDetails["title"] =this.apiMembers[a].title;
      memberDetails["smoking"] = false;
      memberDetails["smokingText"] = "";
      memberDetails["popupType"] = "1";
      memberDetails["showMarried"] = false;
      memberDetails["maritalStatus"] = "";
      memberDetails["maritalCode"] = "S";
      memberDetails["age"] = "";
      memberDetails["ageText"] = "";
      memberDetails["insuredCode"] ="";
      memberDetails["insuredAmtText"] ="";
      memberDetails["showDependent"] = false;
      memberDetails["showAddAnotherBtn"] = false;
      memberDetails["PolicyType"]= '';
      memberDetails["showFamilyMemberOnScreen"] = false;
      memberDetails["disableRemove"] = false;
      memberDetails["ShowInsurenceAmt"] = false;
      memberDetails["insuredPlanTypeCode"] = '';
      memberDetails["insuredPlanTypeText"] = '';
      memberDetails["insuredAmtInDigit"] = '';
      memberDetails["Height"] = '';
      memberDetails["HeightText"] = '';
      memberDetails["Weight"] = '';
      memberDetails["ProposerDetailsDivShow"] = false;
      memberDetails["medicalDeclaration"] = false;
      memberDetails["showMedicalQuestions"] = true;
      memberDetails["nominee"] = '';
      memberDetails["NomineeRelation"] = '';
      memberDetails["NomineeAge"] = '';
      memberDetails["appointeeMember"] = '';
      memberDetails["AptRelWithominee"] = '';
      memberDetails["memberSelectedOnAddmember"] = false;
      if( this.vitalMembers.indexOf(this.apiMembers[a].code)>=0){       
        memberDetails["showFamilyMemberOnScreen"] = true;
        memberDetails["memberSelectedOnAddmember"] = true;
      }
     

      if(term.length!=0){
        memberDetails["imgUrl"] =term[0].imgPath;
        if(term[0].id == "SON" || term[0].id == "DAUG"){
          memberDetails["showDependent"] = true;
          memberDetails["popupType"] = "2";
          memberDetails["showAddAnotherBtn"] = true;
        }else if(term[0].id == "SELF"){
          memberDetails["showMarried"] = true;
          memberDetails["disableRemove"] = true;
        }else if(term[0].id == "SPOU"){
          memberDetails["disableRemove"] = true;
        }
      }
      if(this.apiMembers[a].title.toLowerCase().indexOf("son")>=0){
        memberDetails["imgUrl"] ="assets/imgs/son.png";
      }else if(this.apiMembers[a].title.toLowerCase().indexOf("daughter")>=0){
        memberDetails["imgUrl"] ="assets/imgs/daughterIcon.png";
      }else if(this.apiMembers[a].title.toLowerCase().indexOf("father")>=0){
        memberDetails["imgUrl"] ="assets/imgs/fatherIcon.png";
      }else if(this.apiMembers[a].title.toLowerCase().indexOf("mother")>=0){
        memberDetails["imgUrl"] ="assets/imgs/motherIcon.png";
      }else if(term.length==0){
        memberDetails["imgUrl"] ="assets/imgs/self.png";
      }
      memberDetails["detailsSaved"] =false;
      this.membersShow.push(memberDetails);
    }
  }

  getSumInsuredArray(){
    this.QuickQuotePlans =[];
    this.QuickQuotePlans = this.appService.getSumInsuredArray(this.apiPlanType);
  }




  childAgeRange(){
    for(let i = 0; i <this.apiAgeGroup.length; i++){
        if(this.apiAgeGroup[i].min >  25){
          break;
        }
        this.childAgeGroup.push(this.apiAgeGroup[i]);
    }
  }

  fillData(memberData){
    this.slide = '';
    //this.minDate = '1900-01-01'
    console.log("member: " + JSON.stringify(memberData));
    
    this.memberAge = '';
    this.personHt = '';
    this.personWt = '';
    this.addMember = false;
    this.memberSmokes = false;
    this.memberMarried = false;
    this.editUserData = false;
    //this.populateInsurenceAmt(1,memberData.code);
    this.popupMember = "";    
    if(memberData.detailsSaved){
      return;
    }
    if(memberData.code=="SELF" && this.membersInsured.length==0){
      
      if(this.preLoginDataRecieved != undefined){

        console.log(this.preLoginDataRecieved);
        
        this.preloginArrayRecieved = this.preLoginDataRecieved[0]
        
        for(let i = 0; i<this.preloginArrayRecieved.length ; i++){
        
        this.memberSmokes = this.preloginArrayRecieved[0].smoking
        
        this.marriedStatus = this.preloginArrayRecieved[0].maritalStatus
        console.log(this.marriedStatus)
        
        }
        
        }
        
         
      this.popupTitle = memberData.title;
      this.popupImg = memberData.imgUrl;
      if(this.familyPlan == "P003"){
        this.hideMemberPopup = false;   
        this.showTransBaground = true;  
        this.showToast("Regret we cannot process proposal further on app as it requires medical underwriting. Please contact our nearest Branch office.")
      }else{
        this.hideMemberPopup = true;  
        this.showTransBaground = false;     
      }
      this.popupMemberData = memberData;
      this.hidePopupSmokerDiv = true;
      this.hidePopupMarriedDiv =true;
     // if(this.PolicyTypeSelected == this.familyFloaterCode ){
        this.memberMarried = true;
     // }
      //this.populateInsurenceAmt(1,memberData.code);
    }else if(memberData.code!="SELF" && this.membersInsured.length==0){
      this.showToast("Please insure 'self' first before choosing other member/s.");
    }else if(memberData.code!="SELF" && this.membersInsured.length!=0 && !memberData.detailsSaved){
      if(this.membersInsured.length==1 && this.membersShow[0].maritalStatus == "Married" && memberData.code != "SPOU"){
        this.showToast("Please provide spouse details as your marital status is Married.");
      }else{
        //document.getElementById("addMember").style.marginBottom='60px';
        document.getElementById("btnTopMargin").style.marginBottom='40px';
        //this.hideProceedButton = false;
        if(memberData.code=="SON" || memberData.code == "DAUG"){
          this.showTransBaground = false;    
          this.hideMemberPopup = true;
          this.addMember = true;
          this.popupMember = "Child"
          //this.minDate = ( (this.today.getFullYear()-25)+"-"+this.numberLenght(this.today.getMonth())+"-"+this.numberLenght(this.today.getDate())).toString();
        }else if(memberData.code == "SPOU"){
          if(this.membersShow[0].maritalStatus == "Married"){
            this.hidePopupSmokerDiv = true;
            this.showTransBaground = false;    
            this.hideMemberPopup = true;
          }else if(this.membersShow[0].maritalStatus == "Single"){
            this.showToast("As your marital status is single, you cannot fill spouse details.");
          }else if(this.membersShow[0].maritalStatus == "Divorced"){
            this.showToast("As your marital status is divorced, you cannot fill spouse details.");
          }else if(this.membersShow[0].maritalStatus == "Widow / Widower"){
            this.showToast("As your marital status is widow / widower, you cannot fill spouse details.");
          }
        }else if(memberData.code == "FATH"){       
          this.hideMemberPopup = true; 
          this.popupMember = "Father";
          this.hidePopupSmokerDiv = true;
        }else if(memberData.code == "MOTH"){
          this.hideMemberPopup = true; 
          this.popupMember = "Mother";
          this.hidePopupSmokerDiv =true;
        }else if(memberData.code != "SPOU"){
          this.showTransBaground = false;    
         // this.showMemberPopup = true;
          this.hideMemberPopup = true;         
        }
        this.popupTitle = memberData.title;
        this.popupImg = memberData.imgUrl;
        this.popupMemberData = memberData;
      }

    }else if(memberData.code!="SELF" && this.insuredPlanTitle == "Vital" && this.membersInsured.length == 6){

    }
    this.getSumInsuredArray();
  }

  hidePopup(){
    this.slide = '';
    this.hideMemberPopup = false;
    this.hideChildPopup = false;
    this.insuredCode = '';
    this.childsAge = 0;
    this.memberAge = "";
    this.memberSmokes = false;
    this.memberMarried = false;
    this.showTransBaground = true; 
    // if(this.membersInsured[1].memberAge == "" || this.membersInsured[1].memberAge == undefined){
    //   this.hideProceedButton = true;
    // }else{
    //   this.hideProceedButton = false;
    // }
  }

  getAge(date){
    let dateVar = date.split("-");
    let ag = (Date.now()-(new Date(dateVar[2],dateVar[1]-1,dateVar[0])).getTime());  
    return Math.floor((ag / (1000 * 3600 * 24))/365)+" years";
  }  

  heightClick(){
  }

  // childAgeRange(){
  //   for(let i = 0; i <this.apiAgeGroup.length; i++){
  //       if(this.apiAgeGroup[i].min >  25){
  //         break;
  //       }
  //       this.childAgeGroup.push(this.apiAgeGroup[i]);
  //   }
  // }


  editMemberDetails($event, member){
    event.stopPropagation();
    // for(let i = 0; i <this.membersInsured.length; i++){
    //   if(this.membersInsured[i].code == member.code){
    //   var number_of_elements_to_remove = 1;
    //   var removed_elements = this.membersInsured.splice(i, number_of_elements_to_remove);
    //   console.log(this.membersInsured)
    //   }
    //   }
      // if()
      if(this.PolicyTypeSelected=='HTF'){

        this.slide = "";
        }
        else{
        this.slide = member.insuredCode;
        }
    console.log("plan: " + JSON.stringify(member));
    
    //this.populateInsurenceAmt(1,member.code);

      this.editUserData = true;
      this.addMember = false;
      this.memberSmokes = false;
      this.memberMarried = false;
      this.showTransBaground = false;    
      this.popupTitle = member.title;
      this.popupImg = member.imgUrl;
      this.hideMemberPopup = true;
      this.popupMemberData = member;
      this.memberAge = member.age;
      this.personHt = member.Height;
      this.personWt = member.Weight;
      this.hidePopupSmokerDiv = false;
      this.hidePopupMarriedDiv =false;
      if(member.code=="SELF"){
        this.hidePopupSmokerDiv = true;
        this.hidePopupMarriedDiv =true;
        this.memberSmokes = member.smoking;
        this.popupMember = "SELF"
          if(member.maritalCode == "M"){
            this.memberMarried = true;
          }
      }else{

      }
      if(member.code=="SON" || member.code == "DAUG"){
        this.showTransBaground = false;    
        this.hideMemberPopup = true;
        this.addMember = true;
        this.popupMember = "Child";
        this.hidePopupSmokerDiv = false;
        if(this.membersInsured[0].maritalStatus == "Single"){
          this.hideProceedButton = false;
        }

      }else if(member.code == "SPOU"){
        this.hidePopupSmokerDiv = true;
        this.showTransBaground = false;    
        this.hideMemberPopup = true;
        this.memberSmokes = member.smoking;
        this.popupMember = "Spouse";
      }else if(member.code == "FATH"){
        this.hidePopupSmokerDiv = true;
        this.showTransBaground = false;    
        this.hideMemberPopup = true;
        this.memberSmokes = member.smoking;
        this.popupMember = "Father";
      }else if(member.code == "MOTH"){
        this.hidePopupSmokerDiv = true;
        this.showTransBaground = false;    
        this.hideMemberPopup = true;
        this.memberSmokes = member.smoking;
        this.popupMember = "Mother";
      }
  }



  filledHeight(height){
    for(let i =0; i < this.heightArray.length;i++){
      if(this.personHt == this.heightArray[i].val){
        return this.heightArray[i].filledText;
      }
    }
    
  }

  displayAge(agecode){
    for(let i = 0; i < this.apiAgeGroup.length; i++){
      if(agecode == this.apiAgeGroup[i].ageCode){
        return this.apiAgeGroup[i].title+" years";
      }
    }
  }
  
  invalidDob(userDoB){
    let today = new Date();
    let uDob = userDoB.split("-");
    let uDte = ( this.numberLenght(Number(uDob[1]))+"-"+this.numberLenght( Number(uDob[0]))+"-"+(uDob[2]) ).toString();
    let usrDob = new Date(uDte);    
    if(usrDob.toString() == "Invalid Date"){
      return true;
    }else if(usrDob > today){
      return true;
    }
    return false;
  }
  
  displayAmt(insuranceCode){
    for(let i = 0; i < this.QuickQuotePlans.length; i++){
      if(this.maximumInsuAmtCode!='' && insuranceCode == this.QuickQuotePlans[i].sicode){
        this.insuredPlanTitle = (this.QuickQuotePlans[i].title).toLowerCase();
        console.log("individual: " + this.insuredPlanTitle);
        this.familyInsuredPlanType = this.QuickQuotePlans[i].plancode;
      }else if( this.PolicyTypeSelected ==  this.familyFloaterCode && insuranceCode == this.QuickQuotePlans[i].sicode){
        this.insuredPlanTitle = (this.QuickQuotePlans[i].title).toLowerCase();
        console.log("Family: " + this.insuredPlanTitle);
        
        this.familyInsuredPlanType = this.QuickQuotePlans[i].plancode;
      }
      if(insuranceCode == this.QuickQuotePlans[i].sicode){
        return this.QuickQuotePlans[i].amtSelectedTitle;
      }
    }
  }

  saveMemberDetails(popupNumber,addAnother){  
    this.displayAge(this.memberAge);
    console.log("status: " + this.hideForSelf);    
      console.log("Status: " + this.childAgeStatus);
    if(popupNumber == 1 || popupNumber==2){
      if(this.slide == '' && this.showInsurenceDiv){
        this.showToast("Please select insurance amount.");
      }else if(this.memberAge.trim() == "" || this.memberAge.length<10){
        this.showToast("Please select member's age.");
      }else if(this.invalidDob(this.memberAge)){
        this.showToast("Please enter valid Date of Birth.");
      }else if(this.checkSTPORNonSTP(this.memberAge) && (this.popupMemberData.code == "SELF" || this.popupMemberData.code == "SPOU" || this.popupMemberData.code == "FATH" || this.popupMemberData.code == "MOTH")){
        this.showToast("The maximum age for proposer should not be more than 50 years. Please contact nearest branch.");      
      }else if(this.childAgeStatus == false && (this.popupMemberData.code == "SON" || this.popupMemberData.code == "DAUG")){
        this.showToast("Child's age should not be more than 25 years to enter policy");
      }else if(this.validAgeBelow18(this.memberAge) && (this.popupMemberData.code == "SELF" || this.popupMemberData.code == "SPOU" || this.popupMemberData.code == "FATH" || this.popupMemberData.code == "MOTH")){
        this.showToast("Proposer's age is below 18 years. We apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
      }
      else if(this.validAgeAbove50(this.memberAge) && (this.popupMemberData.code == "SELF" || this.popupMemberData.code == "SPOU" || this.popupMemberData.code == "FATH" || this.popupMemberData.code == "MOTH")){
        this.showToast("Insured person's age is above 50 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
      }else if(this.personHt == ''){
        this.showToast("Please select height to proceed.");
      }else if(this.personWt.trim() == '' || this.personWt =='0'){
        this.showToast("Please enter weight to proceed");
      }else if(this.personWt.length == 1){
        this.showToast("Please enter valid weight");
      }else if(this.personWt.trim() == "00" || this.personWt.trim() == "000"){
        this.showToast("Please enter valid weight");
      }else if(this.calculateBMI(this.memberAge)){
        this.showToast("Insured person's BMI is either obese or underweight. We apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
      }else if(this.marriedStatus == "" && this.popupMemberData.code == "SELF"){
        this.showToast("Please select marital status");
      }/*else if(this.getBMIOrNot == true){
        console.log("BMIStatus" + this.getBMIOrNot);        
       
      }*/else{
        this.showTransBaground = false;  
        if(this.popupMemberData.code == "SELF"){
          //document.getElementById("addMember").style.marginBottom='60px';          
          this.maximumInsuAmtCode = this.slide;
          this.membersShow[0].detailsSaved = true;
          this.membersShow[0].age = this.memberAge;
          this.membersShow[0].ageText = this.getAge(this.memberAge);          
          this.membersShow[0].insuredCode = this.planTypeCode;
          this.membersShow[0].PolicyType = this.PolicyTypeSelected;
          this.membersShow[0].Height = this.personHt;
          this.membersShow[0].Weight =  Number(this.personWt).toFixed(0);
          console.log("Self: " + this.bmiLoadingStatus);
          
          if(this.bmiLoadingStatus == "Yes"){
            this.membersShow[0].bmiLoading = "Yes";
            this.membersShow[0].loadingPerc = this.bmiLoadingPerc;
          }else{
            this.membersShow[0].bmiLoading = "No";
            this.membersShow[0].loadingPerc = this.bmiLoadingPerc;
          }
          
          this.membersShow[0].HeightText= this.filledHeight(this.personHt);
          console.log("heightText:"  + JSON.stringify(this.membersShow[0].HeightText));
          
          this.membersShow[0].insuredAmtText = this.displayAmt(this.planTypeCode);
          this.membersShow[0].smoking = this.memberSmokes;
          this.membersShow[0].insuredAmtInDigit = "";
          this.membersShow[0].ProposerDetailsDivShow = true;
          if(this.memberSmokes){
            this.membersShow[0].smokingText = "Smoking";
          }else{
            this.membersShow[0].smokingText = "";
          }
          if(this.marriedStatus == "Married"){
            this.membersShow[0].maritalStatus = "Married";
            this.membersShow[0].maritalCode = "M"
            this.hidePopupSmokerDiv = true;
            //document.getElementById("addMember").style.marginBottom='5px';
            this.hideProceedButton = true;
            this.membersShow[0].showMarried = true;
            
            //this.popupImg = memberData.imgUrl;
            //this.popupMemberData = memberData;

          }else if(this.marriedStatus == "Single" || this.marriedStatus == "Divorced" || this.marriedStatus == "Widow / Widower"){
            //document.getElementById("addMember").style.marginBottom='60px';
            document.getElementById("btnTopMargin").style.marginBottom='40px';
            //this.hideProceedButton = false;
            this.membersShow[0].maritalStatus = ""; 
            this.hidePopupSmokerDiv = false;
            this.disableProceedBt = false;
            if(this.marriedStatus == "Single" && this.PolicyTypeSelected == "HTF"){
              this.membersShow[0].maritalStatus = "Single";
              //document.getElementById("addMember").style.marginBottom='5px';              
              //this.hideProceedButton = true;

              /*Prachi*/
              this.membersShow[1].detailsSaved = false;
              this.membersShow[1].smoking = false;
              this.membersShow[1].insuredAmtText = "";
              this.membersShow[1].ageText = "";
              this.membersShow[1].smokingText = "";
              this.membersShow[1].maritalStatus = "";
              if(this.membersInsured.length > 1  && this.membersInsured[1].code == "SPOU"){
                this.membersInsured.splice(1, 1);
                console.log("IFToday");                
              }

              
            }else if(this.marriedStatus == "Divorced"){
              //document.getElementById("addMember").style.marginBottom='5px';
              //this.hideProceedButton = true;
              this.membersShow[0].maritalStatus = "Divorced";
              this.hideProceedButton = false;
            }else if(this.marriedStatus == "Widow / Widower"){
              //document.getElementById("addMember").style.marginBottom='5px';
              //this.hideProceedButton = true;
              this.membersShow[0].maritalStatus = "Widow / Widower";
              this.hideProceedButton = false;
            }
            
            this.membersShow[0].maritalCode = "S";
            this.membersShow[0].showMarried = false;
            this.hideProceedButton = false;
          }
          this.slide ='';
          this.memberAge = "";
          this.memberSmokes = false;
          this.memberMarried = false;
          this.hideMemberPopup = false;
          this.hideChildPopup = false;
          this.hidePopupMarriedDiv = false;
          if(this.editUserData){
            this.membersInsured.splice(0, 1, this.membersShow[0]);
          }else{
          this.membersInsured.push(this.membersShow[0]);
          }
          
          this.showTransBaground = true;
          if( this.PolicyTypeSelected == this.individualCode){
            this.disableProceedBt = false;
          }

          // if(this.membersInsured.length == 1){
          //   this.hideProceedButton == false;
          // }
          
// this.hidePopupSmokerDiv = true;
// this.showTransBaground = false; 
// this.hideMemberPopup = true;
// this.popupTitle = "Spouse";
          
        }else{
          this.hideProceedButton = false;
          console.log("test:: " + JSON.stringify(this.membersShow));
          console.log("vital " + JSON.stringify(this.membersInsured));
                    
          for(let i = 0;i < this.membersShow.length;i++){
            if(this.insuredPlanTitle == 'vital' && this.membersInsured.length >=6 && this.editUserData== false){
              this.showToast("Since you have opted for Vital Plan, you can't insure more than 6 members");
              break;
            }else if( this.membersInsured.length >=15 ){
              this.showToast("You can insure maximum 15 members.");
              break;
            }
            if(this.popupMemberData.code == this.membersShow[i].code){

              this.membersShow[i].smoking = this.memberSmokes;
              if(this.memberSmokes){
                this.membersShow[i].smokingText = "Smoking";
              }else{
                this.membersShow[i].smokingText = "";
              }

              console.log("index: " + this.vitalMembers.indexOf(this.membersShow[i].code));
              
             
              if(this.insuredPlanTitle == 'vital' && this.vitalMembers.indexOf(this.membersShow[i].code)<0){
                this.showToast("Vital Plan includes only immediate family members.");
              }else if(this.insuredPlanTitle == 'vital' && this.vitalMembers.indexOf(this.membersShow[i].code)>=0){
                
                if(this.membersShow[i].title ==this.popupTitle ){
                  this.saveMemberData(i,popupNumber,addAnother);
                  break;
                }
              }else if(this.insuredPlanTitle != 'vital'){
                if(this.membersShow[i].title ==this.popupTitle ){
                  this.saveMemberData(i,popupNumber,addAnother);
                  break;
                }
              }
            }
          }
        }
       
      }
    }
  }

  calculateBMI(memberAge){ 
    this.bmiLoadingPerc = "";
    this.bmiLoadingStatus = "";
    let ageNumber = this.getAge(this.memberAge).split(" ");
    console.log("ageinNum: " + ageNumber[0]);
    if(Number(ageNumber[0]) >= 16 && Number(ageNumber[0]) <= 50){
      this.bmi = Number(this.getBMI(this.personHt, Number(this.personWt).toFixed(0)));
      console.log("BMIDIGIT: " + this.bmi);
      

      if(this.bmi >= 18){
        document.getElementById("btnTopMargin").style.marginBottom='40px';
        //this.hideProceedButton = false;
        if(this.bmi  >= 30 && this.bmi <= 32){
          console.log("BMI-IF");                     
          this.bmiLoadingStatus = "Yes";
          this.bmiLoadingPerc = 0;
          return false;
        }else if(this.bmi >= 32 && this.bmi <= 34){        
          console.log("BMI-ELSE");
          this.bmiLoadingStatus = "Yes";
          this.bmiLoadingPerc = 15;
          return false;
        }else if(this.bmi < 30){
          this.bmiLoadingStatus = "No"
          this.bmiLoadingPerc = 0;
          return false;
        }else if(this.bmi > 34){
          this.bmiLoadingStatus = "No"
          this.bmiLoadingPerc = 0;
          return true;
        }
      }else{
        this.hideProceedButton = true;
        this.showToast("Insured person's BMI is either obese or underweight. We apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
        return true;
      }
      
    }else{
      this.bmiLoadingStatus = "No";
      return false;
    }
  }

  getBMI(height, weight){ 
    console.log("height: " + height + "weight: " + weight);
    
    let heightToMeters = height / 100.00;
    console.log("meters: " + heightToMeters);
    
    let bmi = weight / (heightToMeters * heightToMeters);
    console.log("bmi" + bmi);
    
    return bmi.toFixed(2);
  }

  saveMemberData(indexNumber,popupNumber,addAnother){
    console.log("Other Smoking: " + this.memberSmokes);
    
    this.membersShow[indexNumber].detailsSaved = true;
    this.membersShow[indexNumber].age = this.memberAge;
    this.membersShow[indexNumber].ageText = this.getAge(this.memberAge);
    if(this.PolicyTypeSelected == this.individualCode){
      this.membersShow[indexNumber].insuredCode = this.planTypeCode;
    }else if(this.PolicyTypeSelected == this.familyFloaterCode){
      this.membersShow[indexNumber].insuredCode = this.planTypeCode;
    }
    this.membersShow[indexNumber].insuredAmtText = this.displayAmt(this.planTypeCode);
    this.membersShow[indexNumber].PolicyType = this.PolicyTypeSelected;
    this.membersShow[indexNumber].Height = this.personHt;
    this.membersShow[indexNumber].HeightText= this.filledHeight(this.personHt);
    this.membersShow[indexNumber].Weight = Number(this.personWt).toFixed(0);
    this.membersShow[indexNumber].smoking = this.memberSmokes;
    if(this.memberSmokes){
      this.membersShow[indexNumber].smokingText = "Smoking";
    }else{
      this.membersShow[indexNumber].smokingText = "";
    }  
    // let ageNumber = Number(this.getAge(this.memberAge).split(" "));
    // if(ageNumber[0] >= 16 && ageNumber[0] <= 50){
    //   let bmi = Number(this.getBMI(this.personHt, Number(this.personWt).toFixed(0)));
    //   if(bmi  >= 30 && bmi <= 32){
    //     console.log("BMI-IF");              
    //     this.membersShow[0].bmi =  "15";
    //   }else if(bmi > 32 && bmi <= 34){
    //     this.membersShow[0].bmi =  "25";
    //     console.log("BMI-ELSE");
    //   }else{
    //     this.showToast("You can not proceed online")
    //   }
      
    // }
    if(this.bmiLoadingStatus == "Yes"){
      this.membersShow[indexNumber].bmiLoading = "Yes";
      this.membersShow[indexNumber].loadingPerc = this.bmiLoadingPerc;
    }else{
      this.membersShow[indexNumber].bmiLoading = "No";
      this.membersShow[indexNumber].loadingPerc = this.bmiLoadingPerc;
    }    
   
    this.hideMemberPopup = false;
    this.hideChildPopup = false;
    this.hidePopupSmokerDiv = false;
    this.hidePopupMarriedDiv = false;
    this.slide ='';
    this.memberAge = "";
    this.childsAge= 0;

    if(this.membersShow[indexNumber].code == 'SPOU'){
      if(this.editUserData){
        this.membersInsured.splice(1, 1, this.membersShow[indexNumber]);
        console.log("IF condition");
      }else{
        this.membersInsured.splice(1, 0, this.membersShow[indexNumber]);
      }
    }else if(addAnother != 1){
      if(this.editUserData){
        for(let i = 1; i < this.membersInsured.length ; i++){
          if(this.membersShow[indexNumber].code == this.membersInsured[i].code && this.membersShow[indexNumber].title == this.membersInsured[i].title){
            this.membersInsured.splice(i, 1, this.membersShow[indexNumber]);
            break;
          }
        }
   }else{
      this.membersInsured.push(this.membersShow[indexNumber]);
    }
    }else if(addAnother == 1){
      if(this.editUserData){
        for(let i = 1; i < this.membersInsured.length ; i++){
          if(this.membersShow[indexNumber].code == this.membersInsured[i].code && this.membersShow[indexNumber].title == this.membersInsured[i].title){
            this.membersInsured.splice(i, 1, this.membersShow[indexNumber]);
            this.addMemberClicked2(this.popupMemberData.code, indexNumber);
            break;
          }
        }
      }else{
      this.addMemberClicked2(this.popupMemberData.code, indexNumber);
    }
     
    }
    this.showTransBaground = true;
    
    if(  (this.membersInsured.length >= 2 && this.PolicyTypeSelected == this.familyFloaterCode)|| ( this.membersInsured.length >= 1 &&  this.PolicyTypeSelected == this.individualCode)){
      this.disableProceedBt = false;
    }else{
      this.disableProceedBt = true;
    }
  }

  // getSumInsuredArray(){
  //   this.QuickQuotePlans =[];
  //   this.QuickQuotePlans = this.appService.getSumInsuredArray(this.apiPlanType);
  // }

  // populateInsurenceAmt(Number,memberCode){
  //   this.insurenceAmtToShow = [];
  //   for(let i = 0; i < this.QuickQuotePlans.length;i++){
  //     if(this.maximumInsuAmtCode!='' && this.QuickQuotePlans[i].sicode == this.maximumInsuAmtCode && memberCode != "SELF"){
  //       this.insurenceAmtToShow.push(this.QuickQuotePlans[i]); 
  //       if(i==3){
  //         this.insurenceAmtToShow = this.insurenceAmtToShow.slice(this.insurenceAmtToShow.length - 3, this.insurenceAmtToShow.length); 
  //         break; 
  //         }
  //         else if(i>3){
  //         this.insurenceAmtToShow = this.insurenceAmtToShow.slice(i-2, this.insurenceAmtToShow.length); 
  //         break; 
  //         }
  //         else if(i<3){
  //         this.insurenceAmtToShow = this.insurenceAmtToShow.slice(0,i+1); 
  //         break;
  //         }                
  //       //break;
  //     }else{
  //       this.insurenceAmtToShow.push(this.QuickQuotePlans[i]);
  //     }
  //     if(Number == "0" || Number == 0){
  //       this.familyInsurenceAmt.push(this.QuickQuotePlans[i]);
  //     }
  //   }

  //   //saved on home page
  //   var posVerification = sessionStorage.posVerification;
    
  //   if(this.loginData == true && posVerification == "Y"){
  //     this.familyInsurenceAmt = this.familyInsurenceAmt.slice(0, 2);
  //     this.insurenceAmtToShow = this.insurenceAmtToShow.slice(0, 2);
  //   }
    
  // }

  
  clearMemberDetails(event : Event, member){
    console.log("remove Members: " + this.membersInsured.length);
     
    event.stopPropagation();
    //document.getElementById("addMember").style.marginBottom='5px';
    this.hideProceedButton = false;
    for(let i = 0; i < this.membersShow.length;i++){
      if(member.code == "SELF"){
        this.membersShow[i].detailsSaved = false;
        this.membersShow[i].smoking = false;
        member.insuredAmtText = "";
        member.ageText = "";
        member.smokingText = "";
        member.maritalStatus = "";
        this.maximumInsuAmtCode = '';
        this.insuredPlanTitle = '';
      }else if(member.code == this.membersShow[i].code && member.title == this.membersShow[i].title){
        this.membersShow[i].detailsSaved = false;
        this.membersShow[i].smoking = false;
        member.insuredAmtText = "";
        member.ageText = "";
        member.smokingText = "";
        member.maritalStatus = "";
        break;
      }
    }
    for(let i = 0 ; i < this.membersInsured.length ; i++){
      if(member.code == "SELF"){
        this.membersInsured = [];
      }else if(member.code == this.membersInsured[i].code){
        console.log("1");
        console.log("" + this.membersInsured[i].code);
                
        this.membersInsured.splice(i, 1);
        console.log("updatedMember: "+ JSON.stringify(this.membersInsured));        
        console.log("length: " + this.membersInsured.length);
        
        break;
      }
    }
   
    // if(  (this.membersInsured.length >= 2 && this.PolicyTypeSelected == this.familyFloaterCode)|| ( this.membersInsured.length >= 1 &&  this.PolicyTypeSelected == this.individualCode)){
    //   this.disableProceedBt = false;
    //   //document.getElementById("btnTopMargin").style.marginBottom='40px';
    //   this.hideProceedButton = false;
    // }else{
    //   this.disableProceedBt = true;
    //   //this.hideProceedButton = true;
    // }

    if(this.membersInsured.length >= 2 && this.membersInsured[0].maritalStatus == "Married"){
      this.disableProceedBt = false;
      //document.getElementById("btnTopMargin").style.marginBottom='40px';
      this.hideProceedButton = false;
    }else if(this.membersInsured.length >= 1 && this.membersInsured[0].maritalStatus == "Married"){
      this.disableProceedBt = true;
      this.hideProceedButton = true;
    }else{
      this.disableProceedBt = false;
      //this.hideProceedButton = true;
    }
  }


  addMemberClicked2(memberCode,index){
    //alert("Son2" + memberCode);
    for(let i = 0; i < this.membersShow.length;i++){
      
      if(this.membersShow[i].code == memberCode &&(memberCode == "SON" || memberCode == "DAUG")){
        let memberName = '';
        let term = this.membersShow.filter(person => person.code == memberCode );
        console.log("term? " + JSON.stringify(term));
        
        if(memberCode == "SON"){          
         this.showToast(" You can now take policy for another son.");
          memberName = 'Son';
          this.membersShow[i].title = 'Son1'
        }else if(memberCode == "DAUG"){          
          this.showToast(" You can now take policy for another daughter.");
          memberName = 'Daughter';
          this.membersShow[i].title = 'Daughter1'
        }
        this.membersShow.splice(i+term.length, 0, {
        //"MemberId": i+1, 
        "code":this.membersShow[i].code,
        "title":memberName+ (term.length +1),
        "imgUrl":term[0].imgUrl,
        "smoking" : false,
        "showDependent":true,
        "showAddAnotherBtn":true,
        "smokingText":"",
        "showMarried":false,
        "maritalStatus":"",
        "age":"",
        "ageText":"",
        "insuredCode":"",
        "insuredAmtText":"",
        "PolicyType": '',
        "disableRemove":false,
        "ShowInsurenceAmt":false,
        "showFamilyMemberOnScreen":true,
        "popupType":"2",
        "detailsSaved":false,        
      }); 
        break;
      }       
    } 
    if(this.editUserData == false){
      this.membersInsured.push(this.membersShow[index]); 
    }
    
    console.log("BuyMember: " + JSON.stringify(this.membersInsured));
    
    this.popupMember = "";   
  }



  addMemberClicked(event : Event, k){   
    for(let i = 0; i < this.membersShow.length;i++){
      if(this.membersShow[i].code == k.code &&(k.code == "SON" || k.code == "DAUG")){
        let term = this.membersShow.filter(person => person.code == k.code );
        let memberName = '';
        if(k.code == "SON"){
          memberName = 'Son';
          this.membersShow[i].title = 'Son1'

        }else if(k.code == "DAUG"){
          memberName = 'Daughter';
          this.membersShow[i].title = 'Daughter1'
        }

        this.membersShow.splice(i+term.length, 0, {
        //"MemberId": i+1,
        "code":this.membersShow[i].code,       
        "title":memberName+ (term.length +1),
        "imgUrl":k.imgUrl,
        "smoking" : false,
        "showDependent":true,
        "showAddAnotherBtn":true,
        "smokingText":"",
        "showMarried":false,
        "maritalStatus":"",
        "age":"",
        "ageText":"",
        "insuredCode":"",
        "insuredAmtText":"",
        "PolicyType": '',
        "disableRemove":false,
        "ShowInsurenceAmt":false,
        "showFamilyMemberOnScreen":true,
        "popupType":"2",
        "detailsSaved":false
      }); 
      break;
      }
    } 
    this.popupMember = ""; 
  }

  getBuyResultClicked(){
    //this.displayAmt(this.familySlide);
    if(this.membersInsured.length == 0){
      this.showToast("Please select atleast one member");
    }else if(this.membersInsured.length == 1){
      //this.membersInsured[]
      for(let i= 0 ; i < this.membersInsured.length ;i++ ){      
        this.membersInsured[i].insuredPlanTypeCode = this.planTypeCode;
        this.membersInsured[i].insuredPlanTypeText = this.planTypeText;
        this.membersInsured[i].PolicyType = "HTI";
      }
      console.log("memberSuper" + JSON.stringify(this.membersInsured));
      this.navCtrl.push(TBuyPageResultPage, {displayMembers : this.membersInsured, deductableAmount: this.selectedDeductable, siAmounut: this.selectedAmount, SuminsuredArray : this.SIArray});
    }else if( (this.membersInsured.length == 1 && this.membersInsured[0].maritalStatus == "Married") || (this.membersInsured.length > 1 && this.membersInsured[1].code != "SPOU" && this.membersInsured[0].maritalStatus == "Married") ){
      this.showToast("Please enter spouse details.");
    }else{
      let alert = this.alertCtrl.create({
        title: 'You have selected Family Policy',
        subTitle: '',
        cssClass: 'my-class',
        buttons: [
        {
        text: 'OK',
        handler: () => {
        console.log('Agree clicked');
        for(let i= 0 ; i < this.membersInsured.length ;i++ ){      
          // this.membersInsured[i].insuredPlanTypeCode = this.familyInsuredPlanType;
          // this.membersInsured[i].insuredPlanTypeText = this.insuredPlanTitle.toUpperCase();
          this.membersInsured[i].insuredPlanTypeCode = this.planTypeCode;
          this.membersInsured[i].insuredPlanTypeText = this.planTypeText;
          this.membersInsured[i].PolicyType = "HTF";
        }        
        console.log("memberSuper" + JSON.stringify(this.membersInsured));
        
        this.navCtrl.push(TBuyPageResultPage,{displayMembers : this.membersInsured, deductableAmount: this.selectedDeductable, siAmounut: this.selectedAmount, SuminsuredArray : this.SIArray});
        alert.dismiss();
        }
        }
        ]
        });
        alert.present();
    }
    if(this.membersInsured.length == 1){
      this.showToast("Minimum two beneficiaries are required for FLOATER policy.");
    }else if(this.membersInsured.length == 0){
      this.showToast("Please insure atleast 'self' to proceed.");
    }else if( (this.membersInsured.length == 1 && this.membersInsured[0].maritalStatus == "Married") || (this.membersInsured.length > 1 && this.membersInsured[1].code != "SPOU" && this.membersInsured[0].maritalStatus == "Married") ){
      this.showToast("Please enter spouse details.");
    }else if(this.PolicyTypeSelected == this.individualCode){
      sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.membersInsured);
      sessionStorage.MembersShownOnforBuy = JSON.stringify(this.membersShow);
      this.beneficiaryDetailsArray();
      for(let i= 0 ; i < this.membersInsured.length ;i++ ){      
        this.membersInsured[i].insuredPlanTypeCode = this.planTypeCode;
        this.membersInsured[i].insuredPlanTypeText = this.planTypeText;
      }

      let callRequest = this.appService.callServiceDetails(this.PolicyTypeSelected,this.beneficiaryDetails);
      var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
      this.navCtrl.push(TBuyPageResultPage,{displayMembers : this.membersInsured, deductableAmount: this.selectedDeductable, siAmounut: this.selectedAmount, SuminsuredArray : this.SIArray});
      console.log("members: " + JSON.stringify(this.membersInsured));
      
    }else if(this.PolicyTypeSelected ==  this.familyFloaterCode && this.familySlide != ''){
      for(let i= 0 ; i < this.membersInsured.length ;i++ ){
        this.membersInsured[i].insuredCode = this.planTypeCode;
        this.membersInsured[i].insuredPlanTypeCode = this.planTypeCode;
        this.membersInsured[i].insuredPlanTypeText = this.planTypeText;
      }
      if(this.insuredPlanTitle == 'vital'){
        for(let i= 0 ; i < this.membersInsured.length ;i++ ){
          if(this.vitalMembers.indexOf(this.membersInsured[i].code)<0){
            this.showToast("Vital Plan includes only immediate family members.");
            break;
          }else if( i == (this.membersInsured.length -1)){
            sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.membersInsured);
            sessionStorage.MembersShownOnforBuy = JSON.stringify(this.membersShow);
            this.beneficiaryDetailsArray();
            let callRequest = this.appService.callServiceDetails(this.PolicyTypeSelected,this.beneficiaryDetails);
            console.log("");
            
            var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
            this.navCtrl.push(TBuyPageResultPage,{displayMembers : this.membersInsured, deductableAmount: this.selectedDeductable, siAmounut: this.selectedAmount, SuminsuredArray : this.SIArray});
            console.log("members: " + JSON.stringify(this.membersInsured));
          }
        }
      }else{
        sessionStorage.quickQuoteMembersDetails = JSON.stringify(this.membersInsured);
        this.beneficiaryDetailsArray();
        let callRequest = this.appService.callServiceDetails(this.PolicyTypeSelected,this.beneficiaryDetails);
        var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
        this.navCtrl.push(TBuyPageResultPage,{displayMembers : this.membersInsured, deductableAmount: this.selectedDeductable, siAmounut: this.selectedAmount, SuminsuredArray : this.SIArray});
        console.log("members: " + JSON.stringify(this.membersInsured));
      }
    }
  }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'OK',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }


  memberClicked(event : Event,memberToToggle){
    if(memberToToggle.code!= "SELF"){
      for(let i =0; i < this.membersShownOnPopup.length;i++){
        if(memberToToggle.code == this.membersShownOnPopup[i].code && memberToToggle.title == this.membersShownOnPopup[i].title){
          this.membersShownOnPopup[i].memberSelectedOnAddmember = !this.membersShownOnPopup[i].memberSelectedOnAddmember
        }
      }
    }
  }

  hideShowAddmember(Num){
    if(Num == 1){
      this.showTransBaground2 = !this.showTransBaground2;   
      this.membersShownOnPopup = JSON.parse(JSON.stringify(this.membersShow));
      this.isClassOneActive = true;
      this.showAddMember = true;
    }else if(Num == 2){
      this.isClassOneActive = false;
      setTimeout( () => { 
            this.showAddMember = false;
            this.showTransBaground2 = !this.showTransBaground2;  
      }, 1000);
    }else if(Num == 3){
      for(let i = 0; i < this.membersShownOnPopup.length; i++){
        if(!this.membersShownOnPopup[i].showFamilyMemberOnScreen && this.membersShownOnPopup[i].memberSelectedOnAddmember){
          this.membersShow[i].showFamilyMemberOnScreen = this.membersShownOnPopup[i].memberSelectedOnAddmember;
          this.membersShow[i].memberSelectedOnAddmember = this.membersShownOnPopup[i].memberSelectedOnAddmember;
        }
        
      }   
      this.isClassOneActive = false;
      setTimeout( () => { 
            this.showAddMember = false;
            this.showTransBaground2 = !this.showTransBaground2;  
      }, 1000);
    }
  }
 

  changePolicyType(){
    console.log("policy: " + this.PolicyTypeSelected);
    console.log("member: " + JSON.stringify(this.membersInsured));
    this.individualPlan = "";
    this.familyPlan = "";
    this.marriedStatus = "";
    let policy
    if(this.PolicyTypeSelected == "HTI"){
      policy ="Family Floater";
    }else{
      policy ="Individual";
    }
    
    if(this.membersInsured.length != 0){
      const alert = this.alertCtrl.create({
        title: 'Alert',
        message: "If you opt for this Policy, the members will be unselected from the" + " " + policy + " " +"policy.",
        enableBackdropDismiss: false, 
        buttons: [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              if(this.PolicyTypeSelected == "HTI"){
                this.PolicyTypeSelected = "HTF";
                //this.hideProceedButton = true;
              }else{
                this.PolicyTypeSelected = "HTI";
                //this.hideProceedButton = true;
              }            
            }
        },{
            text: 'Continue',
            handler: () => {
              sessionStorage.isViewPopped = "";
              this.membersInsured = [];
              this.disableProceedBt = true;
              this.hideProceedButton = true;
              this.membersShow = [];
              this.membersArrayFunction();              
              //document.getElementById("addMember").style.marginBottom='5px';              
              for(let i = 0; i < this.membersShow.length;i++){
                  this.membersShow[i].detailsSaved = false;
                  this.membersShow[i].insuredAmtText = "";
                  this.membersShow[i].ageText = "";
                  this.membersShow[i].smokingText = "";
                  this.membersShow[i].maritalStatus = "";
                  this.maximumInsuAmtCode = '';
                  this.insuredPlanTitle = '';
                  if( this.PolicyTypeSelected == this.individualCode){
                    this.membersShow[i].ShowInsurenceAmt = true;
                    this.showInsurenceDiv = true;
                    this.individualDiscount = true;
                    this.insuredPlanTitle == 'vital';
                    this.familySlide = '';
                    this.hideDisableCards = true;
                  }else if(this.PolicyTypeSelected == this.familyFloaterCode){
                    this.hideDisableCards = false;
                    this.membersShow[i].ShowInsurenceAmt = false;
                    this.showInsurenceDiv = false;
                    this.individualDiscount = false;
                    this.insuredPlanTitle == '';
                  }
              }
            }
        }]
     });
     alert.present();
    }else{
      this.membersInsured = [];
      this.disableProceedBt = false;
      this.hideProceedButton = true;
      for(let i = 0; i < this.membersShow.length;i++){
          this.membersShow[i].detailsSaved = false;
          this.membersShow[i].insuredAmtText = "";
          this.membersShow[i].ageText = "";
          this.membersShow[i].smokingText = "";
          this.membersShow[i].maritalStatus = "";
          this.maximumInsuAmtCode = '';
          this.insuredPlanTitle = '';
          if( this.PolicyTypeSelected == this.individualCode){
            this.membersShow[i].ShowInsurenceAmt = true;
            this.showInsurenceDiv = true;
            this.individualDiscount = true;
            this.insuredPlanTitle == 'vital';
            this.familySlide = '';
            this.hideDisableCards = true;
          }else if(this.PolicyTypeSelected == this.familyFloaterCode){
            this.hideDisableCards = false;
            this.membersShow[i].ShowInsurenceAmt = false;
            this.showInsurenceDiv = false;
            this.individualDiscount = false;
            this.insuredPlanTitle == '';
          }
      }
    }
    

  
  }

  newCustomer(){
  this.showTransBaground3 = !this.showTransBaground3;
  }

  // DatePick(){    
  //   let minDateLimit  = ( new Date(1900,1,1).getTime());
  //   if(this.popupMember == "Child"){
  //     minDateLimit  = ( new Date(this.today.getFullYear()-25,this.today.getMonth(),this.today.getDate()).getTime());
  //   }
  //     this.datePicker.show({
  //       date: new Date(this.memberAge),
  //       minDate: minDateLimit,
  //       maxDate: Date.now(),
  //       mode: 'date',
  //       androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
  //     }).then(
  //       date => {
  //         let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
  //         this.memberAge = dt.toString();          
  //       },
  //       err => console.log('Error occurred while getting date: ', err)
  //     );
  // }

  // DatePick(event){
  //   let element = (<HTMLInputElement>event.target.previousElementSibling.querySelector('input[name="age"]'));
  //   let dateFieldValue = element.value;
  //   let year : any = 1985, month : any = 1, date : any = 1;
  //   if(dateFieldValue!="" || dateFieldValue == undefined){
  //   date = dateFieldValue.split("-")[0]
  //   month = dateFieldValue.split("-")[1];
  //   year = dateFieldValue.split("-")[2]
  //   }
    
  //   let minDateLimit = ( new Date(1985,1,0).getTime());
  //   this.datePicker.show({
  //   date: new Date(year,month-1,date),
  //   minDate: minDateLimit,
  //   maxDate: Date.now(),
  //   mode: 'date',
  //   androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
  //   }).then(
  //   date => {
  //   let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
  //   element.focus();
  //   element.value = dt;
  //   element.textContent = dt;
  //   },
    
  //   err => console.log('Error occurred while getting date: '+err)
  //   );
    
  //   }

    DatePick(){
    this.keyboard.close();      
    let element = this.memberAge;
    let dateFieldValue = element;
    let year : any = 1985, month : any = 1, date : any = 1;
    this.childAgeStatus = true;
    if(dateFieldValue!="" ||  dateFieldValue == undefined){
      date = dateFieldValue.split("-")[0]
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }else if(this.popupMember == "Child"){
      date = 1;
      month = 1;
      year = this.today.getFullYear()-25;
    }
    

    let minDateLimit  = this.popupMember == "Child" ? ( new Date(year,1,1).getTime()) : (new Date(1900,1,1).getTime()); 
    console.log("popMember: " + this.popupMember);
    
    this.datePicker.show({
      date: new Date(year, month-1, date),
      minDate: minDateLimit,
      maxDate: Date.now(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        let dt = ( this.numberLength(date.getDate())+"-"+this.numberLength(date.getMonth() + 1)+"-"+(date.getFullYear())).toString();
        let age = this.getAge(dt);
       if(parseInt(age.split(" ")[0]) > 50){
        this.showToast("Insured person's age is above 50 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
        this.memberAge = "";
       }else if(parseInt(age.split(" ")[0]) > 24 && this.popupMember == "Child" && this.insuredPlanTitle == "vital"){
          this.showToast("Dependent child's age is more than 25 years. We apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
          this.memberAge = "";
          this.childAgeStatus = false;
       }else if(parseInt(age.split(" ")[0]) > 60 && (this.popupMember == "Father" || this.popupMember == "Mother")){
        console.log("Pare");
        
       this.showToast("Parent's age must not be more than 60 years to enter policy");
       this.memberAge = "";
      }
        this.memberAge = dt;
        //this.memberArray[index].nomineeAge  = this.SelfDob[index];
        //console.log("DOB: " + members.nomineeDate);
        console.log("DOB: " + this.memberAge);
      },

      err => console.log('Error occurred while getting date: '+err)
    );
    }

  beneficiaryDetailsArray(){
    this.beneficiaryDetails = [];
    this.totalLoading ="";
    this.bmiLoad = 0;
    let smokinLoad = 0;
    console.log("JSON: " + JSON.stringify(this.membersInsured));
    
    for(let i = 0;  i<this.membersInsured.length ; i++){
      let smokinLoad = 0;
      if(this.membersInsured[i].smoking){
        smokinLoad = Number(this.smokingLoad);
      }
      if(this.membersInsured[i].bmiLoading == "Yes"){
        this.bmiLoad = Number(this.membersInsured[i].loadingPerc);
        // console.log("loadBMI: " + this.bmiLoad);  
        // this.totalLoading = smokinLoad + this.bmiLoad; 
        // console.log("total" + this.totalLoading);
             
      }else{
        this.bmiLoad = 0;
      }
      this.beneficiaryDetails.push({
            //"MemberId": i+1,
            "InsuredName": "",
            "InsuredDob": this.membersInsured[i].age,
            "InsuredGender": "",
            "InsuredOccpn": "",
            "CoverType": this.insuredPlanTitle.toUpperCase(),
            "SumInsured": this.sIamount,
            "DeductibleDiscount": "",
            "Relation": this.membersInsured[i].code,
            "NomineeName": "",
            "NomineeRelation": "",
            "AnualIncome": "",
            "Height": Math.floor(this.membersInsured[i].Height),
            "Weight": this.membersInsured[i].Weight,
            "NomineeAge": "",
            "AppointeeName": "",
            "AptRelWithominee": "",
            "MedicalLoading": Number(this.smokingLoad) + this.bmiLoad,
            "PreExstDisease": "",
            "DiseaseMedicalHistoryList":this.appService.DiseaseMedicalHistory
      })
      console.log("JSON: " + JSON.stringify(this.beneficiaryDetails));
    }

    //this.membersInsured.push(this.beneficiaryDetails);
  }

  handleKeyUp(){
    this.keyboard.close();
    console.log("inputAge2: "+ this.memberAge);    
  }


  getSumInsuredAmt(sicode){
    for(let i = 0 ;  i < this.QuickQuotePlans.length ; i++){
      if(sicode == this.QuickQuotePlans[i].sicode){
        return this.QuickQuotePlans[i].suminsured;
      }
    }
  }

  // dobInput(ev){
  //   if(ev.key == "Backspace"){

  //   }else if(this.memberAge.length==2 || this.memberAge.length==5){
  //     this.memberAge = this.memberAge+"-";
  //     this.cdr.detectChanges();
  //     console.log("inputAge: "+ this.memberAge);
      
  //   }else if(ev.key == "Enter" && this.memberAge.length==10){
  //     this.handleKeyUp();
  //     console.log("inputAge1: "+ this.memberAge);
  //   }
  // }

  onOtherDateChange(ev){
    console.log("Pop:" + this.popupMember);
    
    
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.memberAge+"-";
   
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(ev.target.value);
      if(parseInt(age.split(" ")[0]) > 50){
        this.showToast("Insured person's age is above 50 years. We apologise that you cannot purchase on the app . Kindly contact our nearest branch office.");
        this.memberAge = "";
      }else if(parseInt(age.split(" ")[0]) > 24 && this.popupMember == "Child" && this.insuredPlanTitle == "vital"){
        console.log("child");
        this.showToast("Dependent child's age is more than 25 years. We apologise that you cannot purchase on app . Kindly contact our nearest branch office.");
        this.memberAge = "";
        ev.target.value = "";
        this.cdr.detectChanges();
      }else if(parseInt(age.split(" ")[0]) > 60 && (this.popupMember == "Father" || this.popupMember == "Mother")){
        console.log("Pare");
        
       this.showToast("Parent's age must not be more than 60 years to enter policy");
       this.memberAge = "";
       ev.target.value = "";
      }
      //this.memberArray[idx].age = ev.target.value;
      this.keyboard.close();
    }
    // else if(ev.keyCode == 8){
    //   ev.target.value.pop();
    // }
  }

  validAgeBelow18(age){
    console.log("age" + age);
    
    let validAge = this.getAge(age).split(" ");
    console.log("digit: " + validAge[0]);
    
    if(parseInt(validAge[0]) < 18){
      console.log("IF");
      return true;
    }else{
      console.log("ELSE");
      return false;
    }
  }

  validAgeAbove50(age){
    console.log("age" + age);
    
    let validAge = this.getAge(age).split(" ");
    console.log("digit: " + validAge[0]);
    
    if(parseInt(validAge[0]) > 50){
      console.log("IF");
      return true;
    }else{
      console.log("ELSE");
      return false;
    }
  }

  checkSTPORNonSTP(age){
    let validAge = this.getAge(age).split(" ");
    this.individualPlan
    if(parseInt(validAge[0]) > 50 &&  (this.individualPlan == "P001" ||  this.individualPlan == "P002")){
      console.log("IF");
      return true;
    }else if(parseInt(validAge[0]) > 50 &&  (this.familyPlan == "P001" ||  this.familyPlan == "P002")){
      console.log("IF");
      return true;
    } else{
      console.log("ELSE");
      return false;
    }
  }

  selectedAmount(amount){
    this.individualPlan = amount.plancode;
    if(this.individualPlan == "P003"){
      this.hideMemberPopup = false;   
      this.showTransBaground = true;  
      this.showToast("Regret we cannot process proposal further on app as it requires medical underwriting. Please contact our nearest Branch office.")

    }
  }

  filledStatus(status){
    console.log("marrieddState: "+status);
    
    for(let i =0; i < this.marriedStatusArray.length;i++){
      if(this.memberMarried == this.marriedStatusArray[i].status){
        return this.marriedStatusArray[i].value;
      }
    }

    if(status == "Single" && this.membersInsured[1].Relation == "SPOU"){
      this.membersInsured.splice(1, 1);
    }
  }

}
