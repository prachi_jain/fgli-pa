import { Component,NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TVerifyDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@NgModule({
  declarations: [
    TVerifyDetailsPage,
  ],
  imports: [
    //IonicPageModule.forChild(TVerifyDetailsPage),
  ],
})

@IonicPage()
@Component({ 
  selector: 'page-t-verify-details',
  templateUrl: 't-verify-details.html',
})
export class TVerifyDetailsPage {
	thankyouPopup = true;  
	otpPopup = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TVerifyDetailsPage');
    setTimeout(function() {         
      	document.getElementById("splash").style.display='none';                 
      }, 7000);
  }

}
