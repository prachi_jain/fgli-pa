import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtpPasswdPage } from './otp-passwd';

@NgModule({
  declarations: [
    //OtpPasswdPage,
  ],
  imports: [
    IonicPageModule.forChild(OtpPasswdPage),
  ],
})
export class OtpPasswdPageModule {}  
