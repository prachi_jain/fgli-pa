import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AppService } from '../../providers/app-service/app-service';
import { ViewProfilePage } from '../view-profile/view-profile';
import { LoginPage } from '../health/login/login';

/**
 * Generated class for the OtpPasswdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp-passwd',
  templateUrl: 'otp-passwd.html',
})
export class OtpPasswdPage {
  newpassword;
  confirmpassword;
  loading;
  canShowToast = true;
  passwordType = 'password';
  reEnterPasswordType = 'password';
  showPass = false;
  showreEnterPassword = false;
  picToView:string="assets/imgs/password_icon-01.svg";
  reEnterPicToView:string ="assets/imgs/password_icon-01.svg";
  serviceResponsePopup = true;
  message;
  serviceCodeStatus;

  constructor(public navCtrl: NavController, public toast: ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, public appService: AppService) {
  }

  ionViewDidLoad() {  
    console.log('ionViewDidLoad OtpPasswdPage');
    setTimeout(function() {         
      document.getElementById("splash").style.display='none';                 
  }, 7000);
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

showToast(Message){
  if(this.canShowToast){
    let pageToast = this.toast.create({
      message:Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 5000,
    });
    this.canShowToast = false;
    pageToast.present();
    pageToast.onDidDismiss(() => {
      this.canShowToast = true;
    });
  }   
}


  serviceCall(){
    if(this.newpassword.trim() == undefined || this.newpassword.trim() == null || this.newpassword.trim() == ""){
      this.showToast("Please enter your new password.");
    }else if(this.newpassword.length < 6){
      this.showToast("Password should be minimum 6 characters.");
    }else if(this.newpassword.length > 24){

    }
    // else if(!this.checkPasswordStrength(this.newpassword)){
    //   this.showToast("Please enter one capital, one small alphabet and one numeric character.")
    // }
    else if(this.confirmpassword.trim() == undefined || this.confirmpassword.trim() == null || this.confirmpassword.trim() == ""){
      this.showToast("Please confirm your new password.")
    }else if(this.newpassword.trim() != this.confirmpassword.trim()){
      this.showToast("Your password do not match.")
    }else{
      this.changePasswordServiceCall();
    }
  }


  checkPasswordStrength(password){
    var strongRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;    
    return strongRegex.test(password);
  }


  changePasswordServiceCall(){
    var sendData = {
      'username' : sessionStorage.username,
      'passwordNew' : this.newpassword
    }

    console.log("sendData: " + JSON.stringify(sendData));
    var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),sessionStorage.TokenId);
    console.log("encrypt" + encryptSendData);
    this.callService({"request" : encryptSendData});       
  }

  callService(senddata){
    this.presentLoadingDefault();         
    
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    console.log("header: " + JSON.stringify(headerString));
    this.appService.callService("Authenticate.svc/ChangePassword",senddata,headerString)
    .subscribe(ChangePassword =>{
     this.loading.dismiss();
     console.log("PreLoginData :" + JSON.stringify(ChangePassword));

     if(ChangePassword.ChangePasswordResult.ReturnCode == "0"){
      sessionStorage.TokenId = ChangePassword.ChangePasswordResult.UserToken.TokenId;
      this.serviceResponsePopup = false;
      this.message = "Your password has been changed successfully.";
      this.serviceCodeStatus = 0;
      //this.showToast("Password changed successfully.");        
     }else if(ChangePassword.ChangePasswordResult.ReturnCode == "807"){
      sessionStorage.TokenId = ChangePassword.ChangePasswordResult.UserToken.TokenId;
      this.serviceCodeStatus = 807;
     }else if(ChangePassword.ChangePasswordResult.ReturnCode == "500"){
      sessionStorage.TokenId = ChangePassword.ChangePasswordResult.UserToken.TokenId;
      this.serviceCodeStatus = 500;
     }else if(ChangePassword.ChangePasswordResult.ReturnCode == "501"){
      sessionStorage.TokenId = ChangePassword.ChangePasswordResult.UserToken.TokenId;
      //this.message = ChangePassword.ChangePasswordResult.ReturnMsg;
      this.message = "Password cannot be the same as previous password.";
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 501;
      this.newpassword = "";
      this.confirmpassword = "";
     }
        
    });
  }

    /* toggle Password input  */
    showpassword(){
      this.showPass = !this.showPass;
      if(this.showPass){
        this.picToView="assets/imgs/password_icon-02.svg";
        this.passwordType = 'text';
      } else {
        this.picToView="assets/imgs/password_icon-01.svg";
        this.passwordType = 'password';
      }
    }

    showReEnterpassword(){
      
      this.showreEnterPassword = !this.showreEnterPassword;
      if(this.showreEnterPassword){
        this.reEnterPicToView="assets/imgs/password_icon-02.svg";
        this.reEnterPasswordType = 'text';
      } else {
        this.reEnterPicToView="assets/imgs/password_icon-01.svg";
        this.reEnterPasswordType = 'password';
      }
    }

    serviceResponseOkButton(){
      if(this.serviceCodeStatus == 501){
        this.serviceResponsePopup = true;
      }else if(this.serviceCodeStatus == 807){
        this.serviceResponsePopup = true;
      }else if(this.serviceCodeStatus == 0){
        this.serviceResponsePopup = true;
        this.navCtrl.push(LoginPage);
      }
  
    }

    

}
