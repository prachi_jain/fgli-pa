import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { QuickqoutePage } from '../quickqoute/quickqoute';
import { ProductlistingPage } from '../productlisting/productlisting';
import { QuickqoutePage } from '../personalAccident/quickqoute/quickqoute';
import { PremiumBuyPage } from '../personalAccident/premium-buy/premium-buy';

/**
 * Generated class for the ProductLandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-landing',
  templateUrl: 'product-landing.html',
})
export class ProductLandingPage {

  page;
  //divHideShow;
  policyStatus;
  loginStatus;
  showButton = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.page = navParams.get("page");
    console.log("pagName" + this.page);
    this.policyStatus = navParams.get("policyStatus");
    this.loginStatus = navParams.get("loginStatus");
    console.log("policy: " + this.policyStatus);
    console.log("login: " + this.loginStatus);    
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductLandingPage');
        setTimeout(function() {         
          document.getElementById("splash").style.display='none';                 
      }, 7000);
      console.log("load" + sessionStorage.TokenId);
      if(sessionStorage.pagename == "Login"){
        this.showButton = false;
      }

      if(this.policyStatus == undefined){
        this.policyStatus = "firsttime";
      }

      // if(sessionStorage.pagename == "Login"){
      //   this.divHideShow = true;
      // }else{
      //   this.divHideShow = false;
      // }
  }

  goToProductListing(){
    this.navCtrl.push(ProductlistingPage, {"page" : this.page, policyStatus: this.policyStatus, loginStatus: this.loginStatus});
  }

  goToPersonalAccdient(){
    //this.navCtrl.push(QuickqoutePage, {"page" : this.page, policyStatus: this.policyStatus, loginStatus: this.loginStatus})
    if(sessionStorage.pagename == "Login" && this.policyStatus == "online"){
      this.navCtrl.push(PremiumBuyPage);
    }else if(sessionStorage.pagename == "Login"  && this.policyStatus == "firsttime"){
      this.navCtrl.push(PremiumBuyPage);
    }else{
      this.navCtrl.push(QuickqoutePage);
    }  

  }

  // navigateToNextPage(){
  //   this.navCtrl.push(QuickqoutePage);
  // }

}
