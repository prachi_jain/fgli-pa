import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductLandingPage } from './product-landing';

@NgModule({
  declarations: [
    //ProductLandingPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductLandingPage),
  ],
})
export class ProductLandingPageModule {}
