import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AppService } from '../../providers/app-service/app-service';
import { LoginPage } from '../health/login/login';

/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {

  newpassword;
  confirmpassword;
  loading;
  canShowToast = true;
  userName;
  serviceResponsePopup = true;
  passwordType = 'password';
  reEnterPasswordType = 'password';
  showPass = false;
  showreEnterPassword = false;
  message;
  picToView:string="assets/imgs/password_icon-01.svg";
  reEnterPicToView:string ="assets/imgs/password_icon-01.svg";
  strongRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})";
  serviceCodeStatus;

  constructor(public navCtrl: NavController, public toast: ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, public appService: AppService) {
    this.userName = navParams.get("userName");
  }

  ionViewDidLoad() {  
    console.log('ionViewDidLoad ForgotPasswdPage');
    setTimeout(function() {         
      document.getElementById("splash").style.display='none';                 
  }, 7000);
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

showToast(Message){
  if(this.canShowToast){
    let pageToast = this.toast.create({
      message:Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 5000,
    });
    this.canShowToast = false;
    pageToast.present();
    pageToast.onDidDismiss(() => {
      this.canShowToast = true;
    });
  }   
}


  serviceCall(){
    if(this.newpassword.trim() == undefined || this.newpassword.trim() == null || this.newpassword.trim() == ""){
      this.showToast("Please enter your new password.");
    }else if(this.newpassword.length < 6){
      this.showToast("Password should be minimum 6 characters.");
    }else if(this.newpassword.length > 24){

    }
    // else if(!this.checkPasswordStrength(this.newpassword)){
    //   this.showToast("Please enter one capital, one small alphabet and one numeric character.")
    // }
    else if(this.confirmpassword.trim() == undefined || this.confirmpassword.trim() == null || this.confirmpassword.trim() == ""){
      this.showToast("Please confirm your new password.")
    }else if(this.newpassword.trim() != this.confirmpassword.trim()){
      this.showToast("Your password do not match.")
    }else{
      this.changePasswordServiceCall();
    }
  }

  checkPasswordStrength(password){
    var strongRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;    
    return strongRegex.test(password);
  }


  changePasswordServiceCall(){
    var sendData = {
      'username' : this.userName,
      'password' : this.newpassword
    }

    console.log("sendData: " + JSON.stringify(sendData));
    var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),'');
    console.log("encrypt" + encryptSendData);
    this.callService({"request" : encryptSendData});       
  }

  callService(senddata){
    this.presentLoadingDefault();         
    
    //let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    //console.log("header: " + JSON.stringify(headerString));
    this.appService.callService("Authenticate.svc/ForgotPassword",senddata,'')
    .subscribe(ForgotPassword =>{
     this.loading.dismiss();
     console.log("PreLoginData :" + JSON.stringify(ForgotPassword));

     if(ForgotPassword.ForgotPasswordResult.ReturnCode == "0"){
      //sessionStorage.TokenId = ForgotPassword.ForgotPasswordResult.UserToken.TokenId;
      //this.showToast("password change successfully.");
      this.serviceCodeStatus = 0;
      this.serviceResponsePopup = false;
      this.message = "Your password has been changed successfully.";
      //this.navCtrl.push(LoginPage);
     }else if(ForgotPassword.ForgotPasswordResult.ReturnCode == "807"){
      this.serviceCodeStatus = 807;
      //sessionStorage.TokenId = ForgotPassword.ForgotPasswordResult.UserToken.TokenId;
     }else if(ForgotPassword.ForgotPasswordResult.ReturnCode == "500"){
      //sessionStorage.TokenId = ForgotPassword.ForgotPasswordResult.UserToken.TokenId;
      this.serviceCodeStatus = 500;
     }else if(ForgotPassword.ForgotPasswordResult.ReturnCode == "501"){    
      //this.message = ForgotPassword.ForgotPasswordResult.ReturnMsg;   
      this.message = "Password cannot be the same as previous password.";
      this.serviceResponsePopup = false;
      this.serviceCodeStatus = 501;
      this.newpassword = "";
      this.confirmpassword = "";
     }
        
    });
  }

   /* toggle Password input  */
   showpassword(){
    this.showPass = !this.showPass;
    if(this.showPass){
      this.picToView="assets/imgs/password_icon-02.svg";
      this.passwordType = 'text';
    } else {
      this.picToView="assets/imgs/password_icon-01.svg";
      this.passwordType = 'password';
    }
  }

  showReEnterpassword(){
    
    this.showreEnterPassword = !this.showreEnterPassword;
    if(this.showreEnterPassword){
      this.reEnterPicToView="assets/imgs/password_icon-02.svg";
      this.reEnterPasswordType = 'text';
    } else {
      this.reEnterPicToView="assets/imgs/password_icon-01.svg";
      this.reEnterPasswordType = 'password';
    }
  }

  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 501){
      this.serviceResponsePopup = true;
    }else if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
    }else if(this.serviceCodeStatus == 0){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);
    }
  }


}
