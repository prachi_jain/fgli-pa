import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AppService } from '../../providers/app-service/app-service';
import { OtpPasswdPage } from '../otp-passwd/otp-passwd';

/**
 * Generated class for the ViewProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-profile',
  templateUrl: 'view-profile.html',
})
export class ViewProfilePage {

  canShowToast = true;
  loading;
  userName;
  imdCode;
  mobile;
  email;
  branchAddress;
  panNumber;
  imdName;

  constructor(public navCtrl: NavController, public toast: ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, public appService: AppService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewProfilePage');
    setTimeout(function() {         
      document.getElementById("splash").style.display='none';                 
  }, 7000);

  this.getProfileValue();
  }

  getProfileValue(){
    var sendData = {
      'username' : sessionStorage.username      
    }

    console.log("sendData: " + JSON.stringify(sendData));
    var encryptSendData = this.appService.encryptData(JSON.stringify(sendData),sessionStorage.TokenId);
    console.log("encrypt" + encryptSendData);
    this.callViewProfileService({"request" : encryptSendData});  
  }

  callViewProfileService(sendData){
    this.presentLoadingDefault();         
    
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    console.log("header: " + JSON.stringify(headerString));
    this.appService.callService("Dashboard.svc/GetUserProfile",sendData,headerString)
    .subscribe(ViewProfile =>{
     this.loading.dismiss();
     console.log("PreLoginData :" + JSON.stringify(ViewProfile));

     if(ViewProfile.GetUserProfileResult.ReturnCode == "0"){
      sessionStorage.TokenId = ViewProfile.GetUserProfileResult.UserToken.TokenId;
      //this.navCtrl.push(ViewProfilePage);
      this.userName = ViewProfile.GetUserProfileResult.Data.Username;
      this.imdCode = ViewProfile.GetUserProfileResult.Data.AgentCode;
      this.mobile = ViewProfile.GetUserProfileResult.Data.Mobile;
      this.email = ViewProfile.GetUserProfileResult.Data.Email;
      this.branchAddress = ViewProfile.GetUserProfileResult.Data.BranchAddr;
      this.panNumber = ViewProfile.GetUserProfileResult.Data.PanNumber;
      this.imdName = ViewProfile.GetUserProfileResult.Data.FullName;
     }else if(ViewProfile.GetUserProfileResult.ReturnCode == "807"){
      sessionStorage.TokenId = ViewProfile.GetUserProfileResult.UserToken.TokenId;
     }else if(ViewProfile.GetUserProfileResult.ReturnCode == "500"){
      sessionStorage.TokenId = ViewProfile.GetUserProfileResult.UserToken.TokenId;
     }
        
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

showToast(Message){
  if(this.canShowToast){
    let pageToast = this.toast.create({
      message:Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 5000,
    });
    this.canShowToast = false;
    pageToast.present();
    pageToast.onDidDismiss(() => {
      this.canShowToast = true;
    });
  }   
}

  changePassword(){
    this.navCtrl.push(OtpPasswdPage);
  }

}
