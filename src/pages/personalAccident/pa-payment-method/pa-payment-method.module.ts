import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaPaymentMethodPage } from './pa-payment-method';

@NgModule({
  declarations: [
  //  PaPaymentMethodPage,
  ],
  imports: [
    IonicPageModule.forChild(PaPaymentMethodPage),
  ],
})
export class PaPaymentMethodPageModule {}
