import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PremiumlistviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-premiumlistview',
  templateUrl: 'premiumlistview.html',
})
export class PremiumlistviewPage {

   premPet = 'summary';
   pet ='OneTime';  
   showPopup = true;
   showTransBaground = true;

   oneTimeOneNew;
   oneTimeTwoNew;
   oneTimeThreeNew;
   oneTimeOneOld;
   oneTimeTwoOld;
   oneTimeThreeOld;
   discountOneTimeOne;
   discountOneTimeTwo;
   discountOneTimeThree;
   totalCalculateAmount =  953.10;
   policyType;
   discountAmount;
   ActualAmountoneTimeTwoNew;
   ActualAmountoneTimeThreeNew;



   monthlyTwoNew;
   monthlyTwoOld;
   discountmonthlyTwo;
   monthlyThreeNew;
   monthlyThreeOld;
   discountmonthlyThree;

   quarterlyTwoNew;
   quarterlyTwoOld;
   discountquarterlyTwo;
   quarterlyThreeNew;
   quarterlyThreeOld;
   discountquarterlyThree;

   halfyearlyTwoNew;
   halfyearlyTwoOld;
   discounthalfyearlyTwo;
   halfyearlyThreeNew;
   halfyearlyThreeOld;
   discounthalfyearlyThree;
   

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.totalCalculateAmount = navParams.get("totalPremium");
    console.log("total" + this.totalCalculateAmount);
    this.policyType = navParams.get("PolicyType");
    
  }



  ionViewDidLoad() {

    console.log('ionViewDidLoad PremiumlistviewPage');
                setTimeout(function() {         
          document.getElementById("splash").style.display='none';                 
      }, 7000);

      this.getOneTimeCaculation();

  }

  getOneTimeCaculation(){
    if(this.policyType == "PATI"){
      //this.oneTimeOneNew = (((this.totalCalculateAmount - (this.totalCalculateAmount))*18)/100).toFixed();
      this.oneTimeOneNew = this.addCommas((Number(this.totalCalculateAmount) + Number(this.totalCalculateAmount*18/100)).toFixed());

      let for2Year = this.totalCalculateAmount * 2;
      let gstfor2Year = Number((for2Year*18)/100).toFixed()
      this.ActualAmountoneTimeTwoNew = (Number(for2Year) + Number(gstfor2Year)).toFixed()

      let withDiscount = for2Year/20;
      let withoutGSt = for2Year - withDiscount;
      this.oneTimeTwoNew = this.addCommas((withoutGSt + (withoutGSt * 18)/100).toFixed());


      let for3Year = this.totalCalculateAmount * 3;
      let gstfor3Year = Number((for3Year*18)/100).toFixed()
      this.ActualAmountoneTimeThreeNew = (Number(for3Year) + Number(gstfor3Year)).toFixed()

      let withThreeDiscount = for3Year/10;
      let withouThreetGSt = for3Year - withThreeDiscount;
      this.oneTimeThreeNew = this.addCommas((withouThreetGSt + (withouThreetGSt * 18)/100).toFixed());


    }else{
      let familyDisc = this.totalCalculateAmount - (this.totalCalculateAmount/10);
      let withGST = familyDisc + (familyDisc * 18)/100;
      this.discountAmount = this.totalCalculateAmount -  (this.totalCalculateAmount * 10)/100;
      this.oneTimeOneNew = this.addCommas(withGST.toFixed());


      let for2Year = familyDisc * 2;
      let gstfor2Year = Number((for2Year*18)/100).toFixed()
      this.ActualAmountoneTimeTwoNew = (Number(for2Year) + Number(gstfor2Year)).toFixed()


      let longTermTwoDisc = (familyDisc * 2) - (((familyDisc * 2) * 5)/100);      
      this.oneTimeTwoNew = this.addCommas((Number(longTermTwoDisc) + ((longTermTwoDisc * 18)/100)).toFixed());

      let for3Year = familyDisc * 3;
      let gstfor3Year = Number((for3Year*18)/100).toFixed()
      this.ActualAmountoneTimeThreeNew = (Number(for3Year) + Number(gstfor3Year)).toFixed()
     
      
      let longTermThreeDisc = (familyDisc * 3) - (((familyDisc * 3) * 10)/100);
      this.oneTimeThreeNew = this.addCommas((Number(longTermThreeDisc) + ((longTermThreeDisc * 18)/100)).toFixed());

      
    }



  }

  getMonthlyCaculation(){
    console.log("Monthly");
    if(this.policyType == "PATI"){
      // this.monthlyTwoNew = ((((this.totalCalculateAmount * 2) + (((this.totalCalculateAmount * 2) * 5)/100))/24) + (((((this.totalCalculateAmount * 2) + (((this.totalCalculateAmount * 2) * 5)/100))/24) * 18)/100)).toFixed();
      // this.monthlyThreeNew = ((((this.totalCalculateAmount * 3) + (((this.totalCalculateAmount * 3) * 5)/100))/36) + (((((this.totalCalculateAmount * 3) + (((this.totalCalculateAmount * 3) * 5)/100))/36) * 18)/100)).toFixed();      



      let policyTermTwo = Number(this.totalCalculateAmount) * 2;
      let withLongTermTwoDiscMonthly = Number(policyTermTwo);    
      let withLoadingMonthlyTwo =  (Number(withLongTermTwoDiscMonthly) + (Number(withLongTermTwoDiscMonthly) * .05))/24;
      this.monthlyTwoNew = this.addCommas((Number(withLoadingMonthlyTwo) + (Number(withLoadingMonthlyTwo) * 0.18)).toFixed());



      let policyTermThree = Number(this.totalCalculateAmount) * 3;
      let withLongTermThreeDiscMonthly = Number(policyTermThree);     
      let withLoadingMonthlyThree =  (Number(withLongTermThreeDiscMonthly) + (Number(withLongTermThreeDiscMonthly) * .05))/36;
      this.monthlyThreeNew = this.addCommas((Number(withLoadingMonthlyThree) + (Number(withLoadingMonthlyThree) * 0.18)).toFixed());


      // let policyTermTwo = Number(this.totalCalculateAmount) * 2;
      // let withLongTermTwoDiscMonthly = Number(policyTermTwo) - (Number(policyTermTwo) * .05);      
      // let withLoadingMonthlyTwo =  (Number(withLongTermTwoDiscMonthly) + (Number(withLongTermTwoDiscMonthly) * .05))/24;
      // this.monthlyTwoNew = this.addCommas((Number(withLoadingMonthlyTwo) + (Number(withLoadingMonthlyTwo) * 0.18)).toFixed());



      // let policyTermThree = Number(this.totalCalculateAmount) * 3;
      // let withLongTermThreeDiscMonthly = Number(policyTermThree) - (Number(policyTermThree) * 0.10);      
      // let withLoadingMonthlyThree =  (Number(withLongTermThreeDiscMonthly) + (Number(withLongTermThreeDiscMonthly) * .05))/36;
      // this.monthlyThreeNew = this.addCommas((Number(withLoadingMonthlyThree) + (Number(withLoadingMonthlyThree) * 0.18)).toFixed());



    }else{

      this.discountAmount = this.totalCalculateAmount -  (this.totalCalculateAmount * 0.10); 
      let policyTermTwo = Number(this.discountAmount) * 2;
      let withLongTermTwoDiscMonthly = Number(policyTermTwo); 
      let withLoadingMonthlyTwo =  (Number(withLongTermTwoDiscMonthly) + (Number(withLongTermTwoDiscMonthly) * .05))/24;
      this.monthlyTwoNew = this.addCommas((Number(withLoadingMonthlyTwo) + (Number(withLoadingMonthlyTwo) * 0.18)).toFixed());

      let policyTermThree = Number(this.discountAmount) * 3;
      let withLongTermThreeDiscMonthly = Number(policyTermThree);  
      let withLoadingMonthlyThree =  (Number(withLongTermThreeDiscMonthly) + (Number(withLongTermThreeDiscMonthly) * .05))/36;
      this.monthlyThreeNew = this.addCommas((Number(withLoadingMonthlyThree) + (Number(withLoadingMonthlyThree) * 0.18)).toFixed());
      
      // this.discountAmount = this.totalCalculateAmount -  (this.totalCalculateAmount * 0.10); 
      // let policyTermTwo = Number(this.discountAmount) * 2;
      // let withLongTermTwoDiscMonthly = Number(policyTermTwo) - (Number(policyTermTwo) * .05);      
      // let withLoadingMonthlyTwo =  (Number(withLongTermTwoDiscMonthly) + (Number(withLongTermTwoDiscMonthly) * .05))/24;
      // this.monthlyTwoNew = this.addCommas((Number(withLoadingMonthlyTwo) + (Number(withLoadingMonthlyTwo) * 0.18)).toFixed());

      // let policyTermThree = Number(this.discountAmount) * 3;
      // let withLongTermThreeDiscMonthly = Number(policyTermThree) - (Number(policyTermThree) * 0.10);      
      // let withLoadingMonthlyThree =  (Number(withLongTermThreeDiscMonthly) + (Number(withLongTermThreeDiscMonthly) * .05))/36;
      // this.monthlyThreeNew = this.addCommas((Number(withLoadingMonthlyThree) + (Number(withLoadingMonthlyThree) * 0.18)).toFixed());
      
      //this.monthlyThreeNew = ((((this.discountAmount * 3) + (((this.discountAmount * 3) * 5)/100))/36) + (((((this.discountAmount * 3) + (((this.discountAmount * 3) * 5)/100))/36) * 18)/100)).toFixed();      
    }
  }


  getQuarterlyCaculation(){
    console.log("Quarter");
    
    if(this.policyType == "PATI"){
      // this.quarterlyTwoNew = ((((this.totalCalculateAmount * 2) + (((this.totalCalculateAmount * 2) * 4)/100))/8) + (((((this.totalCalculateAmount * 2) + (((this.totalCalculateAmount * 2) * 5)/100))/8) * 18)/100)).toFixed();
      // this.quarterlyThreeNew = ((((this.totalCalculateAmount * 3) + (((this.totalCalculateAmount * 3) * 4)/100))/12) + (((((this.totalCalculateAmount * 3) + (((this.totalCalculateAmount * 3) * 5)/100))/12) * 18)/100)).toFixed();      

      // let policyTermTwo = Number(this.totalCalculateAmount) * 2;
      // let withLongTermTwoDiscQuarterly = Number(policyTermTwo) - (Number(policyTermTwo) * .05);      
      // let withLoadingQuarterlyTwo =  (Number(withLongTermTwoDiscQuarterly) + (Number(withLongTermTwoDiscQuarterly) * 0.04))/24;
      // this.quarterlyTwoNew = this.addCommas((Number(withLoadingQuarterlyTwo) + (Number(withLoadingQuarterlyTwo) * 0.18)).toFixed());

      // let policyTermThree = Number(this.totalCalculateAmount) * 3;
      // let withLongTermThreeDiscQuarterly = Number(policyTermThree) - (Number(policyTermThree) * 0.10);      
      // let withLoadingQuarterlyThree =  (Number(withLongTermThreeDiscQuarterly) + (Number(withLongTermThreeDiscQuarterly) * .04))/36;
      // this.quarterlyThreeNew = this.addCommas((Number(withLoadingQuarterlyThree) + (Number(withLoadingQuarterlyThree) * 0.18)).toFixed());

      let policyTermTwo = Number(this.totalCalculateAmount) * 2;
      let withLongTermTwoDiscQuarterly = Number(policyTermTwo);      
      let withLoadingQuarterlyTwo =  (Number(withLongTermTwoDiscQuarterly) + (Number(withLongTermTwoDiscQuarterly) * 0.04))/8;
      this.quarterlyTwoNew = this.addCommas((Number(withLoadingQuarterlyTwo) + (Number(withLoadingQuarterlyTwo) * 0.18)).toFixed());

      let policyTermThree = Number(this.totalCalculateAmount) * 3;
      let withLongTermThreeDiscQuarterly = Number(policyTermThree);     
      let withLoadingQuarterlyThree =  (Number(withLongTermThreeDiscQuarterly) + (Number(withLongTermThreeDiscQuarterly) * .04))/12;
      this.quarterlyThreeNew = this.addCommas((Number(withLoadingQuarterlyThree) + (Number(withLoadingQuarterlyThree) * 0.18)).toFixed());



    }else{ 

      this.discountAmount = this.totalCalculateAmount -  (this.totalCalculateAmount * 0.10); 
      let policyTermTwo = Number(this.discountAmount) * 2;
      let withLongTermTwoDiscQuarterly = Number(policyTermTwo) 
      let withLoadingQuarterlyTwo =  (Number(withLongTermTwoDiscQuarterly) + (Number(withLongTermTwoDiscQuarterly) * 0.04))/8;
      this.quarterlyTwoNew = this.addCommas((Number(withLoadingQuarterlyTwo) + (Number(withLoadingQuarterlyTwo) * 0.18)).toFixed());

      let policyTermThree = Number(this.discountAmount) * 3;
      let withLongTermThreeDiscQuarterly = Number(policyTermThree)    
      let withLoadingQuarterlyThree =  (Number(withLongTermThreeDiscQuarterly) + (Number(withLongTermThreeDiscQuarterly) * .04))/12;
      this.quarterlyThreeNew = this.addCommas((Number(withLoadingQuarterlyThree) + (Number(withLoadingQuarterlyThree) * 0.18)).toFixed());
      
      // this.discountAmount = this.totalCalculateAmount -  (this.totalCalculateAmount * 0.10); 
      // let policyTermTwo = Number(this.discountAmount) * 2;
      // let withLongTermTwoDiscQuarterly = Number(policyTermTwo) - (Number(policyTermTwo) * .05);      
      // let withLoadingQuarterlyTwo =  (Number(withLongTermTwoDiscQuarterly) + (Number(withLongTermTwoDiscQuarterly) * 0.04))/24;
      // this.quarterlyTwoNew = this.addCommas((Number(withLoadingQuarterlyTwo) + (Number(withLoadingQuarterlyTwo) * 0.18)).toFixed());

      // let policyTermThree = Number(this.discountAmount) * 3;
      // let withLongTermThreeDiscQuarterly = Number(policyTermThree) - (Number(policyTermThree) * 0.10);      
      // let withLoadingQuarterlyThree =  (Number(withLongTermThreeDiscQuarterly) + (Number(withLongTermThreeDiscQuarterly) * .04))/36;
      // this.quarterlyThreeNew = this.addCommas((Number(withLoadingQuarterlyThree) + (Number(withLoadingQuarterlyThree) * 0.18)).toFixed());

      //this.quarterlyTwoNew = ((((this.discountAmount * 2) + (((this.discountAmount * 2) * 4)/100))/8) + (((((this.discountAmount * 2) + (((this.discountAmount * 2) * 5)/100))/8) * 18)/100)).toFixed();
      //this.quarterlyThreeNew = ((((this.discountAmount * 3) + (((this.discountAmount * 3) * 4)/100))/12) + (((((this.discountAmount * 3) + (((this.discountAmount * 3) * 5)/100))/12) * 18)/100)).toFixed();      
  }
}


getHalfYearlyCaculation(){
  console.log("HalfYearly");
  if(this.policyType == "PATI"){
    // this.halfyearlyTwoNew = ((((this.totalCalculateAmount * 2) + (((this.totalCalculateAmount * 2) * 3)/100))/4) + (((((this.totalCalculateAmount * 2) + (((this.totalCalculateAmount * 2) * 5)/100))/4) * 18)/100)).toFixed();
    // this.halfyearlyThreeNew = ((((this.totalCalculateAmount * 3) + (((this.totalCalculateAmount * 3) * 3)/100))/6) + (((((this.totalCalculateAmount * 3) + (((this.totalCalculateAmount * 3) * 5)/100))/6) * 18)/100)).toFixed();      

    // let policyTermTwo = Number(this.totalCalculateAmount) * 2;
    // let withLongTermTwoDiscHalfyearly = Number(policyTermTwo) - (Number(policyTermTwo) * .05);      
    // let withLoadingHalfyearlyTwo =  (Number(withLongTermTwoDiscHalfyearly) + (Number(withLongTermTwoDiscHalfyearly) * 0.03))/24;
    // this.halfyearlyTwoNew = this.addCommas((Number(withLoadingHalfyearlyTwo) + (Number(withLoadingHalfyearlyTwo) * 0.18)).toFixed());

    // let policyTermThree = Number(this.totalCalculateAmount) * 3;
    // let withLongTermThreeDiscHalfyearly = Number(policyTermThree) - (Number(policyTermThree) * 0.10);      
    // let withLoadingHalfyearlyThree =  (Number(withLongTermThreeDiscHalfyearly) + (Number(withLongTermThreeDiscHalfyearly) * .03))/36;
    // this.halfyearlyThreeNew = this.addCommas((Number(withLoadingHalfyearlyThree) + (Number(withLoadingHalfyearlyThree) * 0.18)).toFixed());

    let policyTermTwo = Number(this.totalCalculateAmount) * 2;
    let withLongTermTwoDiscHalfyearly = Number(policyTermTwo);      
    let withLoadingHalfyearlyTwo =  (Number(withLongTermTwoDiscHalfyearly) + (Number(withLongTermTwoDiscHalfyearly) * 0.03))/4;
    this.halfyearlyTwoNew = this.addCommas((Number(withLoadingHalfyearlyTwo) + (Number(withLoadingHalfyearlyTwo) * 0.18)).toFixed());

    let policyTermThree = Number(this.totalCalculateAmount) * 3;
    let withLongTermThreeDiscHalfyearly = Number(policyTermThree);      
    let withLoadingHalfyearlyThree =  (Number(withLongTermThreeDiscHalfyearly) + (Number(withLongTermThreeDiscHalfyearly) * .03))/6;
    this.halfyearlyThreeNew = this.addCommas((Number(withLoadingHalfyearlyThree) + (Number(withLoadingHalfyearlyThree) * 0.18)).toFixed());
  }else{
    this.discountAmount = this.totalCalculateAmount -  (this.totalCalculateAmount * 0.10); 
    let policyTermTwo = Number(this.discountAmount) * 2;
    let withLongTermTwoDiscHalfyearly = Number(policyTermTwo);  
    let withLoadingHalfyearlyTwo =  (Number(withLongTermTwoDiscHalfyearly) + (Number(withLongTermTwoDiscHalfyearly) * 0.03))/4;
    this.halfyearlyTwoNew = this.addCommas((Number(withLoadingHalfyearlyTwo) + (Number(withLoadingHalfyearlyTwo) * 0.18)).toFixed());

    let policyTermThree = Number(this.discountAmount) * 3;
    let withLongTermThreeDiscHalfyearly = Number(policyTermThree); 
    let withLoadingHalfyearlyThree =  (Number(withLongTermThreeDiscHalfyearly) + (Number(withLongTermThreeDiscHalfyearly) * .03))/6;
    this.halfyearlyThreeNew = this.addCommas((Number(withLoadingHalfyearlyThree) + (Number(withLoadingHalfyearlyThree) * 0.18)).toFixed());
}
}
addCommas(x) {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  console.log("test: " + parts);
  return parts.join(".");   
}
onBackClick() {
 
  this.navCtrl.pop();

}

  // premiumClick(){
  //   this.showPopup = false;
  //   this.showTransBaground = false;
  // }
  
  hidePopup(){
    this.showPopup = true;
    this.showTransBaground = true;
  }

}
