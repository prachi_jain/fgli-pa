import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaBuyPolicyReviewPage } from './pa-buy-policy-review';

@NgModule({
  declarations: [
  //  PaBuyPolicyReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(PaBuyPolicyReviewPage),
  ],
})
export class PaBuyPolicyReviewPageModule {}
