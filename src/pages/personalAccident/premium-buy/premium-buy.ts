import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, FabButton ,LoadingController} from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { DatePicker } from '@ionic-native/date-picker';
import * as $ from 'jquery';
import { BuypremiumcalciPage } from '../../personalAccident/buypremiumcalci/buypremiumcalci'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { AppService } from '../../../providers/app-service/app-service';
import { LoginPage } from '../../health/login/login';




/**
 * Generated class for the PremiumBuyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-premium-buy',
  templateUrl: 'premium-buy.html',
})
export class PremiumBuyPage {
  hideannualIncome = false
  showTransBaground = true;
  isActive = true;
  occupationLists: any = [];
  insuretype: any = "";
  childMartial = false;
  dob: any = "";
  category: any = "";
  annualincome: any = "";
  maritalstatus: any = "";



  

  @ViewChild('annaulIncomeFocus') mobileFocus;
  
  
  canShowToast = true;
  selfCard = true;
  spouseCard = true;
  child1Card = true;
  child2Card = true;

  selfMaritalstatus;
  selfDob;
  selfCategory;
  selfAnnualincome;
  spouseMaritalstatus;
  spouseDob;
  spouseCategory;
  spouseAnnualincome;
  child1Maritalstatus;
  child1Dob;
  child1Category;
  child1Annualincome;
  child2Maritalstatus;
  child2Dob;
  child2Category;
  child2Annualincome;
  selectedMemberTitle;

  appData;
  apiAgeGroup;
  occupation;
  policyType;
  marriedStatusArray = [{ value: "Single", status: false }, { value: "Married", status: true }, { value: "Divorced", status: false }, { value: "Widow / Widower", status: false }];
  marriedStatus = "";
  memberMarried = false;
  hideOccupation = false;
  lastActiveMember;
  occupationName;
  OccupationCode;
  childAgeGroup = [];
  AgeGroup = [];
  numberLength(Num) { if (Num >= 10) { return Num } else { return "0" + Num } }
  memberAge:any = "26-04-1985"
  memberAgeChild = 0;
  hideAge = false
  hideChildAge = true
  OccupationFocus = false;
  Occupationlistcopy = [];


  // gender structure



  isSelfGenderActive = true;
  isSpouseGenderActive = false;
  isSonGenderActive = true;
  isSon1GenderActive = true;

  selfGenderPopUp = false;
  spouseGenderPopUp = true;
  child1GenderPopUp = true;
  child2GenderPopUp = true;

selfGender = "Male";
selfGenderText = "M";
spouseGender = "Female";
spouseGenderText = "F";
child1Gender = "Son";
child1GenderText = "M";
child2Gender = "Son1";
child2GenderText = "M";

today = new Date();
apiMembersData;
apiMembers = [];
membersShow = [];
membersInsured = [];
beneficiaryDetails = [];
isChild1 = false;


myMember=[{"id":"SELF","imgPath":"assets/imgs/personal/selfIcon.png"},{"id":"SPOU","imgPath":"assets/imgs/personal/spouseIcon.png"},
{"id":"SON","imgPath":"assets/imgs/personal/childIcon.png"},{"id":"DAUG","imgPath":"assets/imgs/personal/childIcon.png"}]
numberLenght(Num){if(Num>=10){return Num}else{return"0"+Num}}
maxDate = (this.today.getFullYear()+"-"+this.numberLenght(this.today.getMonth())+"-"+this.numberLenght(this.today.getDate())).toString();
minDate = '1900-01-01';
indexPos;
editUserData = false;
loading;
ENQ_PolicyResponse;
ReferenceNo
isSpouse = false;
  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, private keyboard: Keyboard, private datePicker: DatePicker, public toast: ToastController, private cdr: ChangeDetectorRef, private alertCtrl: AlertController,public appService: AppService) {
    // console.log("hello"+JSON.parse(localStorage.AppData))
    this.appData = JSON.parse(localStorage.AppData);
    this.apiAgeGroup = this.appData.Masters.Age;
    this.occupationLists = this.appData.Masters.PA_Occupation;
    this.apiMembers = this.appData.PA_MembersLst
    
    if(this.apiMembers != undefined){
      console.log("TotalMembers: " + JSON.stringify(this.apiMembers));
      
    }

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickqoutePage');
    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 7000);
   this.mobileFocus.setFocus();
    this.membersArrayFunction();

    this.childAgeRange();
  }

  
  membersArrayFunction(){
    for(let a = 0; a <this.apiMembers.length; a++){
      let term = this.myMember.filter(person => person.id == this.apiMembers[a].code);
      var memberDetails = {};
      memberDetails["code"] =this.apiMembers[a].code;
      memberDetails["title"] =this.apiMembers[a].title;
      memberDetails["popupType"] = "1";
      memberDetails["showMarried"] = false;
      memberDetails["maritalStatus"] = "";
      memberDetails["maritalCode"] = "S";
      memberDetails["age"] = "";
      memberDetails["ageText"] = "";
      memberDetails["Gender"] = "Male";
      memberDetails["GenderText"] = "M";
      memberDetails["occupationCategory"] = "";
      memberDetails["occupationName"] =  "";
      memberDetails["occupationCode"] =  "";
      memberDetails["annualIncome"] = "";
      memberDetails["showFamilyMemberOnScreen"] = true;
      memberDetails["disableRemove"] = false;
      memberDetails["ShowInsurenceAmt"] = false;
      memberDetails["ProposerDetailsDivShow"] = false;
      memberDetails["medicalDeclaration"] = false;
      memberDetails["showMedicalQuestions"] = true;
      memberDetails["nominee"] = '';
      memberDetails["NomineeRelation"] = '';
      memberDetails["NomineeAge"] = '';
      memberDetails["appointeeMember"] = '';
      memberDetails["AptRelWithominee"] = '';
      memberDetails["memberSelectedOnAddmember"] = false;
      memberDetails["detailsSaved"] = false;
      memberDetails["dependent"] = false;
      memberDetails["showAnnualIncome"] = true
      if(term.length!=0){
        memberDetails["imgUrl"] =term[0].imgPath;
        if(term[0].id == "CHILD1" || term[0].id == "CHILD2"){
          memberDetails["popupType"] = "2";
          memberDetails["showAddAnotherBtn"] = true;
        }else if(term[0].id == "SELF"){
          memberDetails["showMarried"] = true;
          memberDetails["disableRemove"] = true;
        }else if(term[0].id == "SPOU"){
          memberDetails["disableRemove"] = true;
        }
      }
      if(this.apiMembers[a].title.toLowerCase().indexOf("child1")>=0){
        memberDetails["imgUrl"] ="assets/imgs/personal/childIcon.png";
      }else if(this.apiMembers[a].title.toLowerCase().indexOf("child2")>=0){
        memberDetails["imgUrl"] ="assets/imgs/personal/childIcon.png";
      }else if(term.length==0){
        memberDetails["imgUrl"] ="assets/imgs/personal/selfIcon.png";
      }
    
       memberDetails["detailsSaved"] =false;


      this.membersShow.push(memberDetails);
    }
  }
  childAgeRange(){
    for(let i = 0; i <this.apiAgeGroup.length; i++){
        if(this.apiAgeGroup[i].min >  25){
          break;
        }
        this.childAgeGroup.push(this.apiAgeGroup[i]);
    }
  }
  invalidDob(userDoB){
    let today = new Date();
    let uDob = userDoB.split("-");
    let uDte = ( this.numberLenght(Number(uDob[1]))+"-"+this.numberLenght( Number(uDob[0]))+"-"+(uDob[2]) ).toString();
    let usrDob = new Date(uDte);    
    if(usrDob.toString() == "Invalid Date"){
      return true;
    }else if(usrDob > today){
      return true;
    }
    return false;
  }
  showToast(Message) {
    if (this.canShowToast) {
      let pageToast = this.toast.create({
        message: Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }
  }
 fillData(memberData,i){

    this.editUserData  =  false
    this.indexPos = i;
    this.memberAge = '';
    this.memberMarried = false;
    this.showTransBaground = false;  
    $(".qCardPopup").attr("hidden", false);
    this.selectedMemberTitle = this.membersShow[i].title;
   
    this.occupationName = "";
    this.annualincome = "";
    this.marriedStatus = "";
    


    if(this.selectedMemberTitle == "Self"){
    this.childMartial = false;
    this.memberMarried = true;
    this.hideOccupation = false
    this.hideannualIncome = false

    this.selfGenderPopUp = false
    this.spouseGenderPopUp = true
    this.child1GenderPopUp = true
    this.child2GenderPopUp = true
    if(this.membersShow[i].detailsSaved == true){
      this.memberAge = this.membersShow[i].age;
      this.category = this.membersShow[i].occupationCategory;
      this.maritalstatus = this.membersShow[i].maritalStatus;
      this.marriedStatus = this.membersShow[i].maritalStatus;
      this.annualincome = this.membersShow[i].annualIncome;
      this.occupationName = this.membersShow[i].occupationName;
     }
    }

    
    else if (this.selectedMemberTitle == "Spouse") {

    this.OccupationFocus = false;
    this.selfGenderPopUp = true
    this.spouseGenderPopUp = false
    this.child1GenderPopUp = true
    this.child2GenderPopUp = true
    

    this.hideannualIncome = false
    this.hideOccupation = false
    this.childMartial = true;

    if(this.membersShow[i].detailsSaved == true){
      this.memberAge = this.membersShow[i].age;
      this.category = this.membersShow[i].occupationCategory;
      this.maritalstatus = this.membersShow[i].maritalStatus;
      this.marriedStatus = this.membersShow[i].maritalStatus;
      this.annualincome = this.membersShow[i].annualIncome;
      this.occupationName = this.membersShow[i].occupationName;
     }else if (this.membersShow[0].annualIncome === ""){
      this.showTransBaground = true;
      this.showToast('Please fill in self details to move ahead')
      return
    } else {

     if (this.membersShow[0].maritalStatus == "Single") {
        this.showTransBaground = true;
        $(".qCardPopup").attr("hidden", true);
        this.showToast("As your marital status is Single, you cannot fill spouse details.");
        return

      }
      else if (this.membersShow[0].maritalStatus == "Divorced") {
        this.showTransBaground = true;
        $(".qCardPopup").attr("hidden", true);
        this.showToast("As your marital status is Divorced, you cannot fill spouse details.");
        return

      }
      else if (this.membersShow[0].maritalStatus == "Widow / Widower") {
        this.showTransBaground = true;
        $(".qCardPopup").attr("hidden", true);
        this.showToast("As your marital status is Widow / Widower, you cannot fill spouse details.");
        return

      }
     
    }
    }
    else if (this.selectedMemberTitle == "Child1") {
    this.membersShow[i].dependent = false;
    this.selfGenderPopUp = true
    this.spouseGenderPopUp = true
    this.child1GenderPopUp = false
    this.child2GenderPopUp = true
    this.hideannualIncome = true
    this.hideOccupation = true
    this.childMartial = true;

    if (this.membersShow[0].annualIncome === "") {
      $(".qCardPopup").attr("hidden", true);
      this.showTransBaground = true;
      this.showToast('Please fill in self details to move ahead')

      return
    } 
    else{

      if(this.membersShow[i].detailsSaved == true){
        this.memberAge = this.membersShow[i].age;
       }
    }
  } 
    else if (this.selectedMemberTitle == "Child2") {
     

      for(let i = 0 ; i < this.membersInsured.length ; i++){
        if(this.membersInsured[i].title == "Child1"){
          this.isChild1 = true
        }
      }

      if(this.isChild1 == false){

        this.showTransBaground = true;
        this.showToast('Please fill in Child1 details to move ahead')
  
      }
       
     
    this.membersShow[i].dependent = false;
    this.selfGenderPopUp = true
    this.spouseGenderPopUp = true
    this.child1GenderPopUp = true
    this.child2GenderPopUp = false
    this.hideannualIncome = true
    this.hideOccupation = true
    this.childMartial = true;

    if (this.membersShow[0].annualIncome === "") {
      $(".qCardPopup").attr("hidden", true);
      this.showTransBaground = true;
      this.showToast('Please fill in self details to move ahead')

      return
    } else{

      if(this.membersShow[i].detailsSaved == true){
        this.memberAge = this.membersShow[i].age;
       }
    }
  }
      

 }


  hidePopup() {
    this.showTransBaground = true;
    this.memberMarried = false;
    this.OccupationFocus = false;
    $(".qCardPopup").attr("hidden", true);
  }

  genderClick(selectedMemberTitle, gender) {
    
    console.log(selectedMemberTitle, gender);


    if (selectedMemberTitle == "Self") {

      if (gender == "Male") {

        this.isSelfGenderActive = true;
        this.selfGender = "Male"
        this.selfGenderText = "M"
        this.isSpouseGenderActive = false;

        this.isSpouseGenderActive = false;
        this.spouseGender = "Female"
        this.spouseGenderText = "F"


      }
      else {


        this.isSelfGenderActive = false;
        this.selfGender = "Female"
        this.selfGenderText = "F"
        this.isSpouseGenderActive = true;


        this.isSpouseGenderActive = true;
        this.spouseGender = "Male"
        this.spouseGenderText = "M"
      }
    }
    if (selectedMemberTitle == "Spouse") {

      if (gender == "Male") {

        this.isSpouseGenderActive = true;
        this.spouseGender = "Male"
        this.spouseGenderText = "M"
      }
      else {


        this.isSpouseGenderActive = false;
        this.spouseGender = "Female"
        this.spouseGenderText = "F"

      }
    }
    if (selectedMemberTitle == "Child1") {

      if (gender == "Son") {

        this.isSonGenderActive = true;
        this.child1Gender = "Son"
        this.child1GenderText = "M"
      }
      else {


        this.isSonGenderActive = false;
        this.child1Gender = "DAUG"
        this.child1GenderText = "F"
      }
    }
    if (selectedMemberTitle == "Child2") {

      if (gender == "Son") {


        this.isSon1GenderActive = true;


        if(this.child1Gender == "Son"){
          this.child2Gender = "Son1"
          this.child2GenderText = "M"
        }else{

        this.child2Gender = "Son"
        this.child2GenderText = "F"
        }
      }
      else {

        
        this.isSon1GenderActive = false;

        if(this.child1Gender == "DAUG"){

          this.child2Gender = "DAUG1"
          this.child2GenderText = "F"
        }else
        {
          this.child2Gender = "DAUG"
          this.child2GenderText = "F"
        }
       
      }
    }


    
  }

  
  onBackClick() {
    this.showTransBaground = true
    this.navCtrl.pop();

  }
  saveMemberDetails() {
    console.log("status: " + this.selfCard);



    if ((this.memberAge == 0 && this.selectedMemberTitle == "Self") || (this.memberAge == 0 && this.selectedMemberTitle == "Spouse")) {
      //show error messages
      this.showToast("Please select member's age.");
    } else if ((this.memberAge == 0 && this.selectedMemberTitle == "Child1") || (this.memberAge == 0 && this.selectedMemberTitle == "Child2")) {

      this.showToast("Please select member's age.");
    }
    else if ((this.category == "" && this.selectedMemberTitle == "Self") || (this.category == "" && this.selectedMemberTitle == "Spouse")) {
      this.showToast("Please select occupation.");
    } else if (this.marriedStatus == "" && this.selectedMemberTitle == "Self") {
      this.showToast("Please select marital status.");
    } else if (this.annualincome === "" && this.selectedMemberTitle == "Self" && (this.OccupationCode != "STDN" ||  this.OccupationCode != "UNEM" || this.OccupationCode != "HSWF" || this.OccupationCode == "STUD" || this.OccupationCode != "RETR")) {

      this.showToast("Please enter annual income.");
    }
    else if (this.annualincome === "" && this.selectedMemberTitle == "Spouse" &&  (this.OccupationCode != "STDN" ||  this.OccupationCode != "UNEM" || this.OccupationCode != "HSWF" || this.OccupationCode == "STUD"  || this.OccupationCode != "RETR")){

          this.showToast("Please enter annual income.");
    }
    else if(this.selectedMemberTitle == "Self" && this.selfGender == ""){

      this.showToast("Please select gender.");

    }
    else if(this.selectedMemberTitle == "Spouse" && this.spouseGender == ""){

      this.showToast("Please select gender.");

    }
    else if(this.selectedMemberTitle == "Child1" && this.child1Gender == ""){

      this.showToast("Please select gender.");

    }
    else if(this.selectedMemberTitle == "Child2" && this.child2Gender == ""){

      this.showToast("Please select gender.");

    }
     else if(Number(this.annualincome) < 30000 && this.selectedMemberTitle == "Self"  &&  (this.OccupationCode != "STDN" &&  this.OccupationCode != "UNEM" && this.OccupationCode != "HSWF" && this.OccupationCode != "STUD" && this.OccupationCode != "RETR")){
    

      this.showToast("You can not enter amount below 30000");

    }
    else if(Number(this.annualincome) < 30000 && this.selectedMemberTitle == "Spouse"  &&  (this.OccupationCode != "STDN"  &&   this.OccupationCode != "UNEM"  &&  this.OccupationCode != "HSWF"  &&  this.OccupationCode != "STUD" && this.OccupationCode != "RETR")){
    

      this.showToast("You can not enter amount below 30000");

    }
    else {
      //save details depending on type
      

      if(this.selectedMemberTitle == "Self"){
      //self stuff 
      this.membersShow[this.indexPos].age = this.memberAge;
      this.membersShow[this.indexPos].ageText = this.getAge(this.memberAge);  
      this.membersShow[this.indexPos].occupationCategory = this.category;
      this.membersShow[this.indexPos].occupationName = this.occupationName;
      this.membersShow[this.indexPos].occupationCode = this.OccupationCode;
      this.membersShow[this.indexPos].maritalStatus = this.marriedStatus;
      this.membersShow[this.indexPos].annualIncome = this.annualincome;
      this.membersShow[this.indexPos].Gender = this.selfGender;
      this.membersShow[this.indexPos].GenderText = this.selfGenderText;
      this.membersShow[this.indexPos].detailsSaved= true;
      this.membersShow[this.indexPos].dependent = true;
      this.membersShow[this.indexPos].ProposerDetailsDivShow = false
      this.membersShow[this.indexPos].showAnnualIncome = true
      this.membersShow[this.indexPos].showFamilyMemberOnScreen = true
      if(this.membersShow[this.indexPos].maritalStatus == "Married"){
        this.membersShow[this.indexPos].maritalCode = "M"

      }
      else if ((this.membersShow[this.indexPos].maritalStatus == "Single") || (this.membersShow[this.indexPos].maritalStatus == "Divorced") || (this.membersShow[this.indexPos].maritalStatus == "Widow / Widower")){

        this.membersShow[this.indexPos].maritalCode = "S"

        this.membersShow[this.indexPos+1].detailsSaved = false;
        this.membersShow[this.indexPos+1].ageText = "";
        this.membersShow[this.indexPos+1].maritalStatus = "";
        this.membersShow[this.indexPos+1].occupationName = "";
        this.membersShow[this.indexPos+1].annualincome = "";
        this.membersShow[this.indexPos].annualIncome = this.annualincome;
        this.membersShow[this.indexPos].showAnnualIncome = true


        if(this.membersInsured.length > 1  && this.membersInsured[1].code == "SPOU"){
          this.membersInsured.splice(1, 1);
          console.log("IFToday");                
        }


      }

      
      // if(this.OccupationCode == "STDN" || this.OccupationCode == "UNEM" || this.OccupationCode == "HSWF" ||  this.OccupationCode == "STUD" || this.OccupationCode == "RETR"){

      //   this.membersShow[this.indexPos].annualIncome = 0;
      //   this.membersShow[this.indexPos].showAnnualIncome = false

      // }
      // else{
      //   this.membersShow[this.indexPos].annualIncome = this.annualincome;
      //   this.membersShow[this.indexPos].showAnnualIncome = true

      // }
    
    }
    else if(this.selectedMemberTitle == "Spouse"){
      this.membersShow[this.indexPos].age = this.memberAge;
      this.membersShow[this.indexPos].ageText = this.getAge(this.memberAge);  
      this.membersShow[this.indexPos].occupationCategory = this.category;
      this.membersShow[this.indexPos].occupationName = this.occupationName;
      this.membersShow[this.indexPos].occupationCode = this.OccupationCode;
      this.membersShow[this.indexPos].maritalStatus = this.marriedStatus;


      if(this.OccupationCode == "STDN" || this.OccupationCode == "UNEM" || this.OccupationCode == "HSWF" ||  this.OccupationCode == "STUD" || this.OccupationCode == "RETR"){

        this.membersShow[this.indexPos].annualIncome = 0;
        this.membersShow[this.indexPos].showAnnualIncome = false

      }
      else{
        this.membersShow[this.indexPos].annualIncome = this.annualincome;
        this.membersShow[this.indexPos].showAnnualIncome = true

      }
      
      this.membersShow[this.indexPos].Gender = this.spouseGender;
      this.membersShow[this.indexPos].GenderText = this.spouseGenderText;
      this.membersShow[this.indexPos].detailsSaved= true;
      this.membersShow[this.indexPos].dependent = true;
      this.membersShow[this.indexPos].ProposerDetailsDivShow = true
    }
    else if(this.selectedMemberTitle == "Child1"){
     
      this.membersShow[this.indexPos].code = this.child1Gender
      this.membersShow[this.indexPos].age = this.memberAge;
      this.membersShow[this.indexPos].ageText = this.getAge(this.memberAge);  
      this.membersShow[this.indexPos].detailsSaved= true;
      this.membersShow[this.indexPos].annualIncome =  this.membersShow[0].annualIncome
      this.membersShow[this.indexPos].showAnnualIncome = false
      this.membersShow[this.indexPos].dependent = false;
      this.membersShow[this.indexPos].occupationCategory = "1";
      this.membersShow[this.indexPos].Gender = this.child1Gender;
      this.membersShow[this.indexPos].GenderText = this.child1GenderText;
      this.membersShow[this.indexPos].occupationName = "Student"
      this.membersShow[this.indexPos].occupationCode = "STDN"
      this.membersShow[this.indexPos].ProposerDetailsDivShow = true
    }
    else if(this.selectedMemberTitle == "Child2"){
      if(this.child2Gender == "DAUG1"){

        this.membersShow[this.indexPos].code = "DAUG"
      }
      else if(this.child2Gender == "Son1"){

        this.membersShow[this.indexPos].code = "Son"
      }
      else{

        this.membersShow[this.indexPos].code = this.child2Gender
      }
      this.membersShow[this.indexPos].age = this.memberAge;
      this.membersShow[this.indexPos].ageText = this.getAge(this.memberAge);  
      this.membersShow[this.indexPos].detailsSaved= true;
      this.membersShow[this.indexPos].annualIncome =  this.membersShow[0].annualIncome
      this.membersShow[this.indexPos].showAnnualIncome = false

      this.membersShow[this.indexPos].dependent = false;
      this.membersShow[this.indexPos].occupationCategory = "1";
      this.membersShow[this.indexPos].Gender = this.child2Gender;
      this.membersShow[this.indexPos].GenderText = this.child2GenderText;
      this.membersShow[this.indexPos].occupationName = "Student"
      this.membersShow[this.indexPos].occupationCode = "STDN"
      this.membersShow[this.indexPos].ProposerDetailsDivShow = true
    }


    this.hidePopup();
    this.memberAge = 0;
   
    if(this.editUserData == true){
      for(let i = 0 ; i < this.membersInsured.length ; i++){
        if(this.membersInsured[i].title == this.selectedMemberTitle){
          this.membersInsured.splice(i, 1, this.membersShow[this.indexPos]);
          this.editUserData = false
          break;
        }
      }
      console.log("this.membersInsured", this.membersInsured)

    }
    else{

      
      this.membersInsured.push(this.membersShow[this.indexPos]);
    }
  
  }

    
  
  }

  editMemberDetails($event, member,i){
 
    this.editUserData = true;
    event.stopPropagation();
    this.indexPos = i;
    this.memberAge = '';
    this.memberMarried = false;
    this.showTransBaground = false;  
    $(".qCardPopup").attr("hidden", false);
    this.selectedMemberTitle = this.membersShow[i].title;
   
    this.occupationName = "";
    this.annualincome = "";
    this.marriedStatus = "";
    


    if(this.selectedMemberTitle == "Self"){
    this.childMartial = false;
    this.memberMarried = true;
    this.hideOccupation = false
    this.hideannualIncome = false

    this.selfGenderPopUp = false
    this.spouseGenderPopUp = true
    this.child1GenderPopUp = true
    this.child2GenderPopUp = true
    if(this.membersShow[i].detailsSaved == true){
      this.memberAge = this.membersShow[i].age;
      this.category = this.membersShow[i].occupationCategory;
      this.maritalstatus = this.membersShow[i].maritalStatus;
      this.marriedStatus = this.membersShow[i].maritalStatus;
      this.annualincome = this.membersShow[i].annualIncome;
      this.occupationName = this.membersShow[i].occupationName;
      this.OccupationCode = this.membersShow[i].occupationCode;
 
     }
       this.membersShow[this.indexPos].annualIncome = this.annualincome;
       this.membersShow[this.indexPos].showAnnualIncome = true
       this.hideannualIncome = false
 

    //  if(this.OccupationCode == "STDN" || this.OccupationCode == "UNEM" || this.OccupationCode == "HSWF" ||  this.OccupationCode == "STUD" || this.OccupationCode == "RETR"){

    //   this.membersShow[this.indexPos].annualIncome = 0;
    //   this.membersShow[this.indexPos].showAnnualIncome = false
    //   this.hideannualIncome = true

    // }
    // else{
    //   this.membersShow[this.indexPos].annualIncome = this.annualincome;
    //   this.membersShow[this.indexPos].showAnnualIncome = true
    //   this.hideannualIncome = false

    // }
    }

    
    else if (this.selectedMemberTitle == "Spouse") {

    this.OccupationFocus = false;
    this.selfGenderPopUp = true
    this.spouseGenderPopUp = false
    this.child1GenderPopUp = true
    this.child2GenderPopUp = true
    

    this.hideannualIncome = false
    this.hideOccupation = false
    this.childMartial = true;

    if(this.membersShow[i].detailsSaved == true){
      this.memberAge = this.membersShow[i].age;
      this.category = this.membersShow[i].occupationCategory;
      this.maritalstatus = this.membersShow[i].maritalStatus;
      this.marriedStatus = this.membersShow[i].maritalStatus;
      this.annualincome = this.membersShow[i].annualIncome;
      this.occupationName = this.membersShow[i].occupationName;
      this.OccupationCode = this.membersShow[i].occupationCode;
     }else{
    if (this.membersShow[0].annualIncome === "") {
      this.showTransBaground = true;
      this.showToast('Please fill in self details to move ahead')
      return
    } else {

     if (this.membersShow[0].maritalStatus == "Single") {
        this.showTransBaground = true;
        $(".qCardPopup").attr("hidden", true);
        this.showToast("As your marital status is Single, you cannot fill spouse details.");
        return

      }
      else if (this.membersShow[0].maritalStatus == "Divorced") {
        this.showTransBaground = true;
        $(".qCardPopup").attr("hidden", true);
        this.showToast("As your marital status is Divorced, you cannot fill spouse details.");
        return

      }
      else if (this.membersShow[0].maritalStatus == "Widow / Widower") {
        this.showTransBaground = true;
        $(".qCardPopup").attr("hidden", true);
        this.showToast("As your marital status is Widow / Widower, you cannot fill spouse details.");
        return

      }
     
    }
    }


    if(this.OccupationCode == "STDN" || this.OccupationCode == "UNEM" || this.OccupationCode == "HSWF" ||  this.OccupationCode == "STUD" || this.OccupationCode == "RETR"){

      this.membersShow[this.indexPos].annualIncome = 0;
      this.membersShow[this.indexPos].showAnnualIncome = false
      this.hideannualIncome = true

    }
    else{
      this.membersShow[this.indexPos].annualIncome = this.annualincome;
      this.membersShow[this.indexPos].showAnnualIncome = true
      this.hideannualIncome = false

    }
  } else if (this.selectedMemberTitle == "Child1") {
    this.membersShow[i].dependent = false;
    this.selfGenderPopUp = true
    this.spouseGenderPopUp = true
    this.child1GenderPopUp = false
    this.child2GenderPopUp = true
    this.hideannualIncome = true
    this.hideOccupation = true
    this.childMartial = true;

    if (this.membersShow[0].annualIncome === "") {
      $(".qCardPopup").attr("hidden", true);
      this.showTransBaground = true;
      this.showToast('Please fill in self details to move ahead')

      return
    } 
    else{

      if(this.membersShow[i].detailsSaved == true){
        this.memberAge = this.membersShow[i].age;
       }
    }
  } 
    else if (this.selectedMemberTitle == "Child2") {
  

    this.membersShow[i].dependent = false;
    this.selfGenderPopUp = true
    this.spouseGenderPopUp = true
    this.child1GenderPopUp = true
    this.child2GenderPopUp = false
    this.hideannualIncome = true
    this.hideOccupation = true
    this.childMartial = true;

    if (this.membersShow[0].annualIncome === "") {
      $(".qCardPopup").attr("hidden", true);
      this.showTransBaground = true;
      this.showToast('Please fill in self details to move ahead')

      return
    } else{

      if(this.membersShow[i].detailsSaved == true){
        this.memberAge = this.membersShow[i].age;
       }
    }
  }
      

  }
  clearMemberDetails(event: Event, member) {
    event.stopPropagation();
    member.detailsSaved = false;
    member.ageText = "";
    member.maritalStatus = "";
    member.occupationName = "";
    member.annualincome = "";

    for(let i = 0 ; i < this.membersInsured.length ; i++){
      if(member.code == "SELF"){
        this.membersInsured = [];
      }else if(member.code == this.membersInsured[i].code){
        if(member.code == "SPOU"){
         this.membersInsured[0].maritalStatus = "Single"
         this.membersShow[0].maritalStatus = "Single"
        }else if(member.title == "Child1"){
          this.isChild1 = false;
        }
        this.membersInsured.splice(i, 1);
        break;
      }
    }
  }
   
  
  onFocus($event) {
    this.OccupationFocus = true;
    this.Occupationlistcopy = this.occupationLists;
  }
  onOccupationTextChange(ev: any) {
    console.log("" + ev)


    let val = ev.target.value;
    this.OccupationFocus = true;
    if (val && val.length >= 1) {
      console.log("text" + val);

     
      this.Occupationlistcopy = this.occupationLists.filter((data) => {
        return data.occupationName.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.Occupationlistcopy = this.occupationLists;
      this.OccupationFocus = false;
        this.occupationName = ""
        this.category = ""
        this.OccupationCode = ""
    }
  }
  onOccupationSelection(listitem: any) {

    console.log("listitem"+listitem)
    this.category = listitem.category
    this.occupationName = listitem.occupationName;
    this.OccupationCode = listitem.OccupationCode
    this.OccupationFocus = false;

    this.Occupationlistcopy = this.occupationLists;

 if(this.category == "3"){

  this.showToast("We apologize that you cannot purchase on app. Kindly Contact our nearest branch office.");
  this.occupationName = ""
  this.category = ""
  this.OccupationCode = ""

 }

    if(this.selectedMemberTitle == "Spouse"){

      if(this.OccupationCode == "STDN" || this.OccupationCode == "UNEM" || this.OccupationCode == "HSWF" ||  this.OccupationCode == "STUD" || this.OccupationCode == "RETR" ){
        this.hideannualIncome = true
        this.annualincome = 0;
      }
      else{
        this.hideannualIncome = false
      }
    }

    // if(this.selectedMemberTitle == "Self"){

    //   if(this.OccupationCode == "STDN" || this.OccupationCode == "UNEM" || this.OccupationCode == "HSWF" ||  this.OccupationCode == "STUD" || this.OccupationCode == "RETR" ){
    //     this.occupationName = ""
    //     this.category = ""
    //     this.OccupationCode = ""
    //     this.showToast("Please select other occupation to proceed .");
        
    //   }


    // }


  }
  otherFocusOut() {

    for (let i = 0; i < this.Occupationlistcopy.length; i++) {
      if (this.occupationName == this.Occupationlistcopy[i].occupationName) {
        return this.Occupationlistcopy[i].occupationName ;
       
      }
      // else{
      //   this.Occupationlistcopy = this.occupationLists;
      //   this.occupationName = ""
      //   this.category = ""
      //   this.OccupationCode = ""
      // }
    }


    
    setTimeout(() => {
      this.OccupationFocus = false;
    
    }, 100);

  }
  displayAge(agecode) {
    for (let i = 0; i < this.apiAgeGroup.length; i++) {
      if (agecode == this.apiAgeGroup[i].ageCode) {
        return this.apiAgeGroup[i].title + " years";
      }
    }
  }
  proceedtoNext() {
   

    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].title == "Spouse") {
        this.isSpouse = true
      }
    }


    if(this.membersInsured.length == 0){
      this.showToast("Please insure atleast 'self' to proceed.");
    }
    // else if( (this.membersInsured[0].maritalStatus == "Married" && this.isSpouse == false) ){
    //   this.showToast("Please enter spouse details.");
    // }
    else {

     // if (this.membersShow[0].annualIncome > 0 && this.membersShow[1].annualIncome == "" && this.membersShow[2].age == "" && this.membersShow[3].age == "") {

     if(this.membersInsured.length == 1){
      this.policyType = "PATI";
      sessionStorage.PaquickQuoteMembersDetails = JSON.stringify(this.membersInsured);
      sessionStorage.PaMembersShownOnforBuy = JSON.stringify(this.membersShow);


      this.beneficiaryDetailsArray();
      let callRequest = this.appService.callServicePADetails(this.policyType,this.beneficiaryDetails);

      var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
      this.getServiceResponse({'request': sendData},"");  
      console.log("members: " + JSON.stringify(this.membersInsured));
     }else{

      this.policyType = "PATF";
        // this.showToast("You have selected Family Policy");


        sessionStorage.PaquickQuoteMembersDetails = JSON.stringify(this.membersInsured);
        sessionStorage.PaMembersShownOnforBuy = JSON.stringify(this.membersShow);
        this.beneficiaryDetailsArray();
        
        let callRequest = this.appService.callServicePADetails(this.policyType,this.beneficiaryDetails);
        console.log("Send" + JSON.stringify(callRequest));
        
        var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
        this.getServiceResponse({'request': sendData},"");  
     }
    
        
      

     // }

      // if ((this.membersShow[0].annualIncome > 0 && this.membersShow[1].annualIncome > 0) || this.membersShow[2].age != "" || this.membersShow[3].age != "") {

      //   this.policyType = "PATF";
      //   // this.showToast("You have selected Family Policy");


      //   sessionStorage.PaquickQuoteMembersDetails = JSON.stringify(this.membersInsured);
      //   sessionStorage.PaMembersShownOnforBuy = JSON.stringify(this.membersShow);
      //   this.beneficiaryDetailsArray();
        
      //   let callRequest = this.appService.callServiceDetails(this.policyType,this.beneficiaryDetails);
      //   var sendData = this.appService.encryptData(JSON.stringify(callRequest),sessionStorage.TokenId);
      //   this.getServiceResponse({'request': sendData},"");  

       
      // }

    }
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }
  getServiceResponse(serviceData,IndexNumber){
    this.presentLoadingDefault();
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    console.log("header: " + JSON.stringify(headerString));
    
    this.appService.callService("QuotePurchase.svc/ENQ_Policy",serviceData, headerString)
      .subscribe(ENQ_Policy =>{
        console.log(JSON.stringify(ENQ_Policy));
        if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "0"){
		    	sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          
          this.ENQ_PolicyResponse  = ENQ_Policy.ENQ_PolicyResult.Data;
          this.ReferenceNo = this.ENQ_PolicyResponse.RefrenceNo;
          
          if(this.policyType == "PATI"){
            this.navCtrl.push(BuypremiumcalciPage,{displayMembers : this.membersInsured,policyType: this.policyType ,ENQ_PolicyResponse:this.ENQ_PolicyResponse,beneficiaryDetails:this.beneficiaryDetails,"ReferenceNo":this.ReferenceNo});
            this.loading.dismiss();
          }
          
          else{
            this.navCtrl.push(BuypremiumcalciPage,{displayMembers : this.membersInsured,policyType: this.policyType ,ENQ_PolicyResponse:this.ENQ_PolicyResponse,beneficiaryDetails:this.beneficiaryDetails,"ReferenceNo":this.ReferenceNo});

            this.loading.dismiss();

            // let alert = this.alertCtrl.create({
            //   title: 'You have selected Family Policy',
            //   subTitle: '',
            //   cssClass: 'my-class',
            //   buttons: [
            //     {
            //       text: 'OK',
            //       handler: () => {
            //         console.log('Agree clicked');
            //         this.navCtrl.push(BuypremiumcalciPage,{displayMembers : this.membersInsured,policyType: this.policyType ,ENQ_PolicyResponse:this.ENQ_PolicyResponse,beneficiaryDetails:this.beneficiaryDetails,"ReferenceNo":this.ReferenceNo});
            //         alert.dismiss();
                    
            //       }
            //     }
            //   ]
            // });
            // alert.present();
          }
         
        }else if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "807"){
          this.loading.dismiss();
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
          this.navCtrl.push(LoginPage);
        }else if(ENQ_Policy.ENQ_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          console.log("Service resp: "+JSON.stringify(ENQ_Policy));
          this.loading.dismiss(); 
          //this.appService.showloading.dismiss(); 
          this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
         
        }


      });
  }
  
  beneficiaryDetailsArray(){
    this.beneficiaryDetails = [];
    
    console.log("JSON: " + JSON.stringify(this.membersInsured));
    
    for(let i = 0;  i<this.membersInsured.length ; i++){
       
      this.beneficiaryDetails.push({
            "MemberId": i+1,
            "InsuredName": "",
            "InsuredDob": this.membersInsured[i].age,
            "InsuredGender": "",
            "InsuredOccpn": this.membersInsured[i].occupationCode,
            "CoverType": "VITAL",
            "SumInsured": "",
            "DeductibleDiscount": "",
            "Relation": this.membersInsured[i].code,
            "NomineeName": "",
            "NomineeRelation": "",
            "AnualIncome": this.membersInsured[i].annualIncome,
            "Height":"",
            "Weight": "",
            "NomineeAge": "",
            "AppointeeName": "",
            "AptRelWithominee": "",
            "MedicalLoading": "0",
            "PreExstDisease": "N",
            "DiseaseMedicalHistoryList":this.appService.DiseaseMedicalHistory,
            "PrimaryCover": {
              "Cover": [
                {
                  "CoverCode": "AD",
                  "SumInsured": ""
                 },
                {
                  "CoverCode": "PP",
                  "SumInsured": ""
                  
                },
                {
                  "CoverCode": "PT",
                  "SumInsured": ""                
                },
                {
                  "CoverCode": "TT",
                  "SumInsured": ""
                }
              ]
            },
            "SecondaryCover":{
              "Cover": [
                     {
                       "CoverCode": "RF",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "HC",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "AA",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "FT",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "LP",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "LS",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "ME",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "AM",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "CS",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "BB",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "RBC",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "AAC",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "AS",
                       "SumInsured": ""
                     },
                     {
                       "CoverCode": "CP",
                       "SumInsured": ""
                     }
                   ]
           }
         
      })
      console.log("beneficiaryDetails JSON: " + JSON.stringify(this.beneficiaryDetails));
    }

    //this.membersInsured.push(this.beneficiaryDetails);
  }
  DatePick() {

    this.memberAge = "";
    this.keyboard.close();      
    let element = this.memberAge;
    let dateFieldValue = element;
    let year : any = 1985, month : any = 1, date : any = 1;
    
    if(dateFieldValue!="" ||  dateFieldValue == undefined){
      date = dateFieldValue.split("-")[0]
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }else if(this.selectedMemberTitle == "Son"){
      date = 1;
      month = 1;
      year = this.today.getFullYear()-25;
    }else if(this.selectedMemberTitle == "Daughter"){
      date = 1;
      month = 1;
      year = this.today.getFullYear()-25;
    }
    

    let minDateLimit  = (this.selectedMemberTitle == "Child1" || this.selectedMemberTitle == "Child2" ) ? ( new Date(year,1,1).getTime()) : (new Date(1900,1,1).getTime()); 
    console.log("popMember: " + this.selectedMemberTitle);
    
    this.datePicker.show({
      date: new Date(year, month-1, date),
      minDate: minDateLimit,
      maxDate: Date.now(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        let dt = ( this.numberLength(date.getDate())+"-"+this.numberLength(date.getMonth() + 1)+"-"+(date.getFullYear())).toString();
        let age = this.getAge(dt);
        this.memberAge = dt;
       if((parseInt(age.split(" ")[0]) > 25 && this.selectedMemberTitle == "Child1") || (parseInt(age.split(" ")[0]) > 18 && this.selectedMemberTitle == "Child2")){
          this.showToast("Children age should not be more than 25 years");
          this.memberAge = "";
         
       }else if(parseInt(age.split(" ")[0]) > 70 && (this.selectedMemberTitle == "Self" || this.selectedMemberTitle == "Spouse")){
        console.log("Pare");
        
       this.showToast("Parent's age must not be more than 70 years to enter policy");
       this.memberAge = "";
      }

      else if(parseInt(age.split(" ")[0]) <  3 && (this.selectedMemberTitle == "Child1" || this.selectedMemberTitle == "Child2")){
        console.log("Pare");
        
        this.showToast("Children age should not be less than 3 years.");
       this.memberAge = "";
      }

      else if(parseInt(age.split(" ")[0]) < 18 && (this.selectedMemberTitle == "Self" || this.selectedMemberTitle == "Spouse")){
        console.log("Pare");
        
        this.showToast("Parent's age must not be less than 18 years to enter policy");
       this.memberAge = "";
      }
        
       
        console.log("DOB: " + this.memberAge);
      },

      err => console.log('Error occurred while getting date: '+err)
    );
  }

  onOtherDateChange(ev) {
    
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.memberAge+"-";
   
      this.cdr.detectChanges();
    }
    else if(ev.target.value.length==10){
      let age : any= this.getAge(ev.target.value);
      if(parseInt(age.split(" ")[0]) > 25 && (this.selectedMemberTitle == "Child1" || this.selectedMemberTitle == "Child2")){
        console.log("child");
        this.showToast("Children age should not be more than 25 years.");
        this.memberAge = "";
        ev.target.value = "";
        this.cdr.detectChanges();
      }else if(parseInt(age.split(" ")[0]) > 70 && (this.selectedMemberTitle == "Self" || this.selectedMemberTitle == "Spouse")){
        console.log("Pare");
       this.showToast("Parent's age must not be more than 70 years to enter policy");
       this.memberAge = "";
       ev.target.value = "";
      }if(parseInt(age.split(" ")[0]) < 3  && (this.selectedMemberTitle == "Child1" || this.selectedMemberTitle == "Child2")){
        console.log("child");
        this.showToast("Children age should not be less than 3 years.");
        this.memberAge = "";
        ev.target.value = "";
        this.cdr.detectChanges();
      }else if(parseInt(age.split(" ")[0]) < 18 && (this.selectedMemberTitle == "Self" || this.selectedMemberTitle == "Spouse")){
        console.log("Pare");
       this.showToast("Parent's age must not be less than 18 years to enter policy");
       this.memberAge = "";
       ev.target.value = "";
      }
     
      this.keyboard.close();
    }
    
  }


  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365) + " years";
  }

  filledStatus(status) {
    //console.log(status);

    for (let i = 0; i < this.marriedStatusArray.length; i++) {
      if (this.memberMarried == this.marriedStatusArray[i].status) {
        return this.marriedStatusArray[i].value;
      }
    }
  }

}



