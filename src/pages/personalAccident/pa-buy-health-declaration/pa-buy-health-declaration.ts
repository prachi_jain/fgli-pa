import { Component, NgModule } from '@angular/core';
import { IonicPage, Platform, NavController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { PaBuyPolicyReviewPage } from '../pa-buy-policy-review/pa-buy-policy-review'
import { PaVerifyDetailsPage } from '../pa-verify-details/pa-verify-details'
import { IonicPageModule } from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { LandingScreenPage } from '../../health/landing-screen/landing-screen';
import { LoginPage } from '../../health/login/login';

/**
 * Generated class for the PaBuyHealthDeclarationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pa-buy-health-declaration',
  templateUrl: 'pa-buy-health-declaration.html',
})
export class PaBuyHealthDeclarationPage {

  selectedCardArray;
  canShowToast = true;
  //medicalStatus = false;
  medicalStatus = "No";
  memberSelection;
  questionDiv = true;
  membersDeclarationArray =[];
  memberDetails;
  ReferenceNo;
  checkedValue;
  ClientDetails: any = [];
  loading;
  pID;
  thankyouPopup=true;
  policyType;
  ENQ_PolicyResponse
  response;
  QuotationID;
  UID
  insuranceDetailsToSend;
  termsConditionFirst = false
  constructor(private platform: Platform,  private loadingCtrl : LoadingController ,public appService: AppService, public navCtrl: NavController, public navParams: NavParams, public toast: ToastController) {
    this.memberDetails = navParams.get("memberDetails");
    this.selectedCardArray = navParams.get("planCardDetails");
    console.log("Member: " + JSON.stringify(this.memberDetails));
    console.log("card: " + JSON.stringify(this.selectedCardArray));
    this.policyType = navParams.get("PolicyType");
    this.ENQ_PolicyResponse = navParams.get("ENQ_PolicyResponse");
    this.response = this.ENQ_PolicyResponse.PurchaseResponse[0].Result_pa
    this.ReferenceNo = navParams.get("ReferenceNo");

    this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
    this.UID = this.ENQ_PolicyResponse.PurchaseResponse[0].UID;
    this.insuranceDetailsToSend = this.navParams.get("insuranceDetailsToSend");
    
  }

  ionViewDidLoad() {    
    console.log('state: ' + this.questionDiv);
    document.getElementById("splash").style.display='none';
    console.log(this.memberDetails);

    if(sessionStorage.isViewPopped == "true"){
      this.memberDetails = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      sessionStorage.isViewPopped = "";
    }  
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

 

  medicalStatusChange(){
    console.log("status :" + this.medicalStatus);
  }



  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  medicalIssue(value, member){
    console.log("medi" + value);
    this.medicalStatus = value;
    console.log("member: " + JSON.stringify(member));
    if(value == "Yes"){
      member.medicalDeclaration = true;
    }else{
      member.medicalDeclaration = false;
    }
    console.log("boolean: " +member.medicalDeclaration);
    
  }

  proceedReviewPage(){
  
    
    
    

      let proceed = 0;
      for(let i=0; i< this.memberDetails.length; i++){
        console.log("int: for "+ proceed);
          if(this.memberDetails[i].medicalDeclaration == true){           
            proceed++;
            console.log("if"+ proceed);
          }
      }

    
      
    
    if(this.termsConditionFirst == false){
      var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
      this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
      console.log("Data: " + JSON.stringify(sendData));
            
    }else{
      this.showToast("due to medical conditions, you will have to contact your sales person in the nearest branch");
    }
  }

  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          // this.pID = Resp.Proceed_PolicyResult.Data.pID;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.navCtrl.push(PaVerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray,"PolicyType": this.policyType , "ENQ_PolicyResponse": this.ENQ_PolicyResponse ,"beneficiaryDetails": this.selectedCardArray,"insuranceDetailsToSend":this.insuranceDetailsToSend,"ReferenceNo" : this.ReferenceNo});      
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }
  

  onBackClick(){
    sessionStorage.isViewPopped = "true";
    sessionStorage.ProposerDetails = JSON.stringify(this.memberDetails);
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
    this.navCtrl.pop();
  }

  onSaveAndEmail(){
    let proceed = 0;
    for(let i=0; i< this.memberDetails.length; i++){
      console.log("int: for "+ proceed);
        if(this.memberDetails[i].medicalDeclaration == true){           
          proceed++;
          console.log("if"+ proceed);
        }
    }
  
  if(proceed == 0){
    //this.navCtrl.push(VerifyDetailsPage,{"memberDetails":this.memberDetails, "planCardDetails": this.selectedCardArray});      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("Eproposal: " + JSON.stringify(sendData));
    
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }else{
    this.showToast("due to medical conditions, you will have to contact your sales person in the nearest branch");
  }
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": this.QuotationID,
      "UID": this.UID,
      "stage":"4",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.policyType,//"HTF",
        "Duration": sessionStorage.Duration,
        "Installments":  sessionStorage.Installments,
        "IsfgEmployee": sessionStorage.IsFGEmployee,
        "IsPos": sessionStorage.Ispos,
        "BeneficiaryDetails": this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberDetails,
        "CardDetails": this.insuranceDetailsToSend
      }
    }
    console.log("HealthEproposal: " + JSON.stringify(sendRequest));
    return sendRequest;
  }

  getClientNode(){
    let age = this.memberDetails[0].age;
    let clientSalution = "MR";
    if(this.memberDetails[0].Gender == "F"){
      clientSalution = "MRS";
    }
    console.log("email: " + this.memberDetails[0].email);
    
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberDetails[0].proposerName,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberDetails[0].Gender,
      "MaritalStatus": this.memberDetails[0].maritalCode,
      "Occupation": this.memberDetails[0].occupationCode,
      "PANNo": this.memberDetails[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberDetails[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberDetails[0].pincode,
        "City": this.memberDetails[0].city,
        "State": this.memberDetails[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": "",
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberDetails[0].mobile,
        "EmailAddr": this.memberDetails[0].proposerEmail
      }
    });
    console.log("client: " + JSON.stringify(this.ClientDetails));
   
    return this.ClientDetails[this.ClientDetails.length -1];

     
  
  }

  getBeneficiaryDetails(cardDetails){
   

    let BeneDetails = [];
    for(let i = 0 ;i< cardDetails.length ; i++){
      for(let j = 0; j<this.memberDetails.length;j++){
        if( (this.memberDetails[j].code).toUpperCase()== (cardDetails[i].Relation).toUpperCase() ){

        
        
          let mDob = cardDetails[i].InsuredDob.split(" ")[0].split("/");
          let gendr = this.memberDetails[j].Gender;
          BeneDetails.push({
            "MemberId": cardDetails[i].MemberId ,
            "InsuredName": this.memberDetails[j].proposerName,
            "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
            "InsuredGender": gendr,
            "InsuredOccpn": this.memberDetails[j].occupationCode,
            "CoverType": cardDetails[i].CoverType,
            "SumInsured": cardDetails[i].SumInsured,
            "DeductibleDiscount": "0",
            "Relation": cardDetails[i].Relation,
            "NomineeName": this.memberDetails[j].nominee,
            "RelationName":this.memberDetails[i].title,
            "NomineeRelation":this.memberDetails[j].NomineeRelation,
            "AnualIncome":this.memberDetails[j].annualIncome,
            "Height": cardDetails[i].Height,
            "Weight": cardDetails[i].Weight,
            "NomineeAge": this.getAge(this.memberDetails[j].NomineeRelation),
            "AppointeeName": this.memberDetails[j].appointeeName,
            "AptRelWithominee": this.memberDetails[j].AptRelWithominee,
            "MedicalLoading": cardDetails[i].MedicalLoading,
            "PreExstDisease": "N",
            "DiseaseMedicalHistoryList": {
              "DiseaseMedicalHistory": {
                "PreExistingDiseaseCode": "",
                "MedicalHistoryDetail": ""
              }
            },
            "PrimaryCover": cardDetails[i].PrimaryCover,
            "SecondaryCover" : cardDetails[i].SecondaryCover

          })
        }
  
      }
  
    }
    return BeneDetails;
  
  } 

  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365);
  }


  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }
  
  callEProposalService(){   
    var eProposalData = {
          "QuotationID": this.selectedCardArray.QuotationID,
          "UID": this.selectedCardArray.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }
  
  sendEProposerData(serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup=false;
          //this.closeModalPop();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp && resp.ReturnCode == "807"){
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.showToast(resp.ReturnMsg);
          this.navCtrl.push(LoginPage);
          
        }else if(resp.ReturnCode == "500"){
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.showToast(resp.ReturnMsg);
        }
       
      }, (err) => {
      console.log(err);
      });
  }

  OkButton(){
    this.thankyouPopup = true;
    this.navCtrl.push(LandingScreenPage);
  }



}
