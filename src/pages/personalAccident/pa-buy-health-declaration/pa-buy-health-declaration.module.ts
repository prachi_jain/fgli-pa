import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaBuyHealthDeclarationPage } from './pa-buy-health-declaration';

@NgModule({
  declarations: [
    //PaBuyHealthDeclarationPage,
  ],
  imports: [
    IonicPageModule.forChild(PaBuyHealthDeclarationPage),
  ],
})
export class PaBuyHealthDeclarationPageModule {}
