import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaPaymentSuccessPage } from './pa-payment-success';

@NgModule({
  declarations: [
   // PaPaymentSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(PaPaymentSuccessPage),
  ],
})
export class PaPaymentSuccessPageModule {}
