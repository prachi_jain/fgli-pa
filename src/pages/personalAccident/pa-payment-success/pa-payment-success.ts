import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController,LoadingController} from 'ionic-angular';
import { AppService } from '../../../providers/app-service/app-service';
import { PremiumBuyPage } from '../premium-buy/premium-buy'
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LoginPage } from '../../health/login/login';

/**
 * Generated class for the PaPaymentSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pa-payment-success',
  templateUrl: 'pa-payment-success.html',
})
export class PaPaymentSuccessPage {

  paymentStatus;
  paymentData;
  chequeSuccess =true;
  olSuccess =true;
  olFail = true;
  paymentLinkSuccess = true;
  chequeReferenceNo;
  chequeAmount;
  chequeEmail;
  loading;
  QuotationId;
  UId;
  PId;
  email;
  mobile;
  ReferenceNo;
  onlinePayDetails;
  transactionID;
  transactionDate;
  transactionAmount;
  bankTransactionID;
  pdfLink;
  canShowToast = true;

  paymentDetails;
  transactionChequeID;
  pendingGetPaymentDetails = true;
  policyNumber;
  showFooter = true;

  serviceResponsePopup = false;
  serviceCodeStatus;
  message;
  thankyouPopup = true;
  pdfpopupClose = false;
  constructor(public navCtrl: NavController, private iab: InAppBrowser, public toast: ToastController, public navParams: NavParams, public appService: AppService,public loadingCtrl: LoadingController) {
    this.paymentStatus = navParams.get("Result");
    this.paymentDetails = navParams.get("Data");
    console.log("paymentDetails: " + this.paymentStatus+JSON.stringify(this.paymentData));
    console.log("paymentDetails: " + JSON.stringify(this.paymentDetails));
    console.log("navParams: " + JSON.stringify(this.navParams));
  }

  ionViewDidLoad() {
    // this.loading = this.loadingCtrl.create({
    //   content: 'Please wait...'
    // });
    // this.loading.present();
    // this.loading.dismiss();
    this.ReferenceNo = this.navParams.get("ReferenceNo");
    if(this.paymentStatus == 1){
      console.log("Online");      
      
    }else if(this.paymentStatus == 2){
      console.log("Cheque");
      this.QuotationId = this.navParams.get("QuotationID");
      this.UId = this.navParams.get("UID"); 
      this.PId = sessionStorage.policyID; 
      this.email = sessionStorage.email;
      this.mobile = sessionStorage.mobile;
      this.transactionChequeID = this.paymentDetails.TransactionID;
      this.chequeAmount = this.paymentDetails.Amount;  
      //this.ReferenceNo = this.navParams.get("ReferenceNo"); 
    }else if(this.paymentStatus == 3){
      this.QuotationId = this.navParams.get("QuotationID");
      this.UId = this.navParams.get("UID"); 
      this.PId = sessionStorage.policyID; 
      this.email = sessionStorage.email;
      this.mobile = sessionStorage.mobile;
    }
    else if(this.paymentStatus == 4){
      this.pendingGetPaymentDetails = false;
    }
  
   
   var paymentData = {
      "QuotationID": this.QuotationId,
      "UID" : this.UId,
      "PID": this.PId,
   };
    if(this.paymentStatus == 0){
      this.olFail = false;
      this.transactionID = this.paymentDetails.TransactionID;
      this.transactionDate = this.paymentDetails.TransactionDate;
      this.transactionAmount = this.paymentDetails.PremiumAmount;
      //this.loading.dismiss();
    }else if(this.paymentStatus == 1){


      this.olSuccess =false;
      this.transactionID = this.paymentDetails.TransactionID;
    this.policyNumber = this.paymentDetails.PolicyNo;
    this.bankTransactionID = this.paymentDetails.BankTransactionID;
    // this.transactionAmount = this.addCommas(this.paymentDetails.PremiumAmount);
    this.transactionAmount = this.paymentDetails.PremiumAmount;
    this.transactionDate = this.paymentDetails.TransactionDate;
    this.email = this.paymentDetails.Email;
    this.mobile = this.paymentDetails.Mobile;
    this.pdfLink = this.paymentDetails.PDF;
    this.showFooter = false;
    //this.loading.dismiss();
      // this.olSuccess =false;
      // console.log("Online sending Data: " + JSON.stringify(paymentData)); 
      // let dataToSend = this.appService.encryptData(JSON.stringify(paymentData),sessionStorage.TokenId); 
      // let request = {"request":dataToSend};
      // console.log("encryptData: " + JSON.stringify(dataToSend));
      
      // this.callPaymentService("/Payment.svc/GetPaymentDetails",request);  
        
      // let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      // this.appService.callService("/Payment.svc/GetPaymentDetails",this.paymentData, headerString)
      // .subscribe(Resp =>{
      //   this.loading.dismiss();
      //   console.log("Payment online"+ JSON.stringify(Resp));    
      //     if(Resp.GetPaymentDetailsResult.ReturnCode == "0"){
      //       sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
      //      let TracsactionID = Resp.GetPaymentDetailsResult.Data.TransactionID;
      //      //this.navCtrl.push(PaymentSuccessPage,{"Result":2,"Data":Resp.GetPaymentDetailsResult.Data});
      //     }else{
      //       sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
      //      // this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
      //     }
      //   /* else if(serviceFor == "PaymentRequest"){
      //     if(Resp.PaymentRequestResult.ReturnCode== "0"){
      //       this.openBrowser();
      //      }else{
      //        this.showToast(Resp.PaymentRequestResult.ReturnMsg);
      //      }
      //   } */


      // });
      // let request = this.appService.encryptData(JSON.stringify(this.paymentData),sessionStorage.TokenId + "~" +  sessionStorage.UserId);
      // this.callPaymentService("/Payment.svc/GetPaymentDetails",request,"success");
    }else if(this.paymentStatus == 2){
      this.chequeSuccess = false;
      this.showFooter = true;
      console.log("Cheque sending Data: " + JSON.stringify(paymentData)); 
      let dataToSend = this.appService.encryptData(JSON.stringify(paymentData),sessionStorage.TokenId); 
      let request = {"request":dataToSend};
      console.log("encryptData: " + JSON.stringify(dataToSend));
      //this.loading.dismiss();
      //this.callPaymentService("/Payment.svc/GetPaymentDetails",request);  
    }
    else if(this.paymentStatus == 3){
      this.paymentLinkSuccess = false;
      this.showFooter = true;
      console.log("Payment Link Data: " + JSON.stringify(paymentData)); 
      let dataToSend = this.appService.encryptData(JSON.stringify(paymentData),sessionStorage.TokenId); 
      let request = {"request":dataToSend};
      console.log("encryptData: " + JSON.stringify(dataToSend));
      //this.loading.dismiss();
      //this.callPaymentService("/Payment.svc/GetPaymentDetails",request);  
    }
  }
 /* show loading */
 presentLoadingDefault() {
  this.loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  this.loading.present();
}

showToast(Message){
  if(this.canShowToast){
    let pageToast = this.toast.create({
      message:Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 5000,
    });
    this.canShowToast = false;
    pageToast.present();
    pageToast.onDidDismiss(() => {
      this.canShowToast = true;
    });
  }   
}

  callPaymentService(URL,serviceData){                                                                                  
    //this.presentLoadingDefault();    

    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.appService.callService(URL,serviceData,headerString)
      .subscribe(Resp =>{
        this.loading.dismiss(); 
        console.log("test: " + JSON.stringify(Resp));
        if(Resp.GetPaymentDetailsResult.ReturnCode == "0"){
                sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
                this.transactionID = Resp.GetPaymentDetailsResult.Data.TransactionID;
                this.bankTransactionID = Resp.GetPaymentDetailsResult.Data.BankTransactionID;
                //this.transactionAmount = this.addCommas(Resp.GetPaymentDetailsResult.Data.PremiumAmount);
                this.transactionDate = Resp.GetPaymentDetailsResult.Data.TransactionDate;
                this.email = Resp.GetPaymentDetailsResult.Data.Email;
                this.mobile = Resp.GetPaymentDetailsResult.Data.Mobile;
                this.pdfLink = Resp.GetPaymentDetailsResult.Data.PDF;
                this.showFooter = false;		
                this.serviceCodeStatus = 0;
               //this.navCtrl.push(PaymentSuccessPage,{"Result":2,"Data":Resp.GetPaymentDetailsResult.Data});
        }else if(Resp.GetPaymentDetailsResult.ReturnCode == "807"){
          //this.showToast(Resp.GetPaymentDetailsResult.ReturnMsg);
          sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          this.serviceResponsePopup = true;
          this.serviceCodeStatus = 807;
          //this.navCtrl.push(LoginPage);
        }else if(Resp.GetPaymentDetailsResult.ReturnCode == "500"){
         //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
         sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
         this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
         this.serviceResponsePopup = true;
         this.serviceCodeStatus = 500;
         //this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          this.serviceResponsePopup = true;
          this.serviceCodeStatus = 400;
          //this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
        }
      });

    }

    showPDF(){
      console.log(this.pdfLink);  
     // window.open(this.pdfLink,'_system','location=no');    
      let browser = this.iab.create(this.pdfLink  ,'_blank', 'location=no');
      browser.show();
    }
    
    generateNew(){
      sessionStorage.ProposerDetails = undefined;
      sessionStorage.planCardDetails = undefined;
      this.navCtrl.push(PremiumBuyPage,{});
    }

    addCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      console.log("test: " + parts);
      return parts.join(".");   
    }

    serviceResponseOkButton(){
      if(this.serviceCodeStatus == 807){
        this.serviceResponsePopup = false;
        this.navCtrl.push(LoginPage);   
      }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
        this.serviceResponsePopup = false;     
      }
    
    }
    createPDF(){
      var createPDFData = {"policyNo":this.policyNumber};
      let dataToSend = this.appService.encryptData(JSON.stringify(createPDFData),sessionStorage.TokenId); 
      let request = {"request":dataToSend};
      console.log("encryptData: " + JSON.stringify(dataToSend));
      this.callCreatePDFService("/FGHealth.svc/CreatePDF",request);
    }

    callCreatePDFService(URL, serviceData){
      this.presentLoadingDefault();    

      let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService(URL,serviceData,headerString)
        .subscribe(Resp =>{
          this.loading.dismiss(); 
          console.log("test: " + JSON.stringify(Resp));

          if(Resp.CreatePDFResult.ReturnCode == "0"){
            //this.showToast("Your Policy ");
            this.showFooter = true;
            this.pdfpopupClose = true;
            this.serviceCodeStatus = 0;
          }else if(Resp.CreatePDFResult.ReturnCode == "807"){
            sessionStorage.TokenId = Resp.CreatePDFResult.UserToken.TokenId;
            this.message = Resp.CreatePDFResult.ReturnMsg;
            this.serviceResponsePopup = true;
            this.serviceCodeStatus = 807;
          }else if(Resp.CreatePDFResult.ReturnCode == "500"){
            sessionStorage.TokenId = Resp.CreatePDFResult.UserToken.TokenId;
            this.message = Resp.CreatePDFResult.ReturnMsg;
            this.serviceResponsePopup = true;
            this.serviceCodeStatus = 500;
          }else{
            sessionStorage.TokenId = Resp.CreatePDFResult.UserToken.TokenId;
            this.message = Resp.CreatePDFResult.ReturnMsg;
            this.serviceResponsePopup = true;
            this.serviceCodeStatus = 400;
          }
          // if(Resp.GetPaymentDetailsResult.ReturnCode == "0"){
          //         sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          //         this.transactionID = Resp.GetPaymentDetailsResult.Data.TransactionID;
          //         this.policyNumber = Resp.GetPaymentDetailsResult.Data.PolicyNo;
          //         this.bankTransactionID = Resp.GetPaymentDetailsResult.Data.BankTransactionID;
          //         this.transactionAmount = this.addCommas(Resp.GetPaymentDetailsResult.Data.PremiumAmount);
          //         this.transactionDate = Resp.GetPaymentDetailsResult.Data.TransactionDate;
          //         this.email = Resp.GetPaymentDetailsResult.Data.Email;
          //         this.mobile = Resp.GetPaymentDetailsResult.Data.Mobile;
          //         this.pdfLink = Resp.GetPaymentDetailsResult.Data.PDF;
          //        //this.navCtrl.push(PaymentSuccessPage,{"Result":2,"Data":Resp.GetPaymentDetailsResult.Data});
          // }else if(Resp.GetPaymentDetailsResult.ReturnCode == "807"){
          //   //this.showToast(Resp.GetPaymentDetailsResult.ReturnMsg);
          //   sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          //   this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          //   this.serviceResponsePopup = false;
          //   this.serviceCodeStatus = 807;
          //   //this.navCtrl.push(LoginPage);
          // }else if(Resp.GetPaymentDetailsResult.ReturnCode == "500"){
          //   //this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          //   sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          //   this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          //   this.serviceResponsePopup = false;
          //   this.serviceCodeStatus = 500;
          //   //this.navCtrl.push(LoginPage);
          // }else{
          //   sessionStorage.TokenId = Resp.GetPaymentDetailsResult.UserToken.TokenId;
          //   this.message = Resp.GetPaymentDetailsResult.ReturnMsg;
          //   this.serviceResponsePopup = false;
          //   this.serviceCodeStatus = 400;
          //   //this.showToast(Resp.PaymentRequestByChequeResult.ReturnMsg);
          // }
        });
    }

    OkButton(){
     // if(this.serviceCodeStatus == 807){
      //   this.thankyouPopup = true;
      //   this.navCtrl.push(LoginPage);   
      // }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      //   this.thankyouPopup = true;     
      // }else if(this.serviceCodeStatus == 0){
      //   this.thankyouPopup = true;  
      // }

      if(this.serviceCodeStatus == 0){
        console.log("Hurray");        
        this.pdfpopupClose = false;
      }else{
        console.log("Sorry");        
      }
      
    }
}
