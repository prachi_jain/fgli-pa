import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController,
  AlertController
} from "ionic-angular";
import { TRANSITION_ID } from "@angular/platform-browser/src/browser/server-transition";
import { ComponentsModule } from "../../../components/components.module";
import { NumericStepperComponent } from "../../../components/numeric-stepper/numeric-stepper";
import { AddCommasPipe } from "../../../pipes/add-commas/add-commas";
import { NUMBER_TYPE } from "@angular/compiler/src/output/output_ast";
import { BuyPremiumlistviewPage } from "../buy-premiumlistview/buy-premiumlistview";
import { AppService } from "../../../providers/app-service/app-service";
import { LoginPage } from "../../health/login/login";
import { Component } from "@angular/core";
/**
 * Generated class for the BuypremiumcalciPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-buypremiumcalci",
  templateUrl: "buypremiumcalci.html"
})
export class BuypremiumcalciPage {
  setSelfLimitVar = false;
  setSpouseLimitVar = false;
  setChild1LimitVar = false;
  setChild2LimitVar = false;

  maxLimitSelfAccDeath;
  maxLimitSelfPPD;
  maxLimitSelfPTD;
  maxLimitSelfTTD;

  maxLimitSpouseAccDeath;
  maxLimitSSpousePPD;
  maxLimitSpousePTD;
  maxLimitSpouseTTD;

  maxLimitChild1AccDeath;
  maxLimitChild1PPD;
  maxLimitChild1PTD;
  maxLimitChild1TTD;

  maxLimitChild2AccDeath;
  maxLimitChild2PPD;
  maxLimitChild2PTD;
  maxLimitChild2TTD;

  canShowToast = true;

  selfTabShow = true;
  spouseTabShow = true;
  child1TabShow = true;
  child2TabShow = true;

  selfadaptionallowncedivSow = false;
  selfaccidentahospitalisation = false;
  selfaccidentalmedicalExp = false;
  selfchildEdusupport = true;
  selffamilyTransportAllownce = false;
  selfhospitalCashAllownce = false;
  selflifeSuppBenefit = false;
  selfbrokenBones = false;
  selfroadAmbulanceCover = false;
  selfairAmbulanceCover = false;
  selfadventureSportsBenefit = false;
  selfchauffeurPlanBenefit = false;
  selfloanProtector = false;

  spouseadaptionallowncedivSow = true;
  spouseaccidentahospitalisation = true;
  spouseaccidentalmedicalExp = true;
  spousechildEdusupport = true;
  spousefamilyTransportAllownce = true;
  spousehospitalCashAllownce = true;
  spouselifeSuppBenefit = true;
  spousebrokenBones = true;
  spouseroadAmbulanceCover = true;
  spouseairAmbulanceCover = true;
  spouseadventureSportsBenefit = true;
  spousechauffeurPlanBenefit = true;
  spouseloanProtector = true;

  child1adaptionallowncedivSow = true;
  child1accidentahospitalisation = true;
  child1accidentalmedicalExp = true;
  child1Edusupport = true;
  child1familyTransportAllownce = true;
  child1hospitalCashAllownce = true;
  child1lifeSuppBenefit = true;
  child1brokenBones = true;
  child1roadAmbulanceCover = true;
  child1airAmbulanceCover = true;
  child1adventureSportsBenefit = true;
  child1chauffeurPlanBenefit = true;
  child1loanProtector = true;

  child2adaptionallowncedivSow = true;
  child2accidentahospitalisation = true;
  child2accidentalmedicalExp = true;
  child2Edusupport = true;
  child2familyTransportAllownce = true;
  child2hospitalCashAllownce = true;
  child2lifeSuppBenefit = true;
  child2brokenBones = true;
  child2roadAmbulanceCover = true;
  child2airAmbulanceCover = true;
  child2adventureSportsBenefit = true;
  child2chauffeurPlanBenefit = true;
  child2loanProtector = true;
  //addCovers checkbox ngModel value
  selfadaptation;
  selfaccidental;
  selfmedical;
  selfchild;
  selffamily;
  selfhospital;
  selflife;
  selfBroken;
  selfRoadAmbulance;
  selfAirAmbulance;
  selfAdventureSports;
  selfChauffeurPlan;
  selfloanValue;

  spouseadaptation;
  spouseaccidental;
  spousemedical;
  spousechild;
  spousefamily;
  spousehospital;
  spouselife;
  spouseBroken;
  spouseRoadAmbulance;
  spouseAirAmbulance;
  spouseAdventureSports;
  spouseChauffeurPlan;
  spouseloanValue;

  child1adaptation;
  child1accidental;
  child1medical;
  child1child;
  child1family;
  child1hospital;
  child1life;
  child1Broken;
  child1RoadAmbulance;
  child1AirAmbulance;
  child1AdventureSports;
  child1ChauffeurPlan;
  child1loanValue;

  child2adaptation;
  child2accidental;
  child2medical;
  child2child;
  child2family;
  child2hospital;
  child2life;
  child2Broken;
  child2RoadAmbulance;
  child2AirAmbulance;
  child2AdventureSports;
  child2ChauffeurPlan;
  child2loanValue;

  //checkboxNgModel
  selfpartialDis = true;
  selftotalDis = true;
  selftempDis = true;

  adaption_allowance = false;
  acc_hospitalisation = false;
  acc_medical_expenses = false;
  self_child_edu_support = false;
  self_family_trans_allowance = false;
  self_hospital_cash_allownce = false;
  self_life_support_benefit = false;
  self_broken_bones = false;
  self_road_ambulance_cover = false;
  self_air_ambulance_cover = false;
  self_adventure_sports_benefit = false;
  self_chauffeur_plan_benefit = false;
  self_loan_protector = false;

  spousepartialDis = true;
  spousetotalDis = true;
  spousetempDis = true;
  spouse_adaption_allowance = false;
  spouse_acc_hospitalisation = false;
  spouse_acc_medical_expenses = false;
  spouse_child_edu_support = false;
  spouse_family_trans_allowance = false;
  spouse_hospital_cash_allownce = false;
  spouse_life_support_benefit = false;
  spouse_broken_bones = false;
  spouse_road_ambulance_cover = false;
  spouse_air_ambulance_cover = false;
  spouse_adventure_sports_benefit = false;
  spouse_chauffeur_plan_benefit = false;
  spouse_loan_protector = false;

  child1partialDis = true;
  child1totalDis = true;
  child1_adaption_allowance = false;
  child1_acc_hospitalisation = false;
  child1_acc_medical_expenses = false;
  child1_edu_support = false;
  child1_family_trans_allowance = false;
  child1_hospital_cash_allownce = false;
  child1_life_support_benefit = false;
  child1_broken_bones = false;
  child1_road_ambulance_cover = false;
  child1_air_ambulance_cover = false;
  child1_adventure_sports_benefit = false;
  child1_chauffeur_plan_benefit = false;
  child1_loan_protector = false;

  child2partialDis = true;
  child2totalDis = true;
  child2_adaption_allowance = false;
  child2_acc_hospitalisation = false;
  child2_acc_medical_expenses = false;
  child2_edu_support = false;
  child2_family_trans_allowance = false;
  child2_hospital_cash_allownce = false;
  child2_life_support_benefit = false;
  child2_broken_bones = false;
  child2_road_ambulance_cover = false;
  child2_air_ambulance_cover = false;
  child2_adventure_sports_benefit = false;
  child2_chauffeur_plan_benefit = false;
  child2_loan_protector = false;

  //increment decrement div hide show value
  //adaption_allowance= true;
  adaptationallownceCheckDiv = true;
  accidentalhospitalDivShow = true;
  accidentalmedicalExpDivShow = true;
  childeduSuppDivShow = true;
  familyTransAllownceDivShow = true;
  hospicashAllownceDivShow = true;
  lifeSuppBenefitDivShow = true;
  brokenBonesDivShow = true;
  roadAmbulanceCoverDivShow = true;
  airambulancecoverDivShow = true;
  adventureSportsBenefitDivShow = true;
  chauffeurPlanBenefitDivShow = true;
  loanProtectorDivShow = true;

  showTransBagroundSelf = true;
  showTransBagroundSpouse = true;
  showTransBagroundChild1 = true;
  showTransBagroundChild2 = true;
  partdisab = false;
  permdisab = false;
  tempdisab = false;
  showPopupSelf = true;
  showPopupSpouse = true;
  showPopupChild1 = true;
  showPopupChild2 = true;
  lprotect = true;
  spouselprotect = true;
  totalDis;
  //loanValue;
  spousepartdisab = false;
  spousepermdisab = false;
  spousetempdisab = false;
  spouseadaptationallownceCheckDiv = true;
  spouseaccidentalhospitalDivShow = true;
  spouseaccidentalmedicalExpDivShow = true;
  spousechildeduSuppDivShow = true;
  spousefamilyTransAllownceDivShow = true;
  spousehospicashAllownceDivShow = true;
  spouselifeSuppBenefitDivShow = true;
  spousebrokenBonesDivShow = true;
  spouseroadAmbulanceCoverDivShow = true;
  spouseairambulancecoverDivShow = true;
  spouseadventureSportsBenefitDivShow = true;
  spousechauffeurPlanBenefitDivShow = true;
  spouseloanProtectorDivShow = true;
  child1partdisab = false;
  child1permdisab = false;
  child1tempdisab = false;
  child1adaptationallownceCheckDiv = true;
  child1accidentalhospitalDivShow = true;
  child1accidentalmedicalExpDivShow = true;
  child1eduSuppDivShow = true;
  child1familyTransAllownceDivShow = true;
  child1hospicashAllownceDivShow = true;
  child1lifeSuppBenefitDivShow = true;
  child1brokenBonesDivShow = true;
  child1roadAmbulanceCoverDivShow = true;
  child1airambulancecoverDivShow = true;
  child1adventureSportsBenefitDivShow = true;
  child1chauffeurPlanBenefitDivShow = true;
  child1loanProtectorDivShow = true;
  child2partdisab = false;
  child2permdisab = false;
  child2tempdisab = false;
  child2adaptationallownceCheckDiv = true;
  child2accidentalhospitalDivShow = true;
  child2accidentalmedicalExpDivShow = true;
  child2eduSuppDivShow = true;
  child2familyTransAllownceDivShow = true;
  child2hospicashAllownceDivShow = true;
  child2lifeSuppBenefitDivShow = true;
  child2loanProtectorDivShow = true;
  child2brokenBonesDivShow = true;
  child2roadAmbulanceCoverDivShow = true;
  child2airambulancecoverDivShow = true;
  child2adventureSportsBenefitDivShow = true;
  child2chauffeurPlanBenefitDivShow = true;

  selfCategoryForChild;

  value;
  toggleStatus = true;
  spousetoggleStatus = true;
  loanProtectToggle = true;
  public isToggled: boolean;

  category: any = "";
  annualincome: any = "";

  insureddetails = new InsuredDetails();
  additionalCover = new AdditionalCover();

  spouseinsureddetails = new InsuredDetails();
  spouseadditionaldetails = new AdditionalCover();

  child1insureddetails = new InsuredDetails();
  child1additionaldetails = new AdditionalCover();

  child2insureddetails = new InsuredDetails();
  child2additionaldetails = new AdditionalCover();

  policyType;
  selfTTD;
  spouseTTD;

  lastActiveMember;
  ActiveMember;

  SelfOccupationCode = "";
  SpouseOccupationCode = "";

  membersInsured;
  lastIndex;
  response;
  PremiumDetails;

  pet = "Self";
  ENQ_PolicyResponse;

  selfdetails;
  spousedetails;
  sondetails;
  daughterdetails;
  clickedMemberArray;
  hideProceedBtn = true;
  hideNextBtn = true;
  hideRecalculate = true;

  hideRecalculateOnload = true;
  beneficiaryDetails;
  loading;
  ReferenceNo;
  QuotationID;
  UID;

  selfCoversArray = [];
  spouseCoversArray = [];
  child1CoversArray = [];
  child2CoversArray = [];

  selfMainCoversPremium;
  spouseMainCoversPremium;
  child1MainCoversPremium;
  child2MainCoversPremium;

  selfAddCoversPremium;
  spouseAddCoversPremium;
  child1AddCoversPremium;
  child2AddCoversPremium;

  backPremiumDetails;
  hideSpousePPD = false
  hideChild1PPD = false
  hideChild2PPD = false


  hideSpousePTD = false
  hideChild1PTD = false
  hideChild2PTD = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public appService: AppService,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    this.isToggled = true;
    this.policyType = navParams.get("policyType");
    this.membersInsured = navParams.get("displayMembers");
    this.beneficiaryDetails = navParams.get("beneficiaryDetails");
    this.ENQ_PolicyResponse = navParams.get("ENQ_PolicyResponse"); 
    this.ReferenceNo = this.ENQ_PolicyResponse.RefrenceNo;
    this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
    this.UID = this.ENQ_PolicyResponse.PurchaseResponse[0].UID;
    this.response = this.ENQ_PolicyResponse.PurchaseResponse[0].Result_pa;
    this.PremiumDetails = this.response.Root.Policy.NewDataSet.PremiumDeatils;
    this.backPremiumDetails =  this.PremiumDetails
    console.log("response1" + this.PremiumDetails);
    this.ActiveMember = "Self";
  }

  ionViewWillLeave() {
    console.log("ionViewWillLeave");
  }
  ionViewDidLeave() {
    this.ActiveMember = "Self";
    this.pet = "Self";
    //this.PremiumDetails =  this.backPremiumDetails;
    for (let i = 0; i < this.membersInsured.length; i++) {
      this.lastIndex = i;
      if (this.membersInsured[i].detailsSaved == true) {
        let member = this.membersInsured[i];

        this.lastActiveMember = member.title;
      }
    }
    if (this.lastActiveMember == "Self" && this.policyType == "PATI") {
      this.hideProceedBtn = false;
      this.hideRecalculate = true;
      this.hideNextBtn = true;
    } else if(this.lastActiveMember == this.ActiveMember){
      this.hideNextBtn = true;
      this.hideRecalculate = true;
      this.hideProceedBtn = false;
    }
    else{

      this.hideNextBtn = false;
      this.hideRecalculate = true;
      this.hideProceedBtn = true;
    }
   // this.loadMethod()
  }
  ionViewDidLoad() {

    
    this.loadMethod()
  }
 
  loadMethod(){
    this.selfTTD = false;
    this.spouseTTD = false;

    for (let i = 0; i < this.membersInsured.length; i++) {
      this.lastIndex = i;
      if (this.membersInsured[i].detailsSaved == true) {
        let member = this.membersInsured[i];

        if (member.title == "Self") {
          this.calculateSelfCovers(member.code);
        } else if (member.title == "Spouse") {
          this.calculateSpouseCovers(member.code);
        } else if (member.title == "Child1") {
          this.selfchildEdusupport = false
          if (member.code == "Son") {
            this.calculateSonCovers("SON");
          }
          if (member.code == "Daughter") {
            this.calculateSonCovers("DAUGHTER");
          }
        } else if (member.title == "Child2") {
          this.selfchildEdusupport = false
          if (member.code == "Son") {
            this.calculateDaughterCovers("SON");
          }
          if (member.code == "Daughter") {
            this.calculateDaughterCovers("DAUGHTER");
          }
        }

        this.lastActiveMember = member.title;
      }
    }
    if (this.lastActiveMember == "Self" && this.policyType == "PATI") {
      this.hideProceedBtn = false;
    } else {
      this.hideNextBtn = false;
      this.hideRecalculate = true;
      this.hideProceedBtn = true;
    }

    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].title == "Self") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = 0;
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Spouse") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = 0;
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Child1") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = 0;
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Child2") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = 0;
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_lifesupportbenefit;
          }
        }
      }
    }

  }

//   clickTab(title,code){

// if(title == "Self"){
//   this.calculateSelfCovers(code);
// }
// else if(title == "Spouse"){
//   this.calculateSpouseCovers(code);
  
// }
// else if(title == "Child1"){
//   if (code == "Son") {
//     this.calculateSonCovers("SON");
//   }
//   if (code == "Daughter") {
//     this.calculateSonCovers("DAUGHTER");
//   }
// }
// else if(title == "Child2"){
//   if (code == "Son") {
//     this.calculateDaughterCovers("SON");
//   }
//   if (code == "Daughter") {
//     this.calculateDaughterCovers("DAUGHTER");
//   }
// }
  
// }

clickTab(member, num) {
  this.clickedMemberArray = this.membersInsured[num];

  if (member == "Self") {
    this.calculateSelfCovers(this.clickedMemberArray.code);
  } else if (member == "Spouse") {
    this.calculateSpouseCovers(this.clickedMemberArray.code);
  } else if (member == "Child1") {
    if (this.clickedMemberArray.code == "Son") {
      this.calculateSonCovers("SON");
    }
    if (this.clickedMemberArray.code == "DAUG") {
      this.calculateSonCovers("DAUG");
    }
  } else if (member == "Child2") {
    if (this.clickedMemberArray.code == "Son") {
      this.calculateDaughterCovers("SON");
    }
    if (this.clickedMemberArray.code == "DAUG") {
      this.calculateDaughterCovers("DAUG");
    }
  }

  if (this.membersInsured[num].title == this.lastActiveMember) {
    this.hideProceedBtn = false;
    this.hideNextBtn = true;
    this.hideRecalculate = true;
  } else {
    this.hideNextBtn = false;
    this.hideProceedBtn = true;
    this.hideRecalculate = true;
  }
}
  calculateSelfCovers(member) {
    this.selfdetails = this.PremiumDetails.filter(function(self) {
      return self.Relationship == member;
    });

    for (let i = 0; i < this.selfdetails.length; i++) {
      if (this.selfdetails[i].CoverCode == "AD") {
        this.insureddetails.acc_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.acc_premium = this.selfdetails[i].Premium;

        if (this.insureddetails.acc_maxinsured > 3000000) {
          this.insureddetails.acc_maxinsured = 3000000;
        }
        this.maxLimitSelfAccDeath = this.insureddetails.acc_maxinsured;
      } else if (this.selfdetails[i].CoverCode == "PP") {
        this.insureddetails.ppd_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.ppd_premium = this.selfdetails[i].Premium;
        if (this.insureddetails.ppd_maxinsured > 3000000) {
          this.insureddetails.ppd_maxinsured = 3000000;
        }
        this.maxLimitSelfPPD = this.insureddetails.ppd_maxinsured;

        this.insureddetails.temp_ppd_maxinsured = this.insureddetails.ppd_maxinsured;
      } else if (this.selfdetails[i].CoverCode == "PT") {
        this.insureddetails.ptd_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.ptd_premium = this.selfdetails[i].Premium;

        if (this.insureddetails.ptd_maxinsured > 3000000) {
          this.insureddetails.ptd_maxinsured = 3000000;
        }
        this.maxLimitSelfPTD = this.insureddetails.ptd_maxinsured;

        this.insureddetails.temp_ptd_maxinsured = this.insureddetails.ptd_maxinsured;



      } else if (this.selfdetails[i].CoverCode == "TT") {
        this.insureddetails.ttd_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.ttd_premium = this.selfdetails[i].Premium;
        if (Number(this.insureddetails.ttd_maxinsured) >= 1000000) {
          this.insureddetails.ttd_maxinsured = 1000000;
        }
        this.maxLimitSelfTTD = this.insureddetails.ttd_maxinsured;

        this.insureddetails.temp_ttd_maxinsured = this.insureddetails.ttd_maxinsured;
      } else if (this.selfdetails[i].CoverCode == "AA") {
        this.additionalCover.selfbenefitamount_adaptationallowance = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_adaptationallowance = this.additionalCover.selfbenefitamount_adaptationallowance;

        this.additionalCover.selfpremium_adaptationallowance = this.selfdetails[
          i
        ].Premium;

        this.additionalCover.temp_selfbenefitamount_adaptationallowance = this.additionalCover.selfbenefitamount_adaptationallowance;
      } else if (this.selfdetails[i].CoverCode == "CS") {
        this.additionalCover.selfbenefitamount_childeducationsupport = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_childeducationsupport = this.additionalCover.selfbenefitamount_childeducationsupport;
        this.additionalCover.selfpremium_childeducationsupport = this.selfdetails[
          i
        ].Premium;

        this.additionalCover.temp_selfbenefitamount_childeducationsupport = this.additionalCover.selfbenefitamount_childeducationsupport;
      } else if (this.selfdetails[i].CoverCode == "FT") {
        this.additionalCover.selfbenefitamount_familytransportallowance = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_familytransportallowance = this.additionalCover.selfbenefitamount_familytransportallowance;
        this.additionalCover.selfpremium_familytransportallowance = this.selfdetails[
          i
        ].Premium;

        this.additionalCover.temp_selfbenefitamount_familytransportallowance = this.additionalCover.selfbenefitamount_familytransportallowance;
      } else if (this.selfdetails[i].CoverCode == "HC") {
        this.additionalCover.selfbenefitamount_hospitalcashallowance = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_hospitalcashallowance = this.additionalCover.selfbenefitamount_hospitalcashallowance;
        this.additionalCover.selfpremium_hospitalcashallowance = this.selfdetails[
          i
        ].Premium;

        this.additionalCover.temp_selfbenefitamount_hospitalcashallowance = this.additionalCover.selfbenefitamount_hospitalcashallowance;
      } else if (this.selfdetails[i].CoverCode == "LP") {
        this.additionalCover.selfbenefitamount_loanprotector = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_loanprotector = this.additionalCover.selfbenefitamount_loanprotector;
        this.additionalCover.selfpremium_loanprotector = this.selfdetails[
          i
        ].Premium;
        this.additionalCover.temp_selfbenefitamount_loanprotector = this.additionalCover.selfbenefitamount_loanprotector;
      } else if (this.selfdetails[i].CoverCode == "LS") {
        this.additionalCover.selfbenefitamount_lifesupportbenefit = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_lifesupportbenefit = this.additionalCover.selfbenefitamount_lifesupportbenefit;
        this.additionalCover.selfpremium_lifesupportbenefit = this.selfdetails[
          i
        ].Premium;

        this.additionalCover.temp_selfbenefitamount_lifesupportbenefit = this.additionalCover.selfbenefitamount_lifesupportbenefit;
      } else if (this.selfdetails[i].CoverCode == "ME") {
        this.additionalCover.selfbenefitamount_acchospitalisation = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_acchospitalisation = this.additionalCover.selfbenefitamount_acchospitalisation;
        this.additionalCover.selfpremium_acchospitalisation = this.selfdetails[
          i
        ].Premium;
        this.additionalCover.temp_selfbenefitamount_acchospitalisation = this.additionalCover.selfbenefitamount_acchospitalisation;
      } else if (this.selfdetails[i].CoverCode == "AM") {
        this.additionalCover.selfbenefitamount_accmedicalexpenses = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_accmedicalexpenses = this.additionalCover.selfbenefitamount_accmedicalexpenses;
        this.additionalCover.selfpremium_accmedicalexpenses = this.selfdetails[
          i
        ].Premium;
        this.additionalCover.temp_selfbenefitamount_accmedicalexpenses = this.additionalCover.selfbenefitamount_accmedicalexpenses;
      } else if (this.selfdetails[i].CoverCode == "RF") {
        this.additionalCover.selfbenefitamount_repatriationfuneralexpenses = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.temp_selfbenefitamount_repatriationfuneralexpenses = this.additionalCover.selfbenefitamount_repatriationfuneralexpenses;
      } else if (this.selfdetails[i].CoverCode == "BB") {
        this.additionalCover.selfbenefitamount_brokenbones = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_brokenbones = this.additionalCover.selfbenefitamount_brokenbones;
        this.additionalCover.selfpremium_brokenbones = this.selfdetails[
          i
        ].Premium;

        this.additionalCover.temp_selfbenefitamount_brokenbones = this.additionalCover.selfbenefitamount_brokenbones;
      } else if (this.selfdetails[i].CoverCode == "RBC") {
        this.additionalCover.selfbenefitamount_roadambulancecover = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_roadambulancecover = this.additionalCover.selfbenefitamount_roadambulancecover;
        this.additionalCover.selfpremium_roadambulancecover = this.selfdetails[
          i
        ].Premium;
        this.additionalCover.temp_selfbenefitamount_roadambulancecover = this.additionalCover.selfbenefitamount_roadambulancecover;
      } else if (this.selfdetails[i].CoverCode == "AAC") {
        this.additionalCover.selfbenefitamount_airambulancecover = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_airambulancecover = this.additionalCover.selfbenefitamount_airambulancecover;
        this.additionalCover.selfpremium_airambulancecover = this.selfdetails[
          i
        ].Premium;
        this.additionalCover.temp_selfbenefitamount_airambulancecover = this.additionalCover.selfbenefitamount_airambulancecover;
      } else if (this.selfdetails[i].CoverCode == "AS") {
        this.additionalCover.selfbenefitamount_adventuresportbenefit = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_adventuresportbenefit = this.additionalCover.selfbenefitamount_adventuresportbenefit;
        this.additionalCover.selfpremium_adventuresportbenefit = this.selfdetails[
          i
        ].Premium;

        this.additionalCover.temp_selfbenefitamount_adventuresportbenefit = this.additionalCover.selfbenefitamount_adventuresportbenefit;
      } else if (this.selfdetails[i].CoverCode == "CP") {
        this.additionalCover.selfbenefitamount_chauffeurplanbenefit = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfmaxbenefitamount_chauffeurplanbenefit = this.additionalCover.selfbenefitamount_chauffeurplanbenefit;
        this.additionalCover.selfpremium_chauffeurplanbenefit = this.selfdetails[
          i
        ].Premium;
        this.additionalCover.temp_selfbenefitamount_chauffeurplanbenefit = this.additionalCover.selfbenefitamount_chauffeurplanbenefit;
      }
    }
  }

  calculateSpouseCovers(member) {
    this.spousedetails = this.PremiumDetails.filter(function(self) {
      return self.Relationship == member;
    });

    for (let i = 0; i < this.spousedetails.length; i++) {
      if (this.spousedetails[i].CoverCode == "AD") {
        this.spouseinsureddetails.acc_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.acc_premium = this.spousedetails[i].Premium;
        if (this.spouseinsureddetails.acc_maxinsured > 3000000) {
          this.spouseinsureddetails.acc_maxinsured = 3000000;
        }
        if (
          this.spouseinsureddetails.acc_maxinsured >
          this.insureddetails.acc_maxinsured
        ) {
          this.spouseinsureddetails.acc_maxinsured = this.insureddetails.acc_maxinsured;
        }
        this.maxLimitSpouseAccDeath = this.spouseinsureddetails.acc_maxinsured;


      } else if (this.spousedetails[i].CoverCode == "PP") {
        this.spouseinsureddetails.ppd_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.ppd_premium = this.spousedetails[i].Premium;
        if (this.spouseinsureddetails.ppd_maxinsured > 3000000) {
          this.spouseinsureddetails.ppd_maxinsured = 3000000;
        }
        if (
          this.spouseinsureddetails.ppd_maxinsured >
          this.insureddetails.ppd_maxinsured
        ) {
          this.spouseinsureddetails.ppd_maxinsured = this.insureddetails.ppd_maxinsured;
        }
        this.maxLimitSSpousePPD = this.spouseinsureddetails.ppd_maxinsured;

        this.spouseinsureddetails.temp_ppd_maxinsured = this.spouseinsureddetails.ppd_maxinsured;



      } else if (this.spousedetails[i].CoverCode == "PT") {
        this.spouseinsureddetails.ptd_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.ptd_premium = this.spousedetails[i].Premium;

        if (this.spouseinsureddetails.ptd_maxinsured > 3000000) {
          this.spouseinsureddetails.ptd_maxinsured = 3000000;
        }
        if (
          this.spouseinsureddetails.ptd_maxinsured >
          this.insureddetails.ptd_maxinsured
        ) {
          this.spouseinsureddetails.ptd_maxinsured = this.insureddetails.ptd_maxinsured;
        }
        this.maxLimitSpousePTD = this.spouseinsureddetails.ptd_maxinsured;

        this.spouseinsureddetails.temp_ptd_maxinsured = this.spouseinsureddetails.ptd_maxinsured;



      } else if (this.spousedetails[i].CoverCode == "TT") {
        this.spouseinsureddetails.ttd_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.ttd_premium = this.spousedetails[i].Premium;
        if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 1000000) {
          this.spouseinsureddetails.ttd_maxinsured = 1000000;
        }
        if (
          this.spouseinsureddetails.ttd_maxinsured >
          this.insureddetails.ttd_maxinsured
        ) {
          this.spouseinsureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured;
        }
        this.maxLimitSpouseTTD = this.spouseinsureddetails.ttd_maxinsured;
        this.spouseinsureddetails.temp_ttd_maxinsured = this.spouseinsureddetails.ttd_maxinsured;


        
      } else if (this.spousedetails[i].CoverCode == "AA") {
        this.additionalCover.spousebenefitamount_adaptationallowance = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousebenefitamount_adaptationallowance = this.additionalCover.spousebenefitamount_adaptationallowance;
        this.additionalCover.spousepremium_adaptationallowance = this.spousedetails[
          i
        ].Premium;

        this.additionalCover.temp_spousebenefitamount_adaptationallowance = this.additionalCover.spousebenefitamount_adaptationallowance;
      } else if (this.spousedetails[i].CoverCode == "CS") {
        this.additionalCover.spousebenefitamount_childeducationsupport = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_childeducationsupport = this.additionalCover.spousebenefitamount_childeducationsupport;
        this.additionalCover.spousepremium_childeducationsupport = this.spousedetails[
          i
        ].Premium;

        this.additionalCover.temp_spousebenefitamount_childeducationsupport = this.additionalCover.spousebenefitamount_childeducationsupport;
      } else if (this.spousedetails[i].CoverCode == "FT") {
        this.additionalCover.spousebenefitamount_familytransportallowance = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_familytransportallowance = this.additionalCover.spousebenefitamount_familytransportallowance;
        this.additionalCover.spousepremium_familytransportallowance = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_familytransportallowance = this.additionalCover.spousebenefitamount_familytransportallowance;
      } else if (this.spousedetails[i].CoverCode == "HC") {
        this.additionalCover.spousebenefitamount_hospitalcashallowance = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_hospitalcashallowance = this.additionalCover.spousebenefitamount_hospitalcashallowance;
        this.additionalCover.spousepremium_hospitalcashallowance = this.spousedetails[
          i
        ].Premium;

        this.additionalCover.temp_spousebenefitamount_hospitalcashallowance = this.additionalCover.spousebenefitamount_hospitalcashallowance;
      } else if (this.spousedetails[i].CoverCode == "LP") {
        this.additionalCover.spousebenefitamount_loanprotector = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_loanprotector = this.additionalCover.spousebenefitamount_loanprotector;
        this.additionalCover.spousepremium_loanprotector = this.spousedetails[
          i
        ].Premium;

        this.additionalCover.temp_spousebenefitamount_loanprotector = this.additionalCover.spousebenefitamount_loanprotector;
      } else if (this.spousedetails[i].CoverCode == "LS") {
        this.additionalCover.spousebenefitamount_lifesupportbenefit = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_lifesupportbenefit = this.additionalCover.spousebenefitamount_lifesupportbenefit;
        this.additionalCover.spousepremium_lifesupportbenefit = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_lifesupportbenefit = this.additionalCover.spousebenefitamount_lifesupportbenefit;
      } else if (this.spousedetails[i].CoverCode == "ME") {
        this.additionalCover.spousebenefitamount_acchospitalisation = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_acchospitalisation = this.additionalCover.spousebenefitamount_acchospitalisation;
        this.additionalCover.spousepremium_acchospitalisation = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_acchospitalisation = this.additionalCover.spousebenefitamount_acchospitalisation;
      } else if (this.spousedetails[i].CoverCode == "AM") {
        this.additionalCover.spousebenefitamount_accmedicalexpenses = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_accmedicalexpenses = this.additionalCover.spousebenefitamount_accmedicalexpenses;
        this.additionalCover.spousepremium_accmedicalexpenses = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_accmedicalexpenses = this.additionalCover.spousebenefitamount_accmedicalexpenses;
      } else if (this.spousedetails[i].CoverCode == "RF") {
        this.additionalCover.spousebenefitamount_repatriationfuneralexpenses = this.spousedetails[
          i
        ].SumInsured;

        this.additionalCover.temp_spousebenefitamount_repatriationfuneralexpenses = this.additionalCover.spousebenefitamount_repatriationfuneralexpenses;
      } else if (this.spousedetails[i].CoverCode == "BB") {
        this.additionalCover.spousebenefitamount_brokenbones = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_brokenbones = this.additionalCover.spousebenefitamount_brokenbones;
        this.additionalCover.spousepremium_brokenbones = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_brokenbones = this.additionalCover.spousebenefitamount_brokenbones;
      } else if (this.spousedetails[i].CoverCode == "RBC") {
        this.additionalCover.spousebenefitamount_roadambulancecover = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_roadambulancecover = this.additionalCover.spousebenefitamount_roadambulancecover;
        this.additionalCover.spousepremium_roadambulancecover = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_roadambulancecover = this.additionalCover.spousebenefitamount_roadambulancecover;
      } else if (this.spousedetails[i].CoverCode == "AAC") {
        this.additionalCover.spousebenefitamount_airambulancecover = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_airambulancecover = this.additionalCover.spousebenefitamount_airambulancecover;
        this.additionalCover.spousepremium_airambulancecover = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_airambulancecover = this.additionalCover.spousebenefitamount_airambulancecover;
      } else if (this.spousedetails[i].CoverCode == "AS") {
        this.additionalCover.spousebenefitamount_adventuresportbenefit = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_adventuresportbenefit = this.additionalCover.spousebenefitamount_adventuresportbenefit;
        this.additionalCover.spousepremium_adventuresportbenefit = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_adventuresportbenefit = this.additionalCover.spousebenefitamount_adventuresportbenefit;
      } else if (this.spousedetails[i].CoverCode == "CP") {
        this.additionalCover.spousebenefitamount_chauffeurplanbenefit = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousemaxbenefitamount_chauffeurplanbenefit = this.additionalCover.spousebenefitamount_chauffeurplanbenefit;
        this.additionalCover.spousepremium_chauffeurplanbenefit = this.spousedetails[
          i
        ].Premium;
        this.additionalCover.temp_spousebenefitamount_chauffeurplanbenefit = this.additionalCover.spousebenefitamount_chauffeurplanbenefit;
      }
    }
  }
  calculateSonCovers(member) {
    this.sondetails = this.PremiumDetails.filter(function(son) {
      return son.Relationship == member;
    });

    for (let i = 0; i < this.sondetails.length; i++) {
      if (this.sondetails[i].CoverCode == "AD") {
        this.child1insureddetails.acc_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.acc_premium = this.sondetails[i].Premium;
        if (this.child1insureddetails.acc_maxinsured > 500000) {
          this.child1insureddetails.acc_maxinsured = 500000;
        }
        this.maxLimitChild1AccDeath = this.child1insureddetails.acc_maxinsured;




      } else if (this.sondetails[i].CoverCode == "PP") {
        this.child1insureddetails.ppd_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.ppd_premium = this.sondetails[i].Premium;
        if (this.child1insureddetails.ppd_maxinsured > 500000) {
          this.child1insureddetails.ppd_maxinsured = 500000;
        }
        this.maxLimitChild1PPD = this.child1insureddetails.ppd_maxinsured;


        this.child1insureddetails.temp_ppd_maxinsured = this.child1insureddetails.ppd_maxinsured;

      } else if (this.sondetails[i].CoverCode == "PT") {
        this.child1insureddetails.ptd_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.ptd_premium = this.sondetails[i].Premium;

        if (this.child1insureddetails.ptd_maxinsured > 500000) {
          this.child1insureddetails.ptd_maxinsured = 500000;
        }
        this.maxLimitChild1PTD = this.child1insureddetails.ptd_maxinsured;



        this.child1insureddetails.temp_ptd_maxinsured = this.child1insureddetails.ptd_maxinsured;



      } else if (this.sondetails[i].CoverCode == "TT") {
        this.child1insureddetails.ttd_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.ttd_premium = this.sondetails[i].Premium;
        if (this.child1insureddetails.ttd_maxinsured > 500000) {
          this.child1insureddetails.ttd_maxinsured = 500000;
        }
        this.maxLimitChild1TTD = this.child1insureddetails.ttd_maxinsured;

        this.child1insureddetails.temp_ttd_maxinsured = this.child1insureddetails.ttd_maxinsured;

      } else if (this.sondetails[i].CoverCode == "AA") {
        this.additionalCover.child1benefitamount_adaptationallowance = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_adaptationallowance = this.additionalCover.child1benefitamount_adaptationallowance;
        this.additionalCover.child1premium_adaptationallowance = this.sondetails[
          i
        ].Premium;

         this.additionalCover.temp_child1benefitamount_adaptationallowance =  this.additionalCover.child1benefitamount_adaptationallowance

        

      } else if (this.sondetails[i].CoverCode == "CS") {
        this.additionalCover.child1benefitamount_childeducationsupport = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_childeducationsupport = this.additionalCover.child1benefitamount_childeducationsupport;
        this.additionalCover.child2premium_childeducationsupport = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_childeducationsupport =  this.additionalCover.child1benefitamount_childeducationsupport

      } else if (this.sondetails[i].CoverCode == "FT") {
        this.additionalCover.child1benefitamount_familytransportallowance = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_familytransportallowance = this.additionalCover.child1benefitamount_familytransportallowance;
        this.additionalCover.child1premium_familytransportallowance = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_familytransportallowance = this.additionalCover.child1benefitamount_familytransportallowance

      } else if (this.sondetails[i].CoverCode == "HC") {
        this.additionalCover.child1benefitamount_hospitalcashallowance = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_hospitalcashallowance = this.additionalCover.child1benefitamount_hospitalcashallowance;
        this.additionalCover.child1premium_hospitalcashallowance = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_hospitalcashallowance =  this.additionalCover.child1benefitamount_hospitalcashallowance

      } else if (this.sondetails[i].CoverCode == "LP") {
        this.additionalCover.child1benefitamount_loanprotector = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_loanprotector = this.additionalCover.child1benefitamount_loanprotector;
        this.additionalCover.child1premium_loanprotector = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_loanprotector = this.additionalCover.child1benefitamount_loanprotector

      } else if (this.sondetails[i].CoverCode == "LS") {
        this.additionalCover.child1benefitamount_lifesupportbenefit = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_lifesupportbenefit = this.additionalCover.child1benefitamount_lifesupportbenefit;
        this.additionalCover.child1premium_lifesupportbenefit = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_lifesupportbenefit =  this.additionalCover.child1benefitamount_lifesupportbenefit

      } else if (this.sondetails[i].CoverCode == "ME") {
        this.additionalCover.child1benefitamount_acchospitalisation = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_acchospitalisation = this.additionalCover.child1benefitamount_acchospitalisation;
        this.additionalCover.child1premium_acchospitalisation = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_acchospitalisation = this.additionalCover.child1benefitamount_acchospitalisation

      } else if (this.sondetails[i].CoverCode == "AM") {
        this.additionalCover.child1benefitamount_accmedicalexpenses = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_accmedicalexpenses = this.additionalCover.child1benefitamount_accmedicalexpenses;
        this.additionalCover.child1premium_accmedicalexpenses = this.sondetails[
          i
        ].Premium;
        this.additionalCover.temp_child1benefitamount_accmedicalexpenses = this.additionalCover.child1benefitamount_accmedicalexpenses
      } else if (this.sondetails[i].CoverCode == "RF") {
        this.additionalCover.child1benefitamount_repatriationfuneralexpenses = this.sondetails[
          i
        ].SumInsured;



      } else if (this.sondetails[i].CoverCode == "BB") {
        this.additionalCover.child1benefitamount_brokenbones = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_brokenbones = this.additionalCover.child1benefitamount_brokenbones;
        this.additionalCover.child1premium_brokenbones = this.sondetails[
          i
        ].Premium;



        this.additionalCover.temp_child1benefitamount_brokenbones  = this.additionalCover.child1benefitamount_brokenbones 


      } else if (this.sondetails[i].CoverCode == "RBC") {
        this.additionalCover.child1benefitamount_roadambulancecover = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_roadambulancecover = this.additionalCover.child1benefitamount_roadambulancecover;
        this.additionalCover.child1premium_roadambulancecover = this.sondetails[
          i
        ].Premium;


        this.additionalCover.temp_child1benefitamount_roadambulancecover =  this.additionalCover.child1benefitamount_roadambulancecover
      } else if (this.sondetails[i].CoverCode == "AAC") {
        this.additionalCover.child1benefitamount_airambulancecover = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_airambulancecover = this.additionalCover.child1benefitamount_airambulancecover;
        this.additionalCover.child1premium_airambulancecover = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_airambulancecover = this.additionalCover.child1benefitamount_airambulancecover


      } else if (this.sondetails[i].CoverCode == "AS") {
        this.additionalCover.child1benefitamount_adventuresportbenefit = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_adventuresportbenefit = this.additionalCover.child1benefitamount_adventuresportbenefit;
        this.additionalCover.child1premium_adventuresportbenefit = this.sondetails[
          i
        ].Premium;

        this.additionalCover.temp_child1benefitamount_adventuresportbenefit  =  this.additionalCover.child1benefitamount_adventuresportbenefit 
      } else if (this.sondetails[i].CoverCode == "CP") {
        this.additionalCover.child1benefitamount_chauffeurplanbenefit = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1maxbenefitamount_chauffeurplanbenefit = this.additionalCover.child1benefitamount_chauffeurplanbenefit;
        this.additionalCover.child1premium_chauffeurplanbenefit = this.sondetails[
          i
        ].Premium;
        this.additionalCover.temp_child1benefitamount_chauffeurplanbenefit = this.additionalCover.child1benefitamount_chauffeurplanbenefit
      }
    }
  }

  calculateDaughterCovers(member) {
    this.daughterdetails = this.PremiumDetails.filter(function(daughter) {
      return daughter.Relationship == member;
    });

    for (let i = 0; i < this.daughterdetails.length; i++) {
      if (this.daughterdetails[i].CoverCode == "AD") {
        this.child2insureddetails.acc_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.acc_premium = this.daughterdetails[i].Premium;
        if (this.child2insureddetails.acc_maxinsured > 500000) {
          this.child2insureddetails.acc_maxinsured = 500000;
        }
        this.maxLimitChild2AccDeath = this.child2insureddetails.acc_maxinsured;
      } else if (this.daughterdetails[i].CoverCode == "PP") {
        this.child2insureddetails.ppd_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.ppd_premium = this.daughterdetails[i].Premium;

        if (this.child2insureddetails.ppd_maxinsured > 500000) {
          this.child2insureddetails.ppd_maxinsured = 500000;
        }
        this.maxLimitChild2PPD = this.child2insureddetails.ppd_maxinsured;



        this.child2insureddetails.temp_ppd_maxinsured =  this.child2insureddetails.ppd_maxinsured
        
      } else if (this.daughterdetails[i].CoverCode == "PT") {
        this.child2insureddetails.ptd_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.ptd_premium = this.daughterdetails[i].Premium;
        if (this.child2insureddetails.ptd_maxinsured > 500000) {
          this.child2insureddetails.ptd_maxinsured = 500000;
        }
        this.maxLimitChild2PTD = this.child2insureddetails.ptd_maxinsured;


        this.child2insureddetails.temp_ptd_maxinsured = this.child2insureddetails.ptd_maxinsured



      } else if (this.daughterdetails[i].CoverCode == "TT") {
        this.child2insureddetails.ttd_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.ttd_premium = this.daughterdetails[i].Premium;

        if (this.child2insureddetails.ttd_maxinsured > 500000) {
          this.child2insureddetails.ttd_maxinsured = 500000;
        }
        this.maxLimitChild2TTD = this.child2insureddetails.ttd_maxinsured;


        this.child2insureddetails.temp_ttd_maxinsured  = this.child2insureddetails.ttd_maxinsured 

      } else if (this.daughterdetails[i].CoverCode == "AA") {
        this.additionalCover.child2benefitamount_adaptationallowance = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_adaptationallowance = this.additionalCover.child2benefitamount_adaptationallowance;
        this.additionalCover.child2premium_adaptationallowance = this.daughterdetails[
          i
        ].Premium;
        this.additionalCover.temp_child2benefitamount_adaptationallowance = this.additionalCover.child2benefitamount_adaptationallowance

      } else if (this.daughterdetails[i].CoverCode == "CS") {
        this.additionalCover.child2benefitamount_childeducationsupport = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_childeducationsupport = this.additionalCover.child2benefitamount_childeducationsupport;
        this.additionalCover.child2premium_childeducationsupport = this.daughterdetails[
          i
        ].Premium;

        this.additionalCover.temp_child2benefitamount_childeducationsupport = this.additionalCover.child2benefitamount_childeducationsupport

      } else if (this.daughterdetails[i].CoverCode == "FT") {
        this.additionalCover.child2benefitamount_familytransportallowance = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_familytransportallowance = this.additionalCover.child2benefitamount_familytransportallowance;
        this.additionalCover.child2premium_familytransportallowance = this.daughterdetails[
          i
        ].Premium;


        this.additionalCover.temp_child2benefitamount_familytransportallowance = this.additionalCover.child2benefitamount_familytransportallowance

      } else if (this.daughterdetails[i].CoverCode == "HC") {
        this.additionalCover.child2benefitamount_hospitalcashallowance = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_hospitalcashallowance = this.additionalCover.child2benefitamount_hospitalcashallowance;
        this.additionalCover.child2premium_hospitalcashallowance = this.daughterdetails[
          i
        ].Premium;


        this.additionalCover.temp_child2benefitamount_hospitalcashallowance  = this.additionalCover.child2benefitamount_hospitalcashallowance 
      } else if (this.daughterdetails[i].CoverCode == "LP") {
        this.additionalCover.child2benefitamount_loanprotector = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_loanprotector = this.additionalCover.child2benefitamount_loanprotector;
        this.additionalCover.child2premium_loanprotector = this.daughterdetails[
          i
        ].Premium;
        this.additionalCover.temp_child2benefitamount_loanprotector = this.additionalCover.child2benefitamount_loanprotector
      } else if (this.daughterdetails[i].CoverCode == "LS") {
        this.additionalCover.child2benefitamount_lifesupportbenefit = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_lifesupportbenefit = this.additionalCover.child2benefitamount_lifesupportbenefit;
        this.additionalCover.child2premium_lifesupportbenefit = this.daughterdetails[
          i
        ].Premium;

        this.additionalCover.temp_child2benefitamount_lifesupportbenefit =  this.additionalCover.child2benefitamount_lifesupportbenefit
      } else if (this.daughterdetails[i].CoverCode == "ME") {
        this.additionalCover.child2benefitamount_acchospitalisation = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_acchospitalisation = this.additionalCover.child2benefitamount_acchospitalisation;
        this.additionalCover.child2premium_acchospitalisation = this.daughterdetails[
          i
        ].Premium;


        this.additionalCover.temp_child2benefitamount_acchospitalisation = this.additionalCover.child2benefitamount_acchospitalisation
      } else if (this.daughterdetails[i].CoverCode == "AM") {
        this.additionalCover.child2benefitamount_accmedicalexpenses = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_accmedicalexpenses = this.additionalCover.child2benefitamount_accmedicalexpenses;
        this.additionalCover.child2premium_accmedicalexpenses = this.daughterdetails[
          i
        ].Premium;

        this.additionalCover.temp_child2benefitamount_accmedicalexpenses = this.additionalCover.child2benefitamount_accmedicalexpenses

      } else if (this.daughterdetails[i].CoverCode == "RF") {
        this.additionalCover.child2benefitamount_repatriationfuneralexpenses = this.daughterdetails[
          i
        ].SumInsured;
      } else if (this.daughterdetails[i].CoverCode == "BB") {
        this.additionalCover.child2benefitamount_brokenbones = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_brokenbones = this.additionalCover.child2benefitamount_brokenbones;
        this.additionalCover.child2premium_brokenbones = this.daughterdetails[
          i
        ].Premium;


        this.additionalCover.temp_child2benefitamount_brokenbones = this.additionalCover.child2benefitamount_brokenbones
      } else if (this.daughterdetails[i].CoverCode == "RBC") {
        this.additionalCover.child2benefitamount_roadambulancecover = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_roadambulancecover = this.additionalCover.child2benefitamount_roadambulancecover;
        this.additionalCover.child2premium_roadambulancecover = this.daughterdetails[
          i
        ].Premium;

        this.additionalCover.temp_child2benefitamount_roadambulancecover = this.additionalCover.child2benefitamount_roadambulancecover
      } else if (this.daughterdetails[i].CoverCode == "AAC") {
        this.additionalCover.child2benefitamount_airambulancecover = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_airambulancecover = this.additionalCover.child2benefitamount_airambulancecover;
        this.additionalCover.child2premium_airambulancecover = this.daughterdetails[
          i
        ].Premium;

        this.additionalCover.temp_child2benefitamount_airambulancecover =  this.additionalCover.child2benefitamount_airambulancecover
      } else if (this.daughterdetails[i].CoverCode == "AS") {
        this.additionalCover.child2benefitamount_adventuresportbenefit = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_adventuresportbenefit = this.additionalCover.child2benefitamount_adventuresportbenefit;
        this.additionalCover.child2premium_adventuresportbenefit = this.daughterdetails[
          i
        ].Premium;

        this.additionalCover.temp_child2benefitamount_adventuresportbenefit = this.additionalCover.child2benefitamount_adventuresportbenefit
      } else if (this.daughterdetails[i].CoverCode == "CP") {
        this.additionalCover.child2benefitamount_chauffeurplanbenefit = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2maxbenefitamount_chauffeurplanbenefit = this.additionalCover.child2benefitamount_chauffeurplanbenefit;
        this.additionalCover.child2premium_chauffeurplanbenefit = this.daughterdetails[
          i
        ].Premium;
        this.additionalCover.temp_child2benefitamount_chauffeurplanbenefit =  this.additionalCover.child2benefitamount_chauffeurplanbenefit
      }
    }
  }
  calculateMainCovers(member, i) {}
  onBackClick() {
    this.navCtrl.pop();
  }
  

  isValueChanged(ev) {
    this.hideNextBtn = true;
    this.hideProceedBtn = true;
    this.hideRecalculate = false;

    console.log("Value has been changed" + ev);
  }

  getCurrVal(member, stepper, ev) {
    eval("this." + stepper + "=" + ev);
  }

  recalculateClick() {
    this.presentLoadingDefault();
    //service call again
    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].title == "Self") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;
        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ttd_maxinsured;
          }
        }

        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.selfbenefitamount_repatriationfuneralexpenses;
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Spouse") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.spousebenefitamount_repatriationfuneralexpenses
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Child1") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.ttd_maxinsured;
          }
        }

        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.child1benefitamount_repatriationfuneralexpenses
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Child2") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.child2benefitamount_repatriationfuneralexpenses
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_lifesupportbenefit;
          }
        }
      }
    }

    let callRequest = this.appService.callServicePADetails(
      this.policyType,
      this.beneficiaryDetails
    );

    var sendData = this.appService.encryptData(
      JSON.stringify(callRequest),
      sessionStorage.TokenId
    );
    this.getServiceResponse({ request: sendData }, "");
    console.log("members: " + JSON.stringify(this.membersInsured));

    console.log(this.beneficiaryDetails);
  }
  recalculateClickOnProceed() {
   
    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].title == "Self") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;
        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.insureddetails.ttd_maxinsured;
          }
        }

        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.selfbenefitamount_repatriationfuneralexpenses
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.selfbenefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Spouse") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.spouseinsureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.spousebenefitamount_repatriationfuneralexpenses
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.spousebenefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Child1") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child1insureddetails.ttd_maxinsured;
          }
        }

        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.child1benefitamount_repatriationfuneralexpenses
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child1benefitamount_lifesupportbenefit;
          }
        }
      } else if (this.membersInsured[i].title == "Child2") {
        let Cover = this.beneficiaryDetails[i].PrimaryCover.Cover;
        let AdditonalCover = this.beneficiaryDetails[i].SecondaryCover.Cover;

        for (let j = 0; j < Cover.length; j++) {
          if (Cover[j].CoverCode == "AD") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.acc_maxinsured;
          } else if (Cover[j].CoverCode == "PP") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.ppd_maxinsured;
          } else if (Cover[j].CoverCode == "PT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.ptd_maxinsured;
          } else if (Cover[j].CoverCode == "TT") {
            this.beneficiaryDetails[i].PrimaryCover.Cover[
              j
            ].SumInsured = this.child2insureddetails.ttd_maxinsured;
          }
        }
        for (let j = 0; j < AdditonalCover.length; j++) {
          if (AdditonalCover[j].CoverCode == "RF") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[j].SumInsured = this.additionalCover.child2benefitamount_repatriationfuneralexpenses
          } else if (AdditonalCover[j].CoverCode == "HC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_hospitalcashallowance;
          } else if (AdditonalCover[j].CoverCode == "AA") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_adaptationallowance;
          } else if (AdditonalCover[j].CoverCode == "FT") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_familytransportallowance;
          } else if (AdditonalCover[j].CoverCode == "LP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_loanprotector;
          } else if (AdditonalCover[j].CoverCode == "ME") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_acchospitalisation;
          } else if (AdditonalCover[j].CoverCode == "AM") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_accmedicalexpenses;
          } else if (AdditonalCover[j].CoverCode == "CS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_childeducationsupport;
          } else if (AdditonalCover[j].CoverCode == "BB") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_brokenbones;
          } else if (AdditonalCover[j].CoverCode == "RBC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_roadambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AAC") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_airambulancecover;
          } else if (AdditonalCover[j].CoverCode == "AS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_adventuresportbenefit;
          } else if (AdditonalCover[j].CoverCode == "CP") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_chauffeurplanbenefit;
          } else if (AdditonalCover[j].CoverCode == "LS") {
            this.beneficiaryDetails[i].SecondaryCover.Cover[
              j
            ].SumInsured = this.additionalCover.child2benefitamount_lifesupportbenefit;
          }
        }
      }
    }

    let callRequest = this.appService.callServicePADetails(
      this.policyType,
      this.beneficiaryDetails
    );

    var sendData = this.appService.encryptData(
      JSON.stringify(callRequest),
      sessionStorage.TokenId
    );
    this.getServiceResponseProceed({ request: sendData }, "");
    
  }

  getServiceResponseProceed(serviceData, IndexNumber) {
    let headerString = this.appService.getBase64string(
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    console.log("header: " + JSON.stringify(headerString));

    this.appService
      .callService("QuotePurchase.svc/ENQ_Policy", serviceData, headerString)
      .subscribe(ENQ_Policy => {
        console.log(JSON.stringify(ENQ_Policy));
       // this.loading.dismiss();
        if (ENQ_Policy.ENQ_PolicyResult.ReturnCode == "0") {
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;


          console.log("request token id",  sessionStorage.TokenId)


          this.ENQ_PolicyResponse = ENQ_Policy.ENQ_PolicyResult.Data;
          this.ReferenceNo = this.ENQ_PolicyResponse.RefrenceNo;
          this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
          this.UID = this.ENQ_PolicyResponse.PurchaseResponse[0].UID;
          this.response = this.ENQ_PolicyResponse.PurchaseResponse[0].Result_pa;
          this.PremiumDetails = this.response.Root.Policy.NewDataSet.PremiumDeatils;
        

          var eProposalData = {
            quotationID: this.QuotationID,
            UID: this.UID
          };
          console.log("Raw: " + JSON.stringify(eProposalData));

          var sendData = this.appService.encryptData(
            JSON.stringify(eProposalData),
            sessionStorage.TokenId + "~" + sessionStorage.UserId
          );
    
          this.sendEPremumBreakupData({ request: sendData });

         
        } else if (ENQ_Policy.ENQ_PolicyResult.ReturnCode == "807") {
          this.loading.dismiss();
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
          this.navCtrl.push(LoginPage);
        } else if (ENQ_Policy.ENQ_PolicyResult.ReturnCode == "500") {
          this.loading.dismiss();
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.showToast(
            "Oops! There seems to be a technical issue at our end. Please try again later."
          );
          this.navCtrl.push(LoginPage);
        } else {
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          console.log("Service resp: " + JSON.stringify(ENQ_Policy));
          this.loading.dismiss();
          //this.appService.showloading.dismiss();
          this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
        }
      });
  }



  getServiceResponse(serviceData, IndexNumber) {
    let headerString = this.appService.getBase64string(
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    console.log("header: " + JSON.stringify(headerString));

    this.appService
      .callService("QuotePurchase.svc/ENQ_Policy", serviceData, headerString)
      .subscribe(ENQ_Policy => {
        console.log(JSON.stringify(ENQ_Policy));
        this.loading.dismiss();
        if (ENQ_Policy.ENQ_PolicyResult.ReturnCode == "0") {
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.ENQ_PolicyResponse = ENQ_Policy.ENQ_PolicyResult.Data;
          this.ReferenceNo = this.ENQ_PolicyResponse.RefrenceNo;
          this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
          this.UID = this.ENQ_PolicyResponse.PurchaseResponse[0].UID;
          this.response = this.ENQ_PolicyResponse.PurchaseResponse[0].Result_pa;
          this.PremiumDetails = this.response.Root.Policy.NewDataSet.PremiumDeatils;

          
          console.log("response1" + this.PremiumDetails);
          for (let i = 0; i < this.membersInsured.length; i++) {
            if (this.membersInsured[i].detailsSaved == true) {
              let member = this.membersInsured[i];

              if (member.title == "Self") {
                this.calculateSelfCovers(member.code);
              } else if (member.title == "Spouse") {
                this.calculateSpouseCovers(member.code);
              } else if (member.title == "Child1") {
                this.selfchildEdusupport = false

                if (member.code == "Son") {
                  this.calculateSonCovers("SON");
                }
                if (member.code == "Daughter") {
                  this.calculateSonCovers("DAUGHTER");
                }
              } else if (member.title == "Child2") {
                this.selfchildEdusupport = false

                if (member.code == "Son") {
                  this.calculateDaughterCovers("SON");
                }
                if (member.code == "Daughter") {
                  this.calculateDaughterCovers("DAUGHTER");
                }
              }

              this.lastActiveMember = member.title;
            }
          }

          console.log("this.ActiveMember",this.ActiveMember)
          console.log("this.lastActiveMember",this.lastActiveMember)


          if (this.lastActiveMember == "Self" && this.policyType == "PATI") {
            this.hideProceedBtn = false;
            this.hideRecalculate = true;
            this.hideNextBtn = true;
          } else if(this.lastActiveMember == this.ActiveMember){
            this.hideNextBtn = true;
            this.hideRecalculate = true;
            this.hideProceedBtn = false;
          }
          else{

            this.hideNextBtn = false;
            this.hideRecalculate = true;
            this.hideProceedBtn = true;
          }
        } else if (ENQ_Policy.ENQ_PolicyResult.ReturnCode == "807") {
          this.loading.dismiss();
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
          this.navCtrl.push(LoginPage);
        } else if (ENQ_Policy.ENQ_PolicyResult.ReturnCode == "500") {
          this.loading.dismiss();
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          this.showToast(
            "Oops! There seems to be a technical issue at our end. Please try again later."
          );
          this.navCtrl.push(LoginPage);
        } else {
          sessionStorage.TokenId =
            ENQ_Policy.ENQ_PolicyResult.UserToken.TokenId;
          console.log("Service resp: " + JSON.stringify(ENQ_Policy));
          this.loading.dismiss();
          //this.appService.showloading.dismiss();
          this.showToast(ENQ_Policy.ENQ_PolicyResult.ReturnMsg);
        }
      });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
  }

  // clickTabSegment(title,index){

  //   console.log("member",title)

  //   for (let i = 1; i < this.membersInsured.length; i++) {
  //     if (title == "Spouse") {
  //       this.ActiveMember = "Self"
  //     }
  //     else if (title == "Child1") {
  //       this.ActiveMember = "Spouse"
  //     }
  //     else if (title == "Child2") {
  //       this.ActiveMember = "Child1"
  //     }
  //     else{
  //       this.ActiveMember ="Self"
  //     }
  //   }
  //   this.nextPageClick()

  // }
  nextPageClick() {
    if (this.ActiveMember == "Self") {
      // if (Number(this.insureddetails.acc_maxinsured) < 50000) {
      //   this.showToast("You can not select Accidental death below Rs 50000");
      //   return;
      // } else if (Number(this.insureddetails.ppd_maxinsured) < 50000) {
      //   this.showToast(
      //     "You can not select Permanent partial disability below Rs 50000"
      //   );
      //   return;
      // } else if (Number(this.insureddetails.ptd_maxinsured) < 50000) {
      //   this.showToast(
      //     "You can not select Permanent total disability below Rs 50000"
      //   );
      //   return;
      // } else if (Number(this.insureddetails.ttd_maxinsured) < 10000) {
      //   this.showToast(
      //     "You can not select Temporary total disability below Rs 10000"
      //   );
      //   return;
      // } else {
        for (let i = 1; i < this.membersInsured.length; i++) {
          if (this.membersInsured[i].title == "Spouse") {
            this.clickTab(this.membersInsured[i].title, i);
            this.ActiveMember = "Spouse";
            this.pet = "Spouse";
            return;
          } else if (this.membersInsured[i].title == "Child1") {
            this.clickTab(this.membersInsured[i].title, i);
            this.ActiveMember = "Child1";
            this.pet = "Child1";
            return;
          } else if (this.membersInsured[i].title == "Child2") {
            this.clickTab(this.membersInsured[i].title, i);
            this.ActiveMember = "Child2";
            this.pet = "Child2";
            return;
          }
        }
     // }
    } else if (this.ActiveMember == "Spouse") {
      for (let i = 2; i < this.membersInsured.length; i++) {
        if (this.membersInsured[i].title == "Child1") {
          this.clickTab(this.membersInsured[i].title, i);
          this.ActiveMember = "Child1";
          this.pet = "Child1";
          return;
        } else if (this.membersInsured[i].title == "Child2") {
          this.clickTab(this.membersInsured[i].title, i);
          this.ActiveMember = "Child2";
          this.pet = "Child2";
          return;
        }
      }
    } else if (this.ActiveMember == "Child1") {
      for (let i = 3; i < this.membersInsured.length; i++) {
        if (this.membersInsured[i].title == "Child1") {
          this.clickTab(this.membersInsured[i].title, i);
          this.ActiveMember = "Child1";
          this.pet = "Child1";
          return;
        } else if (this.membersInsured[i].title == "Child2") {
          this.clickTab(this.membersInsured[i].title, i);
          this.ActiveMember = "Child2";
          this.pet = "Child2";
          return;
        }
      }
    }
  }
  proceedtoNextPage() {
    // if (Number(this.insureddetails.acc_maxinsured) < 50000) {
    //   this.showToast("You can not select Accidental death below Rs 50000");
    //   return;
    // } else if (Number(this.insureddetails.ppd_maxinsured) < 50000) {
    //   this.showToast(
    //     "You can not select Permanent partial disability below Rs 50000"
    //   );
    //   return;
    // } else if (Number(this.insureddetails.ptd_maxinsured) < 50000) {
    //   this.showToast(
    //     "You can not select Permanent total disability below Rs 50000"
    //   );
    //   return;
    // } else if (Number(this.insureddetails.ttd_maxinsured) < 10000) {
    //   this.showToast(
    //     "You can not select Temporary total disability below Rs 10000"
    //   );
    //   return;
    // } else {
      this.selfMainCoversPremium = 0;
      this.spouseMainCoversPremium = 0;
      this.child1MainCoversPremium = 0;
      this.child2MainCoversPremium = 0;

      this.selfAddCoversPremium = 0;
      this.spouseAddCoversPremium = 0;
      this.child1AddCoversPremium = 0;
      this.child2AddCoversPremium = 0;

      /////main Covers

      this.selfCoversArray = [];
      this.spouseCoversArray = [];
      this.child1CoversArray = [];
      this.child2CoversArray = [];

      this.selfMainCoversPremium =
        Number(this.selfMainCoversPremium) +
        Number(this.insureddetails.acc_premium);

      if (this.selfpartialDis == true) {
        console.log("IF");
        console.log("x" + this.insureddetails.ppd_premium);

        this.selfMainCoversPremium =
          Number(this.selfMainCoversPremium) +
          Number(this.insureddetails.ppd_premium);

        this.selfCoversArray.push({
          selfpartialDis: this.selfpartialDis
        });
      }else{
      
      this.insureddetails.ppd_maxinsured = 0
        
      }
      


      if (this.selftotalDis == true) {
        console.log("IF");
        console.log("x" + this.insureddetails.ptd_premium);
        this.selfMainCoversPremium =
          Number(this.selfMainCoversPremium) +
          Number(this.insureddetails.ptd_premium);

        this.selfCoversArray.push({
          selftotalDis: this.selftotalDis
        });
      }
      else{

        this.insureddetails.ptd_maxinsured = 0;
      }

      if (this.selftempDis == true) {
        console.log("IF");
        this.selfMainCoversPremium =
          Number(this.selfMainCoversPremium) +
          Number(this.insureddetails.ttd_premium);

        this.selfCoversArray.push({
          selftempDis: this.selftempDis
        });
      }else{

        this.insureddetails.ttd_maxinsured = 0;
      }

      //// self additional covers

      if (this.adaption_allowance == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_adaptationallowance);

        this.selfCoversArray.push({
          adaption_allowance: this.adaption_allowance
        });
      }else{

      this.additionalCover.selfbenefitamount_adaptationallowance = 0
      }

      if (this.acc_hospitalisation == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_acchospitalisation);

        this.selfCoversArray.push({
          acc_hospitalisation: this.acc_hospitalisation
        });
      }else{

        this.additionalCover.selfbenefitamount_acchospitalisation = 0;
      }

      if (this.acc_medical_expenses == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_accmedicalexpenses);

        this.selfCoversArray.push({
          acc_medical_expenses: this.acc_medical_expenses
        });
      }
      else{

        this.additionalCover.selfbenefitamount_accmedicalexpenses = 0;
      }

      if (this.self_child_edu_support == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_childeducationsupport);

        this.selfCoversArray.push({
          self_child_edu_support: this.self_child_edu_support
        });
      }else{

      this.additionalCover.selfbenefitamount_childeducationsupport = 0;

      }

      if (this.self_family_trans_allowance == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_familytransportallowance);

        this.selfCoversArray.push({
          self_family_trans_allowance: this.self_family_trans_allowance
        });
      }
      else{

        this.additionalCover.selfbenefitamount_familytransportallowance = 0;
      }

      if (this.self_hospital_cash_allownce == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_hospitalcashallowance);

        this.selfCoversArray.push({
          self_hospital_cash_allownce: this.self_hospital_cash_allownce
        });
      }else{

        this.additionalCover.selfbenefitamount_hospitalcashallowance = 0;
      }

      if (this.self_life_support_benefit == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_lifesupportbenefit);

        this.selfCoversArray.push({
          self_life_support_benefit: this.self_life_support_benefit
        });
      }
      else{

        this.additionalCover.selfbenefitamount_lifesupportbenefit = 0;
      }





      if (this.self_broken_bones == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_brokenbones);

        this.selfCoversArray.push({
          self_broken_bones: this.self_broken_bones
        });
      }else{

        this.additionalCover.selfbenefitamount_brokenbones = 0;
      }




      if (this.self_road_ambulance_cover == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_roadambulancecover);
        this.selfCoversArray.push({
          self_road_ambulance_cover: this.self_road_ambulance_cover
        });
      }else{

        this.additionalCover.selfbenefitamount_roadambulancecover = 0;
      }



      if (this.self_air_ambulance_cover == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_airambulancecover);
        this.selfCoversArray.push({
          self_air_ambulance_cover: this.self_air_ambulance_cover
        });
      }else{

        this.additionalCover.selfbenefitamount_airambulancecover = 0
      }




      if (this.self_adventure_sports_benefit == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_adventuresportbenefit);

        this.selfCoversArray.push({
          self_adventure_sports_benefit: this.self_adventure_sports_benefit
        });
      }else{

        this.additionalCover.selfbenefitamount_adventuresportbenefit = 0
      }





      if (this.self_chauffeur_plan_benefit == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_chauffeurplanbenefit);

        this.selfCoversArray.push({
          self_chauffeur_plan_benefit: this.self_chauffeur_plan_benefit
        });
      }else{
        this.additionalCover.selfbenefitamount_chauffeurplanbenefit = 0

      }




      if (this.self_loan_protector == true) {
        this.selfAddCoversPremium =
          Number(this.selfAddCoversPremium) +
          Number(this.additionalCover.selfpremium_loanprotector);

        this.selfCoversArray.push({
          self_loan_protector: this.self_loan_protector
        });
      }
      else{
        this.additionalCover.selfbenefitamount_loanprotector = 0

      }

      console.log("this.selfCoversArray" + this.selfCoversArray);

      if (this.SelfOccupationCode == "SPOT") {
        this.selfAddCoversPremium = Number(this.selfAddCoversPremium) * 2;
      }

      ///////calculate self
      //console.log("Self calculation" + this.selfTotatPremium)

      /////// main covers spouse  //////////////

      this.spouseMainCoversPremium =
        Number(this.spouseMainCoversPremium) +
        Number(this.spouseinsureddetails.acc_premium);

      if (this.spousepartialDis == true) {
        console.log("IF");
        this.spouseMainCoversPremium =
          Number(this.spouseMainCoversPremium) +
          Number(this.spouseinsureddetails.ppd_premium);

        this.spouseCoversArray.push({
          spousepartialDis: this.spousepartialDis
        });
      }else{


        this.spouseinsureddetails.ppd_maxinsured = 0;
      }

      if (this.spousetotalDis == true) {
        console.log("IF");
        this.spouseMainCoversPremium =
          Number(this.spouseMainCoversPremium) +
          Number(this.spouseinsureddetails.ptd_premium);

        this.spouseCoversArray.push({
          spousetotalDis: this.spousetotalDis
        });
      }else{

        this.spouseinsureddetails.ptd_maxinsured = 0
      }

      if (this.spousetempDis == true) {
        console.log("IF");
        this.spouseMainCoversPremium =
          Number(this.spouseMainCoversPremium) +
          Number(this.spouseinsureddetails.ttd_premium);

        this.spouseCoversArray.push({
          spousetempDis: this.spousetempDis
        });
      }
      else{


        this.spouseinsureddetails.ttd_maxinsured = 0;
      }

      /// SPOUSE ADDITIONAL COVERS

      if (this.spouse_adaption_allowance == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_adaptationallowance);

        this.spouseCoversArray.push({
          spouse_adaption_allowance: this.spouse_adaption_allowance
        });
      }else{

        this.additionalCover.spousebenefitamount_adaptationallowance = 0;
      }

      if (this.spouse_acc_hospitalisation == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_acchospitalisation);

        this.spouseCoversArray.push({
          spouse_acc_hospitalisation: this.spouse_acc_hospitalisation
        });
      }else{


        this.additionalCover.spousebenefitamount_acchospitalisation = 0;
      }

      if (this.spouse_acc_medical_expenses == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_accmedicalexpenses);

        this.spouseCoversArray.push({
          spouse_acc_medical_expenses: this.spouse_acc_medical_expenses
        });
      }else{

        this.additionalCover.spousebenefitamount_accmedicalexpenses = 0
      }

      if (this.spouse_child_edu_support == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_childeducationsupport);

        this.spouseCoversArray.push({
          spouse_child_edu_support: this.spouse_child_edu_support
        });
      }else{

        this.additionalCover.spousebenefitamount_childeducationsupport = 0;
      }

      if (this.spouse_family_trans_allowance == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_familytransportallowance);

        this.spouseCoversArray.push({
          spouse_family_trans_allowance: this.spouse_family_trans_allowance
        });
      }else{

        this.additionalCover.spousebenefitamount_familytransportallowance = 0;
      }

      if (this.spouse_hospital_cash_allownce == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_hospitalcashallowance);

        this.spouseCoversArray.push({
          spouse_hospital_cash_allownce: this.spouse_hospital_cash_allownce
        });
      }else{


        this.additionalCover.spousebenefitamount_hospitalcashallowance = 0;
      }

      if (this.spouse_life_support_benefit == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_lifesupportbenefit);

        this.spouseCoversArray.push({
          spouse_life_support_benefit: this.spouse_life_support_benefit
        });
      }else{

        this.additionalCover.spousebenefitamount_lifesupportbenefit = 0;
      }

      if (this.spouse_broken_bones == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_brokenbones);

        this.spouseCoversArray.push({
          spouse_broken_bones: this.spouse_broken_bones
        });
      }
      else{

      this.additionalCover.spousebenefitamount_brokenbones = 0;

      }



      if (this.spouse_road_ambulance_cover == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_roadambulancecover);

        this.spouseCoversArray.push({
          spouse_road_ambulance_cover: this.spouse_road_ambulance_cover
        });
      }else{

        this.additionalCover.spousebenefitamount_roadambulancecover = 0;
      }

      if (this.spouse_air_ambulance_cover == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_airambulancecover);

        this.spouseCoversArray.push({
          spouse_air_ambulance_cover: this.spouse_air_ambulance_cover
        });
      }else{

        this.additionalCover.spousebenefitamount_airambulancecover = 0;
      }

      if (this.spouse_adventure_sports_benefit == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_adventuresportbenefit);

        this.spouseCoversArray.push({
          spouse_adventure_sports_benefit: this.spouse_adventure_sports_benefit
        });
      }else{

        this.additionalCover.spousebenefitamount_adventuresportbenefit = 0;
      }

      if (this.spouse_chauffeur_plan_benefit == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_chauffeurplanbenefit);

        this.spouseCoversArray.push({
          spouse_chauffeur_plan_benefit: this.spouse_chauffeur_plan_benefit
        });
      }else{

        this.additionalCover.spousebenefitamount_chauffeurplanbenefit = 0
      }

      if (this.spouse_loan_protector == true) {
        this.spouseAddCoversPremium =
          Number(this.spouseAddCoversPremium) +
          Number(this.additionalCover.spousepremium_loanprotector);

        this.spouseCoversArray.push({
          spouse_loan_protector: this.spouse_loan_protector
        });
      }else{
        this.additionalCover.spousebenefitamount_loanprotector = 0;
      }

      if (this.SpouseOccupationCode == "SPOT") {
        this.spouseAddCoversPremium = Number(this.spouseAddCoversPremium) * 2;
      }

      ///////calculate spouse
      // console.log("Spouse calculation" +  this.spouseTotalPremium)

      ////// child1 main covers premium //////

      this.child1MainCoversPremium =
        Number(this.child1MainCoversPremium) +
        Number(this.child1insureddetails.acc_premium);

      if (this.child1partialDis == true) {
        console.log("IF");
        this.child1MainCoversPremium =
          Number(this.child1MainCoversPremium) +
          Number(this.child1insureddetails.ppd_premium);

        this.child1CoversArray.push({
          child1partialDis: this.child1partialDis
        });
      }else{

        this.child1insureddetails.ppd_maxinsured = 0;
      }

      if (this.child1totalDis == true) {
        console.log("IF");
        this.child1MainCoversPremium =
          Number(this.child1MainCoversPremium) +
          Number(this.child1insureddetails.ptd_premium);
        this.child1CoversArray.push({
          child1totalDis: this.child1totalDis
        });
      }else{
        this.child1insureddetails.ptd_maxinsured = 0;
      }

      //// child1 additional covers premim ///

      if (this.child1_adaption_allowance == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_adaptationallowance);
        this.child1CoversArray.push({
          child1_adaption_allowance: this.child1_adaption_allowance
        });
      }else{

        this.additionalCover.child1benefitamount_adaptationallowance = 0;
      }

      if (this.child1_acc_hospitalisation == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_acchospitalisation);
        this.child1CoversArray.push({
          child1_acc_hospitalisation: this.child1_acc_hospitalisation
        });
      }else{
        this.additionalCover.child1benefitamount_acchospitalisation = 0;
      }

      if (this.child1_acc_medical_expenses == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_accmedicalexpenses);
        this.child1CoversArray.push({
          child1_acc_medical_expenses: this.child1_acc_medical_expenses
        });
      }else{
        this.additionalCover.child1benefitamount_accmedicalexpenses = 0;
      }

      if (this.child1_edu_support == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_childeducationsupport);
        this.child1CoversArray.push({
          child1_edu_support: this.child1_edu_support
        });
      }else{

        this.additionalCover.child1benefitamount_childeducationsupport = 0;
      }

      if (this.child1_family_trans_allowance == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_familytransportallowance);
        this.child1CoversArray.push({
          child1_family_trans_allowance: this.child1_family_trans_allowance
        });
      }else{

        this.additionalCover.child1benefitamount_familytransportallowance = 0;
      }

      if (this.child1_hospital_cash_allownce == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_hospitalcashallowance);
        this.child1CoversArray.push({
          child1_hospital_cash_allownce: this.child1_hospital_cash_allownce
        });
      }else{
        this.additionalCover.child1benefitamount_hospitalcashallowance = 0;
      }

      if (this.child1_life_support_benefit == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_lifesupportbenefit);

        this.child1CoversArray.push({
          child1_life_support_benefit: this.child1_life_support_benefit
        });
      }else{
        this.additionalCover.child1benefitamount_lifesupportbenefit = 0;
      }

      if (this.child1_broken_bones == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_brokenbones);

        this.child1CoversArray.push({
          child1_broken_bones: this.child1_broken_bones
        });
      }else{
        this.additionalCover.child1benefitamount_brokenbones = 0;
      }

      if (this.child1_road_ambulance_cover == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_roadambulancecover);

        this.child1CoversArray.push({
          child1_road_ambulance_cover: this.child1_road_ambulance_cover
        });
      }else{
      this.additionalCover.child1benefitamount_roadambulancecover = 0;
      }


      if (this.child1_air_ambulance_cover == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_airambulancecover);

        this.child1CoversArray.push({
          child1_air_ambulance_cover: this.child1_air_ambulance_cover
        });
      }else{

        this.additionalCover.child1benefitamount_airambulancecover = 0;
      }


      if (this.child1_adventure_sports_benefit == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_adventuresportbenefit);

        this.child1CoversArray.push({
          child1_adventure_sports_benefit: this.child1_adventure_sports_benefit
        });
      }else{

        this.additionalCover.child1benefitamount_adventuresportbenefit = 0;
      }




      if (this.child1_chauffeur_plan_benefit == true) {
        this.child1AddCoversPremium =
          Number(this.child1AddCoversPremium) +
          Number(this.additionalCover.child1premium_chauffeurplanbenefit);

        this.child1CoversArray.push({
          child1_chauffeur_plan_benefit: this.child1_chauffeur_plan_benefit
        });
      }else{

        this.additionalCover.child1benefitamount_chauffeurplanbenefit = 0;

      }

      ///////calculate child1

      /// child2 main cover premium

      this.child2MainCoversPremium =
        Number(this.child2MainCoversPremium) +
        Number(this.child2insureddetails.acc_premium);

      if (this.child2partialDis == true) {
        console.log("IF");
        this.child2MainCoversPremium =
          Number(this.child2MainCoversPremium) +
          Number(this.child2insureddetails.ppd_premium);

        this.child2CoversArray.push({
          child2partialDis: this.child2partialDis
        });
      }else{
        this.child2insureddetails.ppd_maxinsured = 0
      }


      if (this.child2totalDis == true) {
        console.log("IF");
        this.child2MainCoversPremium =
          Number(this.child2MainCoversPremium) +
          Number(this.child2insureddetails.ptd_premium);

        this.child2CoversArray.push({
          child2totalDis: this.child2totalDis
        });
      }else{
        this.child2insureddetails.ptd_maxinsured = 0
      }

      //// child2 additional covers premium //

      if (this.child2_adaption_allowance == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_adaptationallowance);

        this.child2CoversArray.push({
          child2_adaption_allowance: this.child2_adaption_allowance
        });
      }
      else{

        this.additionalCover.child2benefitamount_adaptationallowance = 0
      }

      if (this.child2_acc_hospitalisation == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_acchospitalisation);

        this.child2CoversArray.push({
          child2_acc_hospitalisation: this.child2_acc_hospitalisation
        });
      }else{

        this.additionalCover.child2benefitamount_acchospitalisation = 0;
      }

      if (this.child2_acc_medical_expenses == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_accmedicalexpenses);

        this.child2CoversArray.push({
          child2_acc_medical_expenses: this.child2_acc_medical_expenses
        });
      }else{

        this.additionalCover.child2benefitamount_accmedicalexpenses = 0;
      }

      if (this.child2_edu_support == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_childeducationsupport);

        this.child2CoversArray.push({
          child2_edu_support: this.child2_edu_support
        });
      }else{

        this.additionalCover.child2benefitamount_childeducationsupport = 0;
      }

      if (this.child2_family_trans_allowance == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_familytransportallowance);

        this.child2CoversArray.push({
          child2_family_trans_allowance: this.child2_family_trans_allowance
        });
      }else{
        this,this.additionalCover.child2benefitamount_familytransportallowance = 0;
      }

      if (this.child2_hospital_cash_allownce == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_hospitalcashallowance);

        this.child2CoversArray.push({
          child2_hospital_cash_allownce: this.child2_hospital_cash_allownce
        });
      }else{

        this.additionalCover.child2benefitamount_hospitalcashallowance = 0;
      }

      if (this.child2_life_support_benefit == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_lifesupportbenefit);

        this.child2CoversArray.push({
          child2_life_support_benefit: this.child2_life_support_benefit
        });
      }else{

        this.additionalCover.child2benefitamount_lifesupportbenefit = 0;
      }

      if (this.child2_broken_bones == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_brokenbones);

        this.child2CoversArray.push({
          child2_broken_bones: this.child2_broken_bones
        });
      }
      else{

        this.additionalCover.child2benefitamount_brokenbones = 0;

      }
      if (this.child2_road_ambulance_cover == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_roadambulancecover);

        this.child2CoversArray.push({
          child2_road_ambulance_cover: this.child2_road_ambulance_cover
        });
      }else{

        this.additionalCover.child2benefitamount_roadambulancecover = 0;
      }



      if (this.child2_air_ambulance_cover == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_airambulancecover);

        this.child2CoversArray.push({
          child2_air_ambulance_cover: this.child2_air_ambulance_cover
        });
      }else{


        this.additionalCover.child2benefitamount_airambulancecover = 0;
      }

 


      if (this.child2_adventure_sports_benefit == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_adventuresportbenefit);

        this.child2CoversArray.push({
          child2_adventure_sports_benefit: this.child2_adventure_sports_benefit
        });
      }else{
        this.additionalCover.child2benefitamount_adventuresportbenefit = 0;

      }





      if (this.child2_chauffeur_plan_benefit == true) {
        this.child2AddCoversPremium =
          Number(this.child2AddCoversPremium) +
          Number(this.additionalCover.child2premium_chauffeurplanbenefit);

        this.child2CoversArray.push({
          child2_chauffeur_plan_benefit: this.child2_chauffeur_plan_benefit
        });
      }else{

        this.additionalCover.child2benefitamount_chauffeurplanbenefit = 0;
      }

      ///////calculate child2
      //console.log("child2 calculation" + this.child2TotalPremium)


    this.recalculateClickOnProceed()


      
    // }
  }
  sendEPremumBreakupData(serviceData) {
    console.log("request token id",  sessionStorage.TokenId + "~" + sessionStorage.UserId)
    let headerString = this.appService.getBase64string(
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );


   
    this.appService
      .callService(
        "QuotePurchase.svc/GetPremiumBreakup_PA",
        serviceData,
        headerString
      )
      .subscribe(
        response => {
          var resp = response.GetPremiumBreakup_PAResult;
          console.log(JSON.stringify(response));
          if (resp && resp.ReturnCode == "0") {
            sessionStorage.TokenId = resp.UserToken.TokenId;
            sessionStorage.ben = JSON.stringify(this.beneficiaryDetails);
            let PremiumBreakup = resp.Data;
            PremiumBreakup = PremiumBreakup.PremiumBreakup;
            this.navCtrl.push(BuyPremiumlistviewPage, {
              PremiumBreakup: PremiumBreakup,
              PolicyType: this.policyType,
              ENQ_PolicyResponse: this.ENQ_PolicyResponse,
              membersInsured: this.membersInsured,
              ben: this.beneficiaryDetails,
              ReferenceNo: this.ReferenceNo,
              selfCoversArray: this.selfCoversArray,
              spouseCoversArray: this.spouseCoversArray,
              child1CoversArray: this.child1CoversArray,
              child2CoversArray: this.child2CoversArray,
              selfMainCoversPremium: this.selfMainCoversPremium,
              spouseMainCoversPremium: this.spouseMainCoversPremium,
              child1MainCoversPremium: this.child1MainCoversPremium,
              child2MainCoversPremium: this.child2MainCoversPremium,
              selfAddCoversPremium: this.selfAddCoversPremium,
              spouseAddCoversPremium: this.spouseAddCoversPremium,
              child1AddCoversPremium: this.child1AddCoversPremium,
              child2AddCoversPremium: this.child2AddCoversPremium
            });
          } else if (resp && resp.ReturnCode == "807") {
            sessionStorage.TokenId = resp.UserToken.TokenId;
            this.showToast(resp.ReturnMsg);
            this.navCtrl.push(LoginPage);
          } else if (resp.ReturnCode == "500") {
            this.loading.dismiss();
            sessionStorage.TokenId = resp.UserToken.TokenId;
            this.showToast(
              "Oops! There seems to be a technical issue at our end. Please try again later."
            );
            this.navCtrl.push(LoginPage);
          } else {
            this.loading.dismiss();
            sessionStorage.TokenId = resp.UserToken.TokenId;
            this.showToast(resp.ReturnMsg);
          }
        },
        err => {
          console.log(err);
        }
      );
  }
  addCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    console.log("test: " + parts);
    return parts.join(".");
  }

  perClick(num, member) {
    console.log("self : " + member);
    if (num == 1 && member == "Self") {
      if (this.partdisab == true) {
        this.insureddetails.ppd_maxinsured = this.insureddetails.temp_ppd_maxinsured;
      } else {
        this.insureddetails.ppd_maxinsured = 0;
      }

      this.partdisab = !this.partdisab;
      this.spousepartdisab = !this.spousepartdisab
      this.hideSpousePPD = !this.hideSpousePPD
      this.hideChild1PPD = !this.hideChild1PPD
      this.hideChild2PPD = !this.hideChild2PPD

      // this.child1partdisab = ! this.child1partdisab
      // this.child2partdisab = ! this.child2partdisab


    } else if (num == 2 && member == "Self") {
      if (this.permdisab == true) {
        this.insureddetails.ptd_maxinsured = this.insureddetails.temp_ptd_maxinsured;
      } else {
        this.insureddetails.ptd_maxinsured = 0;
      }

      this.permdisab = !this.permdisab;
      this.hideSpousePTD = !this.hideSpousePTD
      this.hideChild1PTD = !this.hideChild1PTD
      this.hideChild2PTD = !this.hideChild2PTD


    } else if (num == 3 && member == "Self") {
      if (this.tempdisab == true) {
        this.insureddetails.ttd_maxinsured = this.insureddetails.temp_ttd_maxinsured;
      } else {
        this.insureddetails.ttd_maxinsured = 0;
      }
      this.tempdisab = !this.tempdisab;
      this.spouseTTD = !this.spouseTTD
      this.spousetempdisab = !this.spousetempdisab

    } else if (num == 4 && member == "Self") {
      this.adaptationallownceCheckDiv = !this.adaptationallownceCheckDiv;

      console.log("adaption_allowance", this.adaption_allowance);

      if (this.adaption_allowance == true) {
        this.additionalCover.selfbenefitamount_adaptationallowance = this.additionalCover.temp_selfbenefitamount_adaptationallowance;
      } else {
        this.additionalCover.selfbenefitamount_adaptationallowance = 0;
      }

      this.spouseadaptionallowncedivSow = !this.spouseadaptionallowncedivSow;
      this.spouse_adaption_allowance = !this.spouse_adaption_allowance;
      this.spouseadaptationallownceCheckDiv = !this
        .spouseadaptationallownceCheckDiv;

      this.child1adaptationallownceCheckDiv = !this
        .child1adaptationallownceCheckDiv;
      this.child1_adaption_allowance = !this.child1_adaption_allowance;
      this.child1adaptionallowncedivSow = !this.child1adaptionallowncedivSow;

      this.child2adaptationallownceCheckDiv = !this
        .child2adaptationallownceCheckDiv;
      this.child2_adaption_allowance = !this.child2_adaption_allowance;
      this.child2adaptionallowncedivSow = !this.child2adaptionallowncedivSow;

      console.log("this.selfCoversArray" + this.selfCoversArray);
    } else if (num == 5 && member == "Self") {
      this.accidentalhospitalDivShow = !this.accidentalhospitalDivShow;

      if (this.acc_hospitalisation == true) {
        this.additionalCover.selfbenefitamount_acchospitalisation = this.additionalCover.temp_selfbenefitamount_acchospitalisation;
      } else {
        this.additionalCover.selfbenefitamount_acchospitalisation = 0;
      }

      this.spouseaccidentahospitalisation = !this
        .spouseaccidentahospitalisation;
      this.spouse_acc_hospitalisation = !this.spouse_acc_hospitalisation;
      this.spouseaccidentalhospitalDivShow = !this
        .spouseaccidentalhospitalDivShow;

      this.child1accidentahospitalisation = !this
        .child1accidentahospitalisation;
      this.child1_acc_hospitalisation = !this.child1_acc_hospitalisation;
      this.child1accidentalhospitalDivShow = !this
        .child1accidentalhospitalDivShow;

      this.child2accidentahospitalisation = !this
        .child2accidentahospitalisation;
      this.child2_acc_hospitalisation = !this.child2_acc_hospitalisation;
      this.child2accidentalhospitalDivShow = !this
        .child2accidentalhospitalDivShow;
    } else if (num == 6 && member == "Self") {
      this.accidentalmedicalExpDivShow = !this.accidentalmedicalExpDivShow;

      if (this.acc_medical_expenses == true) {
        this.additionalCover.selfbenefitamount_accmedicalexpenses = this.additionalCover.temp_selfbenefitamount_accmedicalexpenses;
      } else {
        this.additionalCover.selfbenefitamount_accmedicalexpenses = 0;
      }

      this.spouseaccidentalmedicalExp = !this.spouseaccidentalmedicalExp;
      this.spouse_acc_medical_expenses = !this.spouse_acc_medical_expenses;
      this.spouseaccidentalmedicalExpDivShow = !this
        .spouseaccidentalmedicalExpDivShow;

      this.child1accidentalmedicalExp = !this.child1accidentalmedicalExp;
      this.child1_acc_medical_expenses = !this.child1_acc_medical_expenses;
      this.child1accidentalmedicalExpDivShow = !this
        .child1accidentalmedicalExpDivShow;

      this.child2accidentalmedicalExp = !this.child2accidentalmedicalExp;
      this.child2_acc_medical_expenses = !this.child2_acc_medical_expenses;
      this.child2accidentalmedicalExpDivShow = !this
        .child2accidentalmedicalExpDivShow;
    } else if (num == 7 && member == "Self") {
      this.childeduSuppDivShow = !this.childeduSuppDivShow;

      if (this.self_child_edu_support == true) {
        this.additionalCover.selfbenefitamount_childeducationsupport = this.additionalCover.temp_selfbenefitamount_childeducationsupport;
      } else {
        this.additionalCover.selfbenefitamount_childeducationsupport = 0;
      }

      this.spousechildEdusupport = !this.spousechildEdusupport;
      this.spouse_child_edu_support = !this.spouse_child_edu_support;
      this.spousechildeduSuppDivShow = !this.spousechildeduSuppDivShow;
    } else if (num == 8 && member == "Self") {
      this.familyTransAllownceDivShow = !this.familyTransAllownceDivShow;

      if (this.self_family_trans_allowance == true) {
        this.additionalCover.selfbenefitamount_familytransportallowance = this.additionalCover.temp_selfbenefitamount_familytransportallowance;
      } else {
        this.additionalCover.selfbenefitamount_familytransportallowance = 0;
      }

      this.spousefamilyTransportAllownce = !this.spousefamilyTransportAllownce;
      this.spouse_family_trans_allowance = !this.spouse_family_trans_allowance;
      this.spousefamilyTransAllownceDivShow = !this
        .spousefamilyTransAllownceDivShow;

      this.child1familyTransportAllownce = !this.child1familyTransportAllownce;
      this.child1_family_trans_allowance = !this.child1_family_trans_allowance;
      this.child1familyTransAllownceDivShow = !this
        .child1familyTransAllownceDivShow;

      this.child2familyTransportAllownce = !this.child2familyTransportAllownce;
      this.child2_family_trans_allowance = !this.child2_family_trans_allowance;
      this.child2familyTransAllownceDivShow = !this
        .child2familyTransAllownceDivShow;
    } else if (num == 9 && member == "Self") {
      this.hospicashAllownceDivShow = !this.hospicashAllownceDivShow;

      if (this.self_hospital_cash_allownce == true) {
        this.additionalCover.selfbenefitamount_hospitalcashallowance = this.additionalCover.temp_selfbenefitamount_hospitalcashallowance;
      } else {
        this.additionalCover.selfbenefitamount_hospitalcashallowance = 0;
      }

      this.spousehospitalCashAllownce = !this.spousehospitalCashAllownce;
      this.spouse_hospital_cash_allownce = !this.spouse_hospital_cash_allownce;
      this.spousehospicashAllownceDivShow = !this
        .spousehospicashAllownceDivShow;

      this.child1hospitalCashAllownce = !this.child1hospitalCashAllownce;
      this.child1_hospital_cash_allownce = !this.child1_hospital_cash_allownce;
      this.child1hospicashAllownceDivShow = !this
        .child1hospicashAllownceDivShow;

      this.child2hospitalCashAllownce = !this.child2hospitalCashAllownce;
      this.child2_hospital_cash_allownce = !this.child2_hospital_cash_allownce;
      this.child2hospicashAllownceDivShow = !this
        .child2hospicashAllownceDivShow;
    } else if (num == 10 && member == "Self") {
      this.lifeSuppBenefitDivShow = !this.lifeSuppBenefitDivShow;

      if (this.self_life_support_benefit == true) {
        this.additionalCover.selfbenefitamount_lifesupportbenefit = this.additionalCover.temp_selfbenefitamount_lifesupportbenefit;
      } else {
        this.additionalCover.selfbenefitamount_lifesupportbenefit = 0;
      }

      this.spouselifeSuppBenefit = !this.spouselifeSuppBenefit;
      this.spouse_life_support_benefit = !this.spouse_life_support_benefit;
      this.spouselifeSuppBenefitDivShow = !this.spouselifeSuppBenefitDivShow;

      this.child1lifeSuppBenefit = !this.child1lifeSuppBenefit;
      this.child1_life_support_benefit = !this.child1_life_support_benefit;
      this.child1lifeSuppBenefitDivShow = !this.child1lifeSuppBenefitDivShow;

      this.child2lifeSuppBenefit = !this.child2lifeSuppBenefit;
      this.child2_life_support_benefit = !this.child2_life_support_benefit;
      this.child2lifeSuppBenefitDivShow = !this.child2lifeSuppBenefitDivShow;
    } else if (num == 11 && member == "Self") {
      this.brokenBonesDivShow = !this.brokenBonesDivShow;

      if (this.self_broken_bones == true) {
        this.additionalCover.selfbenefitamount_brokenbones = this.additionalCover.temp_selfbenefitamount_brokenbones;
      } else {
        this.additionalCover.selfbenefitamount_brokenbones = 0;
      }

      this.spousebrokenBones = !this.spousebrokenBones;
      this.spouse_broken_bones = !this.spouse_broken_bones;
      this.spousebrokenBonesDivShow = !this.spousebrokenBonesDivShow;

      this.child1brokenBones = !this.child1brokenBones;
      this.child1_broken_bones = !this.child1_broken_bones;
      this.child1brokenBonesDivShow = !this.child1brokenBonesDivShow;

      this.child2brokenBones = !this.child2brokenBones;
      this.child2_broken_bones = !this.child2_broken_bones;
      this.child2brokenBonesDivShow = !this.child2brokenBonesDivShow;
    } else if (num == 12 && member == "Self") {
      this.roadAmbulanceCoverDivShow = !this.roadAmbulanceCoverDivShow;

      if (this.self_road_ambulance_cover == true) {
        this.additionalCover.selfbenefitamount_roadambulancecover = this.additionalCover.temp_selfbenefitamount_roadambulancecover;
      } else {
        this.additionalCover.selfbenefitamount_roadambulancecover = 0;
      }

      this.spouseroadAmbulanceCover = !this.spouseroadAmbulanceCover;
      this.spouse_road_ambulance_cover = !this.spouse_road_ambulance_cover;
      this.spouseroadAmbulanceCoverDivShow = !this
        .spouseroadAmbulanceCoverDivShow;

      this.child1roadAmbulanceCover = !this.child1roadAmbulanceCover;
      this.child1_road_ambulance_cover = !this.child1_road_ambulance_cover;
      this.child1roadAmbulanceCoverDivShow = !this
        .child1roadAmbulanceCoverDivShow;

      this.child2roadAmbulanceCover = !this.child2roadAmbulanceCover;
      this.child2_road_ambulance_cover = !this.child2_road_ambulance_cover;
      this.child2roadAmbulanceCoverDivShow = !this
        .child2roadAmbulanceCoverDivShow;
    } else if (num == 13 && member == "Self") {
      this.airambulancecoverDivShow = !this.airambulancecoverDivShow;

      if (this.self_air_ambulance_cover == true) {
        this.additionalCover.selfbenefitamount_airambulancecover = this.additionalCover.temp_selfbenefitamount_airambulancecover;
      } else {
        this.additionalCover.selfbenefitamount_airambulancecover = 0;
      }

      this.spouseairAmbulanceCover = !this.spouseairAmbulanceCover;
      this.spouse_air_ambulance_cover = !this.spouse_air_ambulance_cover;
      this.spouseairambulancecoverDivShow = !this
        .spouseairambulancecoverDivShow;

      this.child1airAmbulanceCover = !this.child1airAmbulanceCover;
      this.child1_air_ambulance_cover = !this.child1_air_ambulance_cover;
      this.child1airambulancecoverDivShow = !this
        .child1airambulancecoverDivShow;

      this.child2airAmbulanceCover = !this.child2airAmbulanceCover;
      this.child2_air_ambulance_cover = !this.child2_air_ambulance_cover;
      this.child2airambulancecoverDivShow = !this
        .child2airambulancecoverDivShow;
    } else if (num == 14 && member == "Self") {
      this.adventureSportsBenefitDivShow = !this.adventureSportsBenefitDivShow;

      if (this.self_adventure_sports_benefit == true) {
        this.additionalCover.selfbenefitamount_adventuresportbenefit = this.additionalCover.temp_selfbenefitamount_adventuresportbenefit;
      } else {
        this.additionalCover.selfbenefitamount_adventuresportbenefit = 0;
      }

      this.spouseadventureSportsBenefit = !this.spouseadventureSportsBenefit;
      this.spouse_adventure_sports_benefit = !this
        .spouse_adventure_sports_benefit;
      this.spouseadventureSportsBenefitDivShow = !this
        .spouseadventureSportsBenefitDivShow;

      this.child1adventureSportsBenefit = !this.child1adventureSportsBenefit;
      this.child1_adventure_sports_benefit = !this
        .child1_adventure_sports_benefit;
      this.child1adventureSportsBenefitDivShow = !this
        .child1adventureSportsBenefitDivShow;

      this.child2adventureSportsBenefit = !this.child2adventureSportsBenefit;
      this.child2_adventure_sports_benefit = !this
        .child2_adventure_sports_benefit;
      this.child2adventureSportsBenefitDivShow = !this
        .child2adventureSportsBenefitDivShow;
    } else if (num == 15 && member == "Self") {
      this.chauffeurPlanBenefitDivShow = !this.chauffeurPlanBenefitDivShow;

      if (this.self_chauffeur_plan_benefit == true) {
        this.additionalCover.selfbenefitamount_chauffeurplanbenefit = this.additionalCover.temp_selfbenefitamount_chauffeurplanbenefit;
      } else {
        this.additionalCover.selfbenefitamount_chauffeurplanbenefit = 0;
      }

      this.spousechauffeurPlanBenefit = !this.spousechauffeurPlanBenefit;
      this.spouse_chauffeur_plan_benefit = !this.spouse_chauffeur_plan_benefit;
      this.spousechauffeurPlanBenefitDivShow = !this
        .spousechauffeurPlanBenefitDivShow;

      this.child1chauffeurPlanBenefit = !this.child1chauffeurPlanBenefit;
      this.child1_chauffeur_plan_benefit = !this.child1_chauffeur_plan_benefit;
      this.child1chauffeurPlanBenefitDivShow = !this
        .child1chauffeurPlanBenefitDivShow;

      this.child2chauffeurPlanBenefit = !this.child2chauffeurPlanBenefit;
      this.child2_chauffeur_plan_benefit = !this.child2_chauffeur_plan_benefit;
      this.child2chauffeurPlanBenefitDivShow = !this
        .child2chauffeurPlanBenefitDivShow;
    } else if (num == 16 && member == "Self") {
      this.loanProtectorDivShow = !this.loanProtectorDivShow;

      if (this.self_loan_protector == true) {
        this.additionalCover.selfbenefitamount_loanprotector = this.additionalCover.temp_selfbenefitamount_loanprotector;
      } else {
        this.additionalCover.selfbenefitamount_loanprotector = 0;
      }

      this.spouseloanProtector = !this.spouseloanProtector;
      this.spouse_loan_protector = !this.spouse_loan_protector;
      this.spouseloanProtectorDivShow = !this.spouseloanProtectorDivShow;

      this.child1loanProtector = !this.child1loanProtector;
      this.child1_loan_protector = !this.child1_loan_protector;
      this.child1loanProtectorDivShow = !this.child1loanProtectorDivShow;

      this.child2loanProtector = !this.child2loanProtector;
      this.child2_loan_protector = !this.child2_loan_protector;
      this.child2loanProtectorDivShow = !this.child2loanProtectorDivShow;

      this.lprotect = !this.lprotect;
      this.spouselprotect = !this.spouselprotect;

      this.toggleStatus = true;
      this.spousetoggleStatus = true;
    } else if (num == 1 && member == "Spouse") {
      if (this.spousepartdisab == true) {
        this.spouseinsureddetails.ppd_suminsured = this.spouseinsureddetails.temp_ppd_maxinsured;
      } else {
        this.spouseinsureddetails.ppd_suminsured = 0;
      }
      this.spousepartdisab = !this.spousepartdisab;
    } else if (num == 2 && member == "Spouse") {
      if (this.spousepermdisab == true) {
        this.spouseinsureddetails.ptd_suminsured = this.spouseinsureddetails.temp_ppd_maxinsured;
      } else {
        this.spouseinsureddetails.ptd_suminsured = 0;
      }

      this.spousepermdisab = !this.spousepermdisab;
    } else if (num == 3 && member == "Spouse") {
      if (this.spousetempdisab == true) {
        this.spouseinsureddetails.ttd_maxinsured = this.spouseinsureddetails.temp_ttd_maxinsured;
      } else {
        this.spouseinsureddetails.ttd_maxinsured = 0;
      }
      this.spousetempdisab = !this.spousetempdisab;
    } else if (num == 4 && member == "Spouse") {
      this.spouseadaptationallownceCheckDiv = !this
        .spouseadaptationallownceCheckDiv;

      if (this.spouse_adaption_allowance == true) {
        this.additionalCover.spousebenefitamount_adaptationallowance = this.additionalCover.temp_spousebenefitamount_adaptationallowance;
      } else {
        this.additionalCover.spousebenefitamount_adaptationallowance = 0;
      }
    } else if (num == 5 && member == "Spouse") {
      this.spouseaccidentalhospitalDivShow = !this
        .spouseaccidentalhospitalDivShow;

      if (this.spouse_acc_hospitalisation == true) {
        this.additionalCover.spousebenefitamount_acchospitalisation = this.additionalCover.temp_spousebenefitamount_acchospitalisation;
      } else {
        this.additionalCover.spousebenefitamount_acchospitalisation = 0;
      }
    } else if (num == 6 && member == "Spouse") {
      this.spouseaccidentalmedicalExpDivShow = !this
        .spouseaccidentalmedicalExpDivShow;

      if (this.spouse_acc_medical_expenses == true) {
        this.additionalCover.spousebenefitamount_accmedicalexpenses = this.additionalCover.temp_spousebenefitamount_accmedicalexpenses;
      } else {
        this.additionalCover.spousebenefitamount_accmedicalexpenses = 0;
      }
    } else if (num == 7 && member == "Spouse") {
      this.spousechildeduSuppDivShow = !this.spousechildeduSuppDivShow;

      if (this.spouse_child_edu_support == true) {
        this.additionalCover.spousebenefitamount_childeducationsupport = this.additionalCover.temp_spousebenefitamount_childeducationsupport;
      } else {
        this.additionalCover.spousebenefitamount_childeducationsupport = 0;
      }
    } else if (num == 8 && member == "Spouse") {
      this.spousefamilyTransAllownceDivShow = !this
        .spousefamilyTransAllownceDivShow;

      if (this.spouse_family_trans_allowance == true) {
        this.additionalCover.spousebenefitamount_familytransportallowance = this.additionalCover.temp_spousebenefitamount_familytransportallowance;
      } else {
        this.additionalCover.spousebenefitamount_familytransportallowance = 0;
      }
    } else if (num == 9 && member == "Spouse") {
      this.spousehospicashAllownceDivShow = !this
        .spousehospicashAllownceDivShow;

      if (this.spouse_hospital_cash_allownce == true) {
        this.additionalCover.spousebenefitamount_hospitalcashallowance = this.additionalCover.temp_spousebenefitamount_hospitalcashallowance;
      } else {
        this.additionalCover.spousebenefitamount_hospitalcashallowance = 0;
      }
    } else if (num == 10 && member == "Spouse") {
      this.spouselifeSuppBenefitDivShow = !this.spouselifeSuppBenefitDivShow;

      if (this.spouse_life_support_benefit == true) {
        this.additionalCover.spousebenefitamount_lifesupportbenefit = this.additionalCover.temp_spousebenefitamount_lifesupportbenefit;
      } else {
        this.additionalCover.spousebenefitamount_lifesupportbenefit = 0;
      }
    } else if (num == 11 && member == "Spouse") {
      this.spousebrokenBonesDivShow = !this.spousebrokenBonesDivShow;

      if (this.spouse_broken_bones == true) {
        this.additionalCover.spousebenefitamount_brokenbones = this.additionalCover.temp_spousebenefitamount_brokenbones;
      } else {
        this.additionalCover.spousebenefitamount_brokenbones = 0;
      }
    } else if (num == 12 && member == "Spouse") {
      this.spouseroadAmbulanceCoverDivShow = !this
        .spouseroadAmbulanceCoverDivShow;

      if (this.spouse_road_ambulance_cover == true) {
        this.additionalCover.spousebenefitamount_roadambulancecover = this.additionalCover.temp_spousebenefitamount_roadambulancecover;
      } else {
        this.additionalCover.spousebenefitamount_roadambulancecover = 0;
      }
    } else if (num == 13 && member == "Spouse") {
      this.spouseairambulancecoverDivShow = !this
        .spouseairambulancecoverDivShow;

      if (this.spouse_air_ambulance_cover == true) {
        this.additionalCover.spousebenefitamount_airambulancecover = this.additionalCover.temp_spousebenefitamount_airambulancecover;
      } else {
        this.additionalCover.spousebenefitamount_airambulancecover = 0;
      }
    } else if (num == 14 && member == "Spouse") {
      this.spouseadventureSportsBenefitDivShow = !this
        .spouseadventureSportsBenefitDivShow;

      if (this.spouse_adventure_sports_benefit == true) {
        this.additionalCover.spousebenefitamount_adventuresportbenefit = this.additionalCover.temp_spousebenefitamount_adventuresportbenefit;
      } else {
        this.additionalCover.spousebenefitamount_adventuresportbenefit = 0;
      }
    } else if (num == 15 && member == "Spouse") {
      this.spousechauffeurPlanBenefitDivShow = !this
        .spousechauffeurPlanBenefitDivShow;

      if (this.spouse_chauffeur_plan_benefit == true) {
        this.additionalCover.spousebenefitamount_chauffeurplanbenefit = this.additionalCover.temp_spousebenefitamount_chauffeurplanbenefit;
      } else {
        this.additionalCover.spousebenefitamount_chauffeurplanbenefit = 0;
      }
    } else if (num == 16 && member == "Spouse") {
      console.log(
        "spouseloanProtectorDivShow",
        this.spouseloanProtectorDivShow
      );

      console.log("spouseloanProtector", this.spouseloanProtector);
      console.log("spouse_loan_protector", this.spouse_loan_protector);
      console.log("spouselprotect", this.spouselprotect);

      if (this.spouse_loan_protector == true) {
        this.spouseloanProtectorDivShow = false;
        this.spouseloanProtector = false;
        this.spouse_loan_protector = true;
        this.spouselprotect = false;
        this.spousetoggleStatus = true;
      } else {
        this.spouseloanProtectorDivShow = true;
        this.spouseloanProtector = true;
        this.spouse_loan_protector = false;
        this.spouselprotect = true;
        this.spousetoggleStatus = false;
      }

      if (this.spouse_loan_protector == true) {
        this.additionalCover.spousebenefitamount_loanprotector = this.additionalCover.temp_spousebenefitamount_loanprotector;
      } else {
        this.additionalCover.spousebenefitamount_loanprotector = 0;
      }
    } else if (num == 1 && member == "Child1") {
      if (this.child1partdisab == true) {
        this.child1insureddetails.ppd_maxinsured = this.child1insureddetails.temp_ppd_maxinsured;
      } else {
        this.child1insureddetails.ppd_maxinsured = 0;
      }

      this.child1partdisab = !this.child1partdisab;
    } else if (num == 2 && member == "Child1") {
      if (this.child1permdisab == true) {
        this.child1insureddetails.ptd_maxinsured = this.child1insureddetails.temp_ptd_maxinsured;
      } else {
        this.child1insureddetails.ptd_maxinsured = 0;
      }

      this.child1permdisab = !this.child1permdisab;
    } else if (num == 3 && member == "Child1") {
      if (this.child1tempdisab == true) {
        this.child1insureddetails.ttd_maxinsured = this.insureddetails.temp_ttd_maxinsured;
      } else {
        this.child1insureddetails.ttd_maxinsured = 0;
      }

      this.child1tempdisab = !this.child1tempdisab;
    } else if (num == 4 && member == "Child1") {
      this.child1adaptationallownceCheckDiv = !this
        .child1adaptationallownceCheckDiv;

      if (this.child1_adaption_allowance == true) {
        this.additionalCover.child1benefitamount_adaptationallowance = this.additionalCover.temp_child1benefitamount_adaptationallowance;
      } else {
        this.additionalCover.child1benefitamount_adaptationallowance = 0;
      }
    } else if (num == 5 && member == "Child1") {
      this.child1accidentalhospitalDivShow = !this
        .child1accidentalhospitalDivShow;

      if (this.child1_acc_hospitalisation == true) {
        this.additionalCover.child1benefitamount_acchospitalisation = this.additionalCover.temp_child1benefitamount_acchospitalisation;
      } else {
        this.additionalCover.selfbenefitamount_acchospitalisation = 0;
      }
    } else if (num == 6 && member == "Child1") {
      this.child1accidentalmedicalExpDivShow = !this
        .child1accidentalmedicalExpDivShow;

      if (this.child1_acc_medical_expenses == true) {
        this.additionalCover.child1benefitamount_accmedicalexpenses = this.additionalCover.temp_child1benefitamount_accmedicalexpenses;
      } else {
        this.additionalCover.child1benefitamount_accmedicalexpenses = 0;
      }
    } else if (num == 7 && member == "Child1") {
      this.child1eduSuppDivShow = !this.child1eduSuppDivShow;
    } else if (num == 8 && member == "Child1") {
      this.child1familyTransAllownceDivShow = !this
        .child1familyTransAllownceDivShow;

      if (this.child1_family_trans_allowance == true) {
        this.additionalCover.child1benefitamount_familytransportallowance = this.additionalCover.temp_child1benefitamount_familytransportallowance;
      } else {
        this.additionalCover.child1benefitamount_familytransportallowance = 0;
      }
    } else if (num == 9 && member == "Child1") {
      this.child1hospicashAllownceDivShow = !this
        .child1hospicashAllownceDivShow;

      if (this.child1_hospital_cash_allownce == true) {
        this.additionalCover.child1benefitamount_hospitalcashallowance = this.additionalCover.temp_child1benefitamount_hospitalcashallowance;
      } else {
        this.additionalCover.child1benefitamount_hospitalcashallowance = 0;
      }
    } else if (num == 10 && member == "Child1") {
      this.child1lifeSuppBenefitDivShow = !this.child1lifeSuppBenefitDivShow;

      if (this.child1_life_support_benefit == true) {
        this.additionalCover.child1benefitamount_lifesupportbenefit = this.additionalCover.temp_child1benefitamount_lifesupportbenefit;
      } else {
        this.additionalCover.child1benefitamount_lifesupportbenefit = 0;
      }
    } else if (num == 11 && member == "Child1") {
      this.child1brokenBonesDivShow = !this.child1brokenBonesDivShow;

      if (this.child1_broken_bones == true) {
        this.additionalCover.child1benefitamount_brokenbones = this.additionalCover.temp_child1benefitamount_brokenbones;
      } else {
        this.additionalCover.child1benefitamount_brokenbones = 0;
      }
    } else if (num == 12 && member == "Child1") {
      this.child1roadAmbulanceCoverDivShow = !this
        .child1roadAmbulanceCoverDivShow;

      if (this.child1_road_ambulance_cover == true) {
        this.additionalCover.child1benefitamount_roadambulancecover = this.additionalCover.temp_child1benefitamount_roadambulancecover;
      } else {
        this.additionalCover.child1benefitamount_roadambulancecover = 0;
      }
    } else if (num == 13 && member == "Child1") {
      this.child1airambulancecoverDivShow = !this
        .child1airambulancecoverDivShow;

      if (this.child1_air_ambulance_cover == true) {
        this.additionalCover.child1benefitamount_airambulancecover = this.additionalCover.temp_child1benefitamount_airambulancecover;
      } else {
        this.additionalCover.child1benefitamount_airambulancecover = 0;
      }
    } else if (num == 14 && member == "Child1") {
      this.child1adventureSportsBenefitDivShow = !this
        .child1adventureSportsBenefitDivShow;

      if (this.child1_adventure_sports_benefit == true) {
        this.additionalCover.child1benefitamount_adventuresportbenefit = this.additionalCover.temp_child1benefitamount_adventuresportbenefit;
      } else {
        this.additionalCover.child1benefitamount_adventuresportbenefit = 0;
      }
    } else if (num == 15 && member == "Child1") {
      this.child1chauffeurPlanBenefitDivShow = !this
        .child1chauffeurPlanBenefitDivShow;

      if (this.child1_chauffeur_plan_benefit == true) {
        this.additionalCover.child1benefitamount_chauffeurplanbenefit = this.additionalCover.temp_child1benefitamount_chauffeurplanbenefit;
      } else {
        this.additionalCover.child1benefitamount_chauffeurplanbenefit = 0;
      }
    } else if (num == 1 && member == "Child2") {
      if (this.child2partdisab == true) {
        this.child2insureddetails.ppd_maxinsured = this.child2insureddetails.temp_ppd_maxinsured;
      } else {
        this.child2insureddetails.ppd_maxinsured = 0;
      }

      this.child2partdisab = !this.child2partdisab;
    } else if (num == 2 && member == "Child2") {
      if (this.child2permdisab == true) {
        this.child2insureddetails.ptd_maxinsured = this.child2insureddetails.temp_ptd_maxinsured;
      } else {
        this.child2insureddetails.ptd_maxinsured = 0;
      }

      this.child2permdisab = !this.child2permdisab;
    } else if (num == 3 && member == "Child2") {
      this.child2tempdisab = !this.child2tempdisab;

      if (this.child2tempdisab == true) {
        this.child2insureddetails.ttd_maxinsured = this.child2insureddetails.temp_ttd_maxinsured;
      } else {
        this.child2insureddetails.ttd_maxinsured = 0;
      }
    } else if (num == 4 && member == "Child2") {
      this.child2adaptationallownceCheckDiv = !this
        .child2adaptationallownceCheckDiv;

      if (this.child2_adaption_allowance == true) {
        this.additionalCover.child2benefitamount_adaptationallowance = this.additionalCover.temp_child2benefitamount_adaptationallowance;
      } else {
        this.additionalCover.child2benefitamount_adaptationallowance = 0;
      }
    } else if (num == 5 && member == "Child2") {
      this.child2accidentalhospitalDivShow = !this
        .child2accidentalhospitalDivShow;

      if (this.child2_acc_hospitalisation == true) {
        this.additionalCover.child2benefitamount_acchospitalisation = this.additionalCover.temp_child2benefitamount_acchospitalisation;
      } else {
        this.additionalCover.child2benefitamount_acchospitalisation = 0;
      }
    } else if (num == 6 && member == "Child2") {
      this.child2accidentalmedicalExpDivShow = !this
        .child2accidentalmedicalExpDivShow;

      if (this.child2_acc_medical_expenses == true) {
        this.additionalCover.child2benefitamount_accmedicalexpenses = this.additionalCover.temp_child2benefitamount_accmedicalexpenses;
      } else {
        this.additionalCover.child2benefitamount_accmedicalexpenses = 0;
      }
    } else if (num == 7 && member == "Child2") {
      this.child2eduSuppDivShow = !this.child2eduSuppDivShow;

      if (this.child2_edu_support == true) {
        this.additionalCover.child2benefitamount_childeducationsupport = this.additionalCover.temp_child2benefitamount_childeducationsupport;
      } else {
        this.additionalCover.child2benefitamount_childeducationsupport = 0;
      }
    } else if (num == 8 && member == "Child2") {
      this.child2familyTransAllownceDivShow = !this
        .child2familyTransAllownceDivShow;

      if (this.child2_family_trans_allowance == true) {
        this.additionalCover.child2benefitamount_familytransportallowance = this.additionalCover.temp_child2benefitamount_familytransportallowance;
      } else {
        this.additionalCover.child2benefitamount_familytransportallowance = 0;
      }
    } else if (num == 9 && member == "Child2") {
      this.child2hospicashAllownceDivShow = !this
        .child2hospicashAllownceDivShow;

      if (this.child1_hospital_cash_allownce == true) {
        this.additionalCover.child2benefitamount_hospitalcashallowance = this.additionalCover.temp_child2benefitamount_hospitalcashallowance;
      } else {
        this.additionalCover.child2benefitamount_hospitalcashallowance = 0;
      }
    } else if (num == 10 && member == "Child2") {
      this.child2lifeSuppBenefitDivShow = !this.child2lifeSuppBenefitDivShow;

      if (this.child2_life_support_benefit == true) {
        this.additionalCover.child2benefitamount_lifesupportbenefit = this.additionalCover.temp_child2benefitamount_lifesupportbenefit;
      } else {
        this.additionalCover.child2benefitamount_lifesupportbenefit = 0;
      }
    } else if (num == 11 && member == "Child2") {
      this.child2brokenBonesDivShow = !this.child2brokenBonesDivShow;

      if (this.child2_broken_bones == true) {
        this.additionalCover.child2benefitamount_brokenbones = this.additionalCover.temp_child2benefitamount_brokenbones;
      } else {
        this.additionalCover.child2benefitamount_brokenbones = 0;
      }
    } else if (num == 12 && member == "Child2") {
      this.child2roadAmbulanceCoverDivShow = !this
        .child2roadAmbulanceCoverDivShow;

      if (this.self_road_ambulance_cover == true) {
        this.additionalCover.selfbenefitamount_roadambulancecover = this.additionalCover.temp_selfbenefitamount_roadambulancecover;
      } else {
        this.additionalCover.selfbenefitamount_roadambulancecover = 0;
      }
    } else if (num == 13 && member == "Child2") {
      this.child2airambulancecoverDivShow = !this
        .child2airambulancecoverDivShow;

      if (this.child2_air_ambulance_cover == true) {
        this.additionalCover.child2benefitamount_airambulancecover = this.additionalCover.temp_child2benefitamount_airambulancecover;
      } else {
        this.additionalCover.child2benefitamount_airambulancecover = 0;
      }
    } else if (num == 14 && member == "Child2") {
      this.child2adventureSportsBenefitDivShow = !this
        .child2adventureSportsBenefitDivShow;

      if (this.child2_adventure_sports_benefit == true) {
        this.additionalCover.child2benefitamount_adventuresportbenefit = this.additionalCover.temp_child2benefitamount_adventuresportbenefit;
      } else {
        this.additionalCover.child2benefitamount_adventuresportbenefit = 0;
      }
    } else if (num == 15 && member == "Child2") {
      this.child2chauffeurPlanBenefitDivShow = !this
        .child2chauffeurPlanBenefitDivShow;

      if (this.child2_chauffeur_plan_benefit == true) {
        this.additionalCover.child2benefitamount_chauffeurplanbenefit = this.additionalCover.temp_child2benefitamount_chauffeurplanbenefit;
      } else {
        this.additionalCover.child2benefitamount_chauffeurplanbenefit = 0;
      }
    }
  }

  addCoverClck(num) {
    if (num == 1) {
      this.showPopupSelf = false;
      this.showTransBagroundSelf = false;
    } else if (num == 2) {
      this.showPopupSpouse = false;
      this.showTransBagroundSpouse = false;
    } else if (num == 3) {
      this.showPopupChild1 = false;
      this.showTransBagroundChild1 = false;
    } else if (num == 4) {
      this.showPopupChild2 = false;
      this.showTransBagroundChild2 = false;
    }
  }

  hidePopup(num) {
    if (num == 1) {
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
      this.toggleStatus = false;
    } else if (num == 2) {
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    } else if (num == 3) {
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    } else if (num == 3) {
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
  }

  applianceChange(member) {
    if (member == "Self") {
      if (this.toggleStatus == true) {
        this.selfloanValue = true;
        this.lprotect = false;
      } else {
        this.selfloanValue = false;
        this.lprotect = true;
        this.toggleStatus = false;

        this.self_loan_protector = false;
        this.loanProtectorDivShow = true;

        // this.spouseloanProtector= false;
        // this.spouse_loan_protector = true;
        // this.spouseloanProtectorDivShow= true;

        this.spouseloanProtector = !this.spouseloanProtector;
        this.spouse_loan_protector = !this.spouse_loan_protector;
        this.spouseloanProtectorDivShow = !this.spouseloanProtectorDivShow;

        this.child1loanProtector = false;
        this.child1_loan_protector = true;
        this.child1loanProtectorDivShow = true;

        this.child2loanProtector = false;
        this.child2_loan_protector = true;
        this.child2loanProtectorDivShow = true;
      }
    } else if (member == "Spouse") {
      if (this.spousetoggleStatus == true) {
        this.spouselprotect = false;
      } else {
        this.spouselprotect = true;
        this.spousetoggleStatus = false;

        this.spouseloanProtector = false;

        //this.spouseloanProtector = !this.spouseloanProtector
        this.spouse_loan_protector = false;
        this.spouseloanProtectorDivShow = true;

        this.child1loanProtector = false;
        this.child1_loan_protector = true;
        this.child1loanProtectorDivShow = true;

        this.child2loanProtector = false;
        this.child2_loan_protector = true;
        this.child2loanProtectorDivShow = true;
      }
    }
  }

  // //added by vinay
  // calculateSelfInsured() {

  //   var selfdetails = JSON.parse(sessionStorage.selfdetails);
  //   this.category = selfdetails.occupation;
  //   this.selfCategoryForChild = this.category;
  //   this.annualincome = selfdetails.annualincome;
  //   this.SelfOccupationCode = selfdetails.occupationCode;
  //   if (this.category == "2" && this.SelfOccupationCode == "SPOT") {
  //     this.selfTTD = true;

  //     this.tempdisab = true;
  //   }

  //   this.calculateSumInsured(this.category, this.annualincome, "Self");
  //   this.calculateBenefitAmount(this.category, "Self");

  // }

  // calculateSpouseInsured() {

  //   var spousedetails = JSON.parse(sessionStorage.spousedetails);
  //   this.category = spousedetails.occupation;
  //   console.log("category" + this.category);
  //   this.SpouseOccupationCode = spousedetails.occupationCode;
  //   if ((this.category == "2" && this.SpouseOccupationCode == "SPOT") || this.selfTTD == true || this.tempdisab == true) {
  //     this.spouseTTD = true;
  //     this.spousetempdisab = true;
  //   }
  //   this.annualincome = spousedetails.annualincome;

  //   this.calculateSumInsured(this.category, this.annualincome, "Spouse");

  // }

  // calculateChild1Insured() {

  //   var selfdetails = JSON.parse(sessionStorage.selfdetails);
  //   var child1Details = JSON.parse(sessionStorage.child1details);
  //   this.category = selfdetails.occupation;
  //   this.annualincome = child1Details.annualincome;
  //   this.category = "1"
  //   this.calculateSumInsured(this.category, this.annualincome, "Son");

  // }

  // calculateChild2Insured() {

  //   var selfdetails = JSON.parse(sessionStorage.selfdetails);
  //   var child2Details = JSON.parse(sessionStorage.child2details);
  //   this.category = selfdetails.occupation;
  //   this.annualincome = child2Details.annualincome;
  //   this.category = "1"
  //   this.calculateSumInsured(this.category, this.annualincome, "Daughter");

  // }

  getClassValuesForSumInsured(category: number, type: any) {
    //Call API and get values
    console.log("testSpouseCategory" + category);

    var datavalues = {
      AccidentalDeath: ["144", "144", "144"],
      PPD: ["144", "144", "144"],
      PTD: ["144", "144", "144"],
      TTD: ["24", "24", "0"]
    };

    if (type == "ac") {
      return datavalues.AccidentalDeath[category - 1];
    } else if (type == "ppd") {
      return datavalues.PPD[category - 1];
    } else if (type == "ptd") {
      return datavalues.PTD[category - 1];
    } else if (type == "ttd") {
      return datavalues.TTD[category - 1];
    }
  }

  getClassValuesForPremium(category: any, type: any) {
    //Call API and get values
    var datavalues = {
      AccidentalDeath: ["0.4", "0.6", "0.9"],
      PPD: ["0.25", "0.45", "0.75"],
      PTD: ["0.1", "0.2", "0.35"],
      TTD: ["0.5", "0.75", "1"]
    };

    if (type == "ac") {
      return datavalues.AccidentalDeath[category - 1];
    } else if (type == "ppd") {
      return datavalues.PPD[category - 1];
    } else if (type == "ptd") {
      return datavalues.PTD[category - 1];
    } else if (type == "ttd") {
      return datavalues.TTD[category - 1];
    }
  }

  getClassValuesForAdditionalCoverPremium(type: any, member) {
    let category = 1;
    var datavalues = {
      AdaptationAllowance: ["0.7", "0.75", "0.8"],
      ChildEducationSupport: ["0.5", "0.8", "1.25"],
      FamilyTransportationAllowance: ["0.3", "0.3", "0.3"],
      HospitalCashAllowance: ["25", "30", "40"],
      LoanProtector: ["2", "2.8", "4.25"],
      LifeSuppostBenefit: ["0.1", "0.2", "0.35"],
      AccidentalHospitalisation: ["1.5", "2", "3"],
      AccidentalMedicalExpenses: ["", "", ""],
      RepAndFuneralExpenses: ["", "", ""],
      BrokenBones: ["2.89", "2.89", "2.89"],
      RoadAmbulanceCover: ["2.174", "2.174", "2.174"],
      AirAmbulanceCover: ["0.127", "0.127", "0.127"],
      AdventureSportsBenefit: ["0.75", "1.2", "1.88"],
      ChauffeurPlanBenefit: ["", "", ""]
    };

    if (member == "Self") {
      if (type == "aa") {
        return datavalues.AdaptationAllowance[this.category - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.category - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[this.category - 1];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[this.category - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.category - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[this.category - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[this.category - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.category - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.category - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.category - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.category - 1];
      } else if (type == "cpb") {
        return "";
      }
    } else if (member == "Spouse") {
      if (type == "aa") {
        return datavalues.AdaptationAllowance[this.category - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.category - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[this.category - 1];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[this.category - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.category - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[this.category - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[this.category - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.category - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.category - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.category - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.category - 1];
      } else if (type == "cpb") {
        return "";
      }
    } else if (member == "Child1") {
      console.log("category: " + this.selfCategoryForChild);

      if (type == "aa") {
        return datavalues.AdaptationAllowance[1 - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.selfCategoryForChild - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[
          this.selfCategoryForChild - 1
        ];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[1 - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.selfCategoryForChild - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[1 - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[1 - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.selfCategoryForChild - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.selfCategoryForChild - 1];
      } else if (type == "cpb") {
        return "";
      }
    } else if (member == "Child2") {
      if (type == "aa") {
        return datavalues.AdaptationAllowance[1 - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.selfCategoryForChild - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[
          this.selfCategoryForChild - 1
        ];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[1 - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.selfCategoryForChild - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[1 - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[1 - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.selfCategoryForChild - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.selfCategoryForChild - 1];
      } else if (type == "cpb") {
        return "";
      }
    }
  }

  // calculateSumInsured(category, annualincome, member) {

  //   if (member == "Self") {

  //     if (this.setSelfLimitVar == false) {

  //       var classvalue: any = this.getClassValuesForSumInsured(category, "ac");
  //       this.insureddetails.acc_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;
  //       this.maxLimitSelfAccDeath = this.insureddetails.acc_maxinsured;

  //       classvalue = this.getClassValuesForSumInsured(category, "ppd");
  //       this.insureddetails.ppd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;
  //       this.maxLimitSelfPPD = this.insureddetails.ppd_maxinsured;

  //       classvalue = this.getClassValuesForSumInsured(category, "ptd");
  //       this.insureddetails.ptd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;
  //       this.maxLimitSelfPTD = this.insureddetails.ptd_maxinsured

  //       classvalue = this.getClassValuesForSumInsured(category, "ttd");
  //       this.insureddetails.ttd_maxinsured = (annualincome / 12) * classvalue;
  //       if (this.category == 1) {
  //         if (Number(this.insureddetails.ttd_maxinsured) >= 1000000) {
  //           this.insureddetails.ttd_maxinsured = 1000000
  //         }
  //       } else if (this.category == 2) {
  //         if (Number(this.insureddetails.ttd_maxinsured) >= 1000000) {
  //           this.insureddetails.ttd_maxinsured = 1000000
  //         }
  //       }
  //       this.maxLimitSelfTTD = this.insureddetails.ttd_maxinsured;

  //       this.calculatePremium(member, category);
  //       this.setSelfLimitVar = true;

  //     }

  //     if (this.insureddetails.ppd_maxinsured > this.insureddetails.acc_maxinsured) {
  //       this.insureddetails.ppd_maxinsured = this.insureddetails.acc_maxinsured
  //     }

  //     if (this.insureddetails.ptd_maxinsured > this.insureddetails.acc_maxinsured) {
  //       this.insureddetails.ptd_maxinsured = this.insureddetails.acc_maxinsured
  //     }

  //   } else if (member == "Spouse") {

  //     if (this.setSpouseLimitVar == false) {

  //       if (this.SelfOccupationCode == "HSWF") {
  //         /// in case of occupation househusband / housewife

  //         this.spouseinsureddetails.acc_maxinsured = (this.insureddetails.acc_maxinsured / 2)
  //         this.spouseinsureddetails.ptd_maxinsured = (this.insureddetails.ptd_maxinsured / 2)
  //         this.spouseinsureddetails.ppd_maxinsured = (this.insureddetails.ppd_maxinsured / 2)
  //         this.spouseinsureddetails.ttd_maxinsured = (this.insureddetails.ttd_maxinsured / 2)

  //       }
  //       else {

  //         var classvalue: any = this.getClassValuesForSumInsured(category, "ac");
  //         this.spouseinsureddetails.acc_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;

  //         if (this.spouseinsureddetails.acc_maxinsured > this.insureddetails.acc_maxinsured) {
  //             this.spouseinsureddetails.acc_maxinsured = this.insureddetails.acc_maxinsured
  //         }
  //         if (this.spouseinsureddetails.ppd_maxinsured > this.insureddetails.ppd_maxinsured) {
  //             this.spouseinsureddetails.ppd_maxinsured = this.insureddetails.ppd_maxinsured
  //         }

  //         if (this.spouseinsureddetails.ptd_maxinsured > this.insureddetails.ptd_maxinsured) {
  //           this.spouseinsureddetails.ptd_maxinsured = this.insureddetails.ptd_maxinsured
  //         }

  //         if (this.spouseinsureddetails.ttd_maxinsured > this.insureddetails.ttd_maxinsured) {
  //           this.spouseinsureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured
  //         }

  //         else {
  //           classvalue = this.getClassValuesForSumInsured(category, "ppd");
  //           this.spouseinsureddetails.ppd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;

  //           classvalue = this.getClassValuesForSumInsured(category, "ptd");
  //           this.spouseinsureddetails.ptd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;

  //           classvalue = this.getClassValuesForSumInsured(category, "ttd");
  //           this.spouseinsureddetails.ttd_maxinsured = (annualincome / 12) * classvalue;

  //         }

  //       }

  //     }

  //     if (this.category == 1) {
  //       if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 1000000) {
  //         this.spouseinsureddetails.ttd_maxinsured = 1000000
  //       }
  //     } else if (this.category == 2) {
  //       if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 1000000) {
  //         this.spouseinsureddetails.ttd_maxinsured = 1000000
  //       }
  //     }

  //     if (this.spouseinsureddetails.acc_maxinsured > this.insureddetails.acc_maxinsured) {
  //         this.spouseinsureddetails.acc_maxinsured = this.insureddetails.acc_maxinsured
  //     }

  //     if (this.spouseinsureddetails.ppd_maxinsured > this.insureddetails.ppd_maxinsured) {
  //       this.spouseinsureddetails.ppd_maxinsured = this.insureddetails.ppd_maxinsured
  //     }

  //     if (this.spouseinsureddetails.ptd_maxinsured > this.insureddetails.ptd_maxinsured) {
  //       this.spouseinsureddetails.ptd_maxinsured = this.insureddetails.ptd_maxinsured
  //     }

  //     if (this.spouseinsureddetails.ttd_maxinsured > this.insureddetails.ttd_maxinsured) {
  //       this.spouseinsureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured

  //     }

  //     this.calculatePremium(member, category);
  //     this.maxLimitSpouseAccDeath = this.spouseinsureddetails.acc_maxinsured;
  //     this.maxLimitSSpousePPD = this.spouseinsureddetails.ppd_maxinsured;
  //     this.maxLimitSpousePTD = this.spouseinsureddetails.ptd_maxinsured;
  //     this.maxLimitSpouseTTD = this.spouseinsureddetails.ttd_maxinsured;
  //     this.setSpouseLimitVar = true;

  //   } else if (member == "Son") {
  //     this.category = "1"

  //     if (this.setChild1LimitVar == false) {
  //       var classvalue: any = this.getClassValuesForSumInsured(category, "ac");
  //       this.child1insureddetails.acc_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

  //       if (this.child1insureddetails.acc_maxinsured > 500000) {
  //         this.child1insureddetails.acc_maxinsured = 500000
  //         this.child1insureddetails.ppd_maxinsured = 500000
  //         this.child1insureddetails.ptd_maxinsured = 500000

  //       } else {

  //         classvalue = this.getClassValuesForSumInsured(category, "ppd");
  //         console.log("class value: " + classvalue);
  //         this.child1insureddetails.ppd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

  //         classvalue = this.getClassValuesForSumInsured(category, "ptd");
  //         console.log("class value: " + classvalue);
  //         this.child1insureddetails.ptd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

  //         classvalue = this.getClassValuesForSumInsured(category, "ttd");
  //         console.log("class value: " + classvalue);
  //         this.child1insureddetails.ttd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;
  //         console.log("ttd" + this.insureddetails.ttd_maxinsured);

  //       }
  //       this.calculatePremium(member, category);
  //       this.maxLimitChild1AccDeath = this.child1insureddetails.acc_maxinsured;
  //       this.maxLimitChild1PPD = this.child1insureddetails.ppd_maxinsured;
  //       this.maxLimitChild1PTD = this.child1insureddetails.ptd_maxinsured;
  //       this.maxLimitChild1TTD = this.child1insureddetails.ttd_maxinsured;
  //       this.setChild1LimitVar = true;

  //     }
  //   } else if (member == "Daughter") {
  //     if (this.setChild2LimitVar == false) {
  //       var classvalue: any = this.getClassValuesForSumInsured(category, "ac");
  //       console.log("class value: " + classvalue);
  //       this.child2insureddetails.acc_maxinsured = (this.insureddetails.acc_maxinsured) / 4;
  //       if (this.child2insureddetails.acc_maxinsured > 500000) {
  //         this.child2insureddetails.acc_maxinsured = 500000
  //         this.child2insureddetails.ppd_maxinsured = 500000
  //         this.child2insureddetails.ptd_maxinsured = 500000

  //       } else {
  //         classvalue = this.getClassValuesForSumInsured(category, "ppd");
  //         console.log("class value: " + classvalue);
  //         this.child2insureddetails.ppd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

  //         classvalue = this.getClassValuesForSumInsured(category, "ptd");
  //         console.log("class value: " + classvalue);
  //         this.child2insureddetails.ptd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

  //         classvalue = this.getClassValuesForSumInsured(category, "ttd");
  //         console.log("class value: " + classvalue);
  //         this.child2insureddetails.ttd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;
  //         console.log("ttd" + this.insureddetails.ttd_maxinsured);
  //       }
  //       this.calculatePremium(member, category);
  //       this.maxLimitChild2AccDeath = this.child2insureddetails.acc_maxinsured;
  //       this.maxLimitChild2PPD = this.child2insureddetails.ppd_maxinsured;
  //       this.maxLimitChild2PTD = this.child2insureddetails.ptd_maxinsured;
  //       this.maxLimitChild2TTD = this.child2insureddetails.ttd_maxinsured;
  //       this.setChild1LimitVar = true;
  //     }
  //   }
  //   this.calculateBenefitAmount(category, member);
  // }

  // calculatePremium(member, category) {
  //   if (this.insureddetails.acc_maxinsured != "") {

  //     if (member == "Self") {
  //       var classvalue: any = this.getClassValuesForPremium(category, "ac");
  //       var amountinsured;
  //       amountinsured = this.insureddetails.acc_maxinsured;

  //       if (this.insureddetails.acc_suminsured != "") {
  //         if (this.insureddetails.acc_suminsured >= this.insureddetails.acc_maxinsured)
  //           amountinsured = this.insureddetails.acc_maxinsured;
  //         else
  //           amountinsured = this.insureddetails.acc_suminsured;
  //       }
  //       this.insureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ppd");
  //       amountinsured = this.insureddetails.ppd_maxinsured;
  //       if (this.insureddetails.ppd_suminsured != "") {
  //         if (this.insureddetails.ppd_suminsured >= this.insureddetails.ppd_maxinsured)
  //           amountinsured = this.insureddetails.ppd_maxinsured;
  //         else
  //           amountinsured = this.insureddetails.ppd_suminsured;
  //       }
  //       this.insureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ptd");
  //       amountinsured = this.insureddetails.ptd_maxinsured;
  //       if (this.insureddetails.ptd_suminsured != "") {
  //         if (this.insureddetails.ptd_suminsured >= this.insureddetails.ptd_maxinsured)
  //           amountinsured = this.insureddetails.ptd_maxinsured;
  //         else
  //           amountinsured = this.insureddetails.ptd_suminsured;
  //       }
  //       this.insureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ttd");
  //       amountinsured = this.insureddetails.ttd_maxinsured;
  //       if (this.insureddetails.ttd_suminsured != "") {
  //         if (this.insureddetails.ttd_suminsured >= this.insureddetails.ttd_maxinsured)
  //           amountinsured = this.insureddetails.ttd_maxinsured;
  //         else
  //           amountinsured = this.insureddetails.ttd_suminsured;
  //       }
  //       this.insureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       //this.calculateBenefitAmount();
  //     }
  //   }
  //   if (this.spouseinsureddetails.acc_maxinsured != "") {
  //     if (member == "Spouse") {
  //       var classvalue: any = this.getClassValuesForPremium(category, "ac");
  //       var amountinsured;
  //       amountinsured = this.spouseinsureddetails.acc_maxinsured;
  //       if (this.spouseinsureddetails.acc_suminsured != "") {
  //         if (this.spouseinsureddetails.acc_suminsured >= this.spouseinsureddetails.acc_maxinsured)
  //           amountinsured = this.spouseinsureddetails.acc_maxinsured;
  //         else
  //           amountinsured = this.spouseinsureddetails.acc_suminsured;
  //       }
  //       this.spouseinsureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ppd");
  //       amountinsured = this.spouseinsureddetails.ppd_maxinsured;
  //       if (this.spouseinsureddetails.ppd_suminsured != "") {
  //         if (this.spouseinsureddetails.ppd_suminsured >= this.spouseinsureddetails.ppd_maxinsured)
  //           amountinsured = this.spouseinsureddetails.ppd_maxinsured;
  //         else
  //           amountinsured = this.spouseinsureddetails.ppd_suminsured;
  //       }
  //       this.spouseinsureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ptd");
  //       amountinsured = this.spouseinsureddetails.ptd_maxinsured;
  //       if (this.spouseinsureddetails.ptd_suminsured != "") {
  //         if (this.spouseinsureddetails.ptd_suminsured >= this.spouseinsureddetails.ptd_maxinsured)
  //           amountinsured = this.spouseinsureddetails.ptd_maxinsured;
  //         else
  //           amountinsured = this.spouseinsureddetails.ptd_suminsured;
  //       }
  //       this.spouseinsureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ttd");
  //       amountinsured = this.spouseinsureddetails.ttd_maxinsured;
  //       if (this.spouseinsureddetails.ttd_suminsured != "") {
  //         if (this.spouseinsureddetails.ttd_suminsured >= this.spouseinsureddetails.ttd_maxinsured)
  //           amountinsured = this.spouseinsureddetails.ttd_maxinsured;
  //         else
  //           amountinsured = this.spouseinsureddetails.ttd_suminsured;
  //       }
  //       this.spouseinsureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);
  //       //this.calculateBenefitAmount();

  //     }
  //   }
  //   if (this.child1insureddetails.acc_maxinsured != "") {
  //     if (member == "Child1") {
  //       var classvalue: any = this.getClassValuesForPremium(category, "ac");
  //       var amountinsured;
  //       amountinsured = this.child1insureddetails.acc_maxinsured;
  //       if (this.child1insureddetails.acc_suminsured != "") {
  //         if (this.child1insureddetails.acc_suminsured >= this.child1insureddetails.acc_maxinsured)
  //           amountinsured = this.child1insureddetails.acc_maxinsured;
  //         else
  //           amountinsured = this.child1insureddetails.acc_suminsured;
  //       }
  //       this.child1insureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ppd");
  //       amountinsured = this.child1insureddetails.ppd_maxinsured;
  //       if (this.child1insureddetails.ppd_suminsured != "") {
  //         if (this.child1insureddetails.ppd_suminsured >= this.child1insureddetails.ppd_maxinsured)
  //           amountinsured = this.child1insureddetails.ppd_maxinsured;
  //         else
  //           amountinsured = this.child1insureddetails.ppd_suminsured;
  //       }
  //       this.child1insureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ptd");
  //       amountinsured = this.child1insureddetails.ptd_maxinsured;
  //       if (this.child1insureddetails.ptd_suminsured != "") {
  //         if (this.child1insureddetails.ptd_suminsured >= this.child1insureddetails.ptd_maxinsured)
  //           amountinsured = this.child1insureddetails.ptd_maxinsured;
  //         else
  //           amountinsured = this.child1insureddetails.ptd_suminsured;
  //       }
  //       this.child1insureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ttd");
  //       amountinsured = this.child1insureddetails.ttd_maxinsured;
  //       if (this.child1insureddetails.ttd_suminsured != "") {
  //         if (this.child1insureddetails.ttd_suminsured >= this.child1insureddetails.ttd_maxinsured)
  //           amountinsured = this.child1insureddetails.ttd_maxinsured;
  //         else
  //           amountinsured = this.child1insureddetails.ttd_suminsured;
  //       }
  //       this.child1insureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);
  //       //this.calculateBenefitAmount();

  //     }
  //   } if (this.child2insureddetails.acc_maxinsured != "") {
  //     if (member == "Child2") {
  //       var classvalue: any = this.getClassValuesForPremium(category, "ac");
  //       var amountinsured;
  //       amountinsured = this.child2insureddetails.acc_maxinsured;
  //       if (this.child2insureddetails.acc_suminsured != "") {
  //         if (this.child2insureddetails.acc_suminsured >= this.child2insureddetails.acc_maxinsured)
  //           amountinsured = this.child2insureddetails.acc_maxinsured;
  //         else
  //           amountinsured = this.child2insureddetails.acc_suminsured;
  //       }
  //       this.child2insureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ppd");
  //       amountinsured = this.child2insureddetails.ppd_maxinsured;

  //       if (this.child2insureddetails.ppd_suminsured != "") {
  //         if (this.child2insureddetails.ppd_suminsured >= this.child2insureddetails.ppd_maxinsured)
  //           amountinsured = this.child2insureddetails.ppd_maxinsured;
  //         else
  //           amountinsured = this.child2insureddetails.ppd_suminsured;
  //       }
  //       this.child2insureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ptd");
  //       amountinsured = this.child2insureddetails.ptd_maxinsured;
  //       if (this.child2insureddetails.ptd_suminsured != "") {
  //         if (this.child2insureddetails.ptd_suminsured >= this.child2insureddetails.ptd_maxinsured)
  //           amountinsured = this.child2insureddetails.ptd_maxinsured;
  //         else
  //           amountinsured = this.child2insureddetails.ptd_suminsured;
  //       }
  //       this.child2insureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

  //       classvalue = this.getClassValuesForPremium(category, "ttd");
  //       amountinsured = this.child2insureddetails.ttd_maxinsured;
  //       if (this.child2insureddetails.ttd_suminsured != "") {
  //         if (this.child2insureddetails.ttd_suminsured >= this.child2insureddetails.ttd_maxinsured)
  //           amountinsured = this.child2insureddetails.ttd_maxinsured;
  //         else
  //           amountinsured = this.child2insureddetails.ttd_suminsured;
  //       }
  //       this.child2insureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);
  //       //this.calculateBenefitAmount();

  //     }
  //     console.log("Unable to calculate premium.");

  //   }

  // }

  validateNumber(myString: String) {
    myString = myString.replace(/\D/g, "");

    return myString;
  }

  // checkSumInsured() {

  //   if (this.insureddetails.acc_maxinsured != "") {
  //     if (this.insureddetails.acc_suminsured > this.insureddetails.acc_maxinsured)
  //       this.insureddetails.acc_suminsured = this.insureddetails.acc_maxinsured;
  //   }

  //   if (this.insureddetails.ppd_maxinsured != "") {
  //     if (this.insureddetails.ppd_suminsured > this.insureddetails.ppd_maxinsured)
  //       this.insureddetails.ppd_suminsured = this.insureddetails.ppd_maxinsured;
  //   }

  //   if (this.insureddetails.ptd_maxinsured != "") {
  //     if (this.insureddetails.ptd_suminsured > this.insureddetails.ptd_maxinsured)
  //       this.insureddetails.ptd_suminsured = this.insureddetails.ptd_maxinsured;
  //   }

  //   if (this.insureddetails.ttd_maxinsured != "") {
  //     if (this.insureddetails.ttd_suminsured > this.insureddetails.ttd_maxinsured)
  //       this.insureddetails.ttd_suminsured = this.insureddetails.ttd_maxinsured;
  //   }

  // }

  // calculateBenefitAmount(category, member) {
  //   category = "1";

  //   if (member == 'Self') {
  //     // var classvalue:any = this.getClassValuesForSumInsured(category,"ac");
  //     //     console.log("class value: "+classvalue);

  //     this.additionalCover.selfbenefitamount_adaptationallowance = (this.insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.insureddetails.acc_maxinsured) / 10;
  //     this.additionalCover.selfbenefitamount_childeducationsupport = (this.insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.selfbenefitamount_familytransportallowance = (this.insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.insureddetails.acc_maxinsured) / 10;
  //     if (category == "1")
  //       this.additionalCover.selfbenefitamount_hospitalcashallowance = 2000;
  //     if (this.category == "2")
  //       this.additionalCover.selfbenefitamount_hospitalcashallowance = 1500;
  //     if (this.category == "3")
  //       this.additionalCover.selfbenefitamount_hospitalcashallowance = 1000;

  //     if (this.category == "1")
  //       this.additionalCover.selfbenefitamount_loanprotector = (this.insureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.insureddetails.acc_maxinsured) / 50;
  //     if (this.category == "2")
  //       this.additionalCover.selfbenefitamount_loanprotector = (this.insureddetails.acc_maxinsured) / 50 > 15000 ? 15000 : (this.insureddetails.acc_maxinsured) / 50;
  //     if (this.category == "3")
  //       this.additionalCover.selfbenefitamount_loanprotector = (this.insureddetails.acc_maxinsured) / 50 > 7500 ? 7500 : (this.insureddetails.acc_maxinsured) / 50;

  //     this.additionalCover.selfbenefitamount_lifesupportbenefit = (this.insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.selfbenefitamount_acchospitalisation = (this.insureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.insureddetails.acc_maxinsured) / 4;
  //     this.additionalCover.selfbenefitamount_accmedicalexpenses = "Opted" //As per clause
  //     this.additionalCover.selfbenefitamount_repatriationfuneralexpenses = (this.insureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.insureddetails.acc_maxinsured) / 100;

  //     if (this.category == "1")
  //       this.additionalCover.selfbenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1500000 ? 1500000 : (this.annualincome / 12) * 24;
  //     if (this.category == "2")
  //       this.additionalCover.selfbenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1000000 ? 1000000 : (this.annualincome / 12) * 24;
  //     if (this.category == "3")
  //       this.additionalCover.selfbenefitamount_brokenbones = (this.annualincome / 12) * 24 > 200000 ? 200000 : (this.annualincome / 12) * 24;

  //     this.additionalCover.selfbenefitamount_roadambulancecover = 50000;

  //     this.additionalCover.selfbenefitamount_airambulancecover = (this.insureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.selfbenefitamount_adventuresportbenefit = (this.insureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.insureddetails.acc_maxinsured) / 2;
  //     this.additionalCover.selfbenefitamount_chauffeurplanbenefit = 1000;

  //     this.calculateAdditionalCoverPremium(member);
  //   } else if (member == "Spouse") {
  //     this.additionalCover.spousebenefitamount_adaptationallowance = (this.spouseinsureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.spouseinsureddetails.acc_maxinsured) / 10;
  //     this.additionalCover.spousebenefitamount_childeducationsupport = (this.spouseinsureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.spouseinsureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.spousebenefitamount_familytransportallowance = (this.spouseinsureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.spouseinsureddetails.acc_maxinsured) / 10;
  //     if (this.category == "1")
  //       this.additionalCover.spousebenefitamount_hospitalcashallowance = 2000;
  //     if (this.category == "2")
  //       this.additionalCover.spousebenefitamount_hospitalcashallowance = 1500;
  //     if (this.category == "3")
  //       this.additionalCover.spousebenefitamount_hospitalcashallowance = 1000;

  //     if (this.category == "1")
  //       this.additionalCover.spousebenefitamount_loanprotector = (this.spouseinsureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.spouseinsureddetails.acc_maxinsured) / 50;
  //     if (this.category == "2")
  //       this.additionalCover.spousebenefitamount_loanprotector = (this.spouseinsureddetails.acc_maxinsured) / 50 > 15000 ? 15000 : (this.spouseinsureddetails.acc_maxinsured) / 50;
  //     if (this.category == "3")
  //       this.additionalCover.spousebenefitamount_loanprotector = (this.spouseinsureddetails.acc_maxinsured) / 50 > 7500 ? 7500 : (this.spouseinsureddetails.acc_maxinsured) / 50;

  //     this.additionalCover.spousebenefitamount_lifesupportbenefit = (this.spouseinsureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.spouseinsureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.spousebenefitamount_acchospitalisation = (this.spouseinsureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.spouseinsureddetails.acc_maxinsured) / 4;
  //     this.additionalCover.spousebenefitamount_accmedicalexpenses = "Opted"//As per clause
  //     console.log("spouStatus" + this.spouseTabShow);

  //     this.additionalCover.spousebenefitamount_repatriationfuneralexpenses = (this.spouseinsureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.spouseinsureddetails.acc_maxinsured) / 100;

  //     if (this.category == "1")
  //       this.additionalCover.spousebenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1500000 ? 1500000 : (this.annualincome / 12) * 24;
  //     if (this.category == "2")
  //       this.additionalCover.spousebenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1000000 ? 1000000 : (this.annualincome / 12) * 24;
  //     if (this.category == "3")
  //       this.additionalCover.spousebenefitamount_brokenbones = (this.annualincome / 12) * 24 > 200000 ? 200000 : (this.annualincome / 12) * 24;

  //     this.additionalCover.spousebenefitamount_airambulancecover = (this.spouseinsureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.spouseinsureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.spousebenefitamount_adventuresportbenefit = (this.spouseinsureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.spouseinsureddetails.acc_maxinsured) / 2;
  //     this.additionalCover.spousebenefitamount_chauffeurplanbenefit = 1000;

  //     this.additionalCover.spousebenefitamount_roadambulancecover = 50000;
  //     this.calculateAdditionalCoverPremium(member);
  //   } else if (member == "Child1") {
  //     this.additionalCover.child1benefitamount_adaptationallowance = (this.child1insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child1insureddetails.acc_maxinsured) / 10;
  //     this.additionalCover.child1benefitamount_childeducationsupport = (this.child1insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child1insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.child1benefitamount_familytransportallowance = (this.child1insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child1insureddetails.acc_maxinsured) / 10;
  //     if (this.category == "1")
  //       this.additionalCover.child1benefitamount_hospitalcashallowance = 2000;
  //     // if(this.category == "2")
  //     //   this.additionalCover.child1benefitamount_hospitalcashallowance = 1500;
  //     // if(this.category == "3")
  //     //   this.additionalCover.child1benefitamount_hospitalcashallowance = 1000;

  //     if (this.category == "1")
  //       this.additionalCover.child1benefitamount_loanprotector = (this.child1insureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.child1insureddetails.acc_maxinsured) / 50;
  //     // if(this.category == "2")
  //     // this.additionalCover.child1benefitamount_loanprotector = (this.child1insureddetails.acc_maxinsured)/50 > 15000 ? 15000 : (this.child1insureddetails.acc_maxinsured)/50;
  //     // if(this.category == "3")
  //     // this.additionalCover.child1benefitamount_loanprotector = (this.child1insureddetails.acc_maxinsured)/50 > 7500 ? 7500 : (this.child1insureddetails.acc_maxinsured)/50;

  //     this.additionalCover.child1benefitamount_lifesupportbenefit = (this.child1insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child1insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.child1benefitamount_acchospitalisation = (this.child1insureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.child1insureddetails.acc_maxinsured) / 4;
  //     this.additionalCover.child1benefitamount_accmedicalexpenses = "Opted"//As per clause
  //     this.additionalCover.child1benefitamount_repatriationfuneralexpenses = (this.child1insureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.child1insureddetails.acc_maxinsured) / 100;

  //     //if(this.category == "1")
  //     //this.additionalCover.child1benefitamount_brokenbones = (this.annualincome/12)*24 > 1500000 ? 1500000 : (this.annualincome/12)*24;
  //     // if(this.category == "2")
  //     //   this.additionalCover.child1benefitamount_brokenbones = (this.annualincome/12)*24 > 1000000 ? 1000000 : (this.annualincome/12)*24;
  //     // if(this.category == "3")
  //     //   this.additionalCover.child1benefitamount_brokenbones = (this.annualincome/12)*24 > 200000 ? 200000 : (this.annualincome/12)*24;
  //     this.additionalCover.child1benefitamount_brokenbones = Number(this.additionalCover.selfbenefitamount_brokenbones) / 4;

  //     this.additionalCover.child1benefitamount_airambulancecover = (this.child1insureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.child1insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.child1benefitamount_adventuresportbenefit = (this.child1insureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.child1insureddetails.acc_maxinsured) / 2;
  //     this.additionalCover.child1benefitamount_chauffeurplanbenefit = 1000;

  //     this.additionalCover.child1benefitamount_roadambulancecover = 50000;
  //     this.calculateAdditionalCoverPremium(member);
  //   } else if (member == 'Child2') {
  //     this.additionalCover.child2benefitamount_adaptationallowance = (this.child2insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child2insureddetails.acc_maxinsured) / 10;
  //     this.additionalCover.child2benefitamount_childeducationsupport = (this.child2insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child2insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.child2benefitamount_familytransportallowance = (this.child2insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child2insureddetails.acc_maxinsured) / 10;
  //     if (this.category == "1")
  //       this.additionalCover.child2benefitamount_hospitalcashallowance = 2000;
  //     // if(this.category == "2")
  //     //   this.additionalCover.child2benefitamount_hospitalcashallowance = 1500;
  //     // if(this.category == "3")
  //     //   this.additionalCover.child2benefitamount_hospitalcashallowance = 1000;

  //     if (this.category == "1")
  //       this.additionalCover.child2benefitamount_loanprotector = (this.child2insureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.child2insureddetails.acc_maxinsured) / 50;
  //     // if(this.category == "2")
  //     // this.additionalCover.child2benefitamount_loanprotector = (this.child2insureddetails.acc_maxinsured)/50 > 15000 ? 15000 : (this.child2insureddetails.acc_maxinsured)/50;
  //     // if(this.category == "3")
  //     // this.additionalCover.child2benefitamount_loanprotector = (this.child2insureddetails.acc_maxinsured)/50 > 7500 ? 7500 : (this.child2insureddetails.acc_maxinsured)/50;

  //     this.additionalCover.child2benefitamount_lifesupportbenefit = (this.child2insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child2insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.child2benefitamount_acchospitalisation = (this.child2insureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.child2insureddetails.acc_maxinsured) / 4;
  //     this.additionalCover.child2benefitamount_accmedicalexpenses = "Opted" //As per clause
  //     this.additionalCover.child2benefitamount_repatriationfuneralexpenses = (this.child2insureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.child2insureddetails.acc_maxinsured) / 100;

  //     // if(this.category == "1")
  //     //   this.additionalCover.child2benefitamount_brokenbones = (this.annualincome/12)*24 > 1500000 ? 1500000 : (this.annualincome/12)*24;
  //     // if(this.category == "2")
  //     //   this.additionalCover.child2benefitamount_brokenbones = (this.annualincome/12)*24 > 1000000 ? 1000000 : (this.annualincome/12)*24;
  //     // if(this.category == "3")
  //     //   this.additionalCover.child2benefitamount_brokenbones = (this.annualincome/12)*24 > 200000 ? 200000 : (this.annualincome/12)*24;

  //     this.additionalCover.child2benefitamount_brokenbones = Number(this.additionalCover.selfbenefitamount_brokenbones) / 4;
  //     //pending --> Ask viddhesh
  //     this.additionalCover.child2benefitamount_airambulancecover = (this.child2insureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.child2insureddetails.acc_maxinsured) / 100;
  //     this.additionalCover.child2benefitamount_adventuresportbenefit = (this.child2insureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.child2insureddetails.acc_maxinsured) / 2;
  //     this.additionalCover.child2benefitamount_chauffeurplanbenefit = 1000;

  //     this.additionalCover.child2benefitamount_roadambulancecover = 50000;
  //     this.calculateAdditionalCoverPremium(member);
  //   }

  //   //this.additionalCover.benefitamount_adaptationallowance = (this.insureddetails.acc_maxinsured)/10 > 50000 ? 50000 : (this.insureddetails.acc_maxinsured)/10;

  // }

  // calculateAdditionalCoverPremium(member) {

  //   if (member == 'Self') {
  //     var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
  //     this.additionalCover.selfpremium_childeducationsupport = Number((this.additionalCover.selfbenefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);;

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
  //     this.additionalCover.selfpremium_lifesupportbenefit = Number((this.additionalCover.selfbenefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
  //     this.additionalCover.selfpremium_accmedicalexpenses = Number((Number(this.insureddetails.acc_premium) +
  //       Number(this.insureddetails.ppd_premium) +
  //       Number(this.insureddetails.ptd_premium) +
  //       Number(this.insureddetails.ttd_premium)) * 0.2).toFixed(2);;

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
  //     this.additionalCover.selfpremium_acchospitalisation = Number((this.additionalCover.selfbenefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
  //     this.additionalCover.selfpremium_hospitalcashallowance = Number((this.additionalCover.selfbenefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
  //     this.additionalCover.selfpremium_loanprotector = Number((this.additionalCover.selfbenefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
  //     this.additionalCover.selfpremium_adaptationallowance = Number((this.additionalCover.selfbenefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
  //     this.additionalCover.selfpremium_familytransportallowance = Number((this.additionalCover.selfbenefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

  //     console.log("StatusCheck" + sessionStorage.selfShowStatus);

  //     if (sessionStorage.selfShowStatus == "true") {
  //       classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       // this.additionalCover.selfpremium_repatriationfuneralexpenses = (this.additionalCover.selfbenefitamount_repatriationfuneralexpenses * classvalues) / 1000;
  //       this.additionalCover.selfpremium_repatriationfuneralexpenses = "";
  //     } else {
  //       this.additionalCover.selfpremium_repatriationfuneralexpenses = "";
  //     }

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
  //     this.additionalCover.selfpremium_brokenbones = Number((this.additionalCover.selfbenefitamount_brokenbones * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
  //     this.additionalCover.selfpremium_roadambulancecover = Number((this.additionalCover.selfbenefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
  //     this.additionalCover.selfpremium_airambulancecover = Number((this.additionalCover.selfbenefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
  //     this.additionalCover.selfpremium_adventuresportbenefit = Number((this.additionalCover.selfbenefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
  //     this.additionalCover.selfpremium_chauffeurplanbenefit = 119.50;

  //   } else if (member == "Spouse") {
  //     var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
  //     this.additionalCover.spousepremium_childeducationsupport = Number((this.additionalCover.spousebenefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
  //     this.additionalCover.spousepremium_lifesupportbenefit = Number((this.additionalCover.spousebenefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
  //     this.additionalCover.spousepremium_accmedicalexpenses = Number((Number(this.spouseinsureddetails.acc_premium) +
  //       Number(this.spouseinsureddetails.ppd_premium) +
  //       Number(this.spouseinsureddetails.ptd_premium) +
  //       Number(this.spouseinsureddetails.ttd_premium)) * 0.2).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
  //     this.additionalCover.spousepremium_acchospitalisation = Number((this.additionalCover.spousebenefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
  //     this.additionalCover.spousepremium_hospitalcashallowance = Number((this.additionalCover.spousebenefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);;

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
  //     this.additionalCover.spousepremium_loanprotector = Number((this.additionalCover.spousebenefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);;

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
  //     this.additionalCover.spousepremium_adaptationallowance = Number((this.additionalCover.spousebenefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
  //     this.additionalCover.spousepremium_familytransportallowance = Number((this.additionalCover.spousebenefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

  //     if (sessionStorage.spouseShowStatus == "true") {
  //       classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       //this.additionalCover.spousepremium_repatriationfuneralexpenses = (this.additionalCover.spousebenefitamount_repatriationfuneralexpenses * classvalues) / 1000;
  //       this.additionalCover.spousepremium_repatriationfuneralexpenses = "";
  //     } else {
  //       this.additionalCover.spousepremium_repatriationfuneralexpenses = "";
  //     }

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
  //     this.additionalCover.spousepremium_brokenbones = Number((this.additionalCover.spousebenefitamount_brokenbones * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
  //     this.additionalCover.spousepremium_roadambulancecover = Number((this.additionalCover.spousebenefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
  //     this.additionalCover.spousepremium_airambulancecover = Number((this.additionalCover.spousebenefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
  //     this.additionalCover.spousepremium_adventuresportbenefit = Number((this.additionalCover.spousebenefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
  //     this.additionalCover.spousepremium_chauffeurplanbenefit = 119.50;
  //   } else if (member == "Child1") {

  //     var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
  //     this.additionalCover.child1premium_childeducationsupport = Number((this.additionalCover.child1benefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
  //     this.additionalCover.child1premium_lifesupportbenefit = Number((this.additionalCover.child1benefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
  //     this.additionalCover.child1premium_accmedicalexpenses = Number((Number(this.child1insureddetails.acc_premium) +
  //       Number(this.child1insureddetails.ppd_premium) +
  //       Number(this.child1insureddetails.ptd_premium)) * 0.2).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
  //     this.additionalCover.child1premium_acchospitalisation = Number((this.additionalCover.child1benefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);;

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
  //     this.additionalCover.child1premium_hospitalcashallowance = Number((this.additionalCover.child1benefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);;

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
  //     this.additionalCover.child1premium_loanprotector = Number((this.additionalCover.child1benefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
  //     this.additionalCover.child1premium_adaptationallowance = Number((this.additionalCover.child1benefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
  //     this.additionalCover.child1premium_familytransportallowance = Number((this.additionalCover.child1benefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

  //     if (sessionStorage.child1ShowStatus == "true") {
  //       classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.child1premium_repatriationfuneralexpenses = "";
  //       // this.additionalCover.child1premium_repatriationfuneralexpenses = (this.additionalCover.child1benefitamount_repatriationfuneralexpenses * classvalues) / 1000
  //     } else {
  //       this.additionalCover.child1premium_repatriationfuneralexpenses = "";
  //     }

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
  //     this.additionalCover.child1premium_brokenbones = Number((this.additionalCover.child1benefitamount_brokenbones * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
  //     this.additionalCover.child1premium_roadambulancecover = Number((this.additionalCover.child1benefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
  //     this.additionalCover.child1premium_airambulancecover = Number((this.additionalCover.child1benefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
  //     this.additionalCover.child1premium_adventuresportbenefit = Number((this.additionalCover.child1benefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
  //     this.additionalCover.child1premium_chauffeurplanbenefit = 119.50;
  //   } else if (member == "Child2") {
  //     var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
  //     this.additionalCover.child2premium_childeducationsupport = Number((this.additionalCover.child2benefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
  //     this.additionalCover.child2premium_lifesupportbenefit = Number((this.additionalCover.child2benefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
  //     this.additionalCover.child2premium_accmedicalexpenses = Number((Number(this.child2insureddetails.acc_premium) +
  //       Number(this.child2insureddetails.ppd_premium) +
  //       Number(this.child2insureddetails.ptd_premium)) * 0.2).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
  //     this.additionalCover.child2premium_acchospitalisation = Number((this.additionalCover.child2benefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
  //     this.additionalCover.child2premium_hospitalcashallowance = Number((this.additionalCover.child2benefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
  //     this.additionalCover.child2premium_loanprotector = Number((this.additionalCover.child2benefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
  //     this.additionalCover.child2premium_adaptationallowance = Number((this.additionalCover.child2benefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
  //     this.additionalCover.child2premium_familytransportallowance = Number((this.additionalCover.child2benefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

  //     if (sessionStorage.child2ShowStatus == "true") {
  //       classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.child1premium_repatriationfuneralexpenses = "";
  //       // this.additionalCover.child2premium_repatriationfuneralexpenses = (this.additionalCover.child2benefitamount_repatriationfuneralexpenses * classvalues) / 1000;
  //     } else {
  //       this.additionalCover.child2premium_repatriationfuneralexpenses = "";
  //     }

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
  //     this.additionalCover.child2premium_brokenbones = Number((this.additionalCover.child2benefitamount_brokenbones * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
  //     this.additionalCover.child2premium_roadambulancecover = Number((this.additionalCover.child2benefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
  //     this.additionalCover.child2premium_airambulancecover = Number((this.additionalCover.child2benefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
  //     this.additionalCover.child2premium_adventuresportbenefit = Number((this.additionalCover.child2benefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

  //     classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
  //     this.additionalCover.child2premium_chauffeurplanbenefit = 119.50;
  //   }

  // }

  addCovers() {
    console.log("pet: " + this.pet);
    // this.showPopup = true;
    // this.showTransBaground = true;

    if (this.selfadaptation == true && this.pet == "Self") {
      this.selfadaptionallowncedivSow = false;
      this.adaptationallownceCheckDiv = false;
      this.spouseadaptionallowncedivSow = false;
      this.spouseadaptationallownceCheckDiv = false;
      this.child1adaptionallowncedivSow = false;
      this.child1adaptationallownceCheckDiv = false;
      this.child2adaptionallowncedivSow = false;
      this.child2adaptationallownceCheckDiv = false;
      this.adaption_allowance = true;
      this.spouse_adaption_allowance = true;
      this.child1_adaption_allowance = true;
      this.child2_adaption_allowance = true;
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;

      console.log("IF self");
    }
    // else{
    //   this.selfadaptionallowncedivSow = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }

    if (this.selfaccidental == true && this.pet == "Self") {
      this.accidentalhospitalDivShow = false;
      this.selfaccidentahospitalisation = false;
      this.spouseaccidentahospitalisation = false;
      this.spouseaccidentalhospitalDivShow = false;
      this.child1accidentahospitalisation = false;
      this.child1accidentalhospitalDivShow = false;
      this.child2accidentahospitalisation = false;
      this.child2accidentalhospitalDivShow = false;
      this.acc_hospitalisation = true;
      this.spouse_acc_hospitalisation = true;
      this.child1_acc_hospitalisation = true;
      this.child2_adaption_allowance = true;
      this.child2_acc_hospitalisation = true;
      this.selfaccidentahospitalisation = false;
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }
    // else{
    //   this.selfaccidentahospitalisation = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }
    if (this.selfmedical == true && this.pet == "Self") {
      this.selfaccidentalmedicalExp = false;
      this.accidentalmedicalExpDivShow = false;
      this.spouseaccidentalmedicalExp = false;
      this.spouseaccidentalmedicalExpDivShow = false;
      this.child1accidentalmedicalExp = false;
      this.child1accidentalmedicalExpDivShow = false;
      this.child2accidentalmedicalExp = false;
      this.child2accidentalmedicalExpDivShow = false;
      this.acc_medical_expenses = true;
      this.spouse_acc_medical_expenses = true;
      this.child2_acc_medical_expenses = true;
      this.child1_acc_medical_expenses = true;

      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }
    // else{
    //   this.selfaccidentalmedicalExp = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }
    if (this.selfchild == true && this.pet == "Self") {
      this.selfchildEdusupport = false;
      this.childeduSuppDivShow = false;
      this.spousechildEdusupport = false;
      this.spousechildeduSuppDivShow = false;
      this.self_child_edu_support = true;
      this.spouse_child_edu_support = true;
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }
    // else{
    //   this.selfchildEdusupport = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }
    if (this.selffamily == true && this.pet == "Self") {
      this.selffamilyTransportAllownce = false;
      this.familyTransAllownceDivShow = false;
      this.spousefamilyTransportAllownce = false;
      this.spousefamilyTransAllownceDivShow = false;
      this.child1familyTransportAllownce = false;
      this.child1familyTransAllownceDivShow = false;
      this.child2familyTransportAllownce = false;
      this.child2familyTransAllownceDivShow = false;
      this.self_family_trans_allowance = true;
      this.spouse_family_trans_allowance = true;
      this.child1_family_trans_allowance = true;
      this.child2_family_trans_allowance = true;
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }
    // else{
    //   this.selffamilyTransportAllownce = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }
    if (this.selfhospital == true && this.pet == "Self") {
      this.selfhospitalCashAllownce = false;
      this.hospicashAllownceDivShow = false;
      this.spousehospitalCashAllownce = false;
      this.spousehospicashAllownceDivShow = false;
      this.child1hospitalCashAllownce = false;
      this.child1hospicashAllownceDivShow = false;
      this.child2hospitalCashAllownce = false;
      this.child2hospicashAllownceDivShow = false;
      this.self_hospital_cash_allownce = true;
      this.spouse_hospital_cash_allownce = true;
      this.child1_hospital_cash_allownce = true;
      this.child2_hospital_cash_allownce = true;
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }
    // else{
    //   this.selfhospitalCashAllownce = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }
    if (this.selflife == true && this.pet == "Self") {
      this.selflifeSuppBenefit = false;
      this.lifeSuppBenefitDivShow = false;
      this.spouselifeSuppBenefit = false;
      this.spouselifeSuppBenefitDivShow = false;
      this.child1lifeSuppBenefit = false;
      this.child1lifeSuppBenefitDivShow = false;
      this.child2lifeSuppBenefit = false;
      this.child2lifeSuppBenefitDivShow = false;

      this.self_life_support_benefit = true;
      this.spouse_life_support_benefit = true;
      this.child1_life_support_benefit = true;
      this.child2_life_support_benefit = true;
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }
    // else{
    //   this.selflifeSuppBenefit = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }
    if (this.selfloanValue == true && this.pet == "Self") {
      this.selfloanProtector = false;
      this.loanProtectorDivShow = false;
      this.spouseloanProtector = false;
      this.spouseloanProtectorDivShow = false;

      this.self_loan_protector = true;
      this.spouse_loan_protector = true;
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }

    if (this.selfBroken == true && this.pet == "Self") {
      this.selfbrokenBones = false;
      this.brokenBonesDivShow = false;
      this.child1brokenBones = false;
      this.child2brokenBones = false;

      this.spousebrokenBones = false;
      this.spousebrokenBonesDivShow = false;
      this.child1brokenBonesDivShow = false;
      this.child2brokenBonesDivShow = false;

      this.self_broken_bones = true;
      this.spouse_broken_bones = true;
      this.child1_broken_bones = true;
      this.child2_broken_bones = true;

      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }

    if (this.selfRoadAmbulance == true && this.pet == "Self") {
      this.selfroadAmbulanceCover = false;
      this.spouseroadAmbulanceCover = false;
      this.child1roadAmbulanceCover = false;
      this.child2roadAmbulanceCover = false;

      this.roadAmbulanceCoverDivShow = false;
      this.spouseroadAmbulanceCoverDivShow = false;
      this.child1roadAmbulanceCoverDivShow = false;
      this.child2roadAmbulanceCoverDivShow = false;

      this.self_road_ambulance_cover = true;
      this.spouse_road_ambulance_cover = true;
      this.child1_road_ambulance_cover = true;
      this.child2_road_ambulance_cover = true;

      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }

    if (this.selfAirAmbulance == true && this.pet == "Self") {
      this.selfairAmbulanceCover = false;
      this.spouseairAmbulanceCover = false;
      this.child1airAmbulanceCover = false;
      this.child2airAmbulanceCover = false;

      this.airambulancecoverDivShow = false;
      this.spouseairambulancecoverDivShow = false;
      this.child1airambulancecoverDivShow = false;
      this.child2airambulancecoverDivShow = false;

      this.self_air_ambulance_cover = true;
      this.spouse_air_ambulance_cover = true;
      this.child1_air_ambulance_cover = true;
      this.child2_air_ambulance_cover = true;

      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }

    if (this.selfAdventureSports == true && this.pet == "Self") {
      this.selfadventureSportsBenefit = false;
      this.spouseadventureSportsBenefit = false;
      this.child1adventureSportsBenefit = false;
      this.child2adventureSportsBenefit = false;

      this.adventureSportsBenefitDivShow = false;
      this.spouseadventureSportsBenefitDivShow = false;
      this.child1adventureSportsBenefitDivShow = false;
      this.child2adventureSportsBenefitDivShow = false;

      this.self_adventure_sports_benefit = true;
      this.spouse_adventure_sports_benefit = true;
      this.child1_adventure_sports_benefit = true;
      this.child2_adventure_sports_benefit = true;

      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }

    if (this.selfChauffeurPlan == true && this.pet == "Self") {
      this.selfchauffeurPlanBenefit = false;
      this.spousechauffeurPlanBenefit = false;
      this.child1chauffeurPlanBenefit = false;
      this.child2chauffeurPlanBenefit = false;

      this.chauffeurPlanBenefitDivShow = false;
      this.spousechauffeurPlanBenefitDivShow = false;
      this.child1chauffeurPlanBenefitDivShow = false;
      this.child2chauffeurPlanBenefitDivShow = false;

      this.self_chauffeur_plan_benefit = true;
      this.spouse_chauffeur_plan_benefit = true;
      this.child1_chauffeur_plan_benefit = true;
      this.child2_chauffeur_plan_benefit = true;

      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
    }
    // else{
    //   this.selfloanProtector = true;
    //   this.showPopupSelf = true;
    //   this.showTransBagroundSelf = true;
    // }
    console.log("status" + this.spouseadaptation);
    if (this.spouseadaptation == true && this.pet == "Spouse") {
      console.log("IF spouse");

      this.spouseadaptionallowncedivSow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spouseadaptionallowncedivSow = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }

    if (this.spouseaccidental == true && this.pet == "Spouse") {
      this.spouseaccidentahospitalisation = false;
      //this.spouseaccidentalhospitalDivShow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spouseaccidentahospitalisation = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }
    if (this.spousemedical == true && this.pet == "Spouse") {
      this.spouseaccidentalmedicalExp = false;
      //this.spouseaccidentalmedicalExpDivShow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spouseaccidentalmedicalExp = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }
    if (this.spousechild == true && this.pet == "Spouse") {
      this.spousechildEdusupport = false;
      //this.spousechildeduSuppDivShow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spousechildEdusupport = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }
    if (this.spousefamily == true && this.pet == "Spouse") {
      this.spousefamilyTransportAllownce = false;
      //this.spousefamilyTransAllownceDivShow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spousefamilyTransportAllownce = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }
    if (this.spousehospital == true && this.pet == "Spouse") {
      this.spousehospitalCashAllownce = false;
      //this.spousehospicashAllownceDivShow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spousehospitalCashAllownce = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }
    if (this.spouselife == true && this.pet == "Spouse") {
      this.spouselifeSuppBenefit = false;
      //this.spouselifeSuppBenefitDivShow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spouselifeSuppBenefit = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }

    if (this.spouseBroken == true && this.pet == "Spouse") {
      this.spousebrokenBones = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }

    if (this.spouseRoadAmbulance == true && this.pet == "Spouse") {
      this.spouseroadAmbulanceCover = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }

    if (this.spouseAirAmbulance == true && this.pet == "Spouse") {
      this.spouseairAmbulanceCover = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }

    if (this.spouseAdventureSports == true && this.pet == "Spouse") {
      this.spouseadventureSportsBenefit = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }

    if (this.spouseChauffeurPlan == true && this.pet == "Spouse") {
      this.spousechauffeurPlanBenefit = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }

    if (this.spouseloanValue == true && this.pet == "Spouse") {
      this.spouseloanProtector = false;
      //this.spouseloanProtectorDivShow = false;
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    }
    // else{
    //   this.spouseloanProtector = true;
    //   this.spouseadaptionallowncedivSow = false;
    //   this.showPopupSpouse = true;
    //   this.showTransBagroundSpouse = true;
    // }

    if (this.child1adaptation == true && this.pet == "Child1") {
      this.child1adaptionallowncedivSow = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }
    // else{
    //   this.child1adaptionallowncedivSow = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }

    if (this.child1accidental == true && this.pet == "Child1") {
      this.child1accidentahospitalisation = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }
    // else{
    //   this.child1accidentahospitalisation = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }
    if (this.child1medical == true && this.pet == "Child1") {
      this.child1accidentalmedicalExp = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }
    // else{
    //   this.child1accidentalmedicalExp = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }
    if (this.child1child == true && this.pet == "Child1") {
      this.child1Edusupport = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }
    // else{
    //   this.child1Edusupport = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }
    if (this.child1family == true && this.pet == "Child1") {
      this.child1familyTransportAllownce = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }
    // else{
    //   this.child1familyTransportAllownce = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }
    if (this.child1hospital == true && this.pet == "Child1") {
      this.child1hospitalCashAllownce = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }
    // else{
    //   this.child1hospitalCashAllownce = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }
    if (this.child1life == true && this.pet == "Child1") {
      this.child1lifeSuppBenefit = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }
    // else{
    //   this.child1lifeSuppBenefit = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }
    if (this.child1loanValue == true && this.pet == "Child1") {
      this.child1loanProtector = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }

    if (this.child1Broken == true && this.pet == "Child1") {
      this.child1brokenBones = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }

    if (this.child1RoadAmbulance == true && this.pet == "Child1") {
      this.child1roadAmbulanceCover = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }

    if (this.child1AirAmbulance == true && this.pet == "Child1") {
      this.child1airAmbulanceCover = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }

    if (this.child1AdventureSports == true && this.pet == "Child1") {
      this.child1adventureSportsBenefit = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }

    if (this.child1ChauffeurPlan == true && this.pet == "Child1") {
      this.child1chauffeurPlanBenefit = false;
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    }

    // else{
    //   this.child1loanProtector = true;
    //   this.showPopupChild1 = true;
    //   this.showTransBagroundChild1 = true;
    // }

    if (this.child2adaptation == true && this.pet == "Child2") {
      console.log("IF spouse");

      this.child2adaptionallowncedivSow = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
    // else{
    //   this.child2adaptionallowncedivSow = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }

    if (this.child2accidental == true && this.pet == "Child2") {
      this.child2accidentahospitalisation = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
    // else{
    //   this.child2accidentahospitalisation = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }
    if (this.child2medical == true && this.pet == "Child2") {
      this.child2accidentalmedicalExp = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
    // else{
    //   this.child2accidentalmedicalExp = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }
    if (this.child2child == true && this.pet == "Child2") {
      this.child2Edusupport = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
    // else{
    //   this.child2Edusupport = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }
    if (this.child2family == true && this.pet == "Child2") {
      this.child2familyTransportAllownce = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
    // else{
    //   this.child2familyTransportAllownce = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }
    if (this.child2hospital == true && this.pet == "Child2") {
      this.child2hospitalCashAllownce = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
    // else{
    //   this.child2hospitalCashAllownce = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }
    if (this.child2life == true && this.pet == "Child2") {
      this.child2lifeSuppBenefit = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }
    // else{
    //   this.child2lifeSuppBenefit = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }
    if (this.child2loanValue == true && this.pet == "Child2") {
      this.child2loanProtector = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }

    if (this.child2Broken == true && this.pet == "Child2") {
      this.child2brokenBones = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }

    if (this.child2RoadAmbulance == true && this.pet == "Child2") {
      this.child2roadAmbulanceCover = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }

    if (this.child2AirAmbulance == true && this.pet == "Child2") {
      this.child2airAmbulanceCover = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }

    if (this.child2AdventureSports == true && this.pet == "Child2") {
      this.child2adventureSportsBenefit = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }

    if (this.child2ChauffeurPlan == true && this.pet == "Child2") {
      this.child2chauffeurPlanBenefit = false;
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }

    // else{
    //   this.child2loanProtector = true;
    //   this.showPopupChild2 = true;
    //   this.showTransBagroundChild2 = true;
    // }
  }

  //Sourov=========================

  showToast(Message) {
    if (this.canShowToast) {
      let pageToast = this.toast.create({
        message: Message,
        showCloseButton: true,
        closeButtonText: "Ok",
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }
  }
}

class InsuredDetails {
  acc_maxinsured: any = "";
  acc_suminsured: any = "";
  acc_premium: any = "";
  temp_acc_maxinsured: any = "";

  ppd_maxinsured: any = "";
  ppd_suminsured: any = "";
  ppd_premium: any = "";
  temp_ppd_maxinsured: any = "";

  ptd_maxinsured: any = "";
  ptd_suminsured: any = "";
  ptd_premium: any = "";
  temp_ptd_maxinsured: any = "";

  ttd_maxinsured: any = "";
  ttd_suminsured: any = "";
  ttd_premium: any = "";
  temp_ttd_maxinsured: any = "";

  additionalCover: AdditionalCover;
  self_totalpremium: any = "";
}

class AdditionalCover {
  selfbenefitamount_adaptationallowance: any = "";
  selfmaxbenefitamount_adaptationallowance;
  selfpremium_adaptationallowance: any = "";
  temp_selfbenefitamount_adaptationallowance: any = "";

  selfbenefitamount_childeducationsupport: any = "";
  selfmaxbenefitamount_childeducationsupport: any = "";
  selfpremium_childeducationsupport: any = "";
  temp_selfbenefitamount_childeducationsupport: any = "";

  selfbenefitamount_familytransportallowance: any = "";
  selfmaxbenefitamount_familytransportallowance: any = "";
  selfpremium_familytransportallowance: any = "";
  temp_selfbenefitamount_familytransportallowance: any = "";

  selfbenefitamount_hospitalcashallowance: any = "";
  selfmaxbenefitamount_hospitalcashallowance: any = "";
  selfpremium_hospitalcashallowance: any = "";
  temp_selfbenefitamount_hospitalcashallowance: any = "";

  selfbenefitamount_loanprotector: any = "";
  selfmaxbenefitamount_loanprotector: any = "";
  selfpremium_loanprotector: any = "";
  temp_selfbenefitamount_loanprotector: any = "";

  selfbenefitamount_lifesupportbenefit: any = "";
  selfmaxbenefitamount_lifesupportbenefit: any = "";
  selfpremium_lifesupportbenefit: any = "";
  temp_selfbenefitamount_lifesupportbenefit: any = "";

  selfbenefitamount_acchospitalisation: any = "";
  selfmaxbenefitamount_acchospitalisation: any = "";
  selfpremium_acchospitalisation: any = "";
  temp_selfbenefitamount_acchospitalisation: any = "";

  selfbenefitamount_accmedicalexpenses: any = "";
  selfpremium_accmedicalexpenses: any = "";
  selfmaxbenefitamount_accmedicalexpenses: any = "";
  temp_selfbenefitamount_accmedicalexpenses: any = "";

  selfbenefitamount_repatriationfuneralexpenses: any = "";
  selfpremium_repatriationfuneralexpenses: any = "";
  selfmaxbenefitamount_repatriationfuneralexpenses: any = "";
  temp_selfbenefitamount_repatriationfuneralexpenses: any = "";

  selfbenefitamount_brokenbones: any = "";
  selfpremium_brokenbones: any = "";
  selfmaxbenefitamount_brokenbones: any = "";
  temp_selfbenefitamount_brokenbones: any = "";

  selfbenefitamount_roadambulancecover: any = "";
  selfpremium_roadambulancecover: any = "";
  selfmaxbenefitamount_roadambulancecover: any = "";
  temp_selfbenefitamount_roadambulancecover: any = "";

  selfbenefitamount_airambulancecover: any = "";
  selfpremium_airambulancecover: any = "";
  selfmaxbenefitamount_airambulancecover: any = "";
  temp_selfbenefitamount_airambulancecover: any = "";

  selfbenefitamount_adventuresportbenefit: any = "";
  selfpremium_adventuresportbenefit: any = "";
  selfmaxbenefitamount_adventuresportbenefit: any = "";
  temp_selfbenefitamount_adventuresportbenefit: any = "";

  selfbenefitamount_chauffeurplanbenefit: any = "";
  selfpremium_chauffeurplanbenefit: any = "";
  selfmaxbenefitamount_chauffeurplanbenefit: any = "";
  temp_selfbenefitamount_chauffeurplanbenefit: any = "";

  spousebenefitamount_adaptationallowance: any = "";
  spousepremium_adaptationallowance: any = "";
  spousemaxbenefitamount_adaptationallowance: any = "";
  temp_spousebenefitamount_adaptationallowance: any = "";

  spousebenefitamount_childeducationsupport: any = "";
  spousepremium_childeducationsupport: any = "";
  spousemaxbenefitamount_childeducationsupport: any = "";
  temp_spousebenefitamount_childeducationsupport: any = "";

  spousebenefitamount_familytransportallowance: any = "";
  spousepremium_familytransportallowance: any = "";
  spousemaxbenefitamount_familytransportallowance: any = "";
  temp_spousebenefitamount_familytransportallowance: any = "";

  spousebenefitamount_hospitalcashallowance: any = "";
  spousepremium_hospitalcashallowance: any = "";
  spousemaxbenefitamount_hospitalcashallowance: any = "";
  temp_spousebenefitamount_hospitalcashallowance: any = "";

  spousebenefitamount_loanprotector: any = "";
  spousepremium_loanprotector: any = "";
  spousemaxbenefitamount_loanprotector: any = "";
  temp_spousebenefitamount_loanprotector: any = "";

  spousebenefitamount_lifesupportbenefit: any = "";
  spousemaxbenefitamount_lifesupportbenefit: any = "";
  spousepremium_lifesupportbenefit: any = "";
  temp_spousebenefitamount_lifesupportbenefit: any = "";

  spousebenefitamount_acchospitalisation: any = "";
  spousepremium_acchospitalisation: any = "";
  spousemaxbenefitamount_acchospitalisation: any = "";
  temp_spousebenefitamount_acchospitalisation: any = "";

  spousebenefitamount_accmedicalexpenses: any = "";
  spousemaxbenefitamount_accmedicalexpenses: any = "";
  spousepremium_accmedicalexpenses: any = "";
  temp_spousebenefitamount_accmedicalexpenses: any = "";

  spousebenefitamount_repatriationfuneralexpenses: any = "";
  spousepremium_repatriationfuneralexpenses: any = "";
  spousemaxbenefitamount_repatriationfuneralexpenses: any = "";
  temp_spousebenefitamount_repatriationfuneralexpenses: any = "";

  spousebenefitamount_brokenbones: any = "";
  spousepremium_brokenbones: any = "";
  spousemaxbenefitamount_brokenbones: any = "";
  temp_spousebenefitamount_brokenbones: any = "";

  spousebenefitamount_roadambulancecover: any = "";
  spousepremium_roadambulancecover: any = "";
  spousemaxbenefitamount_roadambulancecover: any = "";
  temp_spousebenefitamount_roadambulancecover: any = "";

  spousebenefitamount_airambulancecover: any = "";
  spousepremium_airambulancecover: any = "";
  spousemaxbenefitamount_airambulancecover: any = "";
  temp_spousebenefitamount_airambulancecover: any = "";

  spousebenefitamount_adventuresportbenefit: any = "";
  spousepremium_adventuresportbenefit: any = "";
  spousemaxbenefitamount_adventuresportbenefit: any = "";
  temp_spousebenefitamount_adventuresportbenefit: any = "";

  spousebenefitamount_chauffeurplanbenefit: any = "";
  spousepremium_chauffeurplanbenefit: any = "";
  spousemaxbenefitamount_chauffeurplanbenefit: any = "";
  temp_spousebenefitamount_chauffeurplanbenefit: any = "";

  // child1
  child1benefitamount_adaptationallowance: any = "";
  child1premium_adaptationallowance: any = "";
  child1maxbenefitamount_adaptationallowance: any = "";
  temp_child1benefitamount_adaptationallowance: any = "";

  child1benefitamount_childeducationsupport: any = "";
  child1premium_childeducationsupport: any = "";
  child1maxbenefitamount_childeducationsupport: any = "";
  temp_child1benefitamount_childeducationsupport: any = "";

  child1benefitamount_familytransportallowance: any = "";
  child1premium_familytransportallowance: any = "";
  child1maxbenefitamount_familytransportallowance: any = "";
  temp_child1benefitamount_familytransportallowance: any = "";

  child1benefitamount_hospitalcashallowance: any = "";
  child1premium_hospitalcashallowance: any = "";
  child1maxbenefitamount_hospitalcashallowance: any = "";
  temp_child1benefitamount_hospitalcashallowance: any = "";

  child1benefitamount_loanprotector: any = "";
  child1premium_loanprotector: any = "";
  child1maxbenefitamount_loanprotector: any = "";
  temp_child1benefitamount_loanprotector: any = "";

  child1benefitamount_lifesupportbenefit: any = "";
  child1premium_lifesupportbenefit: any = "";
  child1maxbenefitamount_lifesupportbenefit: any = "";
  temp_child1benefitamount_lifesupportbenefit: any = "";

  child1benefitamount_acchospitalisation: any = "";
  child1premium_acchospitalisation: any = "";
  child1maxbenefitamount_acchospitalisation: any = "";
  temp_child1benefitamount_acchospitalisation: any = "";

  child1benefitamount_accmedicalexpenses: any = "";
  child1premium_accmedicalexpenses: any = "";
  child1maxbenefitamount_accmedicalexpenses: any = "";
  temp_child1benefitamount_accmedicalexpenses: any = "";

  child1benefitamount_repatriationfuneralexpenses: any = "";
  child1premium_repatriationfuneralexpenses: any = "";
  child1maxbenefitamount_repatriationfuneralexpenses: any = "";
  temp_child1benefitamount_repatriationfuneralexpenses: any = "";

  child1benefitamount_brokenbones: any = "";
  child1premium_brokenbones: any = "";
  child1maxbenefitamount_brokenbones: any = "";
  temp_child1benefitamount_brokenbones: any = "";

  child1benefitamount_roadambulancecover: any = "";
  child1premium_roadambulancecover: any = "";
  child1maxbenefitamount_roadambulancecover: any = "";
  temp_child1benefitamount_roadambulancecover: any = "";

  child1benefitamount_airambulancecover: any = "";
  child1premium_airambulancecover: any = "";
  child1maxbenefitamount_airambulancecover: any = "";
  temp_child1benefitamount_airambulancecover: any = "";

  child1benefitamount_adventuresportbenefit: any = "";
  child1premium_adventuresportbenefit: any = "";
  child1maxbenefitamount_adventuresportbenefit: any = "";
  temp_child1benefitamount_adventuresportbenefit: any = "";

  child1benefitamount_chauffeurplanbenefit: any = "";
  child1premium_chauffeurplanbenefit: any = "";
  child1maxbenefitamount_chauffeurplanbenefit: any = "";
  temp_child1benefitamount_chauffeurplanbenefit: any = "";

  child2benefitamount_adaptationallowance: any = "";
  child2premium_adaptationallowance: any = "";
  child2maxbenefitamount_adaptationallowance: any = "";
  temp_child2benefitamount_adaptationallowance: any = "";

  child2benefitamount_childeducationsupport: any = "";
  child2premium_childeducationsupport: any = "";
  child2maxbenefitamount_childeducationsupport: any = "";
  temp_child2benefitamount_childeducationsupport: any = "";

  child2benefitamount_familytransportallowance: any = "";
  child2premium_familytransportallowance: any = "";
  child2maxbenefitamount_familytransportallowance: any = "";
  temp_child2benefitamount_familytransportallowance: any = "";

  child2benefitamount_hospitalcashallowance: any = "";
  child2premium_hospitalcashallowance: any = "";
  child2maxbenefitamount_hospitalcashallowance: any = "";
  temp_child2benefitamount_hospitalcashallowance: any = "";

  child2benefitamount_loanprotector: any = "";
  child2premium_loanprotector: any = "";
  child2maxbenefitamount_loanprotector: any = "";
  temp_child2benefitamount_loanprotector: any = "";

  child2benefitamount_lifesupportbenefit: any = "";
  child2premium_lifesupportbenefit: any = "";
  child2maxbenefitamount_lifesupportbenefit: any = "";
  temp_child2benefitamount_lifesupportbenefit: any = "";

  child2benefitamount_acchospitalisation: any = "";
  child2premium_acchospitalisation: any = "";
  child2maxbenefitamount_acchospitalisation: any = "";
  temp_child2benefitamount_acchospitalisation: any = "";

  child2benefitamount_accmedicalexpenses: any = "";
  child2premium_accmedicalexpenses: any = "";
  child2maxbenefitamount_accmedicalexpenses: any = "";
  temp_child2benefitamount_accmedicalexpenses: any = "";

  child2benefitamount_repatriationfuneralexpenses: any = "";
  child2premium_repatriationfuneralexpenses: any = "";
  child2maxbenefitamount_repatriationfuneralexpenses: any = "";
  temp_child2benefitamount_repatriationfuneralexpenses: any = "";

  child2benefitamount_brokenbones: any = "";
  child2premium_brokenbones: any = "";
  child2maxbenefitamount_brokenbones: any = "";
  temp_child2benefitamount_brokenbones: any = "";

  child2benefitamount_roadambulancecover: any = "";
  child2premium_roadambulancecover: any = "";
  child2maxbenefitamount_roadambulancecover: any = "";
  temp_child2benefitamount_roadambulancecover: any = "";

  child2benefitamount_airambulancecover: any = "";
  child2premium_airambulancecover: any = "";
  child2maxbenefitamount_airambulancecover: any = "";
  temp_child2benefitamount_airambulancecover: any = "";

  child2benefitamount_adventuresportbenefit: any = "";
  child2premium_adventuresportbenefit: any = "";
  child2maxbenefitamount_adventuresportbenefit: any = "";
  temp_child2benefitamount_adventuresportbenefit: any = "";

  child2benefitamount_chauffeurplanbenefit: any = "";
  child2maxbenefitamount_chauffeurplanbenefit: any = "";
  child2premium_chauffeurplanbenefit: any = "";
  temp_child2benefitamount_chauffeurplanbenefit: any = "";
}
