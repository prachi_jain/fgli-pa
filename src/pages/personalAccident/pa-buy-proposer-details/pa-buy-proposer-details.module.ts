import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaBuyProposerDetailsPage } from './pa-buy-proposer-details';

@NgModule({
  declarations: [
    //PaBuyProposerDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PaBuyProposerDetailsPage),
  ],
})
export class PaBuyProposerDetailsPageModule {}
