import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { IonicPage,Platform, NavController, NavParams, ToastController, LoadingController,Content, List } from 'ionic-angular'
import { PaBuyHealthDeclarationPage } from '../pa-buy-health-declaration/pa-buy-health-declaration'
import { PaNomineeDetailsPage } from '../pa-nominee-details/pa-nominee-details';
import { LandingScreenPage } from '../../health/landing-screen/landing-screen';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material';
import { PaBuyPolicyReviewPage } from '../pa-buy-policy-review/pa-buy-policy-review'
import { DatePicker } from '@ionic-native/date-picker';
import { getLocaleFirstDayOfWeek } from '@angular/common';
import { AppService } from '../../../providers/app-service/app-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import * as $ from 'jquery';
import { LoginPage } from '../../health/login/login';

/**
 * Generated class for the PaBuyProposerDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pa-buy-proposer-details',
  templateUrl: 'pa-buy-proposer-details.html',
})
export class PaBuyProposerDetailsPage {
  
  @ViewChild(List) list: List;
  loading;
  appData;
  proposerName;
  proposerOccupation = '';
  canShowToast = true;
  customText = "male";
  address;
  pinCode;
  state;
  city;
  email;
  mobile;
  apiState = [];
  apiStateArray;
  checkedAddress;
  corspndncAddressStatus = false;
  corpAddress;
  corpondncPinCode;
  corpondncState;
  corpondncCity;
  pan;
  stateCode;
  nameOfProposer= '';
  memberTitle;
  memberOccupation='';
  selectedCardArray;
  occupationList =[];
  occupation;
  occupationOther;
  hideShowDiv = true;
  forQuickQuote = true;
  activeMember = "SELF";
  activeMemberIndex = 0;
  pID;
  emailPattern;
  ReferenceNo;
  pet = "Self";
  disableProceedBt = true;
  segmentactive = false;
  inactive = false;
  SelfDob;
  memberDOB;
  selfMaritalStatus;
  otherGender = false;
  salutationList = [];
  ClientDetails = [];
  pinData;
  thumbnail;
  ImageData;
  imageCaptured = true;
  imagePOp = true;
  saveandEmailHideShowProposer = true;
  saveandEmailHideShowOther = true;
  fromPage;
  testVar = false;
  currentNumber = 0;
  thankyouPopup=true;
  insuranceDetailsToSend;

  memberArray;
  marriedStatusArray = [{value: "Single", status: false},{value: "Married", status: true},{value: "Divorced", status: false},{value: "Widow / Widower", status: false}];
  selfAnnualIncome
  isActive = true;
  otherAnnualIncome


  hideDivChild = false

  regexEmail(email){
    let emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    return emailPattern.match(this.email);
  }
  // = [{"showMedicalQuestions":true,"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","age":"1993-03-02","ageText":"25 years","insuredCode":"si004","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P002","insuredPlanTypeText":"SUPERIOR","insuredAmtInDigit":"","Height":"96.520","HeightText":"3' 2\"","Weight":"5","ProposerDetailsDivShow":true,"medicalDeclaration":false,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"nameOfProposer":"TEST","Gender":"M","genderText":"Male"},{"showMedicalQuestions":true,"nameOfProposer":"test3","code":"SPOU","title":"Spouse","smoking":true,"smokingText":"Smoking","popupType":"1","showMarried":false,"maritalStatus":"","age":"2001-03-02","ageText":"17 years","insuredCode":"si004","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P002","insuredPlanTypeText":"SUPERIOR","insuredAmtInDigit":"","Height":"96.520","HeightText":"3' 2\"","Weight":"3","ProposerDetailsDivShow":false,"medicalDeclaration":false,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true}];

  today = new Date();
  maxDate = (this.today.getFullYear()+"-"+this.numberLenght(this.today.getMonth())+"-"+this.numberLenght(this.today.getDate())).toString();
  
  minDate = '1900-01-01';
  policyType;
  ENQ_PolicyResponse;
  response;
  QuotationID;
  UID;
  marriedStatus

  isPinValid:boolean = true;
  serviceResponsePopup = true;
  serviceCodeStatus;
  message;
  hideShowSpouseOccupation = true
  spouseOccupation = ""
  numberLenght(Num){if(Num>=10){return Num}else{return"0"+Num}}
  occupationListChild = []

  
  constructor(private platform: Platform, private base64: Base64, public camera: Camera, public navCtrl: NavController,  private cdr: ChangeDetectorRef, private datePicker: DatePicker,
    public navParams: NavParams, public toast: ToastController, public appService: AppService, private loadingCtrl : LoadingController) {
    this.appData = JSON.parse(localStorage.AppData);  
   
    this.occupationList =  this.appData.Masters.PA_Occupation;
    this.occupationListChild = this.appData.Masters.Occupation;



    this.salutationList = this.appData.Masters.Salutations;
    this.memberArray = navParams.get("buyPageMemberDetailsResult"); // MEMBER INSURED
    this.selectedCardArray = navParams.get("buyPageCardDetailsResult");// BENEFICIARY ARRAY
    this.ReferenceNo = navParams.get("ReferenceNo");
    
    console.log("card: " + JSON.stringify(this.selectedCardArray));
    console.log("member: " + JSON.stringify(this.memberArray));
    this.policyType = navParams.get("PolicyType");
    this.ENQ_PolicyResponse = navParams.get("ENQ_PolicyResponse");
    this.response = this.ENQ_PolicyResponse.PurchaseResponse[0].Result_pa
    
    this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
    this.UID = this.ENQ_PolicyResponse.PurchaseResponse[0].UID;
    this.insuranceDetailsToSend = this.navParams.get("insuranceDetailsToSend");
    this.fromPage = this.navParams.get("fromDashboardListing");
    console.log("fromPage: " + this.fromPage);
    this.pet = "Self";



  
  }

  ionViewDidLoad() {   
   
    this.pet = "Self";
    if(this.fromPage == "true"){
      this.proposerName = this.memberArray[0].proposerName;
      this.email = this.memberArray[0].proposerEmail;
      this.mobile = this.memberArray[0].mobile;
      this.selfMaritalStatus = this.memberArray[0].showMarried;
      this.SelfDob = this.memberArray[0].age;
      this.address = this.memberArray[0].address;
      this.pinCode = this.memberArray[0].pincode;
      this.state = this.memberArray[0].state;
      this.city = this.memberArray[0].city;
      this.pan =  this.memberArray[0].pan;
     
      this.selfAnnualIncome = this.memberArray[0].annualIncome;
      this.marriedStatus = this.memberArray[0].maritalStatus;
    }else{
      this.proposerName = sessionStorage.propName;
      this.email = sessionStorage.email;
      this.mobile = sessionStorage.mobile;
      this.SelfDob = this.memberArray[0].age;
      this.selfAnnualIncome = this.memberArray[0].annualIncome;
      this.marriedStatus = this.memberArray[0].maritalStatus;
    }   

    this.proposerOccupation = this.memberArray[0].occupationName;

    if(this.memberArray[0].maritalStatus == "Married"){
    this.spouseOccupation = this.memberArray[1].occupationName;
    }else{
      this.spouseOccupation = ""

    }


    for(let i = 0 ; i < this.memberArray.length ; i++){
      if(this.memberArray[i].title == "Spouse"){
        this.hideShowSpouseOccupation = true
        break;
      }else if(this.memberArray[i].title == "Child1" || this.memberArray[i].title == "Child2"){
        this.hideShowSpouseOccupation = false
        break;
      }
    
    }
    console.log('ionViewDidLoad BuyProposerDetailsPage');  
    //this.loadState();
    this.pet = "Self";
    this.thumbnail = "assets/imgs/proposer_profile_pic.svg";  
    this.forQuickQuote = this.memberArray.length == 1 ? false : true;
    this.saveandEmailHideShowProposer = this.memberArray.length == 1 ? false : true;  

    
    console.log("back State: " + sessionStorage.isViewPopped);
    
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
      console.log("MemberA:" + JSON.stringify(this.memberArray));     
      this.memberArray[0].proposerName =  sessionStorage.propName;
      this.memberArray[0].proposerEmail = sessionStorage.email;
      this.memberArray[0].mobile = sessionStorage.mobile;


      
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);          
      this.pet = "Self";
      this.hideShowDiv = true;
      this.setStoredData(0);
      sessionStorage.isViewPopped = ""; 
    }  
    if(this.memberArray[0].GenderText == "M"){

      this.isActive = true;
      
    }
    else if(this.memberArray[0].GenderText == "F"){
      this.isActive = false;


    }
  }

  ionViewDidEnter(){
    console.log("back State: " + sessionStorage.isViewPopped);
    this.thumbnail = "assets/imgs/proposer_profile_pic.svg";
   
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
    
      console.log("BACK: " + JSON.stringify(this.memberArray));
      
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);     
     
     
      this.pet = "Self";
      this.forQuickQuote = true;
      this.hideShowDiv = true;
      this.setStoredData(0);

      sessionStorage.isViewPopped = ""; 

      this.activeMemberIndex = 0;




      if(this.memberArray.length == 1){

        this.forQuickQuote = false;

      }else{

        this.forQuickQuote = true;

      }

      // }
    }  
  }


  setStoredData(i){
    //for(let i=0; i< this.memberArray.length; i++){  
  
      if(i == 0){
        this.proposerName = this.memberArray[i].proposerName;
        this.email = this.memberArray[i].proposerEmail;
        this.mobile = this.memberArray[i].mobile;
        this.SelfDob = this.memberArray[i].age;
        this.selfAnnualIncome = this.memberArray[i].annualIncome;
        this.marriedStatus = this.memberArray[i].maritalStatus;

        if(this.memberArray[i].maritalCode == "M"){
          this.selfMaritalStatus = true;
        }   
        if(this.memberArray[0].GenderText == "M"){

          this.isActive = true;
          
        }
        else if(this.memberArray[0].GenderText == "F"){
          this.isActive = false;
  
  
        }
        this.address = this.memberArray[i].address;
        this.pinCode = this.memberArray[i].pincode;
        this.state = this.memberArray[i].state;
        this.city = this.memberArray[i].city;
        this.pan =  this.memberArray[i].pan;
        this.proposerOccupation = this.memberArray[i].occupationName;
        this.disableProceedBt = false;
        
      }else{
        console.log("propName: " + JSON.stringify(this.memberArray[i].proposerName));
        console.log(this.memberArray[i].proposerName != undefined);
        //this.pet = "Self";
        
        this.nameOfProposer = this.memberArray[i].proposerName != undefined ? this.memberArray[i].proposerName : "";
        this.memberDOB = this.memberArray[i].age != undefined ? this.memberArray[i].age : "";
        this.memberOccupation = this.memberArray[i].occupationName != undefined ? this.memberArray[i].occupationName : "";
        this.otherAnnualIncome = this.memberArray[i].annualIncome != undefined ? this.memberArray[i].annualIncome : "";
        this.disableProceedBt = false;
      }
    //}
  }
  
  
  genderChange(){
    console.log("Toggled: "+ this.customText);
  }

  changePinCode(){   
    
   
    this.pinData = {"pin": this.pinCode} 
    var sendData = this.appService.encryptData(JSON.stringify(this.pinData),sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.stateCityService("FGHealth.svc/GetPinDetails", {'request': sendData});
  }

    /* call state city service */
    stateCityService(URL,serviceData){
      this.presentLoadingDefault();
      //this.appService.presentServiceLoading();
      let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService(URL,serviceData,headerString)
        .subscribe(data =>{
         //this.loading.dismiss();       
          console.log();
          if(data && data.GetPinDetailsResult.ReturnCode == "0"){
            this.isPinValid = true;
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            //this.showToast(data.GetPinDetailsResult.ReturnMsg); 
            this.state = data.GetPinDetailsResult.Data.state;
            this.city = data.GetPinDetailsResult.Data.city;
            this.loading.dismiss();
          }else if(data.GetPinDetailsResult.ReturnCode == "807"){
            this.isPinValid = true;
            this.state = "";
            this.city = "";;
           // this.showToast(data.GetPinDetailsResult.ReturnMsg);
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
           // this.navCtrl.push(LoginPage);
           this.message = data.GetPinDetailsResult.ReturnMsg;
           this.serviceResponsePopup = false;
           this.serviceCodeStatus = 807;
          }else if(data.GetPinDetailsResult.ReturnCode == "500"){

            this.isPinValid = true;
            this.state = "";
            this.city = "";
           // this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 500;
            //this.navCtrl.push(LoginPage);
          } else{
            sessionStorage.TokenId = data.GetPinDetailsResult.UserToken.TokenId;
            this.isPinValid = true;
            this.state = "";
            this.city = "";
            //this.showToast(data.GetPinDetailsResult.ReturnMsg); 
            this.loading.dismiss();
            this.message = data.GetPinDetailsResult.ReturnMsg;
            this.serviceResponsePopup = false;
            this.serviceCodeStatus = 400;
          }
        
        });
    }

  addressChange(){
    console.log("Toggled: "+ this.checkedAddress);
    if(this.corspndncAddressStatus == false){
      this.corspndncAddressStatus = true;
    }else{
      this.corspndncAddressStatus = false;
    }
  }

  proceedNomineeDetailsPage(){
   
    if(this.memberArray.length == 1){    
     

      if (this.proposerName == undefined || this.proposerName == null || this.proposerName == "") {     
        this.showToast("Please enter proposer name to proceed");              
      } else if (this.address == undefined || this.address == null || this.address == "") {     
            this.showToast("Please enter your address to proceed");                             
      } else  if (this.pinCode == undefined || this.pinCode == null || this.pinCode == "") {     
            this.showToast("Please enter your pincode to proceed");                         
      } else if(this.pinCode.length<6){
            this.showToast("Please enter 6 digit pincode");          
      } else if (this.state == undefined || this.state == null || this.state == "") {     
            this.showToast("Please enter your state to proceed");                            
      } else if (this.city == undefined || this.city == null || this.city == "") {     
            this.showToast("Please enter your city to proceed");                             
      } else if (this.email == undefined || this.email == null || this.email == "") { 
            this.showToast("Please enter your email Id to proceed");           
      } else if (!this.matchEmail(this.email)) {
        this.showToast("Please enter valid email id");   
        }
      else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
              this.showToast("Please enter mobile number to proceed");                              
      } else if(this.mobile.length < 10){
              this.showToast("Please enter 10 digit mobile number"); 
              
              

      }
      else if(this.selfAnnualIncome == undefined || this.selfAnnualIncome == ''){
        this.showToast("Please enter annual income to proceed");     
    }
      else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
          this.showToast("Please select occupation to proceed");     
      } 
       /*else if ( this.pan == undefined || this.pan == null || this.pan == "") { 
          this.showToast("Please enter your PAN to proceed");      
      } else if (!this.matchPan(this.pan)) {
          this.showToast("Please enter valid pan");     
      } else if(this.imageCaptured == true){
        this.showToast("Please click image of Proposer");   
      }*/else{
        this.disableProceedBt = true;          
        this.getMemberContactDetails();      
       
        this.encryptIncompleteData();
      }
    }else{
      if(this.nameOfProposer == ""){
        this.showToast("Enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
       }else if(this.memberOccupation == ''){
         this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
       }else if((this.memberArray[this.activeMemberIndex].title == "Child1" || this.memberArray[this.activeMemberIndex].title == "Child2") && (this.memberOccupation == "Businessman/Businesswoman" || this.memberOccupation == "Housewife/Househusband" || this.memberOccupation == "Retired" || this.memberOccupation == "Salaried" || this.memberOccupation == "Self Employed Professional")){
        this.showToast("Only dependent childs are covered");
      }else if(this.selectedCardArray[0].CoverType == "VITAL" && (this.memberArray[this.activeMemberIndex].title == "Father" || this.memberArray[this.activeMemberIndex].title == "Mother") && (this.memberOccupation == "BUSM" || this.memberOccupation == "RETR" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS" || this.memberOccupation == "STDN")){
        this.showToast("Only dependent parents are covered");
      }else{           
         this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
         this.memberArray[this.activeMemberIndex].occupationName = this.memberOccupation;
         this.memberArray[this.activeMemberIndex].age = this.memberDOB;

        if(this.customText == "male" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.customText == "female" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }else if(this.memberArray[this.activeMemberIndex].title == "Son"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }else if(this.memberArray[this.activeMemberIndex].title == "Daughter"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.memberArray[this.activeMemberIndex].title == "Father"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }else if(this.memberArray[this.activeMemberIndex].title == "Mother"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.memberArray[this.activeMemberIndex].title == "Grand Mother"){
          this.memberArray[this.activeMemberIndex].Gender = 'F';
          this.memberArray[this.activeMemberIndex].genderText = 'Female';
        }else if(this.memberArray[this.activeMemberIndex].title == "Grand Father"){
          this.memberArray[this.activeMemberIndex].Gender = 'M';
          this.memberArray[this.activeMemberIndex].genderText = 'Male';
        }      
        
        console.log("member: " + JSON.stringify(this.memberArray));
        
  
        
        if(this.memberArray[this.activeMemberIndex].title == "Spouse"){
          
          this.memberArray[this.activeMemberIndex].occupationName = this.spouseOccupation;
      
        for(let j=0; j<this.occupationList.length; j++){
          if(this.memberArray[this.activeMemberIndex].occupationName == this.occupationList[j].occupationName){
          this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].occupationName;
          break;
          }
          }
        }
        else if(this.memberArray[this.activeMemberIndex].title == "Child1" || this.memberArray[this.activeMemberIndex].title == "Child2"){
          this.memberArray[this.activeMemberIndex].occupationName = this.memberOccupation;
          for(let j=0; j<this.occupationListChild.length; j++){
            if(this.memberArray[this.activeMemberIndex].occupationName == this.occupationListChild[j].OccTitle){
            this.memberArray[this.activeMemberIndex].occupation = this.occupationListChild[j].OccTitle;
            break;
            }
            }
          }
         this.getMemberContactDetails();   
         
        this.encryptIncompleteData();
        //  this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
       }
    }  

  }

  genderClick(gender){

    if(sessionStorage.isViewPopped == "true"){
      if(gender == "male"){
        //this.customText = $(".male").attr('rel');
        this.customText = "male";
        this.memberArray[0].gender = "M"
        sessionStorage.gender = "M"

      }else{
        //this.customText = $(".female").attr('rel');           
        this.customText = "female";
        this.memberArray[0].gender = "F"
        sessionStorage.gender = "F"
      }
    }else{
      if(gender == "male"){
        //this.customText = $(".male").attr('rel');
        this.customText = "male";
        this.memberArray[0].gender = "M"
        sessionStorage.gender = "M"
      }else{
        //this.customText = $(".female").attr('rel');           
        this.customText = "female";
        this.memberArray[0].gender = "F"
        sessionStorage.gender = "F"
      }
    }

  
    this.isActive = !this.isActive;
  } 
  getMemberContactDetails(){
    var contactDetails = {};
    this.memberArray[0].proposerName = this.proposerName;


    if(this.customText == "male"){
      this.memberArray[0].Gender = 'M';
      this.memberArray[0].genderText = 'Male';
    }else{
      this.memberArray[0].Gender = 'F';
      this.memberArray[0].genderText = 'Female';
    }


   
    this.memberArray[0].age = this.SelfDob;
    this.memberArray[0].ageText = this.getAge(this.SelfDob);
    if(this.selfMaritalStatus){
      this.memberArray[0].maritalStatus = "Married";
      this.memberArray[0].maritalCode = "M"   
    }else{
      //this.memberArray[0].maritalStatus = "Single"; 
      this.memberArray[0].maritalCode = "S"       
    }    
    this.memberArray[0].address = this.address;
    this.memberArray[0].pincode = this.pinCode;
    this.memberArray[0].state = this.state;
    this.memberArray[0].city = this.city;
    this.memberArray[0].proposerEmail = this.email;
    this.memberArray[0].mobile = this.mobile;
    this.memberArray[0].corpAddress = this.corpAddress;
    this.memberArray[0].corpPinCode = this.corpondncPinCode;
    this.memberArray[0].corpState = this.corpondncState;
    this.memberArray[0].corpCity = this.corpondncCity;
    this.memberArray[0].pan = (this.pan).toUpperCase();
    this.memberArray[0].occupationName = this.proposerOccupation;


      for(let j=0; j<this.occupationList.length; j++){
        if(this.memberArray[0].occupationName == this.occupationList[j].occupationName){
          this.memberArray[0].occupation = this.occupationList[j].occupationName;
          break;
        }
      }

    console.log("Updated :  "+ JSON.stringify(this.memberArray));
  }


  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }
  serviceResponseOkButton(){
    if(this.serviceCodeStatus == 807){
      this.serviceResponsePopup = true;
      this.navCtrl.push(LoginPage);   
    }else if(this.serviceCodeStatus == 500 || this.serviceCodeStatus == 400){
      this.serviceResponsePopup = true;     
    }
  
  }

  // onChange(value) {
  //   this.pan = value;
  //  }

  onSelectChangeSelf() {
    console.log('Selected self', this.occupation);
   
      this.memberArray[0].occupation = this.occupation;    
  
  }
  onSelectChangeOther(){

    for(let i=0; i<this.memberArray.length;i++){
      this.memberArray[i].occupationOther = this.occupationOther;
    }

    console.log('Selected other', this.occupationOther);
  }

  continueButtonClick(){

    console.log("active: " + this.activeMemberIndex);
    

    this.emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    let panPattern = "[A-Za-z]{5}\d{4}[A-Za-z]{1}";
    console.log("1: " + this.activeMemberIndex + "2: " + (this.memberArray.length - 1));



    if(this.activeMemberIndex == (this.memberArray.length-1)){
      this.forQuickQuote = false;
      this.saveandEmailHideShowProposer = true;
      this.saveandEmailHideShowOther = false;
    }

   
      

     if(this.activeMemberIndex == 0){
        let proceed = true;
        if (this.proposerName == undefined || this.proposerName == null || this.proposerName == "") {     
              this.showToast("Please enter proposer name to proceed");
              proceed = false;
              this.segmentactive = true;
        } else if (this.address == undefined || this.address == null || this.address == "") {     
              this.showToast("Please enter your address to proceed");                   
              proceed = false;  
              this.segmentactive = true; 
        } else  if (this.pinCode == undefined || this.pinCode == null || this.pinCode == "") {     
              this.showToast("Please enter your pincode to proceed");                   
              proceed = false;   
              this.segmentactive = true;
        } else if(this.pinCode.length<6){
              this.showToast("Please enter 6 digit pincode");
              proceed = false;  
              this.segmentactive = true; 
        } else if (this.state == undefined || this.state == null || this.state == "") {     
              this.showToast("Please enter your state to proceed");                    
              proceed = false;   
              this.segmentactive = true;
        } else if (this.city == undefined || this.city == null || this.city == "") {     
              this.showToast("Please enter your city to proceed");                    
              proceed = false;  
              this.segmentactive = true; 
        } else if (this.email == undefined || this.email == null || this.email == "") { 
              this.showToast("Please enter your email Id to proceed");
              proceed = false;   
              this.segmentactive = true;
        } else if (!this.matchEmail(this.email)) {
          this.showToast("Please enter valid email id");
          proceed = false; 
          this.segmentactive = true;
          }
         else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
                this.showToast("Please enter mobile number to proceed");                   
                proceed = false;   
                this.segmentactive = true;
        } else if(this.mobile.length < 10){
                this.showToast("Please enter 10 digit mobile number");
                proceed = false;   
                this.segmentactive = true;
        } else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
            this.showToast("Please select occupation to proceed");
            proceed = false;
            this.segmentactive = true;
        } 
        /*else if ( this.pan == undefined || this.pan == null || this.pan == "") { 
            this.showToast("Please enter your PAN to proceed");
            proceed = false;
            this.segmentactive = true;
        } else if (!this.matchPan(this.pan)) {
            this.showToast("Please enter valid pan");
            proceed = false;
            this.segmentactive = true;
        } else if(this.imageCaptured == true){
            this.showToast("Please click image of Proposer");
            proceed = false;
        }*/
       else if(proceed){ 

          //this.segmentactive = false;

          this.disableProceedBt = true;          
          this.getMemberContactDetails();
          this.activeMemberIndex++;
          this.pet = this.memberArray[this.activeMemberIndex].title;
          this.memberTitle = this.memberArray[this.activeMemberIndex].title;
          this.memberDOB = this.memberArray[this.activeMemberIndex].age;
          this.setStoredData(this.activeMemberIndex);
          this.hideShowDiv = false;
        
           
        
            if(this.memberArray[this.activeMemberIndex].title == "Spouse"){
              this.hideShowSpouseOccupation = true
              
            }else if(this.memberArray[this.activeMemberIndex].title == "Child1" || this.memberArray[this.activeMemberIndex].title == "Child2"){
              this.hideShowSpouseOccupation = false
          
            }
          
          

          if(this.memberArray.length == 2){
            this.forQuickQuote = false;
            this.saveandEmailHideShowProposer = true;
            this.saveandEmailHideShowOther = false;
          }
        }else{

        }  
        
     } 
     
     
     
     else{
      for(let i=1; i<this.memberArray.length;i++){
        //if(this.memberArray[i].title == "")
        

        
        if(this.activeMemberIndex == i){
         
          //this.nameOfProposer = "";
          //this.memberOccupation = "";
          if(this.nameOfProposer == ""){
           this.showToast("Please enter "+this.memberArray[i].title+" name");
           this.segmentactive = true;
          }else if(this.memberOccupation == '' ){
            this.showToast("Please select "+this.memberArray[i].title+" occupation");
          }else if((this.memberArray[this.activeMemberIndex].title == "Child1" || this.memberArray[this.activeMemberIndex].title == "Child2") && (this.memberOccupation == "Businessman/Businesswoman" || this.memberOccupation == "Housewife/Househusband" || this.memberOccupation == "Retired" || this.memberOccupation == "Salaried" || this.memberOccupation == "Self Employed Professional")){
            this.showToast("Only dependent childs are covered");
          }else{           
            this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
            this.memberArray[this.activeMemberIndex].age = this.memberDOB;
            this.memberArray[this.activeMemberIndex].ageText = this.getAge(this.memberDOB);
            
            if(i == 1){
              this.memberArray[this.activeMemberIndex].occupationName = this.spouseOccupation;
            }
             else{

                this.memberArray[this.activeMemberIndex].occupationName = this.memberOccupation;
              }
            


            if(this.customText == "male" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }else if(this.customText == "female" && this.memberArray[this.activeMemberIndex].title == "Spouse"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Son"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }else if(this.memberArray[this.activeMemberIndex].title == "Daughter"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Father"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }else if(this.memberArray[this.activeMemberIndex].title == "Mother"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Grand Mother"){
              this.memberArray[this.activeMemberIndex].Gender = 'F';
              this.memberArray[this.activeMemberIndex].genderText = 'Female';
            }else if(this.memberArray[this.activeMemberIndex].title == "Grand Father"){
              this.memberArray[this.activeMemberIndex].Gender = 'M';
              this.memberArray[this.activeMemberIndex].genderText = 'Male';
            }              
            
            if(this.memberArray[this.activeMemberIndex].title == "Spouse"){
            for(let j=0; j<this.occupationList.length; j++){
              if(this.memberArray[this.activeMemberIndex].occupationName == this.occupationList[j].occupationName){
              this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].occupationName;
              break;
              }
              }
            }
            else if(this.memberArray[this.activeMemberIndex].title == "Child1" || this.memberArray[this.activeMemberIndex].title == "Child2"){
              for(let j=0; j<this.occupationListChild.length; j++){
                if(this.memberArray[this.activeMemberIndex].occupationName == this.occupationListChild[j].OccTitle){
                this.memberArray[this.activeMemberIndex].occupation = this.occupationListChild[j].OccTitle;
                break;
                }
                }
              }
            i++;
            if( i < this.memberArray.length){
              this.disableProceedBt = true;
              this.nameOfProposer = "";
              this.memberOccupation ="";
              this.activeMemberIndex++;  
              this.pet = this.memberArray[this.activeMemberIndex].title;


              if(this.memberArray[this.activeMemberIndex].title == "Spouse"){
                this.hideShowSpouseOccupation = true
               
                
              }else if(this.memberArray[this.activeMemberIndex].title == "Child1" || this.memberArray[this.activeMemberIndex].title == "Child2"){
                this.hideShowSpouseOccupation = false

               
            
              }
              if(this.pet == "Child1" || this.pet == "Child2"){

                this.hideDivChild = true
              }else{

                this.hideDivChild = false
              }
              console.log("activeMembers: " + this.pet);
              this.setStoredData(this.activeMemberIndex);

              this.memberTitle = this.memberArray[this.activeMemberIndex].title; 
              this.memberDOB = this.memberArray[this.activeMemberIndex].age;
              //this.memberOccupation ="";

              if(this.activeMemberIndex == (this.memberArray.length - 1)){
                this.forQuickQuote = false;
                //this.disableProceedBt = false;
                this.saveandEmailHideShowProposer = true;
                this.saveandEmailHideShowOther = false;
              }
            
            }/* else{
              this.forQuickQuote = false;
            }   */      
                   
          }
          break;
        }      
      }
     }  
    
  }
  
  

  matchEmail(email){
    
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  matchPan(pan){
    var regex = /[A-Za-z]{5}\d{4}[A-Za-z]{1}/;
    return regex.test(pan);
  }

  checkFocus(){  
    if((this.activeMemberIndex == 0 && (this.pan != undefined || this.pan != null || this.pan != "")) || this.activeMemberIndex == this.memberArray.length - 1){
      this.disableProceedBt = false;
    }
  }

  otherFocusOut(){
    this.disableProceedBt = false;
  }

  DatePick(event, member){   
    //this.keyboard.hide()();
    // let element = (<HTMLInputElement>event.target.previousElementSibling.querySelector('input[name="nomineeDOB"]'));
    let element = this.SelfDob;
    let dateFieldValue = element;

    let year: any = 1985, month: any = 1, date: any = 1;
    if (dateFieldValue != "" || dateFieldValue == undefined) {
      date = dateFieldValue.split("-")[0]
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }


    let minDateLimit  = ( new Date(1900,1,1).getTime());  
      this.datePicker.show({
        date: new Date(year, month - 1, date),
        minDate: minDateLimit,
        maxDate: Date.now(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
      }).then(
        date => {
          let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
          this.SelfDob = dt.toString();
          console.log(this.SelfDob);
          //this.memberArray[0].age = this.SelfDob;
        },
        err => console.log('Error occurred while getting date: ', err)
      );
  }
  DatePickOtherMember(event, members, index){
    console.log("index: " + index);
    
    let element = this.memberDOB;
    let dateFieldValue = element;

    let year: any = 1985, month: any = 1, date: any = 1;
    if (dateFieldValue != "" || dateFieldValue == undefined) {
      date = dateFieldValue.split("-")[0];
      month = dateFieldValue.split("-")[1];
      year = dateFieldValue.split("-")[2]
    }

    let minDateLimit  = ( new Date(1900,1,1).getTime());  
      this.datePicker.show({
        date: new Date(year, month-1, date),
        minDate: minDateLimit,
        maxDate: Date.now(),
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
      }).then(
        date => {
          let dt = ( this.numberLenght(date.getDate())+"-"+this.numberLenght(date.getMonth() + 1)+"-"+(date.getFullYear()) ).toString();
          this.memberDOB = dt.toString();
          console.log(this.memberDOB);
        },
        err => console.log('Error occurred while getting date: ', err)
      );
  }


  onDateChange(ev,members){

    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.SelfDob+"-";
   
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){

      let age : any= this.getAge(this.SelfDob);    
      if(this.invalidDob(this.SelfDob)){
        this.showToast('Please enter valid DOB.');
      }
      //this.keyboard.hide()();
    }
  }


  onDateOtherChange(ev,members){
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      ev.target.value = this.memberDOB+"-";
   
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(this.memberDOB);
      this.memberArray[this.activeMemberIndex].age = this.memberDOB;
      if(this.invalidDob(this.memberDOB)){
        this.showToast('Please enter valid DOB.');
      }
      //this.keyboard.hide()();
    }
  }

  invalidDob(userDoB){
    let today = new Date();
    let uDob = userDoB.split("-");
    let uDte = ( this.numberLenght(Number(uDob[1]))+"-"+this.numberLenght( Number(uDob[0]))+"-"+(uDob[2]) ).toString();
    let usrDob = new Date(uDte);    
    if(usrDob.toString() == "Invalid Date"){
      return true;
    }else if(usrDob > today){
      return true;
    }
    return false;
  }

  getAge(date){
    let dateVar = date.split("-");
    let ag = (Date.now()-(new Date(dateVar[2],dateVar[1]-1,dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24))/365)+" years";
  }




  encryptIncompleteData(){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }



  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          sessionStorage.policyID = this.pID;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          //sessionStorage.isViewPopped = undefined;

          this.navCtrl.push(PaNomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray,"PolicyType": this.policyType , "ENQ_PolicyResponse": this.ENQ_PolicyResponse ,"beneficiaryDetails": this.selectedCardArray,"insuranceDetailsToSend":this.insuranceDetailsToSend,"ReferenceNo" : this.ReferenceNo});
          console.log("cardProp: " + JSON.stringify(this.selectedCardArray));
          console.log("memberFromProposer: " + JSON.stringify(this.memberArray));
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.loading.dismiss();
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": this.QuotationID,
      "UID": this.UID,
      "stage":"2",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.policyType ,//"HTF",
        "Duration":  sessionStorage.Duration,
        "Installments": sessionStorage.Installments,
        "IsfgEmployee": sessionStorage.IsFGEmployee,
        "IsPos": sessionStorage.Ispos,
        "BeneficiaryDetails": this.selectedCardArray.length == undefined ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberArray,
        "CardDetails": this.insuranceDetailsToSend
      }
    }
    console.log("Request data: "+JSON.stringify(sendRequest));
    return sendRequest;
  }
  
  getClientNode(){
    let age = this.SelfDob;
    let clientSalution = "MR";
    if(this.memberArray[0].Gender == "F"){
      clientSalution = "MRS";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.proposerName ,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberArray[0].Gender,
      "MaritalStatus": this.memberArray[0].maritalCode,
      "Occupation": this.memberArray[0].occupationCode,
      "PANNo": this.memberArray[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.pinCode,
        "City": this.city,
        "State": this.state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.mobile,
        "EmailAddr": this.email
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.pinCode,
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.mobile,
        "EmailAddr": this.email
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
    //console.log("clientData: " + JSON.stringify(this.ClientDetails));
    
  }
  
  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberArray[0].Gender;

    BeneDetails.push({
      "MemberId": cardDetails.MemberId ,
      "InsuredName": this.memberArray[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberArray[0].occupationCode,
      "CoverType": cardDetails.CoverType,
      "SumInsured": cardDetails.SumInsured,
      "DeductibleDiscount": cardDetails.DeductibleDiscount,
      "Relation": cardDetails.Relation,
      "NomineeName": "",
      "NomineeRelation": "",
      "AnualIncome": cardDetails.annualIncome,
      "Height": cardDetails.Height,
      "Weight": cardDetails.Weight,
      "NomineeAge": "",
      "AppointeeName": "",
      "AptRelWithominee": "",
      "MedicalLoading": cardDetails.MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      },
      "PrimaryCover": cardDetails[0].PrimaryCover,
      "SecondaryCover" : cardDetails[0].SecondaryCover
    })
    return BeneDetails;
  }

  getBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    
    for(let i = 0 ;i <= cardDetails.length-1 ; i++){
      for(let j = 0; j <=this.memberArray.length-1;j++){
        if(i==j){
          if( (this.memberArray[j].code).toUpperCase() == (cardDetails[i].Relation).toUpperCase() ){
          
            let mDob = cardDetails[i].InsuredDob.split(" ")[0].split("/");
            let gendr = this.memberArray[j].Gender;
            console.log("Gender: " + this.memberArray[j].Gender);
            
            BeneDetails.push({
              "MemberId": cardDetails[i].MemberId ,
              "InsuredName": this.memberArray[j].proposerName,
              "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
              "InsuredGender": gendr,
              "InsuredOccpn": this.memberArray[j].occupationCode,
              "CoverType": cardDetails[i].CoverType,
              "SumInsured": cardDetails[i].SumInsured,
              "DeductibleDiscount": cardDetails[i].DeductibleDiscount,
              "Relation": cardDetails[i].Relation,
              "NomineeName": "",
              "NomineeRelation": "",
              "AnualIncome": cardDetails[i].annualIncome,
              "Height": cardDetails[i].Height,
              "Weight": cardDetails[i].Weight,
              "NomineeAge": "",
              "AppointeeName": "",
              "AptRelWithominee": "",
              "MedicalLoading": cardDetails[i].MedicalLoading,
              "PreExstDisease": "N",
              "DiseaseMedicalHistoryList": {
                "DiseaseMedicalHistory": {
                  "PreExistingDiseaseCode": "",
                  "MedicalHistoryDetail": ""
                }
              },
              "PrimaryCover": cardDetails[i].PrimaryCover,
              "SecondaryCover" : cardDetails[i].SecondaryCover
            })
          }
        }
 
  
      }
  
    }
    console.log("ben Details:" + JSON.stringify(BeneDetails));
    
    return BeneDetails;
  
  } 


  onBackClick(){
    sessionStorage.isViewPopped = "true";
    sessionStorage.ProposerDetails = JSON.stringify(this.memberArray);
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);
    this.navCtrl.pop();
  }

  captureImage(){

    let options: CameraOptions = {   
      // destinationType: this.camera.DestinationType.DATA_URL,
      // encodingType: this.camera.EncodingType.JPEG,
      // mediaType: this.camera.MediaType.PICTURE,
      // quality: 100,
      // saveToPhotoAlbum: false,
      // correctOrientation: true
      quality: 95,
      destinationType: this.camera.DestinationType.FILE_URI,      
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    this.presentLoadingDefault();
    this.camera.getPicture(options).then(imageData => {    
    this.thumbnail= imageData;       
    console.log("image: "  +this.thumbnail);
    this.getFileAsBase64(this.thumbnail);
    this.ImageData = imageData;
    this.imageCaptured = false;
    this.loading.dismiss();
    this.showToast("Proposer image captured.");
    }, (err) => {
    /*  Handle error */
      this.loading.dismiss();
    });

  }

  showImage(){
    this.imagePOp = false;
  }
  

  getFileAsBase64(thumbnail){
    this.base64.encodeFile(thumbnail).then((base64File: string) => {
      console.log("base: " + base64File);
    }, (err) => {
      console.log(err);
    });
  }

  closePop(){
    this.imagePOp = true;
  }

  onSaveAndEmail(){
    if(this.memberArray.length == 1){    
      // if(this.proposerName == ""){
      //   this.showToast("Enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
      //  }else if(this.memberOccupation == ''){
      //    this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
      //  }else{           
      //    this.memberArray[this.activeMemberIndex].proposerName = this.proposerName;
      //    this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
  
      //    for(let j=0; j<this.occupationList.length; j++){
      //      if(this.memberArray[this.activeMemberIndex].occupationCode == this.occupationList[j].OccCode){
      //      this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].OccTitle;
      //      break;
      //      }
      //      }
      //    this.getMemberContactDetails();   
      //    this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
      //  }

      if (this.proposerName == undefined || this.proposerName == null || this.proposerName == "") {     
        this.showToast("Please enter proposer name to proceed");              
      } else if (this.address == undefined || this.address == null || this.address == "") {     
            this.showToast("Please enter your address to proceed");                             
      } else  if (this.pinCode == undefined || this.pinCode == null || this.pinCode == "") {     
            this.showToast("Please enter your pincode to proceed");                         
      } else if(this.pinCode.length<6){
            this.showToast("Please enter 6 digit pincode");          
      } else if (this.state == undefined || this.state == null || this.state == "") {     
            this.showToast("Please enter your state to proceed");                            
      } else if (this.city == undefined || this.city == null || this.city == "") {     
            this.showToast("Please enter your city to proceed");                             
      } else if (this.email == undefined || this.email == null || this.email == "") { 
            this.showToast("Please enter your email Id to proceed");           
      } else if (!this.matchEmail(this.email)) {
        this.showToast("Please enter valid email id");   
      }else if (this.mobile == undefined || this.mobile == null || this.mobile == "") {      
        this.showToast("Please enter mobile number to proceed");                              
      } else if(this.mobile.length < 10){
        this.showToast("Please enter 10 digit mobile number");            
      }else if(this.proposerOccupation == undefined || this.proposerOccupation == null || this.proposerOccupation == ''){
        this.showToast("Please select occupation to proceed");     
      } else if ( this.pan == undefined || this.pan == null || this.pan == "") { 
        this.showToast("Please enter your PAN to proceed");      
      } else if (!this.matchPan(this.pan)) {
        this.showToast("Please enter valid pan");     
      } /*else if(this.imageCaptured == true){
        this.showToast("Please click image of Proposer");   
      }*/else{
        this.disableProceedBt = true;          
        this.getMemberContactDetails();      
       
        this.saveEmailData();
      }
    }else{
      if(this.nameOfProposer == ""){
        this.showToast("Enter "+ this.memberArray[this.activeMemberIndex].title +" Name");
       }else if(this.memberOccupation == ''){
         this.showToast("Select "+ this.memberArray[this.activeMemberIndex].title +" occupation");
       }else if((this.memberArray[this.activeMemberIndex].title == "Son" || this.memberArray[this.activeMemberIndex].title == "Daughter") && (this.memberOccupation == "BUSM" || this.memberOccupation == "HSWF" || this.memberOccupation == "RETR" || this.memberOccupation == "SVCM" || this.memberOccupation == "PRFS")){
        this.showToast("Only dependent members are covered");
       }else{           
         this.memberArray[this.activeMemberIndex].proposerName = this.nameOfProposer;
         this.memberArray[this.activeMemberIndex].occupationCode = this.memberOccupation;
         this.memberArray[this.activeMemberIndex].age = this.memberDOB;
  
         if(this.memberArray[this.activeMemberIndex].title == "Spouse"){
          
            this.memberArray[this.activeMemberIndex].occupationName = this.spouseOccupation;
        
          for(let j=0; j<this.occupationList.length; j++){
            if(this.memberArray[this.activeMemberIndex].occupationName == this.occupationList[j].occupationName){
            this.memberArray[this.activeMemberIndex].occupation = this.occupationList[j].occupationName;
            break;
            }
            }
          }
          else if(this.memberArray[this.activeMemberIndex].title == "Child1" || this.memberArray[this.activeMemberIndex].title == "Child2"){
            this.memberArray[this.activeMemberIndex].occupationName = this.memberOccupation;
            for(let j=0; j<this.occupationListChild.length; j++){
              if(this.memberArray[this.activeMemberIndex].occupationName == this.occupationListChild[j].OccTitle){
              this.memberArray[this.activeMemberIndex].occupation = this.occupationListChild[j].OccTitle;
              break;
              }
              }
            }
         this.getMemberContactDetails();   
         
        this.saveEmailData();
        //  this.navCtrl.push(NomineeDetailsPage,{"ProposerDetails":this.memberArray, "planCardDetails":this.selectedCardArray});
       }
    } 
  }

  saveEmailData(){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  callEProposalService(){   
    var eProposalData = {
          "QuotationID": this.QuotationID,
          "UID": this.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }

  sendEProposerData(serviceData){
    this.presentLoadingDefault();
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup = false;
          //this.closeModalPop();
          this.loading.dismiss();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp.ReturnCode == "807"){
          this.showToast(resp.ReturnMsg);
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else if(resp.ReturnCode == "500"){
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.showToast(resp.ReturnMsg); 
        }        
       
      }, (err) => {
      console.log(err);
      this.loading.dismiss();
      });
  }

  checkValidPan(){
    if(!this.matchPan(this.pan)){
      this.disableProceedBt = true;
    }else{
      this.disableProceedBt = false;
    }
  }


  OkButton(){
      this.thankyouPopup = true;
      this.navCtrl.push(LandingScreenPage);
    }

  // EProposerEmail(){
  //   this.encryptIncompleteData();
  // }

  // getFileContentAsBase64(thumbnail).then(base64Image => {
  //   //window.open(base64Image);
  //   console.log(base64Image); 
  //   // Then you'll be able to handle the myimage.png file as base64
  // });
}






