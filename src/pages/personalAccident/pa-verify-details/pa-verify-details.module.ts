import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaVerifyDetailsPage } from './pa-verify-details';

@NgModule({
  declarations: [
   // PaVerifyDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PaVerifyDetailsPage),
  ],
})
export class PaVerifyDetailsPageModule {}
