import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaNomineeDetailsPage } from './pa-nominee-details';

@NgModule({
  declarations: [
   // PaNomineeDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PaNomineeDetailsPage),
  ],
})
export class PaNomineeDetailsPageModule {}
