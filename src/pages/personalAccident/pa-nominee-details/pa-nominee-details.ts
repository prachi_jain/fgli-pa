import { Component, Input, ChangeDetectorRef, ElementRef, ViewChild, Renderer, NgModule } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { DatePicker } from "@ionic-native/date-picker";
import { PaBuyPolicyReviewPage } from "../pa-buy-policy-review/pa-buy-policy-review";
import { LandingScreenPage } from "../../health/landing-screen/landing-screen";
import * as $ from "jquery";
import { Keyboard } from "@ionic-native/keyboard";
import { first } from "rxjs/operator/first";
import { AppService } from "../../../providers/app-service/app-service";
import { PaBuyProposerDetailsPage } from "../pa-buy-proposer-details/pa-buy-proposer-details";
import { LoginPage } from "../../health/login/login";

/**
 * Generated class for the PaNomineeDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-pa-nominee-details",
  templateUrl: "pa-nominee-details.html"
})
export class PaNomineeDetailsPage {
  
  /*variable declarations*/
  currToggleNum;
  nomTabActive: any = []
  ReferenceNo; //nomineeName; NomineeAge; NomineeRelation;
  members;
  nomineeDate: any;
  nomineeName: any;
  nomineeAge: any;
  nomineeRelation: any;
  appointeeName: any;
  appointeeRelation: any;
  temp: any = [];
  ClientDetails: any = [];
  loading;
  activeItem;
  //memberArray = [{"code":"SELF","title":"Self","smoking":false,"smokingText":"","popupType":"1","showMarried":true,"maritalStatus":"Married","maritalCode":"M","age":"01-01-1985","ageText":"33 years","insuredCode":"si003","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"111.76","HeightText":"3' 8\"","Weight":"54","ProposerDetailsDivShow":true,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"","NomineeRelation":"","NomineeAge":"","appointeeMember":"","AptRelWithominee":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/self.png","detailsSaved":true,"occupation":"Businessman","proposerName":"Teat","Gender":"M","genderText":"Male","address":"Dhddj","pincode":"884994","state":"Zvsvsgge","city":"Sgsgsgs","proposerEmail":"a@gmail.com","mobile":"8097708434","pan":"HUHBF1234B","occupationCode":"BUSM"},{"code":"SPOU","title":"Spouse","smoking":false,"smokingText":"","popupType":"1","showMarried":false,"maritalStatus":"","maritalCode":"S","age":"01-01-1987","ageText":"31 years","insuredCode":"si003","showDependent":false,"showAddAnotherBtn":false,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":true,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"111.76","HeightText":"3' 8\"","Weight":"54","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"","NomineeRelation":"","NomineeAge":"","appointeeMember":"","AptRelWithominee":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/spouse.png","detailsSaved":true,"proposerName":"Dhdhd","occupationCode":"HSWF","Gender":"M","genderText":"Male","occupation":"Housewife"},{"code":"SON","title":"Son","smoking":false,"smokingText":"","popupType":"2","showMarried":false,"maritalStatus":"","maritalCode":"S","age":"01-01-1990","ageText":"28 years","insuredCode":"si003","showDependent":true,"showAddAnotherBtn":true,"PolicyType":"HTF","showFamilyMemberOnScreen":true,"disableRemove":false,"ShowInsurenceAmt":false,"insuredPlanTypeCode":"P001","insuredPlanTypeText":"VITAL","insuredAmtInDigit":"","Height":"114.30","HeightText":"3' 9\"","Weight":"85","ProposerDetailsDivShow":false,"medicalDeclaration":false,"showMedicalQuestions":true,"nominee":"","NomineeRelation":"","NomineeAge":"","appointeeMember":"","AptRelWithominee":"","memberSelectedOnAddmember":true,"imgUrl":"assets/imgs/son.png","detailsSaved":true,"proposerName":"Dhhdd","occupationCode":"STDN","occupation":"Student"}];

  memberArray = [];
  memberArrayCopy = [];
  nomineeMemberArray = [];
  selectedCardArray: any = [];
  isNomineeMinor = false;
  immediateMemberArray = ['Self', 'Spouse', 'Mother', 'Father', 'Son', 'Daughter', 'Sibling', 'Grand Son', 'Grand Daughter', 'Grand Mother', 'Grand Father'];
  rellationArray;// = [{title:'Spouse'},{title : 'Son'},{title : 'Daughter'},{title : 'Mother'}, {title:'Father'}]; //= ['Spouse', 'Mother', 'Father', 'Son', 'Daughter'];
  //rellationArray = [{title: "Father", code: "FATH"},{title: "Mother", code: "MOTH"} ]
  tempRelationArray;
  hideDiv = true;
  shownGroup = null;
  isComplete = false;
  nomineeSelect : any = [];
  hideAptInput = true;
  hideApt = true;
  pID;
  proposerName;
  OtherNomineeRelation;
  thankyouPopup= true;
  pet = "Self"
  selfnomineeName
  selfnomineeRelation
  selfnomineeDate
  rellationArraySelf
  spousenomineeName;
  spousenomineeDate
  spousenomineeRelation
  rellationArraySpouse
  sonnomineeName
  sonnomineeDate
  sonnomineeRelation
  rellationArraySon
  daughternomineeName
  daughternomineeDate
  daughternomineeRelation
  rellationArrayDaughter
  index = 0;
  policyType
  ENQ_PolicyResponse
  response
  QuotationID
  UID
  insuranceDetailsToSend
  appointeeArraySelf;
  selfappointeeName;
  selfappointeeRelation



  /// 
  hideAptSpouse = true
  spouseappointeeRelation
  spouseappointeeName
  appointeeArraySpouse

  //
  hideAptChild1 = true
  sonappointeeRelation
  sonappointeeName
  appointeeArraySon

  hideAptChild2 = true
  daughterappointeeRelation
  daughterappointeeName
  appointeeArrayDaughter

  activeMemberIndex = 0
  constructor(private platform: Platform, public navCtrl: NavController, public toast: ToastController, private datePicker: DatePicker,
    public navParams: NavParams, private cdr: ChangeDetectorRef, private keyboard: Keyboard, private render: Renderer,
    public appService: AppService, private loadingCtrl : LoadingController) {
      
    this.memberArray = navParams.get("ProposerDetails");
    console.log("nomineeMember:" + JSON.stringify(this.memberArray));
    
    this.selectedCardArray = navParams.get("planCardDetails");
    console.log("test:" + JSON.stringify(this.selectedCardArray));
    this.memberArrayCopy = this.memberArray;
    this.nomineeMemberArray = this.immediateMemberArray;


    this.policyType = navParams.get("PolicyType");
    this.ENQ_PolicyResponse = navParams.get("ENQ_PolicyResponse");
    this.response = this.ENQ_PolicyResponse.PurchaseResponse[0].Result_pa
    
    this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
    this.UID = this.ENQ_PolicyResponse.PurchaseResponse[0].UID;
    this.insuranceDetailsToSend = this.navParams.get("insuranceDetailsToSend");

    this.ReferenceNo = navParams.get("ReferenceNo");

    // platform.registerBackButtonAction(() => {
    //   this.onBackClick();
    // });
  }
  ionViewDidLoad() {
      this.hideApt = true;
   
    
    if(this.memberArray[0].maritalStatus == "Divorced" || this.memberArray[0].maritalStatus == "Single" || this.memberArray[0].maritalStatus == "Widow / Widower"){
     
      this.rellationArraySelf = ['Child1', 'Mother', 'Father', 'Child2'];
      this.appointeeArraySelf = ['Child1', 'Mother', 'Father', 'Child2'];


      console.log("Single" + JSON.stringify(this.rellationArray));
      console.log("IF");
      
    }
    else{
      this.rellationArraySelf = ['Spouse', 'Child1', 'Mother', 'Father', 'Child2'];
      this.appointeeArraySelf = ['Spouse', 'Child1', 'Mother', 'Father', 'Child2'];
    }

    for(let i=1; i<this.memberArray.length;i++){
    
      if(this.memberArray[i].title == "Spouse"){

       this.rellationArraySpouse = ['Self','Child1', 'Mother', 'Father', 'Child2'];
       this.appointeeArraySpouse = ['Self','Child1', 'Mother', 'Father', 'Child2'];
      } 
      else if(this.memberArray[i].title == "Child1"){

        this.rellationArraySon = ['Self','Spouse', 'Mother', 'Father', 'Child2'];
        this.appointeeArraySon = ['Self','Spouse', 'Mother', 'Father', 'Child2'];
      }
      else if(this.memberArray[i].title == "Child2"){

        this.rellationArrayDaughter = ['Self','Spouse','Child1','Mother', 'Father'];
        this.appointeeArrayDaughter = ['Self','Spouse','Child1','Mother', 'Father'];
      }
    }
    


    //this.activeItem = "SELF"; 
    this.proposerName = this.memberArray[0].proposerName;
    console.log("back State: " + sessionStorage.isViewPopped);
    if(sessionStorage.isViewPopped == "true"){
      this.memberArray = JSON.parse(sessionStorage.ProposerDetails);
      this.selectedCardArray = JSON.parse(sessionStorage.planCardDetails);
      console.log("memberStorage: " + JSON.stringify(this.memberArray));
      console.log("cardArray: " + JSON.stringify(this.selectedCardArray));
      
      sessionStorage.isViewPopped = "";
    }

    this.setStoredData();
  }

  ionViewDidLeave(){

    this.setStoredDatafromBack();
  }
  setStoredDatafromBack(){
    this.pet = "Self";
    this.index = 0;
    for(let i=0; i<this.memberArray.length;i++){
     
      if (this.memberArray[i].title == "Self") {
        this.selfnomineeRelation =this.memberArray[i].NomineeRelation
        this.selfnomineeName = this.memberArray[i].nominee
        this.selfnomineeDate =  this.memberArray[i].nomineeDOB


        this.memberArray[0].nominee = this.selfnomineeName
        this.memberArray[0].nomineeDOB = this.selfnomineeDate
        this.memberArray[0].NomineeRelation = this.selfnomineeRelation

        if(this.hideApt == false){


          this.selfappointeeName =  this.memberArray[0].appointeeMember
          this.selfappointeeRelation = this.memberArray[0].AptRelWithominee


          this.memberArray[0].appointeeMember = this.selfappointeeName;
          this.memberArray[0].AptRelWithominee = this.selfappointeeRelation
          
        }
       
      }
      else if (this.memberArray[i].title == "Spouse") {
        this.spousenomineeRelation = this.memberArray[i].NomineeRelation
        this.spousenomineeName =this.memberArray[i].nominee
        this.spousenomineeDate = this.memberArray[i].nomineeDOB


        if(this.hideAptSpouse == false){

          this.spouseappointeeName = this.memberArray[i].appointeeMember
          this.spouseappointeeRelation = this.memberArray[i].AptRelWithominee 


          this.memberArray[i].appointeeMember = this.spouseappointeeName;
          this.memberArray[i].AptRelWithominee = this.spouseappointeeRelation
         
        }
      }

      else if (this.memberArray[i].title == "Child1") {
        this.sonnomineeRelation =  this.memberArray[i].NomineeRelation
        this.sonnomineeName = this.memberArray[i].nominee
        this.sonnomineeDate = this.memberArray[i].nomineeDOB


       


          if(this.hideAptChild1 == false){
  
            this.sonappointeeName = this.memberArray[i].appointeeMember
            this.sonappointeeRelation =   this.memberArray[i].AptRelWithominee


              this.memberArray[i].appointeeMember = this.sonappointeeName;
              this.memberArray[i].AptRelWithominee = this.sonappointeeRelation
             
            }
          

      }
      else if (this.memberArray[i].title == "Child2") {
        this.daughternomineeRelation = this.memberArray[i].NomineeRelation
        this.daughternomineeName =  this.memberArray[i].nominee
        this.daughternomineeDate =  this.memberArray[i].nomineeDOB



        if(this.hideAptChild2 == false){
          this.daughterappointeeName = this.memberArray[i].appointeeMember
          this.daughterappointeeRelation =  this.memberArray[i].AptRelWithominee 

            this.memberArray[this.index].appointeeMember = this.daughterappointeeName;
            this.memberArray[this.index].AptRelWithominee = this.daughterappointeeRelation
          
          }
        }
      
    

  }


  }

  setStoredData(){
    for(let i=1; i<this.memberArray.length;i++){
     

        if (this.memberArray[i].title == "Spouse") {
          this.spousenomineeRelation = "Self"
          this.spousenomineeName = this.memberArray[0].proposerName
          this.spousenomineeDate = this.memberArray[0].age




        }

        else if (this.memberArray[i].title == "Child1") {
          this.sonnomineeRelation = "Self"
          this.sonnomineeName = this.memberArray[0].proposerName
          this.sonnomineeDate = this.memberArray[0].age

        }
        else if (this.memberArray[i].title == "Child2") {
          this.daughternomineeRelation = "Self"
          this.daughternomineeName = this.memberArray[0].proposerName
          this.daughternomineeDate = this.memberArray[0].age

        }
      

    }


    this.pet = "Self";
    this.index = 0;

    this.selfnomineeRelation = this.memberArray[0].NomineeRelation;     
    this.selfnomineeName = this.memberArray[0].nominee;
    this.selfnomineeDate =  this.memberArray[0].nomineeDOB;
    this.selfappointeeName = this.memberArray[0].appointeeName;
    this.selfappointeeRelation =  this.memberArray[0].appointeeRelation;
   
    this.hideApt = true;


    if((this.memberArray[0].nomineeAge != null  || this.memberArray[0].nomineeAge != undefined) && this.isDateValid(this.memberArray[0].nomineeAge) == "1"){
      this.hideApt = true;
    }else if((this.memberArray[0].nomineeAge != null  || this.memberArray[0].nomineeAge != undefined) && this.isDateValid(this.memberArray[0].nomineeAge) == "2"){
      this.hideApt = false;
    }    
  }

 

  onDateChange(ev,members, idx){

    if(members == "Self"){
     
      if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
        if(idx == "0"){
          ev.target.value = this.selfnomineeDate+"-";
        }
        
        this.cdr.detectChanges();
      }else if(ev.target.value.length==10){
        let age : any= this.getAge(ev.target.value);
        this.memberArray[idx].nomineeAge = ev.target.value;
        if(this.isDateValid(ev.target.value) == "2"){
          this.showToast('Please enter valid nominee DOB.');
        }else if(this.isDateValid(ev.target.value) == "1"){
          // 0 indicates that date of birth is greater than 18 
          if ((this.selfnomineeRelation == "Child1") || (this.selfnomineeRelation == "Child2" ) || (this.selfnomineeRelation == "Brother") || (this.selfnomineeRelation == "Sister")){
            this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
            this.hideApt = false;
          } 
          else  if ((this.selfnomineeRelation == "Spouse") || (this.selfnomineeRelation == "Mother")|| (this.selfnomineeRelation == "Father") || (this.selfnomineeRelation == "Uncle") || (this.selfnomineeRelation == "Aunt")){
            this.showToast('Please select a nominee above 18 years of age');
            this.hideApt = true;
            this.selfnomineeDate = "";
          }  
         
        }else{
          this.selfappointeeName = "";
          this.selfappointeeRelation = "";
          this.hideApt = true;
        }
        this.keyboard.hide();
      }
    }


    else if(members == "Spouse"){
     

      if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
        if(idx == "0"){
          ev.target.value = this.spousenomineeDate+"-";
        }
        
        this.cdr.detectChanges();
      }else if(ev.target.value.length==10){
        let age : any= this.getAge(ev.target.value);
        this.memberArray[idx].nomineeAge = ev.target.value;
        if(this.isDateValid(ev.target.value) == "2"){
          this.showToast('Please enter valid nominee DOB.');
        }else if(this.isDateValid(ev.target.value) == "1"){
          if ((this.spousenomineeRelation == "Child1")|| (this.spousenomineeRelation == "Child2" ) || (this.spousenomineeRelation == "Brother") || (this.spousenomineeRelation == "Sister" )){
            this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
            this.hideAptSpouse = false;
            this.spousenomineeDate = ""
          } 
          else if ((this.spousenomineeRelation == "Self") || (this.spousenomineeRelation == "Mother")|| (this.spousenomineeRelation == "Father") || (this.spousenomineeRelation == "Uncle") || (this.spousenomineeRelation == "Aunt")){
            this.showToast('Please select a nominee above 18 years of age');
            this.hideAptSpouse = true;
          } 
         
        }else{
          this.spouseappointeeName = "";
          this.spouseappointeeRelation = "";
          this.hideAptSpouse = true;
        }
        this.keyboard.hide();
      }

    }
   else if(members == "Child1"){
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      if(idx == "0"){
        ev.target.value = this.sonnomineeDate+"-";
      }
      
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(ev.target.value);
      this.memberArray[idx].nomineeAge = ev.target.value;
      if(this.isDateValid(ev.target.value) == "2"){
        this.showToast('Please enter valid nominee DOB.');
      }else if(this.isDateValid(ev.target.value) == "1"){
        if ((this.sonnomineeRelation == "Child1")|| (this.sonnomineeRelation == "Child2" ) || (this.sonnomineeRelation == "Brother") || (this.sonnomineeRelation == "Sister" )) {
          this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
          this.hideAptChild1 = false;
        }
        else if ((this.sonnomineeRelation == "Self") || (this.sonnomineeRelation == "Spouse") || (this.sonnomineeRelation == "Mother")|| (this.sonnomineeRelation == "Father") || (this.sonnomineeRelation == "Uncle") || (this.sonnomineeRelation == "Aunt")){
          this.showToast('Please select a nominee above 18 years of age');
          this.sonnomineeDate = ""
          this.hideAptChild1 = true;
        } 
       
      }else{
        this.sonappointeeName = "";
        this.sonappointeeRelation = "";
        this.hideAptChild1 = true;
      }
      this.keyboard.hide();
    }

   }
   else if(members == "Child2"){
    if((ev.target.value.length==2 || ev.target.value.length==5) && ev.key != "Backspace"){
      if(idx == "0"){
        ev.target.value = this.daughternomineeDate+"-";
      }
      
      this.cdr.detectChanges();
    }else if(ev.target.value.length==10){
      let age : any= this.getAge(ev.target.value);
      this.memberArray[idx].nomineeAge = ev.target.value;
      if(this.isDateValid(ev.target.value) == "2"){
        this.showToast('Please enter valid nominee DOB.');
      }else if(this.isDateValid(ev.target.value) == "1"){
        if ((this.daughternomineeRelation == "Child1")|| (this.daughternomineeRelation == "Child2" ) || (this.daughternomineeRelation == "Brother") || (this.daughternomineeRelation == "Sister" )){
          this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
          this.hideAptChild2 = false;
        }
        else if ((this.daughternomineeRelation == "Self") || (this.daughternomineeRelation == "Spouse") || (this.daughternomineeRelation == "Mother")|| (this.daughternomineeRelation == "Father") || (this.daughternomineeRelation == "Uncle") || (this.daughternomineeRelation == "Aunt")){
          this.showToast('Please select a nominee above 18 years of age');
          this.daughternomineeDate = ""
          this.hideAptChild2 = true;
        }
       
      }else{
        this.daughterappointeeName = "";
        this.daughterappointeeRelation = "";
        this.hideAptChild2 = true;
      }
      this.keyboard.hide();
    }

   }
  }
  showHideNomDetails(num) {
    console.log("index: " + num);
    
    if (this.currToggleNum != num) {
      this.currToggleNum = num
      $(".acorContent").slideUp();
      $('#nomDetails' + num).slideDown();
    }
  }

  isDateValid(date){

        var monthfield = date.split("-")[1]
        var dayfield = date.split("-")[0]
        var yearfield = date.split("-")[2]
        
        if((monthfield<=12)&&(dayfield<=31)&&(yearfield <= new Date().getFullYear())){
          return this.getAge(date) != null && this.getAge(date) >= 18 ? "0" : "1";
        }
        return "2";
      }


  proceedNext(){
    

    console.log("sonnomineeRelation"+ this.sonnomineeRelation)
    console.log("sonnomineeRelation"+ this.daughternomineeRelation)

    if(this.index < this.memberArray.length){

      if(this.index==0){
        if(this.selfnomineeRelation == "" || this.selfnomineeRelation == undefined){
          this.showToast("Please enter relation with nominee against Self");
          
        }else if(this.selfnomineeName == "" || this.selfnomineeName == undefined){
          this.showToast("Please enter nominee name against Self");
          
        }else if(this.selfnomineeDate == "" || this.selfnomineeDate == undefined){
          this.showToast("Please enter nominee DOB against Self");
          
        }else{
          this.isComplete = true;
         
          this.memberArray[0].nominee = this.selfnomineeName
          this.memberArray[0].nomineeDOB = this.selfnomineeDate
          this.memberArray[0].NomineeRelation = this.selfnomineeRelation
         

          if(this.hideApt == false){
            if(this.selfappointeeRelation == "" || this.selfappointeeRelation == undefined){
              this.showToast("Please enter relation with nominee against Self");
              
            }else if(this.selfappointeeName == "" || this.selfappointeeName == undefined){
              this.showToast("Please enter appointee name against Self");
              
            }
            else{
              this.isComplete = true;
              this.memberArray[0].appointeeMember = this.selfappointeeName;
              this.memberArray[0].AptRelWithominee = this.selfappointeeRelation
              
              //this.hideApt = true
            }

          }

          this.index++
    
          if(this.memberArray.length > 1){

            this.pet = this.memberArray[1].title
          }else{
            this.encryptIncompleteData()


          }
         
        }
      }
      else{
         
  
            if(this.pet == "Spouse"){

              
                if(this.spousenomineeRelation == "" || this.spousenomineeRelation == undefined){
                  this.showToast("Please enter relation with nominee against Self");
                
                }else if(this.spousenomineeName == "" || this.spousenomineeName == undefined){
                  this.showToast("Please enter nominee name against Self");
                
                }else if(this.spousenomineeDate == "" || this.spousenomineeDate == undefined){
                  this.showToast("Please enter nominee DOB against Self");
                 
                }else{
            
                this.memberArray[this.index].nominee = this.spousenomineeName
                this.memberArray[this.index].nomineeDOB = this.spousenomineeDate
                this.memberArray[this.index].NomineeRelation = this.spousenomineeRelation
                
                
              
  
                if(this.hideAptSpouse == false){
  
                  if(this.spouseappointeeRelation == "" || this.spouseappointeeRelation == undefined){
                    this.showToast("Please enter relation with nominee against Self");
                    
                  }else if(this.spouseappointeeName == "" || this.spouseappointeeName == undefined){
                    this.showToast("Please enter appointee name against Self");
                    
                  }
                  else{
                    this.isComplete = true;
                   
                    this.memberArray[this.index].appointeeMember = this.spouseappointeeName;
                    this.memberArray[this.index].AptRelWithominee = this.spouseappointeeRelation
                  //  this.hideAptSpouse = true
                  }
                }

                this.index++
                if(this.index == this.memberArray.length){
                  this.encryptIncompleteData()
                }
                else{
                  this.pet = this.memberArray[this.index].title
                }
                }
                
              }
            
            else if(this.pet == "Child1"){
             
              
  
                if(this.sonnomineeRelation == "" || this.sonnomineeRelation == undefined){
                  this.showToast("Please enter relation with nominee against Self");
                
                }else if(this.sonnomineeName == "" || this.sonnomineeName == undefined){
                  this.showToast("Please enter nominee name against Self");
                
                }else if(this.sonnomineeDate == "" || this.sonnomineeDate == undefined){
                  this.showToast("Please enter nominee DOB against Self");
                 
                }else{
                
                this.memberArray[this.index].nominee = this.sonnomineeName
                this.memberArray[this.index].nomineeDOB = this.sonnomineeDate
                this.memberArray[this.index].NomineeRelation = this.sonnomineeRelation
                
                if(this.hideAptChild1 == false){
  
                  if(this.sonappointeeRelation == "" || this.sonappointeeRelation == undefined){
                    this.showToast("Please enter relation with nominee against Self");
                    
                  }else if(this.sonappointeeName == "" || this.sonappointeeName == undefined){
                    this.showToast("Please enter appointee name against Self");
                    
                  }
                  else{
                    this.isComplete = true;
                    this.memberArray[this.index].appointeeMember = this.sonappointeeName;
                    this.memberArray[this.index].AptRelWithominee = this.sonappointeeRelation
                    //this.hideAptChild1 = true
                  }
                }

                this.index++
                if(this.index == this.memberArray.length){
                  this.encryptIncompleteData()
                }
                else{
                  this.pet = this.memberArray[this.index].title
                }
                }
                
              
            }
            else if(this.pet == "Child2"){
              if(this.daughternomineeRelation == "" || this.daughternomineeRelation == undefined){
                this.showToast("Please enter relation with nominee against Self");
                
              }else if(this.daughternomineeName == "" || this.daughternomineeName == undefined){
                this.showToast("Please enter nominee name against Self");
                
              }else if(this.daughternomineeDate == "" || this.daughternomineeDate == undefined){
                this.showToast("Please enter nominee DOB against Self");
                
              }else{
              this.memberArray[this.index].nominee = this.daughternomineeName
              this.memberArray[this.index].nomineeDOB = this.daughternomineeDate
              this.memberArray[this.index].NomineeRelation = this.daughternomineeRelation
              
              if(this.hideAptChild2 == false){

                if(this.daughterappointeeRelation == "" || this.daughterappointeeRelation == undefined){
                  this.showToast("Please enter relation with nominee against Self");
                  
                }else if(this.daughterappointeeName == "" || this.daughterappointeeName == undefined){
                  this.showToast("Please enter appointee name against Self");
                  
                }
                else{
                  this.isComplete = true;
                  this.memberArray[this.index].appointeeMember = this.daughterappointeeName;
                  this.memberArray[this.index].AptRelWithominee = this.daughterappointeeRelation
                 // this.hideAptChild2 = true
                }
              }

              this.index++
              if(this.index == this.memberArray.length){
                this.encryptIncompleteData()
              }
              else{
                this.pet = this.memberArray[this.index].title
              }
              }

              
            }


          
        
        
  
        
      }
    }
    else {
      this.encryptIncompleteData()

    }
    
   
   
  }

  showToast(Message) {
    let pageToast = this.toast.create({
      message: Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 2500,
    });
    pageToast.present();
  }


  numberLength(Num) { if (Num >= 10) { return Num } else { return "0" + Num } }

 
  

  DatePick(event, members, index) {
 
    this.keyboard.hide();
    // let element = (<HTMLInputElement>event.target.previousElementSibling.querySelector('input[name="nomineeDOB"]'));
    
     if(members == "Self"){
      this.selfnomineeDate = "";
      let element = this.selfnomineeDate;
      let dateFieldValue = element;
  
      let year: any = 1985, month: any = 1, date: any = 1;
      if (dateFieldValue != "" || dateFieldValue == undefined) {
        date = dateFieldValue ? dateFieldValue.split("-")[0]  : "";
        month = dateFieldValue ? dateFieldValue.split("-")[1] : "";
        year = dateFieldValue ? dateFieldValue.split("-")[2] : "";
      }
    
      let minDateLimit = (new Date(1985, 1, 0).getTime());
        this.datePicker.show({
        date: new Date(year, month - 1, date),
          minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
        }).then(
          date => {
          let dt = (this.numberLength(date.getDate()) + "-" + this.numberLength(date.getMonth() + 1) + "-" + (date.getFullYear())).toString();
          this.selfnomineeDate = dt
          this.memberArrayCopy[0].nomineeAge = this.selfnomineeDate;
          let appointeeAge = this.getAge(dt);
          
          if (appointeeAge < 18 && ((this.selfnomineeRelation == "Child1") || (this.selfnomineeRelation == "Child2" ) || (this.selfnomineeRelation == "Brother") || (this.selfnomineeRelation == "Sister") )){
            this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
            this.hideApt = false;
          } 
          else  if (appointeeAge < 18 && ((this.selfnomineeRelation == "Spouse") || (this.selfnomineeRelation == "Mother")|| (this.selfnomineeRelation == "Father") || (this.selfnomineeRelation == "Uncle") || (this.selfnomineeRelation == "Aunt"))){
            this.showToast('Please select a nominee above 18 years of age');
            this.hideApt = true;
            this.selfnomineeDate = "";
          }  
          else {
            this.selfappointeeName ="";
            this.selfappointeeRelation ="";
            this.hideApt = true;
          }
        },
  
        err => console.log('Error occurred while getting date: ' + err)
        );
    }

    else if(members == "Spouse"){

      let element = this.spousenomineeDate;
      let dateFieldValue = element;
      this.spousenomineeDate = ""
      let year: any = 1985, month: any = 1, date: any = 1;
      if (dateFieldValue != "" || dateFieldValue == undefined) {
        date = dateFieldValue ? dateFieldValue.split("-")[0]  : "";
        month = dateFieldValue ? dateFieldValue.split("-")[1] : "";
        year = dateFieldValue ? dateFieldValue.split("-")[2] : "";
      }
    
      let minDateLimit = (new Date(1985, 1, 0).getTime());
        this.datePicker.show({
        date: new Date(year, month - 1, date),
          minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
        }).then(
          date => {
          let dt = (this.numberLength(date.getDate()) + "-" + this.numberLength(date.getMonth() + 1) + "-" + (date.getFullYear())).toString();
          this.spousenomineeDate = dt
          this.memberArrayCopy[index].nomineeAge = this.spousenomineeDate;
          let appointeeAge = this.getAge(dt);
          
          if (appointeeAge < 18  && ((this.spousenomineeRelation == "Child1")|| (this.spousenomineeRelation == "Child2" ) || (this.spousenomineeRelation == "Brother") || (this.spousenomineeRelation == "Sister" ))){
            this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
            this.hideAptSpouse = false;
            this.spousenomineeDate = ""
          } 
          else if (appointeeAge < 18 && (((this.spousenomineeRelation == "Self") || this.spousenomineeRelation == "Mother")|| (this.spousenomineeRelation == "Father") || (this.spousenomineeRelation == "Uncle") || (this.spousenomineeRelation == "Aunt"))){
            this.showToast('Please select a nominee above 18 years of age');
            this.hideAptSpouse = true;
          } 
          else {
            this.spouseappointeeName ="";
            this.spouseappointeeRelation ="";
            this.hideAptSpouse = true;
          }
        },
  
        err => console.log('Error occurred while getting date: ' + err)
        );
    }
    
    else if(members == "Child1"){
      this.sonnomineeDate = "";
      let element = this.sonnomineeDate;
      let dateFieldValue = element;
  
      let year: any = 1985, month: any = 1, date: any = 1;
      if (dateFieldValue != "" || dateFieldValue == undefined) {
        date = dateFieldValue ? dateFieldValue.split("-")[0]  : "";
        month = dateFieldValue ? dateFieldValue.split("-")[1] : "";
        year = dateFieldValue ? dateFieldValue.split("-")[2] : "";
      }
    
      let minDateLimit = (new Date(1985, 1, 0).getTime());
        this.datePicker.show({
        date: new Date(year, month - 1, date),
          minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
        }).then(
          date => {
          let dt = (this.numberLength(date.getDate()) + "-" + this.numberLength(date.getMonth() + 1) + "-" + (date.getFullYear())).toString();
          this.sonnomineeDate = dt
          this.memberArrayCopy[index].nomineeAge = this.sonnomineeDate;
          let appointeeAge = this.getAge(dt);
          
          if (appointeeAge < 18  && ((this.sonnomineeRelation == "Child1")|| (this.sonnomineeRelation == "Child2" ) || (this.sonnomineeRelation == "Brother") || (this.sonnomineeRelation == "Sister" ))) {
            this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
            this.hideAptChild1 = false;
          }
          else if (appointeeAge < 18 && ((this.sonnomineeRelation == "Self") || (this.sonnomineeRelation == "Spouse") || (this.sonnomineeRelation == "Mother")|| (this.sonnomineeRelation == "Father") || (this.sonnomineeRelation == "Uncle") || (this.sonnomineeRelation == "Aunt"))){
            this.showToast('Please select a nominee above 18 years of age');
            this.sonnomineeDate = ""
            this.hideAptChild1 = true;
          }  else {
            this.sonappointeeName ="";
            this.sonappointeeRelation ="";
            this.hideAptChild1 = true;
          }
        },
  
        err => console.log('Error occurred while getting date: ' + err)
        );
    }

    else if(members == "Child2"){
      this.daughternomineeDate = ""
      let element = this.daughternomineeDate;
      let dateFieldValue = element;
  
      let year: any = 1985, month: any = 1, date: any = 1;
      if (dateFieldValue != "" || dateFieldValue == undefined) {
        date = dateFieldValue ? dateFieldValue.split("-")[0]  : "";
        month = dateFieldValue ? dateFieldValue.split("-")[1] : "";
        year = dateFieldValue ? dateFieldValue.split("-")[2] : "";
      }
    
      let minDateLimit = (new Date(1985, 1, 0).getTime());
        this.datePicker.show({
        date: new Date(year, month - 1, date),
          minDate: minDateLimit,
          maxDate: Date.now(),
          mode: 'date',
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
        }).then(
          date => {
          let dt = (this.numberLength(date.getDate()) + "-" + this.numberLength(date.getMonth() + 1) + "-" + (date.getFullYear())).toString();
          this.daughternomineeDate = dt
          this.memberArrayCopy[index].nomineeAge = this.daughternomineeDate;
          let appointeeAge = this.getAge(dt);
          
          if (appointeeAge < 18  && ((this.daughternomineeRelation == "Child1")|| (this.daughternomineeRelation == "Child2" ) || (this.daughternomineeRelation == "Brother") || (this.daughternomineeRelation == "Sister" ))){
            this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
            this.hideAptChild2 = false;
          }
          else if (appointeeAge < 18 && ((this.daughternomineeRelation == "Self") || (this.daughternomineeRelation == "Spouse") ||(this.daughternomineeRelation == "Mother")|| (this.daughternomineeRelation == "Father") || (this.daughternomineeRelation == "Uncle") || (this.daughternomineeRelation == "Aunt"))){
            this.showToast('Please select a nominee above 18 years of age');
            this.daughternomineeDate = ""
            this.hideAptChild2 = true;
          } else {
            this.daughterappointeeName ="";
            this.daughterappointeeRelation ="";
            this.hideAptChild2 = true;
          }
        },
  
        err => console.log('Error occurred while getting date: ' + err)
        );
    }
  }
  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365);
  }

  encryptIncompleteData(){  
    //$(".acorContent:not(:first)").hide();   
  
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    this.memberArrayCopy = this.memberArray 
    this.sendStage1Data("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }



  sendStage1Data(URL,serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          // this.pID = Resp.Proceed_PolicyResult.Data.pID;         
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          
          this.navCtrl.push(PaBuyPolicyReviewPage,{"ProposerDetails":this.memberArrayCopy, "planCardDetails":this.selectedCardArray,"PolicyType": this.policyType , "ENQ_PolicyResponse": this.ENQ_PolicyResponse ,"beneficiaryDetails": this.selectedCardArray,"insuranceDetailsToSend":this.insuranceDetailsToSend,"ReferenceNo":this.ReferenceNo});
          
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.loading.dismiss();
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  getProceedrequest(){
    let sendRequest = {
      "QuotationID": this.QuotationID,
      "UID": this.UID,
      "stage":"3",
      "Client": this.getClientNode(),
      "Risk": {
        "PolicyType": this.policyType ,//"HTF",
        "Duration":  sessionStorage.Duration,
        "Installments": sessionStorage.Installments,
        "IsfgEmployee": sessionStorage.IsFGEmployee,
        "IsPos": sessionStorage.Ispos,
        "BeneficiaryDetails": this.selectedCardArray.length == undefined ? this.getSelfBeneficiaryDetails(this.selectedCardArray)  : this.getBeneficiaryDetails(this.selectedCardArray),
        "MemberDetails": this.memberArray,
        "CardDetails": this.insuranceDetailsToSend
      }
    }
    console.log("VerifyPage: "+JSON.stringify(sendRequest));
    return sendRequest;
  }
  
  getClientNode(){
    let age = this.memberArray[0].age;
    let clientSalution = "MR";
    if(this.memberArray[0].Gender == "F"){
      clientSalution = "MRS";
    }
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType": "C",
      "Salutation": clientSalution,
      "FirstName": this.memberArray[0].proposerName,
      "LastName": "",
      "DOB": age,
      "Gender": this.memberArray[0].Gender,
      "MaritalStatus": this.memberArray[0].maritalCode,
      "Occupation": this.memberArray[0].occupationCode,
      "PANNo": this.memberArray[0].pan,
      "GSTIN": "",
      "AadharNo": "",
      "CKYCNo": "",
      "EIANo": "",
      "Address1": {
        "AddrLine1": this.memberArray[0].address,
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": this.memberArray[0].pincode,
        "City": this.memberArray[0].city,
        "State": this.memberArray[0].state,
        "Country": "IND",
        "AddressType": "R",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberArray[0].mobile,
        "EmailAddr": this.memberArray[0].proposerEmail
      },
      "Address2": {
        "AddrLine1": "",
        "AddrLine2": "",
        "AddrLine3": "",
        "Landmark": "",
        "Pincode": "",
        "City": "",
        "State": "",
        "Country": "IND",
        "AddressType": "P",
        "HomeTelNo": "",
        "OfficeTelNo": "",
        "FAXNO": "",
        "MobileNo": this.memberArray[0].mobile,
        "EmailAddr": this.memberArray[0].proposerEmail
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  
  }
  getSelfBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    let mDob = cardDetails.InsuredDob.split(" ")[0].split("/");
    let gendr = this.memberArray[0].Gender;
    
    BeneDetails.push({
      "MemberId": cardDetails.MemberId ,
      "InsuredName": this.memberArray[0].proposerName,
      "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
      "InsuredGender": gendr,
      "InsuredOccpn": this.memberArray[0].occupationCode,
      "CoverType": cardDetails.CoverType,
      "SumInsured": cardDetails.SumInsured,
      "DeductibleDiscount": cardDetails.DeductibleDiscount,
      "Relation": cardDetails.Relation,
      "NomineeName": this.memberArray[0].nominee,
      "NomineeRelation":this.memberArray[0].NomineeRelation,
      "AnualIncome":cardDetails[0].annualIncome,
      "Height": cardDetails.Height,
      "Weight": cardDetails.Weight,
      "NomineeAge": this.memberArray[0].nomineeDate,
      "AppointeeName": this.memberArray[0].appointeeMember,
      "AptRelWithominee": this.memberArray[0].AptRelWithominee,
      "MedicalLoading": cardDetails.MedicalLoading,
      "PreExstDisease": "N",
      "DiseaseMedicalHistoryList": {
        "DiseaseMedicalHistory": {
          "PreExistingDiseaseCode": "",
          "MedicalHistoryDetail": ""
        }
      },
      "PrimaryCover": cardDetails[0].PrimaryCover,
      "SecondaryCover" : cardDetails[0].SecondaryCover
    })
    return BeneDetails;
  } 
  getBeneficiaryDetails(cardDetails){
    let BeneDetails = [];
    
    for(let i = 0 ;i <= cardDetails.length-1 ; i++){
      for(let j = 0; j <=this.memberArray.length-1;j++){
        if(i==j){
          if( (this.memberArray[j].code).toUpperCase() == (cardDetails[i].Relation).toUpperCase() ){
          
            let mDob = cardDetails[i].InsuredDob.split(" ")[0].split("/");
            let gendr = this.memberArray[j].Gender;
            console.log("Gender: " + this.memberArray[j].Gender);
            
            BeneDetails.push({
              "MemberId": cardDetails[i].MemberId ,
              "InsuredName": this.memberArray[j].proposerName,
              "InsuredDob": mDob[0]+"-"+mDob[1]+"-"+mDob[2],
              "InsuredGender": gendr,
              "InsuredOccpn": this.memberArray[j].occupationCode,
              "CoverType": cardDetails[i].CoverType,
              "SumInsured": cardDetails[i].SumInsured,
              "DeductibleDiscount": cardDetails[i].DeductibleDiscount,
              "Relation": cardDetails[i].Relation,
              "NomineeName": this.memberArray[j].nominee,
              "NomineeRelation": this.memberArray[j].NomineeRelation,
              "AnualIncome":this.memberArray[j].annualIncome,
              "Height": cardDetails[i].Height,
              "Weight": cardDetails[i].Weight,
              "NomineeAge": this.memberArray[j].nomineeDate,
              "AppointeeName": this.memberArray[j].appointeeMember,
              "AptRelWithominee":this.memberArray[j].AptRelWithominee,
              "MedicalLoading": cardDetails[i].MedicalLoading,
              "PreExstDisease": "N",
              "DiseaseMedicalHistoryList": {
                "DiseaseMedicalHistory": {
                  "PreExistingDiseaseCode": "",
                  "MedicalHistoryDetail": ""
                }
              },
              "PrimaryCover": cardDetails[i].PrimaryCover,
              "SecondaryCover" : cardDetails[0].SecondaryCover
            })
          }
        }
 
  
      }
  
    }
    console.log("ben Details:" + JSON.stringify(BeneDetails));
    
    return BeneDetails;
  
  } 

  getRelationForOtherMembers(memberDetailsrelation, selfGender){
    
  if(memberDetailsrelation == "Spouse"){
    this.OtherNomineeRelation = "Spouse";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son1" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son1" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son2" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son2" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son3" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son3" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son4" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Son4" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter1" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter1" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter2" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter2" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter3" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter3" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter4" && selfGender == "M"){
    this.OtherNomineeRelation = "Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Daughter4" && selfGender == "F"){
    this.OtherNomineeRelation = "Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Father" && selfGender == "M"){
    this.OtherNomineeRelation = "Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Father" && selfGender == "F"){
    this.OtherNomineeRelation = "Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Mother" && selfGender == "M"){
    this.OtherNomineeRelation = "Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Mother" && selfGender == "F"){
    this.OtherNomineeRelation = "Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Mother" && selfGender == "M"){
    this.OtherNomineeRelation = "Grand Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Mother" && selfGender == "F"){
    this.OtherNomineeRelation = "Grand Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Father" && selfGender =="M"){
    this.OtherNomineeRelation = "Grand Son";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Father" && selfGender =="F"){
    this.OtherNomineeRelation = "Grand Daughter";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Daughter" && selfGender == "M"){
    this.OtherNomineeRelation = "Grand Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Daughter" && selfGender == "F"){
    this.OtherNomineeRelation = "Grand Mother";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Son" && selfGender == "M"){
    this.OtherNomineeRelation = "Grand Father";
    return this.OtherNomineeRelation;
  }else if(memberDetailsrelation == "Grand Son" && selfGender == "F"){
    this.OtherNomineeRelation = "Grand Mother";
    return this.OtherNomineeRelation;
  }
    
  }


  onBackClick(events: Event){
    sessionStorage.isViewPopped = "true";
    console.log("MemberBackNominee: " + JSON.stringify(this.memberArray));
    
    sessionStorage.ProposerDetails = JSON.stringify(this.memberArray);
    console.log("NomineeBackMemberDetails: " + sessionStorage.ProposerDetails);
    
    sessionStorage.planCardDetails = JSON.stringify(this.selectedCardArray);   
    this.navCtrl.pop();
  }

  

  ItemClicked(title){
    this.activeItem=title;
  }

  onSaveAndEmail(){
    let isComplete = true;
    if(this.nomineeRelation == "" || this.nomineeRelation == undefined){
      this.showToast("Please enter relation with nominee against Self");
      isComplete = false;
    }else if(this.nomineeName == "" || this.nomineeName == undefined){
      this.showToast("Please enter nominee name against Self");
      isComplete = false;
    }else if(this.nomineeDate == "" || this.nomineeDate == undefined){
      this.showToast("Please enter nominee DOB against Self");
      isComplete = false;
    }else if(this.isDateValid(this.nomineeDate) == "2"){
      this.showToast("Please enter valid nominee DOB against Self");                       
      isComplete = false;
    }else if(this.isDateValid(this.nomineeDate) == "1"){
      if(this.appointeeName=="" || this.appointeeName == undefined){
        this.showToast("Please enter appointee name against Self");       
        isComplete = false;
      }else if(this.appointeeRelation=="" || this.appointeeRelation == undefined){
        this.showToast("Please enter appointee relation against Self");       
        isComplete = false;
      }
    }else{
      this.isComplete = true;

    }   
    isComplete ? this.saveEmailData() : null ;
  }

  saveEmailData(){      
    var sendData = this.appService.encryptData(JSON.stringify(this.getProceedrequest()),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1ForEProposalData("QuotePurchase.svc/Proceed_Policy",{'request': sendData});
  }

  sendStage1ForEProposalData(URL, serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
    this.presentLoadingDefault();
    this.appService.callService(URL,serviceData,headerString)
    .subscribe(Resp =>{
      this.loading.dismiss();
      console.log("Stage 1: "+ JSON.stringify(Resp)); 
        if(Resp.Proceed_PolicyResult.ReturnCode == "0"){
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          let QuotationID = Resp.Proceed_PolicyResult.Data.QuotationID;
          let UID = Resp.Proceed_PolicyResult.Data.UID;
          //this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.callEProposalService();        
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "807"){
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else if(Resp.Proceed_PolicyResult.ReturnCode == "500"){
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else{
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
    });
  }

  callEProposalService(){   
    var eProposalData = {
          "QuotationID": this.selectedCardArray.QuotationID,
          "UID": this.selectedCardArray.UID
        };
        console.log("Raw: " + JSON.stringify(eProposalData));
        var sendData = this.appService.encryptData(JSON.stringify(eProposalData),sessionStorage.TokenId+"~"+sessionStorage.UserId); 
  
        this.sendEProposerData({'request': sendData});
  }

  sendEProposerData(serviceData){
    let headerString = this.appService.getBase64string(sessionStorage.TokenId+"~"+sessionStorage.UserId);
      this.appService.callService("FGHealth.svc/Eproposal",serviceData, headerString)
      .subscribe(response =>{
        var resp = response.EproposalResult;
        console.log(JSON.stringify(response));
        if(resp && resp.ReturnCode == "0"){
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.thankyouPopup = false;
          //this.closeModalPop();
          //this.showToast("Successfully send E-Proposal data.")
        }else if(resp.ReturnCode == "807"){
          this.showToast(resp.ReturnMsg);
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else if(resp.ReturnCode == "500"){
          this.showToast("Oops! There seems to be a technical issue at our end. Please try again later.");
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        }else{
          sessionStorage.TokenId = resp.UserToken.TokenId;
          this.showToast(resp.ReturnMsg); 
        }        
       
      }, (err) => {
      console.log(err);
      });
  }
 
  setSelfAppointee(){

        const index: number = this.selfappointeeRelation.indexOf(this.selfappointeeName);
        if (index !== -1) {
            this.selfappointeeRelation.splice(index, 1);
        }  
    


  }
  setNomineeRelation(member){


    if(member == "Self"){

      if(this.memberArray[0].maritalStatus == "Divorced" || this.memberArray[0].maritalStatus == "Single" || this.memberArray[0].maritalStatus == "Widow / Widower"){
  
        this.appointeeArraySelf = ['Child1', 'Mother', 'Father', 'Child2', 'Uncle' , 'Aunt' , 'Sister' , 'Brother'];
       
      }
      else{
       
        this.appointeeArraySelf = ['Spouse', 'Child1', 'Mother', 'Father', 'Child2','Uncle' , 'Aunt' , 'Sister' , 'Brother'];
      }
  
  
  
      for(let i=0; i< this.memberArray.length;i++){
      
        if(this.memberArray[i].title == this.selfnomineeRelation){        
          this.selfnomineeName = this.memberArray[i].proposerName;
          console.log("age: " + this.memberArray[i].age);
           
      
          this.selfnomineeDate = this.memberArray[i].age;   
            
          let appointeeAge = this.getAge(this.selfnomineeDate);    
          if (appointeeAge  < 18) {
            this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
            this.hideApt = false;
          } else {
            // this.selfappointeeName = "";
            // this.selfappointeeRelation = "";
            this.hideApt = true;
          }
          break;
        }else{
          if(this.memberArray[0].maritalStatus == "Divorced"){
            this.showToast("Please ")
          }else{
            this.selfnomineeName = "";              
            this.selfnomineeDate = "";
          }
          
        }
      }
        const index: number = this.appointeeArraySelf.indexOf(this.selfnomineeRelation);
        if (index !== -1) {
          this.appointeeArraySelf.splice(index, 1);
  
          } 
    }


    else if(member == "Spouse"){
      this.appointeeArraySpouse = ['Self','Child1', 'Mother', 'Father', 'Child2'];
      

        for(let i=0; i< this.memberArray.length;i++){
        
          if(this.memberArray[i].title == this.spousenomineeRelation){        
            this.spousenomineeName = this.memberArray[i].proposerName;
            console.log("age: " + this.memberArray[i].age);
             
        
            this.spousenomineeDate = this.memberArray[i].age;   
              
            let appointeeAge = this.getAge(this.spousenomineeDate);    
            if (appointeeAge  < 18) {
              this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
              this.hideAptSpouse = false;
            } else {
              // this.selfappointeeName = "";
              // this.selfappointeeRelation = "";
              this.hideAptSpouse = true;
            }
            break;
          }else{
            if(this.memberArray[0].maritalStatus == "Divorced"){
              this.showToast("Please ")
            }else{
              this.spousenomineeName = "";              
              this.spousenomineeDate = "";
            }
            
          }
        }
          const index: number = this.appointeeArraySpouse.indexOf(this.spousenomineeRelation);
          if (index !== -1) {
            this.appointeeArraySpouse.splice(index, 1);
    
            } 
      
    }
  

    else if(member == "Child1"){

      this.appointeeArraySon = ['Self','Spouse', 'Mother', 'Father', 'Child2'];
     
    
    
        for(let i=0; i< this.memberArray.length;i++){
        
          if(this.memberArray[i].title == this.sonnomineeRelation){        
            this.sonnomineeName = this.memberArray[i].proposerName;
            console.log("age: " + this.memberArray[i].age);
             
        
            this.sonnomineeDate = this.memberArray[i].age;   
              
            let appointeeAge = this.getAge(this.sonnomineeDate);    
            if (appointeeAge  < 18) {
              this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
              this.hideAptChild1 = false;
            } else {
              // this.selfappointeeName = "";
              // this.selfappointeeRelation = "";
              this.hideAptChild1 = true;
            }
            break;
          }else{
            if(this.memberArray[0].maritalStatus == "Divorced"){
              this.showToast("Please ")
            }else{
              this.sonnomineeName = "";              
              this.sonnomineeDate = "";
            }
            
         
        }
          const index: number = this.appointeeArraySon.indexOf(this.sonnomineeRelation);
          if (index !== -1) {
            this.appointeeArraySon.splice(index, 1);
    
            } 
      }
    }



    else if(member == "Child2"){

      this.appointeeArrayDaughter = ['Self','Spouse','Child1','Mother', 'Father'];
     
    

     
    
    
        for(let i=0; i< this.memberArray.length;i++){
        
          if(this.memberArray[i].title == this.daughternomineeRelation){        
            this.daughternomineeName = this.memberArray[i].proposerName;
            console.log("age: " + this.memberArray[i].age);
             
        
            this.daughternomineeDate = this.memberArray[i].age;   
              
            let appointeeAge = this.getAge(this.daughternomineeDate);    
            if (appointeeAge  < 18) {
              this.showToast('Selected nominee is a minor. Please select a nominee above 18 years of age');
              this.hideAptChild2 = false;
            } else {
              // this.selfappointeeName = "";
              // this.selfappointeeRelation = "";
              this.hideAptChild2 = true;
            }
            break;
          }else{
            if(this.memberArray[0].maritalStatus == "Divorced"){
              this.showToast("Please ")
            }else{
              this.daughternomineeName = "";              
              this.daughternomineeDate = "";
            }
            
          }
        }
          const index: number = this.appointeeArrayDaughter.indexOf(this.daughternomineeRelation);
          if (index !== -1) {
            this.appointeeArrayDaughter.splice(index, 1);
    
            } 
     
    }
    
  }

  OkButton(){
    this.thankyouPopup = true;
    this.navCtrl.push(LandingScreenPage);
  }


}
  
