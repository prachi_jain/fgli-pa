import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { PremiumlistviewPage } from '../premiumlistview/premiumlistview';
import { TRANSITION_ID } from '@angular/platform-browser/src/browser/server-transition';
import { ComponentsModule } from '../../../components/components.module'
import { NumericStepperComponent } from '../../../components/numeric-stepper/numeric-stepper';
import { AddCommasPipe } from '../../../pipes/add-commas/add-commas'
import { NUMBER_TYPE } from '@angular/compiler/src/output/output_ast';

/**
 * Generated class for the PremiumcalciPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@NgModule({
  declarations: [
    //PremiumcalciPage,
  ],
  imports: [
    ComponentsModule,
    AddCommasPipe
    // IonicPageModule.forChild(PremiumcalciPage),
  ],
})
@IonicPage()
@Component({
  selector: 'page-premiumcalci',
  templateUrl: 'premiumcalci.html',
})
export class PremiumcalciPage {
  //Sourov =================================



  setSelfLimitVar = false;
  setSpouseLimitVar = false;
  setChild1LimitVar = false;
  setChild2LimitVar = false;

  maxLimitSelfAccDeath;
  maxLimitSelfPPD;
  maxLimitSelfPTD;
  maxLimitSelfTTD;


  maxLimitSpouseAccDeath;
  maxLimitSSpousePPD;
  maxLimitSpousePTD;
  maxLimitSpouseTTD;


  maxLimitChild1AccDeath;
  maxLimitChild1PPD;
  maxLimitChild1PTD;
  maxLimitChild1TTD;

  maxLimitChild2AccDeath;
  maxLimitChild2PPD;
  maxLimitChild2PTD;
  maxLimitChild2TTD;


  canShowToast = true;

  selfTabShow = true;
  spouseTabShow = true;
  child1TabShow = true;
  child2TabShow = true;

  selfadaptionallowncedivSow = true;
  selfaccidentahospitalisation = true;
  selfaccidentalmedicalExp = true;
  selfchildEdusupport = true;
  selffamilyTransportAllownce = true;
  selfhospitalCashAllownce = true;
  selflifeSuppBenefit = true;
  selfbrokenBones = true;
  selfroadAmbulanceCover = true;
  selfairAmbulanceCover = true;
  selfadventureSportsBenefit = true;
  selfchauffeurPlanBenefit = true;
  selfloanProtector = true;
  spouseadaptionallowncedivSow = true;
  spouseaccidentahospitalisation = true;
  spouseaccidentalmedicalExp = true;
  spousechildEdusupport = true;
  spousefamilyTransportAllownce = true;
  spousehospitalCashAllownce = true;
  spouselifeSuppBenefit = true;
  spousebrokenBones = true;
  spouseroadAmbulanceCover = true;
  spouseairAmbulanceCover = true;
  spouseadventureSportsBenefit = true;
  spousechauffeurPlanBenefit = true;
  spouseloanProtector = true;
  child1adaptionallowncedivSow = true;
  child1accidentahospitalisation = true;
  child1accidentalmedicalExp = true;
  child1Edusupport = true;
  child1familyTransportAllownce = true;
  child1hospitalCashAllownce = true;
  child1lifeSuppBenefit = true;
  child1brokenBones = true;
  child1roadAmbulanceCover = true;
  child1airAmbulanceCover = true;
  child1adventureSportsBenefit = true;
  child1chauffeurPlanBenefit = true;
  child1loanProtector = true;
  child2adaptionallowncedivSow = true;
  child2accidentahospitalisation = true;
  child2accidentalmedicalExp = true;
  child2Edusupport = true;
  child2familyTransportAllownce = true;
  child2hospitalCashAllownce = true;
  child2lifeSuppBenefit = true;
  child2brokenBones = true;
  child2roadAmbulanceCover = true;
  child2airAmbulanceCover = true;
  child2adventureSportsBenefit = true;
  child2chauffeurPlanBenefit = true;
  child2loanProtector = true;
  //addCovers checkbox ngModel value
  selfadaptation;
  selfaccidental;
  selfmedical;
  selfchild;
  selffamily;
  selfhospital;
  selflife;
  selfBroken;
  selfRoadAmbulance;
  selfAirAmbulance;
  selfAdventureSports;
  selfChauffeurPlan;
  selfloanValue;

  spouseadaptation;
  spouseaccidental;
  spousemedical;
  spousechild;
  spousefamily;
  spousehospital;
  spouselife;
  spouseBroken;
  spouseRoadAmbulance;
  spouseAirAmbulance;
  spouseAdventureSports;
  spouseChauffeurPlan;
  spouseloanValue;

  child1adaptation;
  child1accidental;
  child1medical;
  child1child;
  child1family;
  child1hospital;
  child1life;
  child1Broken;
  child1RoadAmbulance;
  child1AirAmbulance;
  child1AdventureSports;
  child1ChauffeurPlan;
  child1loanValue;

  child2adaptation;
  child2accidental;
  child2medical;
  child2child;
  child2family;
  child2hospital;
  child2life;
  child2Broken;
  child2RoadAmbulance;
  child2AirAmbulance;
  child2AdventureSports;
  child2ChauffeurPlan;
  child2loanValue;

  //checkboxNgModel
  selfpartialDis = true;
  selftotalDis = true;
  selftempDis = true;
  adaption_allowance = false;
  acc_hospitalisation = false;
  acc_medical_expenses = false;
  self_child_edu_support = false;
  self_family_trans_allowance = false;
  self_hospital_cash_allownce = false;
  self_life_support_benefit = false;
  self_broken_bones = false;
  self_road_ambulance_cover = false;
  self_air_ambulance_cover = false;
  self_adventure_sports_benefit = false;
  self_chauffeur_plan_benefit = false;
  self_loan_protector = false;


  spousepartialDis = true;
  spousetotalDis = true;
  spousetempDis = true;
  spouse_adaption_allowance = false;
  spouse_acc_hospitalisation = false;
  spouse_acc_medical_expenses = false;
  spouse_child_edu_support = false;
  spouse_family_trans_allowance = false;
  spouse_hospital_cash_allownce = false;
  spouse_life_support_benefit = false;
  spouse_broken_bones = false;
  spouse_road_ambulance_cover = false;
  spouse_air_ambulance_cover = false;
  spouse_adventure_sports_benefit = false;
  spouse_chauffeur_plan_benefit = false;
  spouse_loan_protector = false;

  child1partialDis = true;
  child1totalDis = true;
  child1_adaption_allowance = false;
  child1_acc_hospitalisation = false;
  child1_acc_medical_expenses = false;
  child1_edu_support = false;
  child1_family_trans_allowance = false;
  child1_hospital_cash_allownce = false;
  child1_life_support_benefit = false;
  child1_broken_bones = false;
  child1_road_ambulance_cover = false;
  child1_air_ambulance_cover = false;
  child1_adventure_sports_benefit = false;
  child1_chauffeur_plan_benefit = false;
  child1_loan_protector = false;

  child2partialDis = true;
  child2totalDis = true;
  child2_adaption_allowance = false;
  child2_acc_hospitalisation = false;
  child2_acc_medical_expenses = false;
  child2_edu_support = false;
  child2_family_trans_allowance = false;
  child2_hospital_cash_allownce = false;
  child2_life_support_benefit = false;
  child2_broken_bones = false;
  child2_road_ambulance_cover = false;
  child2_air_ambulance_cover = false;
  child2_adventure_sports_benefit = false;
  child2_chauffeur_plan_benefit = false;
  child2_loan_protector = false;




  //increment decrement div hide show value
  //adaption_allowance= true;
  adaptationallownceCheckDiv = true;
  accidentalhospitalDivShow = true;
  accidentalmedicalExpDivShow = true;
  childeduSuppDivShow = true;
  familyTransAllownceDivShow = true;
  hospicashAllownceDivShow = true;
  lifeSuppBenefitDivShow = true;
  brokenBonesDivShow = true;
  roadAmbulanceCoverDivShow = true;
  airambulancecoverDivShow = true;
  adventureSportsBenefitDivShow = true;
  chauffeurPlanBenefitDivShow = true;
  loanProtectorDivShow = true;

  showTransBagroundSelf = true;
  showTransBagroundSpouse = true;
  showTransBagroundChild1 = true;
  showTransBagroundChild2 = true;
  partdisab = false;
  permdisab = false;
  tempdisab = false;
  showPopupSelf = true;
  showPopupSpouse = true;
  showPopupChild1 = true;
  showPopupChild2 = true;
  lprotect = true;
  totalDis;
  //loanValue;
  spousepartdisab = false;
  spousepermdisab = false;
  spousetempdisab = false;
  spouseadaptationallownceCheckDiv = true;
  spouseaccidentalhospitalDivShow = true;
  spouseaccidentalmedicalExpDivShow = true;
  spousechildeduSuppDivShow = true;
  spousefamilyTransAllownceDivShow = true;
  spousehospicashAllownceDivShow = true;
  spouselifeSuppBenefitDivShow = true;
  spousebrokenBonesDivShow = true;
  spouseroadAmbulanceCoverDivShow = true;
  spouseairambulancecoverDivShow = true;
  spouseadventureSportsBenefitDivShow = true;
  spousechauffeurPlanBenefitDivShow = true;
  spouseloanProtectorDivShow = true;
  child1partdisab = false;
  child1permdisab = false;
  child1tempdisab = false;
  child1adaptationallownceCheckDiv = true;
  child1accidentalhospitalDivShow = true;
  child1accidentalmedicalExpDivShow = true;
  child1eduSuppDivShow = true;
  child1familyTransAllownceDivShow = true;
  child1hospicashAllownceDivShow = true;
  child1lifeSuppBenefitDivShow = true;
  child1brokenBonesDivShow = true;
  child1roadAmbulanceCoverDivShow = true;
  child1airambulancecoverDivShow = true;
  child1adventureSportsBenefitDivShow = true;
  child1chauffeurPlanBenefitDivShow = true;
  child1loanProtectorDivShow = true;
  child2partdisab = false;
  child2permdisab = false;
  child2tempdisab = false;
  child2adaptationallownceCheckDiv = true;
  child2accidentalhospitalDivShow = true;
  child2accidentalmedicalExpDivShow = true;
  child2eduSuppDivShow = true;
  child2familyTransAllownceDivShow = true;
  child2hospicashAllownceDivShow = true;
  child2lifeSuppBenefitDivShow = true;
  child2loanProtectorDivShow = true;
  child2brokenBonesDivShow = true;
  child2roadAmbulanceCoverDivShow = true;
  child2airambulancecoverDivShow = true;
  child2adventureSportsBenefitDivShow = true;
  child2chauffeurPlanBenefitDivShow = true;

  selfCategoryForChild;

  value;
  toggleStatus = true;
  loanProtectToggle = true;
  public isToggled: boolean;

  category: any = "";
  annualincome: any = "";

  insureddetails = new InsuredDetails();
  additionalCover = new AdditionalCover();

  spouseinsureddetails = new InsuredDetails();
  spouseadditionaldetails = new AdditionalCover();

  child1insureddetails = new InsuredDetails();
  child1additionaldetails = new AdditionalCover();

  child2insureddetails = new InsuredDetails();
  child2additionaldetails = new AdditionalCover();

  policyType;
  selfTTD;
  spouseTTD;
  hideProceedBtn = true
  lastActiveMember
  ActiveMember;

  SelfOccupationCode = "";
  SpouseOccupationCode = "";


  constructor(public navCtrl: NavController, public navParams: NavParams, public toast: ToastController) {
    this.isToggled = true;
    this.policyType = navParams.get("policyType");
    this.ActiveMember = "Self"
  }

  pet = 'Self';

  // ionViewDidEnter(){

  //   this.insureddetails.acc_maxinsured = 258899989;
  // }
  // ionViewWillEnter(){


  //   this.insureddetails.acc_maxinsured = 3000000;
  // }
  ionViewDidLoad() {
    this.selfTTD = false;
    this.spouseTTD = false;
    // this.numeric.initValue = 60000;


    this.insureddetails.acc_premium = 0;
    this.insureddetails.ptd_premium = 0;
    this.insureddetails.ppd_premium = 0;
    this.insureddetails.ttd_premium = 0;

    this.spouseinsureddetails.acc_premium = 0;
    this.spouseinsureddetails.ptd_premium = 0;
    this.spouseinsureddetails.ppd_premium = 0;
    this.spouseinsureddetails.ttd_premium = 0;

    this.child1insureddetails.acc_premium = 0;
    this.child1insureddetails.ptd_premium = 0;
    this.child1insureddetails.ppd_premium = 0;
    this.child1insureddetails.ttd_premium = 0;

    this.child2insureddetails.acc_premium = 0;
    this.child2insureddetails.ptd_premium = 0;
    this.child2insureddetails.ppd_premium = 0;
    this.child2insureddetails.ttd_premium = 0;


    console.log("test: " + this.showPopupSelf);

    console.log("self" + sessionStorage.selfdetails);
    console.log("spouse" + sessionStorage.spousedetails);
    console.log("spouseStatus" + sessionStorage.spouseShowStatus);
    console.log("child1Status" + sessionStorage.child1ShowStatus);
    console.log("child2Status" + sessionStorage.child2ShowStatus);


    console.log('ionViewDidLoad PremiumcalciPage');
    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 7000);


    if (sessionStorage.selfShowStatus == "true") {
      console.log("IF");
      this.selfTabShow = true;
      this.calculateSelfInsured();
      this.lastActiveMember = "Self"
      if (this.lastActiveMember == "Self" && this.policyType == "PATI") {

        this.hideProceedBtn = false
      }
      else {

        this.hideProceedBtn = true
      }


    }
    if (sessionStorage.spouseShowStatus == "true") {
      console.log("SpouseIF");
      this.spouseTabShow = false;
      this.calculateSpouseInsured();
      this.lastActiveMember = "Spouse"

    }
    if (sessionStorage.child1ShowStatus == "true") {
      this.child1TabShow = false;
      this.calculateChild1Insured();
      this.lastActiveMember = "Child1"

    }
    if (sessionStorage.child2ShowStatus == "true") {
      this.child2TabShow = false;
      this.calculateChild2Insured();
      this.lastActiveMember = "Child2"
    }

  }
  onBackClick() {
    this.navCtrl.pop();

  }
  clickTab(num) {
    if (num == 1) {

      this.ActiveMember = "Self"
      this.calculateSelfInsured();


    } else if (num == 2) {

      this.ActiveMember = "Spouse"
      this.calculateSpouseInsured();
      if (this.lastActiveMember == "Spouse") {

        this.hideProceedBtn = false

      }
      else {

        this.hideProceedBtn = true
      }



    } else if (num == 3) {
      this.ActiveMember = "Child1"
      this.calculateChild1Insured();

      if (this.lastActiveMember == "Child1") {

        this.hideProceedBtn = false

      }
      else {

        this.hideProceedBtn = true
      }

    } else if (num == 4) {
      this.ActiveMember = "Child2"
      this.calculateChild2Insured();


      if (this.lastActiveMember == "Child2") {

        this.hideProceedBtn = false

      }
      else {

        this.hideProceedBtn = true
      }
    }


    if (this.ActiveMember == "Self") {

      this.hideProceedBtn = true;
    }



  }

  getCurrVal(member, stepper, ev) {
    eval("this." + stepper + "=" + ev);

    console.log("input value" + eval("this." + stepper + "=" + ev))
    if (member == "Self") {

      console.log("this.insureddetails.acc_maxinsured" + this.insureddetails.acc_maxinsured)


      var selfdetails = JSON.parse(sessionStorage.selfdetails);
      this.category = selfdetails.occupation;
      if (this.category == "4") {

        this.category = "1"

      }
      this.calculatePremium("Self", this.category)
      this.calculateBenefitAmount(this.category, "Self");

      

     

        if(this.insureddetails.ptd_maxinsured > this.insureddetails.acc_maxinsured){


          this.insureddetails.ptd_maxinsured = this.insureddetails.acc_maxinsured
        }
     
        if(this.insureddetails.ppd_maxinsured > this.insureddetails.acc_maxinsured){


          this.insureddetails.ppd_maxinsured = this.insureddetails.acc_maxinsured
        }
        

    }

    if (member == "Spouse") {


      var spousedetails = JSON.parse(sessionStorage.spousedetails);
      this.category = spousedetails.occupation;
      if (this.category == "4") {

        this.category = "1"

      }


      this.calculatePremium("Spouse", this.category)
      this.calculateBenefitAmount(this.category, "Spouse");


    }


    if (member == "Child1") {

      var selfdetails = JSON.parse(sessionStorage.selfdetails);
      var child1Details = JSON.parse(sessionStorage.child1details);
      this.category = selfdetails.occupation;
      this.category = "1"
      if (this.category == "4") {

        this.category = "1"

      }
      this.calculatePremium("Child1", this.category)
      this.calculateBenefitAmount(this.category, "Child1");
    }


    if (member == "Child2") {

      var selfdetails = JSON.parse(sessionStorage.selfdetails);
      var child2Details = JSON.parse(sessionStorage.child2details);
      this.category = selfdetails.occupation;
      this.category = "1"
      if (this.category == "4") {

        this.category = "1"

      }
      this.calculatePremium("Child2", this.category)
      this.calculateBenefitAmount(this.category, "Child2");

    }
  }


  nextPageClick() {

    if (this.ActiveMember == "Self") {

      if(Number(this.insureddetails.acc_maxinsured) < 50000)
      {
        this.showToast("You can not select Accidental death below Rs 50000");
        return
      }
      else if (Number(this.insureddetails.ppd_maxinsured) < 50000){
        this.showToast("You can not select Permanent partial disability below Rs 50000");
        return

      }
      else if (Number(this.insureddetails.ptd_maxinsured) < 50000){
        this.showToast("You can not select Permanent total disability below Rs 50000");
        return

      }
      else if((Number(this.insureddetails.ttd_maxinsured) < 10000)){
        this.showToast("You can not select Temporary total disability below Rs 10000");
        return
      }
      else{
      if (sessionStorage.spouseShowStatus == "true") {
        this.clickTab(2)
        this.ActiveMember = "Spouse"
        this.pet = "Spouse";
      }
      else if (sessionStorage.child1ShowStatus == "true") {
        this.clickTab(3)


        this.ActiveMember = "Child1"
        this.pet = "Child1";

      }
      else if (sessionStorage.child2ShowStatus == "true") {
        this.clickTab(4)

        this.ActiveMember = "Child2"
        this.pet = "Child2";
      }
    }

    }
    else if (this.ActiveMember == "Spouse") {

      this.clickTab(3)
      this.ActiveMember = "Child1"
      this.pet = "Child1";
      if (this.lastActiveMember == "Child1") {

        this.hideProceedBtn = false

      }
    }
    else if (this.ActiveMember == "Child1") {

      this.clickTab(4)
      this.ActiveMember = "Child2"
      this.pet = "Child2";
      if (this.lastActiveMember == "Child2") {

        this.hideProceedBtn = false

      }
    }




  }
  proceedtoNextPage() {
    
    

      if(Number(this.insureddetails.acc_maxinsured) < 50000)
      {
        this.showToast("You can not select Accidental death below Rs 50000");
        return
      }
      else if (Number(this.insureddetails.ppd_maxinsured) < 50000){
        this.showToast("You can not select Permanent partial disability below Rs 50000");
        return

      }
      else if (Number(this.insureddetails.ptd_maxinsured) < 50000){
        this.showToast("You can not select Permanent total disability below Rs 50000");
        return

      }
      else if((Number(this.insureddetails.ttd_maxinsured) < 10000)){
        this.showToast("You can not select Temporary total disability below Rs 10000");
        return
      }
    
      else{

    let test = 0;


    if (this.selfpartialDis == true) {
      console.log("IF");
      console.log("x" + this.insureddetails.ppd_premium);
      test = Number(test) + Number(this.insureddetails.ppd_premium);
    }

    if (this.selftotalDis == true) {
      console.log("IF");
      console.log("x" + this.insureddetails.ptd_premium);
      test = Number(test) + Number(this.insureddetails.ptd_premium);
    }

    if (this.selftempDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.insureddetails.ttd_premium);
    }

    if (this.adaption_allowance == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_adaptationallowance);
    }

    if (this.acc_hospitalisation == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_acchospitalisation);
    }

    if (this.acc_medical_expenses == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_accmedicalexpenses);
    }

    if (this.self_child_edu_support == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_childeducationsupport);
    }

    if (this.self_family_trans_allowance == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_familytransportallowance);
    }

    if (this.self_hospital_cash_allownce == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_hospitalcashallowance);
    }

    if (this.self_life_support_benefit == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_lifesupportbenefit);
    }
    if (this.self_broken_bones == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_brokenbones);
    }
    if (this.self_road_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_roadambulancecover);
    }
    if (this.self_air_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_airambulancecover);
    }
    if (this.self_adventure_sports_benefit == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_adventuresportbenefit);
    }
    if (this.self_chauffeur_plan_benefit == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_chauffeurplanbenefit);
    }
    if (this.self_loan_protector == true) {
      test = Number(test) + Number(this.additionalCover.selfpremium_loanprotector);
    }

    test = Number(test) + Number(this.insureddetails.acc_premium);


    if(this.SelfOccupationCode == "SPOT"){


      test = Number(test) * 2 
    }



    ///////calculate self  
    console.log("Self calculation" + test)

 

    if (this.spousepartialDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.spouseinsureddetails.ppd_premium);
    }

    if (this.spousetotalDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.spouseinsureddetails.ptd_premium);
    }

    if (this.spousetempDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.spouseinsureddetails.ttd_premium);
    }

    if (this.spouse_adaption_allowance == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_adaptationallowance);
    }

    if (this.spouse_acc_hospitalisation == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_acchospitalisation);
    }

    if (this.spouse_acc_medical_expenses == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_accmedicalexpenses);
    }

    if (this.spouse_child_edu_support == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_childeducationsupport);
    }

    if (this.spouse_family_trans_allowance == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_familytransportallowance);
    }

    if (this.spouse_hospital_cash_allownce == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_hospitalcashallowance);
    }

    if (this.spouse_life_support_benefit == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_lifesupportbenefit);
    }

    if (this.spouse_broken_bones == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_brokenbones);
    }
    if (this.spouse_road_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_roadambulancecover);
    }

    if (this.spouse_air_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_airambulancecover);
    }

    if (this.spouse_adventure_sports_benefit == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_adventuresportbenefit);
    }

    if (this.spouse_chauffeur_plan_benefit == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_chauffeurplanbenefit);
    }

    if (this.spouse_loan_protector == true) {
      test = Number(test) + Number(this.additionalCover.spousepremium_loanprotector);
    }

    test = Number(test) + Number(this.spouseinsureddetails.acc_premium);


    if(this.SpouseOccupationCode == "SPOT"){


      test = Number(test) * 2 
    }

    ///////calculate spouse  
    console.log("Spouse calculation" + test)



    if (this.child1partialDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.child1insureddetails.ppd_premium);
    }

    if (this.child1totalDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.child1insureddetails.ptd_premium);
    }

    if (this.child1_adaption_allowance == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_adaptationallowance);
    }

    if (this.child1_acc_hospitalisation == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_acchospitalisation);
    }

    if (this.child1_acc_medical_expenses == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_accmedicalexpenses);
    }

    if (this.child1_edu_support == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_childeducationsupport);
    }

    if (this.child1_family_trans_allowance == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_familytransportallowance);
    }

    if (this.child1_hospital_cash_allownce == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_hospitalcashallowance);
    }

    if (this.child1_life_support_benefit == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_lifesupportbenefit);
    }

    if (this.child1_broken_bones == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_brokenbones);
    }

    if (this.child1_road_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_roadambulancecover);
    }
    if (this.child1_air_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_airambulancecover);
    }
    if (this.child1_adventure_sports_benefit == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_adventuresportbenefit);
    }
    if (this.child1_chauffeur_plan_benefit == true) {
      test = Number(test) + Number(this.additionalCover.child1premium_chauffeurplanbenefit);
    }
    
    test = Number(test) + Number(this.child1insureddetails.acc_premium);

    ///////calculate child1  
    console.log("child1 calculation" + test)




    if (this.child2partialDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.child2insureddetails.ppd_premium);
    }

    if (this.child2totalDis == true) {
      console.log("IF");
      test = Number(test) + Number(this.child2insureddetails.ptd_premium);
    }

    if (this.child2_adaption_allowance == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_adaptationallowance);
    }

    if (this.child2_acc_hospitalisation == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_acchospitalisation);
    }

    if (this.child2_acc_medical_expenses == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_accmedicalexpenses);
    }

    if (this.child2_edu_support == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_childeducationsupport);
    }

    if (this.child2_family_trans_allowance == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_familytransportallowance);
    }

    if (this.child2_hospital_cash_allownce == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_hospitalcashallowance);
    }

    if (this.child2_life_support_benefit == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_lifesupportbenefit);
    }

    if (this.child2_broken_bones == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_brokenbones);
    }
    if (this.child2_road_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_roadambulancecover);
    }
    if (this.child2_air_ambulance_cover == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_airambulancecover);
    }
    if (this.child2_adventure_sports_benefit == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_adventuresportbenefit);
    }
    if (this.child2_chauffeur_plan_benefit == true) {
      test = Number(test) + Number(this.additionalCover.child2premium_chauffeurplanbenefit);
    }



    test = Number(test) + Number(this.child2insureddetails.acc_premium);


    ///////calculate child2  
    console.log("child2 calculation" + test)




    this.navCtrl.push(PremiumlistviewPage, {
      "totalPremium": Number(test)  .toFixed(2), "PolicyType": this.policyType
    });
  }

  }
  addCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    console.log("test: " + parts);
    return parts.join(".");
  }

  perClick(num, member) {
    console.log("self : " + member)
    if (num == 1 && member == "Self") {
      console.log("IF self");
      this.partdisab = !this.partdisab;
    } else if (num == 2 && member == "Self") {
      this.permdisab = !this.permdisab;
    } else if (num == 3 && member == "Self") {
      this.tempdisab = !this.tempdisab;
    } else if (num == 4 && member == "Self") {
      this.adaptationallownceCheckDiv = !this.adaptationallownceCheckDiv;
      this.spouseadaptionallowncedivSow = !this.spouseadaptionallowncedivSow;
      this.child1adaptionallowncedivSow = !this.child1adaptionallowncedivSow;
      this.child2adaptionallowncedivSow = !this.child2adaptionallowncedivSow;


    } else if (num == 5 && member == "Self") {

      this.accidentalhospitalDivShow = !this.accidentalhospitalDivShow;
      this.spouseaccidentalhospitalDivShow = !this.spouseaccidentalhospitalDivShow;
      this.child1accidentalhospitalDivShow = !this.child1accidentalhospitalDivShow;
      this.child2accidentalhospitalDivShow = !this.child2accidentalhospitalDivShow;
      this.spouseaccidentahospitalisation = !this.spouseaccidentahospitalisation;
      this.child1accidentahospitalisation = !this.child1accidentahospitalisation;
      this.child2accidentahospitalisation = !this.child2accidentahospitalisation;


    } else if (num == 6 && member == "Self") {
      this.accidentalmedicalExpDivShow = !this.accidentalmedicalExpDivShow;
      this.spouseaccidentalmedicalExp = !this.spouseaccidentalmedicalExp;
      this.spouseaccidentalmedicalExpDivShow = !this.spouseaccidentalmedicalExpDivShow;

      this.child1accidentalmedicalExp = !this.child1accidentalmedicalExp;
      this.child1accidentalmedicalExpDivShow = !this.child1accidentalmedicalExpDivShow;

      this.child2accidentalmedicalExp = !this.child2accidentalmedicalExp;
      this.child2accidentalmedicalExpDivShow = !this.child2accidentalmedicalExpDivShow;
    } else if (num == 7 && member == "Self") {
      this.childeduSuppDivShow = !this.childeduSuppDivShow;
      this.spousechildEdusupport = !this.spousechildEdusupport;
      this.spousechildeduSuppDivShow = !this.spousechildeduSuppDivShow;
    } else if (num == 8 && member == "Self") {
      this.familyTransAllownceDivShow = !this.familyTransAllownceDivShow;
      this.spousefamilyTransportAllownce = !this.spousefamilyTransportAllownce;
      this.spousefamilyTransAllownceDivShow = !this.spousefamilyTransAllownceDivShow;
      this.child1familyTransportAllownce = !this.child1familyTransportAllownce;
      this.child1familyTransAllownceDivShow = !this.child1familyTransAllownceDivShow;
      this.child2familyTransportAllownce = !this.child2familyTransportAllownce;
      this.child2familyTransAllownceDivShow = !this.child2familyTransAllownceDivShow;
    } else if (num == 9 && member == "Self") {
      this.hospicashAllownceDivShow = !this.hospicashAllownceDivShow;
      this.spousehospitalCashAllownce = !this.spousehospitalCashAllownce;
      this.spousehospicashAllownceDivShow = !this.spousehospicashAllownceDivShow;
      this.child1hospitalCashAllownce = !this.child1hospitalCashAllownce;
      this.child1hospicashAllownceDivShow = !this.child1hospicashAllownceDivShow;
      this.child2hospitalCashAllownce = !this.child2hospitalCashAllownce;
      this.child2hospicashAllownceDivShow = !this.child2hospicashAllownceDivShow;
    } else if (num == 10 && member == "Self") {
      this.lifeSuppBenefitDivShow = !this.lifeSuppBenefitDivShow;
      this.spouselifeSuppBenefit = !this.spouselifeSuppBenefit;
      this.spouselifeSuppBenefitDivShow = !this.spouselifeSuppBenefitDivShow;
      this.child1lifeSuppBenefit = !this.child1lifeSuppBenefit;
      this.child1lifeSuppBenefitDivShow = !this.child1lifeSuppBenefitDivShow;
      this.child2lifeSuppBenefit = !this.child2lifeSuppBenefit;
      this.child2lifeSuppBenefitDivShow = !this.child2lifeSuppBenefitDivShow;
    } else if (num == 11 && member == "Self") {
      this.brokenBonesDivShow = !this.brokenBonesDivShow;
      this.spousebrokenBones = !this.spousebrokenBones;
      this.spousebrokenBonesDivShow = !this.spousebrokenBonesDivShow;
      this.child1brokenBones = !this.child1brokenBones;
      this.child1brokenBonesDivShow = !this.child1brokenBonesDivShow;
      this.child2brokenBones = !this.child2brokenBones;
      this.child2brokenBonesDivShow = !this.child2brokenBonesDivShow;
    } else if (num == 12 && member == "Self") {
      this.roadAmbulanceCoverDivShow = !this.roadAmbulanceCoverDivShow;
    } else if (num == 13 && member == "Self") {
      this.airambulancecoverDivShow = !this.airambulancecoverDivShow;
    } else if (num == 14 && member == "Self") {
      this.adventureSportsBenefitDivShow = !this.adventureSportsBenefitDivShow;
    } else if (num == 15 && member == "Self") {
      this.chauffeurPlanBenefitDivShow = !this.chauffeurPlanBenefitDivShow;
    } else if (num == 16 && member == "Self") {
      this.loanProtectorDivShow = !this.loanProtectorDivShow;
      this.spouseloanProtector = !this.spouseloanProtector;
      this.spouseloanProtectorDivShow = !this.spouseloanProtectorDivShow;
      this.lprotect = !this.lprotect




    } else if (num == 1 && member == "Spouse") {
      this.spousepartdisab = !this.spousepartdisab;
    } else if (num == 2 && member == "Spouse") {
      this.spousepermdisab = !this.spousepermdisab;
    } else if (num == 3 && member == "Spouse") {
      this.spousetempdisab = !this.spousetempdisab;
    } else if (num == 4 && member == "Spouse") {
      this.spouseadaptationallownceCheckDiv = !this.spouseadaptationallownceCheckDiv;
    } else if (num == 5 && member == "Spouse") {
      this.spouseaccidentalhospitalDivShow = !this.spouseaccidentalhospitalDivShow;
    } else if (num == 6 && member == "Spouse") {
      this.spouseaccidentalmedicalExpDivShow = !this.spouseaccidentalmedicalExpDivShow;
    } else if (num == 7 && member == "Spouse") {
      this.spousechildeduSuppDivShow = !this.spousechildeduSuppDivShow;
    } else if (num == 8 && member == "Spouse") {
      this.spousefamilyTransAllownceDivShow = !this.spousefamilyTransAllownceDivShow;
    } else if (num == 9 && member == "Spouse") {
      this.spousehospicashAllownceDivShow = !this.spousehospicashAllownceDivShow;
    } else if (num == 10 && member == "Spouse") {
      this.spouselifeSuppBenefitDivShow = !this.spouselifeSuppBenefitDivShow;
    } else if (num == 11 && member == "Spouse") {
      this.spousebrokenBonesDivShow = !this.spousebrokenBonesDivShow;
    } else if (num == 12 && member == "Spouse") {
      this.spouseroadAmbulanceCoverDivShow = !this.spouseroadAmbulanceCoverDivShow;
    } else if (num == 13 && member == "Spouse") {
      this.spouseairambulancecoverDivShow = !this.spouseairambulancecoverDivShow;
    } else if (num == 14 && member == "Spouse") {
      this.spouseadventureSportsBenefitDivShow = !this.spouseadventureSportsBenefitDivShow;
    } else if (num == 15 && member == "Spouse") {
      this.spousechauffeurPlanBenefitDivShow = !this.spousechauffeurPlanBenefitDivShow;
    } else if (num == 16 && member == "Spouse") {
      this.spouseloanProtectorDivShow = !this.spouseloanProtectorDivShow;
    } else if (num == 1 && member == "Child1") {
      this.child1partdisab = !this.child1partdisab;
    } else if (num == 2 && member == "Child1") {
      this.child1permdisab = !this.child1permdisab;
    } else if (num == 3 && member == "Child1") {
      this.child1tempdisab = !this.child1tempdisab;
    } else if (num == 4 && member == "Child1") {
      this.child1adaptationallownceCheckDiv = !this.child1adaptationallownceCheckDiv;
    } else if (num == 5 && member == "Child1") {
      this.child1accidentalhospitalDivShow = !this.child1accidentalhospitalDivShow;
    } else if (num == 6 && member == "Child1") {
      this.child1accidentalmedicalExpDivShow = !this.child1accidentalmedicalExpDivShow;
    } else if (num == 7 && member == "Child1") {
      this.child1eduSuppDivShow = !this.child1eduSuppDivShow;
    } else if (num == 8 && member == "Child1") {
      this.child1familyTransAllownceDivShow = !this.child1familyTransAllownceDivShow;
    } else if (num == 9 && member == "Child1") {
      this.child1hospicashAllownceDivShow = !this.child1hospicashAllownceDivShow;
    } else if (num == 10 && member == "Child1") {
      this.child1lifeSuppBenefitDivShow = !this.child1lifeSuppBenefitDivShow;
    } else if (num == 11 && member == "Child1") {
      this.child1brokenBonesDivShow = !this.child1brokenBonesDivShow;
    } else if (num == 12 && member == "Child1") {
      this.child1roadAmbulanceCoverDivShow = !this.child1roadAmbulanceCoverDivShow;
    } else if (num == 13 && member == "Child1") {
      this.child1airambulancecoverDivShow = !this.child1airambulancecoverDivShow;
    } else if (num == 14 && member == "Child1") {
      this.child1adventureSportsBenefitDivShow = !this.child1adventureSportsBenefitDivShow;
    } else if (num == 15 && member == "Child1") {
      this.child1chauffeurPlanBenefitDivShow = !this.child1chauffeurPlanBenefitDivShow;
    } else if (num == 1 && member == "Child2") {
      this.child2partdisab = !this.child2partdisab;
    } else if (num == 2 && member == "Child2") {
      this.child2permdisab = !this.child2permdisab;
    } else if (num == 3 && member == "Child2") {
      this.child2tempdisab = !this.child2tempdisab;
    } else if (num == 4 && member == "Child2") {
      this.child2adaptationallownceCheckDiv = !this.child2adaptationallownceCheckDiv;
    } else if (num == 5 && member == "Child2") {
      this.child2accidentalhospitalDivShow = !this.child2accidentalhospitalDivShow;
    } else if (num == 6 && member == "Child2") {
      this.child2accidentalmedicalExpDivShow = !this.child2accidentalmedicalExpDivShow;
    } else if (num == 7 && member == "Child2") {
      this.child2eduSuppDivShow = !this.child2eduSuppDivShow;
    } else if (num == 8 && member == "Child2") {
      this.child2familyTransAllownceDivShow = !this.child2familyTransAllownceDivShow;
    } else if (num == 9 && member == "Child2") {
      this.child2hospicashAllownceDivShow = !this.child2hospicashAllownceDivShow;
    } else if (num == 10 && member == "Child2") {
      this.child2lifeSuppBenefitDivShow = !this.child2lifeSuppBenefitDivShow;
    } else if (num == 11 && member == "Child2") {
      this.child2brokenBonesDivShow = !this.child2brokenBonesDivShow;
    } else if (num == 12 && member == "Child2") {
      this.child2roadAmbulanceCoverDivShow = !this.child2roadAmbulanceCoverDivShow;
    } else if (num == 13 && member == "Child2") {
      this.child2airambulancecoverDivShow = !this.child2airambulancecoverDivShow;
    } else if (num == 14 && member == "Child2") {
      this.child2adventureSportsBenefitDivShow = !this.child2adventureSportsBenefitDivShow;
    } else if (num == 15 && member == "Child2") {
      this.child2chauffeurPlanBenefitDivShow = !this.child2chauffeurPlanBenefitDivShow;
    }

  }

  addCoverClck(num) {
    if (num == 1) {
      this.showPopupSelf = false;
      this.showTransBagroundSelf = false;
    } else if (num == 2) {
      this.showPopupSpouse = false;
      this.showTransBagroundSpouse = false;
    } else if (num == 3) {
      this.showPopupChild1 = false;
      this.showTransBagroundChild1 = false;
    } else if (num == 4) {
      this.showPopupChild2 = false;
      this.showTransBagroundChild2 = false;
    }

  }

  hidePopup(num) {
    if (num == 1) {
      this.showPopupSelf = true;
      this.showTransBagroundSelf = true;
      this.toggleStatus = false;
    } else if (num == 2) {
      this.showPopupSpouse = true;
      this.showTransBagroundSpouse = true;
    } else if (num == 3) {
      this.showPopupChild1 = true;
      this.showTransBagroundChild1 = true;
    } else if (num == 3) {
      this.showPopupChild2 = true;
      this.showTransBagroundChild2 = true;
    }

  }

  loanCheckClick() {

    this.selfloanValue = true
    if (this.lprotect == true) {
      this.lprotect = false
      this.toggleStatus = true;
    } else {
      this.lprotect = true

    }



  }

  applianceChange() {

    if (this.toggleStatus == true) {
      this.selfloanValue = true
      this.lprotect = false

    }
    else {
      this.selfloanValue = false
      this.lprotect = true
      this.toggleStatus = false
    }

    // if(this.isToggled == true){

    //   this.selfloanValue = false
    //   this.lprotect = true
    //   this.isToggled = true
    // }else{


    //  this.selfloanValue = true
    //  this.lprotect = false

    // }


  }







  //added by vinay
  calculateSelfInsured() {

    var selfdetails = JSON.parse(sessionStorage.selfdetails);
    this.category = selfdetails.occupation;
    this.selfCategoryForChild = this.category;
    this.annualincome = selfdetails.annualincome;
    this.SelfOccupationCode = selfdetails.occupationCode;
    if (this.category == "2" &&  this.SelfOccupationCode =="SPOT") {
      this.selfTTD = true;

      this.tempdisab = true;
    }

    this.calculateMaxSumInsured(this.category, this.annualincome, "Self");
    this.calculateBenefitAmount(this.category, "Self");


  }

  calculateSpouseInsured() {

    var spousedetails = JSON.parse(sessionStorage.spousedetails);
    this.category = spousedetails.occupation;
    console.log("category" + this.category);
    this.SpouseOccupationCode = spousedetails.occupationCode;
    if ((this.category == "2" && this.SpouseOccupationCode =="SPOT") || this.selfTTD == true || this.tempdisab == true) {
      this.spouseTTD = true;
      this.spousetempdisab = true;
    }
    this.annualincome = spousedetails.annualincome;

    this.calculateMaxSumInsured(this.category, this.annualincome, "Spouse");

  }

  calculateChild1Insured() {

    var selfdetails = JSON.parse(sessionStorage.selfdetails);
    var child1Details = JSON.parse(sessionStorage.child1details);
    this.category = selfdetails.occupation;
    this.annualincome = child1Details.annualincome;
    this.category = "1"
    this.calculateMaxSumInsured(this.category, this.annualincome, "Child1");

  }

  calculateChild2Insured() {

    var selfdetails = JSON.parse(sessionStorage.selfdetails);
    var child2Details = JSON.parse(sessionStorage.child2details);
    this.category = selfdetails.occupation;
    this.annualincome = child2Details.annualincome;
    this.category = "1"
    this.calculateMaxSumInsured(this.category, this.annualincome, "Child2");

  }

  getClassValuesForMaxSumInsured(category: number, type: any) {
    //Call API and get values
    console.log("testSpouseCategory" + category);

    var datavalues =
    {
      "AccidentalDeath": ["144", "144", "144"],
      "PPD": ["144", "144", "144"],
      "PTD": ["144", "144", "144"],
      "TTD": ["24", "24", "0"]
    };

    if (type == "ac") {
      return datavalues.AccidentalDeath[category - 1];
    } else if (type == "ppd") {
      return datavalues.PPD[category - 1];
    } else if (type == "ptd") {
      return datavalues.PTD[category - 1];
    } else if (type == "ttd") {
      return datavalues.TTD[category - 1];
    }

  }

  getClassValuesForPremium(category: any, type: any) {
    //Call API and get values
    var datavalues =
    {
      "AccidentalDeath": ["0.4", "0.6", "0.9"],
      "PPD": ["0.25", "0.45", "0.75"],
      "PTD": ["0.1", "0.2", "0.35"],
      "TTD": ["0.5", "0.75", "1"]
    };

    if (type == "ac") {
      return datavalues.AccidentalDeath[category - 1];
    } else if (type == "ppd") {
      return datavalues.PPD[category - 1];
    } else if (type == "ptd") {
      return datavalues.PTD[category - 1];
    } else if (type == "ttd") {
      return datavalues.TTD[category - 1];
    }

  }

  getClassValuesForAdditionalCoverPremium(type: any, member) {
    let category = 1;
    var datavalues =
    {
      "AdaptationAllowance": ["0.7", "0.75", "0.8"],
      "ChildEducationSupport": ["0.5", "0.8", "1.25"],
      "FamilyTransportationAllowance": ["0.3", "0.3", "0.3"],
      "HospitalCashAllowance": ["25", "30", "40"],
      "LoanProtector": ["2", "2.8", "4.25"],
      "LifeSuppostBenefit": ["0.1", "0.2", "0.35"],
      "AccidentalHospitalisation": ["1.5", "2", "3"],
      "AccidentalMedicalExpenses": ["", "", ""],
      "RepAndFuneralExpenses": ["", "", ""],
      "BrokenBones": ["2.89", "2.89", "2.89"],
      "RoadAmbulanceCover": ["2.174", "2.174", "2.174"],
      "AirAmbulanceCover": ["0.127", "0.127", "0.127"],
      "AdventureSportsBenefit": ["0.75", "1.2", "1.88"],
      "ChauffeurPlanBenefit": ["", "", ""]
    };

    if (member == "Self") {
      if (type == "aa") {
        return datavalues.AdaptationAllowance[this.category - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.category - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[this.category - 1];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[this.category - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.category - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[this.category - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[this.category - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.category - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.category - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.category - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.category - 1];
      } else if (type == "cpb") {
        return "";
      }
    } else if (member == "Spouse") {
      if (type == "aa") {
        return datavalues.AdaptationAllowance[this.category - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.category - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[this.category - 1];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[this.category - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.category - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[this.category - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[this.category - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.category - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.category - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.category - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.category - 1];
      } else if (type == "cpb") {
        return "";
      }
    } else if (member == "Child1") {
      console.log("category: " + this.selfCategoryForChild);

      if (type == "aa") {
        return datavalues.AdaptationAllowance[1 - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.selfCategoryForChild - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[this.selfCategoryForChild - 1];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[1 - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.selfCategoryForChild - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[1 - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[1 - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.selfCategoryForChild - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.selfCategoryForChild - 1];
      } else if (type == "cpb") {
        return "";
      }
    } else if (member == "Child2") {
      if (type == "aa") {
        return datavalues.AdaptationAllowance[1 - 1];
      } else if (type == "ces") {
        return datavalues.ChildEducationSupport[this.selfCategoryForChild - 1];
      } else if (type == "fta") {
        return datavalues.FamilyTransportationAllowance[this.selfCategoryForChild - 1];
      } else if (type == "hca") {
        return datavalues.HospitalCashAllowance[1 - 1];
      } else if (type == "lp") {
        return datavalues.LoanProtector[this.selfCategoryForChild - 1];
      } else if (type == "lsb") {
        return datavalues.LifeSuppostBenefit[1 - 1];
      } else if (type == "ach") {
        return datavalues.AccidentalHospitalisation[1 - 1];
      } else if (type == "ame") {
        return "";
      } else if (type == "rfe") {
        return "";
      } else if (type == "bb") {
        return datavalues.BrokenBones[this.selfCategoryForChild - 1];
      } else if (type == "rac") {
        return datavalues.RoadAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "aac") {
        return datavalues.AirAmbulanceCover[this.selfCategoryForChild - 1];
      } else if (type == "asb") {
        return datavalues.AdventureSportsBenefit[this.selfCategoryForChild - 1];
      } else if (type == "cpb") {
        return "";
      }
    }



  }

  calculateMaxSumInsured(category, annualincome, member) {
    // console.log(this.category);
    console.log("ttdSourov:" + annualincome);
    console.log("SourovCategory" + category);

    //if(category != "" && annualincome != ""){

    if (member == "Self") {
      console.log("SelfCal");
      if (this.setSelfLimitVar == false) {
        if (category == "4") {
          category = "1"
          this.category = "1"
        }
        var classvalue: any = this.getClassValuesForMaxSumInsured(category, "ac");
        console.log("class value: " + classvalue);
        this.insureddetails.acc_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;


        this.maxLimitSelfAccDeath = this.insureddetails.acc_maxinsured;
        console.log("ac" + this.insureddetails.acc_maxinsured);


        


        classvalue = this.getClassValuesForMaxSumInsured(category, "ppd");
        console.log("class value: " + classvalue);
        this.insureddetails.ppd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;

        this.maxLimitSelfPPD = this.insureddetails.ppd_maxinsured;



        classvalue = this.getClassValuesForMaxSumInsured(category, "ptd");
        console.log("class value: " + classvalue);
        this.insureddetails.ptd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;



        this.maxLimitSelfPTD = this.insureddetails.ptd_maxinsured




        classvalue = this.getClassValuesForMaxSumInsured(category, "ttd");
        console.log("class value: " + classvalue);
        this.insureddetails.ttd_maxinsured = (annualincome / 12) * classvalue;
        console.log("ttd" + this.insureddetails.ttd_maxinsured);


        if (this.category == 1) {
          if (Number(this.insureddetails.ttd_maxinsured) >= 1000000) {
            this.insureddetails.ttd_maxinsured = 1000000
          }
        } else if (this.category == 2) {
          if (Number(this.insureddetails.ttd_maxinsured) >= 1000000) {
            this.insureddetails.ttd_maxinsured = 1000000
          }
        }

        this.maxLimitSelfTTD = this.insureddetails.ttd_maxinsured;

        this.calculatePremium(member, category);


        this.setSelfLimitVar = true;
      }

      if(this.insureddetails.ppd_maxinsured > this.insureddetails.acc_maxinsured){


        this.insureddetails.ppd_maxinsured = this.insureddetails.acc_maxinsured
      }

      if(this.insureddetails.ptd_maxinsured > this.insureddetails.acc_maxinsured){


        this.insureddetails.ptd_maxinsured = this.insureddetails.acc_maxinsured
      }

    } else if (member == "Spouse") {
      console.log("SpouseCal");
      if (this.setSpouseLimitVar == false) {



        if (this.SelfOccupationCode == "HSWF") {
          /// in case of occupation househusband / housewife 
          category = "1"
          this.category = "1"
          this.spouseinsureddetails.acc_maxinsured = (this.insureddetails.acc_maxinsured / 2)
          this.spouseinsureddetails.ptd_maxinsured = (this.insureddetails.ptd_maxinsured / 2)
          this.spouseinsureddetails.ppd_maxinsured = (this.insureddetails.ppd_maxinsured / 2)
          this.spouseinsureddetails.ttd_maxinsured = (this.insureddetails.ttd_maxinsured / 2)

        }
        else {
          var classvalue: any = this.getClassValuesForMaxSumInsured(category, "ac");
          console.log("class value: " + classvalue);

          this.spouseinsureddetails.acc_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;

          if (this.spouseinsureddetails.acc_maxinsured > this.insureddetails.acc_maxinsured) {

            this.spouseinsureddetails.acc_maxinsured = this.insureddetails.acc_maxinsured


          }
          if (this.spouseinsureddetails.ppd_maxinsured > this.insureddetails.ppd_maxinsured) {

            this.spouseinsureddetails.ppd_maxinsured = this.insureddetails.ppd_maxinsured


          }

          if (this.spouseinsureddetails.ptd_maxinsured > this.insureddetails.ptd_maxinsured) {

            this.spouseinsureddetails.ptd_maxinsured = this.insureddetails.ptd_maxinsured

          }


         if (this.spouseinsureddetails.ttd_maxinsured > this.insureddetails.ttd_maxinsured) {

            this.spouseinsureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured

          }

        
          else {
          classvalue = this.getClassValuesForMaxSumInsured(category, "ppd");
          console.log("class value: " + classvalue);
          this.spouseinsureddetails.ppd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;

          classvalue = this.getClassValuesForMaxSumInsured(category, "ptd");
          console.log("class value: " + classvalue);
          this.spouseinsureddetails.ptd_maxinsured = (annualincome / 12) * classvalue > 3000000 ? 3000000 : (annualincome / 12) * classvalue;

          classvalue = this.getClassValuesForMaxSumInsured(category, "ttd");
          console.log("class value: " + classvalue);
          this.spouseinsureddetails.ttd_maxinsured = (annualincome / 12) * classvalue;
          console.log("ttd" + this.insureddetails.ttd_maxinsured);

          //this.checkSumInsured();


        }

      }


    }


    if (this.category == 1) {
      if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 1000000) {
        this.spouseinsureddetails.ttd_maxinsured = 1000000
      }
    } else if (this.category == 2) {
      if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 1000000) {
        this.spouseinsureddetails.ttd_maxinsured = 1000000
      }
    }

    if (this.spouseinsureddetails.acc_maxinsured > this.insureddetails.acc_maxinsured) {

      this.spouseinsureddetails.acc_maxinsured = this.insureddetails.acc_maxinsured


    }
    if (this.spouseinsureddetails.ppd_maxinsured > this.insureddetails.ppd_maxinsured) {

      this.spouseinsureddetails.ppd_maxinsured = this.insureddetails.ppd_maxinsured


    }

    if (this.spouseinsureddetails.ptd_maxinsured > this.insureddetails.ptd_maxinsured) {

      this.spouseinsureddetails.ptd_maxinsured = this.insureddetails.ptd_maxinsured

    }


    if (this.spouseinsureddetails.ttd_maxinsured > this.insureddetails.ttd_maxinsured) {

      this.spouseinsureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured

    }

    
    //this.maxLimitSpouseTTD =  this.spouseinsureddetails.ttd_maxinsured;

    this.calculatePremium(member, category);
    this.maxLimitSpouseAccDeath = this.spouseinsureddetails.acc_maxinsured;
    this.maxLimitSSpousePPD = this.spouseinsureddetails.ppd_maxinsured;
    this.maxLimitSpousePTD = this.spouseinsureddetails.ptd_maxinsured;
    this.maxLimitSpouseTTD = this.spouseinsureddetails.ttd_maxinsured;
    this.setSpouseLimitVar = true;




  } else if(member == "Child1") {
  console.log("child1");
  if (this.setChild1LimitVar == false) {
    var classvalue: any = this.getClassValuesForMaxSumInsured(category, "ac");
    console.log("class value: " + classvalue);
    this.child1insureddetails.acc_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

    if (this.child1insureddetails.acc_maxinsured > 500000) {
      this.child1insureddetails.acc_maxinsured = 500000
      this.child1insureddetails.ppd_maxinsured = 500000
      this.child1insureddetails.ptd_maxinsured = 500000

    } else {


      classvalue = this.getClassValuesForMaxSumInsured(category, "ppd");
      console.log("class value: " + classvalue);
      this.child1insureddetails.ppd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

      classvalue = this.getClassValuesForMaxSumInsured(category, "ptd");
      console.log("class value: " + classvalue);
      this.child1insureddetails.ptd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

      classvalue = this.getClassValuesForMaxSumInsured(category, "ttd");
      console.log("class value: " + classvalue);
      this.child1insureddetails.ttd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;
      console.log("ttd" + this.insureddetails.ttd_maxinsured);

      //this.checkSumInsured();
    }
    this.calculatePremium(member, category);
    this.maxLimitChild1AccDeath = this.child1insureddetails.acc_maxinsured;
    this.maxLimitChild1PPD = this.child1insureddetails.ppd_maxinsured;
    this.maxLimitChild1PTD = this.child1insureddetails.ptd_maxinsured;
    this.maxLimitChild1TTD = this.child1insureddetails.ttd_maxinsured;
    this.setChild1LimitVar = true;

  }
} else if (member == "Child2") {
  if (this.setChild2LimitVar == false) {
    var classvalue: any = this.getClassValuesForMaxSumInsured(category, "ac");
    console.log("class value: " + classvalue);
    this.child2insureddetails.acc_maxinsured = (this.insureddetails.acc_maxinsured) / 4;


    if (this.child2insureddetails.acc_maxinsured > 500000) {
      this.child2insureddetails.acc_maxinsured = 500000
      this.child2insureddetails.ppd_maxinsured = 500000
      this.child2insureddetails.ptd_maxinsured = 500000

    } else {
      classvalue = this.getClassValuesForMaxSumInsured(category, "ppd");
      console.log("class value: " + classvalue);
      this.child2insureddetails.ppd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

      classvalue = this.getClassValuesForMaxSumInsured(category, "ptd");
      console.log("class value: " + classvalue);
      this.child2insureddetails.ptd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;

      classvalue = this.getClassValuesForMaxSumInsured(category, "ttd");
      console.log("class value: " + classvalue);
      this.child2insureddetails.ttd_maxinsured = (this.insureddetails.acc_maxinsured) / 4;
      console.log("ttd" + this.insureddetails.ttd_maxinsured);

      //this.checkSumInsured();
    }
    this.calculatePremium(member, category);
    this.maxLimitChild2AccDeath = this.child2insureddetails.acc_maxinsured;
    this.maxLimitChild2PPD = this.child2insureddetails.ppd_maxinsured;
    this.maxLimitChild2PTD = this.child2insureddetails.ptd_maxinsured;
    this.maxLimitChild2TTD = this.child2insureddetails.ttd_maxinsured;
    this.setChild1LimitVar = true;
  }
}
this.calculateBenefitAmount(category, member);
    // }else{
    //   console.log("Unable to calculate max sum insured.");
    // }

  }

calculatePremium(member, category) {
  if (this.insureddetails.acc_maxinsured != "") {

    if (member == "Self") {
      var classvalue: any = this.getClassValuesForPremium(category, "ac");
      var amountinsured;
      amountinsured = this.insureddetails.acc_maxinsured;


      if (this.insureddetails.acc_suminsured != "") {
        if (this.insureddetails.acc_suminsured >= this.insureddetails.acc_maxinsured)
          amountinsured = this.insureddetails.acc_maxinsured;
        else
          amountinsured = this.insureddetails.acc_suminsured;
      }
      this.insureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);



      classvalue = this.getClassValuesForPremium(category, "ppd");
      amountinsured = this.insureddetails.ppd_maxinsured;
      if (this.insureddetails.ppd_suminsured != "") {
        if (this.insureddetails.ppd_suminsured >= this.insureddetails.ppd_maxinsured)
          amountinsured = this.insureddetails.ppd_maxinsured;
        else
          amountinsured = this.insureddetails.ppd_suminsured;
      }
      this.insureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);



      classvalue = this.getClassValuesForPremium(category, "ptd");
      amountinsured = this.insureddetails.ptd_maxinsured;
      if (this.insureddetails.ptd_suminsured != "") {
        if (this.insureddetails.ptd_suminsured >= this.insureddetails.ptd_maxinsured)
          amountinsured = this.insureddetails.ptd_maxinsured;
        else
          amountinsured = this.insureddetails.ptd_suminsured;
      }
      this.insureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);




      classvalue = this.getClassValuesForPremium(category, "ttd");
      amountinsured = this.insureddetails.ttd_maxinsured;
      if (this.insureddetails.ttd_suminsured != "") {
        if (this.insureddetails.ttd_suminsured >= this.insureddetails.ttd_maxinsured)
          amountinsured = this.insureddetails.ttd_maxinsured;
        else
          amountinsured = this.insureddetails.ttd_suminsured;
      }
      this.insureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);



      //this.calculateBenefitAmount();
    }
  }
  if (this.spouseinsureddetails.acc_maxinsured != "") {
    if (member == "Spouse") {
      var classvalue: any = this.getClassValuesForPremium(category, "ac");
      var amountinsured;
      amountinsured = this.spouseinsureddetails.acc_maxinsured;
      if (this.spouseinsureddetails.acc_suminsured != "") {
        if (this.spouseinsureddetails.acc_suminsured >= this.spouseinsureddetails.acc_maxinsured)
          amountinsured = this.spouseinsureddetails.acc_maxinsured;
        else
          amountinsured = this.spouseinsureddetails.acc_suminsured;
      }
      this.spouseinsureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ppd");
      amountinsured = this.spouseinsureddetails.ppd_maxinsured;
      if (this.spouseinsureddetails.ppd_suminsured != "") {
        if (this.spouseinsureddetails.ppd_suminsured >= this.spouseinsureddetails.ppd_maxinsured)
          amountinsured = this.spouseinsureddetails.ppd_maxinsured;
        else
          amountinsured = this.spouseinsureddetails.ppd_suminsured;
      }
      this.spouseinsureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ptd");
      amountinsured = this.spouseinsureddetails.ptd_maxinsured;
      if (this.spouseinsureddetails.ptd_suminsured != "") {
        if (this.spouseinsureddetails.ptd_suminsured >= this.spouseinsureddetails.ptd_maxinsured)
          amountinsured = this.spouseinsureddetails.ptd_maxinsured;
        else
          amountinsured = this.spouseinsureddetails.ptd_suminsured;
      }
      this.spouseinsureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ttd");
      amountinsured = this.spouseinsureddetails.ttd_maxinsured;
      if (this.spouseinsureddetails.ttd_suminsured != "") {
        if (this.spouseinsureddetails.ttd_suminsured >= this.spouseinsureddetails.ttd_maxinsured)
          amountinsured = this.spouseinsureddetails.ttd_maxinsured;
        else
          amountinsured = this.spouseinsureddetails.ttd_suminsured;
      }
      this.spouseinsureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);
      //this.calculateBenefitAmount();

    }
  }
  if (this.child1insureddetails.acc_maxinsured != "") {
    if (member == "Child1") {
      var classvalue: any = this.getClassValuesForPremium(category, "ac");
      var amountinsured;
      amountinsured = this.child1insureddetails.acc_maxinsured;
      if (this.child1insureddetails.acc_suminsured != "") {
        if (this.child1insureddetails.acc_suminsured >= this.child1insureddetails.acc_maxinsured)
          amountinsured = this.child1insureddetails.acc_maxinsured;
        else
          amountinsured = this.child1insureddetails.acc_suminsured;
      }
      this.child1insureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ppd");
      amountinsured = this.child1insureddetails.ppd_maxinsured;
      if (this.child1insureddetails.ppd_suminsured != "") {
        if (this.child1insureddetails.ppd_suminsured >= this.child1insureddetails.ppd_maxinsured)
          amountinsured = this.child1insureddetails.ppd_maxinsured;
        else
          amountinsured = this.child1insureddetails.ppd_suminsured;
      }
      this.child1insureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ptd");
      amountinsured = this.child1insureddetails.ptd_maxinsured;
      if (this.child1insureddetails.ptd_suminsured != "") {
        if (this.child1insureddetails.ptd_suminsured >= this.child1insureddetails.ptd_maxinsured)
          amountinsured = this.child1insureddetails.ptd_maxinsured;
        else
          amountinsured = this.child1insureddetails.ptd_suminsured;
      }
      this.child1insureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ttd");
      amountinsured = this.child1insureddetails.ttd_maxinsured;
      if (this.child1insureddetails.ttd_suminsured != "") {
        if (this.child1insureddetails.ttd_suminsured >= this.child1insureddetails.ttd_maxinsured)
          amountinsured = this.child1insureddetails.ttd_maxinsured;
        else
          amountinsured = this.child1insureddetails.ttd_suminsured;
      }
      this.child1insureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);
      //this.calculateBenefitAmount();

    }
  } if (this.child2insureddetails.acc_maxinsured != "") {
    if (member == "Child2") {
      var classvalue: any = this.getClassValuesForPremium(category, "ac");
      var amountinsured;
      amountinsured = this.child2insureddetails.acc_maxinsured;
      if (this.child2insureddetails.acc_suminsured != "") {
        if (this.child2insureddetails.acc_suminsured >= this.child2insureddetails.acc_maxinsured)
          amountinsured = this.child2insureddetails.acc_maxinsured;
        else
          amountinsured = this.child2insureddetails.acc_suminsured;
      }
      this.child2insureddetails.acc_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ppd");
      amountinsured = this.child2insureddetails.ppd_maxinsured;


      if (this.child2insureddetails.ppd_suminsured != "") {
        if (this.child2insureddetails.ppd_suminsured >= this.child2insureddetails.ppd_maxinsured)
          amountinsured = this.child2insureddetails.ppd_maxinsured;
        else
          amountinsured = this.child2insureddetails.ppd_suminsured;
      }
      this.child2insureddetails.ppd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ptd");
      amountinsured = this.child2insureddetails.ptd_maxinsured;
      if (this.child2insureddetails.ptd_suminsured != "") {
        if (this.child2insureddetails.ptd_suminsured >= this.child2insureddetails.ptd_maxinsured)
          amountinsured = this.child2insureddetails.ptd_maxinsured;
        else
          amountinsured = this.child2insureddetails.ptd_suminsured;
      }
      this.child2insureddetails.ptd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);

      classvalue = this.getClassValuesForPremium(category, "ttd");
      amountinsured = this.child2insureddetails.ttd_maxinsured;
      if (this.child2insureddetails.ttd_suminsured != "") {
        if (this.child2insureddetails.ttd_suminsured >= this.child2insureddetails.ttd_maxinsured)
          amountinsured = this.child2insureddetails.ttd_maxinsured;
        else
          amountinsured = this.child2insureddetails.ttd_suminsured;
      }
      this.child2insureddetails.ttd_premium = Number((amountinsured * classvalue) / 1000).toFixed(2);
      //this.calculateBenefitAmount();

    }
    console.log("Unable to calculate premium.");

  }

}


validateNumber(myString: String) {

  myString = myString.replace(/\D/g, '');

  return myString;

}

checkSumInsured() {

  if (this.insureddetails.acc_maxinsured != "") {
    if (this.insureddetails.acc_suminsured > this.insureddetails.acc_maxinsured)
      this.insureddetails.acc_suminsured = this.insureddetails.acc_maxinsured;
  }

  if (this.insureddetails.ppd_maxinsured != "") {
    if (this.insureddetails.ppd_suminsured > this.insureddetails.ppd_maxinsured)
      this.insureddetails.ppd_suminsured = this.insureddetails.ppd_maxinsured;
  }

  if (this.insureddetails.ptd_maxinsured != "") {
    if (this.insureddetails.ptd_suminsured > this.insureddetails.ptd_maxinsured)
      this.insureddetails.ptd_suminsured = this.insureddetails.ptd_maxinsured;
  }

  if (this.insureddetails.ttd_maxinsured != "") {
    if (this.insureddetails.ttd_suminsured > this.insureddetails.ttd_maxinsured)
      this.insureddetails.ttd_suminsured = this.insureddetails.ttd_maxinsured;
  }

}

calculateBenefitAmount(category, member) {
  category = "1";

  if (member == 'Self') {
    // var classvalue:any = this.getClassValuesForMaxSumInsured(category,"ac");
    //     console.log("class value: "+classvalue); 

    this.additionalCover.selfbenefitamount_adaptationallowance = (this.insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.insureddetails.acc_maxinsured) / 10;
    this.additionalCover.selfbenefitamount_childeducationsupport = (this.insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.insureddetails.acc_maxinsured) / 100;
    this.additionalCover.selfbenefitamount_familytransportallowance = (this.insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.insureddetails.acc_maxinsured) / 10;
    if (category == "1")
      this.additionalCover.selfbenefitamount_hospitalcashallowance = 2000;
    if (this.category == "2")
      this.additionalCover.selfbenefitamount_hospitalcashallowance = 1500;
    if (this.category == "3")
      this.additionalCover.selfbenefitamount_hospitalcashallowance = 1000;

    if (this.category == "1")
      this.additionalCover.selfbenefitamount_loanprotector = (this.insureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.insureddetails.acc_maxinsured) / 50;
    if (this.category == "2")
      this.additionalCover.selfbenefitamount_loanprotector = (this.insureddetails.acc_maxinsured) / 50 > 15000 ? 15000 : (this.insureddetails.acc_maxinsured) / 50;
    if (this.category == "3")
      this.additionalCover.selfbenefitamount_loanprotector = (this.insureddetails.acc_maxinsured) / 50 > 7500 ? 7500 : (this.insureddetails.acc_maxinsured) / 50;


    this.additionalCover.selfbenefitamount_lifesupportbenefit = (this.insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.insureddetails.acc_maxinsured) / 100;
    this.additionalCover.selfbenefitamount_acchospitalisation = (this.insureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.insureddetails.acc_maxinsured) / 4;
    this.additionalCover.selfbenefitamount_accmedicalexpenses = "Opted" //As per clause
    this.additionalCover.selfbenefitamount_repatriationfuneralexpenses = (this.insureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.insureddetails.acc_maxinsured) / 100;

    if (this.category == "1")
      this.additionalCover.selfbenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1500000 ? 1500000 : (this.annualincome / 12) * 24;
    if (this.category == "2")
      this.additionalCover.selfbenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1000000 ? 1000000 : (this.annualincome / 12) * 24;
    if (this.category == "3")
      this.additionalCover.selfbenefitamount_brokenbones = (this.annualincome / 12) * 24 > 200000 ? 200000 : (this.annualincome / 12) * 24;



    this.additionalCover.selfbenefitamount_roadambulancecover = 50000;



    this.additionalCover.selfbenefitamount_airambulancecover = (this.insureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.insureddetails.acc_maxinsured) / 100;
    this.additionalCover.selfbenefitamount_adventuresportbenefit = (this.insureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.insureddetails.acc_maxinsured) / 2;
    this.additionalCover.selfbenefitamount_chauffeurplanbenefit = 1000;

    this.calculateAdditionalCoverPremium(member);
  } else if (member == "Spouse") {
    this.additionalCover.spousebenefitamount_adaptationallowance = (this.spouseinsureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.spouseinsureddetails.acc_maxinsured) / 10;
    this.additionalCover.spousebenefitamount_childeducationsupport = (this.spouseinsureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.spouseinsureddetails.acc_maxinsured) / 100;
    this.additionalCover.spousebenefitamount_familytransportallowance = (this.spouseinsureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.spouseinsureddetails.acc_maxinsured) / 10;
    if (this.category == "1")
      this.additionalCover.spousebenefitamount_hospitalcashallowance = 2000;
    if (this.category == "2")
      this.additionalCover.spousebenefitamount_hospitalcashallowance = 1500;
    if (this.category == "3")
      this.additionalCover.spousebenefitamount_hospitalcashallowance = 1000;

    if (this.category == "1")
      this.additionalCover.spousebenefitamount_loanprotector = (this.spouseinsureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.spouseinsureddetails.acc_maxinsured) / 50;
    if (this.category == "2")
      this.additionalCover.spousebenefitamount_loanprotector = (this.spouseinsureddetails.acc_maxinsured) / 50 > 15000 ? 15000 : (this.spouseinsureddetails.acc_maxinsured) / 50;
    if (this.category == "3")
      this.additionalCover.spousebenefitamount_loanprotector = (this.spouseinsureddetails.acc_maxinsured) / 50 > 7500 ? 7500 : (this.spouseinsureddetails.acc_maxinsured) / 50;


    this.additionalCover.spousebenefitamount_lifesupportbenefit = (this.spouseinsureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.spouseinsureddetails.acc_maxinsured) / 100;
    this.additionalCover.spousebenefitamount_acchospitalisation = (this.spouseinsureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.spouseinsureddetails.acc_maxinsured) / 4;
    this.additionalCover.spousebenefitamount_accmedicalexpenses = "Opted"//As per clause
    console.log("spouStatus" + this.spouseTabShow);

    this.additionalCover.spousebenefitamount_repatriationfuneralexpenses = (this.spouseinsureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.spouseinsureddetails.acc_maxinsured) / 100;

    if (this.category == "1")
      this.additionalCover.spousebenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1500000 ? 1500000 : (this.annualincome / 12) * 24;
    if (this.category == "2")
      this.additionalCover.spousebenefitamount_brokenbones = (this.annualincome / 12) * 24 > 1000000 ? 1000000 : (this.annualincome / 12) * 24;
    if (this.category == "3")
      this.additionalCover.spousebenefitamount_brokenbones = (this.annualincome / 12) * 24 > 200000 ? 200000 : (this.annualincome / 12) * 24;

    this.additionalCover.spousebenefitamount_airambulancecover = (this.spouseinsureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.spouseinsureddetails.acc_maxinsured) / 100;
    this.additionalCover.spousebenefitamount_adventuresportbenefit = (this.spouseinsureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.spouseinsureddetails.acc_maxinsured) / 2;
    this.additionalCover.spousebenefitamount_chauffeurplanbenefit = 1000;


    this.additionalCover.spousebenefitamount_roadambulancecover = 50000;
    this.calculateAdditionalCoverPremium(member);
  } else if (member == "Child1") {
    this.additionalCover.child1benefitamount_adaptationallowance = (this.child1insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child1insureddetails.acc_maxinsured) / 10;
    this.additionalCover.child1benefitamount_childeducationsupport = (this.child1insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child1insureddetails.acc_maxinsured) / 100;
    this.additionalCover.child1benefitamount_familytransportallowance = (this.child1insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child1insureddetails.acc_maxinsured) / 10;
    if (this.category == "1")
      this.additionalCover.child1benefitamount_hospitalcashallowance = 2000;
    // if(this.category == "2")
    //   this.additionalCover.child1benefitamount_hospitalcashallowance = 1500; 
    // if(this.category == "3")
    //   this.additionalCover.child1benefitamount_hospitalcashallowance = 1000; 

    if (this.category == "1")
      this.additionalCover.child1benefitamount_loanprotector = (this.child1insureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.child1insureddetails.acc_maxinsured) / 50;
    // if(this.category == "2")
    // this.additionalCover.child1benefitamount_loanprotector = (this.child1insureddetails.acc_maxinsured)/50 > 15000 ? 15000 : (this.child1insureddetails.acc_maxinsured)/50;
    // if(this.category == "3")
    // this.additionalCover.child1benefitamount_loanprotector = (this.child1insureddetails.acc_maxinsured)/50 > 7500 ? 7500 : (this.child1insureddetails.acc_maxinsured)/50;


    this.additionalCover.child1benefitamount_lifesupportbenefit = (this.child1insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child1insureddetails.acc_maxinsured) / 100;
    this.additionalCover.child1benefitamount_acchospitalisation = (this.child1insureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.child1insureddetails.acc_maxinsured) / 4;
    this.additionalCover.child1benefitamount_accmedicalexpenses = "Opted"//As per clause
    this.additionalCover.child1benefitamount_repatriationfuneralexpenses = (this.child1insureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.child1insureddetails.acc_maxinsured) / 100;

    //if(this.category == "1")
    //this.additionalCover.child1benefitamount_brokenbones = (this.annualincome/12)*24 > 1500000 ? 1500000 : (this.annualincome/12)*24;
    // if(this.category == "2")
    //   this.additionalCover.child1benefitamount_brokenbones = (this.annualincome/12)*24 > 1000000 ? 1000000 : (this.annualincome/12)*24;
    // if(this.category == "3")
    //   this.additionalCover.child1benefitamount_brokenbones = (this.annualincome/12)*24 > 200000 ? 200000 : (this.annualincome/12)*24;
    this.additionalCover.child1benefitamount_brokenbones = Number(this.additionalCover.selfbenefitamount_brokenbones) / 4;

    this.additionalCover.child1benefitamount_airambulancecover = (this.child1insureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.child1insureddetails.acc_maxinsured) / 100;
    this.additionalCover.child1benefitamount_adventuresportbenefit = (this.child1insureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.child1insureddetails.acc_maxinsured) / 2;
    this.additionalCover.child1benefitamount_chauffeurplanbenefit = 1000;


    this.additionalCover.child1benefitamount_roadambulancecover = 50000;
    this.calculateAdditionalCoverPremium(member);
  } else if (member == 'Child2') {
    this.additionalCover.child2benefitamount_adaptationallowance = (this.child2insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child2insureddetails.acc_maxinsured) / 10;
    this.additionalCover.child2benefitamount_childeducationsupport = (this.child2insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child2insureddetails.acc_maxinsured) / 100;
    this.additionalCover.child2benefitamount_familytransportallowance = (this.child2insureddetails.acc_maxinsured) / 10 > 50000 ? 50000 : (this.child2insureddetails.acc_maxinsured) / 10;
    if (this.category == "1")
      this.additionalCover.child2benefitamount_hospitalcashallowance = 2000;
    // if(this.category == "2")
    //   this.additionalCover.child2benefitamount_hospitalcashallowance = 1500; 
    // if(this.category == "3")
    //   this.additionalCover.child2benefitamount_hospitalcashallowance = 1000; 


    if (this.category == "1")
      this.additionalCover.child2benefitamount_loanprotector = (this.child2insureddetails.acc_maxinsured) / 50 > 20000 ? 20000 : (this.child2insureddetails.acc_maxinsured) / 50;
    // if(this.category == "2")
    // this.additionalCover.child2benefitamount_loanprotector = (this.child2insureddetails.acc_maxinsured)/50 > 15000 ? 15000 : (this.child2insureddetails.acc_maxinsured)/50;
    // if(this.category == "3")
    // this.additionalCover.child2benefitamount_loanprotector = (this.child2insureddetails.acc_maxinsured)/50 > 7500 ? 7500 : (this.child2insureddetails.acc_maxinsured)/50;


    this.additionalCover.child2benefitamount_lifesupportbenefit = (this.child2insureddetails.acc_maxinsured) / 100 > 10000 ? 10000 : (this.child2insureddetails.acc_maxinsured) / 100;
    this.additionalCover.child2benefitamount_acchospitalisation = (this.child2insureddetails.acc_maxinsured) / 4 > 1000000 ? 1000000 : (this.child2insureddetails.acc_maxinsured) / 4;
    this.additionalCover.child2benefitamount_accmedicalexpenses = "Opted" //As per clause
    this.additionalCover.child2benefitamount_repatriationfuneralexpenses = (this.child2insureddetails.acc_maxinsured) / 100 > 12500 ? 12500 : (this.child2insureddetails.acc_maxinsured) / 100;

    // if(this.category == "1")
    //   this.additionalCover.child2benefitamount_brokenbones = (this.annualincome/12)*24 > 1500000 ? 1500000 : (this.annualincome/12)*24;
    // if(this.category == "2")
    //   this.additionalCover.child2benefitamount_brokenbones = (this.annualincome/12)*24 > 1000000 ? 1000000 : (this.annualincome/12)*24;
    // if(this.category == "3")
    //   this.additionalCover.child2benefitamount_brokenbones = (this.annualincome/12)*24 > 200000 ? 200000 : (this.annualincome/12)*24;

    this.additionalCover.child2benefitamount_brokenbones = Number(this.additionalCover.selfbenefitamount_brokenbones) / 4;
    //pending --> Ask viddhesh
    this.additionalCover.child2benefitamount_airambulancecover = (this.child2insureddetails.acc_maxinsured) / 100 > 500000 ? 500000 : (this.child2insureddetails.acc_maxinsured) / 100;
    this.additionalCover.child2benefitamount_adventuresportbenefit = (this.child2insureddetails.acc_maxinsured) / 2 > 5000000 ? 5000000 : (this.child2insureddetails.acc_maxinsured) / 2;
    this.additionalCover.child2benefitamount_chauffeurplanbenefit = 1000;


    this.additionalCover.child2benefitamount_roadambulancecover = 50000;
    this.calculateAdditionalCoverPremium(member);
  }

  //this.additionalCover.benefitamount_adaptationallowance = (this.insureddetails.acc_maxinsured)/10 > 50000 ? 50000 : (this.insureddetails.acc_maxinsured)/10;




}

calculateAdditionalCoverPremium(member) {

  if (member == 'Self') {
    var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
    this.additionalCover.selfpremium_childeducationsupport = Number((this.additionalCover.selfbenefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);;

    classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
    this.additionalCover.selfpremium_lifesupportbenefit = Number((this.additionalCover.selfbenefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
    this.additionalCover.selfpremium_accmedicalexpenses = Number((Number(this.insureddetails.acc_premium) +
      Number(this.insureddetails.ppd_premium) +
      Number(this.insureddetails.ptd_premium) +
      Number(this.insureddetails.ttd_premium)) * 0.2).toFixed(2);;

    classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
    this.additionalCover.selfpremium_acchospitalisation = Number((this.additionalCover.selfbenefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
    this.additionalCover.selfpremium_hospitalcashallowance = Number((this.additionalCover.selfbenefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
    this.additionalCover.selfpremium_loanprotector = Number((this.additionalCover.selfbenefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
    this.additionalCover.selfpremium_adaptationallowance = Number((this.additionalCover.selfbenefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
    this.additionalCover.selfpremium_familytransportallowance = Number((this.additionalCover.selfbenefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

    console.log("StatusCheck" + sessionStorage.selfShowStatus);

    if (sessionStorage.selfShowStatus == "true") {
      classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
      // this.additionalCover.selfpremium_repatriationfuneralexpenses = (this.additionalCover.selfbenefitamount_repatriationfuneralexpenses * classvalues) / 1000;
      this.additionalCover.selfpremium_repatriationfuneralexpenses = "";
    } else {
      this.additionalCover.selfpremium_repatriationfuneralexpenses = "";
    }


    classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
    this.additionalCover.selfpremium_brokenbones = Number((this.additionalCover.selfbenefitamount_brokenbones * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
    this.additionalCover.selfpremium_roadambulancecover = Number((this.additionalCover.selfbenefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
    this.additionalCover.selfpremium_airambulancecover = Number((this.additionalCover.selfbenefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
    this.additionalCover.selfpremium_adventuresportbenefit = Number((this.additionalCover.selfbenefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
    this.additionalCover.selfpremium_chauffeurplanbenefit = 119.50;


  } else if (member == "Spouse") {
    var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
    this.additionalCover.spousepremium_childeducationsupport = Number((this.additionalCover.spousebenefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
    this.additionalCover.spousepremium_lifesupportbenefit = Number((this.additionalCover.spousebenefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
    this.additionalCover.spousepremium_accmedicalexpenses = Number((Number(this.spouseinsureddetails.acc_premium) +
      Number(this.spouseinsureddetails.ppd_premium) +
      Number(this.spouseinsureddetails.ptd_premium) +
      Number(this.spouseinsureddetails.ttd_premium)) * 0.2).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
    this.additionalCover.spousepremium_acchospitalisation = Number((this.additionalCover.spousebenefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
    this.additionalCover.spousepremium_hospitalcashallowance = Number((this.additionalCover.spousebenefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);;

    classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
    this.additionalCover.spousepremium_loanprotector = Number((this.additionalCover.spousebenefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);;

    classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
    this.additionalCover.spousepremium_adaptationallowance = Number((this.additionalCover.spousebenefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
    this.additionalCover.spousepremium_familytransportallowance = Number((this.additionalCover.spousebenefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

    if (sessionStorage.spouseShowStatus == "true") {
      classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
      //this.additionalCover.spousepremium_repatriationfuneralexpenses = (this.additionalCover.spousebenefitamount_repatriationfuneralexpenses * classvalues) / 1000;
      this.additionalCover.spousepremium_repatriationfuneralexpenses = "";
    } else {
      this.additionalCover.spousepremium_repatriationfuneralexpenses = "";
    }


    classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
    this.additionalCover.spousepremium_brokenbones = Number((this.additionalCover.spousebenefitamount_brokenbones * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
    this.additionalCover.spousepremium_roadambulancecover = Number((this.additionalCover.spousebenefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
    this.additionalCover.spousepremium_airambulancecover = Number((this.additionalCover.spousebenefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
    this.additionalCover.spousepremium_adventuresportbenefit = Number((this.additionalCover.spousebenefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
    this.additionalCover.spousepremium_chauffeurplanbenefit = 119.50;
  } else if (member == "Child1") {

    var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
    this.additionalCover.child1premium_childeducationsupport = Number((this.additionalCover.child1benefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
    this.additionalCover.child1premium_lifesupportbenefit = Number((this.additionalCover.child1benefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
    this.additionalCover.child1premium_accmedicalexpenses = Number((Number(this.child1insureddetails.acc_premium) +
      Number(this.child1insureddetails.ppd_premium) +
      Number(this.child1insureddetails.ptd_premium)) * 0.2).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
    this.additionalCover.child1premium_acchospitalisation = Number((this.additionalCover.child1benefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);;

    classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
    this.additionalCover.child1premium_hospitalcashallowance = Number((this.additionalCover.child1benefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);;

    classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
    this.additionalCover.child1premium_loanprotector = Number((this.additionalCover.child1benefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
    this.additionalCover.child1premium_adaptationallowance = Number((this.additionalCover.child1benefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
    this.additionalCover.child1premium_familytransportallowance = Number((this.additionalCover.child1benefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

    if (sessionStorage.child1ShowStatus == "true") {
      classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
      this.additionalCover.child1premium_repatriationfuneralexpenses = "";
      // this.additionalCover.child1premium_repatriationfuneralexpenses = (this.additionalCover.child1benefitamount_repatriationfuneralexpenses * classvalues) / 1000
    } else {
      this.additionalCover.child1premium_repatriationfuneralexpenses = "";
    }


    classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
    this.additionalCover.child1premium_brokenbones = Number((this.additionalCover.child1benefitamount_brokenbones * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
    this.additionalCover.child1premium_roadambulancecover = Number((this.additionalCover.child1benefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
    this.additionalCover.child1premium_airambulancecover = Number((this.additionalCover.child1benefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
    this.additionalCover.child1premium_adventuresportbenefit = Number((this.additionalCover.child1benefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
    this.additionalCover.child1premium_chauffeurplanbenefit = 119.50;
  } else if (member == "Child2") {
    var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
    this.additionalCover.child2premium_childeducationsupport = Number((this.additionalCover.child2benefitamount_childeducationsupport * classvalues * 48) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("lsb", member);
    this.additionalCover.child2premium_lifesupportbenefit = Number((this.additionalCover.child2benefitamount_lifesupportbenefit * classvalues * 48) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("ame", member);
    this.additionalCover.child2premium_accmedicalexpenses = Number((Number(this.child2insureddetails.acc_premium) +
      Number(this.child2insureddetails.ppd_premium) +
      Number(this.child2insureddetails.ptd_premium)) * 0.2).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("ach", member);
    this.additionalCover.child2premium_acchospitalisation = Number((this.additionalCover.child2benefitamount_acchospitalisation * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("hca", member);
    this.additionalCover.child2premium_hospitalcashallowance = Number((this.additionalCover.child2benefitamount_hospitalcashallowance * classvalues) / 100).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("lp", member);
    this.additionalCover.child2premium_loanprotector = Number((this.additionalCover.child2benefitamount_loanprotector * classvalues * 12) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("aa", member);
    this.additionalCover.child2premium_adaptationallowance = Number((this.additionalCover.child2benefitamount_adaptationallowance * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("fta", member);
    this.additionalCover.child2premium_familytransportallowance = Number((this.additionalCover.child2benefitamount_familytransportallowance * classvalues) / 1000).toFixed(2);

    if (sessionStorage.child2ShowStatus == "true") {
      classvalues = this.getClassValuesForAdditionalCoverPremium("rfe", member);
      this.additionalCover.child1premium_repatriationfuneralexpenses = "";
      // this.additionalCover.child2premium_repatriationfuneralexpenses = (this.additionalCover.child2benefitamount_repatriationfuneralexpenses * classvalues) / 1000;
    } else {
      this.additionalCover.child2premium_repatriationfuneralexpenses = "";
    }


    classvalues = this.getClassValuesForAdditionalCoverPremium("bb", member);
    this.additionalCover.child2premium_brokenbones = Number((this.additionalCover.child2benefitamount_brokenbones * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("rac", member);
    this.additionalCover.child2premium_roadambulancecover = Number((this.additionalCover.child2benefitamount_roadambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("aac", member);
    this.additionalCover.child2premium_airambulancecover = Number((this.additionalCover.child2benefitamount_airambulancecover * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("asb", member);
    this.additionalCover.child2premium_adventuresportbenefit = Number((this.additionalCover.child2benefitamount_adventuresportbenefit * classvalues) / 1000).toFixed(2);

    classvalues = this.getClassValuesForAdditionalCoverPremium("cpb", member);
    this.additionalCover.child2premium_chauffeurplanbenefit = 119.50;
  }


}

addCovers() {
  console.log("pet: " + this.pet);
  // this.showPopup = true;
  // this.showTransBaground = true;

  if (this.selfadaptation == true && this.pet == "Self") {
    this.selfadaptionallowncedivSow = false;
    this.adaptationallownceCheckDiv = false;
    this.spouseadaptionallowncedivSow = false
    this.spouseadaptationallownceCheckDiv = false;
    this.child1adaptionallowncedivSow = false;
    this.child1adaptationallownceCheckDiv = false;
    this.child2adaptionallowncedivSow = false;
    this.child2adaptationallownceCheckDiv = false;
    this.adaption_allowance = true
    this.spouse_adaption_allowance = true
    this.child1_adaption_allowance = true
    this.child2_adaption_allowance = true
    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
    
    console.log("IF self");
  }
  // else{
  //   this.selfadaptionallowncedivSow = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }

  if (this.selfaccidental == true && this.pet == "Self") {
    this.accidentalhospitalDivShow = false;
    this.selfaccidentahospitalisation = false;
    this.spouseaccidentahospitalisation = false
    this.spouseaccidentalhospitalDivShow = false;
    this.child1accidentahospitalisation = false;
    this.child1accidentalhospitalDivShow = false;
    this.child2accidentahospitalisation = false;
    this.child2accidentalhospitalDivShow = false;
    this.acc_hospitalisation = true
    this.spouse_acc_hospitalisation = true
    this.child1_acc_hospitalisation = true
    this.child2_adaption_allowance = true
    this.child2_acc_hospitalisation = true
    this.selfaccidentahospitalisation = false;
    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }
  // else{
  //   this.selfaccidentahospitalisation = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }
  if (this.selfmedical == true && this.pet == "Self") {
    this.selfaccidentalmedicalExp = false;
    this.accidentalmedicalExpDivShow = false;
    this.spouseaccidentalmedicalExp = false
    this.spouseaccidentalmedicalExpDivShow = false
    this.child1accidentalmedicalExp = false
    this.child1accidentalmedicalExpDivShow = false
    this.child2accidentalmedicalExp = false
    this.child2accidentalmedicalExpDivShow = false
    this.acc_medical_expenses = true
    this.spouse_acc_medical_expenses = true
    this.child2_acc_medical_expenses = true
    this.child1_acc_medical_expenses = true

    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }
  // else{
  //   this.selfaccidentalmedicalExp = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }
  if (this.selfchild == true && this.pet == "Self") {
    this.selfchildEdusupport = false;
    this.childeduSuppDivShow = false;
    this.spousechildEdusupport = false
    this.spousechildeduSuppDivShow = false
    this.self_child_edu_support = true
    this.spouse_child_edu_support = true
    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }
  // else{
  //   this.selfchildEdusupport = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }
  if (this.selffamily == true && this.pet == "Self") {
    this.selffamilyTransportAllownce = false;
    this.familyTransAllownceDivShow = false;
    this.spousefamilyTransportAllownce = false;
    this.spousefamilyTransAllownceDivShow = false;
    this.child1familyTransportAllownce = false;
    this.child1familyTransAllownceDivShow = false;
    this.child2familyTransportAllownce = false;
    this.child2familyTransAllownceDivShow = false;
    this.self_family_trans_allowance = true
    this.spouse_family_trans_allowance = true
    this.child1_family_trans_allowance = true
    this.child2_family_trans_allowance = true
    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }
  // else{
  //   this.selffamilyTransportAllownce = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }
  if (this.selfhospital == true && this.pet == "Self") {
    this.selfhospitalCashAllownce = false;
    this.hospicashAllownceDivShow = false;
    this.spousehospitalCashAllownce = false;
    this.spousehospicashAllownceDivShow = false;
    this.child1hospitalCashAllownce = false;
    this.child1hospicashAllownceDivShow = false;
    this.child2hospitalCashAllownce = false;
    this.child2hospicashAllownceDivShow = false;
    this.self_hospital_cash_allownce = true
    this.spouse_hospital_cash_allownce = true
    this.child1_hospital_cash_allownce = true
    this.child2_hospital_cash_allownce = true
    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }
  // else{
  //   this.selfhospitalCashAllownce = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }
  if (this.selflife == true && this.pet == "Self") {
    this.selflifeSuppBenefit = false;
    this.lifeSuppBenefitDivShow = false;
    this.spouselifeSuppBenefit = false;
    this.spouselifeSuppBenefitDivShow = false;
    this.child1lifeSuppBenefit = false;
    this.child1lifeSuppBenefitDivShow = false;
    this.child2lifeSuppBenefit = false;
    this.child2lifeSuppBenefitDivShow = false

    this.self_life_support_benefit = true
    this.spouse_life_support_benefit = true
    this.child1_life_support_benefit = true;
    this.child2_life_support_benefit = true
    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }
  // else{
  //   this.selflifeSuppBenefit = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }
  if (this.selfloanValue == true && this.pet == "Self") {
    this.selfloanProtector = false;
    this.loanProtectorDivShow = false;
    this.spouseloanProtector = false;
    this.spouseloanProtectorDivShow = false;
    
    this.self_loan_protector = true;
    this.spouse_loan_protector = true;
    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }

  if (this.selfBroken == true && this.pet == "Self") {
    this.selfbrokenBones = false;
    this.brokenBonesDivShow = false;
    this.child1brokenBones = false;
    this.child2brokenBones = false;

    this.spousebrokenBones = false;
    this.spousebrokenBonesDivShow = false;
    this.child1brokenBonesDivShow = false;
    this.child2brokenBonesDivShow = false;

    this.self_broken_bones = true;
    this.spouse_broken_bones = true;
    this.child1_broken_bones = true;
    this.child2_broken_bones = true;

    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }

  if (this.selfRoadAmbulance == true && this.pet == "Self") {
    this.selfroadAmbulanceCover = false;
    this.spouseroadAmbulanceCover = false;
    this.child1roadAmbulanceCover = false;
    this.child2roadAmbulanceCover = false;

    this.roadAmbulanceCoverDivShow = false;
    this.spouseroadAmbulanceCoverDivShow = false;
    this.child1roadAmbulanceCoverDivShow = false;
    this.child2roadAmbulanceCoverDivShow = false;

    this.self_road_ambulance_cover = true;
    this.spouse_road_ambulance_cover = true;
    this.child1_road_ambulance_cover = true;
    this.child2_road_ambulance_cover = true;

    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }

  if (this.selfAirAmbulance == true && this.pet == "Self") {
    this.selfairAmbulanceCover = false;
    this.spouseairAmbulanceCover = false;
    this.child1airAmbulanceCover = false;
    this.child2airAmbulanceCover = false;

    this.airambulancecoverDivShow = false;
    this.spouseairambulancecoverDivShow = false;
    this.child1airambulancecoverDivShow = false;
    this.child2airambulancecoverDivShow = false;


    this.self_air_ambulance_cover = true;
    this.spouse_air_ambulance_cover = true;
    this.child1_air_ambulance_cover = true;
    this.child2_air_ambulance_cover = true;

    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }

  if (this.selfAdventureSports == true && this.pet == "Self") {
    this.selfadventureSportsBenefit = false;
    this.spouseadventureSportsBenefit = false;
    this.child1adventureSportsBenefit = false;
    this.child2adventureSportsBenefit = false;

    this.adventureSportsBenefitDivShow = false;
    this.spouseadventureSportsBenefitDivShow = false;
    this.child1adventureSportsBenefitDivShow = false;
    this.child2adventureSportsBenefitDivShow = false;

    this.self_adventure_sports_benefit = true;
    this.spouse_adventure_sports_benefit = true;
    this.child1_adventure_sports_benefit = true;
    this.child2_adventure_sports_benefit = true;

    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }

  if (this.selfChauffeurPlan == true && this.pet == "Self") {
    this.selfchauffeurPlanBenefit = false;
    this.spousechauffeurPlanBenefit = false;
    this.child1chauffeurPlanBenefit = false;
    this.child2chauffeurPlanBenefit = false;

    this.chauffeurPlanBenefitDivShow = false;
    this.spousechauffeurPlanBenefitDivShow = false;
    this.child1chauffeurPlanBenefitDivShow = false;
    this.child2chauffeurPlanBenefitDivShow = false;

    this.self_chauffeur_plan_benefit = true;
    this.spouse_chauffeur_plan_benefit = true;
    this.child1_chauffeur_plan_benefit = true;
    this.child2_chauffeur_plan_benefit = true;

    this.showPopupSelf = true;
    this.showTransBagroundSelf = true;
  }
  // else{
  //   this.selfloanProtector = true;
  //   this.showPopupSelf = true;
  //   this.showTransBagroundSelf = true;
  // }
  console.log("status" + this.spouseadaptation);
  if (this.spouseadaptation == true && this.pet == "Spouse") {
    console.log("IF spouse");


    this.spouseadaptionallowncedivSow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spouseadaptionallowncedivSow = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }

  if (this.spouseaccidental == true && this.pet == "Spouse") {
    this.spouseaccidentahospitalisation = false;
    //this.spouseaccidentalhospitalDivShow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spouseaccidentahospitalisation = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }
  if (this.spousemedical == true && this.pet == "Spouse") {
    this.spouseaccidentalmedicalExp = false;
    //this.spouseaccidentalmedicalExpDivShow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spouseaccidentalmedicalExp = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }
  if (this.spousechild == true && this.pet == "Spouse") {
    this.spousechildEdusupport = false;
    //this.spousechildeduSuppDivShow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spousechildEdusupport = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }
  if (this.spousefamily == true && this.pet == "Spouse") {
    this.spousefamilyTransportAllownce = false;
    //this.spousefamilyTransAllownceDivShow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spousefamilyTransportAllownce = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }
  if (this.spousehospital == true && this.pet == "Spouse") {
    this.spousehospitalCashAllownce = false;
    //this.spousehospicashAllownceDivShow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spousehospitalCashAllownce = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }
  if (this.spouselife == true && this.pet == "Spouse") {
    this.spouselifeSuppBenefit = false;
    //this.spouselifeSuppBenefitDivShow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spouselifeSuppBenefit = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }

  if (this.spouseBroken == true && this.pet == "Spouse") {
    this.spousebrokenBones = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }

  if (this.spouseRoadAmbulance == true && this.pet == "Spouse") {
    this.spouseroadAmbulanceCover = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }

  if (this.spouseAirAmbulance == true && this.pet == "Spouse") {
    this.spouseairAmbulanceCover = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }

  if (this.spouseAdventureSports == true && this.pet == "Spouse") {
    this.spouseadventureSportsBenefit = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }

  if (this.spouseChauffeurPlan == true && this.pet == "Spouse") {
    this.spousechauffeurPlanBenefit = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }

  if (this.spouseloanValue == true && this.pet == "Spouse") {
    this.spouseloanProtector = false;
    //this.spouseloanProtectorDivShow = false;
    this.showPopupSpouse = true;
    this.showTransBagroundSpouse = true;
  }
  // else{
  //   this.spouseloanProtector = true;
  //   this.spouseadaptionallowncedivSow = false;
  //   this.showPopupSpouse = true;
  //   this.showTransBagroundSpouse = true;
  // }

  if (this.child1adaptation == true && this.pet == "Child1") {

    this.child1adaptionallowncedivSow = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }
  // else{
  //   this.child1adaptionallowncedivSow = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }

  if (this.child1accidental == true && this.pet == "Child1") {
    this.child1accidentahospitalisation = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }
  // else{
  //   this.child1accidentahospitalisation = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }
  if (this.child1medical == true && this.pet == "Child1") {
    this.child1accidentalmedicalExp = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }
  // else{
  //   this.child1accidentalmedicalExp = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }
  if (this.child1child == true && this.pet == "Child1") {
    this.child1Edusupport = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }
  // else{
  //   this.child1Edusupport = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }
  if (this.child1family == true && this.pet == "Child1") {
    this.child1familyTransportAllownce = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }
  // else{
  //   this.child1familyTransportAllownce = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }
  if (this.child1hospital == true && this.pet == "Child1") {
    this.child1hospitalCashAllownce = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }
  // else{
  //   this.child1hospitalCashAllownce = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }
  if (this.child1life == true && this.pet == "Child1") {
    this.child1lifeSuppBenefit = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }
  // else{
  //   this.child1lifeSuppBenefit = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }
  if (this.child1loanValue == true && this.pet == "Child1") {
    this.child1loanProtector = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }

  if (this.child1Broken == true && this.pet == "Child1") {
    this.child1brokenBones = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }

  if (this.child1RoadAmbulance == true && this.pet == "Child1") {
    this.child1roadAmbulanceCover = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }

  if (this.child1AirAmbulance == true && this.pet == "Child1") {
    this.child1airAmbulanceCover = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }

  if (this.child1AdventureSports == true && this.pet == "Child1") {
    this.child1adventureSportsBenefit = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }

  if (this.child1ChauffeurPlan == true && this.pet == "Child1") {
    this.child1chauffeurPlanBenefit = false;
    this.showPopupChild1 = true;
    this.showTransBagroundChild1 = true;
  }


  // else{
  //   this.child1loanProtector = true;
  //   this.showPopupChild1 = true;
  //   this.showTransBagroundChild1 = true;
  // }

  if (this.child2adaptation == true && this.pet == "Child2") {
    console.log("IF spouse");

    this.child2adaptionallowncedivSow = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }
  // else{
  //   this.child2adaptionallowncedivSow = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }

  if (this.child2accidental == true && this.pet == "Child2") {
    this.child2accidentahospitalisation = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }
  // else{
  //   this.child2accidentahospitalisation = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }
  if (this.child2medical == true && this.pet == "Child2") {
    this.child2accidentalmedicalExp = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }
  // else{
  //   this.child2accidentalmedicalExp = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }
  if (this.child2child == true && this.pet == "Child2") {
    this.child2Edusupport = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }
  // else{
  //   this.child2Edusupport = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }
  if (this.child2family == true && this.pet == "Child2") {
    this.child2familyTransportAllownce = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }
  // else{
  //   this.child2familyTransportAllownce = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }
  if (this.child2hospital == true && this.pet == "Child2") {
    this.child2hospitalCashAllownce = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }
  // else{
  //   this.child2hospitalCashAllownce = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }
  if (this.child2life == true && this.pet == "Child2") {
    this.child2lifeSuppBenefit = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }
  // else{
  //   this.child2lifeSuppBenefit = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }
  if (this.child2loanValue == true && this.pet == "Child2") {
    this.child2loanProtector = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }

  if (this.child2Broken == true && this.pet == "Child2") {
    this.child2brokenBones = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }

  if (this.child2RoadAmbulance == true && this.pet == "Child2") {
    this.child2roadAmbulanceCover = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }

  if (this.child2AirAmbulance == true && this.pet == "Child2") {
    this.child2airAmbulanceCover = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }

  if (this.child2AdventureSports == true && this.pet == "Child2") {
    this.child2adventureSportsBenefit = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }

  if (this.child2ChauffeurPlan == true && this.pet == "Child2") {
    this.child2chauffeurPlanBenefit = false;
    this.showPopupChild2 = true;
    this.showTransBagroundChild2 = true;
  }

  // else{
  //   this.child2loanProtector = true;
  //   this.showPopupChild2 = true;
  //   this.showTransBagroundChild2 = true;
  // }

}


//Sourov=========================


showToast(Message) {
  if (this.canShowToast) {
    let pageToast = this.toast.create({
      message: Message,
      showCloseButton: true,
      closeButtonText: 'Ok',
      dismissOnPageChange: true,
      position: "bottom",
      duration: 5000,
    });
    this.canShowToast = false;
    pageToast.present();
    pageToast.onDidDismiss(() => {
      this.canShowToast = true;
    });
  }
}
onInputChange(ev: any, member) {

  let val = ev.target.value;
  // val = val.substring(0, val.length-1);
  // console.log("this value"+ val);


  if (member == "Self") {

    if (Number(this.insureddetails.acc_maxinsured) <= 50000) {
      this.insureddetails.acc_maxinsured = this.insureddetails.acc_maxinsured.substring(0, this.insureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfAccDeath)) {


      this.insureddetails.acc_maxinsured = this.insureddetails.acc_maxinsured.substring(0, this.insureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select above Rs" + this.insureddetails.acc_maxinsured);

    }
    else if (Number(this.insureddetails.ppd_maxinsured) <= 50000) {
      this.insureddetails.ppd_maxinsured = this.insureddetails.ppd_maxinsured.substring(0, this.insureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.insureddetails.ppd_maxinsured) >= Number(this.maxLimitSelfPPD)) {
      this.insureddetails.ppd_maxinsured = this.insureddetails.ppd_maxinsured.substring(0, this.insureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select above Rs" + this.insureddetails.ppd_maxinsured);
    }
    else if (Number(this.insureddetails.ptd_maxinsured) <= 50000) {
      this.insureddetails.ptd_maxinsured = this.insureddetails.ptd_maxinsured.substring(0, this.insureddetails.ptd_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.insureddetails.ptd_maxinsured) >= Number(this.maxLimitSelfPTD)) {
      this.insureddetails.ptd_maxinsured = this.insureddetails.ptd_maxinsured.substring(0, this.insureddetails.ptd_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.insureddetails.ptd_maxinsured);
    }
    else if (this.category == 1 || this.category == 2) {
      if (Number(this.insureddetails.ttd_maxinsured) <= 10000) {
        this.insureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured.substring(0, this.insureddetails.ttd_maxinsured.length - 1)
        this.showToast("You can not select below Rs 10000");
      }
    }
    else if (this.category == 1) {
      this.insureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured.substring(0, this.insureddetails.ttd_maxinsured.length - 1)
      if (Number(this.insureddetails.ttd_maxinsured) >= 5000000) {
        this.showToast("You can not select above Rs 5000000");
      }
    }
    else if (this.category == 2) {
      this.insureddetails.ttd_maxinsured = this.insureddetails.ttd_maxinsured.substring(0, this.insureddetails.ttd_maxinsured.length - 1)
      if (Number(this.insureddetails.ttd_maxinsured) >= 2500000) {
        this.showToast("You can not select above Rs 2500000");
      }
    }
  }
  else if (member == "Spouse") {

    if (Number(this.spouseinsureddetails.acc_maxinsured) <= 50000) {
      this.spouseinsureddetails.acc_maxinsured = this.spouseinsureddetails.acc_maxinsured.substring(0, this.spouseinsureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSpouseAccDeath)) {
      this.spouseinsureddetails.acc_maxinsured = this.spouseinsureddetails.acc_maxinsured.substring(0, this.spouseinsureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.spouseinsureddetails.acc_maxinsured);
    }
    else if (Number(this.spouseinsureddetails.ppd_maxinsured) <= 50000) {
      this.spouseinsureddetails.ppd_maxinsured = this.spouseinsureddetails.ppd_maxinsured.substring(0, this.spouseinsureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.spouseinsureddetails.ppd_maxinsured) >= Number(this.maxLimitSSpousePPD)) {
      this.spouseinsureddetails.ppd_maxinsured = this.spouseinsureddetails.ppd_maxinsured.substring(0, this.spouseinsureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.spouseinsureddetails.ppd_maxinsured);
    }
    else if (Number(this.spouseinsureddetails.ptd_maxinsured) <= 50000) {
      this.spouseinsureddetails.ptd_maxinsured = this.spouseinsureddetails.ptd_maxinsured.substring(0, this.spouseinsureddetails.ptd_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.spouseinsureddetails.ptd_maxinsured) >= Number(this.maxLimitSpousePTD)) {
      this.spouseinsureddetails.ptd_maxinsured = this.spouseinsureddetails.ptd_maxinsured.substring(0, this.spouseinsureddetails.ptd_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.spouseinsureddetails.ptd_maxinsured);
    }
    else if (this.category == 1 || this.category == 2) {
      this.spouseinsureddetails.ttd_maxinsured = this.spouseinsureddetails.ttd_maxinsured.substring(0, this.insureddetails.ttd_maxinsured.length - 1)
      if (Number(this.spouseinsureddetails.ttd_maxinsured) <= 10000) {
        this.showToast("You can not select below Rs 10000");
      }
    }
    else if (this.category == 1) {
      this.spouseinsureddetails.ttd_maxinsured = this.spouseinsureddetails.ttd_maxinsured.substring(0, this.insureddetails.ttd_maxinsured.length - 1)
      if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 5000000) {
        this.showToast("You can not select above Rs 5000000");
      }
    }
    else if (this.category == 2) {
      this.spouseinsureddetails.ttd_maxinsured = this.spouseinsureddetails.ttd_maxinsured.substring(0, this.insureddetails.ttd_maxinsured.length - 1)
      if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 2500000) {
        this.showToast("You can not select above Rs 2500000")
      }
    }
  }
  else if (member == "Child1") {
    if (Number(this.child1insureddetails.acc_maxinsured) <= 50000) {
      this.child1insureddetails.acc_maxinsured = this.child1insureddetails.acc_maxinsured.substring(0, this.child1insureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.child1insureddetails.acc_maxinsured) >= Number(this.maxLimitChild1AccDeath)) {
      this.child1insureddetails.acc_maxinsured = this.child1insureddetails.acc_maxinsured.substring(0, this.child1insureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.child1insureddetails.acc_maxinsured);
    }
    else if (Number(this.child1insureddetails.ppd_maxinsured) <= 50000) {
      this.child1insureddetails.ppd_maxinsured = this.child1insureddetails.ppd_maxinsured.substring(0, this.child1insureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.child1insureddetails.ppd_maxinsured) >= Number(this.maxLimitChild1PPD)) {
      this.child1insureddetails.ppd_maxinsured = this.child1insureddetails.ppd_maxinsured.substring(0, this.child1insureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.child1insureddetails.ppd_maxinsured);
    }
    else if (Number(this.child1insureddetails.ptd_maxinsured) <= 50000) {
      this.child1insureddetails.ptd_maxinsured = this.child1insureddetails.ptd_maxinsured.substring(0, this.child1insureddetails.ptd_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.child1insureddetails.ptd_maxinsured) >= Number(this.maxLimitChild1PTD)) {
      this.child1insureddetails.ptd_maxinsured = this.child1insureddetails.ptd_maxinsured.substring(0, this.child1insureddetails.ptd_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.child1insureddetails.ptd_maxinsured);
    }

  } else if (member == "Child2") {
    if (Number(this.child2insureddetails.acc_maxinsured) <= 50000) {
      this.child2insureddetails.acc_maxinsured = this.child2insureddetails.acc_maxinsured.substring(0, this.child2insureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.child2insureddetails.acc_maxinsured) >= Number(this.maxLimitChild2AccDeath)) {
      this.child2insureddetails.acc_maxinsured = this.child2insureddetails.acc_maxinsured.substring(0, this.child2insureddetails.acc_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.child2insureddetails.acc_maxinsured);

    }
    else if (Number(this.child2insureddetails.ppd_maxinsured) <= 50000) {
      this.child2insureddetails.ppd_maxinsured = this.child2insureddetails.ppd_maxinsured.substring(0, this.child2insureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.child2insureddetails.ppd_maxinsured) >= Number(this.maxLimitChild2PPD)) {
      this.child2insureddetails.ppd_maxinsured = this.child2insureddetails.ppd_maxinsured.substring(0, this.child2insureddetails.ppd_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.child2insureddetails.ppd_maxinsured);
    }
    else if (Number(this.child2insureddetails.ptd_maxinsured) <= 50000) {
      this.child2insureddetails.ptd_maxinsured = this.child2insureddetails.ptd_maxinsured.substring(0, this.child2insureddetails.ptd_maxinsured.length - 1)

      this.showToast("You can not select below Rs 50000");
    }
    else if (Number(this.child2insureddetails.ptd_maxinsured) >= Number(this.maxLimitChild2PTD)) {
      this.child2insureddetails.ptd_maxinsured = this.child2insureddetails.ptd_maxinsured.substring(0, this.child2insureddetails.ptd_maxinsured.length - 1)
      this.showToast("You can not select above Rs " + this.child2insureddetails.ptd_maxinsured);
    }
  }

  //this.calculatePremium(member, );



}
clickRemoveAccD(member, num) {
  if (member == "Self" && num == 1) {
    if (Number(this.insureddetails.acc_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.insureddetails.acc_maxinsured = Number(this.insureddetails.acc_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.insureddetails.acc_premium = (this.insureddetails.acc_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Spouse" && num == 1) {
    if (Number(this.spouseinsureddetails.acc_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.spouseinsureddetails.acc_maxinsured = Number(this.spouseinsureddetails.acc_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.spouseinsureddetails.acc_premium = (this.spouseinsureddetails.acc_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child1" && num == 1) {
    if (Number(this.child1insureddetails.acc_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.child1insureddetails.acc_maxinsured = Number(this.child1insureddetails.acc_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.child1insureddetails.acc_premium = (this.child1insureddetails.acc_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child2" && num == 1) {
    if (Number(this.child2insureddetails.acc_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.child2insureddetails.acc_maxinsured = Number(this.child2insureddetails.acc_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.child2insureddetails.acc_premium = (this.child2insureddetails.acc_maxinsured * classvalue) / 1000;
    }
  }
}

clickAddAccD(member, num) {
  if (member == "Self" && num == 1) {
    if (Number(this.insureddetails.acc_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfAccDeath)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfAccDeath);
    } else {
      this.insureddetails.acc_maxinsured = Number(this.insureddetails.acc_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.insureddetails.acc_premium = (this.insureddetails.acc_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Spouse" && num == 1) {
    if (Number(this.spouseinsureddetails.acc_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSelfAccDeath)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfAccDeath);
    } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSpouseAccDeath)) {
      this.showToast("You can not select above Rs " + this.maxLimitSpouseAccDeath);
    } else {
      this.spouseinsureddetails.acc_maxinsured = Number(this.spouseinsureddetails.acc_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.spouseinsureddetails.acc_premium = (this.spouseinsureddetails.acc_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child1" && num == 1) {
    if (Number(this.child1insureddetails.acc_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.child1insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfAccDeath)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfAccDeath);
    } else if (Number(this.child1insureddetails.acc_maxinsured) >= Number(this.maxLimitChild1AccDeath)) {
      this.showToast("You can not select above Rs " + this.maxLimitChild1AccDeath);
    } else {
      this.child1insureddetails.acc_maxinsured = Number(this.child1insureddetails.acc_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.child1insureddetails.acc_premium = (this.child1insureddetails.acc_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child2" && num == 1) {
    if (Number(this.child2insureddetails.acc_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.child2insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfAccDeath)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfAccDeath);
    } else if (Number(this.child2insureddetails.acc_maxinsured) >= Number(this.maxLimitChild2AccDeath)) {
      this.showToast("You can not select above Rs " + this.maxLimitChild2AccDeath);
    } else {
      this.child2insureddetails.acc_maxinsured = Number(this.child2insureddetails.acc_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ac");
      this.child2insureddetails.acc_premium = (this.child2insureddetails.acc_maxinsured * classvalue) / 1000;
    }
  }
}

clickRemovePPD(member, num) {
  if (member == "Self" && num == 2) {
    if (Number(this.insureddetails.ppd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.insureddetails.ppd_maxinsured = Number(this.insureddetails.ppd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.insureddetails.ppd_premium = (this.insureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Spouse" && num == 2) {
    if (Number(this.spouseinsureddetails.ppd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.spouseinsureddetails.ppd_maxinsured = Number(this.spouseinsureddetails.ppd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.spouseinsureddetails.ppd_premium = (this.spouseinsureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child1" && num == 2) {
    if (Number(this.child1insureddetails.ppd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.child1insureddetails.ppd_maxinsured = Number(this.child1insureddetails.ppd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.child1insureddetails.ppd_premium = (this.child1insureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child2" && num == 2) {
    if (Number(this.child2insureddetails.ppd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.child2insureddetails.ppd_maxinsured = Number(this.child2insureddetails.ppd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.child2insureddetails.ppd_premium = (this.child2insureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  }
}

clickAddPPD(member, num) {
  if (member == "Self" && num == 2) {
    if (Number(this.insureddetails.ppd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPPD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPPD);
    } else {
      this.insureddetails.ppd_maxinsured = Number(this.insureddetails.ppd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.insureddetails.ppd_premium = (this.insureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Spouse" && num == 2) {
    if (Number(this.spouseinsureddetails.ppd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPPD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPPD);
    } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSSpousePPD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSSpousePPD);
    } else {
      this.spouseinsureddetails.ppd_maxinsured = Number(this.spouseinsureddetails.ppd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.spouseinsureddetails.ppd_premium = (this.spouseinsureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child1" && num == 2) {
    if (Number(this.child1insureddetails.ppd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.child1insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPPD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPPD);
    } else if (Number(this.child1insureddetails.acc_maxinsured) >= Number(this.maxLimitChild1PPD)) {
      this.showToast("You can not select above Rs " + this.maxLimitChild1PPD);
    } else {
      this.child1insureddetails.ppd_maxinsured = Number(this.child1insureddetails.ppd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.child1insureddetails.ppd_premium = (this.child1insureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child2" && num == 2) {
    if (Number(this.child2insureddetails.ppd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.child2insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPPD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPPD);
    } else if (Number(this.child2insureddetails.acc_maxinsured) >= Number(this.maxLimitChild2PPD)) {
      this.showToast("You can not select above Rs " + this.maxLimitChild2PPD);
    } else {
      this.child2insureddetails.ppd_maxinsured = Number(this.child2insureddetails.ppd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ppd");
      this.child2insureddetails.ppd_premium = (this.child2insureddetails.ppd_maxinsured * classvalue) / 1000;
    }
  }

}

clickRemovePTD(member, num) {
  if (member == "Self" && num == 3) {
    if (Number(this.insureddetails.ptd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.insureddetails.ptd_maxinsured = Number(this.insureddetails.ptd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.insureddetails.ptd_premium = (this.insureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Spouse" && num == 3) {
    if (Number(this.spouseinsureddetails.ptd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.spouseinsureddetails.ptd_maxinsured = Number(this.spouseinsureddetails.ptd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.spouseinsureddetails.ptd_premium = (this.spouseinsureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child1" && num == 3) {
    if (Number(this.child1insureddetails.ptd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.child1insureddetails.ptd_maxinsured = Number(this.child1insureddetails.ptd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.child1insureddetails.ptd_premium = (this.child1insureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child2" && num == 3) {
    if (Number(this.child2insureddetails.ptd_maxinsured) <= 50000) {
      this.showToast("You can not select below Rs 50000");
    } else {
      this.child2insureddetails.ptd_maxinsured = Number(this.child2insureddetails.ptd_maxinsured) - 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.child2insureddetails.ptd_premium = (this.child2insureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  }
}

clickAddPTD(member, num) {
  if (member == "Self" && num == 3) {
    if (Number(this.insureddetails.ptd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPTD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPTD);
    } else {
      this.insureddetails.ptd_maxinsured = Number(this.insureddetails.ptd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.insureddetails.ptd_premium = (this.insureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Spouse" && num == 3) {
    if (Number(this.spouseinsureddetails.ptd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPTD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPTD);
    } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSpousePTD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSpousePTD);
    } else {
      this.spouseinsureddetails.ptd_maxinsured = Number(this.spouseinsureddetails.ptd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.spouseinsureddetails.ptd_premium = (this.spouseinsureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child1" && num == 3) {
    if (Number(this.child1insureddetails.ptd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.child1insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPTD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPTD);
    } else if (Number(this.child1insureddetails.acc_maxinsured) >= Number(this.maxLimitChild1PTD)) {
      this.showToast("You can not select above Rs " + this.maxLimitChild1PTD);
    } else {
      this.child1insureddetails.ptd_maxinsured = Number(this.child1insureddetails.ptd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.child1insureddetails.ptd_premium = (this.child1insureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  } else if (member == "Child2" && num == 3) {
    if (Number(this.child2insureddetails.ptd_maxinsured) >= 3000000) {
      this.showToast("You can not select above Rs " + "3000000");
    } else if (Number(this.child2insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfPTD)) {
      this.showToast("You can not select above Rs " + this.maxLimitSelfPTD);
    } else if (Number(this.child2insureddetails.acc_maxinsured) >= Number(this.maxLimitChild2PTD)) {
      this.showToast("You can not select above Rs " + this.maxLimitChild2PTD);
    } else {
      this.child2insureddetails.ptd_maxinsured = Number(this.child2insureddetails.ptd_maxinsured) + 10000;
      var classvalue: any = this.getClassValuesForPremium(this.category, "ptd");
      this.child2insureddetails.ptd_premium = (this.child2insureddetails.ptd_maxinsured * classvalue) / 1000;
    }
  }

}

clickRemoveTTD(member, num) {
  if (member == "Self" && num == 4) {
    if (this.category == 1 || this.category == 2) {
      if (Number(this.insureddetails.ttd_maxinsured) <= 10000) {
        this.showToast("You can not select below Rs 10000");
      } else {
        this.insureddetails.ttd_maxinsured = Number(this.insureddetails.ttd_maxinsured) - 10000;
        var classvalue: any = this.getClassValuesForPremium(this.category, "ttd");
        this.insureddetails.ttd_premium = (this.insureddetails.ttd_maxinsured * classvalue) / 1000;
      }
    }

  } else if (member == "Spouse" && num == 4) {
    if (this.category == 1 || this.category == 2) {
      if (Number(this.spouseinsureddetails.ttd_maxinsured) <= 10000) {
        this.showToast("You can not select below Rs 10000");
      } else {
        this.spouseinsureddetails.ttd_maxinsured = Number(this.spouseinsureddetails.ttd_maxinsured) - 10000;
        var classvalue: any = this.getClassValuesForPremium(this.category, "ttd");
        this.spouseinsureddetails.ttd_premium = (this.spouseinsureddetails.ttd_maxinsured * classvalue) / 1000;
      }
    }
  }
}

clickAddTTD(member, num) {
  if (member == "Self" && num == 4) {
    if (this.category == 1) {
      if (Number(this.insureddetails.ttd_maxinsured) >= 1000000) {
        this.showToast("You can not select above Rs 1000000");
      } else if (Number(this.insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfTTD)) {
        this.showToast("You can not select above Rs " + this.maxLimitSelfTTD);
      } else {
        this.insureddetails.ttd_maxinsured = Number(this.insureddetails.ttd_maxinsured) + 10000;
        var classvalue: any = this.getClassValuesForPremium(this.category, "ttd");
        this.insureddetails.ttd_premium = (this.insureddetails.ttd_maxinsured * classvalue) / 1000;
      }
    } else if (this.category == 2) {
      if (Number(this.insureddetails.ttd_maxinsured) >= 1000000) {
        this.showToast("You can not select above Rs 1000000");
      } else if (Number(this.insureddetails.acc_maxinsured) >= Number(this.maxLimitSelfTTD)) {
        this.showToast("You can not select above Rs " + this.maxLimitSelfTTD);
      } else {
        this.insureddetails.ttd_maxinsured = Number(this.insureddetails.ttd_maxinsured) + 10000;
        var classvalue: any = this.getClassValuesForPremium(this.category, "ttd");
        this.insureddetails.ttd_premium = (this.insureddetails.ttd_maxinsured * classvalue) / 1000;
      }
    }
  } else if (member == "Spouse" && num == 4) {
    if (this.category == 1) {
      if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 1000000) {
        this.showToast("You can not select above Rs 1000000");
      } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSelfTTD)) {
        this.showToast("You can not select above Rs " + this.maxLimitSelfTTD);
      } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSpouseTTD)) {
        this.showToast("You can not select above Rs " + this.maxLimitSpouseTTD);
      } else {
        this.spouseinsureddetails.ttd_maxinsured = Number(this.spouseinsureddetails.ttd_maxinsured) + 10000;
        var classvalue: any = this.getClassValuesForPremium(this.category, "ttd");
        this.spouseinsureddetails.ttd_premium = (this.spouseinsureddetails.ttd_maxinsured * classvalue) / 1000;
      }
    } else if (this.category == 2) {
      if (Number(this.spouseinsureddetails.ttd_maxinsured) >= 1000000) {
        this.showToast("You can not select above Rs 1000000")
      } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSelfTTD)) {
        this.showToast("You can not select above Rs " + this.maxLimitSelfTTD);
      } else if (Number(this.spouseinsureddetails.acc_maxinsured) >= Number(this.maxLimitSpouseTTD)) {
        this.showToast("You can not select above Rs " + this.maxLimitSpouseTTD);
      } else {
        this.spouseinsureddetails.ttd_maxinsured = Number(this.spouseinsureddetails.ttd_maxinsured) + 10000;
        var classvalue: any = this.getClassValuesForPremium(this.category, "ttd");
        this.spouseinsureddetails.ttd_premium = (this.spouseinsureddetails.ttd_maxinsured * classvalue) / 1000;
      }
    }

  }
}


clickAddRFE(member, num) {
  if (member == "Self" && num == 5) {

    if (Number(this.additionalCover.selfbenefitamount_repatriationfuneralexpenses) == 12000) {
      this.additionalCover.selfbenefitamount_repatriationfuneralexpenses = Number(this.additionalCover.selfbenefitamount_repatriationfuneralexpenses) + 500;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
      this.additionalCover.selfpremium_repatriationfuneralexpenses = (this.additionalCover.selfbenefitamount_repatriationfuneralexpenses * classvalues) / 1000;
    }

    else {

      if (Number(this.additionalCover.selfbenefitamount_repatriationfuneralexpenses) >= 12500) {
        this.showToast("You can not select above Rs 12500");
      } else {
        this.additionalCover.selfbenefitamount_repatriationfuneralexpenses = Number(this.additionalCover.selfbenefitamount_repatriationfuneralexpenses) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
        this.additionalCover.selfpremium_repatriationfuneralexpenses = (this.additionalCover.selfbenefitamount_repatriationfuneralexpenses * classvalues) / 1000;
      }
    }
  }
  //   if(member == "Spouse" && num == 5){


  //     if(Number(this.additionalCover.spousebenefitamount_repatriationfuneralexpenses) == 12000){
  //       this.additionalCover.spousebenefitamount_repatriationfuneralexpenses = Number(this.additionalCover.spousebenefitamount_repatriationfuneralexpenses) + 500;
  //       var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.spousepremium_repatriationfuneralexpenses = (this.additionalCover.spousebenefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //     }

  //     else{

  //     if(Number(this.additionalCover.spousebenefitamount_repatriationfuneralexpenses) >= 12500){
  //       this.showToast("You can not select above Rs 12500");
  //     }else{
  //       this.additionalCover.spousebenefitamount_repatriationfuneralexpenses = Number(this.additionalCover.spousebenefitamount_repatriationfuneralexpenses) + 10000;
  //       var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.spousepremium_repatriationfuneralexpenses = (this.additionalCover.spousebenefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //     }
  //   }
  //   }
  //   if(member == "Child1" && num == 5){

  //     if(Number(this.additionalCover.child1benefitamount_repatriationfuneralexpenses) == 12000){
  //       this.additionalCover.child1benefitamount_repatriationfuneralexpenses = Number(this.additionalCover.child1benefitamount_repatriationfuneralexpenses) + 500;
  //       var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.child1premium_repatriationfuneralexpenses = (this.additionalCover.child1benefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //     }

  //     else{
  //     if(Number(this.additionalCover.child1benefitamount_repatriationfuneralexpenses) >= 12500){
  //       this.showToast("You can not select above Rs 12500");
  //     }else{
  //       this.additionalCover.child1benefitamount_repatriationfuneralexpenses = Number(this.additionalCover.child1benefitamount_repatriationfuneralexpenses) + 10000;
  //       var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.child1premium_repatriationfuneralexpenses = (this.additionalCover.child1benefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //     }
  //   }
  //   }
  //   if(member == "Child2" && num == 5){
  //     if(Number(this.additionalCover.child2benefitamount_repatriationfuneralexpenses) == 12000){
  //       this.additionalCover.child2benefitamount_repatriationfuneralexpenses = Number(this.additionalCover.child2benefitamount_repatriationfuneralexpenses) + 500;
  //       var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.child2premium_repatriationfuneralexpenses = (this.additionalCover.child2benefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //     }

  //     else{
  //     if(Number(this.additionalCover.child2benefitamount_repatriationfuneralexpenses) >= 12500){
  //       this.showToast("You can not select above Rs 12500");
  //     }else{
  //       this.additionalCover.child2benefitamount_repatriationfuneralexpenses = Number(this.additionalCover.child2benefitamount_repatriationfuneralexpenses) + 10000;
  //       var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //       this.additionalCover.child2premium_repatriationfuneralexpenses = (this.additionalCover.child2benefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //     }
  //   }
  // }
}
clickRemoveRFE(member, num) {
  if (member == "Self" && num == 5) {
    if (Number(this.additionalCover.selfbenefitamount_repatriationfuneralexpenses) <= 0) {
      this.showToast("You can not select below Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_repatriationfuneralexpenses = Number(this.additionalCover.selfbenefitamount_repatriationfuneralexpenses) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
      this.additionalCover.selfpremium_repatriationfuneralexpenses = (this.additionalCover.selfbenefitamount_repatriationfuneralexpenses * classvalues) / 1000;
    }
  }
  // if(member == "Spouse" && num == 5){
  //   if(Number(this.additionalCover.spousebenefitamount_repatriationfuneralexpenses) <= 0){
  //     this.showToast("You can not select below Rs 0");
  //   }else{
  //     this.additionalCover.spousebenefitamount_repatriationfuneralexpenses = Number(this.additionalCover.spousebenefitamount_repatriationfuneralexpenses) - 10000;
  //     var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //     this.additionalCover.spousepremium_repatriationfuneralexpenses = (this.additionalCover.spousebenefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //   }
  // }
  // if(member == "Child1" && num == 5){
  //   if(Number(this.additionalCover.child1benefitamount_repatriationfuneralexpenses) <= 0){
  //     this.showToast("You can not select below Rs 0");
  //   }else{
  //     this.additionalCover.child1benefitamount_repatriationfuneralexpenses = Number(this.additionalCover.child1benefitamount_repatriationfuneralexpenses) - 10000;
  //     var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //     this.additionalCover.child1premium_repatriationfuneralexpenses = (this.additionalCover.child1benefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //   }
  // }
  // if(member == "Child2" && num == 5){
  //   if(Number(this.additionalCover.child2benefitamount_repatriationfuneralexpenses) <= 0){
  //     this.showToast("You can not select above Rs 0");
  //   }else{
  //     this.additionalCover.child2benefitamount_repatriationfuneralexpenses = Number(this.additionalCover.child2benefitamount_repatriationfuneralexpenses) + 10000;
  //     var classvalues:any = this.getClassValuesForAdditionalCoverPremium("rfe", member);
  //     this.additionalCover.child2premium_repatriationfuneralexpenses = (this.additionalCover.child2benefitamount_repatriationfuneralexpenses*classvalues)/1000;
  //   }
  // }

}

clickAddAdapAllow(member, num) {

  if (member == "Self" && num == 6) {


    if (Number(this.additionalCover.selfbenefitamount_adaptationallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.selfbenefitamount_adaptationallowance = Number(this.additionalCover.selfbenefitamount_adaptationallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.selfpremium_adaptationallowance = (this.additionalCover.selfbenefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
  if (member == "Spouse" && num == 6) {


    if (Number(this.additionalCover.spousebenefitamount_adaptationallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.spousebenefitamount_adaptationallowance = Number(this.additionalCover.spousebenefitamount_adaptationallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.spousepremium_adaptationallowance = (this.additionalCover.spousebenefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
  if (member == "Child1" && num == 6) {


    if (Number(this.additionalCover.child1benefitamount_adaptationallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.child1benefitamount_adaptationallowance = Number(this.additionalCover.child1benefitamount_adaptationallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.child1premium_adaptationallowance = (this.additionalCover.child1benefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
  if (member == "Child2" && num == 6) {

    if (Number(this.additionalCover.child2benefitamount_adaptationallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.child2benefitamount_adaptationallowance = Number(this.additionalCover.child2benefitamount_adaptationallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.child2premium_adaptationallowance = (this.additionalCover.child2benefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
}
clickRemoveAdapAllow(member, num) {
  if (member == "Self" && num == 6) {


    if (Number(this.additionalCover.selfbenefitamount_adaptationallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_adaptationallowance = Number(this.additionalCover.selfbenefitamount_adaptationallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.selfpremium_adaptationallowance = (this.additionalCover.selfbenefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
  if (member == "Spouse" && num == 6) {


    if (Number(this.additionalCover.spousebenefitamount_adaptationallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_adaptationallowance = Number(this.additionalCover.spousebenefitamount_adaptationallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.spousepremium_adaptationallowance = (this.additionalCover.spousebenefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
  if (member == "Child1" && num == 6) {


    if (Number(this.additionalCover.child1benefitamount_adaptationallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_adaptationallowance = Number(this.additionalCover.child1benefitamount_adaptationallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.child1premium_adaptationallowance = (this.additionalCover.child1benefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
  if (member == "Child2" && num == 6) {

    if (Number(this.additionalCover.child2benefitamount_adaptationallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child2benefitamount_adaptationallowance = Number(this.additionalCover.child2benefitamount_adaptationallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("aa", member);
      this.additionalCover.child2premium_adaptationallowance = (this.additionalCover.child2benefitamount_adaptationallowance * classvalues) / 1000;

    }

  }
}
clickAddAccHosp(member, num) {

  if (member == "Self" && num == 7) {


    if (Number(this.additionalCover.selfbenefitamount_acchospitalisation) >= 1000000) {
      this.showToast("You can not select above Rs 1000000");
    } else {
      this.additionalCover.selfbenefitamount_acchospitalisation = Number(this.additionalCover.selfbenefitamount_acchospitalisation) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.selfpremium_acchospitalisation = (this.additionalCover.selfbenefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
  if (member == "Spouse" && num == 7) {


    if (Number(this.additionalCover.spousebenefitamount_acchospitalisation) >= 1000000) {
      this.showToast("You can not select above Rs 1000000");
    } else {
      this.additionalCover.spousebenefitamount_acchospitalisation = Number(this.additionalCover.spousebenefitamount_acchospitalisation) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.spousepremium_acchospitalisation = (this.additionalCover.spousebenefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
  if (member == "Child1" && num == 7) {


    if (Number(this.additionalCover.child1benefitamount_acchospitalisation) >= 1000000) {
      this.showToast("You can not select above Rs 1000000");
    } else {
      this.additionalCover.child1benefitamount_acchospitalisation = Number(this.additionalCover.child1benefitamount_acchospitalisation) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.child1premium_acchospitalisation = (this.additionalCover.child1benefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
  if (member == "Child2" && num == 7) {

    if (Number(this.additionalCover.child2benefitamount_acchospitalisation) >= 1000000) {
      this.showToast("You can not select above Rs 1000000");
    } else {
      this.additionalCover.child2benefitamount_acchospitalisation = Number(this.additionalCover.child2benefitamount_acchospitalisation) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.child2premium_acchospitalisation = (this.additionalCover.child2benefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
}
clickRemoveAccHosp(member, num) {
  if (member == "Self" && num == 7) {


    if (Number(this.additionalCover.selfbenefitamount_acchospitalisation) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_acchospitalisation = Number(this.additionalCover.selfbenefitamount_acchospitalisation) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.selfpremium_acchospitalisation = (this.additionalCover.selfbenefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
  if (member == "Spouse" && num == 7) {


    if (Number(this.additionalCover.spousebenefitamount_acchospitalisation) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_acchospitalisation = Number(this.additionalCover.spousebenefitamount_acchospitalisation) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.spousepremium_acchospitalisation = (this.additionalCover.spousebenefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
  if (member == "Child1" && num == 7) {


    if (Number(this.additionalCover.child1benefitamount_acchospitalisation) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_acchospitalisation = Number(this.additionalCover.child1benefitamount_acchospitalisation) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.child1premium_acchospitalisation = (this.additionalCover.child1benefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
  if (member == "Child2" && num == 7) {

    if (Number(this.additionalCover.child2benefitamount_acchospitalisation) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child2benefitamount_acchospitalisation = Number(this.additionalCover.child2benefitamount_acchospitalisation) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ach", member);
      this.additionalCover.child2premium_acchospitalisation = (this.additionalCover.child2benefitamount_acchospitalisation * classvalues) / 1000;

    }

  }
}
clickAddChildEduSup(member, num) {

  if (member == "Self" && num == 9) {


    if (Number(this.additionalCover.selfbenefitamount_childeducationsupport) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.selfbenefitamount_childeducationsupport = Number(this.additionalCover.selfbenefitamount_childeducationsupport) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.selfpremium_childeducationsupport = (this.additionalCover.selfbenefitamount_childeducationsupport * classvalues * 48) / 1000;
    }

  }
  if (member == "Spouse" && num == 9) {


    if (Number(this.additionalCover.spousebenefitamount_childeducationsupport) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.spousebenefitamount_childeducationsupport = Number(this.additionalCover.spousebenefitamount_childeducationsupport) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.selfpremium_childeducationsupport = (this.additionalCover.selfbenefitamount_childeducationsupport * classvalues * 48) / 1000;
    }


  }


  if (member == "Child1" && num == 9) {


    if (Number(this.additionalCover.child1benefitamount_childeducationsupport) >= 1000000) {
      this.showToast("You can not select above Rs 1000000");
    } else {
      this.additionalCover.child1benefitamount_childeducationsupport = Number(this.additionalCover.child1benefitamount_childeducationsupport) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.child1premium_childeducationsupport = (this.additionalCover.child1benefitamount_childeducationsupport * classvalues * 48) / 1000;

    }

  }
  if (member == "Child2" && num == 9) {

    if (Number(this.additionalCover.child2benefitamount_childeducationsupport) >= 1000000) {
      this.showToast("You can not select above Rs 1000000");
    } else {
      this.additionalCover.child2benefitamount_childeducationsupport = Number(this.additionalCover.child2benefitamount_childeducationsupport) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.child2premium_childeducationsupport = (this.additionalCover.child2benefitamount_childeducationsupport * classvalues * 48) / 1000;

    }

  }
}


clickRemoveChildEduSup(member, num) {

  if (member == "Self" && num == 9) {


    if (Number(this.additionalCover.selfbenefitamount_childeducationsupport) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_childeducationsupport = Number(this.additionalCover.selfbenefitamount_childeducationsupport) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.selfpremium_childeducationsupport = (this.additionalCover.selfbenefitamount_childeducationsupport * classvalues * 48) / 1000;
    }

  }
  if (member == "Spouse" && num == 9) {


    if (Number(this.additionalCover.spousebenefitamount_childeducationsupport) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_childeducationsupport = Number(this.additionalCover.spousebenefitamount_childeducationsupport) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.selfpremium_childeducationsupport = (this.additionalCover.selfbenefitamount_childeducationsupport * classvalues * 48) / 1000;
    }


  }


  if (member == "Child1" && num == 9) {


    if (Number(this.additionalCover.child1benefitamount_childeducationsupport) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_childeducationsupport = Number(this.additionalCover.child1benefitamount_childeducationsupport) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.child1premium_childeducationsupport = (this.additionalCover.child1benefitamount_childeducationsupport * classvalues * 48) / 1000;

    }

  }
  if (member == "Child2" && num == 9) {

    if (Number(this.additionalCover.child2benefitamount_childeducationsupport) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child2benefitamount_childeducationsupport = Number(this.additionalCover.child2benefitamount_childeducationsupport) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("ces", member);
      this.additionalCover.child2premium_childeducationsupport = (this.additionalCover.child2benefitamount_childeducationsupport * classvalues * 48) / 1000;

    }

  }


}
clickAddFamTransAllow(member, num) {

  if (member == "Self" && num == 10) {


    if (Number(this.additionalCover.selfbenefitamount_familytransportallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.selfbenefitamount_familytransportallowance = Number(this.additionalCover.selfbenefitamount_familytransportallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.selfpremium_familytransportallowance = (this.additionalCover.selfbenefitamount_familytransportallowance * classvalues) / 1000;
    }


  }
  if (member == "Spouse" && num == 10) {


    if (Number(this.additionalCover.spousebenefitamount_familytransportallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.spousebenefitamount_familytransportallowance = Number(this.additionalCover.spousebenefitamount_familytransportallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.spousepremium_familytransportallowance = (this.additionalCover.spousebenefitamount_familytransportallowance * classvalues) / 1000;
    }



  }


  if (member == "Child1" && num == 10) {


    if (Number(this.additionalCover.child1benefitamount_familytransportallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.child1benefitamount_familytransportallowance = Number(this.additionalCover.child1benefitamount_familytransportallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.child1premium_familytransportallowance = (this.additionalCover.child1benefitamount_familytransportallowance * classvalues) / 1000;
    }

  }
  if (member == "Child2" && num == 10) {

    if (Number(this.additionalCover.child2benefitamount_familytransportallowance) >= 50000) {
      this.showToast("You can not select above Rs 50000");
    } else {
      this.additionalCover.child2benefitamount_familytransportallowance = Number(this.additionalCover.child2benefitamount_familytransportallowance) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.child2premium_familytransportallowance = (this.additionalCover.child2benefitamount_familytransportallowance * classvalues) / 1000;
    }


  }
}



clickRemoveFamTransAllow(member, num) {
  if (member == "Self" && num == 10) {


    if (Number(this.additionalCover.selfbenefitamount_familytransportallowance) <= 0) {
      this.showToast("You can not select above Rs 0 ");
    } else {
      this.additionalCover.selfbenefitamount_familytransportallowance = Number(this.additionalCover.selfbenefitamount_familytransportallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.selfpremium_familytransportallowance = (this.additionalCover.selfbenefitamount_familytransportallowance * classvalues) / 1000;
    }

  }
  if (member == "Spouse" && num == 10) {


    if (Number(this.additionalCover.spousebenefitamount_familytransportallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_familytransportallowance = Number(this.additionalCover.spousebenefitamount_familytransportallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.spousepremium_familytransportallowance = (this.additionalCover.spousebenefitamount_familytransportallowance * classvalues) / 1000;
    }



  }


  if (member == "Child1" && num == 10) {


    if (Number(this.additionalCover.child1benefitamount_familytransportallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_familytransportallowance = Number(this.additionalCover.child1benefitamount_familytransportallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.child1premium_familytransportallowance = (this.additionalCover.child1benefitamount_familytransportallowance * classvalues) / 1000;
    }

  }
  if (member == "Child2" && num == 10) {

    if (Number(this.additionalCover.child2benefitamount_familytransportallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child2benefitamount_familytransportallowance = Number(this.additionalCover.child2benefitamount_familytransportallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("fta", member);
      this.additionalCover.child2premium_familytransportallowance = (this.additionalCover.child2benefitamount_familytransportallowance * classvalues) / 1000;
    }


  }
}

clickAddLifeSupBen(member, num) {

  if (member == "Self" && num == 12) {


    if (Number(this.additionalCover.selfbenefitamount_lifesupportbenefit) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.selfbenefitamount_lifesupportbenefit = Number(this.additionalCover.selfbenefitamount_lifesupportbenefit) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.selfpremium_lifesupportbenefit = (this.additionalCover.selfbenefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }


  }
  if (member == "Spouse" && num == 12) {


    if (Number(this.additionalCover.spousebenefitamount_lifesupportbenefit) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.spousebenefitamount_lifesupportbenefit = Number(this.additionalCover.spousebenefitamount_lifesupportbenefit) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.spousepremium_lifesupportbenefit = (this.additionalCover.spousebenefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }



  }


  if (member == "Child1" && num == 12) {


    if (Number(this.additionalCover.child1benefitamount_lifesupportbenefit) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.child1benefitamount_lifesupportbenefit = Number(this.additionalCover.child1benefitamount_lifesupportbenefit) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.child1premium_lifesupportbenefit = (this.additionalCover.child1benefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }

  }
  if (member == "Child2" && num == 12) {

    if (Number(this.additionalCover.child2benefitamount_lifesupportbenefit) >= 10000) {
      this.showToast("You can not select above Rs 10000");

    } else {
      this.additionalCover.child2benefitamount_lifesupportbenefit = Number(this.additionalCover.child2benefitamount_lifesupportbenefit) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.child2premium_lifesupportbenefit = (this.additionalCover.child2benefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }


  }
}

clickRemoveLifeSupBen(member, num) {

  if (member == "Self" && num == 12) {


    if (Number(this.additionalCover.selfbenefitamount_lifesupportbenefit) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_lifesupportbenefit = Number(this.additionalCover.selfbenefitamount_lifesupportbenefit) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.selfpremium_lifesupportbenefit = (this.additionalCover.selfbenefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }


  }
  if (member == "Spouse" && num == 12) {


    if (Number(this.additionalCover.spousebenefitamount_lifesupportbenefit) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_lifesupportbenefit = Number(this.additionalCover.spousebenefitamount_lifesupportbenefit) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.spousepremium_lifesupportbenefit = (this.additionalCover.spousebenefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }



  }


  if (member == "Child1" && num == 12) {


    if (Number(this.additionalCover.child1benefitamount_lifesupportbenefit) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_lifesupportbenefit = Number(this.additionalCover.child1benefitamount_lifesupportbenefit) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.child1premium_lifesupportbenefit = (this.additionalCover.child1benefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }

  }
  if (member == "Child2" && num == 12) {

    if (Number(this.additionalCover.child2benefitamount_lifesupportbenefit) <= 0) {
      this.showToast("You can not select above Rs 0");

    } else {
      this.additionalCover.child2benefitamount_lifesupportbenefit = Number(this.additionalCover.child2benefitamount_lifesupportbenefit) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.child2premium_lifesupportbenefit = (this.additionalCover.child2benefitamount_lifesupportbenefit * classvalues * 48) / 1000;
    }


  }
}
clickAddLoanProtector(member, num) {

  if (member == "Self" && num == 18) {

    if (this.category == 1) {

      if (Number(this.additionalCover.selfbenefitamount_loanprotector) >= 20000) {
        this.showToast("You can not select above Rs 20000");
      } else {
        this.additionalCover.selfbenefitamount_loanprotector = Number(this.additionalCover.selfbenefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.selfpremium_loanprotector = (this.additionalCover.selfbenefitamount_loanprotector * classvalues * 12) / 1000;
      }

    } if (this.category == 2) {

      if (Number(this.additionalCover.selfbenefitamount_loanprotector) >= 15000) {
        this.showToast("You can not select above Rs 15000");
      } else {
        this.additionalCover.selfbenefitamount_loanprotector = Number(this.additionalCover.selfbenefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.selfpremium_loanprotector = (this.additionalCover.selfbenefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }
    if (this.category == 3) {

      if (Number(this.additionalCover.selfbenefitamount_loanprotector) >= 7500) {
        this.showToast("You can not select above Rs 7500");
      } else {
        this.additionalCover.selfbenefitamount_loanprotector = Number(this.additionalCover.selfbenefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.selfpremium_loanprotector = (this.additionalCover.selfbenefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }


  }
  if (member == "Spouse" && num == 18) {


    if (this.category == 1) {

      if (Number(this.additionalCover.spousebenefitamount_loanprotector) >= 20000) {
        this.showToast("You can not select above Rs 20000");
      } else {
        this.additionalCover.spousebenefitamount_loanprotector = Number(this.additionalCover.spousebenefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.spousepremium_loanprotector = (this.additionalCover.spousebenefitamount_loanprotector * classvalues * 12) / 1000;
      }

    } if (this.category == 2) {

      if (Number(this.additionalCover.spousebenefitamount_loanprotector) >= 15000) {
        this.showToast("You can not select above Rs 15000");
      } else {
        this.additionalCover.spousebenefitamount_loanprotector = Number(this.additionalCover.spousebenefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.spousepremium_loanprotector = (this.additionalCover.spousebenefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }
    if (this.category == 3) {

      if (Number(this.additionalCover.spousebenefitamount_loanprotector) >= 7500) {
        this.showToast("You can not select above Rs 7500");
      } else {
        this.additionalCover.spousebenefitamount_loanprotector = Number(this.additionalCover.spousebenefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.spousepremium_loanprotector = (this.additionalCover.spousebenefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }


  }


  if (member == "Child1" && num == 18) {


    if (this.category == 1) {

      if (Number(this.additionalCover.child1benefitamount_loanprotector) >= 20000) {
        this.showToast("You can not select above Rs 20000");
      } else {
        this.additionalCover.child1benefitamount_loanprotector = Number(this.additionalCover.child1benefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.child1premium_loanprotector = (this.additionalCover.child1benefitamount_loanprotector * classvalues * 12) / 1000;
      }

    } if (this.category == 2) {

      if (Number(this.additionalCover.child1benefitamount_loanprotector) >= 15000) {
        this.showToast("You can not select above Rs 15000");
      } else {
        this.additionalCover.child1benefitamount_loanprotector = Number(this.additionalCover.child1benefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.child1premium_loanprotector = (this.additionalCover.child1benefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }
    if (this.category == 3) {

      if (Number(this.additionalCover.child1benefitamount_loanprotector) >= 7500) {
        this.showToast("You can not select above Rs 7500");
      } else {
        this.additionalCover.child1benefitamount_loanprotector = Number(this.additionalCover.child1benefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.child1premium_loanprotector = (this.additionalCover.child1benefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }

  }
  if (member == "Child2" && num == 18) {

    if (this.category == 1) {

      if (Number(this.additionalCover.child2benefitamount_loanprotector) >= 20000) {
        this.showToast("You can not select above Rs 20000");
      } else {
        this.additionalCover.child2benefitamount_loanprotector = Number(this.additionalCover.child2benefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.child2premium_loanprotector = (this.additionalCover.child2benefitamount_loanprotector * classvalues * 12) / 1000;
      }

    } if (this.category == 2) {

      if (Number(this.additionalCover.child2benefitamount_loanprotector) >= 15000) {
        this.showToast("You can not select above Rs 15000");
      } else {
        this.additionalCover.child2benefitamount_loanprotector = Number(this.additionalCover.child2benefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.child2premium_loanprotector = (this.additionalCover.child2benefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }
    if (this.category == 3) {

      if (Number(this.additionalCover.child2benefitamount_loanprotector) >= 7500) {
        this.showToast("You can not select above Rs 7500");
      } else {
        this.additionalCover.child2benefitamount_loanprotector = Number(this.additionalCover.child2benefitamount_loanprotector) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
        this.additionalCover.child2premium_loanprotector = (this.additionalCover.child2benefitamount_loanprotector * classvalues * 12) / 1000;
      }

    }
  }

}
clickRemoveLoanProtector(member, num) {

  if (member == "Self" && num == 18) {
    if (Number(this.additionalCover.selfbenefitamount_loanprotector) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_loanprotector = Number(this.additionalCover.selfbenefitamount_loanprotector) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
      this.additionalCover.selfpremium_loanprotector = (this.additionalCover.selfbenefitamount_loanprotector * classvalues * 12) / 1000;
    }

  }
  if (member == "Spouse" && num == 18) {
    if (Number(this.additionalCover.spousebenefitamount_loanprotector) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_loanprotector = Number(this.additionalCover.spousebenefitamount_loanprotector) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
      this.additionalCover.spousepremium_loanprotector = (this.additionalCover.spousebenefitamount_loanprotector * classvalues * 12) / 1000;
    }

  }

  if (member == "Child1" && num == 18) {
    if (Number(this.additionalCover.child1benefitamount_loanprotector) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_loanprotector = Number(this.additionalCover.child1benefitamount_loanprotector) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
      this.additionalCover.child1premium_loanprotector = (this.additionalCover.child1benefitamount_loanprotector * classvalues * 12) / 1000;
    }

  }
  if (member == "Child2" && num == 18) {
    if (Number(this.additionalCover.child2benefitamount_loanprotector) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child2benefitamount_loanprotector = Number(this.additionalCover.child2benefitamount_loanprotector) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
      this.additionalCover.child2premium_loanprotector = (this.additionalCover.child2benefitamount_loanprotector * classvalues * 12) / 1000;
    }

  }

}

clickAddHospCashAllow(member, num) {

  if (member == "Self" && num == 11) {
    if (this.category == "1") {
      if (Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) >= 2000) {
        this.showToast("You can not select above Rs 2000");
      } else {
        this.additionalCover.selfbenefitamount_hospitalcashallowance = Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.selfpremium_hospitalcashallowance = (this.additionalCover.selfbenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "2") {
      if (Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) >= 1500) {
        this.showToast("You can not select above Rs 1500");
      } else {
        this.additionalCover.selfbenefitamount_hospitalcashallowance = Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.selfpremium_hospitalcashallowance = (this.additionalCover.selfbenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "3") {

      if (Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) >= 1000) {
        this.showToast("You can not select above Rs 2000");
      } else {
        this.additionalCover.selfbenefitamount_hospitalcashallowance = Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.selfpremium_hospitalcashallowance = (this.additionalCover.selfbenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }
    }


  }
  if (member == "Spouse" && num == 11) {
    if (this.category == "1") {
      if (Number(this.additionalCover.spousebenefitamount_hospitalcashallowance) >= 2000) {
        this.showToast("You can not select above Rs 2000");
      } else {
        this.additionalCover.spousebenefitamount_hospitalcashallowance = Number(this.additionalCover.spousebenefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.spousepremium_hospitalcashallowance = (this.additionalCover.spousebenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "2") {
      if (Number(this.additionalCover.spousebenefitamount_hospitalcashallowance) >= 1500) {
        this.showToast("You can not select above Rs 1500");
      } else {
        this.additionalCover.spousebenefitamount_hospitalcashallowance = Number(this.additionalCover.spousebenefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.spousepremium_hospitalcashallowance = (this.additionalCover.spousebenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "3") {

      if (Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) >= 1000) {
        this.showToast("You can not select above Rs 1000");
      } else {
        this.additionalCover.spousebenefitamount_hospitalcashallowance = Number(this.additionalCover.spousebenefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.spousepremium_hospitalcashallowance = (this.additionalCover.spousebenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }
    }


  }
  if (member == "Child1" && num == 11) {
    if (this.category == "1") {
      if (Number(this.additionalCover.child1benefitamount_hospitalcashallowance) >= 2000) {
        this.showToast("You can not select above Rs 2000");
      } else {
        this.additionalCover.child1benefitamount_hospitalcashallowance = Number(this.additionalCover.child1benefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.child1premium_hospitalcashallowance = (this.additionalCover.child1benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "2") {
      if (Number(this.additionalCover.child1benefitamount_hospitalcashallowance) >= 1500) {
        this.showToast("You can not select above Rs 1500");
      } else {
        this.additionalCover.child1benefitamount_hospitalcashallowance = Number(this.additionalCover.child1benefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.child1premium_hospitalcashallowance = (this.additionalCover.child1benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "3") {

      if (Number(this.additionalCover.child1benefitamount_hospitalcashallowance) >= 1000) {
        this.showToast("You can not select above Rs 1000");
      } else {
        this.additionalCover.child1benefitamount_hospitalcashallowance = Number(this.additionalCover.child1benefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.child1premium_hospitalcashallowance = (this.additionalCover.child1benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }
    }


  }
  if (member == "Child2" && num == 11) {
    if (this.category == "1") {
      if (Number(this.additionalCover.child2benefitamount_hospitalcashallowance) >= 2000) {
        this.showToast("You can not select above Rs 2000");
      } else {
        this.additionalCover.child2benefitamount_hospitalcashallowance = Number(this.additionalCover.child2benefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.child2premium_hospitalcashallowance = (this.additionalCover.child2benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "2") {
      if (Number(this.additionalCover.child2benefitamount_hospitalcashallowance) >= 1500) {
        this.showToast("You can not select above Rs 1500");
      } else {
        this.additionalCover.child2benefitamount_hospitalcashallowance = Number(this.additionalCover.child2benefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.child2premium_hospitalcashallowance = (this.additionalCover.child2benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }

    }


    if (this.category == "3") {

      if (Number(this.additionalCover.child2benefitamount_hospitalcashallowance) >= 1000) {
        this.showToast("You can not select above Rs 1000");
      } else {
        this.additionalCover.child2benefitamount_hospitalcashallowance = Number(this.additionalCover.child2benefitamount_hospitalcashallowance) + 10000;
        var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
        this.additionalCover.child2premium_hospitalcashallowance = (this.additionalCover.child2benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
      }
    }


  }
}

clickRemoveHospCashAllow(member, num) {
  if (member == "Self" && num == 11) {
    if (Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_hospitalcashallowance = Number(this.additionalCover.selfbenefitamount_hospitalcashallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("hca", member);
      this.additionalCover.selfpremium_hospitalcashallowance = (this.additionalCover.selfbenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
    }
  }


  if (member == "Spouse" && num == 11) {
    if (Number(this.additionalCover.spousebenefitamount_hospitalcashallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_hospitalcashallowance = Number(this.additionalCover.spousebenefitamount_hospitalcashallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
      this.additionalCover.spousepremium_hospitalcashallowance = (this.additionalCover.spousebenefitamount_hospitalcashallowance * classvalues * 30) / 1000;
    }

  }

  if (member == "Child1" && num == 11) {
    if (Number(this.additionalCover.child1benefitamount_hospitalcashallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_hospitalcashallowance = Number(this.additionalCover.child1benefitamount_hospitalcashallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
      this.additionalCover.child1premium_hospitalcashallowance = (this.additionalCover.child1benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
    }

  }
  if (member == "Child2" && num == 11) {
    if (Number(this.additionalCover.child2benefitamount_hospitalcashallowance) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child2benefitamount_hospitalcashallowance = Number(this.additionalCover.child2benefitamount_hospitalcashallowance) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lp", member);
      this.additionalCover.child2premium_hospitalcashallowance = (this.additionalCover.child2benefitamount_hospitalcashallowance * classvalues * 30) / 1000;
    }

  }
}


clickRemoveBrokenBones(member, num) {
  if (member == "Self" && num == 13) {
    if (Number(this.additionalCover.selfbenefitamount_brokenbones) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.selfbenefitamount_brokenbones = Number(this.additionalCover.selfbenefitamount_brokenbones) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("bb", member);
      this.additionalCover.selfpremium_brokenbones = (this.additionalCover.selfbenefitamount_brokenbones * classvalues) / 1000;
    }
  }


  if (member == "Spouse" && num == 13) {
    if (Number(this.additionalCover.spousebenefitamount_brokenbones) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.spousebenefitamount_brokenbones = Number(this.additionalCover.spousebenefitamount_brokenbones) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("bb", member);
      this.additionalCover.spousepremium_brokenbones = (this.additionalCover.spousebenefitamount_brokenbones * classvalues) / 1000;
    }

  }

  if (member == "Child1" && num == 13) {
    if (Number(this.additionalCover.child1benefitamount_brokenbones) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child1benefitamount_brokenbones = Number(this.additionalCover.child1benefitamount_brokenbones) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("bb", member);
      this.additionalCover.child1premium_brokenbones = (this.additionalCover.child1benefitamount_brokenbones * classvalues) / 1000;
    }

  }
  if (member == "Child2" && num == 13) {
    if (Number(this.additionalCover.child2benefitamount_brokenbones) <= 0) {
      this.showToast("You can not select above Rs 0");
    } else {
      this.additionalCover.child2benefitamount_brokenbones = Number(this.additionalCover.child2benefitamount_brokenbones) - 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("bb", member);
      this.additionalCover.child2premium_brokenbones = (this.additionalCover.child2benefitamount_brokenbones * classvalues) / 1000;
    }

  }
}

clickAddBrokenBones(member, num) {
  if (member == "Self" && num == 12) {


    if (Number(this.additionalCover.selfbenefitamount_brokenbones) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.selfbenefitamount_brokenbones = Number(this.additionalCover.selfbenefitamount_brokenbones) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.selfpremium_brokenbones = (this.additionalCover.selfbenefitamount_brokenbones * classvalues * 48) / 1000;
    }


  }
  if (member == "Spouse" && num == 12) {


    if (Number(this.additionalCover.selfbenefitamount_brokenbones) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.spousebenefitamount_brokenbones = Number(this.additionalCover.spousebenefitamount_brokenbones) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("bb", member);
      this.additionalCover.spousepremium_brokenbones = (this.additionalCover.spousebenefitamount_brokenbones * classvalues) / 1000;
    }



  }


  if (member == "Child1" && num == 12) {


    if (Number(this.additionalCover.child1benefitamount_brokenbones) >= 10000) {
      this.showToast("You can not select above Rs 10000");
    } else {
      this.additionalCover.child1benefitamount_brokenbones = Number(this.additionalCover.child1benefitamount_brokenbones) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.child1premium_brokenbones = (this.additionalCover.child1benefitamount_brokenbones * classvalues * 48) / 1000;
    }

  }
  if (member == "Child2" && num == 12) {

    if (Number(this.additionalCover.child2benefitamount_brokenbones) >= 10000) {
      this.showToast("You can not select above Rs 10000");

    } else {
      this.additionalCover.child2benefitamount_brokenbones = Number(this.additionalCover.child2benefitamount_brokenbones) + 10000;
      var classvalues: any = this.getClassValuesForAdditionalCoverPremium("lsb", member);
      this.additionalCover.child2premium_brokenbones = (this.additionalCover.child2benefitamount_brokenbones * classvalues * 48) / 1000;
    }


  }
}
}

class InsuredDetails {

  acc_maxinsured: any = "";
  acc_suminsured: any = "";
  acc_premium: any = "";
  ppd_maxinsured: any = "";
  ppd_suminsured: any = "";
  ppd_premium: any = "";
  ptd_maxinsured: any = "";
  ptd_suminsured: any = "";
  ptd_premium: any = "";
  ttd_maxinsured: any = "";
  ttd_suminsured: any = "";
  ttd_premium: any = "";
  additionalCover: AdditionalCover;
  self_totalpremium: any = "";
}

class AdditionalCover {
  selfbenefitamount_adaptationallowance: any = "";
  selfpremium_adaptationallowance: any = "";
  selfbenefitamount_childeducationsupport: any = "";
  selfpremium_childeducationsupport: any = "";
  selfbenefitamount_familytransportallowance: any = "";
  selfpremium_familytransportallowance: any = "";
  selfbenefitamount_hospitalcashallowance: any = "";
  selfpremium_hospitalcashallowance: any = "";
  selfbenefitamount_loanprotector: any = "";
  selfpremium_loanprotector: any = "";
  selfbenefitamount_lifesupportbenefit: any = "";
  selfpremium_lifesupportbenefit: any = "";
  selfbenefitamount_acchospitalisation: any = "";
  selfpremium_acchospitalisation: any = "";
  selfbenefitamount_accmedicalexpenses: any = "";
  selfpremium_accmedicalexpenses: any = "";
  selfbenefitamount_repatriationfuneralexpenses: any = "";
  selfpremium_repatriationfuneralexpenses: any = "";
  selfbenefitamount_brokenbones: any = "";
  selfpremium_brokenbones: any = "";
  selfbenefitamount_roadambulancecover: any = "";
  selfpremium_roadambulancecover: any = "";
  selfbenefitamount_airambulancecover: any = "";
  selfpremium_airambulancecover: any = "";
  selfbenefitamount_adventuresportbenefit: any = "";
  selfpremium_adventuresportbenefit: any = "";
  selfbenefitamount_chauffeurplanbenefit: any = "";
  selfpremium_chauffeurplanbenefit: any = "";


  spousebenefitamount_adaptationallowance: any = "";
  spousepremium_adaptationallowance: any = "";
  spousebenefitamount_childeducationsupport: any = "";
  spousepremium_childeducationsupport: any = "";
  spousebenefitamount_familytransportallowance: any = "";
  spousepremium_familytransportallowance: any = "";
  spousebenefitamount_hospitalcashallowance: any = "";
  spousepremium_hospitalcashallowance: any = "";
  spousebenefitamount_loanprotector: any = "";
  spousepremium_loanprotector: any = "";
  spousebenefitamount_lifesupportbenefit: any = "";
  spousepremium_lifesupportbenefit: any = "";
  spousebenefitamount_acchospitalisation: any = "";
  spousepremium_acchospitalisation: any = "";
  spousebenefitamount_accmedicalexpenses: any = "";
  spousepremium_accmedicalexpenses: any = "";
  spousebenefitamount_repatriationfuneralexpenses: any = "";
  spousepremium_repatriationfuneralexpenses: any = "";
  spousebenefitamount_brokenbones: any = "";
  spousepremium_brokenbones: any = "";
  spousebenefitamount_roadambulancecover: any = "";
  spousepremium_roadambulancecover: any = "";
  spousebenefitamount_airambulancecover: any = "";
  spousepremium_airambulancecover: any = "";
  spousebenefitamount_adventuresportbenefit: any = "";
  spousepremium_adventuresportbenefit: any = "";
  spousebenefitamount_chauffeurplanbenefit: any = "";
  spousepremium_chauffeurplanbenefit: any = "";


  child1benefitamount_adaptationallowance: any = "";
  child1premium_adaptationallowance: any = "";
  child1benefitamount_childeducationsupport: any = "";
  child1premium_childeducationsupport: any = "";
  child1benefitamount_familytransportallowance: any = "";
  child1premium_familytransportallowance: any = "";
  child1benefitamount_hospitalcashallowance: any = "";
  child1premium_hospitalcashallowance: any = "";
  child1benefitamount_loanprotector: any = "";
  child1premium_loanprotector: any = "";
  child1benefitamount_lifesupportbenefit: any = "";
  child1premium_lifesupportbenefit: any = "";
  child1benefitamount_acchospitalisation: any = "";
  child1premium_acchospitalisation: any = "";
  child1benefitamount_accmedicalexpenses: any = "";
  child1premium_accmedicalexpenses: any = "";
  child1benefitamount_repatriationfuneralexpenses: any = "";
  child1premium_repatriationfuneralexpenses: any = "";
  child1benefitamount_brokenbones: any = "";
  child1premium_brokenbones: any = "";
  child1benefitamount_roadambulancecover: any = "";
  child1premium_roadambulancecover: any = "";
  child1benefitamount_airambulancecover: any = "";
  child1premium_airambulancecover: any = "";
  child1benefitamount_adventuresportbenefit: any = "";
  child1premium_adventuresportbenefit: any = "";
  child1benefitamount_chauffeurplanbenefit: any = "";
  child1premium_chauffeurplanbenefit: any = "";


  child2benefitamount_adaptationallowance: any = "";
  child2premium_adaptationallowance: any = "";
  child2benefitamount_childeducationsupport: any = "";
  child2premium_childeducationsupport: any = "";
  child2benefitamount_familytransportallowance: any = "";
  child2premium_familytransportallowance: any = "";
  child2benefitamount_hospitalcashallowance: any = "";
  child2premium_hospitalcashallowance: any = "";
  child2benefitamount_loanprotector: any = "";
  child2premium_loanprotector: any = "";
  child2benefitamount_lifesupportbenefit: any = "";
  child2premium_lifesupportbenefit: any = "";
  child2benefitamount_acchospitalisation: any = "";
  child2premium_acchospitalisation: any = "";
  child2benefitamount_accmedicalexpenses: any = "";
  child2premium_accmedicalexpenses: any = "";
  child2benefitamount_repatriationfuneralexpenses: any = "";
  child2premium_repatriationfuneralexpenses: any = "";
  child2benefitamount_brokenbones: any = "";
  child2premium_brokenbones: any = "";
  child2benefitamount_roadambulancecover: any = "";
  child2premium_roadambulancecover: any = "";
  child2benefitamount_airambulancecover: any = "";
  child2premium_airambulancecover: any = "";
  child2benefitamount_adventuresportbenefit: any = "";
  child2premium_adventuresportbenefit: any = "";
  child2benefitamount_chauffeurplanbenefit: any = "";
  child2premium_chauffeurplanbenefit: any = "";
}
