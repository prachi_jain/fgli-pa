import { Component, NgModule, Input, ViewChild } from "@angular/core";
import {
  IonicPage,
  Platform,
  NavController,
  NavParams,
  App,
  Nav,
  ToastController,
  LoadingController
} from "ionic-angular";
import { AppService } from "../../../providers/app-service/app-service";
import { LoginPage } from "../../health/login/login";
import { PremiumBuyPage } from "../premium-buy/premium-buy";
import { PaBuyProposerDetailsPage } from "../pa-buy-proposer-details/pa-buy-proposer-details";
import { LandingScreenPage } from "../../health/landing-screen/landing-screen";
import { NumericStepperComponent } from "../../../components/numeric-stepper/numeric-stepper";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
/**
/**
 * Generated class for the BuyPremiumlistviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-buy-premiumlistview",
  templateUrl: "buy-premiumlistview.html"
})
export class BuyPremiumlistviewPage {
  @ViewChild("input") myInput;
  @ViewChild("emailfocus") emailFocus;
  @ViewChild("mobilefocus") mobileFocus;

  premPet = "summary";
  pet = "OneTime";

  showTransBaground = true;

  oneTimeOneNew = 0;
  oneTimeTwoNew = 0;
  oneTimeThreeNew = 0;
  oneTimeOneOld;
  oneTimeTwoOld;
  oneTimeThreeOld;
  discountOneTimeOne;
  discountOneTimeTwo;
  discountOneTimeThree;
  totalCalculateAmount = 953.1;
  policyType;
  discountAmount;
  ActualAmountoneTimeTwoNew;
  ActualAmountoneTimeThreeNew;

  monthlyTwoNew;
  monthlyTwoOld;
  discountmonthlyTwo;
  monthlyThreeNew;
  monthlyThreeOld;
  discountmonthlyThree;

  quarterlyTwoNew;
  quarterlyTwoOld;
  discountquarterlyTwo;
  quarterlyThreeNew;
  quarterlyThreeOld;
  discountquarterlyThree;

  halfyearlyTwoNew;
  halfyearlyTwoOld;
  discounthalfyearlyTwo;
  halfyearlyThreeNew;
  halfyearlyThreeOld;
  discounthalfyearlyThree;

  ReferenceNo = "";
  NameOfEmailsmsProposer;
  email;
  mobile;
  successLabel = true;
  smsTo;
  emailTo;
  sendSMSList;
  sendEmailList;
  canShowToast = true;
  loading;
  ClientDetails = [];
  voluntaryPopup = true;
  thankyouPopup = true;
  ENQ_PolicyResponse;
  response;
  EMIDetails;
  totalPremium = 0;
  familyDiscount = 10;
  discountPremium = 0;
  PremiumWithGST = 0;
  memberCount = "2";
  NetPremium = 0;
  totalMainCoverPremium = 0;
  accidentalDeath = 0;
  accidentalDeathPremium = 0;
  permanetpartialdisability = 0;
  permanetpartialdisabilityPremium = 0;
  permanettotaldisability = 0;
  permanettotaldisabilityPremium = 0;
  temporarytotaldisability = 0;
  temporarytotaldisabilityPremium = 0;
  repatriationfuneralexpenses = 0;
  totalAdditionalCoverPremium = 0;
  NetTotalCovers = 0;
  showPopup = true;
  buyPopup = true;
  PremiumBreakup;
  membersInsured;
  selfdetails;
  spousedetails;
  sondetails;
  daughterdetails;

  insureddetails = new InsuredDetails();
  additionalCover = new AdditionalCover();

  spouseinsureddetails = new InsuredDetails();
  spouseadditionaldetails = new AdditionalCover();

  child1insureddetails = new InsuredDetails();
  child1additionaldetails = new AdditionalCover();

  child2insureddetails = new InsuredDetails();
  child2additionaldetails = new AdditionalCover();

  selfCoversArray;
  selfPremiumArray;
  spouseCoversArray;
  spousePremiumArray;

  child1CoversArray;
  child2CoversArray

  sonCoversArray;
  sonPremiumArray;
  daughterCoversArray;
  daughterPremiumArray;


  selfCoversArrayRecieved;
  spouseCoversArrayRecieved;
  child1CoversArrayRecieved;
  child2CoversArrayRecieved;

  familytab = "Self";
  summaryArray;
  selfTotalPremium;
  spouseTotalPremium;
  familyDiscountPercentage;
  totalFinalPremium;
  year = 1;
  PolicyTerm = "OneTime";
  employeeDiscount;
  longTermPercentageDiscount;
  premiumwithoutGST;
  installmentLoadingPercentage;
  totalInstallment;
  premiumWithLoading;
  premiumEmiAmount = 0;
  GstAmount = 0;
  totalPremiumCalculated = 0;
  longTermDiscount;
  QuotationID;
  UID;
  spouseGender = "F";
  beneficiaryDetailsMember1 = [];
  beneficiaryDetailsMember = [];
  pID;
  insuranceDetailsToSend;
  memberlist = [];
  PremiumAmount  = 0
  sonTotalPremium = 0
  daughterTotalPremium = 0
 







  selfpartialDis = true;
  selftotalDis = true;
  selftempDis = true;
  adaption_allowance = true;
  acc_hospitalisation = true;
  acc_medical_expenses = true;
  self_child_edu_support = true;
  self_family_trans_allowance = true;
  self_hospital_cash_allownce = true;
  self_life_support_benefit = true;
  self_broken_bones = true;
  self_road_ambulance_cover = true;
  self_air_ambulance_cover = true;
  self_adventure_sports_benefit = true;
  self_chauffeur_plan_benefit = true;
  self_loan_protector = true;

  


selfMainCoversPremium;
spouseMainCoversPremium;
child1MainCoversPremium;
child2MainCoversPremium

selfAddCoversPremium;
spouseAddCoversPremium;
child1AddCoversPremium;
child2AddCoversPremium


isFamilyDiscount = true
isEmployeeDiscount = true
employeeDiscountPercentage = 0


selfPremiumHide = false
spousePremiumHide = false
child1PremiumHide = false
child2PremiumHide = false

goToNext;
  constructor(
    private platform: Platform,
    public navCtrl: NavController,
    public app: App,
    public loadingCtrl: LoadingController,
    public toast: ToastController,
    public navParams: NavParams,
    public appService: AppService
  ) {
    this.totalCalculateAmount = navParams.get("totalPremium");
    console.log("total" + this.totalCalculateAmount);
    this.policyType = navParams.get("PolicyType");
    
    this.ENQ_PolicyResponse = navParams.get("ENQ_PolicyResponse");
    this.response = this.ENQ_PolicyResponse.PurchaseResponse[0].Result_pa;
    this.EMIDetails = this.response.Root.Policy.NewDataSet.EMIDetails;
    this.PremiumBreakup = navParams.get("PremiumBreakup");
    this.membersInsured = navParams.get("membersInsured");

    this.selfCoversArrayRecieved = navParams.get("selfCoversArray")
    this.spouseCoversArrayRecieved = navParams.get("spouseCoversArray")
    this.child1CoversArrayRecieved = navParams.get("child1CoversArray")
    this.child2CoversArrayRecieved  = navParams.get("child2CoversArray")


  
    this.memberCount = this.membersInsured .length
    
    if(this.memberCount=="1"){

      this.memberCount = this.memberCount + " " +"MEMBER"
    }
  else{
  this.memberCount = this.memberCount + " " +"MEMBERS"
  }



    this.selfMainCoversPremium =  navParams.get("selfMainCoversPremium")
    this.spouseMainCoversPremium =  navParams.get("spouseMainCoversPremium")
    this.child1MainCoversPremium =  navParams.get("child1MainCoversPremium")
    this.child2MainCoversPremium =  navParams.get("child2MainCoversPremium")

    this.selfAddCoversPremium =  navParams.get("selfAddCoversPremium")
    this.spouseAddCoversPremium =  navParams.get("spouseAddCoversPremium")
    this.child1AddCoversPremium =  navParams.get("child1AddCoversPremium")
    this.child2AddCoversPremium =  navParams.get("child2AddCoversPremium")


    

    this.QuotationID = this.ENQ_PolicyResponse.QuotationID;
    this.UID = this.ENQ_PolicyResponse.PurchaseResponse[0].UID;
    this.beneficiaryDetailsMember = navParams.get("ben");
    this.beneficiaryDetailsMember = JSON.parse(sessionStorage.ben);
    console.log("load" + JSON.stringify(this.beneficiaryDetailsMember));
    
    this.ReferenceNo = navParams.get("ReferenceNo");

    
  }

  ionViewDidLoad() {
    for (let i = 0; i < this.membersInsured.length; i++) {
      if (this.membersInsured[i].detailsSaved == true) {
        let member = this.membersInsured[i];

        this.memberlist.push({name: this.membersInsured[i].title});

       
        if (member.title == "Self") {


          for (let i = 0; i < this.selfCoversArrayRecieved.length; i++){

            if(this.selfCoversArrayRecieved[i].selfpartialDis == true){

              this.selfpartialDis = false
            }
            else if(this.selfCoversArrayRecieved[i].selftotalDis == true){
              this.selftotalDis = false
            }
            else if(this.selfCoversArrayRecieved[i].selftempDis == true){
              this.selftempDis = false
            }

            else if(this.selfCoversArrayRecieved[i].adaption_allowance == true){
              this.adaption_allowance = false
            }
            else if(this.selfCoversArrayRecieved[i].acc_hospitalisation == true){
              this.acc_hospitalisation = false
            }
            else if(this.selfCoversArrayRecieved[i].acc_medical_expenses == true){
              this.acc_medical_expenses = false
            }

            else if(this.selfCoversArrayRecieved[i].self_child_edu_support == true){
              this.self_child_edu_support = false
            }
            else if(this.selfCoversArrayRecieved[i].self_family_trans_allowance == true){
              this.self_family_trans_allowance = false
            }
            else if(this.selfCoversArrayRecieved[i].self_hospital_cash_allownce == true){
              this.self_hospital_cash_allownce = false
            }
            else if(this.selfCoversArrayRecieved[i].self_life_support_benefit == true){
              this.self_life_support_benefit = false
            }


            else if(this.selfCoversArrayRecieved[i].self_broken_bones == true){
              this.self_broken_bones = false
            }

            else if(this.selfCoversArrayRecieved[i].self_road_ambulance_cover == true){
              this.self_road_ambulance_cover = false
            }



            else if(this.selfCoversArrayRecieved[i].self_air_ambulance_cover == true){
              this.self_air_ambulance_cover = false
            }


            else if(this.selfCoversArrayRecieved[i].self_adventure_sports_benefit == true){
              this.self_adventure_sports_benefit = false
            }



            else if(this.selfCoversArrayRecieved[i].self_chauffeur_plan_benefit == true){
              this.self_chauffeur_plan_benefit = false
            }


            else if(this.selfCoversArrayRecieved[i].self_loan_protector == true){
              this.self_loan_protector = false
            }
          }

            this.calculateSelfCovers(member.code);


        }
      }
    }

    if(this.membersInsured.length == 1 && this.membersInsured[0].title == "Self"){

      this.isFamilyDiscount = true
    }
    else{
      this.isFamilyDiscount = false

    }

    this.getOneTimeCaculation("OneTime");
  }



  ionViewDidEnter(){
    if(sessionStorage.isViewPopped == "true"){
      this.membersInsured = JSON.parse(sessionStorage.ProposerDetails);
      this.insuranceDetailsToSend = JSON.parse(sessionStorage.planCardDetails);    
      console.log("cardData: " + JSON.stringify(this.insuranceDetailsToSend));
      this.buyPopup = true;
      //this.serviceResponsePopup = true;
      this.NameOfEmailsmsProposer = sessionStorage.propName;
      this.email = sessionStorage.email;
      this.mobile = sessionStorage.mobile;         
      //sessionStorage.isViewPopped = "";
      this.successLabel = true;
    }
    //this.successLabel = true;
  }




  calculateSelfCovers(member) {
    this.selfdetails = this.PremiumBreakup.filter(function(self) {
      return self.Relationship == member;
    });

    for (let i = 0; i < this.selfdetails.length; i++) {
      if (this.selfdetails[i].CoverCode == "AD") {
        this.insureddetails.acc_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.acc_premium = this.selfdetails[i].Premium;


      } else if (this.selfdetails[i].CoverCode == "PP") {
        this.insureddetails.ppd_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.ppd_premium = this.selfdetails[i].Premium;
      } else if (this.selfdetails[i].CoverCode == "PT") {
        this.insureddetails.ptd_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.ptd_premium = this.selfdetails[i].Premium;
      } else if (this.selfdetails[i].CoverCode == "TT") {
        this.insureddetails.ttd_maxinsured = this.selfdetails[i].SumInsured;
        this.insureddetails.ttd_premium = this.selfdetails[i].Premium;
      } else if (this.selfdetails[i].CoverCode == "AA") {
        this.additionalCover.selfbenefitamount_adaptationallowance = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_adaptationallowance = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "CS") {
        this.additionalCover.selfbenefitamount_childeducationsupport = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_childeducationsupport = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "FT") {
        this.additionalCover.selfbenefitamount_familytransportallowance = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_familytransportallowance = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "HC") {
        this.additionalCover.selfbenefitamount_hospitalcashallowance = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_hospitalcashallowance = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "LP") {
        this.additionalCover.selfbenefitamount_loanprotector = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_loanprotector = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "LS") {
        this.additionalCover.selfbenefitamount_lifesupportbenefit = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_lifesupportbenefit = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "ME") {
        this.additionalCover.selfbenefitamount_acchospitalisation = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_acchospitalisation = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "AM") {
        // this.additionalCover.selfbenefitamount_accmedicalexpenses = this.selfdetails[
        //   i
        // ].SumInsured;

        this.additionalCover.selfbenefitamount_accmedicalexpenses = "Opted"
        this.additionalCover.selfpremium_accmedicalexpenses = this.selfdetails[
          i
        ].Premium;
      } else if (this.selfdetails[i].CoverCode == "RF") {
        this.additionalCover.selfbenefitamount_repatriationfuneralexpenses = this.selfdetails[
          i
        ].SumInsured;
      } else if (this.selfdetails[i].CoverCode == "BB") {
        this.additionalCover.selfbenefitamount_brokenbones = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_brokenbones = this.selfdetails[
          i
        ].Premium;
      }
      else if (this.selfdetails[i].CoverCode == "AS") {
        this.additionalCover.selfbenefitamount_adventuresportbenefit = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_adventuresportbenefit = this.selfdetails[
          i
        ].Premium;
      }
      else if (this.selfdetails[i].CoverCode == "CP") {
        this.additionalCover.selfbenefitamount_chauffeurplanbenefit = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_chauffeurplanbenefit = this.selfdetails[
          i
        ].Premium;
      }
      else if (this.selfdetails[i].CoverCode == "RBC") {
        this.additionalCover.selfbenefitamount_roadambulancecover = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_roadambulancecover = this.selfdetails[
          i
        ].Premium;
      }


      else if (this.selfdetails[i].CoverCode == "AAC") {
        this.additionalCover.selfbenefitamount_airambulancecover = this.selfdetails[
          i
        ].SumInsured;
        this.additionalCover.selfpremium_airambulancecover = this.selfdetails[
          i
        ].Premium;
      }

    }

    // this.selfTotalPremium =
    //   Number(this.insureddetails.acc_premium) +
    //   Number(this.insureddetails.ppd_premium) +
    //   Number(this.insureddetails.ptd_premium) +
    //   Number(this.insureddetails.ttd_premium) +
    //   Number(this.additionalCover.selfpremium_adaptationallowance) +
    //   Number(this.additionalCover.selfpremium_childeducationsupport) +
    //   Number(this.additionalCover.selfpremium_familytransportallowance) +
    //   Number(this.additionalCover.selfpremium_hospitalcashallowance) +
    //   Number(this.additionalCover.selfpremium_loanprotector) +
    //   Number(this.additionalCover.selfpremium_lifesupportbenefit) +
    //   Number(this.additionalCover.selfpremium_acchospitalisation) +
    //   Number(this.additionalCover.selfpremium_accmedicalexpenses) +
    //   Number(this.additionalCover.selfpremium_brokenbones)+
    //   Number(this.additionalCover.selfpremium_adventuresportbenefit)+
    //   Number(this.additionalCover.selfpremium_chauffeurplanbenefit)+
    //   Number(this.additionalCover.selfpremium_roadambulancecover)+
    //   Number(this.additionalCover.selfpremium_airambulancecover);

    this.clickTab("Self",0);
  }


  calculateSpouseCovers(member) {
    this.spousedetails = this.PremiumBreakup.filter(function(self) {
      return self.Relationship == member;
    });

    for (let i = 0; i < this.spousedetails.length; i++) {
      if (this.spousedetails[i].CoverCode == "AD") {
        this.spouseinsureddetails.acc_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.acc_premium = this.spousedetails[i].Premium;
      } else if (this.spousedetails[i].CoverCode == "PP") {
        this.spouseinsureddetails.ppd_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.ppd_premium = this.spousedetails[i].Premium;
      } else if (this.spousedetails[i].CoverCode == "PT") {
        this.spouseinsureddetails.ptd_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.ptd_premium = this.spousedetails[i].Premium;
      } else if (this.spousedetails[i].CoverCode == "TT") {
        this.spouseinsureddetails.ttd_maxinsured = this.spousedetails[
          i
        ].SumInsured;
        this.spouseinsureddetails.ttd_premium = this.spousedetails[i].Premium;
      } else if (this.spousedetails[i].CoverCode == "AA") {
        this.additionalCover.spousebenefitamount_adaptationallowance = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_adaptationallowance = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "CS") {
        this.additionalCover.spousebenefitamount_childeducationsupport = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_childeducationsupport = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "FT") {
        this.additionalCover.spousebenefitamount_familytransportallowance = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_familytransportallowance = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "HC") {
        this.additionalCover.spousebenefitamount_hospitalcashallowance = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_hospitalcashallowance = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "LP") {
        this.additionalCover.spousebenefitamount_loanprotector = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_loanprotector = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "LS") {
        this.additionalCover.spousebenefitamount_lifesupportbenefit = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_lifesupportbenefit = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "ME") {
        this.additionalCover.spousebenefitamount_acchospitalisation = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_acchospitalisation = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "AM") {
        // this.additionalCover.spousebenefitamount_accmedicalexpenses = this.spousedetails[
        //   i
        // ].SumInsured;

        this.additionalCover.spousebenefitamount_accmedicalexpenses = "Opted"
        this.additionalCover.spousepremium_accmedicalexpenses = this.spousedetails[
          i
        ].Premium;
      } else if (this.spousedetails[i].CoverCode == "RF") {
        this.additionalCover.spousebenefitamount_repatriationfuneralexpenses = this.spousedetails[
          i
        ].SumInsured;
      } else if (this.spousedetails[i].CoverCode == "BB") {
        this.additionalCover.spousebenefitamount_brokenbones = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_brokenbones = this.spousedetails[
          i
        ].Premium;
      }
      else if (this.spousedetails[i].CoverCode == "AS") {
        this.additionalCover.spousebenefitamount_adventuresportbenefit = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_adventuresportbenefit = this.spousedetails[
          i
        ].Premium;
      }
      else if (this.spousedetails[i].CoverCode == "CP") {
        this.additionalCover.spousebenefitamount_chauffeurplanbenefit = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_chauffeurplanbenefit = this.spousedetails[
          i
        ].Premium;
      }
      else if (this.spousedetails[i].CoverCode == "RBC") {
        this.additionalCover.spousebenefitamount_roadambulancecover = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_roadambulancecover = this.spousedetails[
          i
        ].Premium;
      }


      else if (this.spousedetails[i].CoverCode == "AAC") {
        this.additionalCover.spousebenefitamount_airambulancecover = this.spousedetails[
          i
        ].SumInsured;
        this.additionalCover.spousepremium_airambulancecover = this.spousedetails[
          i
        ].Premium;
      }
    }

    // this.spouseTotalPremium =
    //   Number(this.spouseinsureddetails.acc_premium) +
    //   Number(this.spouseinsureddetails.ppd_premium) +
    //   Number(this.spouseinsureddetails.ptd_premium) +
    //   Number(this.spouseinsureddetails.ttd_premium) +
    //   Number(this.additionalCover.spousepremium_adaptationallowance) +
    //   Number(this.additionalCover.spousepremium_childeducationsupport) +
    //   Number(this.additionalCover.spousepremium_familytransportallowance) +
    //   Number(this.additionalCover.spousepremium_hospitalcashallowance) +
    //   Number(this.additionalCover.spousepremium_loanprotector) +
    //   Number(this.additionalCover.spousepremium_lifesupportbenefit) +
    //   Number(this.additionalCover.spousepremium_acchospitalisation) +
    //   Number(this.additionalCover.spousepremium_accmedicalexpenses) +
    //   Number(this.additionalCover.spousepremium_brokenbones)+
    //   Number(this.additionalCover.spousepremium_adventuresportbenefit)+
    //   Number(this.additionalCover.spousepremium_chauffeurplanbenefit)+
    //   Number(this.additionalCover.spousepremium_roadambulancecover)+
    //   Number(this.additionalCover.spousepremium_airambulancecover);
  }






  calculateSonCovers(member) {
    this.sondetails = this.PremiumBreakup.filter(function(son) {
      return son.Relationship == member;
    });

    for (let i = 0; i < this.sondetails.length; i++) {
      if (this.sondetails[i].CoverCode == "AD") {
        this.child1insureddetails.acc_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.acc_premium = this.sondetails[i].Premium;
      } else if (this.sondetails[i].CoverCode == "PP") {
        this.child1insureddetails.ppd_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.ppd_premium = this.sondetails[i].Premium;
      } else if (this.sondetails[i].CoverCode == "PT") {
        this.child1insureddetails.ptd_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.ptd_premium = this.sondetails[i].Premium;
      } else if (this.sondetails[i].CoverCode == "TT") {
        this.child1insureddetails.ttd_maxinsured = this.sondetails[
          i
        ].SumInsured;
        this.child1insureddetails.ttd_premium = this.sondetails[i].Premium;
      } else if (this.sondetails[i].CoverCode == "AA") {
        this.additionalCover.child1benefitamount_adaptationallowance = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_adaptationallowance = this.sondetails[
          i
        ].Premium;
      } 
      // else if (this.sondetails[i].CoverCode == "CS") {
      //   this.additionalCover.child1benefitamount_childeducationsupport = this.sondetails[
      //     i
      //   ].SumInsured;
      //   this.additionalCover.child1premium_childeducationsupport = this.sondetails[
      //     i
      //   ].Premium;
      // } 
      
      else if (this.sondetails[i].CoverCode == "FT") {
        this.additionalCover.child1benefitamount_familytransportallowance = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_familytransportallowance = this.sondetails[
          i
        ].Premium;
      } else if (this.sondetails[i].CoverCode == "HC") {
        this.additionalCover.child1benefitamount_hospitalcashallowance = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_hospitalcashallowance = this.sondetails[
          i
        ].Premium;
      } 
      // else if (this.sondetails[i].CoverCode == "LP") {
      //   this.additionalCover.child1benefitamount_loanprotector = this.sondetails[
      //     i
      //   ].SumInsured;
      //   this.additionalCover.child1premium_loanprotector = this.sondetails[
      //     i
      //   ].Premium;
      // } 
      else if (this.sondetails[i].CoverCode == "LS") {
        this.additionalCover.child1benefitamount_lifesupportbenefit = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_lifesupportbenefit = this.sondetails[
          i
        ].Premium;
      } else if (this.sondetails[i].CoverCode == "ME") {
        this.additionalCover.child1benefitamount_acchospitalisation = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_acchospitalisation = this.sondetails[
          i
        ].Premium;
      } else if (this.sondetails[i].CoverCode == "AM") {
        // this.additionalCover.child1benefitamount_accmedicalexpenses = this.sondetails[
        //   i
        // ].SumInsured;

        this.additionalCover.child1benefitamount_accmedicalexpenses = "Opted"
        this.additionalCover.child1premium_accmedicalexpenses = this.sondetails[
          i
        ].Premium;
      } else if (this.sondetails[i].CoverCode == "RF") {
        this.additionalCover.child1benefitamount_repatriationfuneralexpenses = this.sondetails[
          i
        ].SumInsured;
      } else if (this.sondetails[i].CoverCode == "BB") {
        this.additionalCover.child1benefitamount_brokenbones = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_brokenbones = this.sondetails[
          i
        ].Premium;
      }
      else if (this.sondetails[i].CoverCode == "AS") {
        this.additionalCover.child1benefitamount_adventuresportbenefit = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_adventuresportbenefit = this.sondetails[
          i
        ].Premium;
      }
      else if (this.sondetails[i].CoverCode == "CP") {
        this.additionalCover.child1benefitamount_chauffeurplanbenefit = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_chauffeurplanbenefit = this.sondetails[
          i
        ].Premium;
      }
      else if (this.sondetails[i].CoverCode == "RBC") {
        this.additionalCover.child1benefitamount_roadambulancecover = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_roadambulancecover = this.sondetails[
          i
        ].Premium;
      }
      else if (this.sondetails[i].CoverCode == "AAC") {
        this.additionalCover.child1benefitamount_airambulancecover = this.sondetails[
          i
        ].SumInsured;
        this.additionalCover.child1premium_airambulancecover = this.sondetails[
          i
        ].Premium;
      }
    }


    // this.sonCoversArray = {
    //   AD: this.child1insureddetails.acc_maxinsured,
    //   PP: this.child1insureddetails.ppd_maxinsured,
    //   PT: this.child1insureddetails.ptd_maxinsured,
    //   TT: this.child1insureddetails.ttd_maxinsured,
    //   RF: this.additionalCover.child1benefitamount_repatriationfuneralexpenses,
    //   HC: this.additionalCover.child1benefitamount_hospitalcashallowance,
    //  // CS: this.additionalCover.child1benefitamount_childeducationsupport,
    //   AA: this.additionalCover.child1benefitamount_adaptationallowance,
    //   FT: this.additionalCover.child1benefitamount_familytransportallowance,
    // //  LP: this.additionalCover.child1benefitamount_loanprotector,
    //   ME: this.additionalCover.child1benefitamount_acchospitalisation,
    //   AM: this.additionalCover.child1benefitamount_accmedicalexpenses
    // };

    // this.sonPremiumArray = {
    //   ADPremium: this.child1insureddetails.acc_premium,
    //   PPPremium: this.child1insureddetails.ppd_premium,
    //   PTPremium: this.child1insureddetails.ptd_maxinsured,
    //   TTPremium: this.child1insureddetails.ttd_premium,
    //   RFPremium: "0",
    //   HCPremium: this.additionalCover.child1premium_hospitalcashallowance,
    //   //CSPremium: this.additionalCover.child1premium_childeducationsupport,
    //   AAPremium: this.additionalCover.child1premium_adaptationallowance,
    //   FTPremium: this.additionalCover.child1premium_familytransportallowance,
    //   //LPPremium: this.additionalCover.child1premium_loanprotector,
    //   LSPremium: this.additionalCover.child1premium_lifesupportbenefit,
    //   MEPremium: this.additionalCover.child1premium_acchospitalisation,
    //   AMPremium: this.additionalCover.child1premium_accmedicalexpenses,
      
    // };


  //   this.sonTotalPremium =
  //   Number(this.child1insureddetails.acc_premium) +
  //   Number(this.child1insureddetails.ppd_premium) +
  //   Number(this.child1insureddetails.ptd_premium) +
  //   Number(this.child1insureddetails.ttd_premium) +
  //   Number(this.additionalCover.child1premium_adaptationallowance) +
  //  // Number(this.additionalCover.child1premium_childeducationsupport) +
  //   Number(this.additionalCover.child1premium_familytransportallowance) +
  //   Number(this.additionalCover.child1premium_hospitalcashallowance) +
  // //  Number(this.additionalCover.child1premium_loanprotector) +
  //   Number(this.additionalCover.child1premium_lifesupportbenefit) +
  //   Number(this.additionalCover.child1premium_acchospitalisation) +
  //   Number(this.additionalCover.child1premium_accmedicalexpenses) +
  //   Number(this.additionalCover.child1premium_brokenbones)+
  //   Number(this.additionalCover.spousepremium_adventuresportbenefit)+
  //   Number(this.additionalCover.spousepremium_chauffeurplanbenefit)+
  //   Number(this.additionalCover.spousepremium_roadambulancecover)+
  //   Number(this.additionalCover.spousepremium_airambulancecover);



   
  }

  calculateDaughterCovers(member) {
    this.daughterdetails = this.PremiumBreakup.filter(function(daughter) {
      return daughter.Relationship == member;
    });

    for (let i = 0; i < this.daughterdetails.length; i++) {
      if (this.daughterdetails[i].CoverCode == "AD") {
        this.child2insureddetails.acc_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.acc_premium = this.daughterdetails[i].Premium;
      } else if (this.daughterdetails[i].CoverCode == "PP") {
        this.child2insureddetails.ppd_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.ppd_premium = this.daughterdetails[i].Premium;
      } else if (this.daughterdetails[i].CoverCode == "PT") {
        this.child2insureddetails.ptd_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.ptd_premium = this.daughterdetails[i].Premium;
      } else if (this.daughterdetails[i].CoverCode == "TT") {
        this.child2insureddetails.ttd_maxinsured = this.daughterdetails[
          i
        ].SumInsured;
        this.child2insureddetails.ttd_premium = this.daughterdetails[i].Premium;
      } else if (this.daughterdetails[i].CoverCode == "AA") {
        this.additionalCover.child2benefitamount_adaptationallowance = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_adaptationallowance = this.daughterdetails[
          i
        ].Premium;
      } 
      // else if (this.daughterdetails[i].CoverCode == "CS") {
      //   this.additionalCover.child2benefitamount_childeducationsupport = this.daughterdetails[
      //     i
      //   ].SumInsured;
      //   this.additionalCover.child2premium_childeducationsupport = this.daughterdetails[
      //     i
      //   ].Premium;
      // } 
      else if (this.daughterdetails[i].CoverCode == "FT") {
        this.additionalCover.child2benefitamount_familytransportallowance = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_familytransportallowance = this.daughterdetails[
          i
        ].Premium;
      } else if (this.daughterdetails[i].CoverCode == "HC") {
        this.additionalCover.child2benefitamount_hospitalcashallowance = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_hospitalcashallowance = this.daughterdetails[
          i
        ].Premium;
      } 
      // else if (this.daughterdetails[i].CoverCode == "LP") {
      //   this.additionalCover.child2benefitamount_loanprotector = this.daughterdetails[
      //     i
      //   ].SumInsured;
      //   this.additionalCover.child2premium_loanprotector = this.daughterdetails[
      //     i
      //   ].Premium;
      // } 
      else if (this.daughterdetails[i].CoverCode == "LS") {
        this.additionalCover.child2benefitamount_lifesupportbenefit = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_lifesupportbenefit = this.daughterdetails[
          i
        ].Premium;
      } else if (this.daughterdetails[i].CoverCode == "ME") {
        this.additionalCover.child2benefitamount_acchospitalisation = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_acchospitalisation = this.daughterdetails[
          i
        ].Premium;
      } else if (this.daughterdetails[i].CoverCode == "AM") {
        // this.additionalCover.child2benefitamount_accmedicalexpenses = this.daughterdetails[
        //   i
        // ].SumInsured;

        this.additionalCover.child2benefitamount_accmedicalexpenses = "Opted"
        this.additionalCover.child2premium_accmedicalexpenses = this.daughterdetails[
          i
        ].Premium;
      } else if (this.daughterdetails[i].CoverCode == "RF") {
        this.additionalCover.child2benefitamount_repatriationfuneralexpenses = this.daughterdetails[
          i
        ].SumInsured;
      } else if (this.daughterdetails[i].CoverCode == "BB") {
        this.additionalCover.child2benefitamount_brokenbones = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_brokenbones = this.daughterdetails[
          i
        ].Premium;
      }
      else if (this.daughterdetails[i].CoverCode == "AS") {
        this.additionalCover.child2benefitamount_adventuresportbenefit = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_adventuresportbenefit = this.daughterdetails[
          i
        ].Premium;
      }
      else if (this.daughterdetails[i].CoverCode == "CP") {
        this.additionalCover.child2benefitamount_chauffeurplanbenefit = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_chauffeurplanbenefit = this.daughterdetails[
          i
        ].Premium;
      }
      else if (this.daughterdetails[i].CoverCode == "RBC") {
        this.additionalCover.child2benefitamount_roadambulancecover = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_roadambulancecover = this.daughterdetails[
          i
        ].Premium;
      }
      else if (this.daughterdetails[i].CoverCode == "AAC") {
        this.additionalCover.child2benefitamount_airambulancecover = this.daughterdetails[
          i
        ].SumInsured;
        this.additionalCover.child2premium_airambulancecover = this.daughterdetails[
          i
        ].Premium;
      }




    }

    this.daughterCoversArray = {
      AD: this.child2insureddetails.acc_maxinsured,
      PP: this.child2insureddetails.ppd_maxinsured,
      PT: this.child2insureddetails.ptd_maxinsured,
      TT: this.child2insureddetails.ttd_maxinsured,
      RF: this.additionalCover.child2benefitamount_repatriationfuneralexpenses,
      HC: this.additionalCover.child2benefitamount_hospitalcashallowance,
     // CS: this.additionalCover.child2benefitamount_childeducationsupport,
      AA: this.additionalCover.child2benefitamount_adaptationallowance,
      FT: this.additionalCover.child2benefitamount_familytransportallowance,
    //  LP: this.additionalCover.child2benefitamount_loanprotector,
      ME: this.additionalCover.child2benefitamount_acchospitalisation,
      AM: this.additionalCover.child2benefitamount_accmedicalexpenses
    };

    this.daughterPremiumArray = {
      ADPremium: this.child2insureddetails.acc_premium,
      PPPremium: this.child2insureddetails.ppd_premium,
      PTPremium: this.child2insureddetails.ptd_maxinsured,
      TTPremium: this.child2insureddetails.ttd_premium,
      RFPremium: "0",
      HCPremium: this.additionalCover.child2premium_hospitalcashallowance,
     // CSPremium: this.additionalCover.child2premium_childeducationsupport,
      AAPremium: this.additionalCover.child2premium_adaptationallowance,
      FTPremium: this.additionalCover.child2premium_familytransportallowance,
     // LPPremium: this.additionalCover.child2premium_loanprotector,
      LSPremium: this.additionalCover.child2premium_lifesupportbenefit,
      MEPremium: this.additionalCover.child2premium_acchospitalisation,
      AMPremium: this.additionalCover.child2premium_accmedicalexpenses
    };

//     this.daughterTotalPremium =
//     Number(this.child2insureddetails.acc_premium) +
//     Number(this.child2insureddetails.ppd_premium) +
//     Number(this.child2insureddetails.ptd_premium) +
//     Number(this.child2insureddetails.ttd_premium) +
//     Number(this.additionalCover.child2premium_adaptationallowance) +
//  //   Number(this.additionalCover.child2premium_childeducationsupport) +
//     Number(this.additionalCover.child2premium_familytransportallowance) +
//     Number(this.additionalCover.child2premium_hospitalcashallowance) +
//   //  Number(this.additionalCover.child2premium_loanprotector) +
//     Number(this.additionalCover.child2premium_lifesupportbenefit) +
//     Number(this.additionalCover.child2premium_acchospitalisation) +
//     Number(this.additionalCover.child2premium_accmedicalexpenses) +
//     Number(this.additionalCover.child2premium_brokenbones)+
//     Number(this.additionalCover.child2premium_adventuresportbenefit)+
//     Number(this.additionalCover.child2premium_chauffeurplanbenefit)+
//     Number(this.additionalCover.child2premium_roadambulancecover)+
//     Number(this.additionalCover.child2premium_airambulancecover);
    
    



  }

  calculateSummary() {
    console.log(Number(this.selfTotalPremium))
    console.log(Number(this.spouseTotalPremium))
    console.log(Number(this.totalPremiumCalculated))
   
    let index = this.year - 1

    this.selfTotalPremium = this.selfMainCoversPremium + this.selfAddCoversPremium
    this.spouseTotalPremium =  this.spouseMainCoversPremium + this.spouseAddCoversPremium
    this.sonTotalPremium = this.child1MainCoversPremium + this.child1AddCoversPremium
    this.daughterTotalPremium  = this.child2MainCoversPremium + this.child2AddCoversPremium

    if(Number(this.selfTotalPremium) == 0){

      this.selfPremiumHide = true
    }

    if(Number(this.spouseTotalPremium) == 0){

      this.spousePremiumHide = true
    }
    if(Number(this.sonTotalPremium) == 0){

      this.child1PremiumHide = true
    }
    if(Number(this.daughterTotalPremium) == 0){

      this.child2PremiumHide = true
      
    }



    this.summaryArray = [ 
      {
        title: "Self",
        totalPremium: Number(this.selfTotalPremium.toFixed(2)),
        hide : this.selfPremiumHide
      },
      {
        title: "Spouse",
        totalPremium:Number(this.spouseTotalPremium.toFixed(2)),
        hide : this.spousePremiumHide
      },
      {
        title: "Child1",
        totalPremium: Number(this.sonTotalPremium.toFixed(2)),
        hide : this.child1PremiumHide
      },
      {
        title: "Child2",
        totalPremium:  Number(this.daughterTotalPremium.toFixed(2)), 
        hide : this.child2PremiumHide
      }
    ];




    this.totalPremiumCalculated = Number((this.selfTotalPremium + this.spouseTotalPremium + this.sonTotalPremium + this.daughterTotalPremium).toFixed(2))
    
    if (this.year == 2) {
    this.totalPremiumCalculated = Number((this.selfTotalPremium + this.spouseTotalPremium + this.sonTotalPremium + this.daughterTotalPremium).toFixed(2)) * 2
    }
    else if (this.year == 3){
      this.totalPremiumCalculated = Number((this.selfTotalPremium + this.spouseTotalPremium + this.sonTotalPremium + this.daughterTotalPremium).toFixed(2)) * 3
    
    }

    this.totalPremium = Number(this.selfTotalPremium) + Number(this.spouseTotalPremium) + Number(this.sonTotalPremium) + Number(this.daughterTotalPremium) 
  
    if (this.membersInsured.length == 1 && this.isFamilyDiscount == true) {
      this.familyDiscountPercentage = 0;
      this.familyDiscount = 0;
      this.totalPremium = Number(this.totalPremium) - Number(this.familyDiscount);

    } else {

     
      this.familyDiscountPercentage = this.EMIDetails[index].FamilyDiscountPerc;
      this.familyDiscount = this.EMIDetails[index].FamilyDiscountAmt;
      this.totalPremium = Number(this.totalPremium) - Number(this.familyDiscount);

      
    }

    
   // this.totalPremium = Number(this.totalPremium) - Number(this.familyDiscount);
    
   if (this.year == 2) {
    this.totalPremium =  this.totalPremium * 2

   }
   else if (this.year == 3){

    this.totalPremium =  this.totalPremium * 3

   }

   if(sessionStorage.IsFGEmployee =="Y"){

    this.isEmployeeDiscount = false
    this.employeeDiscount = this.EMIDetails[index].EmployeeDiscountAmt
    this.employeeDiscountPercentage = this.EMIDetails[index].EmployeeDiscountPerc

   }else{

    this.isEmployeeDiscount = true


   }
   
      // Number(this.totalPremium) - Number((this.totalPremium * 15) / 100);

  //  this.totalPremium = this.employeeDiscount;

    if (this.PolicyTerm == "OneTime") {
      if (this.year == 1) {
        this.longTermPercentageDiscount = 0
        ;
        this.longTermDiscount = 0

        this.premiumwithoutGST = Number(this.totalPremium.toFixed(2));

      } else if (this.year == 2) {
        this.longTermPercentageDiscount = this.EMIDetails[index].LongTermDiscountPercent
        this.longTermDiscount = this.EMIDetails[index].LongTermDiscountAmount
        this.premiumwithoutGST =
          Number(((this.totalPremium * this.longTermPercentageDiscount) / 100) .toFixed(2));
      } else if (this.year == 3) {
        this.longTermPercentageDiscount = this.EMIDetails[index].LongTermDiscountPercent
        this.longTermDiscount = this.EMIDetails[index].LongTermDiscountAmount;
      
      }
    } else {
      this.longTermPercentageDiscount = 0;
      this.longTermDiscount = 0;
      this.premiumwithoutGST = Number(this.totalPremium.toFixed(2));
    }
    this.premiumwithoutGST =  Number((this.totalPremium -  this.longTermDiscount).toFixed(2))
   


    if (this.PolicyTerm == "OneTime") {
      this.installmentLoadingPercentage = 0;
    } else if (this.PolicyTerm == "Monthly") {
      this.installmentLoadingPercentage = 5;
    } else if (this.PolicyTerm == "Quarterly") {
      this.installmentLoadingPercentage = 4;
    } else if (this.PolicyTerm == "HalfYearly") {
      this.installmentLoadingPercentage = 3;
    }

    this.premiumWithLoading =Number(((this.premiumwithoutGST * this.installmentLoadingPercentage) / 100).toFixed(2)) + Number(this.premiumwithoutGST.toFixed(2));

    if (this.PolicyTerm == "OneTime") {
      if (this.year == 1) {
        this.totalInstallment = 1;
      } else if (this.year == 2) {
        this.totalInstallment = 1;
      } else if (this.year == 3) {
        this.totalInstallment = 1;
      }
    } else if (this.PolicyTerm == "Monthly") {
      if (this.year == 1) {
        this.totalInstallment = 1;
      } else if (this.year == 2) {
        this.totalInstallment = 24;
      } else if (this.year == 3) {
        this.totalInstallment = 36;
      }
    } else if (this.PolicyTerm == "Quarterly") {
      if (this.year == 1) {
        this.totalInstallment = 1;
      } else if (this.year == 2) {
        this.totalInstallment = 8;
      } else if (this.year == 3) {
        this.totalInstallment = 12;
      }
    } else if (this.PolicyTerm == "HalfYearly") {
      if (this.year == 1) {
        this.totalInstallment = 1;
      } else if (this.year == 2) {
        this.totalInstallment = 4;
      } else if (this.year == 3) {
        this.totalInstallment = 6;
      }
    }

    this.premiumEmiAmount = this.EMIDetails[index].FullPaymentPremium

    this.GstAmount = Number(Number(this.EMIDetails[index].FullPaymentTax).toFixed(2))

   // this.GstAmount = Number(this.GstAmount.toFixed(2))

    this.PremiumWithGST = Number(Number(this.EMIDetails[index].TotalFullPayment).toFixed(2))
  }

  clickTab(member , index) {
    if (member == "Self") {

      //this.calculateSelfCovers(member.code);

      this.selfPremiumArray = [];
      this.selfpartialDis = true;
      this.selftotalDis = true;
      this.selftempDis = true;
      this.adaption_allowance = true;
      this.acc_hospitalisation = true;
      this.acc_medical_expenses = true;
      this.self_child_edu_support = true;
      this.self_family_trans_allowance = true;
      this.self_hospital_cash_allownce = true;
      this.self_life_support_benefit = true;
      this.self_broken_bones = true;
      this.self_road_ambulance_cover = true;
      this.self_air_ambulance_cover = true;
      this.self_adventure_sports_benefit = true;
      this.self_chauffeur_plan_benefit = true;
      this.self_loan_protector = true;
      
      

      for (let i = 0; i < this.selfCoversArrayRecieved.length; i++){

        if(this.selfCoversArrayRecieved[i].selfpartialDis == true){

          this.selfpartialDis = false
        }
        else if(this.selfCoversArrayRecieved[i].selftotalDis == true){
          this.selftotalDis = false
        }
        else if(this.selfCoversArrayRecieved[i].selftempDis == true){
          this.selftempDis = false
        }

        else if(this.selfCoversArrayRecieved[i].adaption_allowance == true){
          this.adaption_allowance = false
        }
        else if(this.selfCoversArrayRecieved[i].acc_hospitalisation == true){
          this.acc_hospitalisation = false
        }
        else if(this.selfCoversArrayRecieved[i].acc_medical_expenses == true){
          this.acc_medical_expenses = false
        }

        else if(this.selfCoversArrayRecieved[i].self_child_edu_support == true){
          this.self_child_edu_support = false
        }
        else if(this.selfCoversArrayRecieved[i].self_family_trans_allowance == true){
          this.self_family_trans_allowance = false
        }
        else if(this.selfCoversArrayRecieved[i].self_hospital_cash_allownce == true){
          this.self_hospital_cash_allownce = false
        }
        else if(this.selfCoversArrayRecieved[i].self_life_support_benefit == true){
          this.self_life_support_benefit = false
        }


        else if(this.selfCoversArrayRecieved[i].self_broken_bones == true){
          this.self_broken_bones = false
        }
        else if(this.selfCoversArrayRecieved[i].self_road_ambulance_cover == true){
          this.self_road_ambulance_cover = false
        }



        else if(this.selfCoversArrayRecieved[i].self_air_ambulance_cover == true){
          this.self_air_ambulance_cover = false
        }


        else if(this.selfCoversArrayRecieved[i].self_adventure_sports_benefit == true){
          this.self_adventure_sports_benefit = false
        }



        else if(this.selfCoversArrayRecieved[i].self_chauffeur_plan_benefit == true){
          this.self_chauffeur_plan_benefit = false
        }


        else if(this.selfCoversArrayRecieved[i].self_loan_protector == true){
          this.self_loan_protector = false
        }
      }

      

      this.selfCoversArray = {
        AD: this.insureddetails.acc_maxinsured,
        ADPremium: this.insureddetails.acc_premium,
        PP: this.insureddetails.ppd_maxinsured,
        PPPremium: this.insureddetails.ppd_premium,
        PT: this.insureddetails.ptd_maxinsured,
        PTPremium: this.insureddetails.ptd_premium,
        TT: this.insureddetails.ttd_maxinsured,
        TTPremium: this.insureddetails.ttd_premium
      };

      this.selfPremiumArray = [
        {
          hide : this.self_hospital_cash_allownce,
          additionalCover: "Hospital Cash Allowance",
          amount: this.addCommas(Number(this.additionalCover.selfbenefitamount_hospitalcashallowance).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.selfpremium_hospitalcashallowance).toFixed(2))
        },
        {
          hide : this.self_child_edu_support,
          additionalCover: "Child Education Support",
          amount: this.addCommas(Number(this.additionalCover.selfbenefitamount_childeducationsupport).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.selfpremium_childeducationsupport).toFixed(2))
        },
        {
          hide : this.adaption_allowance,
          additionalCover: "Adaptation Allowance",
          amount:  this.addCommas(Number(this.additionalCover.selfbenefitamount_adaptationallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.selfpremium_adaptationallowance).toFixed(2))
        },
        {
          hide : this.self_family_trans_allowance,
          additionalCover: "Family Transportation Allowance",
          amount: this.addCommas(Number(this.additionalCover
            .selfbenefitamount_familytransportallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.selfpremium_familytransportallowance).toFixed(2))
        },
        {
          hide : this.self_loan_protector,
          additionalCover: "Loan Protector",
          amount: this.addCommas(Number(this.additionalCover.selfbenefitamount_loanprotector).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.selfpremium_loanprotector).toFixed(2))
        },
        {
          hide : this.self_life_support_benefit,
          additionalCover: "Life Support Benefit",
          amount: this.addCommas(Number(this.additionalCover.selfbenefitamount_lifesupportbenefit).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.selfpremium_lifesupportbenefit).toFixed(2))
        },
        {
          hide : this.acc_hospitalisation,
          additionalCover: "Accidental Hospitalisation",
          amount: this.addCommas(Number(this.additionalCover.selfbenefitamount_acchospitalisation).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.selfpremium_acchospitalisation).toFixed(2))
        },
        {
          hide : this.acc_medical_expenses,
          additionalCover: "Accidental Medical Expenses",
          amount: "Opted",
          premium:  this.addCommas(Number(this.additionalCover.selfpremium_accmedicalexpenses).toFixed(2))
        },
        {
          hide : this.self_broken_bones,
          additionalCover: "Broken Bones",
          amount:  this.addCommas(Number(this.additionalCover.selfbenefitamount_brokenbones).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.selfpremium_brokenbones).toFixed(2))
        },
        {
          hide : this.self_road_ambulance_cover,
          additionalCover: "Road Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.selfbenefitamount_roadambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.selfpremium_roadambulancecover).toFixed(2))
        },
        {
          hide : this.self_air_ambulance_cover,
          additionalCover: "Air Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.selfbenefitamount_airambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.selfpremium_airambulancecover).toFixed(2))
        },
        {
          hide : this.self_adventure_sports_benefit,
          additionalCover: "Adventure Sports Benefit",
          amount:  this.addCommas(Number(this.additionalCover.selfbenefitamount_adventuresportbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.selfpremium_adventuresportbenefit).toFixed(2))
        },
        {
          hide : this.self_chauffeur_plan_benefit,
          additionalCover: "Chauffeur Plan Benefit",
          amount:  this.addCommas(Number(this.additionalCover.selfbenefitamount_chauffeurplanbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.selfpremium_chauffeurplanbenefit).toFixed(2))
        }

        
      ];

      this.totalMainCoverPremium = this.selfMainCoversPremium
       

      this.accidentalDeath = this.addCommas(Number(this.selfCoversArray["AD"]).toFixed());
      this.accidentalDeathPremium = this.addCommas(Number(this.selfCoversArray["ADPremium"]).toFixed(2));

      this.permanetpartialdisability = this.addCommas(Number(this.selfCoversArray["PP"]).toFixed());
      this.permanetpartialdisabilityPremium = this.addCommas(Number(this.selfCoversArray["PPPremium"]).toFixed(2));

      this.permanettotaldisability = this.addCommas(Number(this.selfCoversArray["PT"]).toFixed());
      this.permanettotaldisabilityPremium = this.addCommas(Number(this.selfCoversArray["PTPremium"]).toFixed(2));

      this.temporarytotaldisability = this.addCommas(Number(this.selfCoversArray["TT"]).toFixed());
      this.temporarytotaldisabilityPremium = this.addCommas(Number(this.selfCoversArray["TTPremium"]).toFixed(2));

      this.repatriationfuneralexpenses = this.addCommas(Number(this.additionalCover.selfbenefitamount_repatriationfuneralexpenses).toFixed(2));



      this.totalAdditionalCoverPremium = this.addCommas(Number(this.selfAddCoversPremium).toFixed(2));
       


      this.NetTotalCovers = this.addCommas(Number((this.selfMainCoversPremium) + Number(this.selfAddCoversPremium)).toFixed(2));


     

    } else if (member == "Spouse") {


      this.calculateSpouseCovers("SPOU")

      this.selfPremiumArray = [];
      this.selfpartialDis = true;
      this.selftotalDis = true;
      this.selftempDis = true;
      this.adaption_allowance = true;
      this.acc_hospitalisation = true;
      this.acc_medical_expenses = true;
      this.self_child_edu_support = true;
      this.self_family_trans_allowance = true;
      this.self_hospital_cash_allownce = true;
      this.self_life_support_benefit = true;
      this.self_broken_bones = true;
      this.self_road_ambulance_cover = true;
      this.self_air_ambulance_cover = true;
      this.self_adventure_sports_benefit = true;
      this.self_chauffeur_plan_benefit = true;
      this.self_loan_protector = true;



      for (let i = 0; i < this.spouseCoversArrayRecieved.length; i++){

        if(this.spouseCoversArrayRecieved[i].spousepartialDis == true){

          this.selfpartialDis = false
        }
        else if(this.spouseCoversArrayRecieved[i].spousetotalDis == true){
          this.selftotalDis = false
        }
        else if(this.spouseCoversArrayRecieved[i].spousetempDis == true){
          this.selftempDis = false
        }

        else if(this.spouseCoversArrayRecieved[i].spouse_adaption_allowance == true){
          this.adaption_allowance = false
        }
        else if(this.spouseCoversArrayRecieved[i].spouse_acc_hospitalisation == true){
          this.acc_hospitalisation = false
        }
        else if(this.spouseCoversArrayRecieved[i].spouse_acc_medical_expenses == true){
          this.acc_medical_expenses = false
        }

        else if(this.spouseCoversArrayRecieved[i].spouse_child_edu_support == true){
          this.self_child_edu_support = false
        }
        else if(this.spouseCoversArrayRecieved[i].spouse_family_trans_allowance == true){
          this.self_family_trans_allowance = false
        }
        else if(this.spouseCoversArrayRecieved[i].spouse_hospital_cash_allownce == true){
          this.self_hospital_cash_allownce = false
        }
        else if(this.spouseCoversArrayRecieved[i].spouse_life_support_benefit == true){
          this.self_life_support_benefit = false
        }


        else if(this.spouseCoversArrayRecieved[i].spouse_broken_bones == true){
          this.self_broken_bones = false
        }
        else if(this.spouseCoversArrayRecieved[i].spouse_road_ambulance_cover == true){
          this.self_road_ambulance_cover = false
        }



        else if(this.spouseCoversArrayRecieved[i].spouse_air_ambulance_cover == true){
          this.self_air_ambulance_cover = false
        }


        else if(this.spouseCoversArrayRecieved[i].spouse_adventure_sports_benefit == true){
          this.self_adventure_sports_benefit = false
        }



        else if(this.spouseCoversArrayRecieved[i].spouse_chauffeur_plan_benefit == true){
          this.self_chauffeur_plan_benefit = false
        }


        else if(this.spouseCoversArrayRecieved[i].spouse_loan_protector == true){
          this.self_loan_protector = false
        }
      }

     

      this.spouseCoversArray = {
        AD: this.spouseinsureddetails.acc_maxinsured,
        ADPremium: this.spouseinsureddetails.acc_premium,
        PP: this.spouseinsureddetails.ppd_maxinsured,
        PPPremium: this.spouseinsureddetails.ppd_premium,
        PT: this.spouseinsureddetails.ptd_maxinsured,
        PTPremium: this.spouseinsureddetails.ptd_premium,
        TT: this.spouseinsureddetails.ttd_maxinsured,
        TTPremium: this.spouseinsureddetails.ttd_premium
      };

      this.selfPremiumArray = [
        {
          hide : this.self_hospital_cash_allownce,
          additionalCover: "Hospital Cash Allowance",
          amount:  this.addCommas(Number(this.additionalCover
            .spousebenefitamount_hospitalcashallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.spousepremium_hospitalcashallowance).toFixed(2))
        },
        {
          hide : this.self_child_edu_support,
          additionalCover: "Child Education Support",
          amount: this.addCommas(Number(this.additionalCover
            .spousebenefitamount_childeducationsupport).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.spousepremium_childeducationsupport).toFixed(2))
        },
        {
          hide : this.adaption_allowance,
          additionalCover: "Adaptation Allowance",
          amount: this.addCommas(Number(this.additionalCover.spousebenefitamount_adaptationallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.spousepremium_adaptationallowance).toFixed(2))
      
        },
        {
          hide : this.self_family_trans_allowance,
          additionalCover: "Family Transportation Allowance",
          amount: this.addCommas(Number(this.additionalCover
            .spousebenefitamount_familytransportallowance).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_familytransportallowance).toFixed(2))
        },
        {
          hide : this.self_loan_protector,
          additionalCover: "Loan Protector",
          amount: this.addCommas(Number(this.additionalCover.spousebenefitamount_loanprotector).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_loanprotector).toFixed(2))
        },
        {
          hide : this.self_life_support_benefit,
          additionalCover: "Life Support Benefit",
          amount: this.addCommas(Number(this.additionalCover.spousebenefitamount_lifesupportbenefit).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.spousepremium_lifesupportbenefit).toFixed(2))
        },
        {
          hide : this.acc_hospitalisation,
          additionalCover: "Accidental Hospitalisation",
          amount: this.addCommas(Number(this.additionalCover.spousebenefitamount_acchospitalisation).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_acchospitalisation).toFixed(2))
        },
        {
          hide : this.acc_medical_expenses,
          additionalCover: "Accidental Medical Expenses",
          amount: "Opted",
          premium: this.addCommas(Number(this.additionalCover.spousepremium_accmedicalexpenses).toFixed(2))
        },
        {
          hide : this.self_broken_bones,
          additionalCover: "Broken Bones",
          amount:  this.addCommas(Number(this.additionalCover.spousebenefitamount_brokenbones).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_brokenbones).toFixed(2))
        },
        {
          hide : this.self_road_ambulance_cover,
          additionalCover: "Road Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.spousebenefitamount_roadambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_roadambulancecover).toFixed(2))
        },
        {
          hide : this.self_air_ambulance_cover,
          additionalCover: "Air Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.spousebenefitamount_airambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_airambulancecover).toFixed(2))
        },
        {
          hide : this.self_adventure_sports_benefit,
          additionalCover: "Adventure Sports Benefit",
          amount:  this.addCommas(Number(this.additionalCover.spousebenefitamount_adventuresportbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_adventuresportbenefit).toFixed(2))
        },
        {
          hide : this.self_chauffeur_plan_benefit,
          additionalCover: "Chauffeur Plan Benefit",
          amount:  this.addCommas(Number(this.additionalCover.spousebenefitamount_chauffeurplanbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.spousepremium_chauffeurplanbenefit).toFixed(2))
        }

      ];

      this.totalMainCoverPremium = this.spouseMainCoversPremium
        

      this.accidentalDeath = this.addCommas(Number(this.spouseCoversArray["AD"]).toFixed());
      this.accidentalDeathPremium = this.addCommas(Number(this.spouseCoversArray["ADPremium"]).toFixed(2))
    ;

      this.permanetpartialdisability = this.addCommas(Number(this.spouseCoversArray["PP"]).toFixed());
      this.permanetpartialdisabilityPremium = this.addCommas(Number(this.spouseCoversArray[
        "PPPremium"]).toFixed(2));

      this.permanettotaldisability = this.addCommas(Number(this.spouseCoversArray["PT"]).toFixed());
      this.permanettotaldisabilityPremium = this.addCommas(Number(this.spouseCoversArray["PTPremium"]).toFixed(2));

      this.temporarytotaldisability = this.addCommas(Number(this.spouseCoversArray["TT"]).toFixed());
      this.temporarytotaldisabilityPremium =  this.addCommas(Number(this.spouseCoversArray[
        "TTPremium"
      ]).toFixed(2));

      this.repatriationfuneralexpenses = this.addCommas(Number(this.additionalCover.spousebenefitamount_repatriationfuneralexpenses).toFixed(2));



      this.totalAdditionalCoverPremium = this.addCommas(Number(this.spouseAddCoversPremium).toFixed(2));
        

      this.NetTotalCovers = this.addCommas(Number((this.spouseMainCoversPremium) + Number(this.spouseAddCoversPremium)).toFixed(2));


       

      console.log("this"+ JSON.stringify(this.selfPremiumArray))
    }

    else if (member == "Child1") {


       for (let i = 0; i < this.membersInsured.length; i++) {
       if (this.membersInsured[i].detailsSaved == true) {
          let member = this.membersInsured[i];
  
          this.memberlist.push({name: this.membersInsured[i].title});
  
          console.log("member",member)
        
         if (this.membersInsured[i].title  == "Child1") {

            if(this.membersInsured[i].code == "Son"){
              this.calculateSonCovers("SON")
              
            }
            else if(member.code == "DAUG"){
              this.calculateSonCovers("DAUG")
              
            }
            break
        }
      }
    }



    this.selfPremiumArray = [];
    this.selfpartialDis = true;
    this.selftotalDis = true;
    this.selftempDis = true;
    this.adaption_allowance = true;
    this.acc_hospitalisation = true;
    this.acc_medical_expenses = true;
    this.self_child_edu_support = true;
    this.self_family_trans_allowance = true;
    this.self_hospital_cash_allownce = true;
    this.self_life_support_benefit = true;
    this.self_broken_bones = true;
    this.self_road_ambulance_cover = true;
    this.self_air_ambulance_cover = true;
    this.self_adventure_sports_benefit = true;
    this.self_chauffeur_plan_benefit = true;
    this.self_loan_protector = true;



    for (let i = 0; i < this.child1CoversArrayRecieved.length; i++){

      if(this.child1CoversArrayRecieved[i].child1partialDis == true){

        this.selfpartialDis = false
      }
      else if(this.child1CoversArrayRecieved[i].child1totalDis == true){
        this.selftotalDis = false
      }

      else if(this.child1CoversArrayRecieved[i].child1_adaption_allowance == true){
        this.adaption_allowance = false
      }
      else if(this.child1CoversArrayRecieved[i].child1_acc_hospitalisation == true){
        this.acc_hospitalisation = false
      }
      else if(this.child1CoversArrayRecieved[i].child1_acc_medical_expenses == true){
        this.acc_medical_expenses = false
      }

      else if(this.child1CoversArrayRecieved[i].child1_edu_support == true){
        this.self_child_edu_support = false
      }
      else if(this.child1CoversArrayRecieved[i].child1_family_trans_allowance == true){
        this.self_family_trans_allowance = false
      }
      else if(this.child1CoversArrayRecieved[i].child1_hospital_cash_allownce == true){
        this.self_hospital_cash_allownce = false
      }
      else if(this.child1CoversArrayRecieved[i].child1_life_support_benefit == true){
        this.self_life_support_benefit = false
      }


      else if(this.child1CoversArrayRecieved[i].child1_broken_bones == true){
        this.self_broken_bones = false
      }
      else if(this.child1CoversArrayRecieved[i].child1_road_ambulance_cover == true){
        this.self_road_ambulance_cover = false
      }



      else if(this.child1CoversArrayRecieved[i].child1_air_ambulance_cover == true){
        this.self_air_ambulance_cover = false
      }


      else if(this.child1CoversArrayRecieved[i].child1_adventure_sports_benefit == true){
        this.self_adventure_sports_benefit = false
      }



      else if(this.child1CoversArrayRecieved[i].child1_chauffeur_plan_benefit == true){
        this.self_chauffeur_plan_benefit = false
      }


      
    }



      this.child1CoversArray = {
        AD: this.child1insureddetails.acc_maxinsured,
        ADPremium: this.child1insureddetails.acc_premium,
        PP: this.child1insureddetails.ppd_maxinsured,
        PPPremium: this.child1insureddetails.ppd_premium,
        PT: this.child1insureddetails.ptd_maxinsured,
        PTPremium: this.child1insureddetails.ptd_premium,
        TT: this.child1insureddetails.ttd_maxinsured,
        TTPremium: this.child1insureddetails.ttd_premium
      };

      this.selfPremiumArray = [
        {
          hide : this.self_hospital_cash_allownce,
          additionalCover: "Hospital Cash Allowance",
          amount:  this.addCommas(Number(this.additionalCover
            .child1benefitamount_hospitalcashallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.child1premium_hospitalcashallowance).toFixed(2))
        },
        {
          hide : this.adaption_allowance,
          additionalCover: "Adaptation Allowance",
          amount: this.addCommas(Number(this.additionalCover.child1benefitamount_adaptationallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.child1premium_adaptationallowance).toFixed(2))
      
        },
        {
          hide : this.self_family_trans_allowance,
          additionalCover: "Family Transportation Allowance",
          amount: this.addCommas(Number(this.additionalCover
            .child1benefitamount_familytransportallowance).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child1premium_familytransportallowance).toFixed(2))
        },
        {
          hide : this.self_life_support_benefit,
          additionalCover: "Life Support Benefit",
          amount: this.addCommas(Number(this.additionalCover.child1benefitamount_lifesupportbenefit).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.child1premium_lifesupportbenefit).toFixed(2))
        },
        {
          hide : this.acc_hospitalisation,
          additionalCover: "Accidental Hospitalisation",
          amount: this.addCommas(Number(this.additionalCover.child1benefitamount_acchospitalisation).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child1premium_acchospitalisation).toFixed(2))
        },
        {
          hide : this.acc_medical_expenses,
          additionalCover: "Accidental Medical Expenses",
          amount: "Opted",
          premium: this.addCommas(Number(this.additionalCover.child1premium_accmedicalexpenses).toFixed(2))
        },
        {
          hide : this.self_broken_bones,
          additionalCover: "Broken Bones",
          amount:  this.addCommas(Number(this.additionalCover.child1benefitamount_brokenbones).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child1premium_brokenbones).toFixed(2))
        },
        {
          hide : this.self_road_ambulance_cover,
          additionalCover: "Road Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.child1benefitamount_roadambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child1premium_roadambulancecover).toFixed(2))
        },
        {
          hide : this.self_air_ambulance_cover,
          additionalCover: "Air Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.child1benefitamount_airambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child1premium_airambulancecover).toFixed(2))
        },
        {
          hide : this.self_adventure_sports_benefit,
          additionalCover: "Adventure Sports Benefit",
          amount:  this.addCommas(Number(this.additionalCover.child1benefitamount_adventuresportbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child1premium_adventuresportbenefit).toFixed(2))
        },
        {
          hide : this.self_chauffeur_plan_benefit,
          additionalCover: "Chauffeur Plan Benefit",
          amount:  this.addCommas(Number(this.additionalCover.child1benefitamount_chauffeurplanbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child1premium_chauffeurplanbenefit).toFixed(2))
        }
      ];

      this.totalMainCoverPremium = this.child1MainCoversPremium
       

      this.accidentalDeath = this.addCommas(Number(this.child1CoversArray["AD"]).toFixed());
      this.accidentalDeathPremium = this.addCommas(Number(this.child1CoversArray["ADPremium"]).toFixed(2))
    ;

      this.permanetpartialdisability = this.addCommas(Number(this.child1CoversArray["PP"]).toFixed());
      this.permanetpartialdisabilityPremium = this.addCommas(Number(this.child1CoversArray[
        "PPPremium"]).toFixed(2));

      this.permanettotaldisability = this.addCommas(Number(this.child1CoversArray["PT"]).toFixed());
      this.permanettotaldisabilityPremium = this.addCommas(Number(this.child1CoversArray["PTPremium"]).toFixed(2));

      this.temporarytotaldisability = this.addCommas(Number(this.child1CoversArray["TT"]).toFixed());
      this.temporarytotaldisabilityPremium =  this.addCommas(Number(this.child1CoversArray[
        "TTPremium"
      ]).toFixed(2));




      this.repatriationfuneralexpenses = this.addCommas(Number(this.additionalCover.child1benefitamount_repatriationfuneralexpenses).toFixed(2));

      this.totalAdditionalCoverPremium = this.addCommas(Number(this.child1AddCoversPremium).toFixed(2));


      this.NetTotalCovers = this.addCommas(Number((this.child1MainCoversPremium) + Number(this.child1AddCoversPremium)).toFixed(2));

       
      console.log("selfPremiumArrayChild1", this.selfPremiumArray)



    }

    else if (member == "Child2") {

      for (let i = 0; i < this.membersInsured.length; i++) {
        if (this.membersInsured[i].detailsSaved == true) {
          let member = this.membersInsured[i];
  
          this.memberlist.push({name: this.membersInsured[i].title});
  
         
          if (member.title  == "Child2") {
    
            if(member.code == "Son"){
              this.calculateDaughterCovers("SON")
            }
            if(member.code == "DAUG"){
              this.calculateDaughterCovers("DAUG")
            }
        }
      }
    }


      this.selfPremiumArray = [];
      this.selfpartialDis = true;
      this.selftotalDis = true;
      this.selftempDis = true;
      this.adaption_allowance = true;
      this.acc_hospitalisation = true;
      this.acc_medical_expenses = true;
      this.self_child_edu_support = true;
      this.self_family_trans_allowance = true;
      this.self_hospital_cash_allownce = true;
      this.self_life_support_benefit = true;
      this.self_broken_bones = true;
      this.self_road_ambulance_cover = true;
      this.self_air_ambulance_cover = true;
      this.self_adventure_sports_benefit = true;
      this.self_chauffeur_plan_benefit = true;
      this.self_loan_protector = true;





      for (let i = 0; i < this.child2CoversArrayRecieved.length; i++){

        if(this.child2CoversArrayRecieved[i].child2partialDis == true){

          this.selfpartialDis = false
        }
        else if(this.child2CoversArrayRecieved[i].child2totalDis == true){
          this.selftotalDis = false
        }

        else if(this.child2CoversArrayRecieved[i].child2_adaption_allowance == true){
          this.adaption_allowance = false
        }
        else if(this.child2CoversArrayRecieved[i].child2_acc_hospitalisation == true){
          this.acc_hospitalisation = false
        }
        else if(this.child2CoversArrayRecieved[i].child2_acc_medical_expenses == true){
          this.acc_medical_expenses = false
        }

        else if(this.child2CoversArrayRecieved[i].child2_edu_support == true){
          this.self_child_edu_support = false
        }
        else if(this.child2CoversArrayRecieved[i].child2_family_trans_allowance == true){
          this.self_family_trans_allowance = false
        }
        else if(this.child2CoversArrayRecieved[i].child2_hospital_cash_allownce == true){
          this.self_hospital_cash_allownce = false
        }
        else if(this.child2CoversArrayRecieved[i].child2_life_support_benefit == true){
          this.self_life_support_benefit = false
        }


        else if(this.child2CoversArrayRecieved[i].child2_broken_bones == true){
          this.self_broken_bones = false
        }
        else if(this.child2CoversArrayRecieved[i].child2_road_ambulance_cover == true){
          this.self_road_ambulance_cover = false
        }



        else if(this.child2CoversArrayRecieved[i].child2_air_ambulance_cover == true){
          this.self_air_ambulance_cover = false
        }


        else if(this.child2CoversArrayRecieved[i].child2_adventure_sports_benefit == true){
          this.self_adventure_sports_benefit = false
        }



        else if(this.child2CoversArrayRecieved[i].child2_chauffeur_plan_benefit == true){
          this.self_chauffeur_plan_benefit = false
        }


    
      }
    

      
      this.child2CoversArray = {
        AD: this.child2insureddetails.acc_maxinsured,
        ADPremium: this.child2insureddetails.acc_premium,
        PP: this.child2insureddetails.ppd_maxinsured,
        PPPremium: this.child2insureddetails.ppd_premium,
        PT: this.child2insureddetails.ptd_maxinsured,
        PTPremium: this.child2insureddetails.ptd_premium,
        TT: this.child2insureddetails.ttd_maxinsured,
        TTPremium: this.child2insureddetails.ttd_premium
      };

      this.selfPremiumArray = [
      
        {
          hide:this.self_hospital_cash_allownce,
          additionalCover: "Hospital Cash Allowance",
          amount:  this.addCommas(Number(this.additionalCover
            .child2benefitamount_hospitalcashallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.child2premium_hospitalcashallowance).toFixed(2))
        },
        // {
        //   additionalCover: "Child Education Support",
        //   amount: this.addCommas(Number(this.additionalCover
        //     .child2benefitamount_childeducationsupport).toFixed()),
        //   premium: this.addCommas(Number(this.additionalCover.child2premium_childeducationsupport).toFixed(2))
        // },
        {
          hide:this.adaption_allowance,
          additionalCover: "Adaptation Allowance",
          amount: this.addCommas(Number(this.additionalCover.child2benefitamount_adaptationallowance).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.child2premium_adaptationallowance).toFixed(2))
      
        },
        {
          hide:this.self_family_trans_allowance,
          additionalCover: "Family Transportation Allowance",
          amount: this.addCommas(Number(this.additionalCover
            .child2benefitamount_familytransportallowance).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child2premium_familytransportallowance).toFixed(2))
        },
        // {
        //   additionalCover: "Loan Protector",
        //   amount: this.addCommas(Number(this.additionalCover.child2benefitamount_loanprotector).toFixed()),
        //   premium:  this.addCommas(Number(this.additionalCover.child2premium_loanprotector).toFixed(2))
        // },
        {
          hide:this.self_life_support_benefit,
          additionalCover: "Life Support Benefit",
          amount: this.addCommas(Number(this.additionalCover.child2benefitamount_lifesupportbenefit).toFixed()),
          premium: this.addCommas(Number(this.additionalCover.child2premium_lifesupportbenefit).toFixed(2))
        },
        {
          hide:this.acc_hospitalisation,
          additionalCover: "Accidental Hospitalisation",
          amount: this.addCommas(Number(this.additionalCover.child2benefitamount_acchospitalisation).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child2premium_acchospitalisation).toFixed(2))
        },
        {
          hide:this.acc_medical_expenses,
          additionalCover: "Accidental Medical Expenses",
          amount:  "Opted",
          premium: this.addCommas(Number(this.additionalCover.child2premium_accmedicalexpenses).toFixed(2))
        },
        {
          hide:this.self_broken_bones,
          additionalCover: "Broken Bones",
          amount:  this.addCommas(Number(this.additionalCover.child2benefitamount_brokenbones).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child2premium_brokenbones).toFixed(2))
        },
        {
          hide:this.self_road_ambulance_cover,
          additionalCover: "Road Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.child2benefitamount_roadambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child2premium_roadambulancecover).toFixed(2))
        },
        {
          hide:this.self_air_ambulance_cover,
          additionalCover: "Air Ambulance Cover",
          amount:  this.addCommas(Number(this.additionalCover.child2benefitamount_airambulancecover).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child2premium_airambulancecover).toFixed(2))
        },
        {
          hide:this.self_adventure_sports_benefit,
          additionalCover: "Adventure Sports Benefit",
          amount:  this.addCommas(Number(this.additionalCover.child2benefitamount_adventuresportbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child2premium_adventuresportbenefit).toFixed(2))
        },
        {
           hide:this.self_chauffeur_plan_benefit,
           additionalCover: "Chauffeur Plan Benefit",
          amount:  this.addCommas(Number(this.additionalCover.child2benefitamount_chauffeurplanbenefit).toFixed()),
          premium:  this.addCommas(Number(this.additionalCover.child2premium_chauffeurplanbenefit).toFixed(2))
        }
      ];




      this.totalMainCoverPremium = this.child2MainCoversPremium
       

      this.accidentalDeath = this.addCommas(Number(this.child2CoversArray["AD"]).toFixed());
      this.accidentalDeathPremium = this.addCommas(Number(this.child2CoversArray["ADPremium"]).toFixed(2))
    ;

      this.permanetpartialdisability = this.addCommas(Number(this.child2CoversArray["PP"]).toFixed());
      this.permanetpartialdisabilityPremium = this.addCommas(Number(this.child2CoversArray[
        "PPPremium"]).toFixed(2));

      this.permanettotaldisability = this.addCommas(Number(this.child2CoversArray["PT"]).toFixed());
      this.permanettotaldisabilityPremium = this.addCommas(Number(this.child2CoversArray["PTPremium"]).toFixed(2));

      this.temporarytotaldisability = this.addCommas(Number(this.child2CoversArray["TT"]).toFixed());
      this.temporarytotaldisabilityPremium =  this.addCommas(Number(this.child2CoversArray[
        "TTPremium"
      ]).toFixed(2));


      this.repatriationfuneralexpenses = this.addCommas(Number(this.additionalCover.child2benefitamount_repatriationfuneralexpenses).toFixed(2));


      this.totalAdditionalCoverPremium = this.addCommas(Number(this.child2AddCoversPremium).toFixed(2));



      this.NetTotalCovers = this.addCommas(Number((this.child2MainCoversPremium) + Number(this.child2AddCoversPremium)).toFixed(2));


    }
  }

  getOneTimeCaculation(policyTerm) {
    this.PolicyTerm = policyTerm;
    this.oneTimeOneNew = this.addCommas(
      Number(this.EMIDetails[0].TotalFullPayment).toFixed()
    );

    this.oneTimeTwoNew = this.addCommas(
      Number(this.EMIDetails[1].TotalFullPayment).toFixed()
    );

    this.ActualAmountoneTimeTwoNew = this.addCommas(
      Number(this.EMIDetails[0].TotalFullPayment * 2).toFixed()
    );

    this.oneTimeThreeNew = this.addCommas(
      Number(this.EMIDetails[2].TotalFullPayment).toFixed()
    );

    this.ActualAmountoneTimeThreeNew = this.addCommas(
      Number(this.EMIDetails[0].TotalFullPayment * 3).toFixed()
    );

   
  }

  getMonthlyCaculation(policyTerm) {
    console.log("Monthly");

    this.PolicyTerm = policyTerm;
    this.monthlyTwoNew = this.addCommas(
      Number(this.EMIDetails[1].MonthelyEmiPayment).toFixed()
    );
    this.monthlyThreeNew = this.addCommas(
      Number(this.EMIDetails[2].MonthelyEmiPayment).toFixed()
    );

   
  }

  getQuarterlyCaculation(policyTerm) {
    console.log("Quarter");
    this.PolicyTerm = policyTerm;

    this.quarterlyTwoNew = this.addCommas(
      Number(this.EMIDetails[1].QuarterlyEmiPayment).toFixed()
    );
    this.quarterlyThreeNew = this.addCommas(
      Number(this.EMIDetails[2].QuarterlyEmiPayment).toFixed()
    );

 
  }

  getHalfYearlyCaculation(policyTerm) {
    console.log("HalfYearly");
    this.PolicyTerm = policyTerm;

    this.halfyearlyTwoNew = this.addCommas(
      Number(this.EMIDetails[1].HalfYearlyEmiPayment).toFixed()
    );
    this.halfyearlyThreeNew = this.addCommas(
      Number(this.EMIDetails[2].HalfYearlyEmiPayment).toFixed()
    );
    
  }
  showCardDetailsPop(event: Event, cardData, year) {
    event.stopPropagation();
    this.showPopup = false;
    this.buyPopup = true;
    this.year = year;
    this.totalPremiumCalculated = 0;
    this.premPet = "summary"
    this.familytab ="Self"
    this.calculateSummary();
  }
  buyPopUpOpen(sumInsured, year) {
    this.year = year;
    this.successLabel = true;
    this.NameOfEmailsmsProposer = "";
    this.smsTo = [];
    this.emailTo = [];
    this.email = "";
    this.mobile = "";
    this.showPopup = true;
    this.buyPopup = false;

    if (this.year == 1) {
      this.insuranceDetailsToSend = this.EMIDetails[0];
    } else if (this.year == 2) {
      this.insuranceDetailsToSend = this.EMIDetails[1];
    } else if (this.year == 3) {
      this.insuranceDetailsToSend = this.EMIDetails[2];
    }

    if(this.year == 1 && this.pet == "OneTime"){

      sessionStorage.PremiumAmount = this.oneTimeOneNew

    }
    else if(this.year == 2 && this.pet == "OneTime"){

      sessionStorage.PremiumAmount = this.oneTimeTwoNew

    }
    else if(this.year == 3 && this.pet == "OneTime"){

      sessionStorage.PremiumAmount = this.oneTimeThreeNew

    }



    else if(this.year == 2 && this.pet == "Monthly"){

      sessionStorage.PremiumAmount = this.monthlyTwoNew

    }
    else if(this.year == 3 && this.pet == "Monthly"){

      sessionStorage.PremiumAmount = this.monthlyThreeNew

    }



    else if(this.year == 2 && this.pet == "Quarterly"){

      sessionStorage.PremiumAmount = this.quarterlyTwoNew

    }
    else if(this.year == 3 && this.pet == "Quarterly"){

      sessionStorage.PremiumAmount = this.quarterlyThreeNew

    }






    else if(this.year == 2 && this.pet == "HalfYearly"){

      sessionStorage.PremiumAmount = this.halfyearlyTwoNew

    }
    else if(this.year == 3 && this.pet == "HalfYearly"){

      sessionStorage.PremiumAmount = this.halfyearlyThreeNew

    }
  }

  ////process for sendquote
  sendEMailSMS() {
    if (
      this.NameOfEmailsmsProposer == undefined ||
      this.NameOfEmailsmsProposer == null ||
      this.NameOfEmailsmsProposer == ""
    ) {
      this.showToast("Please enter proposer name");
      this.myInput.setFocus();
    } else if (
      this.email == undefined ||
      this.email == null ||
      this.email == ""
    ) {
      this.showToast("Please enter email id");
      this.emailFocus.setFocus();
    } else if (!this.matchEmail(this.email)) {
      this.showToast("Please enter valid email id");
      this.emailFocus.setFocus();
    } else if (
      this.mobile == undefined ||
      this.mobile == null ||
      this.mobile == ""
    ) {
      this.showToast("Please enter mobile number");
      this.mobileFocus.setFocus();
    } else if (this.mobile.length < 10) {
      this.showToast("Please enter 10 digit mobile number");
      this.mobileFocus.setFocus();
    } else if (!this.isValidMobile(this.mobile)) {
      this.showToast("Please enter valid mobile no");
      this.mobileFocus.setFocus();
    } else {
      sessionStorage.propName = this.NameOfEmailsmsProposer;
      sessionStorage.email = this.email;
      sessionStorage.mobile = this.mobile;
      this.emailTo[0] = this.email;
      var emailData = {
        emailto: this.emailTo[0],
        pname: this.NameOfEmailsmsProposer,
        qid: this.QuotationID,
        uid: this.UID
      };
      this.encryptEmailData(emailData);
      console.log(JSON.stringify(emailData));
    }
  }
  encryptEmailData(sendEmailData) {
    var sendData = this.appService.encryptData(
      JSON.stringify(sendEmailData),
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    console.log("testt: " + JSON.stringify(sendData));
    this.getEmailResponseData({ request: sendData });
  }

  encryptSmsData(sendSmsData) {
    var sendData = this.appService.encryptData(
      JSON.stringify(sendSmsData),
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    console.log("testt: " + JSON.stringify(sendData));
    this.getSmsResponseData({ request: sendData });
  }
  getEmailResponseData(serviceData) {
    this.presentLoadingDefault();
    let headerString = this.appService.getBase64string(
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    this.appService
      .callService(
        "FGHealth.svc/SendQuotationByEmail",
        serviceData,
        headerString
      )
      .subscribe(
        SendQuotationByEmail => {
          this.loading.dismiss();
          console.log(SendQuotationByEmail.SendQuotationByEmailResult);
          if (
            SendQuotationByEmail &&
            SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "0"
          ) {
            //this.NameOfEmailProposerIndividual = "";
            //this.emailTo = [];
            //this.sendEmailList = [];
            sessionStorage.TokenId =
              SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
            console.log("emailToken: " + sessionStorage.TokenId);
            console.log(
              SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg
            );
            this.sendSms();
          } else if (
            SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "807"
          ) {
            this.showToast(
              SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg
            );
            sessionStorage.TokenId =
              SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
            this.navCtrl.push(LoginPage);
          } else if (
            SendQuotationByEmail.SendQuotationByEmailResult.ReturnCode == "500"
          ) {
            this.showToast(
              "Oops! There seems to be a technical issue at our end. Please try again later."
            );
            sessionStorage.TokenId =
              SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
            this.navCtrl.push(LoginPage);
          } else {
            this.loading.dismiss();
            sessionStorage.TokenId =
              SendQuotationByEmail.SendQuotationByEmailResult.UserToken.TokenId;
            this.showToast(
              SendQuotationByEmail.SendQuotationByEmailResult.ReturnMsg
            );
          }
        },
        err => {
          console.log(err);
        }
      );
  }
  sendSms() {
    console.log(JSON.stringify(this.smsTo));
    this.smsTo = this.mobile;
    var smsData = {
      mobile: this.smsTo,
      pname: this.NameOfEmailsmsProposer,
      qid: this.QuotationID,
      uid: this.UID
    };
    this.encryptSmsData(smsData);
    console.log(JSON.stringify(smsData));
  }
  getSmsResponseData(serviceData) {
    let headerString = this.appService.getBase64string(
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    this.appService
      .callService("FGHealth.svc/SendQuotationBySMS", serviceData, headerString)
      .subscribe(
        SendQuotationBySMS => {
          console.log(SendQuotationBySMS.SendQuotationBySMSResult);
          if (
            SendQuotationBySMS &&
            SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "0"
          ) {
            sessionStorage.TokenId =
              SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
            //this.NameOfEmailsmsProposer = "";
            // this.smsTo = [];
            // this.emailTo = [];
            // this.email = "";
            // this.mobile = "";
            //this.buyPopup = false;
            this.successLabel = false;
            console.log("smsToken: " + sessionStorage.TokenId);
            console.log(SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg);
            //this.navCtrl.push(BuyProposerDetailsPage,{"buyPageMemberDetailsResult": this.membersInsured,"buyPageCardDetailsResult":this.insuranceDetailsToSend});
          } else if (
            SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "807"
          ) {
            this.showToast(
              SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg
            );
            sessionStorage.TokenId =
              SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
            this.navCtrl.push(LoginPage);
          } else if (
            SendQuotationBySMS.SendQuotationBySMSResult.ReturnCode == "500"
          ) {
            this.showToast(
              "Oops! There seems to be a technical issue at our end. Please try again later."
            );
            sessionStorage.TokenId =
              SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
            this.navCtrl.push(LoginPage);
          } else {
            sessionStorage.TokenId =
              SendQuotationBySMS.SendQuotationBySMSResult.UserToken.TokenId;
            this.showToast(
              SendQuotationBySMS.SendQuotationBySMSResult.ReturnMsg
            );
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  ////process for proceed
  proceedMemberDetailsPage(goToNext) {
    this.goToNext = goToNext
    if (
      this.NameOfEmailsmsProposer == undefined ||
      this.NameOfEmailsmsProposer == null ||
      this.NameOfEmailsmsProposer == ""
    ) {
      this.showToast("Please enter proposer name");
      this.myInput.setFocus();
    } else if (
      this.email == undefined ||
      this.email == null ||
      this.email == ""
    ) {
      this.showToast("Please enter email id");
      this.emailFocus.setFocus();
    } else if (!this.matchEmail(this.email)) {
      this.showToast("Please enter valid email id");
      this.emailFocus.setFocus();
    } else if (
      this.mobile == undefined ||
      this.mobile == null ||
      this.mobile == ""
    ) {
      this.showToast("Please enter mobile number");
      this.mobileFocus.setFocus();
    } else if (this.mobile.length < 10) {
      this.showToast("Please enter 10 digit mobile number");
      this.mobileFocus.setFocus();
    } else if (!this.isValidMobile(this.mobile)) {
      this.showToast("Please enter valid mobile no");
      this.mobileFocus.setFocus();
    } else {
      console.log("SessionData: " + sessionStorage.ProposerDetails);

      if (sessionStorage.isViewPopped == "true") {
        

        sessionStorage.ProposerDetails = JSON.stringify(this.membersInsured);
        console.log("click: " + sessionStorage.ProposerDetails);
        
      }

      sessionStorage.propName = this.NameOfEmailsmsProposer;
      sessionStorage.email = this.email;
      sessionStorage.mobile = this.mobile;
      this.buyPopup = false; 

      this.encryptIncompleteData(this.insuranceDetailsToSend);
    }
  }
  encryptIncompleteData(InsuredCardDetails) { 

    var sendData = this.appService.encryptData(
      JSON.stringify(this.getProceedrequest(InsuredCardDetails)),
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    console.log("testt: " + JSON.stringify(sendData));
    this.sendStage1Data("QuotePurchase.svc/Proceed_Policy", {
      request: sendData
    });
  }

  sendStage1Data(URL, serviceData) {
    let headerString = this.appService.getBase64string(
      sessionStorage.TokenId + "~" + sessionStorage.UserId
    );
    this.presentLoadingDefault();
    this.appService
      .callService(URL, serviceData, headerString)
      .subscribe(Resp => {
        this.loading.dismiss();
        console.log("Stage 1: " + JSON.stringify(Resp));
        if (Resp.Proceed_PolicyResult.ReturnCode == "0") {
          console.log("test: " + JSON.stringify(this.membersInsured));
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.pID = Resp.Proceed_PolicyResult.Data.pID;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          this.membersInsured[0].proposerName = this.NameOfEmailsmsProposer;
          this.membersInsured[0].proposerEmail = this.email;
          this.membersInsured[0].mobile = this.mobile;
          this.sendEMailSMS(); 

          if(this.goToNext == "goToNext"){
            this.navCtrl.push(PaBuyProposerDetailsPage, {
              buyPageMemberDetailsResult: this.membersInsured,
              buyPageCardDetailsResult: this.beneficiaryDetailsMember,
              PolicyType: this.policyType,
              ENQ_PolicyResponse: this.ENQ_PolicyResponse,
              beneficiaryDetailsMember: this.beneficiaryDetailsMember,
              insuranceDetailsToSend: this.insuranceDetailsToSend,
              ReferenceNo :  this.ReferenceNo
            });

          }
          
          // console.log("card: "+ JSON.stringify(this.individualDisplayData));
          console.log("member: " + JSON.stringify(this.membersInsured));
        } else if (Resp.Proceed_PolicyResult.ReturnCode == "807") {
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        } else if (Resp.Proceed_PolicyResult.ReturnCode == "500") {
          this.showToast(
            "Oops! There seems to be a technical issue at our end. Please try again later."
          );
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.navCtrl.push(LoginPage);
        } else {
          this.loading.dismiss();
          sessionStorage.TokenId = Resp.Proceed_PolicyResult.UserToken.TokenId;
          this.showToast(Resp.Proceed_PolicyResult.ReturnMsg);
        }
      });
  }
  getProceedrequest(TracsactionDetails) {

    sessionStorage.Duration = this.year;
    sessionStorage.Installments = this.pet;
    console.log("beneficiaryDetailsMemberLatest: " + JSON.stringify(this.beneficiaryDetailsMember));

    let sendRequest = {
      QuotationID: this.QuotationID,
      UID: this.UID,
      stage: "1",
      Client: this.getClientNode(),
      Risk: {
        PolicyType: this.policyType, //"HTF",
        Duration:  sessionStorage.Duration,
        Installments:  sessionStorage.Installments,
        IsfgEmployee: sessionStorage.IsFGEmployee,
        IsPos: sessionStorage.Ispos,
        beneficiaryDetailsMember: this.getbeneficiaryDetailsMember(this.beneficiaryDetailsMember),
        MemberDetails: this.membersInsured,
        CardDetails: TracsactionDetails
      }
    };
    console.log("CRTrequest: " + JSON.stringify(sendRequest));
    return sendRequest;
  }

  getClientNode() {
    let age = this.membersInsured[0].age.split("-");
    let clientSalution = "MR";
    if (this.membersInsured[0].Gender == "F") {
      clientSalution = "MRS";
      this.spouseGender = "M";
    }
    this.ClientDetails.push({
      ClientType: "I",
      CreationType: "C",
      Salutation: clientSalution,
      FirstName: this.NameOfEmailsmsProposer,
      LastName: "",
      DOB: age[0] + "/" + age[1] + "/" + age[2],
      Gender: this.membersInsured[0].Gender,
      MaritalStatus: this.membersInsured[0].maritalCode,
      Occupation: this.membersInsured[0].occupationCode,
      PANNo: this.membersInsured[0].pan,
      GSTIN: "",
      AadharNo: "",
      CKYCNo: "",
      EIANo: "",
      Address1: {
        AddrLine1: this.membersInsured[0].address,
        AddrLine2: "",
        AddrLine3: "",
        Landmark: "",
        Pincode: this.membersInsured[0].pincode,
        City: this.membersInsured[0].city,
        State: this.membersInsured[0].state,
        Country: "IND",
        AddressType: "R",
        HomeTelNo: "",
        OfficeTelNo: "",
        FAXNO: "",
        MobileNo: this.mobile,
        EmailAddr: this.email
      },
      Address2: {
        AddrLine1: "",
        AddrLine2: "",
        AddrLine3: "",
        Landmark: "",
        Pincode: this.membersInsured[0].pincode,
        City: "",
        State: "",
        Country: "IND",
        AddressType: "P",
        HomeTelNo: "",
        OfficeTelNo: "",
        FAXNO: "",
        MobileNo: this.mobile,
        EmailAddr: this.email
      }
    });
    return this.ClientDetails[this.ClientDetails.length - 1];
  }

  getbeneficiaryDetailsMember(cardDetails) {
    console.log("BuyPageResult: " + JSON.stringify(cardDetails));
    //let tempArray = this.membersInsured;
    //let tempCardArray = cardDetails.Request.beneficiaryDetailsMember.Member;
    //console.log("tempMember: " + JSON.stringify(tempArray));
    console.log("ben"+ JSON.stringify(this.beneficiaryDetailsMember));
    

    let BeneDetails = [];
    for (let i = 0; i < cardDetails.length; i++) {
      for (let j = 0; j < this.membersInsured.length; j++) {
        if (i == j) {
          if (
            this.membersInsured[j].code.toUpperCase() ==
            cardDetails[i].Relation.toUpperCase()
          ) {
            let mDob = cardDetails[i].InsuredDob.split(" ");
            let gendr = "M";
            if (
              this.membersInsured[j].title.toLowerCase().indexOf("mother") >=
                0 ||
              this.membersInsured[j].title.toLowerCase().indexOf("daughter") >=
                0
            ) {
              gendr = "F";
            } else if (this.membersInsured[j].title.toLowerCase() == "spouse") {
              gendr = this.spouseGender;
            } else if (this.membersInsured[j].title.toLowerCase() == "self") {
              gendr = this.membersInsured[0].Gender;
            }
            BeneDetails.push({
              MemberId: cardDetails[i].MemberId,
              InsuredName: this.membersInsured[j].proposerName,
              InsuredDob: mDob[0],
              InsuredGender: gendr,
              InsuredOccpn: this.membersInsured[j].occupationCode,
              CoverType: cardDetails[i].CoverType,
              SumInsured: cardDetails[i].SumInsured,
              DeductibleDiscount: "",
              Relation: cardDetails[i].Relation,
              NomineeName: "A",
              NomineeRelation: "SON",
              AnualIncome: cardDetails[i].annualIncome,
              Height: cardDetails[i].Height,
              Weight: cardDetails[i].Weight,
              NomineeAge: "",
              AppointeeName: "",
              AptRelWithominee: "",
              MedicalLoading: cardDetails[i].MedicalLoading,
              PreExstDisease: "N",
              DiseaseMedicalHistoryList: {
                DiseaseMedicalHistory: {
                  PreExistingDiseaseCode: "",
                  MedicalHistoryDetail: ""
                }
              },
              PrimaryCover: this.beneficiaryDetailsMember[i].PrimaryCover,
              SecondaryCover : this.beneficiaryDetailsMember[i].SecondaryCover
            });
            // tempArray.splice(j, 1);
            //tempCardArray.splice(j, 1);
          }
        }
      }
    }
    return BeneDetails;
    
  }
  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loading.present();
  }
  matchEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  isValidMobile(mobile) {
    var regex = /^[6-9][0-9]{9}$/;
    return regex.test(mobile);
  }
  showToast(Message) {
    if (this.canShowToast) {
      let pageToast = this.toast.create({
        message: Message,
        showCloseButton: true,
        closeButtonText: "OK",
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }
  }

  addCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    console.log("test: " + parts);
    return parts.join(".");
  }
  onBackClick() {
    this.navCtrl.pop();
  }

  // premiumClick(){
  //   this.showPopup = false;
  //   this.showTransBaground = false;
  // }

  closePopInfo() {
    this.showPopup = true;
  }
  closeModalPop() {
    // this.showPopup = false;
    this.buyPopup = true;
    this.NameOfEmailsmsProposer = "";
    this.smsTo = [];
    this.emailTo = [];
    this.sendSMSList = [];
    this.sendEmailList = [];
  }
}

class InsuredDetails {
  acc_maxinsured: any = "";
  acc_suminsured: any = "";
  acc_premium: any = "";
  ppd_maxinsured: any = "";
  ppd_suminsured: any = "";
  ppd_premium: any = "";
  ptd_maxinsured: any = "";
  ptd_suminsured: any = "";
  ptd_premium: any = "";
  ttd_maxinsured: any = "";
  ttd_suminsured: any = "";
  ttd_premium: any = "";
  additionalCover: AdditionalCover;
  self_totalpremium: any = "";
}

class AdditionalCover {
  selfbenefitamount_adaptationallowance: any = "";
  selfpremium_adaptationallowance: any = "";
  selfbenefitamount_childeducationsupport: any = "";
  selfpremium_childeducationsupport: any = "";
  selfbenefitamount_familytransportallowance: any = "";
  selfpremium_familytransportallowance: any = "";
  selfbenefitamount_hospitalcashallowance: any = "";
  selfpremium_hospitalcashallowance: any = "";
  selfbenefitamount_loanprotector: any = "";
  selfpremium_loanprotector: any = "";
  selfbenefitamount_lifesupportbenefit: any = "";
  selfpremium_lifesupportbenefit: any = "";
  selfbenefitamount_acchospitalisation: any = "";
  selfpremium_acchospitalisation: any = "";
  selfbenefitamount_accmedicalexpenses: any = "";
  selfpremium_accmedicalexpenses: any = "";
  selfbenefitamount_repatriationfuneralexpenses: any = "";
  selfpremium_repatriationfuneralexpenses: any = "";
  selfbenefitamount_brokenbones: any = "";
  selfpremium_brokenbones: any = "";
  selfbenefitamount_roadambulancecover: any = "";
  selfpremium_roadambulancecover: any = "";
  selfbenefitamount_airambulancecover: any = "";
  selfpremium_airambulancecover: any = "";
  selfbenefitamount_adventuresportbenefit: any = "";
  selfpremium_adventuresportbenefit: any = "";
  selfbenefitamount_chauffeurplanbenefit: any = "";
  selfpremium_chauffeurplanbenefit: any = "";

  spousebenefitamount_adaptationallowance: any = "";
  spousepremium_adaptationallowance: any = "";
  spousebenefitamount_childeducationsupport: any = "";
  spousepremium_childeducationsupport: any = "";
  spousebenefitamount_familytransportallowance: any = "";
  spousepremium_familytransportallowance: any = "";
  spousebenefitamount_hospitalcashallowance: any = "";
  spousepremium_hospitalcashallowance: any = "";
  spousebenefitamount_loanprotector: any = "";
  spousepremium_loanprotector: any = "";
  spousebenefitamount_lifesupportbenefit: any = "";
  spousepremium_lifesupportbenefit: any = "";
  spousebenefitamount_acchospitalisation: any = "";
  spousepremium_acchospitalisation: any = "";
  spousebenefitamount_accmedicalexpenses: any = "";
  spousepremium_accmedicalexpenses: any = "";
  spousebenefitamount_repatriationfuneralexpenses: any = "";
  spousepremium_repatriationfuneralexpenses: any = "";
  spousebenefitamount_brokenbones: any = "";
  spousepremium_brokenbones: any = "";
  spousebenefitamount_roadambulancecover: any = "";
  spousepremium_roadambulancecover: any = "";
  spousebenefitamount_airambulancecover: any = "";
  spousepremium_airambulancecover: any = "";
  spousebenefitamount_adventuresportbenefit: any = "";
  spousepremium_adventuresportbenefit: any = "";
  spousebenefitamount_chauffeurplanbenefit: any = "";
  spousepremium_chauffeurplanbenefit: any = "";

  child1benefitamount_adaptationallowance: any = "";
  child1premium_adaptationallowance: any = "";
  child1benefitamount_childeducationsupport: any = "";
  child1premium_childeducationsupport: any = "";
  child1benefitamount_familytransportallowance: any = "";
  child1premium_familytransportallowance: any = "";
  child1benefitamount_hospitalcashallowance: any = "";
  child1premium_hospitalcashallowance: any = "";
  child1benefitamount_loanprotector: any = "";
  child1premium_loanprotector: any = "";
  child1benefitamount_lifesupportbenefit: any = "";
  child1premium_lifesupportbenefit: any = "";
  child1benefitamount_acchospitalisation: any = "";
  child1premium_acchospitalisation: any = "";
  child1benefitamount_accmedicalexpenses: any = "";
  child1premium_accmedicalexpenses: any = "";
  child1benefitamount_repatriationfuneralexpenses: any = "";
  child1premium_repatriationfuneralexpenses: any = "";
  child1benefitamount_brokenbones: any = "";
  child1premium_brokenbones: any = "";
  child1benefitamount_roadambulancecover: any = "";
  child1premium_roadambulancecover: any = "";
  child1benefitamount_airambulancecover: any = "";
  child1premium_airambulancecover: any = "";
  child1benefitamount_adventuresportbenefit: any = "";
  child1premium_adventuresportbenefit: any = "";
  child1benefitamount_chauffeurplanbenefit: any = "";
  child1premium_chauffeurplanbenefit: any = "";

  child2benefitamount_adaptationallowance: any = "";
  child2premium_adaptationallowance: any = "";
  child2benefitamount_childeducationsupport: any = "";
  child2premium_childeducationsupport: any = "";
  child2benefitamount_familytransportallowance: any = "";
  child2premium_familytransportallowance: any = "";
  child2benefitamount_hospitalcashallowance: any = "";
  child2premium_hospitalcashallowance: any = "";
  child2benefitamount_loanprotector: any = "";
  child2premium_loanprotector: any = "";
  child2benefitamount_lifesupportbenefit: any = "";
  child2premium_lifesupportbenefit: any = "";
  child2benefitamount_acchospitalisation: any = "";
  child2premium_acchospitalisation: any = "";
  child2benefitamount_accmedicalexpenses: any = "";
  child2premium_accmedicalexpenses: any = "";
  child2benefitamount_repatriationfuneralexpenses: any = "";
  child2premium_repatriationfuneralexpenses: any = "";
  child2benefitamount_brokenbones: any = "";
  child2premium_brokenbones: any = "";
  child2benefitamount_roadambulancecover: any = "";
  child2premium_roadambulancecover: any = "";
  child2benefitamount_airambulancecover: any = "";
  child2premium_airambulancecover: any = "";
  child2benefitamount_adventuresportbenefit: any = "";
  child2premium_adventuresportbenefit: any = "";
  child2benefitamount_chauffeurplanbenefit: any = "";
  child2premium_chauffeurplanbenefit: any = "";
}
