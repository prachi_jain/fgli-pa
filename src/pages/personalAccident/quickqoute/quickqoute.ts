import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { DatePicker } from '@ionic-native/date-picker';
import * as $ from 'jquery';
import { PremiumcalciPage } from '../premiumcalci/premiumcalci';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';



/**
 * Generated class for the QuickqoutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quickqoute',
  templateUrl: 'quickqoute.html',
})
export class QuickqoutePage {
  hideannualIncome = false
  showTransBaground = true;
  isActive = true;
  occupationLists: any = [];
  //suminsured:any = "";

  insuretype: any = "";
  childMartial = false;

  dob: any = "";
  category: any = "";
  annualincome: any = "";
  maritalstatus: any = "";

  selfdetails = new Details();
  spousedetails = new Details();
  child1details = new Details();
  child2details = new Details();

  //Sourov Declaration----------------------------------
  canShowToast = true;
  selfCard = true;
  spouseCard = true;
  child1Card = true;
  child2Card = true;

  selfMaritalstatus;
  selfDob;
  selfCategory;
  selfAnnualincome;
  spouseMaritalstatus;
  spouseDob;
  spouseCategory;
  spouseAnnualincome;
  child1Maritalstatus;
  child1Dob;
  child1Category;
  child1Annualincome;
  child2Maritalstatus;
  child2Dob;
  child2Category;
  child2Annualincome;
  selectedMemberTitle;

  appData;
  apiAgeGroup;
  occupation;
  policyType;
  marriedStatusArray = [{ value: "Single", status: false }, { value: "Married", status: true }, { value: "Divorced", status: false }, { value: "Widow / Widower", status: false }];
  marriedStatus = "";
  memberMarried = false;
  hideOccupation = false;
  lastActiveMember;
  occupationName;
  OccupationCode;
  childAgeGroup = [];
  AgeGroup = [];
  numberLength(Num) { if (Num >= 10) { return Num } else { return "0" + Num } }
  memberAge = 0;
  memberAgeChild = 0;
  hideAge = false
  hideChildAge = true
  OccupationFocus = true;
  Occupationlistcopy = [];


  // gender structure



  isSelfGenderActive = true;
  isSpouseGenderActive = false;
  isSonGenderActive = true;
  isSon1GenderActive = true;

  selfGenderPopUp = false;
  spouseGenderPopUp = true;
  child1GenderPopUp = true;
  child2GenderPopUp = true;

selfGender = "Male";
spouseGender = "Male";
child1Gender = "Son";
child2Gender = "Son1";

  constructor(public navCtrl: NavController, public navParams: NavParams, private keyboard: Keyboard, private datePicker: DatePicker, public toast: ToastController, private cdr: ChangeDetectorRef, private alertCtrl: AlertController) {
    // console.log("hello"+JSON.parse(localStorage.AppData))
    // this.appData = JSON.parse(localStorage.AppData);
    // this.apiAgeGroup = this.appData.Masters.Age;

    this.apiAgeGroup = [
      {
        "ageCode": "ag001",
        "max": "17",
        "min": "3",
        "title": "3-17"
      },
      {
        "ageCode": "ag002",
        "max": "25",
        "min": "18",
        "title": "18-25"
      },
      {
        "ageCode": "ag003",
        "max": "30",
        "min": "26",
        "title": "26-30"
      },
      {
        "ageCode": "ag004",
        "max": "35",
        "min": "31",
        "title": "31-35"
      },
      {
        "ageCode": "ag005",
        "max": "40",
        "min": "36",
        "title": "36-40"
      },
      {
        "ageCode": "ag006",
        "max": "45",
        "min": "41",
        "title": "41-45"
      },
      {
        "ageCode": "ag007",
        "max": "50",
        "min": "46",
        "title": "46-50"
      },
      {
        "ageCode": "ag008",
        "max": "55",
        "min": "51",
        "title": "51-55"
      },
      {
        "ageCode": "ag009",
        "max": "60",
        "min": "56",
        "title": "56-60"
      },
      {
        "ageCode": "ag0010",
        "max": "65",
        "min": "61",
        "title": "61-65"
      },
      {
        "ageCode": "ag0011",
        "max": "70",
        "min": "66",
        "title": "66-70"
      },

    ];
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickqoutePage');
    setTimeout(function () {
      document.getElementById("splash").style.display = 'none';
    }, 7000);
    this.childAgeRange();
    this.AgeRange();
    console.log("this is childage" + this.childAgeGroup)
    console.log("this is age" + this.AgeGroup)
    this.getOccupationList();
  }
  AgeRange() {
    for (let i = 0; i < this.apiAgeGroup.length; i++) {
      if (this.apiAgeGroup[i].min >= 18) {
        this.AgeGroup.push(this.apiAgeGroup[i]);
      }

    }
  }
  childAgeRange() {
    for (let i = 0; i < this.apiAgeGroup.length; i++) {
      if (this.apiAgeGroup[i].min <= 18) {
        this.childAgeGroup.push(this.apiAgeGroup[i]);
      }

    }
  }
  
  showToast(Message) {
    if (this.canShowToast) {
      let pageToast = this.toast.create({
        message: Message,
        showCloseButton: true,
        closeButtonText: 'Ok',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }
  }

  openPopup(type) {




    this.insuretype = type;
    this.memberMarried = false;

    if (type == 1) {
      this.showTransBaground = false;
      if (this.isActive == true) {
        // male
        //this.occupation = new Occupation("Househusband", "4");
      }
      else {
       
       // this.occupation = new Occupation("Housewife", "4");
      }


    //  this.occupationLists[3] = this.occupation;

      $(".qCardPopup").attr("hidden", false);


      console.log(this.occupationLists)
      this.hideannualIncome = false
      this.selectedMemberTitle = "Self";
      this.memberAge = this.selfdetails.age
      this.category = this.selfdetails.occupation;
      this.maritalstatus = this.selfdetails.maritalstatus;
      this.marriedStatus = this.selfdetails.maritalstatus;
      this.annualincome = this.selfdetails.annualincome;
      this.childMartial = false;
      this.memberMarried = true;
      this.hideOccupation = false
      this.hideAge = false
      this.hideChildAge = true
      this.occupationName = this.selfdetails.occupationName


      this.selfGenderPopUp = false
      this.spouseGenderPopUp = true
      this.child1GenderPopUp = true
      this.child2GenderPopUp = true


    } else if (type == 2) {



      this.OccupationFocus = false;



      this.selfGenderPopUp = true
      this.spouseGenderPopUp = false
      this.child1GenderPopUp = true
      this.child2GenderPopUp = true

      if (this.selfdetails.annualincome == "") {

        this.showToast('Please fill in self details to move ahead')

        return
      } else {

        this.showTransBaground = false;
        this.hideannualIncome = false
        if (this.marriedStatus == "Single") {
          this.showTransBaground = true;
          $(".qCardPopup").attr("hidden", true);
          this.showToast("As your marital status is Single, you cannot fill spouse details.");
          return

        }
        else if (this.marriedStatus == "Divorced") {
          this.showTransBaground = true;
          $(".qCardPopup").attr("hidden", true);
          this.showToast("As your marital status is Divorced, you cannot fill spouse details.");
          return

        }
        else if (this.marriedStatus == "Widow / Widower") {
          this.showTransBaground = true;
          $(".qCardPopup").attr("hidden", true);
          this.showToast("As your marital status is Widow / Widower, you cannot fill spouse details.");
          return

        }
        else {


          this.isActive = !this.isActive;
          if (this.isActive == false) {
            // female
           // this.occupation = new Occupation("Housewife", "4");
          }
          else {

           // this.occupation = new Occupation("Househusband", "4");
          }
          //this.occupationLists[3] = this.occupation;

          console.log(this.occupationLists)
          this.selectedMemberTitle = "Spouse";
          this.memberAge = this.spousedetails.age
          this.category = this.spousedetails.occupation;
          this.maritalstatus = this.spousedetails.maritalstatus;
          this.marriedStatus = this.spousedetails.maritalstatus;
          this.annualincome = this.spousedetails.annualincome;
          this.childMartial = true;
          this.hideOccupation = false
          this.hideAge = false
          this.hideChildAge = true
          this.occupationName = this.spousedetails.occupationName
        }
        $(".qCardPopup").attr("hidden", false);
      }
    } else if (type == 3) {


      this.selfGenderPopUp = true
      this.spouseGenderPopUp = true
      this.child1GenderPopUp = false
      this.child2GenderPopUp = true


      if (this.selfdetails.annualincome == "") {

        this.showToast('Please fill in self details to move ahead')

        return
      } else {


        this.showTransBaground = false;
        this.occupationLists.splice(3, 1);
        this.hideannualIncome = true
        this.selectedMemberTitle = "Child1";
        this.memberAgeChild = this.child2details.age;
        this.category = this.child1details.occupation;
        this.maritalstatus = this.child1details.maritalstatus;
        this.marriedStatus = this.child1details.maritalstatus;
        this.annualincome = this.child1details.annualincome;
        this.childMartial = true;
        this.hideOccupation = true
        this.hideAge = true
        this.hideChildAge = false
        this.occupation = this.child1Category
        $(".qCardPopup").attr("hidden", false);
      }

    } else if (type == 4) {


      this.selfGenderPopUp = true
      this.spouseGenderPopUp = true
      this.child1GenderPopUp = true
      this.child2GenderPopUp = false

      if (this.selfdetails.annualincome == "") {

        this.showToast('Please fill in self details to move ahead')

        return
      } else {
        this.showTransBaground = false;



        this.occupationLists.splice(3, 1);
        this.hideannualIncome = true
        this.selectedMemberTitle = "Child2";
        this.memberAgeChild = this.child2details.age
        this.category = this.child2details.occupation;
        this.maritalstatus = this.child2details.maritalstatus;
        this.marriedStatus = this.child2details.maritalstatus;
        this.annualincome = this.child2details.annualincome;
        this.childMartial = true;
        this.hideOccupation = true
        this.hideAge = true
        this.hideChildAge = false
        this.occupation = this.child2Category
        $(".qCardPopup").attr("hidden", false);
      }
    }


  }

  hidePopup() {
    this.showTransBaground = true;
    this.memberMarried = false;
    this.OccupationFocus = false;
    $(".qCardPopup").attr("hidden", true);
  }

  genderClick(selectedMemberTitle, gender) {


    console.log(selectedMemberTitle, gender);


    if (selectedMemberTitle == "Self") {

      if (gender == "Male") {

        this.isSelfGenderActive = true;
        this.selfGender = "Male"
        this.isSpouseGenderActive = false;
      }
      else {


        this.isSelfGenderActive = false;
        this.selfGender = "Female"
        this.isSpouseGenderActive = true;
      }
    }
    if (selectedMemberTitle == "Spouse") {

      if (gender == "Male") {

        this.isSpouseGenderActive = true;
        this.spouseGender = "Male"
      }
      else {


        this.isSpouseGenderActive = false;
        this.spouseGender = "Female"

      }
    }
    if (selectedMemberTitle == "Child1") {

      if (gender == "Son") {

        this.isSonGenderActive = true;
        this.child1Gender = "Son"
      }
      else {


        this.isSonGenderActive = false;
        this.child1Gender = "Daughter"

      }
    }
    if (selectedMemberTitle == "Child2") {

      if (gender == "Son") {


        this.isSon1GenderActive = true;


        if(this.child1Gender == "Son"){
          this.child2Gender = "Son1"
        }else{

        this.child2Gender = "Son"
        }
      }
      else {

        
        this.isSon1GenderActive = false;

        if(this.child1Gender == "Daughter"){

          this.child2Gender = "Daughter1"
        }else
        {
          this.child2Gender = "Daughter"
        }
       
      }
    }


    //   if (this.isActive == true) {
    //     // male
    //     this.occupation = new Occupation("Househusband", "4");
    //   }
    //   else {

    //     this.occupation = new Occupation("Housewife", "4");
    //   }

    // }
    // else if (this.selectedMemberTitle == "Spouse") {


    //   if (this.isActive == false) {
    //     // female
    //     this.occupation = new Occupation("Housewife", "4");
    //   }
    //   else {

    //     this.occupation = new Occupation("Househusband", "4");
    //   }

    // }


    // this.occupationLists[3] = this.occupation;

    // console.log(this.occupationLists)
  }

  getOccupationList() {
    //Call API and get master
    // let occupation1 = new Occupation("Accountant", "1");
    // let occupation2 = new Occupation("Actor/Actress", "2");
    // let occupation3 = new Occupation("Air Force", "3");
    // let occupation4 = new Occupation("Househusband", "4");

    // this.occupationLists[0] = occupation1;
    // this.occupationLists[1] = occupation2;
    // this.occupationLists[2] = occupation3;
    // this.occupationLists[3] = occupation4;

    let occupation1 = new Occupation("Accountant","1","ACCT");
    let occupation2 = new Occupation("Advocate","1","ADVO");
    let occupation3 = new Occupation("Admin Executive","1","AMEX");
    let occupation4 = new Occupation("Builder","2","BULD");
    let occupation5 = new Occupation("Businessman","1","BUS1");
    let occupation6 = new Occupation("Computer Engineer","1","COME");
    let occupation7 = new Occupation("Consultant","1","CONS");
    let occupation8 = new Occupation("Doctor","1","DOCT");
    let occupation9 = new Occupation("Engineer (Non IT)","2","ENGR");
    let occupation10 = new Occupation("Executive","1","EXEC");
    let occupation11 = new Occupation("Farmer","2","FARM");
    let occupation12= new Occupation("Human Resource Officer","1","HROF");
    let occupation13 = new Occupation("Housewife/ HouseHusband","1","HSWF");
    let occupation14 = new Occupation("Manager","1","MGR");
    let occupation15 = new Occupation("Marketing Executive","2","MKTG");
    let occupation16 = new Occupation("Officer","1","OFFR");
    let occupation17 = new Occupation("Retired","1","RETR");
    let occupation18 = new Occupation("Salesmen","2","SALE");
    let occupation19 = new Occupation("Sports Person","2","SPOT");
    let occupation20 = new Occupation("Student","1","STDN");
    let occupation21 = new Occupation("Service","1","SVCM");
    let occupation22 = new Occupation("Teacher","1","TEAC");
    let occupation23 = new Occupation("Technician","2","TECH");
    let occupation24 = new Occupation("Unemployed","1","UNEM");


    this.occupationLists[0] = occupation1;
    this.occupationLists[1] = occupation2;
    this.occupationLists[2] = occupation3;
    this.occupationLists[3] = occupation4;
    this.occupationLists[4] = occupation5;
    this.occupationLists[5] = occupation6;
    this.occupationLists[6] = occupation7;
    this.occupationLists[7] = occupation8
    this.occupationLists[8] = occupation9;
    this.occupationLists[9] = occupation10;
    this.occupationLists[10] = occupation11;
    this.occupationLists[11] = occupation12;
    this.occupationLists[12] = occupation13;
    this.occupationLists[13] = occupation14;
    this.occupationLists[14] = occupation15;
    this.occupationLists[15] = occupation16;
    this.occupationLists[16] = occupation17;
    this.occupationLists[17] = occupation18;
    this.occupationLists[18] = occupation19;
    this.occupationLists[19] = occupation20;
    this.occupationLists[20] = occupation21;
    this.occupationLists[21] = occupation22;
    this.occupationLists[22] = occupation23;
    this.occupationLists[23] = occupation24;

  }
  onBackClick() {
    this.showTransBaground = true
    this.navCtrl.pop();

  }
  saveMemberDetails() {
    console.log("status: " + this.selfCard);



    if ((this.memberAge == 0 && this.selectedMemberTitle == "Self") || (this.memberAge == 0 && this.selectedMemberTitle == "Spouse")) {
      //show error messages
      this.showToast("Please select member's age.");
    } else if ((this.memberAgeChild == 0 && this.selectedMemberTitle == "Child1") || (this.memberAgeChild == 0 && this.selectedMemberTitle == "Child2")) {

      this.showToast("Please select member's age.");
    }
    else if ((this.category == "" && this.selectedMemberTitle == "Self") || (this.category == "" && this.selectedMemberTitle == "Spouse")) {
      this.showToast("Please select occupation.");
    } else if (this.marriedStatus == "" && this.selectedMemberTitle == "Self") {
      this.showToast("Please select marital status.");
    } else if ((this.annualincome == "" && this.selectedMemberTitle == "Self") || (this.annualincome == "" && this.selectedMemberTitle == "Spouse")) {

      this.showToast("Please enter annual income.");

    }
    else if(this.selectedMemberTitle == "Self" && this.selfGender == ""){

      this.showToast("Please select gender.");

    }
    else if(this.selectedMemberTitle == "Spouse" && this.spouseGender == ""){

      this.showToast("Please select gender.");

    }
    else if(this.selectedMemberTitle == "Child1" && this.child1Gender == ""){

      this.showToast("Please select gender.");

    }
    else if(this.selectedMemberTitle == "Child2" && this.child2Gender == ""){

      this.showToast("Please select gender.");

    } else if((Number(this.annualincome) < 40000 && this.selectedMemberTitle == "Self") || (Number(this.annualincome) < 40000 && this.selectedMemberTitle == "Spouse") ){

      this.showToast("You can not enter amount below 40000");

    }
    else {
      //save details depending on type
      if (this.insuretype != "") {
        if (this.insuretype == 1) {

          this.selfCard = false;
          this.selfdetails.age = this.memberAge;
          this.selfDob = this.displayAge(this.memberAge);
          this.selfdetails.ageText = this.selfDob;
          this.selfdetails.occupation = this.category;
          this.selfCategory = this.occupationName;
          this.selfdetails.occupationName = this.occupationName;
          this.selfdetails.occupationCode = this.OccupationCode
          this.selfdetails.maritalstatus = this.marriedStatus;
          this.selfMaritalstatus = this.marriedStatus;
          this.selfdetails.annualincome = this.annualincome;
          this.selfdetails.annualincome = this.selfdetails.annualincome.replace(/\D/g, '');
          this.selfAnnualincome = this.annualincome;
          this.selfAnnualincome = this.selfAnnualincome.replace(/\D/g, '');
          this.selfdetails.detailsSaved = true;
          this.selfdetails.gender = this.selfGender;

          if((this.selfdetails.maritalstatus == "Single") || (this.selfdetails.maritalstatus == "Divorced") || (this.selfdetails.maritalstatus == "Widow / Widower")){
            this.spouseCard = true;
            this.spousedetails.age = "";
            this.spouseDob = "";
            this.spousedetails.occupation = "";
            this.spouseCategory = "";
            this.spousedetails.occupationName = "";
            this.spousedetails.occupationCode = "";
            this.spousedetails.maritalstatus = "";
            this.spousedetails.annualincome = "";
            this.spouseAnnualincome = "";
            sessionStorage.spousedetails = "";
            sessionStorage.spouseShowStatus = false;
            this.isSpouseGenderActive = true

          }
          this.lastActiveMember = "Self"
          sessionStorage.selfdetails = JSON.stringify(this.selfdetails);
          sessionStorage.selfShowStatus = true;


        } else if (this.insuretype == 2) {
          this.spouseCard = false;
          this.spouseDob = this.displayAge(this.memberAge);
          this.spousedetails.age = this.memberAge;
          this.spousedetails.ageText = this.spouseDob
          this.spousedetails.occupation = this.category;
          this.spouseCategory = this.occupationName;
          this.spousedetails.occupationName = this.occupationName;
          this.spousedetails.occupationCode = this.OccupationCode
          this.spousedetails.maritalstatus = this.marriedStatus;
          this.spouseMaritalstatus = this.marriedStatus;

          this.spousedetails.annualincome = this.annualincome;
          this.spousedetails.annualincome = this.spousedetails.annualincome.replace(/\D/g, '');
          this.spouseAnnualincome = this.annualincome;
          this.spouseAnnualincome = this.spouseAnnualincome.replace(/\D/g, '');
          this.spousedetails.gender = this.spouseGender;
          this.lastActiveMember = "Spouse"
          this.spousedetails.detailsSaved = true;
          sessionStorage.spousedetails = JSON.stringify(this.spousedetails);
          sessionStorage.spouseShowStatus = true;

        } else if (this.insuretype == 3) {
          this.child1Card = false;
          this.child1details.age = this.memberAgeChild;
          this.child1Dob = this.displayAge(this.memberAgeChild);
          this.child1details.ageText = this.child1Dob;




          this.child1details.occupation = this.category;
          this.child1Category = this.category;
          this.child1details.occupationCode = this.OccupationCode



          this.child1details.maritalstatus = this.marriedStatus;
          this.child1Maritalstatus = this.marriedStatus;

          this.child1details.annualincome = this.annualincome;
          this.child1Annualincome = this.annualincome;
          this.child1Annualincome = this.child1Annualincome.replace(/\D/g, '');


          this.child1details.gender = this.child1Gender;
          this.lastActiveMember = "Child1"
          this.child1details.detailsSaved = true;
          sessionStorage.child1details = JSON.stringify(this.child1details);
          sessionStorage.child1ShowStatus = true;


        } else if (this.insuretype == 4) {
          this.child2Card = false;
          this.child2details.age = this.memberAgeChild;
          this.child2Dob = this.displayAge(this.memberAgeChild);
          this.child2details.ageText = this.child2Dob;


          this.child2details.occupation = this.category;
          this.child2Category = this.category;
          this.child2details.occupationCode = this.OccupationCode



          this.child2details.maritalstatus = this.marriedStatus;
          this.child2Maritalstatus = this.marriedStatus;

          this.child2details.annualincome = this.annualincome;
          this.child2Annualincome = this.annualincome;
          this.child2Annualincome = this.child2Annualincome.replace(/\D/g, '');
          this.child2details.gender = this.child2Gender;

          this.lastActiveMember = "Child2"
          this.child2details.detailsSaved = true;
          sessionStorage.child2details = JSON.stringify(this.child2details);
          sessionStorage.child2ShowStatus = true;
        }

        this.hidePopup();
      }
      this.memberAge = 0;
    this.memberAgeChild = 0;
    }
    

  }
  clearMemberDetails(event: Event, member) {
    event.stopPropagation();

    if (member == "Self") {

      this.isActive = true;
      this.selfCard = true;
      console.log("status: " + this.selfCard);
      this.selfdetails.age = "";
      this.selfDob = "";
      this.selfdetails.occupation = "";
      this.selfCategory = "";
      this.selfdetails.occupationName = "";
      this.selfdetails.maritalstatus = "";
      this.selfdetails.annualincome = "";
      this.selfdetails.occupationCode = "";
      this.selfAnnualincome = "";
      sessionStorage.selfdetails = "";
      sessionStorage.selfShowStatus = false;
      this.isSelfGenderActive = true
    }
    else if (member == "Spouse") {

      this.spouseCard = true;
      this.spousedetails.age = "";
      this.spouseDob = "";
      this.spousedetails.occupation = "";
      this.spouseCategory = "";
      this.spousedetails.occupationName = "";
      this.spousedetails.occupationCode = "";
      this.spousedetails.maritalstatus = "";
      this.spousedetails.annualincome = "";
      this.spouseAnnualincome = "";
      sessionStorage.spousedetails = "";
      sessionStorage.spouseShowStatus = false;
      this.isSpouseGenderActive = true
    }
    else if (member == "Child1") {
      this.child1Card = true;
      this.child1details.age = "";
      this.child1Dob = "";
      this.child1details.occupation = "";
      this.child1details.occupationCode = "";
      this.child1Category = "";

      this.child1details.maritalstatus = "";
      if (this.maritalstatus == 0) {
        console.log("IF");

        this.child1Maritalstatus = "Single";
      } else {
        console.log("else");
        this.child1Maritalstatus = "Married";
      }
      this.child1details.annualincome = "";
      this.child1Annualincome = "";
      sessionStorage.child1details = "";
      sessionStorage.child1ShowStatus = "";
      this.isSpouseGenderActive = true

    }
    else if (member == "Child2") {

      this.child2Card = true;
      this.child2details.age = "";
      this.child2Dob = "";
      this.child2details.occupation = "";
      this.child2details.occupationCode = "";
      
      this.child2Category = "";

      this.child2details.maritalstatus = "";

      this.child2details.annualincome = "";
      this.child2Annualincome = "";
      sessionStorage.child2details = "";
      sessionStorage.child2ShowStatus = "";
      this.isSon1GenderActive = true
    }

  }
  
  onFocus($event) {
    this.OccupationFocus = true;
    this.Occupationlistcopy = this.occupationLists;
  }
  onOccupationTextChange(ev: any) {
    console.log("" + ev)


    let val = ev.target.value;
    if (val && val.length >= 1) {
      console.log("text" + val);

      this.OccupationFocus = true;
      this.Occupationlistcopy = this.occupationLists.filter((data) => {
        return data.occupationName.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.Occupationlistcopy = this.occupationLists;
      this.OccupationFocus = true;
    }
  }
  onOccupationSelection(listitem: any) {

    console.log("listitem"+listitem)
    this.category = listitem.category
    this.occupationName = listitem.occupationName;
    this.OccupationCode = listitem.OccupationCode
    this.OccupationFocus = false;

  }
  displayAge(agecode) {
    for (let i = 0; i < this.apiAgeGroup.length; i++) {
      if (agecode == this.apiAgeGroup[i].ageCode) {
        return this.apiAgeGroup[i].title + " years";
      }
    }
  }
  proceedtoNext() {
    console.log("stausmarrid" + this.selfdetails.maritalstatus);
    console.log("stausmarrid" + this.selfdetails.maritalstatus);

    if (this.selfdetails.age == "") {
      this.showToast("Please fill self details before going ahead.");
    } else if (this.selfdetails.maritalstatus == "Married" && this.spousedetails.age == "") {
      this.showToast("Please fill spouse details before going ahead.");
    } else {

      if (this.selfdetails.annualincome > 0 && this.spousedetails.annualincome == 0 && this.child1details.age == "" && this.child2details.age == "") {

        this.policyType = "PATI";
        this.navCtrl.push(PremiumcalciPage, { policyType: this.policyType });

        // this.showToast("You have selected Individual Policy");
        // let alert = this.alertCtrl.create({
        //   title: 'You have selected Individual Policy',
        //   subTitle: '',
        //   cssClass: 'my-class',
        //   buttons: [
        //     {
        //       text: 'OK',
        //       handler: () => {
        //         console.log('Agree clicked');
        //         this.navCtrl.push(PremiumcalciPage, { policyType: this.policyType });
        //         alert.dismiss();
        //       }
        //     }
        //   ]
        // });

        //alert.present();
      }

      if ((this.selfdetails.annualincome > 0 && this.spousedetails.annualincome > 0) || this.child1details.age != "" || this.child2details.age != "") {

        this.policyType = "PATF";
        // this.showToast("You have selected Family Policy");
        let alert = this.alertCtrl.create({
          title: 'You have selected Family Policy',
          subTitle: '',
          cssClass: 'my-class',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                console.log('Agree clicked');
                this.navCtrl.push(PremiumcalciPage, { policyType: this.policyType });
                alert.dismiss();
              }
            }
          ]
        });
        alert.present();
      }

    }
  }

  //Sourov -------

  DatePick() {
    //this.keyboard.hide();      
    let element = this.dob;
    let dateFieldValue = element;
    let year: any = 1985, month: any = 1, date: any = 1;
    //this.childAgeStatus = true;
    // if(dateFieldValue!="" ||  dateFieldValue == undefined){
    //   date = dateFieldValue.split("-")[0]
    //   month = dateFieldValue.split("-")[1];
    //   year = dateFieldValue.split("-")[2]
    // }else if(this.popupMember == "Child"){
    //   date = 1;
    //   month = 1;
    //   year = this.today.getFullYear()-25;
    // }


    // let minDateLimit  = this.popupMember == "Child" ? ( new Date(year,1,1).getTime()) : (new Date(1900,1,1).getTime()); 
    // console.log("popMember: " + this.popupMember);

    this.datePicker.show({
      date: new Date(year, month - 1, date),
      //minDate: minDateLimit,
      maxDate: Date.now(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        let dt = (this.numberLength(date.getDate()) + "-" + this.numberLength(date.getMonth() + 1) + "-" + (date.getFullYear())).toString();
        let age = this.getAge(dt);
        //  if(parseInt(age.split(" ")[0]) > 50){
        //   this.showToast("The maximum age for proposer should not be more than 50 years. Please contact nearest branch.");
        //   this.dob = "";
        //  }else if(parseInt(age.split(" ")[0]) > 24 && this.popupMember == "Child"){
        //     this.showToast("Child's age must not be more than 25 years to enter policy");
        //     this.dob = "";
        //     //this.childAgeStatus = false;
        //  }else if(parseInt(age.split(" ")[0]) > 60 && this.popupMember == "Pare"){
        //   console.log("Pare");

        //  this.showToast("Parent's age must not be more than 60 years to enter policy");
        //  this.dob = "";
        // }
        this.dob = dt;
        //this.memberArray[index].nomineeAge  = this.SelfDob[index];
        //console.log("DOB: " + members.nomineeDate);
        console.log("DOB: " + this.dob);
      },

      err => console.log('Error occurred while getting date: ' + err)
    );
  }

  onOtherDateChange(ev) {
    //console.log("Pop:" + this.popupMember);


    if ((ev.target.value.length == 2 || ev.target.value.length == 5) && ev.key != "Backspace") {
      ev.target.value = this.dob + "-";

      this.cdr.detectChanges();
    } else if (ev.target.value.length == 10) {
      let age: any = this.getAge(ev.target.value);
      // if(parseInt(age.split(" ")[0]) > 50){
      //   this.showToast("Insured age must not be more than 50 years.Please contact nearest branch.");
      //   this.dob = "";
      // }else if(parseInt(age.split(" ")[0]) > 24 && this.popupMember == "Child"){
      //   console.log("child");
      //   this.showToast("Child's age must not be more than 25 years to enter policy");
      //   this.dob = "";
      //   ev.target.value = "";
      //   this.cdr.detectChanges();
      // }else if(parseInt(age.split(" ")[0]) > 60 && this.popupMember == "Pare"){
      //   console.log("Pare");

      //  this.showToast("Parent's age must not be more than 60 years to enter policy");
      //  this.dob = "";
      //  ev.target.value = "";
      // }
      //this.memberArray[idx].age = ev.target.value;
      //this.keyboard.hide()();
    }
    // else if(ev.keyCode == 8){
    //   ev.target.value.pop();
    // }
  }


  getAge(date) {
    let dateVar = date.split("-");
    let ag = (Date.now() - (new Date(dateVar[2], dateVar[1] - 1, dateVar[0])).getTime());
    return Math.floor((ag / (1000 * 3600 * 24)) / 365) + " years";
  }

  filledStatus(status) {
    //console.log(status);

    for (let i = 0; i < this.marriedStatusArray.length; i++) {
      if (this.memberMarried == this.marriedStatusArray[i].status) {
        return this.marriedStatusArray[i].value;
      }
    }
  }
}

class Occupation {

  private occupationName: any;
  private category: any;
  private OccupationCode: any;

  constructor(occupationName: any, category: any , OccupationCode:any) {
    this.occupationName = occupationName;
    this.category = category;
    this.OccupationCode = OccupationCode
  }

}

class Details {
  age: any = "";
  occupation: any = "";
  maritalstatus: any = "";
  annualincome: any = "";
  ageText: any = "";
  detailsSaved: any = "";
  occupationName: any = "";
  gender:any = "";
  occupationCode:any = "";
}



