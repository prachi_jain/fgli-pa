import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Network } from '@ionic-native/network';
import { Keyboard } from '@ionic-native/keyboard';
import { MyApp } from './app.component';
import { BuyPage } from '../pages/health/buy/buy';
import { BuyPageResultPage } from '../pages/health/buy-page-result/buy-page-result';
import { HomePage } from '../pages/health/home/home';
import { LoginPage } from '../pages/health/login/login';
import { LandingScreenPage } from '../pages/health/landing-screen/landing-screen';
import { DashboardPage } from '../pages/health/dashboard/dashboard';
import { DashboardListingPage } from '../pages/health/dashboard-listing/dashboard-listing';
import { QuickQuotePage } from '../pages/health/quick-quote/quick-quote';
import { QuickQuoteResultPage } from '../pages/health/quick-quote-result/quick-quote-result';
import { BuyProposerDetailsPage } from '../pages/health/buy-proposer-details/buy-proposer-details';
import { BuyHealthDeclarationPage } from '../pages/health/buy-health-declaration/buy-health-declaration';
import { VerifyDetailsPage } from '../pages/health/verify-details/verify-details';
import { BuyPolicyReviewPage } from '../pages/health/buy-policy-review/buy-policy-review';
import { PaymentMethodPage } from '../pages/health/payment-method/payment-method';
import { PaymentSuccessPage } from '../pages/health/payment-success/payment-success';
import { HealthPolicyReviewPage } from '../pages/health/health-policy-review/health-policy-review';
import { AppService } from '../providers/app-service/app-service';
import { HttpClientModule }from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatExpansionModule} from '@angular/material';
import {ChartsModule} from 'ng2-charts';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DatePicker } from '@ionic-native/date-picker';
import { Camera } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import { ToggleTextDirective } from '../directives/toggle-text/toggle-text';
import { NomineeDetailsPage } from '../pages/health/nominee-details/nominee-details';
import { DisclaimerPage } from '../pages/health/disclaimer/disclaimer';
import { CallNumber } from '@ionic-native/call-number';
import { ProductBenefitsPage } from '../pages/health/product-benefits/product-benefits';
import { MarketingColateralPage } from '../pages/health/marketing-colateral/marketing-colateral';
import { SearchResultPage } from '../pages/health/search-result/search-result';
import { PlansLimitsPage } from '../pages/health/plans-limits/plans-limits';
import { HospitalLocatorPage } from '../pages/health/hospital-locator/hospital-locator';
import { HospitalSearchResultPage } from '../pages/health/hospital-search-result/hospital-search-result';
import { LocationmapPage } from '../pages/health/locationmap/locationmap';
import { NativeGeocoder }  from '@ionic-native/native-geocoder';
import { FaqPage } from '../pages/health/faq/faq';
import { Device } from '@ionic-native/device';
import { ViewProfilePage } from '../pages/view-profile/view-profile';
import { OtpPasswdPage } from '../pages/otp-passwd/otp-passwd';
import { GenOtpPage } from '../pages/gen-otp/gen-otp';
import { ForgotpasswordPage } from '../pages/forgotpassword/forgotpassword';
import { KnowledgebasePage } from '../pages/health/knowledgebase/knowledgebase';
import { DirectivesModule } from '../directives/directives.module';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Market } from '@ionic-native/market';
import { VecproposerdetailsPage } from '../pages/vector/vecproposerdetails/vecproposerdetails';
import { VecquickquotePage } from '../pages/vector/vecquickquote/vecquickquote';
import { VectornomineedeclarationPage } from '../pages/vector/vectornomineedeclaration/vectornomineedeclaration';
import { ReviewPage } from '../pages/vector/review/review';
import { VectordeclarationquestionPage } from '../pages/vector/vectordeclarationquestion/vectordeclarationquestion';
import { VectorHealthDeclarationPage } from '../pages/vector/vector-health-declaration/vector-health-declaration';
import { VectorVerifyDetailsPage } from '../pages/vector/vector-verify-details/vector-verify-details';
import { VectorpaymentmethodPage } from '../pages/vector/vectorpaymentmethod/vectorpaymentmethod';
import { VectorpaymentsuccessPage } from '../pages/vector/vectorpaymentsuccess/vectorpaymentsuccess';
import { QuickquoteOfflinePage } from '../pages/vector/quickquote-offline/quickquote-offline';
import { ProductlistingPage } from '../pages/productlisting/productlisting';
import { ProductLandingPage } from '../pages/product-landing/product-landing';
import { QuickqoutePage } from '../pages/personalAccident/quickqoute/quickqoute';
import { PremiumcalciPage } from '../pages/personalAccident/premiumcalci/premiumcalci';
import { PremiumlistviewPage } from '../pages/personalAccident/premiumlistview/premiumlistview';
import { ComponentsModule } from '../components/components.module';
import { AddCommasPipe } from '../pipes/add-commas/add-commas';
import { TbuyPage } from '../pages/topup/tbuy/tbuy';
import { TplandingPage } from '../pages/topup/tplanding/tplanding';
import { TBuyPageResultPage } from '../pages/topup/t-buy-page-result/t-buy-page-result';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { WaitingperiodsPage } from '../pages/health/waitingperiods/waitingperiods';
import { TpquickquotePage } from '../pages/topup/tpquickquote/tpquickquote';
import { TMedicaldeclarationPage } from '../pages/topup/t-medicaldeclaration/t-medicaldeclaration';
import { TNomineeDetailsPage } from '../pages/topup/t-nominee-details/t-nominee-details';
import { TProposerDetailsPage } from '../pages/topup/t-proposer-details/t-proposer-details';
import { TReviewPage } from '../pages/topup/t-review/t-review';
import { TVerifyDetailsPage } from '../pages/topup/t-verify-details/t-verify-details';
import { TpquickquoteResultPage } from '../pages/topup/tpquickquote-result/tpquickquote-result';
import { BuyPremiumlistviewPage } from '../pages/personalAccident/buy-premiumlistview/buy-premiumlistview';
import { BuypremiumcalciPage } from '../pages/personalAccident/buypremiumcalci/buypremiumcalci';
import { PaBuyHealthDeclarationPage } from '../pages/personalAccident/pa-buy-health-declaration/pa-buy-health-declaration';
import { PaBuyPolicyReviewPage } from '../pages/personalAccident/pa-buy-policy-review/pa-buy-policy-review';
import { PaBuyProposerDetailsPage } from '../pages/personalAccident/pa-buy-proposer-details/pa-buy-proposer-details';
import { PaNomineeDetailsPage } from '../pages/personalAccident/pa-nominee-details/pa-nominee-details';
import { PaPaymentMethodPage } from '../pages/personalAccident/pa-payment-method/pa-payment-method';
import { PaPaymentSuccessPage } from '../pages/personalAccident/pa-payment-success/pa-payment-success';
import { PaVerifyDetailsPage } from '../pages/personalAccident/pa-verify-details/pa-verify-details';
import { PremiumBuyPage } from '../pages/personalAccident/premium-buy/premium-buy';
import { TplandingofflinePage } from '../pages/topup/tplandingoffline/tplandingoffline';
//import { QuickqoutePage } from '../pages/health/quick-qoute/quick-qoute';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    LandingScreenPage,
    DashboardPage,
    DashboardListingPage,
    QuickQuotePage,
    QuickQuoteResultPage,
    BuyPage,
    BuyPageResultPage,
    BuyProposerDetailsPage,
    BuyHealthDeclarationPage,
    VerifyDetailsPage,
    BuyPolicyReviewPage,
    PaymentMethodPage,
    PaymentSuccessPage,
    HealthPolicyReviewPage,
    NomineeDetailsPage,
    DisclaimerPage,
    ToggleTextDirective,
    ProductBenefitsPage,
    MarketingColateralPage,
    SearchResultPage,
    PlansLimitsPage,
    HospitalLocatorPage,
    HospitalSearchResultPage,
    LocationmapPage,
    FaqPage,
    ViewProfilePage,
    OtpPasswdPage,
    GenOtpPage,
    ForgotpasswordPage,
    KnowledgebasePage,
    VecquickquotePage,
    VecproposerdetailsPage,
    VectornomineedeclarationPage,
    VectorHealthDeclarationPage,
    ReviewPage,    
    VectordeclarationquestionPage,
    VectorVerifyDetailsPage,
    VectorpaymentmethodPage,
    VectorpaymentsuccessPage,
    QuickquoteOfflinePage,
    ProductlistingPage,
    ProductLandingPage,
    QuickqoutePage,
    PremiumcalciPage,
    PremiumlistviewPage,
    AddCommasPipe,
    TbuyPage,
    TplandingPage,
    TBuyPageResultPage,
    WaitingperiodsPage,
    TpquickquotePage,
    TMedicaldeclarationPage,
    TNomineeDetailsPage,
    TProposerDetailsPage,
    TReviewPage,
    TVerifyDetailsPage,
    TpquickquoteResultPage,
    BuyPremiumlistviewPage,
    BuypremiumcalciPage,
    PaBuyHealthDeclarationPage,
    PaBuyPolicyReviewPage,
    PaBuyProposerDetailsPage,
    PaNomineeDetailsPage,
    PaPaymentMethodPage,
    PaPaymentSuccessPage,
    PaVerifyDetailsPage,
    PremiumBuyPage,
    PremiumcalciPage,
    PremiumlistviewPage,
    QuickqoutePage,
    TplandingofflinePage,
    TpquickquotePage,
    TpquickquoteResultPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    DirectivesModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    ComponentsModule,
    ChartsModule,
    IonicModule.forRoot(MyApp, {
      menuType: 'push',
      platforms: {
        android: {
          menuType: 'reveal',
        }
      }
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LandingScreenPage,
    DashboardPage,
    DashboardListingPage,
    QuickQuotePage,
    QuickQuoteResultPage,
    BuyPage,
    BuyPageResultPage,
    BuyProposerDetailsPage,
    BuyHealthDeclarationPage,
    VerifyDetailsPage,
    BuyPolicyReviewPage,
    PaymentMethodPage,
    PaymentSuccessPage,
    HealthPolicyReviewPage,
    NomineeDetailsPage,
    LandingScreenPage,
    DisclaimerPage,
    ProductBenefitsPage,
    MarketingColateralPage,
    SearchResultPage,
    PlansLimitsPage,
    HospitalLocatorPage,
    HospitalSearchResultPage,
    LocationmapPage,
    FaqPage,
    ViewProfilePage,
    OtpPasswdPage,
    GenOtpPage,
    ForgotpasswordPage,
    KnowledgebasePage,
    VecquickquotePage,
    VecproposerdetailsPage,
    VectornomineedeclarationPage,
    VectorHealthDeclarationPage,
    ReviewPage,   
    VectordeclarationquestionPage,
    VectorVerifyDetailsPage,
    VectorpaymentmethodPage,
    VectorpaymentsuccessPage,
    QuickquoteOfflinePage,
    ProductlistingPage,
    ProductLandingPage,
    QuickqoutePage,
    PremiumcalciPage,
    PremiumlistviewPage,
    TbuyPage,
    TplandingPage,
    TBuyPageResultPage,
    WaitingperiodsPage,
    TpquickquotePage,
    TMedicaldeclarationPage,
    TNomineeDetailsPage,
    TProposerDetailsPage,
    TReviewPage,
    TVerifyDetailsPage,
    TpquickquoteResultPage,
    BuyPremiumlistviewPage,
    BuypremiumcalciPage,
    PaBuyHealthDeclarationPage,
    PaBuyPolicyReviewPage,
    PaBuyProposerDetailsPage,
    PaNomineeDetailsPage,
    PaPaymentMethodPage,
    PaPaymentSuccessPage,
    PaVerifyDetailsPage,
    PremiumBuyPage,
    PremiumcalciPage,
    PremiumlistviewPage,
    QuickqoutePage,
    TplandingofflinePage,
    TpquickquotePage,
    TpquickquoteResultPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppService,
    InAppBrowser,
    DatePicker,
    Camera,
    Keyboard,
    CallNumber,
    Base64,
    NativeGeocoder,
    ScreenOrientation,
    Market,
    Device,
    FirebaseAnalytics
  ]
})
export class AppModule {}
