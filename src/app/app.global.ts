import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AppGlobals{
    readonly baseAPIUrl: string = 'https://fghealthapi.idealake.com/';
    
}