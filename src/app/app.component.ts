import { Component,ViewChild } from '@angular/core';
import { Platform,App,AlertController,Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/health/home/home';
import { Network } from '@ionic-native/network';
import { BuyPage } from '../pages/health/buy/buy';
import { BuyPageResultPage } from '../pages/health/buy-page-result/buy-page-result';
import { LoginPage } from '../pages/health/login/login';
import { LandingScreenPage } from '../pages/health/landing-screen/landing-screen';
import { DashboardPage } from '../pages/health/dashboard/dashboard';
import { DashboardListingPage } from '../pages/health/dashboard-listing/dashboard-listing';
import { QuickQuotePage } from '../pages/health/quick-quote/quick-quote';
import { QuickQuoteResultPage } from '../pages/health/quick-quote-result/quick-quote-result';
import { BuyProposerDetailsPage } from '../pages/health/buy-proposer-details/buy-proposer-details';
import { BuyHealthDeclarationPage } from '../pages/health/buy-health-declaration/buy-health-declaration';
import { VerifyDetailsPage } from '../pages/health/verify-details/verify-details';
import { BuyPolicyReviewPage } from '../pages/health/buy-policy-review/buy-policy-review';
import { PaymentMethodPage } from '../pages/health/payment-method/payment-method';
import { PaymentSuccessPage } from '../pages/health/payment-success/payment-success';
import { HealthPolicyReviewPage } from '../pages/health/health-policy-review/health-policy-review';
import { NomineeDetailsPage } from '../pages/health/nominee-details/nominee-details';
import { DisclaimerPage } from '../pages/health/disclaimer/disclaimer';
import { FaqPage } from '../pages/health/faq/faq';
import { SearchResultPage } from '../pages/health/search-result/search-result';
import { ProductBenefitsPage } from '../pages/health/product-benefits/product-benefits';
import { MarketingColateralPage } from '../pages/health/marketing-colateral/marketing-colateral';
import { PlansLimitsPage } from '../pages/health/plans-limits/plans-limits';
import { HospitalLocatorPage } from '../pages/health/hospital-locator/hospital-locator';
import { HospitalSearchResultPage } from '../pages/health/hospital-search-result/hospital-search-result';
import { LocationmapPage } from '../pages/health/locationmap/locationmap';
import { Events } from 'ionic-angular'
import { ViewProfilePage } from '../pages/view-profile/view-profile';
import { KnowledgebasePage } from '../pages/health/knowledgebase/knowledgebase';
import { ProductLandingPage } from '../pages/product-landing/product-landing';
import { TbuyPage } from '../pages/topup/tbuy/tbuy';
import { TplandingPage } from '../pages/topup/tplanding/tplanding';

@Component({ 
  templateUrl: 'app.html' 
})
export class MyApp { 

  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;   
  activePage: any;
  pageData;
  healthSubmenu:any = true;
  healthDownArrow:any = true;
  healthUpArrow:any = false; 
  loginData=[];
  username;
  showStatus = false;

  pages: Array<{title: string, component: any}>;
  constructor(public events: Events, private platform: Platform, public alertCtrl: AlertController, public  app: App,public statusBar: StatusBar,private network: Network,private splashScreen: SplashScreen) {
    statusBar.backgroundColorByHexString('#752127');
    console.log("loginState: " + sessionStorage.loginStatus);     
    events.subscribe('user:created', (user, time, status, loginStatus) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome', user, 'at', time);
      this.username = user;
      this.showStatus = loginStatus;
      console.log("state: " + this.showStatus);
      
    });
   
    this.loginData = sessionStorage.loginUserData;
    console.log("DAta: " + this.loginData);
    
 
    platform.ready().then(() => {
      /*  Okay, so the platform is ready and our plugins are available.
       Here you can do any higher level native things you might need. */
    
      setTimeout(() => {
        this.splashScreen.hide();
      }, 3000); 
      platform.registerBackButtonAction(() => {
        this.backButtonFunction();
      });
        this.network.onConnect().subscribe(()=>{
        
        });

        this.network.onDisconnect().subscribe(()=>{
      
        });

      this.pages = [
        {title : 'Health Insurance', component : HomePage},
        /*{ title: 'Dashboard', component: DashboardPage },
        { title: 'Marketing collaterals', component: DashboardPage },
        { title: 'Quick quote', component: DashboardPage },
        { title: 'Buy policy', component: DashboardPage },
        { title: 'Port Policy', component: DashboardPage },
        { title: 'Hospital list', component: DashboardPage },
        { title: 'FAQs', component: DashboardPage }*/
      ];
    });
  }

  backButtonFunction(){
    console.log("test");

    let nav = this.app.getActiveNavs()[0];
    console.log("ActivePage Name: " + this.nav.getActive().component.pageName);
    if(this.nav.getActive().component.pageName === "LoginPage" || this.nav.getActive().component.pageName === "LandingScreenPage") {
        console.log("testIF");
            this.closeAppFunc();
      }else{
        console.log("testELSE");
        if(this.nav.getActive().component.pageName === "QuickQuotePage" && (localStorage.userData && (JSON.parse(localStorage.userData).LoginStatus == true) || (sessionStorage.userData && JSON.parse(sessionStorage.userData).LoginStatus) )) {
          console.log("testNested");
          this.closeAppFunc();
        }
        if (nav.canGoBack()){ 
        
        } 
      }
    

  
  }

  closeAppFunc(){
    const alert = this.alertCtrl.create({
        title: 'Close Application',
        message: 'Do you want to close this application?',
        buttons: [{
            text: 'No',
            role: 'cancel',
            handler: () => {
            }
        },{
            text: 'Yes',
            handler: () => {
                this.platform.exitApp(); 
            }
        }]
    });
    alert.present();
  }

  toggleMenu(){
    this.healthSubmenu =! this.healthSubmenu;
    this.healthUpArrow =! this.healthUpArrow;
    this.healthDownArrow =! this.healthDownArrow;
  }



  /*openPage(page) {
    this.nav.setRoot(page.component);
    this.activePage = page;
  }*/

  /*checkActive(page){
    return page == this.activePage;
  }*/

  menuButton(){
    this.nav.push(LoginPage);
    }

    buttonTest(){
      this.nav.push(BuyPage);
    }

    dashboard(){
      this.nav.push(DashboardPage);
    }

    goTOBuyPolicy(){
      this.nav.push(ProductLandingPage, {loginStatus: this.showStatus, policyStatus: "online"});
    }

    goTOquickQuote(){      
      this.nav.push(ProductLandingPage, {loginStatus: this.showStatus, policyStatus: "offline"} );
    }

    goToHospitalList(){
      this.nav.push(HospitalLocatorPage, {gotoWhichPage: "hospital"});

    }

    home(){
      this.nav.push(LandingScreenPage);
    }

   goTOmarketingCollaterals(){
     this.nav.push(KnowledgebasePage);
  }  

  goToBranchLocator(){
    this.nav.push(HospitalLocatorPage, {gotoWhichPage: "branch"});
  }

  goToFaq(){
    this.nav.push(FaqPage);
  }

  viewProfile(){
    this.nav.push(ViewProfilePage);
  }
  
  //public username = "Sourov";
  
}

