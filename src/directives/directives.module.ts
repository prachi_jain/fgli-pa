import { NgModule } from '@angular/core';
import { CharOnlyDirective } from './char-only/char-only';
import { NumberOnlyDirective } from './number-only/number-only';
//import { ToggleTextDirective } from './toggle-text/toggle-text';
@NgModule({
	declarations: [CharOnlyDirective,
    NumberOnlyDirective],
	imports: [],
	exports: [CharOnlyDirective,
    NumberOnlyDirective
    ]
})
export class DirectivesModule {}
