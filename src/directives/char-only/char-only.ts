import { Directive,ElementRef, HostListener } from '@angular/core';

/**
 * Generated class for the CharOnlyDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[char-only]' // Attribute selector
})
export class CharOnlyDirective {

  constructor(private _el: ElementRef) { }
  @HostListener('input', ['$event']) onInputChange(event) {
  const initalValue = event.currentTarget.value
  if (initalValue != undefined) {
  event.currentTarget.value = initalValue.replace(/[^a-z A-Z]/g, '');
  }
  if (initalValue !== event.currentTarget.value) {
  event.stopPropagation();
  }
  }
  }