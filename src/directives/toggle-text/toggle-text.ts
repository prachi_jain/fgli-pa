import { NgModule, Directive, ElementRef, Input, Renderer2, AfterViewInit } from '@angular/core';

/**
* Generated class for the ToggleTextDirective directive.
*
* See https://angular.io/api/core/Directive for more info on Angular
* Directives.
*/
@NgModule({
	declarations: [ToggleTextDirective],
	imports: [],
	exports: [
    ]
})
@Directive({
selector: '[toggle-text]', // Attribute selector
host: {
'(ionChange)': 'handleChange($event)'
}
})
export class ToggleTextDirective {
@Input() dText: any
@Input() eText: any
constructor(public element: ElementRef, public renderer: Renderer2) {
}

ngAfterViewInit() {
var div = this.renderer.createElement('div');
this.renderer.addClass(div, 'toggleText')
var text = this.renderer.createText(this.dText);
this.renderer.appendChild(div, text);
this.renderer.appendChild(this.element.nativeElement, div);
}
handleChange(ev){
var targetDiv = this.element.nativeElement.querySelectorAll('.toggleText')
if(ev.checked){
targetDiv[0].innerHTML = this.eText 
}else{
targetDiv[0].innerHTML = this.dText
}
}
}