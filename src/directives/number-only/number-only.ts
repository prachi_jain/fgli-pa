import { Directive, ElementRef, HostListener } from '@angular/core';

/**
* Generated class for the NumberOnlyDirective directive.
*
* See https://angular.io/api/core/Directive for more info on Angular
* Directives.
*/
@Directive({
selector: '[number-only]' // Attribute selector
})
export class NumberOnlyDirective {

constructor(private _el: ElementRef) { }

@HostListener('input', ['$event']) onInputChange(event) {
const initalValue = event.currentTarget.value
if (initalValue != undefined) {
event.currentTarget.value = initalValue.replace(/[^0-9]*/g, '');
}
if (initalValue !== event.currentTarget.value) {
event.stopPropagation();
}
}

}
