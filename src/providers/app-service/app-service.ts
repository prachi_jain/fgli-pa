import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams,ToastController,LoadingController,MenuController   } from 'ionic-angular';

//import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Rx';
import {Network} from '@ionic-native/network';
import { Http } from "@angular/http";

import { NativeGeocoder,
  NativeGeocoderReverseResult,
  NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
  import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
/*
  Generated class for the BootstrapServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

//const appAPI = "https://fghealthapi.idealake.com/";
//const fgiStateApi = "https://general.futuregenerali.in/";

const appAPI = "https://i-insureuat.futuregenerali.in/";
//const fgiStateApi = "https://general.futuregenerali.in/";

//production URL
//const appAPI = "https://i-insure.futuregenerali.in/";


declare var CryptoJS: any;
;
@Injectable()
export class AppService {

  addIndividualDiscont:number; 
  lessSmokingLoading:number;
  showloading;
  canShowToast = true;
  month_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
  duration = ["1","2","3"];
  installments = ["FULL","MONTHLY","QUARTERLY","HALFYEARLY"];
  client= {"ClientType":"I","CreationType":"C","Salutation":"MRS","FirstName":"","LastName":"","DOB":"03-12-1998","Gender":"F","MaritalStatus":"M","Occupation":"","PANNo":"","GSTIN":"","AadharNo":"","CKYCNo":"","EIANo":"","Address1":{"AddrLine1":"","AddrLine2":"","AddrLine3":"","Landmark":"","Pincode":"401107","City":"","State":"","Country":"IND","AddressType":"R","HomeTelNo":"","OfficeTelNo":"","FAXNO":"","MobileNo":"","EmailAddr":""}}
  DiseaseMedicalHistory={"DiseaseMedicalHistory":{"PreExistingDiseaseCode":"","MedicalHistoryDetail":""}};
  online;
  ClientDetails=[];

  constructor(private platform: Platform, public fba: FirebaseAnalytics, private _http: Http, private _GEOCODE  : NativeGeocoder, private network: Network, public http: HttpClient,public loadingCtrl: LoadingController,public toast: ToastController) {
    //console.log('Hello BootstrapServiceProvider Provider');

    // platform.ready().then(() => {
    //   /*  Okay, so the platform is ready and our plugins are available.
    //    Here you can do any higher level native things you might need. */
    //    let type = this.network.type;
     
    //    //console.log("Connection type: ", this.network.type);
    //   // Try and find out the current online status of the device
    //   if(type == "unknown" || type == "none" || type == undefined){
    //     //console.log("The device is not online");
    //     this.online = false;
    //   }else{
    //     //console.log("The device is online!");
    //     this.online = true;
    //   }
    // });

    // this.network.onDisconnect().subscribe( () => {
    //   this.online = false;
    //   console.log('network was disconnected :-(');
    // });

    // this.network.onConnect().subscribe( () => {
    //   this.online = true;
    //   console.log('network was connected :-)');
    // });
   
  }

  logButton(name:string,value:any){
    this.fba.logEvent(name, { pram:value })
    .then((res: any) => {console.log(res);})
    .catch((error: any) => console.error(error));
  }

  presentServiceLoading() {
    this.showloading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss : false,
      dismissOnPageChange : true
    });
    this.showloading.present();
  }

  encryptData(request,secretkey){
    //console.log("DataToEncrypt : "+JSON.stringify(request));
    var seckey = secretkey!="" && secretkey!=null ? secretkey : '8080808080808080';
    var seckey16 = seckey.substring(0,16);
    var key = CryptoJS.enc.Utf8.parse(seckey16);
    var iv = CryptoJS.enc.Utf8.parse(seckey16);
    var encryptedData = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(request), key,
    {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return encryptedData.toString();
  }

  callService(serviceName,data,headerData){
     // if(this.network.type != "none"){
        if(headerData == ''){
          return this.http.post(appAPI+serviceName,data).pipe(
            map(this.extractData),
            catchError(this.handleError)
          );
        }else{
          return this.http.post(appAPI+serviceName,data, {headers: new HttpHeaders().set('FGHToken', headerData)}).pipe(
            map(this.extractData),
            catchError(this.handleError)
          );
        }
    // }else{
    //   /* NO internet access */
    //   this.showloading.dismiss();
    //   this.showToast("You need internet access to proceed.");
    // }
  }

  logoutUser(){
    
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getBase64string(str){
    return btoa(str);
  }
  
  private handleError (error: Response | any) {
    this.showloading.dismiss();
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    this.showloading.dismiss();

    return Observable.throw(errMsg);
  }

  showToast(Message){
    if(this.canShowToast){
      let pageToast = this.toast.create({
        message:Message,
        showCloseButton: true,
        closeButtonText: 'OK',
        dismissOnPageChange: true,
        position: "bottom",
        duration: 5000,
      });
      this.canShowToast = false;
      pageToast.present();
      pageToast.onDidDismiss(() => {
        this.canShowToast = true;
      });
    }   
  }

  returnAmountTitle(amount,type){
    let amtQ = amount/100000;
    let amtR = amount%100000;
    if(amtR == 0 && amtQ <100 && type == 1){
      return amtQ+"L";
    }else if(amtR == 0 && amtQ <100 && type == 3){
      return amtQ+"L";
    }else if(amtR == 0 && amtQ <100 && type == 2){
      return amtQ+" Lakhs";
    }else if(amtQ >=100 && type == 2){
      return amtQ/100+" Crore";
    }else if(amtQ >=100 && type == 1){
      return amtQ/100+"Cr";
    }else{
      return amtQ/100+"Cr";
    }
  }

  getSumInsuredArray(apiPlanType){
    let SumInsuredArray =[];
    //console.log(this.QuickQuotePlans);
    for(let a = 0 ; a < apiPlanType.length;a++){
      if(apiPlanType[a].isquote){
        for(let b = 0; b< apiPlanType[a].suminsured.length;b++ ){
          SumInsuredArray.push({
          "planIndex" : a,
          "plancode":apiPlanType[a].plancode,
          "title":apiPlanType[a].title,
          "sicode":apiPlanType[a].suminsured[b].sicode,
          "suminsured":apiPlanType[a].suminsured[b].suminsured,
          "amtTitle" : "₹"+this.returnAmountTitle(apiPlanType[a].suminsured[b].suminsured,1),
          "amtTitle2" : "₹ "+this.returnAmountTitle(apiPlanType[a].suminsured[b].suminsured,3),
          "amtSelectedTitle" : this.returnAmountTitle(apiPlanType[a].suminsured[b].suminsured,2)})
        }
      }
    }
    return SumInsuredArray;
  }
  getMemberCalculationAmount(baseValue, smokingState, smokingLoad, indivDiscount){
       if(smokingState == true){
        this.addIndividualDiscont = parseInt(baseValue) - (baseValue * indivDiscount)/100;
        this.lessSmokingLoading =  this.addIndividualDiscont + (this.addIndividualDiscont * smokingLoad)/100;
         return this.lessSmokingLoading;
       }else{
        this.addIndividualDiscont = parseInt(baseValue) - (baseValue * indivDiscount)/100;
        return this.addIndividualDiscont;
       }
  }

  getMemberBaseFloaterValue(member ,baseValue, smokingState, familyDiscount, smokingLoad){
    console.log("test:" + member);
    
     if(member == 'self'){
      if(smokingState){
        return  (baseValue * ((100+ parseFloat(smokingLoad))/100));
      }else{
        return baseValue;
      }
    }else if(member == "spou"){
      if(member == "spou" && smokingState){
        return (baseValue*((100- parseFloat(familyDiscount))/100)* ((100+ parseFloat(smokingLoad))/100));
      }else{
        return (baseValue*((100- parseFloat(familyDiscount))/100));
      }
      
    }else if(member == "fath"){
      if(member == "fath" && smokingState){
        return (baseValue*((100- parseFloat(familyDiscount))/100)* ((100+ parseFloat(smokingLoad))/100));
      }else{
        return (baseValue*((100- parseFloat(familyDiscount))/100));
      }
    }else if(member == "moth"){
      if(member == "moth" && smokingState){
        return (baseValue*((100- parseFloat(familyDiscount))/100)* ((100+ parseFloat(smokingLoad))/100));
      }else{
        return (baseValue*((100- parseFloat(familyDiscount))/100));
      }
    }else if(member == "son"){
      return (baseValue*((100- parseFloat(familyDiscount))/100));
    }else if(member == "daug"){
      return (baseValue*((100- parseFloat(familyDiscount))/100));
    }

  }

  callServiceDetails(PolicyType,beneficiaryMembers){
    console.log("install: " + this.installments);  
      let enqReq = {
        "Duration":this.duration,
        "Installments":this.installments,
        "RefrenceNo" : "",
        "Client":this.client,
        "Risk":{
        "PolicyType":  PolicyType,
        "Duration": "1",
        "Installments": "FULL",
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails" : beneficiaryMembers
        }
    }
    return enqReq;
  }

  callServiceForFirstTimeDetails(PolicyType,beneficiaryMembers, memberInsured){
    console.log("install: " + this.installments);
    console.log("memberApp: " + JSON.stringify(memberInsured));
      let enqReq = {
        "Duration":this.duration,
        "Installments":this.installments,
        "RefrenceNo" : "",
        "Client":this.getClientNode(memberInsured),
        "Risk":{
        "PolicyType":  PolicyType,
        "Duration": "1",
        "Installments": "FULL",
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails" : beneficiaryMembers
        }
    }
    return enqReq;
  }

  callServicePADetails(PolicyType,beneficiaryMembers){
    console.log("install: " + this.installments);  
      let enqReq = {
        "Duration":this.duration,
        "Installments":this.installments,
        "RefrenceNo" : "",
        "ProductType": "pa",
        "Client":this.client,
        "Risk":{
        "PolicyType":  PolicyType,
        "Duration": "1",
        "Installments": "FULL",
        "IsfgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails" : beneficiaryMembers
        }
    }
    return enqReq;
  }
  

  callServiceForTopup(PolicyType,beneficiaryMembers, memberInsured){
    console.log("install: " + this.installments);
    console.log("memberApp: " + JSON.stringify(memberInsured));
      let enqReq = {
        "Duration":this.duration,
        "Installments":this.installments,
        "ProductType": "topup",
        "RefrenceNo" : "",
        "Client":this.getClientNodeTopUp(memberInsured),
        "Risk":{
        "PolicyType":  PolicyType,
        "Duration": "1",
        "Installments": "FULL",
        "IsFgEmployee": sessionStorage.IsFGEmployee,
        "IsPos" : sessionStorage.Ispos,
        "BeneficiaryDetails" : beneficiaryMembers
        }
    }
    return enqReq;
  }


  getClientNodeTopUp(memberInsured){
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType":"C",
      "Salutation":"MR",     
      "FirstName":"",
      "LastName":"",
      "DOB":memberInsured[0].age,
      "Gender":"M",      
      "MaritalStatus":memberInsured[0].maritalCode,
      "Occupation":"",
      "PANNo":"",
      "GSTIN":"",
      "AadharNo":"",
      "CKYCNo":"",
      "EIANo":"",
      "Address1":{
        "AddrLine1":"",
        "AddrLine2":"",
        "AddrLine3":"",
        "Landmark":"",
        "Pincode":"",
        "City":"",
        "State":"",
        "Country":"IND",
        "AddressType":"R",
        "HomeTelNo":"",
        "OfficeTelNo":"",
        "FAXNO":"",
        "MobileNo":"",
        "EmailAddr":""
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  }

  getClientNode(memberInsured){
    this.ClientDetails.push({
      "ClientType":"I",
      "CreationType":"C",
      "Salutation":"MR",
      "FirstName":"",
      "LastName":"",
      "DOB":memberInsured[0].age,
      "Gender":"M",      
      "MaritalStatus":memberInsured[0].maritalCode,
      "Occupation":"",
      "PANNo":"",
      "GSTIN":"",
      "AadharNo":"",
      "CKYCNo":"",
      "EIANo":"",
      "Address1":{
        "AddrLine1":"",
        "AddrLine2":"",
        "AddrLine3":"",
        "Landmark":"",
        "Pincode":"",
        "City":"",
        "State":"",
        "Country":"IND",
        "AddressType":"R",
        "HomeTelNo":"",
        "OfficeTelNo":"",
        "FAXNO":"",
        "MobileNo":"",
        "EmailAddr":""
      }
    });
    return this.ClientDetails[this.ClientDetails.length -1];
  }

  getStateFGIAPiCall(){  
      let url = 'https://general.futuregenerali.in/networkhospitals.asmx/GetState';
      return this._http.get(url).map(res => res.json());
  }

  getCityFGIAPiCall(stateid){
   
      let url = 'https://general.futuregenerali.in/networkhospitals.asmx/GetCityAccToState' + '?' + 'stateid=' + stateid;
      return this._http.get(url).map(res => res.json());
        
  }

  getHospitalNameApiCall(hospitalName, pageName){
    if(pageName == "hospital"){
      let url = 'https://general.futuregenerali.in/networkhospitals.asmx/GetLocatorsAccToSearch' + '?' + 'workshopname=' + hospitalName;
      return this._http.get(url).map(res => res.json());
    }else{
      let url = 'https://general.futuregenerali.in/BranchLocators.asmx/GetLocatorsAccToSearch' + '?' + 'workshopname=' + hospitalName;
      return this._http.get(url).map(res => res.json());
    }   
  }

  getHospitalDataByStateCity(cityID, pageName){
    if(pageName == "hospital"){
      let url = 'https://general.futuregenerali.in/networkhospitals.asmx/GetLocatorsAccToStateAndCity'+ '?' + 'cityid=' + cityID;
      return this._http.get(url).map(res => res.json());
    }else{
      let url = 'https://general.futuregenerali.in/BranchLocators.asmx/GetLocatorsAccToStateAndCity'+ '?' + 'cityid=' + cityID;
      return this._http.get(url).map(res => res.json());
    }   
  }

  getHospitalByState(pageName){
    console.log("appState");
    if(pageName == "hospital"){
      let url = 'https://general.futuregenerali.in/networkhospitals.asmx/GetAllDataToDownload';
      return this._http.get(url).map(res => res.json());
    }else{
      let url = 'https://general.futuregenerali.in/BranchLocators.asmx/GetAllDataToDownload';
      return this._http.get(url).map(res => res.json());
    }       
  }

  getHospitalByStateCityHospitalName(stateID, cityID, hospitalName, pageName){   
    if(pageName == "hospital"){
      let url = 'https://general.futuregenerali.in/networkhospitals.asmx/GetHospitalAccToSearch?' + 'stateid=' + stateID + '&' + 'cityid=' + cityID + '&' + 'HospitalName=' + hospitalName;
      return this._http.get(url).map(res => res.json());
    }else{
      let url = 'https://general.futuregenerali.in/BranchLocators.asmx/GetHospitalAccToSearch?' + 'stateid=' + stateID + '&' + 'cityid=' + cityID + '&' + 'HospitalName=' + hospitalName;
      return this._http.get(url).map(res => res.json());
    }  
  }
  

}
