import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the AddCommasPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'addCommas',
})
export class AddCommasPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(x) {
    console.log("From Pipe:  "+x);
    if (x != undefined) {
        var isNegative = false;
        x = x.toString();
        if (x.indexOf("-") != -1) {
        x = x.slice(1)
        isNegative = true
        }
        var afterPoint = '';
        if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
        x = Math.floor(x);
        x = x.toString();
        var lastThree = x.substring(x.length - 3);
        var otherNumbers = x.substring(0, x.length - 3);
        if (otherNumbers != '')
        lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
        if (isNegative) {
        res = "-" + res
        }
        return res;
        } else {
        return null;
        }
    // return value;
  }

  // return function (x) {
  //   if (x != undefined) {
  //   var isNegative = false;
  //   x = x.toString();
  //   if (x.indexOf("-") != -1) {
  //   x = x.slice(1)
  //   isNegative = true
  //   }
  //   var afterPoint = '';
  //   if (x.indexOf('.') > 0)
  //   afterPoint = x.substring(x.indexOf('.'), x.length);
  //   x = Math.floor(x);
  //   x = x.toString();
  //   var lastThree = x.substring(x.length - 3);
  //   var otherNumbers = x.substring(0, x.length - 3);
  //   if (otherNumbers != '')
  //   lastThree = ',' + lastThree;
  //   var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
  //   if (isNegative) {
  //   res = "-" + res
  //   }
  //   return res;
  //   } else {
  //   return null;
  //   }
    
  //   }
}
