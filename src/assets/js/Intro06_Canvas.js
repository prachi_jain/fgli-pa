(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 320,
	height: 480,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"assets/imgs/Shield.png?1541404383509", id:"Shield"}
	]
};



// symbols:



(lib.Shield = function() {
	this.initialize(img.Shield);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,205,256);


(lib.mc_Star = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-2.2,-4.8,0,-2.2,-4.8,62.4).s().p("AgVCSIlAFUIEEl8Ij6BQIDXiCInCh+IHWBEIhzjSICiCoIgHnFIBOG9IBzjcIg3DSID6iHIj1DDIHggKIngBOIDIB9Ijhg5IANFsg");
	this.shape.setTransform(0.7,-2.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.1,-52.4,113.5,99.7);


(lib.mc_Shield = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Shield();
	this.instance.setTransform(-102.5,-128);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-102.5,-128,205,256);


(lib.mc_Gradient = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","#FFFFFF","rgba(255,255,255,0)"],[0,0.341,0.525,1],8.2,-3.6,-8.7,-0.6).s().p("AhjgeIC6ghIANBeIi6Ahg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10,-6.5,20.1,13);


(lib.mc_Glow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],0,0,0,0,0,16.7).s().p("Ah0B0QgvgvAAhFQAAhDAvgxQAxgvBDAAQBEAAAwAvQAwAxAABDQAABFgwAvQgwAwhEAAQhDAAgxgwg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.5,-16.5,33,33);


(lib.ilogo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQATQgGgKAAgJQAAgLAGgHQAIgJAIAAQALAAAGAIQAGAGAAAMIgjAAQAAAIADAGQAGAIAHAAQAHABAMgKQgBAHgHAFQgFAEgIAAQgKAAgIgJgAgHgRQgEAIABAEIAYAAQAAgFgCgGQgEgGgFAAQgFAAgFAFg");
	this.shape.setTransform(63,22.9,1.605,1.605);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAUQgIgIABgMQgBgKAIgIQAHgIAJgBQAIAAAJAGIgBAQQgCgIgDgEQgEgFgGAAQgGAAgFAIQgEAGAAAIQAAAHAFAIQAGAJAGAAQAKgCAIgIQgGANgPAAQgJAAgHgHg");
	this.shape_1.setTransform(54.5,23,1.605,1.605);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgWAbIACgCIAAgrQAAgBAAgBQAAAAAAgBQAAAAgBAAQAAAAAAAAIgDgDIANgDIAAAKQAJgJAIAAQAJAAAFAIQACADgBAPIAAAYIACAAIACABIgOAAIACgCIAAgVQAAgMgBgDQgDgJgJAAQgGAAgFAIIAAAmQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAg");
	this.shape_2.setTransform(45.4,23.4,1.605,1.605);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPAYQgGgFAAgFQAAgHAGgFIAFgCQAEgCADAAIAKgEIAAgCQAAgHgCgDQgBgEgGAAQgLAAgFASIgBgQQAHgHALAAQAKAAAFAIQABAGAAANIAAATQAEAFADAAIgJADQgCAAgDgGQgFAHgHAAQgIAAgDgEgAAAABIgIAEQgEACAAAGIACAGQAFADACAAQAFABAGgGIAAgRg");
	this.shape_3.setTransform(37.2,23,1.605,1.605);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgMAbIADgCIAAgsQAAAAgBgBQAAAAAAgBQAAAAAAAAQAAAAAAAAIgEgCIAOgEIAAANQAFgLAFAAIAFACIAAAGIgHgCQgGgBgCAJIAAAjQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAAAg");
	this.shape_4.setTransform(30.6,23,1.605,1.605);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgQAVQgDgHAAgLIAAgVQgBgFgFgCIANgDIAAAgQABAGACAGQACAHAHAAQAFAAAHgHIAAgjQAAgBAAgBQAAAAAAgBQAAAAAAAAQAAAAgBAAIgCgDIAMgDIAAAyQAAAAAAAAQAAAAAAABQAAAAAAAAQABAAAAABIAEAAIgLADQgEgDABgEQgIAIgHABQgKgBgDgHg");
	this.shape_5.setTransform(23.4,23,1.605,1.605);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPAXIAAgQIADALQAEAGAGAAQAFAAABgDQAEgDAAgFQAAgEgHgEIgKgFQgHgFAAgGQAAgGAFgFQAFgFAGAAQAGAAAGAFIAAAOQgBgDgDgHQgDgFgFAAQgIAAAAAJQAAAGAHAEIALAFQAHAEAAAGQAAAHgFAFQgFAFgHAAQgFAAgKgFg");
	this.shape_6.setTransform(15.2,23.1,1.605,1.605);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgWAbIACgCIAAgrQAAgBAAgBQAAAAAAgBQAAAAgBAAQAAAAAAAAIgDgDIANgDIAAAKQAJgJAIAAQAKAAADAIQADADAAAPIAAAYIABAAIACABIgOAAIACgCIAAgVQAAgMgBgDQgDgJgJAAQgGAAgFAIIAAAmQAAACADABg");
	this.shape_7.setTransform(7.2,23.4,1.605,1.605);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgIAoIAEgDIABgFIAAhCQAAgDgEgCIAQAAQgDACgBABIgBAFIAABCQAAADAEACg");
	this.shape_8.setTransform(0.6,21,1.605,1.605);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgQATQgGgKAAgJQAAgLAGgHQAIgJAIAAQALAAAGAIQAGAGAAAMIgiAAQAAAIADAGQAGAIAHAAQAGABANgKQgCAHgHAFQgFAEgJAAQgJAAgIgJgAgHgRQgEAIABAEIAYAAQAAgFgCgGQgEgGgFAAQgEAAgGAFg");
	this.shape_9.setTransform(-9.4,22.9,1.605,1.605);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgFAbIADgBIAAgYQAAgLgCgEQgCgIgKgBQgHABgFAHIAAAmQAAAAAAABQAAAAAAABQABAAAAABQABAAABAAIgPAAIADgBIAAgsQAAgBgBgBQAAAAAAgBQAAAAAAAAQAAgBAAAAIgEgCIAOgCIAAAKQAKgKAJABQAJgBADAKQAJgKAMABQAKgBADAJQAEAFAAAMIAAASIAAAGQgBAAAAAAQgBABABAAQAAABABAAQAAAAACABIgPAAIACgCIAAgXQAAgMgBgDQgDgJgIAAQgJAAgEAIIAAAmQAAAAAAABQAAAAAAABQABAAAAABQABAAABAAg");
	this.shape_10.setTransform(-21.3,23.1,1.605,1.605);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgFApIACgCIAAgsQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAAAgBAAIgCgCIAKgDIAAA0QABAAAAAAQAAAAAAAAQABAAAAABQABAAABABgAgDgeQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBAAAAgBQAAAAABgBQAAAAAAgBQAAAAABgBQAAgBAAAAQABgBAAAAQABAAABgBQAAAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAABAAAAQABAAAAABIACAEIgCAEIgDABIgDgBg");
	this.shape_11.setTransform(-30.4,20.9,1.605,1.605);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgIAWQgBgDAAgQIAAgUIgFAAIANgPIAAALIANAAIAAAEIgNAAIAAAlQABAJAGAAIAJgEIgEAGQgCACgFAAQgJAAgDgLg");
	this.shape_12.setTransform(-34.2,22.3,1.605,1.605);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRApIAEgEIAEgGIAIgTIgQgtIgDgEIgFgCIASAAQAAABgBAAQAAAAgBABQAAAAAAAAQAAABAAAAIAKAkIAQgjQAAAAgBgBQAAgBAAAAQgBAAgBgBQAAAAgBAAIAKAAIgDADIgcBMgAAXgmIADgCIAAACgAAXgmg");
	this.shape_13.setTransform(-41.5,25.2,1.605,1.605);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgWAbIACgCIAAgrQAAgBAAgBQAAAAAAgBQAAAAgBAAQAAAAAAAAIgDgDIANgDIAAAKQAJgJAIAAQAKAAADAIQACADAAAPIAAAYIACAAIACABIgOAAIACgCIAAgVQAAgMgBgDQgDgJgJAAQgGAAgFAIIAAAmQAAAAAAABQAAAAABABQAAAAABABQAAAAABAAg");
	this.shape_14.setTransform(-49.4,23.4,1.605,1.605);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AARAoQADgCAAgCIgIgUIgcAAIgJAVQAAAAAAABQAAAAABAAQAAABABAAQAAABABAAIgNAAQAEgCADgIIAchFIAdBJQACADAFADgAgNAJIAXAAIgKgdg");
	this.shape_15.setTransform(-59.8,21,1.605,1.605);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgDAjQgPgEgHgKQgGgJAAgMIAAgDQADgQAKgIQAIgHAKAAIAEAAQAPABAHANQAGAJAAALIAAAEQgBAOgMAKQgIAHgLAAg");
	this.shape_16.setTransform(-51.8,5.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgzCYQgHgIgEgKQgCgKAAgJQAAgVAFggQAGgeAKgfIAPgxQAUgIAPAAQAGAAAIACQAHABAEAFQgKAXgFAYQgJAYgIAgIgCAyIABAIQABAEABAAQAKAAANgTQAIgMAQgeQAOgbADgOQAAAEgEARQgGAUgIARQgGASgOAWQgNAVgNAMQgLANgQAAQgQAAgJgHgAAXhXQgOgCgJgMQgDgJgBgMIAAgFQAEgQAIgIQAIgHAMAAIAFAAQAPADAGALQAHAJAAAMIAAAFQgDAPgLAJQgIAHgMAAg");
	this.shape_17.setTransform(-61.9,-6.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AhbBsIgLgDQgYgGgHgEQgPgJgPgYIgEgiQAAgQADgMQANhBA/giQAUgLAVAFQAaAFAMAWQAJAWgFAbQgJAdgXAOQgYAQgegEIgJgBQgIAVATAQQAYAUAigNQALgFANgNQAFgEASgXQAlgsAxAOQAyAMAMA7IAEARIAAAMIgXglQgVgcgagHQgbgGgbARQgQAJgUAXQgVAYgVAJIgdAKgAgwhTQgEABgHAEQgaAUgLAkIgFAYIAMgCQAkgKASgwQAGgQgJgGQgDgDgEAAIgDAAg");
	this.shape_18.setTransform(51.1,-1.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("Ag4BkQgQAAgKgKQgKgMAAgTQAAgXAJgnQAKgtAOgjQAHgKAUgDIAIAAQAPgBAJAIQgGAKgJAcQgHAXgJAkQgHAdAAARIAAABQAAAGACABIAFgVQADgRAGgQQAFgTAHgQQAKgZAEgKQAJgSAMgIQANgKANAAQAKAAAIAFQAIAHAAAMQAAAGgDAKIgGATIgGATQgDAKAAAGQAAAIAEAAQAFAAAGgEIAMgQIAHgNQAAAJgGAMQgGAJgKAKQgMALgMgBIgBAAQgMAAgJgFQgJgHAAgMQAAgGAFgWQAEgQANgWQgJgBgIANQgHAKgJAWIgMAkQgFAPgGAaIgIAgQABABgCAHQgOAIgOAAg");
	this.shape_19.setTransform(26.7,-2.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AhgBbQgKgLAAgUQAAgPAGggQAHglAGgWQAGgYAIgUQAHgJARgBIACAAQASABAOAIIgHAYIgQA2QgHAagFAeIAAAGQAAAMAFAAQAFABAJgTQAIgOAKgYQAMgcADgPIARgzQAQgGASAAQAEAAAIACQAGABADAFIgOAqQgJAhgEASQgEAlAAAIIABAHQABADAEAAQAGAAAIgLQALgOAHgOIAQgcQAGgMAEgMIgFATQgFAOgIASQgJASgKATQgKAQgNANQgMANgNAAQgPgBgHgGQgHgJgBgHQgDgKAAgIQAAgOADgRIgTAjQgLARgKAJQgLAMgMAAQgQgBgJgJg");
	this.shape_20.setTransform(9.3,-2.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AhABlQgJgIAAgOQAAgeAhghIgEgIQgGgQgCgKQgDgOAAgPQAAgUAJgPQAHgOAMgGQANgGAMAAQAMAAAJAHQALAHAAAMQgBALgEAGQgFAIgFAAQADgGAAgIQAAgFgDgFQgDgFgGAAQgEAAgDAEQgDAEAAAKQAAAPADANIAFAaQArgZAaAAQgfAIgkAaIAHATQACAMAAAMQAAAhgQATQgQATgbAAQgNAAgHgIgAg4BHQABAQAHAAQAHAAAEgLQADgKAAgKQAAgRgDgMQgUAZABATg");
	this.shape_21.setTransform(-5,-1.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AACBYQgHgLAAgTQgEglAYhKQgMAIgKAcQgFAIgFALQgHARgHATQgKAZgHAcQgOAIgOAAQgQgBgLgKQgJgLAAgTQAAgXAJgnQAJgrAPglQAIgOAcABQAOgBAKAIQgNAVgOAuQgMAngCAeQAAAPAFAAIAKghQAGgTAMgYQAKgUALgRQALgVAOgMQAQgNAPABQAIAAAKAEQAIAHAAALQAAAKgJAdQgSA5AAAcQAAAMAGAAQAGAAAIgMQAKgNAIgPIAQgcQAGgNAEgMIgFATIgNAgIgTAlQgKARgNAOQgMALgNABQgQgBgKgKg");
	this.shape_22.setTransform(-20.8,-2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AggCkQAAgwAThYIARhHIAUhDIAGgXIAAgEQgCgCgEAAQgNAAgOALQgOALgLARQgJAQgJATQgGARAAAQQAAAQAFAMQAHALAMAJIgBAAIgKAEQgFACgHAAQgOAAgIgGQgGgGgGgKQgDgGAAgLQAAgbANgcQARgeAUgTQAVgUAcgPQAagNAbAAQALAAAHAFQAFAEABAIIACAJIgCAIQgBAKgCAHQgNAjgLAnQgLAmgMA4QgOA/AAArQgJAKgSADIgIABQgOAAgHgGg");
	this.shape_23.setTransform(-38.9,-8.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-68.5,-25.4,136.5,57.4);


(lib.mc_Light = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mc_Gradient();
	this.instance.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9.9,-6.5,20,13);


// stage content:
(lib.Intro06_Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// i logo
	this.instance = new lib.ilogo("synched",0);
	this.instance.setTransform(160,66);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(170));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgrBIIgZgDQgLgCAKgFQALgFALgTIAAAAQASg5gjg0QBWARACBKIADAAIASgBIAQgBQAJADADAHQADAHgCAEQgKgGgOAGIgTALQgOAKgKAEQgRAIgZAAIgIAAg");
	this.shape.setTransform(66.7,146.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FDF8F5").s().p("AhFAxQgNgBAKgGQAKgGANgVIACgCQAGgigZgbQBHgJARAzIAGAAIASgCQAKgCAGAAQAKADAEAHQACAFgBAEQgLgFgOAHQgIADgLAKQgOALgKAEQgWALgeAAIgagBg");
	this.shape_1.setTransform(66.5,145.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FDF3EC").s().p("AhGAjQgQAAALgHQAKgGANgWIACAAQgCgMgLgGQA0gdAYAUIAOAAIAUgEQAKgDAGAAQAKACAFAIQADAHgBAEQgMgFgPAJQgHADgMAJQgNAMgLAFQgVANghACIgSABIgIgBg");
	this.shape_2.setTransform(66.1,143.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FEEEE3").s().p("AhOAhQAKgHAPgZIABgBQgCgDgEgBQAkgbAYAEIAZgDIAUgFIARgEQALACAEAIQAFAHgCAEQgMgEgPAJQgHAEgMAKQgOANgLAHQgVANghAEIgcACIgCAAQgOAAAJgHg");
	this.shape_3.setTransform(65.3,140);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FEE9DA").s().p("AhQAmQAKgHAOgbQAUgaAbgFIAjgGQAJgCALgEQALgEAHgBQAKABAGAJQAFAGgBAFQgNgEgPALQgIAFgLALQgOANgKAHQgWAQgjAFIgcAEIgEAAQgMAAAIgHg");
	this.shape_4.setTransform(64.6,136.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FDE2D0").s().p("AhRAqQAKgJAMgbIACgCQgDgEgFAAQAmgiAcAEIAYgGIAVgGQALgFAGgBQAMABAGAIQAFAGgBAGQgNgEgPALQgIAGgMANQgNANgLAIQgWARgjAIQgQADgOABIgFAAQgKAAAIgHg");
	this.shape_5.setTransform(63.7,133.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FCDCC7").s().p("AhRAuQALgJAJgdIAAgBQgDgMgOgFQA2gsAhATIAPgEIAVgHQALgFAHgCQAMABAHAIQAFAGgBAGQgNgDgQAMQgHAGgMAOQgNAPgLAIQgWATglAJQgQAEgOABIgFABQgKAAAJgIg");
	this.shape_6.setTransform(62.9,129.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FBD7BE").s().p("AhIA6QALgKAHgdIABgCQgEgtgngZQBWgjAjA6IAHgCQAKgDAMgGIASgHQANAAAHAIQAGAGgBAGQgOgDgQAOQgHAGgMANQgNASgLAJQgYAUgjALQgSAEgOACIgGABQgIAAAJgJg");
	this.shape_7.setTransform(60.9,125.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FAD0B4").s().p("Ag4BTQALgMAEgcIABgBQgFhQhEgxQB1gVAlBgIAFgCIAWgJQAMgHAHgBQANgBAIAIQAGAHAAAFQgPgCgQAOQgHAFgMAQQgNATgLAKQgYAWgkAMQgSAFgPADIgEAAQgIAAAJgJg");
	this.shape_8.setTransform(58.1,120.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FBCBAB").s().p("Ag2BVQALgMAEgeIABgBQgIhShIgxQB5gYApBhIAFgCIAXgKQAMgHAHgDQANgBAJAIQAGAHAAAGQgQgCgPAPQgIAGgLARQgNATgLALQgYAXglAOQgSAGgQADIgFABQgIAAAJgKg");
	this.shape_9.setTransform(56.4,117.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FBC6A2").s().p("AhHBCQALgMAGghIACgEQgKgwgsgXQBaguAsA7IAHgDQALgFAMgHQAMgHAIgDQANgCAJAJQAHAGAAAHQgQgCgPAQQgIAIgLASQgNASgLAMQgZAZglAOIgiALIgIABQgHAAAHgJg");
	this.shape_10.setTransform(56,116.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FAC099").s().p("AhZBDQAKgNAIgjIABgDQgGgNgQgDQA1g6ApAOIARgHIAYgMQAMgIAIgDQAOgCAJAIQAIAHgBAGQgQgBgPARQgJAIgKATQgNATgMAMQgWAagoAQQgTAIgQAFIgKACQgHAAAHgJg");
	this.shape_11.setTransform(55.8,113.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FABB90").s().p("AhbBHQALgMAIgmIABgEQgEgEgGABQAkgvAjgGIAdgOQALgFANgJQAMgIAIgDQAOgCAKAIQAIAHAAAGQgRgBgQASQgIAJgLATQgMAVgLAMQgXAbgpASIgjANQgIADgDAAQgHAAAGgJg");
	this.shape_12.setTransform(54,110.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#F8B486").s().p("AhfBHQAKgNAKgnQASgoAigOIAsgTIAZgNQANgIAJgDQAOgCAKAIQAHAIABAGQgSgBgRASQgIAJgMATQgNAVgMAMQgYAcgrARQgUAIgQAEQgIADgEAAQgHAAAHgJg");
	this.shape_13.setTransform(52.1,107.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F7AE7D").s().p("AhhBIQALgNAJgnIABgDQgEgGgIAAQAqgzAngDIAcgNQAMgEAOgJQAOgIAIgDQAPgDAKAKQAIAHAAAHQgSgCgRATQgJAJgMAUQgNAVgNANQgZAcgsASQgUAIgRAFIgLACQgIAAAIgKg");
	this.shape_14.setTransform(50.7,104.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F9AA74").s().p("AhiBKQAMgPAHgmIABgCQgHgRgUgCQA/hEAwATIARgHQAMgFAOgJQANgIAJgEQAQgCAKAKQAJAHAAAIQgTgCgRATQgKAJgMAUQgOAWgNANQgaAdgtASQgVAIgSAFIgJABQgJAAAJgJg");
	this.shape_15.setTransform(49.2,101.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F8A46B").s().p("AhRBOQAMgOAGgmIABgDQgKg7g3gcQBsg3A0BGIAJgDQANgGAOgIQAOgJAKgEQAPgBALAJQAJAJAAAHQgUgCgSATQgJAJgNAWQgPAWgNANQgdAegsARQgXAJgSAEIgIABQgJAAAKgLg");
	this.shape_16.setTransform(45.9,97.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#F79D61").s().p("AhABrQAOgRAEglIABgBQgNhnhbg7QCXgiA2B4IAHgDQAMgFAPgIQAPgJAJgDQARgDAKALQAJAIAAAHQgTgCgUAUQgJAJgNAUQgPAZgOANQgeAegtATQgXAIgUAEIgHABQgJAAAKgLg");
	this.shape_17.setTransform(42.5,92.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#F79858").s().p("AhCBtQAOgQAEgmIAAgCQgLhqhdg8QCagiA3B8IAGgDQAOgFAPgJQAPgJAKgDQAQgCALAKQAJAJAAAIQgUgDgTAUQgKAKgOAUQgQAZgOAOQgfAegvASQgYAJgTAFIgGAAQgKAAALgMg");
	this.shape_18.setTransform(40.9,89);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#F5924F").s().p("AhGBwQAOgRAFgmIABgCQgLhtheg/QCeggA3B/IAHgCQANgGAQgJQAQgIAKgEQARgCALALQAJAJgBAIQgUgDgUAUQgLAIgOAWQgRAbgOAOQggAegxASQgYAIgUAFIgHABQgKAAAMgNg");
	this.shape_19.setTransform(39.3,85.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#F58C46").s().p("Ah1BOQAOgPAMgtIACgDQgIgPgUgFQBJhHA0AWIAWgIIAegNQAQgJAKgEQASgBALALQAJAJAAAJQgVgEgWAUQgKAKgPAWQgSAZgPAOQggAeg0ASQgYAIgVAEIgLACQgLAAALgLg");
	this.shape_20.setTransform(41.1,85.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#F6873D").s().p("Ah7BNQAPgOAPguIACgEQgEgGgIAAQA1g2AsgCIAngNIAfgNQARgJALgDQARAAALALQAJAKAAAIQgWgDgWATQgLAKgQAWQgTAZgQANQghAfg2AQQgZAHgVAEIgMACQgMAAALgLg");
	this.shape_21.setTransform(39.3,82.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#F58133").s().p("AiABLQAPgOASgvQAdgwAtgMIA6gQQAPgEARgIQASgIALgDQASAAALAMQAIALgBAIQgVgFgXAUQgMAJgSAXQgUAYgRANQgjAeg4AOQgZAHgWAEIgLABQgOAAAMgLg");
	this.shape_22.setTransform(37.7,79.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#F57C2A").s().p("AiFBGQARgOATguIACgDQgEgHgJgBQA/g4AuAFIAngJQAQgEASgHQATgHALgCQATABAKANQAIALgBAIQgWgGgZATQgMAJgTAWQgWAYgSANQglAdg6ALQgaAGgXACIgIABQgRAAAOgMg");
	this.shape_23.setTransform(36.7,76.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#F47621").s().p("AiKA+QATgNATgtIACgCQgEgVgWgJQBfg/A0AlIAXgEIAkgIQATgGALgCQATACAKAPQAHALgBAJQgWgIgbARQgNAJgWATQgYAYgTAMQgoAbg8AIQgbADgXABIgCAAQgYAAASgNg");
	this.shape_24.setTransform(35.5,73.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F47621").s().p("AiBBlQgaABAUgOQATgNARgrIADgEQAEhHg3gxQCNghAqBhIAMgBQAQgDATgGQAUgGALgBQATACAKAPQAHALgCAJQgWgIgbAQQgNAIgVAUQgZAZgTAMQgoAag8AHQgYADgVAAIgFAAg");
	this.shape_25.setTransform(35,68.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#F47621").s().p("Ah7CBQAUgPAPgpIABgBQAQh6hYhcQC2AEAcCXIAIgCQAQgCAUgGQATgEAMgBQATACAJANQAHAMgCAJQgWgIgbARQgNAIgVAUQgZAZgTALQgpAag8AHQgbADgXAAQgXAAATgOg");
	this.shape_26.setTransform(33.6,62.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#F47621").s().p("Ah5CPQgYgBAUgNQAVgOAPgpIABgCQARh5hXhdQC2AGAbCWIAHgBQAQgCAUgFQAUgGALAAQATABAJAPQAHAMgCAIQgVgIgbARQgOAIgWAUQgZAYgTAMQgoAZg8AHQgYACgVAAIgGAAg");
	this.shape_27.setTransform(33.5,60);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#F47621").s().p("AiLBXQATgNAUgrIADgEQAGhHg1gwQCNgeAoBhIALgBQAQgCATgFQAUgGAMgBQASADAKAOQAHAMgCAJQgWgJgbAPQgOAIgVAUQgaAYgTALQgpAZg8AGQgcACgWAAQgbAAAUgNg");
	this.shape_28.setTransform(34.2,61.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F47621").s().p("AiDBGQgdgBASgLQATgNAZgtIACAAQgEgVgUgKQBeg5AyAkIAagDQAQgCAUgFQATgGAMgBQATADAIAPQAIAMgDAJQgVgJgcAQQgNAIgWASQgZAYgUALQgpAYg8AGQgXACgUAAIgHAAg");
	this.shape_29.setTransform(33.9,62.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#F47621").s().p("AiPA4QATgLAagtIADgBQgDgIgIgCQBDguAsAHIAugFQAQgCAUgEQATgGAMgBQATAEAJAOQAHAMgDAJQgVgJgbAQQgOAIgWARQgaAYgUALQgpAYg8AFQgbACgXAAQgeAAASgNg");
	this.shape_30.setTransform(33.8,60.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#F47621").s().p("AiQA3QATgLAcgsQAmgtAxgEIA+gGQAQgCAUgEQAUgFALgBQATADAJAPQAGAMgCAJQgVgJgcAPQgOAIgWASQgZAXgUAKQgqAZg8AEIgyACQgfgBASgMg");
	this.shape_31.setTransform(33.9,58);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#F47621").s().p("AiFBCQgdgBATgLQATgMAagqIADgDQgEgJgIgDQBHgwAuAMIAqgDQAQgBAUgFQATgFAMgBQATAEAIAPQAHAMgCAIQgVgJgdAPQgNAIgXASQgaAWgTALQgqAYg8ADIgjACIgQgBg");
	this.shape_32.setTransform(34.6,56);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#F47621").s().p("AiGBDQgbgBATgMQAUgMAXgqIACgBQgCgXgVgLQBjg4AxAqIAXgCQAQgBAUgFQAUgFAMAAQASADAJAPQAGANgCAIQgVgJgcAPQgOAHgWASQgbAWgUALQgpAXg9ADIgaABIgYgBg");
	this.shape_33.setTransform(35.3,54);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#F47621").s().p("AiIBjQgZgCAUgMQAVgMAUgpIADgDQALhHg0g2QCOgVAkBkIAMgBQAQgBAUgEQATgFAMAAQATAEAIAPQAGAKgCAJQgVgKgcAPQgPAHgWATQgaAXgUAKQgrAXg8ADIgXAAIgcgBg");
	this.shape_34.setTransform(36.2,48.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#F47621").s().p("AiKCPQgYgDAWgMQAVgLAUgnIAAgBIABgBQAeh3hNhmQC0AaALCYIAIAAQAQgBAUgDQAUgDAMgBQASAFAIAPQAGANgDAIQgUgLgeAPQgOAGgXATQgcAUgUAKQgrAVg8AAQgdAAgWgDg");
	this.shape_35.setTransform(37.1,42);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F47621").s().p("AhZCTQgdgBgXgEQgXgEAWgLQAWgLAWgmIABgBQAjh2hIhqQCzAiAECZIAIAAQAQAAAUgCQAUgDAMABQASAFAHAQQAGAMgEAJQgUgLgdAMQgPAGgYARQgdAUgUAJQgnARg0AAIgNgBg");
	this.shape_36.setTransform(38,39.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#F47621").s().p("AhaBkQgcgDgXgEQgZgFAVgJQAVgKAcgnIAEgEQAShEgrg5QCOgEAYBnIAMAAQAQAAAUAAQAUgCAMABQASAEAGAQQAFANgEAIQgTgMgeAMQgPAFgZAQQgdATgWAIQgiANgsAAIgagBg");
	this.shape_37.setTransform(38.9,41.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#F47621").s().p("AhbA8QgcgDgWgGQgcgGAVgIQAVgJAhgkIADgCQAAgVgSgOQBqglAqAuIAZADQAQABAVgBQAUgBAMABQASAIAFAQQAFALgFAIQgTgLgeAIQgPAFgaAPQgdASgWAHQgeAKgmAAQgSAAgUgCg");
	this.shape_38.setTransform(40.1,43.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#F47621").s().p("AhcAxQgbgEgXgFQgdgHAVgIQAVgHAkgkIADgDQgBgIgHgEQBMgeAqASIAuAGQAQACAVAAQAUgBALACQASAHAFARQAFAMgFAHQgSgLgfAHQgQAEgaAPQgeARgWAGQgbAIghAAQgWAAgYgEg");
	this.shape_39.setTransform(41.5,42.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#F47621").s().p("AhdAuQgbgFgWgGQgegIAVgHQAVgGAmgkQAxgjAuAJIBBAKQAQACAVABQAUgBAMADQARAHAFARQADAMgEAHQgSgLgfAGQgQAEgbAOQgfAQgWAFQgYAHgdAAQgZAAgcgFg");
	this.shape_40.setTransform(43.1,40.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#F47621").s().p("AheAvQgcgFgWgHQgcgJAWgGQAWgHAkgiIAEgCQgBgKgIgFQBTgcApAZIAqAIQAQACAUABQAUAAAMACQARAJAFARQADALgEAIQgTgOgfAHQgPAEgbANQggAQgWAFQgVAGgbAAQgbAAgfgHg");
	this.shape_41.setTransform(45.1,39);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#F47621").s().p("AhgA4QgbgGgWgHQgZgJAVgGQAXgHAighIACgCQAEgXgRgQQBugbAmA1IAYAFQAPADAVABQAVABALADQASAIADAPQAEANgFAIQgSgOggAHQgPADgbANQggAPgWAFQgUAEgYAAQgeAAghgHg");
	this.shape_42.setTransform(47,36.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#F47621").s().p("AhhBjQgcgHgVgHQgYgJAXgHQAXgGAfgiIADgCQAfhDgkhCQCQASAIBqIAMACQAPADAVACQAUAAAMADQARAJAEARQADAOgEAHQgSgPggAHQgPAEgcAMQggAPgWAEQgTAFgXAAQgfAAgigIg");
	this.shape_43.setTransform(49.2,31.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#F47621").s().p("AhiCTQgcgHgVgIQgXgIAYgHQAYgGAdgjIABAAQA6hvg0h3QCsBEgaCZIAIACQAQADAUABQAVABAMADQARAJAEARQADAOgFAHQgSgPggAHQgPADgcANQggAOgWAFQgTAEgWAAQgfAAgjgIg");
	this.shape_44.setTransform(51.3,25.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#F47621").s().p("AhiCTQgcgHgVgJQgXgIAYgGQAYgHAdgiIACgBQA5hugzh3QCsBEgaCZIAIACQAQADAUACQAVABALADQASAJADARQADANgFAIQgRgPggAGQgQAEgbAMQggAPgXAEQgSAEgWAAQgfAAgkgIg");
	this.shape_45.setTransform(53.4,24.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#F47621").s().p("AhhBjQgcgHgVgIQgagJAXgGQAXgHAhgiIAGgDQAchCgihAQCPASAIBpIALACQAQADAVACQAUABAMACQASAKADARQADANgFAIQgSgPgfAHQgQADgcAMQggAPgWAEQgTAEgXAAQgfAAgigHg");
	this.shape_46.setTransform(55.4,28.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#F47621").s().p("AhfA3QgcgGgWgIQgcgJAWgGQAWgGAmgiIADgCQACgVgQgQQBsgaAnAzIAaAFQAPADAWABQAUABALADQASAJAEAOQADAOgFAIQgSgPggAHQgPADgcANQggAPgWAFQgUAFgXAAQgfAAghgIg");
	this.shape_47.setTransform(57.4,31.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#F47621").s().p("AhfAuIgxgNQgegJAVgHQAWgFAngjIAEgCQgBgIgHgFQBPgaApAVIAuAKQARACAUABQAVABALACQASAJAEARQADAMgEAIQgTgPgfAHQgQAEgcANQgfAPgWAFQgWAFgZAAQgdAAgggHg");
	this.shape_48.setTransform(59.6,31.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#F47621").s().p("AhfAtQgbgGgWgGQgegJAUgHQAWgGAngjQAzgiAvAJIBBAMQAQADAVABQAUAAAMADQASAIAEARQAEAMgFAHQgSgOggAIQgQAEgbANQggAPgWAGQgWAGgbAAQgcAAgfgHg");
	this.shape_49.setTransform(61.8,30.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#F47621").s().p("AhfAxQgcgGgWgGQgdgJAWgHQAWgHAlgjIADgCQgBgJgIgFQBUgeAqAZIAqAHQAQADAVAAQAVAAALADQASAIAFARQADAMgEAHQgTgOggAIQgPAEgbAOQggAQgWAFQgYAGgdAAQgaAAgdgFg");
	this.shape_50.setTransform(64.3,30.2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#F47621").s().p("AhgA6QgcgFgWgHQgbgIAXgHQAXgHAhgjIACgBQAEgXgSgRQBugfAoA2IAYAEQARADAUAAQAVAAAMADQASAIAEAPQAEAOgFAIQgSgPggAIQgQAEgbAOQggAQgWAFQgXAGgbAAQgcAAgegGg");
	this.shape_51.setTransform(66.7,28.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#F47621").s().p("AhhBmQgcgGgWgHQgZgIAXgHQAXgIAfgkIACgCQAchFgnhBQCSALANBrIAMACQAQADAWAAQAUAAAMADQASAIAFARQADANgEAIQgTgOggAIQgQAEgbAOQgfARgWAFQgYAHgeAAQgaAAgdgFg");
	this.shape_52.setTransform(69.3,24.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#F47621").s().p("AhhCWQgdgFgWgGQgXgHAYgIQAXgJAbgkIACgBQAyhzg7h1QCyA5gQCdIAIABQAQACAVAAQAVAAAMACQASAIAEARQAEAOgEAHQgTgNgfAIQgQAFgbAOQgfARgXAGQgZAHgfAAQgZAAgbgFg");
	this.shape_53.setTransform(71.8,19.3);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#F47621").s().p("AhhCXQgcgEgXgHQgXgGAYgJQAXgJAagkIACgCQAwh0g+h0QCzA2gMCdIAIABQAQABAVAAQAVAAAMABQASAIAFARQAEANgEAIQgTgNgfAJQgQAFgbAPQgfARgWAHQgcAJgiAAQgXAAgYgEg");
	this.shape_54.setTransform(74.3,19.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#F47621").s().p("AhfBmQgcgEgXgGQgagFAWgJQAWgJAegnIAFgEQAWhFgqg9QCSADAUBpIAMABQAQABAVAAQAVgBAMABQASAHAGARQAFAOgFAIQgTgNggAJQgPAGgbAPQgeASgWAHQgfALglAAQgUAAgVgDg");
	this.shape_55.setTransform(76.5,23.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#F47621").s().p("AhdA/QgcgDgXgFQgegGAVgIQAWgKAhgmIADgCQAAgVgSgOQBrgpAsAvIAaACIAmgBQAUgCAMACQATAHAGAQQAFANgEAHQgUgLgfAJQgPAGgaAQQgfATgVAHQghAMgqAAQgRAAgRgBg");
	this.shape_56.setTransform(78.8,27);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#F47621").s().p("AhbA3QgcgCgXgEQgfgEAUgKQAVgJAigoIAEgDQgDgIgHgDQBLgmAsAPIAwACQARABAVgCQAUgDANABQASAGAHARQAFANgDAIQgVgMgfAMQgPAGgZAPQgeAUgWAIQglAPgxAAIgWgBg");
	this.shape_57.setTransform(81.1,27.6);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#F47621").s().p("AhaA5IgzgEQgggEAUgKQAUgKAigpQAtgrAyACIBCACQAQgBAVgCQAVgDAMAAQATAGAHAQQAFANgDAJQgVgMgeANQgPAGgZAQQgdAVgWAJQgoASg2AAIgOgBg");
	this.shape_58.setTransform(83.5,27.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#F47621").s().p("AiNA6QgegDAUgLQAVgLAdgpIADgDQgCgKgJgDQBOgsAvAQIArgBQARAAAVgDQAUgEAMAAQAUAFAHAQQAGANgDAJQgVgLgeAOQgPAHgZAQQgcAWgVAJQguAVg+AAIgHAAQgZAAgUgDg");
	this.shape_59.setTransform(86.2,27.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#F47621").s().p("AiMBCQgcgCAVgMQAUgLAagpIACgCQgBgYgVgMQBpg1AwAtIAYgBQARgBAVgEQAUgEAMAAQATAFAIAQQAGAMgDAJQgVgLgdAPQgPAHgYARQgcAWgVAKQgtAWg9AAQgdAAgXgCg");
	this.shape_60.setTransform(92.2,26.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#F47621").s().p("AiMBlQgZgCAVgMQAUgMAWgqIADgDQAMhJg0g3QCRgVAjBoIANAAQAQgCAVgEQATgEAMAAQAUAEAIAPQAGALgCAIQgWgKgdAPQgOAIgYATQgbAXgUAKQgsAWg9ADIgMAAQgWAAgTgCg");
	this.shape_61.setTransform(98.3,23.2);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#F47621").s().p("AiFCRQgXgBAVgNQAVgNARgpIACgBQAXh7hUhjQC4APAUCaIAIAAQAQgCAVgFQAUgEAMgBQATAEAIAPQAHAMgDAJQgVgJgcAPQgPAIgXATQgaAYgUAKQgrAYg9AEIgXABIgdgCg");
	this.shape_62.setTransform(103.7,19.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#F47621").s().p("AiACRQgXgBAVgNQAUgOARgpIABgBQAUh7hVhgQC3ALAXCYIAIAAQARgCATgFQAUgGAMAAQATADAJAPQAHAMgDAJQgVgJgcAQQgOAIgWAUQgaAXgUALQgpAZg9AFIgqABIgKAAg");
	this.shape_63.setTransform(109.4,19.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#F47621").s().p("AiMBXQAUgNATgrIAEgFQAFhGg2gxQCOgeAoBhIALgCQARgCATgGQATgFAMgBQATACAKAPQAHALgCAJQgWgIgbAOQgOAJgVAUQgZAZgUALQgpAag8AGQgcADgWAAQgbAAATgOg");
	this.shape_64.setTransform(115.8,23.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#F47621").s().p("AiLA/QASgNAWguIADgCQgFgTgUgJQBbg9A0AhIAZgEQAQgCATgGQATgGAMgCQATACAJAPQAHALgBAJQgWgIgaARQgOAJgVATQgYAYgTAMQgoAbg7AIQgcADgWABIgEAAQgYAAARgMg");
	this.shape_65.setTransform(121.4,26.6);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#F47621").s().p("AiKBCQASgNAWgvIADgDQgEgFgIgCQA+gzAtAEIAtgIQAPgDAUgHQATgGALgCQATACAKANQAHAMgBAIQgWgGgaARQgNAJgUAVQgYAXgSANQgnAbg7AKIgxAGIgHAAQgVAAAPgMg");
	this.shape_66.setTransform(127.1,26.8);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#F47621").s().p("AiHBGQAQgNAWgwQAggvAvgJIA8gNIAjgKQASgIALgCQATABAKAOQAIALgBAIQgWgGgZASQgNAJgTAXQgXAXgSANQglAcg6AMIgxAHIgJAAQgSAAAOgLg");
	this.shape_67.setTransform(133,27.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#F47621").s().p("AiEBIQARgOASguIACgDQgEgJgJAAQA+g4AuAEIAngJQAQgEASgIQASgHALgDQATAAAKAOQAJAKgBAJQgWgGgYATQgNAKgSAWQgWAYgSAOQgkAdg5ANQgbAGgWACIgJABQgQAAANgMg");
	this.shape_68.setTransform(138.9,27.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#F47621").s().p("Ah/BKQAQgPAOgtIABgCQgGgUgWgGQBUhIA2AeIAWgGQAPgEASgIQASgIALgDQATAAALANQAIAKgBAJQgWgFgYATQgMAKgRAWQgVAZgRANQgkAeg4APQgaAGgWADIgJABQgOAAAOgMg");
	this.shape_69.setTransform(145,28.5);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#F47621").s().p("AhtBZQAQgQALgsIACgCQgGhGg8gmQCBg0A3BXIAKgEQAPgFASgIIAcgLQATgBALAMQAIALAAAIQgWgEgXAUQgMAKgRAVQgUAagQAPQglAfg1APQgbAIgWADIgIABQgMAAANgNg");
	this.shape_70.setTransform(149.3,28);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#F47621").s().p("AhoBZQAQgQAJgsIACgDQgIhEg8glQB/g3A3BUIALgDQAPgFARgJQARgJALgDQASgBAMAMQAIAJAAAJQgWgEgWAVQgLAKgRAXQgTAZgPAPQgkAfg1ASQgaAIgVAEIgIABQgMAAAMgNg");
	this.shape_71.setTransform(154.9,28.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#F47621").s().p("AhNB3QAQgRAGgqIABgBQgLh0hkhEQCpghA5CIIAIgDQAOgFARgKQARgJAKgDQASgCAMAMQAJAJAAAIQgWgDgVAWQgLAIgQAXQgSAcgQAPQgiAgg0ATIguANIgIABQgLAAAMgOg");
	this.shape_72.setTransform(158.6,27);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#F47621").s().p("AhgBZQAOgRAIgsIADgFQgNhBg7ggQB6g8A7BPIAKgEQANgGARgJQAQgKALgEQASgCAMAMQAJAJAAAIQgWgCgUAVQgLALgPAXQgRAagQAPQghAhgzATQgZAJgVAFIgKACQgKAAALgMg");
	this.shape_73.setTransform(165.8,31.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#F47621").s().p("Ah0BXQAOgRAKguIABgDQgJgRgUgEQBGhLA1ASIAWgJQAPgGAQgKQAQgKAKgEQASgCAMALQAJAIABAJQgWgCgVAWQgKALgOAYQgQAZgPAPQgeAig0AVQgZAJgUAGQgIACgFAAQgJAAAJgKg");
	this.shape_74.setTransform(173.3,32.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#F47621").s().p("AhyBZQANgQAKgwIACgEQgFgFgIAAQAug6ArgHIAmgRQANgHAQgKQAQgKAKgEQASgDALALQAKAIABAJQgWgCgUAWQgKALgNAYQgQAagOAPQgeAigzAWQgYAKgUAGQgKADgEAAQgJAAAJgKg");
	this.shape_75.setTransform(178.7,33.7);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#F47621").s().p("AhwBaQAMgQAKgwQATgxApgTIA0gZQANgHAQgKQAPgLAKgEQASgDAMAKQAJAIABAJQgVgCgTAXQgKALgNAYQgPAagNAQQgdAigyAXQgXAKgUAHQgKADgEAAQgIAAAHgKg");
	this.shape_76.setTransform(184.4,34.9);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#F47621").s().p("AhsBbQAMgRAHguIABgDQgFgGgJAAQAtg/AtgGIAggRQANgHAPgLQAPgKAKgFQARgDANAJQAJAIABAIQgVAAgTAWQgIAMgNAYQgOAagNAQQgcAjgyAYQgXALgTAGQgJAEgFAAQgHAAAIgLg");
	this.shape_77.setTransform(190.2,36.4);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#F47621").s().p("AhjBbQANgRADgsIABgCQgLgTgVgBQA/hRA3AQIATgKQANgHAPgLQAOgLAKgFQAQgDANAJQAKAIABAIQgUgBgTAXQgIALgNAZQgNAagMAQQgdAkguAYQgYALgTAIQgIADgDAAQgIAAAIgMg");
	this.shape_78.setTransform(195.5,38.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#F47621").s().p("AhFBbQAMgSABgpIABgDQgUg+g+gYQBshJBDBEIAJgFQANgHAOgLQAPgMAJgEQARgFAMAKQAKAHABAIQgUAAgSAXQgJAMgLAYQgMAagNAQQgbAkguAZQgWAMgUAIQgGACgEAAQgHAAAIgMg");
	this.shape_79.setTransform(198.6,39.9);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#F47621").s().p("AgmBuQALgSgCgoIABgBQgdhqhogvQCZg7BKB1IAGgEQAMgHAOgLQAPgMAJgFQAQgFANAKQAKAGABAIQgUABgRAXQgJAMgKAYQgMAbgMAQQgbAkgvAaQgUAMgTAIIgJACQgHAAAJgNg");
	this.shape_80.setTransform(201.6,39.9);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#F47621").s().p("AgiBsQALgSgCgnIAAgCQgfhohogsQCXg+BLByIAGgDQAMgIAOgMQAOgLAJgGQAQgEANAIQAKAHABAIQgUABgQAYQgIALgKAZQgMAagLAQQgaAkguAbQgUANgSAIQgGACgDAAQgHAAAIgNg");
	this.shape_81.setTransform(206.9,42.1);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#F47621").s().p("Ag8BfQALgSAAgqIABgEQgYg6g9gTQBmhNBEA+IAHgGQANgHANgMQANgMAKgFQAQgGANAJQAKAHABAHQgUACgQAXQgHAMgKAYQgLAbgLAQQgYAkgsAcQgVAMgSAJQgIADgDAAQgGAAAGgLg");
	this.shape_82.setTransform(214.5,45.6);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#F47621").s().p("AhUBhQAJgRABgsIAAgDQgKgOgUAAQAzhRA1AHIASgMQAMgIANgMQANgMAJgGQAQgFANAIQAKAGABAIQgUACgPAXQgHAMgJAYQgKAbgLAQQgYAlgqAcQgUANgTAJQgJAFgEAAQgGAAAHgLg");
	this.shape_83.setTransform(222.2,47.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#F47621").s().p("AhfBYQAKgPAFgsIABgDQgFgFgHABQAkg4AmgKIAfgTQAMgHAOgKQANgKAIgFQAQgEAMAIQAJAHABAHQgTABgQAVQgIALgKAXQgMAYgLAPQgXAhgsAYIgmASQgJAFgEAAQgGAAAGgKg");
	this.shape_84.setTransform(223.5,48);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#F47621").s().p("AhfBPQAKgOAIgpQAQgrAjgQIAtgXQALgFAOgKQANgJAIgEQAPgCALAIQAIAHABAHQgTAAgQATQgIAJgLAVQgNAXgLANQgYAfgsATIgkAQQgJADgEAAQgHAAAHgJg");
	this.shape_85.setTransform(224.1,48.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#F47621").s().p("AheBFQALgNAJgmIABgCQgEgGgHAAQApgxAlgCIAbgMIAYgMQAOgJAIgDQAOgBAKAIQAHAHAAAHQgRgBgQARQgJAJgLATQgOAUgMAMQgYAbgqARQgUAHgRAFIgKACQgHAAAHgJg");
	this.shape_86.setTransform(225,48.9);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#F47621").s().p("AhbA7QALgLAJgiIABgCQgFgNgRgFQA7g4ApAUIAQgFIAYgKQAMgHAJgDQANAAAIAJQAHAHAAAGQgQgDgRAQQgJAHgMASQgOASgMALQgYAXgqANQgTAGgQADIgIABQgIAAAJgJg");
	this.shape_87.setTransform(225.8,49.4);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#F47621").s().p("AhQA+QALgLAJgeIABgDQgCgwgpgcQBdghAiA+IAIgBQALgDANgGQAMgFAIgCQANAAAHAJQAGAHgBAGQgPgDgRANQgIAHgNANQgOATgNAJQgYAVgnAJQgTAEgQACIgEABQgKAAAKgJg");
	this.shape_88.setTransform(225.9,48.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#F47621").s().p("AhIBRQAMgKAJgaIABgBQAGhNg7g4QBzgDAXBeIAEgBQALgBAMgDQAMgFAHgBQANABAGAIQAFAGgBAGQgOgFgRAMQgIAGgNAOQgPAQgMAIQgZASglAGQgSADgOAAIgBAAQgNAAALgJg");
	this.shape_89.setTransform(226.4,46.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},21).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_89}]},1).wait(59));

	// Layer 15 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AtfHDIgBgFIAIxAQMkjaOUDVIAADVQxeF5oAMqQhXiHgKing");
	mask.setTransform(163,223.9);

	// Layer 16
	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.102)","rgba(255,255,255,0)"],[0,0.51,1],96,-10.5,271,-10.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_90.setTransform(160.5,208.8);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.114)","rgba(255,255,255,0)"],[0,0.51,1],86.6,-10.3,261.6,-10.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_91.setTransform(160.5,208.8);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.122)","rgba(255,255,255,0)"],[0,0.51,1],77.3,-10.1,252.3,-10.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_92.setTransform(160.5,208.8);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.133)","rgba(255,255,255,0)"],[0,0.51,1],67.9,-9.8,242.9,-9.8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_93.setTransform(160.5,208.8);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.141)","rgba(255,255,255,0)"],[0,0.51,1],58.6,-9.6,233.6,-9.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_94.setTransform(160.5,208.8);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.153)","rgba(255,255,255,0)"],[0,0.51,1],49.3,-9.4,224.3,-9.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_95.setTransform(160.5,208.8);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.165)","rgba(255,255,255,0)"],[0,0.51,1],39.9,-9.2,214.9,-9.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_96.setTransform(160.5,208.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.173)","rgba(255,255,255,0)"],[0,0.51,1],30.6,-9,205.6,-9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_97.setTransform(160.5,208.8);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.184)","rgba(255,255,255,0)"],[0,0.51,1],21.2,-8.8,196.2,-8.8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_98.setTransform(160.5,208.8);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.196)","rgba(255,255,255,0)"],[0,0.51,1],11.9,-8.6,186.9,-8.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_99.setTransform(160.5,208.8);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.204)","rgba(255,255,255,0)"],[0,0.51,1],2.5,-8.4,177.5,-8.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_100.setTransform(160.5,208.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.216)","rgba(255,255,255,0)"],[0,0.51,1],-6.8,-8.1,168.2,-8.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_101.setTransform(160.5,208.8);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.224)","rgba(255,255,255,0)"],[0,0.51,1],-16.2,-7.9,158.8,-7.9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_102.setTransform(160.5,208.8);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.235)","rgba(255,255,255,0)"],[0,0.51,1],-25.5,-7.7,149.5,-7.7).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_103.setTransform(160.5,208.8);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.247)","rgba(255,255,255,0)"],[0,0.51,1],-34.9,-7.5,140.1,-7.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_104.setTransform(160.5,208.8);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.255)","rgba(255,255,255,0)"],[0,0.51,1],-44.2,-7.3,130.8,-7.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_105.setTransform(160.5,208.8);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.267)","rgba(255,255,255,0)"],[0,0.51,1],-53.5,-7.1,121.5,-7.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_106.setTransform(160.5,208.8);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.275)","rgba(255,255,255,0)"],[0,0.51,1],-62.9,-6.9,112.1,-6.9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_107.setTransform(160.5,208.8);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.286)","rgba(255,255,255,0)"],[0,0.51,1],-72.2,-6.7,102.8,-6.7).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_108.setTransform(160.5,208.8);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.298)","rgba(255,255,255,0)"],[0,0.51,1],-81.6,-6.5,93.4,-6.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_109.setTransform(160.5,208.8);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.306)","rgba(255,255,255,0)"],[0,0.51,1],-90.9,-6.2,84.1,-6.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_110.setTransform(160.5,208.8);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.318)","rgba(255,255,255,0)"],[0,0.51,1],-100.2,-6,74.8,-6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_111.setTransform(160.5,208.8);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.329)","rgba(255,255,255,0)"],[0,0.51,1],-109.6,-5.8,65.4,-5.8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_112.setTransform(160.5,208.8);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.337)","rgba(255,255,255,0)"],[0,0.51,1],-118.9,-5.6,56.1,-5.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_113.setTransform(160.5,208.8);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.349)","rgba(255,255,255,0)"],[0,0.51,1],-128.3,-5.4,46.7,-5.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_114.setTransform(160.5,208.8);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.357)","rgba(255,255,255,0)"],[0,0.51,1],-137.6,-5.2,37.4,-5.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_115.setTransform(160.5,208.8);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.369)","rgba(255,255,255,0)"],[0,0.51,1],-146.9,-5,28.1,-5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_116.setTransform(160.5,208.8);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.38)","rgba(255,255,255,0)"],[0,0.51,1],-156.3,-4.8,18.7,-4.8).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_117.setTransform(160.5,208.8);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.388)","rgba(255,255,255,0)"],[0,0.51,1],-165.6,-4.6,9.4,-4.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_118.setTransform(160.5,208.8);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.4)","rgba(255,255,255,0)"],[0,0.51,1],-175,-4.3,0,-4.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_119.setTransform(160.5,208.8);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.408)","rgba(255,255,255,0)"],[0,0.51,1],-184.3,-4.1,-9.3,-4.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_120.setTransform(160.5,208.8);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.42)","rgba(255,255,255,0)"],[0,0.51,1],-193.7,-3.9,-18.7,-3.9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_121.setTransform(160.5,208.8);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.431)","rgba(255,255,255,0)"],[0,0.51,1],-203,-3.7,-28,-3.7).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_122.setTransform(160.5,208.8);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.439)","rgba(255,255,255,0)"],[0,0.51,1],-212.4,-3.5,-37.4,-3.5).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_123.setTransform(160.5,208.8);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.451)","rgba(255,255,255,0)"],[0,0.51,1],-221.7,-3.3,-46.7,-3.3).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_124.setTransform(160.5,208.8);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.463)","rgba(255,255,255,0)"],[0,0.51,1],-231,-3.1,-56,-3.1).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_125.setTransform(160.5,208.8);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.471)","rgba(255,255,255,0)"],[0,0.51,1],-240.4,-2.9,-65.4,-2.9).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_126.setTransform(160.5,208.8);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.482)","rgba(255,255,255,0)"],[0,0.51,1],-249.7,-2.6,-74.7,-2.6).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_127.setTransform(160.5,208.8);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.49)","rgba(255,255,255,0)"],[0,0.51,1],-259.1,-2.4,-84.1,-2.4).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_128.setTransform(160.5,208.8);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.502)","rgba(255,255,255,0)"],[0,0.51,1],-268.4,-2.2,-93.4,-2.2).s().p("AykPCIAA+DMAlJAAAIAAeDg");
	this.shape_129.setTransform(160.5,208.8);

	this.shape_90.mask = this.shape_91.mask = this.shape_92.mask = this.shape_93.mask = this.shape_94.mask = this.shape_95.mask = this.shape_96.mask = this.shape_97.mask = this.shape_98.mask = this.shape_99.mask = this.shape_100.mask = this.shape_101.mask = this.shape_102.mask = this.shape_103.mask = this.shape_104.mask = this.shape_105.mask = this.shape_106.mask = this.shape_107.mask = this.shape_108.mask = this.shape_109.mask = this.shape_110.mask = this.shape_111.mask = this.shape_112.mask = this.shape_113.mask = this.shape_114.mask = this.shape_115.mask = this.shape_116.mask = this.shape_117.mask = this.shape_118.mask = this.shape_119.mask = this.shape_120.mask = this.shape_121.mask = this.shape_122.mask = this.shape_123.mask = this.shape_124.mask = this.shape_125.mask = this.shape_126.mask = this.shape_127.mask = this.shape_128.mask = this.shape_129.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_90}]}).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[]},1).wait(130));

	// Layer 11
	this.instance_1 = new lib.mc_Star();
	this.instance_1.setTransform(62.5,149.3,0.052,0.052,30.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(16).to({_off:false},0).to({scaleX:0.52,scaleY:0.52,rotation:390.5},4,cjs.Ease.get(1)).to({scaleX:0.05,scaleY:0.05},3).to({_off:true},1).wait(146));

	// Layer 10
	this.instance_2 = new lib.mc_Glow();
	this.instance_2.setTransform(63,149.3,0.5,0.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(16).to({_off:false},0).to({alpha:1},3).wait(3).to({_off:true},1).wait(147));

	// Layer 8
	this.instance_3 = new lib.mc_Light();
	this.instance_3.setTransform(162.4,380.5,1,1,-79.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({rotation:-23.1,guide:{path:[161.3,380.4,68,326.5,67.3,264.4,67.3,208.4,67.3,152.3]},alpha:0.25},18).to({_off:true},1).wait(151));

	// Layer 6
	this.instance_4 = new lib.mc_Light();
	this.instance_4.setTransform(258,150.8,1,1,29.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({rotation:-9.5,guide:{path:[258,150.7,163.1,124.6,68.7,150.8]}},18).to({_off:true},1).wait(151));

	// Layer 5
	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#D7D7D7").s().p("AOPKAIAAxZQuGjvuVDvIAARZIACAaIhhhLIAAxsQP0kCPiECIAARrIhfBRIADgfg");
	this.shape_130.setTransform(162.6,201.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_130).wait(170));

	// Shield
	this.instance_5 = new lib.mc_Shield();
	this.instance_5.setTransform(162.5,260);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(170));

	// BG
	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.rf(["#E35248","#B8273A"],[0,1],0,0,0,0,0,288.5).s().p("EgY/AlfMAAAhK9MAx/AAAMAAABK9g");
	this.shape_131.setTransform(160,240);

	this.timeline.addTween(cjs.Tween.get(this.shape_131).wait(170));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.6,240.1,503.2,480);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;